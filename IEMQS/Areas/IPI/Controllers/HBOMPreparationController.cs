﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.IPI.Models;
using IEMQSImplementation;
using IEMQS.Models;
using System.Text;
using IEMQS.DESCore.Data;

namespace IEMQS.Areas.IPI.Controllers
{
    public class HBOMPreparationController : clsBase
    {
        List<ProjectNodesDtlEnt> listProjectNodesDtlEnt = null;
        List<ProjectLinkDtlEnt> listProjectLinkDtlEnt = null;
        public StringBuilder html { get; private set; }

        string ASMENUM = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
        string TJFENUM = clsImplementationEnum.NodeTypes.TJF.GetStringValue(); 
        string PendingENUM = clsImplementationEnum.KitStatus.Pending.GetStringValue(); 
        string RequestENUM = clsImplementationEnum.KitStatus.Request.GetStringValue();

        // GET: IPI/HBOMPreparation
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetHBOMGridDataPartial(string project)
        {
            ViewBag.Project = project;

            List<HBOMModel> list = GetHBOMData(project, "");

            //TempData["Data"] = list;

            ViewBag.Level = 0;
            foreach (var item in list.Where(c => c.ParentPart == ""))
            {
                //bool isChild = false;

                //if (list.Count(c => c.ParentPart == item.Part) > 0)
                //    isChild = true;

                ViewBag.ParentSeries = "-" + Convert.ToString(item.Part.Replace(".", "").Replace(" ", "")) + "-";

                ViewBag.ChildParts = ViewBag.ChildParts + GenerateChildPartHTML(item, true, true, ViewBag.ParentSeries, 0);

                //GenerateParentParts(item, list, "-" + Convert.ToString(item.Part.Replace(".", "").Replace(" ", "")) + "-");
            }

            //ViewBag.ChildParts = returnString;
             var typeASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
            ViewBag.lstSection = db.SP_IPI_GETHBOMLIST_VW(project).Where(c => c.PartLevel == 2 && c.ProductType == typeASM).Select(s => new SelectItemList { id = s.Part, text = s.Description }).ToList();
            bool IsPLMProject = Manager.IsPLMProject(project);
            List<string> lstDrawing = new List<string>();
            var SkipPLM = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SkipPLM.GetStringValue()));

            if (IsPLMProject)
            {
                if (!SkipPLM)
                {
                    lstDrawing = getDrawingNoList(project);
                    if (lstDrawing.Count == 0)
                        lstDrawing.Add("Dummy Drawing Number");
                }
            }
            else {
                var contract = db.COM005.Where(i => i.t_sprj == project).Select(i => i.t_cono).FirstOrDefault();

                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    try
                    {
                        List<string> lstProject = db.DES051.Where(x => x.Contract == contract).Select(x => x.Project).ToList();
                        lstDrawing = db.DES059.Where(x => x.DocumentTypeId == 1 && lstProject.Contains(x.Project) && x.DocumentNo != null && x.Status == "Completed" && x.IsLatestRevision == true).Select(x => x.DocumentNo).ToList();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
            }

            ViewBag.lstDrawingNo = lstDrawing.AsEnumerable().Select(x => new SelectItemList { id = x, text= x }).ToList();

            return PartialView("_GetHBOMGridDataPartial", list);
        }

        public JsonResult GetSectionPart(string project,string parentPartName)
        {
            var SectionPart = getHBOMPartList(project).FirstOrDefault(f=>f.Part == parentPartName);
            List<string> lstDrawing = getDrawingNoList(project);
            HBOMModel objHBOMModel = new HBOMModel();
            objHBOMModel.RowNo = 0;
            objHBOMModel.Part = SectionPart.Part.Trim();
            objHBOMModel.PartLevel = SectionPart.PartLevel;
            objHBOMModel.ParentPart = SectionPart.ParentPart.Trim();
            objHBOMModel.Description = SectionPart.Description.Trim();
            objHBOMModel.ItemGroup = SectionPart.ItemGroup.Trim();
            objHBOMModel.RevisionNo = SectionPart.RevisionNo + "".Trim();
            objHBOMModel.FindNo = Convert.ToString(SectionPart.FindNo).Trim();
            objHBOMModel.MaterialSpecification = SectionPart.MaterialSpecification.Trim();
            objHBOMModel.MaterialDescription = SectionPart.MaterialDescription.Trim();
            objHBOMModel.Length = Convert.ToString(SectionPart.Length).Trim();
            objHBOMModel.Width = Convert.ToString(SectionPart.Width).Trim();
            objHBOMModel.NoOfPieces = Convert.ToString(SectionPart.NoOfPieces).Trim();
            objHBOMModel.Weight = Convert.ToString(SectionPart.Weight).Trim();
            objHBOMModel.Thickness = Convert.ToString(SectionPart.Thickness).Trim();
            objHBOMModel.UOM = SectionPart.UOM.Trim();
            objHBOMModel.DrawingNo = Manager.getDrawingNoByFindNo(objHBOMModel.FindNo, lstDrawing);

            var ParentSeries = "-" + Convert.ToString(SectionPart.Part.Replace(".", "").Replace(" ", "")) + "-";

            return Json(GenerateChildPartHTML(objHBOMModel, true, true, ParentSeries, 0), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPartByDrawingNo(string project,string drawingNo)
        {
            var SectionPart = getHBOMPartList(project);
            int _rowNo = 1;
            string ParentSeries, ChildParts = "";
            ViewBag.Level = 0;
            foreach (var item in SectionPart)
            {
                if (item.FindNo.Length > 4 || !drawingNo.EndsWith(Convert.ToDecimal(item.FindNo).ToString("0000").Substring(0, 2)))
                    continue;

                HBOMModel mItem = new HBOMModel();
                mItem.RowNo = _rowNo;
                mItem.Part = item.Part.Trim();
                mItem.PartLevel = item.PartLevel;
                mItem.ParentPart = item.ParentPart.Trim();
                mItem.Description = item.Description.Trim();
                mItem.ItemGroup = item.ItemGroup.Trim();
                mItem.RevisionNo = item.RevisionNo + "".Trim();
                mItem.FindNo = Convert.ToString(item.FindNo).Trim();
                mItem.MaterialSpecification = item.MaterialSpecification.Trim();
                mItem.MaterialDescription = item.MaterialDescription.Trim();
                mItem.Length = Convert.ToString(item.Length).Trim();
                mItem.Width = Convert.ToString(item.Width).Trim();
                mItem.NoOfPieces = Convert.ToString(item.NoOfPieces).Trim();
                mItem.Weight = Convert.ToString(item.Weight).Trim();
                mItem.Thickness = Convert.ToString(item.Thickness).Trim();
                mItem.UOM = item.UOM.Trim();
                if (item.PartLevel > 2)
                {
                    var Section = getParentSectionDesc(item, SectionPart);
                    mItem.ParentSection = Section[0];
                    mItem.ParentSectionDesc = Section[1];
                }
                mItem.DrawingNo = drawingNo;
                _rowNo = _rowNo + 1;

                ParentSeries = "-" + Convert.ToString(item.Part.Replace(".", "").Replace(" ", "")) + "-";
                ChildParts = ChildParts + GenerateChildPartHTML(mItem, false, false, ParentSeries, 0);
            }

            return Json(ChildParts, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerAndContractNo(string project)
        {
            PurchaseDetail objQCPCommon = new PurchaseDetail();

            try
            {
                objQCPCommon = Manager.getProjectWiseBULocation(project);
                ApproverModel objApproverModel = Manager.GetContractProjectWise(project);
                objQCPCommon.Contract = objApproverModel.Code + "-" + objApproverModel.Name;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return Json(objQCPCommon, JsonRequestBehavior.AllowGet);
        }

        public string GenerateClildControls(string parentPartName, string parentSeries, int hLevel, string project)
        {

            List<HBOMModel> ChildParts = GetHBOMData(project, parentPartName);

            //TempData["Data"] = ChildParts;

            string returnString = "";
            foreach (var item in ChildParts.Where(c => c.ParentPart == parentPartName).ToList())
            {
                bool isChild = false;

                if (GetHBOMDataCount(project, item.Part) > 0)
                    isChild = true;

                returnString = returnString + GenerateChildPartHTML(item, isChild, false, parentSeries, hLevel);
            }

            return returnString;
        }

        //public void GenerateParentParts(HBOMModel BOMModel, List<HBOMModel> ChildParts, string series)
        //{

        //    foreach (var item in ChildParts.Where(c => c.ParentPart.Replace(".", "").Replace(" ", "") == BOMModel.Part.Replace(".", "").Replace(" ", "")).ToList())
        //    {
        //        GenerateChildParts(item, ChildParts, series);
        //    }

        //    //return returnString;
        //}

        //public void GenerateChildParts(HBOMModel BOMModel, List<HBOMModel> ChildParts, string series)
        //{

        //    bool isChild = false;

        //    if (ChildParts.Count(c => c.ParentPart == BOMModel.Part) > 0)
        //        isChild = true;
        //    else
        //    {
        //        series = series.LastIndexOf("S" + "-" + BOMModel.ParentPart.Replace(".", "").Replace(" ", "") + "-") > -1 ? series.Remove(series.LastIndexOf("S" + "-" + BOMModel.ParentPart.Replace(".", "").Replace(" ", "") + "-")) : series;
        //    }

        //    //if (ViewBag.Level >= BOMModel.PartLevel)
        //    //{
        //    //    ViewBag.ParentSeries = ViewBag.ParentSeries.LastIndexOf("S") > -1 ? ViewBag.ParentSeries.Remove(ViewBag.ParentSeries.LastIndexOf("S")) : ViewBag.ParentSeries;
        //    //    ViewBag.Level = ViewBag.Level - 1;
        //    //}
        //    //else
        //    //{
        //    //    ViewBag.Level = BOMModel.PartLevel;
        //    //}

        //    ViewBag.ChildParts = ViewBag.ChildParts + GenerateChildPartHTML(BOMModel, isChild, false, series, 0);

        //    if (isChild)
        //    {
        //        series = series + "S" + "-" + BOMModel.Part.Replace(".", "").Replace(" ", "") + "-";
        //        GenerateParentParts(BOMModel, ChildParts, series);
        //    }


        //}

        public string GenerateChildPartHTML(HBOMModel parts, bool isChildPartAvailable, bool isParentPart, string ParentSeries, int HLevel)
        {
            string htmlString = "";
            if (isParentPart)
            {
                htmlString += "<tr><td><nobr>";
            }
            else
            {
                htmlString += "<tr class=\"expand " + ParentSeries + "\">" +
                                "<td style=\"padding-left: " + ((HLevel) * 20) + "px\">" +
                                "<nobr>";
            }
            htmlString += "<span class='borderleft"+parts.NodeColor+"'>&nbsp;</span>";
            if (isChildPartAvailable)
            {
                if (!isParentPart)
                    ParentSeries = ParentSeries + "S-" + parts.Part.Replace(".", "").Replace(" ", "") + "-";

                //htmlString += "<a onclick=\"OpenSubParts('" + ParentSeries + "', this)\"  style=\"margin-right: 5px;\" class=\"fa fa-plus-circle\"></a>";
                //htmlString += "<a onclick=\"CloseSubParts('" + ParentSeries + "', this)\" style=\"display: none; margin-right: 5px;\" class=\"fa fa-minus-circle\"></a>";

                htmlString += "<a onclick=\"LoadChildParts('" + parts.Part + "', '" + ParentSeries.Replace(".", "").Replace(" ", "") + "', this, " + HLevel + ")\"  style=\"margin-right: 5px;\" class=\"fa fa-plus-circle\"></a>";
                htmlString += "<a onclick=\"CloseSubParts('" + parts.Part.Replace(".", "").Replace(" ", "") + "', this)\" style=\"display: none; margin-right: 5px;\" class=\"fa fa-minus-circle\"></a>";
            }

            htmlString += parts.Part + "</nobr></td>"+
                        "<td>" + parts.DrawingNo+ "</td>" +
                        "<td title=\""+parts.ParentSection+"\">" + parts.ParentSectionDesc + "</td>" +
                        "<td>" + parts.Description + "</td>" +
                        "<td>" + parts.ItemGroup + "</td>" +
                        "<td>" + parts.RevisionNo + "</td>" +
                         "<td><span title=" + parts.ProductType + ">" + parts.FindNo + "</span></td>" +
                        "<td>" + parts.ItemStatus + "</td>" +
                        "<td>" + parts.MaterialSpecification + "</td>" +
                        "<td>" + parts.MaterialDescription + "</td>" +
                        "<td>" + parts.Length + "</td>" +
                        "<td>" + parts.Width + "</td>" +
                        "<td>" + parts.Thickness + "</td>" +
                        "<td>" + parts.NoOfPieces + "</td>" +
                        "<td>" + parts.Weight + "</td>" +
                        "<td>" + parts.UOM + "</td></tr>";

            return htmlString;
        }

        [NonAction]
        public List<HBOMModel> GetHBOMData(string ProjectId, string ParentPartName)
        {
            List<HBOMModel> returnList = new List<HBOMModel>();
            List<string> lstDrawing = getDrawingNoList(ProjectId);

            //var SPresult = db.SP_IPI_GETHBOMLIST(ProjectId, ParentPartName).ToList();
            List<SP_IPI_GETHBOMLIST_VW_Result> SPresult = getHBOMPartList(ProjectId);
            List<SP_IPI_GETHBOMLIST_VW_Result> FilteredResult = new List<SP_IPI_GETHBOMLIST_VW_Result>();
            if (string.IsNullOrEmpty(ParentPartName))
            {
                if (SPresult != null)
                    FilteredResult = (from u in SPresult orderby u.PartLevel ascending select u).Take(1).ToList();
            }
            else
            {
                if (SPresult != null)
                    FilteredResult = (from u in SPresult where u.Project == ProjectId && u.ParentPart.Trim() == ParentPartName.Trim() orderby u.FindNo ascending select u).ToList();
            }

            //bool firstPartAdded = false;

            int _rowNo = 1;
            foreach (var item in FilteredResult)
            {
                //if (!firstPartAdded)
                //{
                //    // add most parent part
                //    string ParentPart = item.ParentPart;
                //    returnList.Add(new HBOMModel() { RowNo = _rowNo, Part = ParentPart.Trim(), ParentPart = "", PartLevel = 0 });
                //    _rowNo = _rowNo + 1;
                //    firstPartAdded = true;
                //}
                HBOMModel mItem = new HBOMModel();
                mItem.RowNo = _rowNo;
                if (string.IsNullOrEmpty(ParentPartName))
                {
                    mItem.Part = item.ParentPart.Trim();
                    mItem.PartLevel = 0;
                    mItem.ParentPart = "";
                }
                else
                {
                    mItem.Part = item.Part.Trim();
                    mItem.PartLevel = item.PartLevel;
                    mItem.ParentPart = item.ParentPart.Trim();
                }
                var nodeData = db.FKM102.FirstOrDefault(w => w.FindNo == item.FindNo && w.Project == item.Project);
                if (nodeData != null)
                {
                    mItem.NodeColor = nodeData.NodeColor;
                    mItem.ProductType = nodeData.ProductType;
                }

                var statusData = db.FKM102.Where(w => w.FindNo == item.FindNo && w.Project == item.Project && w.NodeKey == item.Part && (w.ProductType == ASMENUM || w.ProductType == TJFENUM)).Select(x => ((string.IsNullOrEmpty(x.KitStatus)) ? "PENDING" : (x.KitStatus == RequestENUM ? "REQUESTED" : x.KitStatus))).FirstOrDefault();

                if (statusData != null)
                    mItem.ItemStatus = statusData;

                mItem.Description = item.Description.Trim();
                mItem.ItemGroup = item.ItemGroup.Trim();
                mItem.RevisionNo = item.RevisionNo + "".Trim();
                mItem.FindNo = Convert.ToString(item.FindNo).Trim();
                mItem.MaterialSpecification = item.MaterialSpecification.Trim();
                mItem.MaterialDescription = item.MaterialDescription.Trim();
                mItem.Length = Convert.ToString(item.Length).Trim();
                mItem.Width = Convert.ToString(item.Width).Trim();
                mItem.NoOfPieces = Convert.ToString(item.NoOfPieces).Trim();
                mItem.Weight = Convert.ToString(item.Weight).Trim();
                mItem.Thickness = Convert.ToString(item.Thickness).Trim();
                mItem.UOM = item.UOM.Trim();
                if (item.PartLevel > 2)
                {
                    var Section = getParentSectionDesc(item, SPresult);
                    mItem.ParentSection = Section[0];
                    mItem.ParentSectionDesc = Section[1];
                }
                mItem.DrawingNo = Manager.getDrawingNoByFindNo(mItem.FindNo,lstDrawing);
                returnList.Add(mItem);
                _rowNo = _rowNo + 1;
            }

            //List<HBOMModel> returnList = new List<HBOMModel>
            //                            {
            //                                new HBOMModel { Part="Part1", PartLevel=1, ParentPart="", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part1.1", PartLevel=2, ParentPart="Part1", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part1.2", PartLevel=2,ParentPart="Part1", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2", PartLevel=1,ParentPart="", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1", PartLevel=2,ParentPart="Part2", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.1", PartLevel=3, ParentPart="Part2.1", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.2", PartLevel=3, ParentPart="Part2.1", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.2.1", PartLevel=4, ParentPart="Part2.1.2", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.2.2", PartLevel=4, ParentPart="Part2.1.2", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.2.2.1", PartLevel=5, ParentPart="Part2.1.2.2", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.2.2.2", PartLevel=5, ParentPart="Part2.1.2.2", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part2.1.2.3", PartLevel=4, ParentPart="Part2.1.2", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                                new HBOMModel { Part="Part3", PartLevel=1,ParentPart="", Description="Part1 Desc", FindNo="P1", ItemGroup="Part", Length="10.20", MaterialDescription="Part1 M Desc", MaterialSpecification="Part Specification", NoOfPieces="11", RevisionNo="R0001", Thickness="102.2", UOM="Nos", Weight="12.12", Width="12.53" },
            //                            };

            return returnList;

        }

        public List<SP_IPI_GETHBOMLIST_VW_Result> getHBOMPartList(string Project)
        {
            var GETHBOMLIST = new List<SP_IPI_GETHBOMLIST_VW_Result>();
            if (TempData["PartList"] != null && TempData["HBOMProject"] == Project)
                GETHBOMLIST = (List<SP_IPI_GETHBOMLIST_VW_Result>)TempData["PartList"];
            if (GETHBOMLIST.Count == 0)
            {
                GETHBOMLIST = db.SP_IPI_GETHBOMLIST_VW(Project).OrderBy(x => x.PartLevel).ToList();
                TempData["PartList"] = GETHBOMLIST;
            }
            TempData["HBOMProject"] = Project;
            TempData.Keep("PartList");
            TempData.Keep("HBOMProject");
            return GETHBOMLIST;
        }

        public List<string> getDrawingNoList(string Project)
        {
            var lstDrawing = new List<string>();
            if (TempData["DrawingList"] != null && TempData["HBOMProject"] == Project)
                lstDrawing = (List<string>)TempData["DrawingList"];
            if (lstDrawing.Count == 0)
            {

                bool IsPLMProject = Manager.IsPLMProject(Project);
                var SkipPLM = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SkipPLM.GetStringValue()));

                if (IsPLMProject)
                {
                    if (!SkipPLM)
                    {
                        DrawingReference.hedProjectDocListWebServiceClient drawref = new DrawingReference.hedProjectDocListWebServiceClient();

                        lstDrawing = drawref.getDocumentMasterList(Project).Where(w => w.Contains("|")).Select(s => s.Split('|')[1]).ToList();
                        if (lstDrawing.Count == 0)
                            lstDrawing.Add("Dummy Drawing Number");
                    }
                }
                else
                {
                    var contract = db.COM005.Where(i => i.t_sprj == Project).Select(i => i.t_cono).FirstOrDefault();

                    using (IEMQSDESEntities db = new IEMQSDESEntities())
                    {
                        try
                        {
                            List<string> lstProject = db.DES051.Where(x => x.Contract == contract).Select(x => x.Project).ToList();
                            lstDrawing = db.DES059.Where(x => x.DocumentTypeId == 1 && lstProject.Contains(x.Project) && x.DocumentNo != null && x.Status == "Completed" && x.IsLatestRevision == true).Select(x => x.DocumentNo).ToList();
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                }

                TempData["DrawingList"] = lstDrawing;
            }
            TempData["HBOMProject"] = Project;
            TempData.Keep("DrawingList");
            TempData.Keep("HBOMProject");
            return lstDrawing;
        }

        public string[] getParentSectionDesc(SP_IPI_GETHBOMLIST_VW_Result HBOM, List<SP_IPI_GETHBOMLIST_VW_Result> SPresult)
        {
            foreach (var item in SPresult.Where(w => w.Part == HBOM.ParentPart))
            {
                if (item.PartLevel == 2 && item.ProductType == clsImplementationEnum.NodeTypes.ASM.GetStringValue())
                    return new string[] { item.Part, item.Description };
                else
                    return getParentSectionDesc(item, SPresult);
            }
            return new string[] { "", "" };
        }

        [NonAction]
        public int GetHBOMDataCount(string ProjectId, string ParentPartName)
        {
            int res = 0;
            try
            {
                var SPresult = (List<SP_IPI_GETHBOMLIST_VW_Result>)TempData["PartList"];
                res = (from u in SPresult where u.Project == ProjectId && u.ParentPart.Trim() == ParentPartName.Trim() orderby u.FindNo ascending select u).Count();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return res;
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                //var lst = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == gridType);
                var lst = db.SP_IPI_GETHBOMLIST_VW(gridType).OrderBy(x => x.PartLevel).ToList();
                //var data = lst.FirstOrDefault();
                var data = new SP_IPI_GETHBOMLIST_VW_Result();
                data.Part = Manager.GetProjectAndDescription(lst.FirstOrDefault().Project);
                lst.Insert(0, data);
                // var a = lst.Where(x => x.Part == "45905-WMR").Distinct().ToList();
                var lstDrawingNo = getDrawingNoList(data.Project);
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  //Project = li.Project,
                                  Part = li.Part,
                                  DrawingNo = Manager.getDrawingNoByFindNo(li.FindNo, lstDrawingNo),
                                  Section = String.Join(" - ", getParentSectionDesc(li, lst)),
                                  ParentPart = li.ParentPart,
                                  PartLevel = li.PartLevel,
                                  Description = li.Description,
                                  ItemGroup = li.ItemGroup,
                                  RevisionNo = li.RevisionNo,
                                  FindNo = li.FindNo,
                                  ItemStatus = db.FKM102.Where(w => w.FindNo == li.FindNo && w.Project == li.Project && w.NodeKey == li.Part && (w.ProductType == ASMENUM || w.ProductType == TJFENUM)).Select(x => ((string.IsNullOrEmpty(x.KitStatus)) ? "PENDING" : (x.KitStatus == RequestENUM ? "REQUESTED" : x.KitStatus))).FirstOrDefault(),
                                  MaterialSpecification = li.MaterialSpecification,
                                  MaterialDescription = li.MaterialDescription,
                                  MaterialCode = li.MaterialCode,
                                  ProductType = li.ProductType,
                                  Length = li.Length,
                                  Width = li.Width,
                                  NoOfPieces = li.NoOfPieces,
                                  Weight = li.Weight,
                                  Thickness = li.Thickness,
                                  UOM = li.UOM
                              }).ToList();
                if ((whereCondition+"").Contains(','))
                {
                    if (!string.IsNullOrWhiteSpace(whereCondition.Split(',')[0]))
                    {
                        var part = whereCondition.Split(',')[0];
                        var SectionDesc = newlst.FirstOrDefault(f => f.Part == part).Description;
                        newlst = newlst.Where(w => w.Section == part + " - " + SectionDesc || w.Part == part).ToList();
                    }
                    if (string.IsNullOrWhiteSpace(whereCondition.Split(',')[1]))
                    {
                        var drawingNo = whereCondition.Split(',')[1];
                        //newlst = newlst.Where(w => w.DrawingNo == drawingNo).ToList();
                    }
                }

                strFileName = Helper.GenerateExcel(newlst.ToList(), objClsLoginInfo.UserName);


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        // main method for expanded grid.
        public PartialViewResult GetHBOMExpandedGridDataPartial(string project)
        {
            return PartialView("_GetHBOMExpandedGridDataPartial", project);
        }

        public string GridExpandedResult(string project)
        {
            TempData["PartList"] = db.SP_IPI_GETHBOMLIST_VW(project).ToList();
            return GetGridDetails();
        }

        private string GetGridDetails()
        {
            var hBomList = TempData["PartList"] as List<SP_IPI_GETHBOMLIST_VW_Result>;
            var partLevelRecordList = hBomList.OrderBy(x => x.PartLevel).Take(1).ToList();
            var lstDrawing = getDrawingNoList(partLevelRecordList.First().Project);

            html = new StringBuilder();
            foreach (var item in partLevelRecordList)
            {
                AddHtml(item, true, 0, string.Empty, getParentSectionDesc(item, hBomList), Manager.getDrawingNoByFindNo(item.FindNo, lstDrawing));
                var childerns = hBomList.Where(x => item.ParentPart == x.ParentPart).OrderBy(x => x.FindNo).ToList();
                foreach (var childern in childerns)
                {
                    GenerateExpandedChildPartHTML(childern, false, 1);
                }
                TempData.Keep("PartList");
            }
            return html.ToString();
        }

        private void GenerateExpandedChildPartHTML(SP_IPI_GETHBOMLIST_VW_Result item, bool isParent, int HLevel)
        {
            TempData.Keep("PartList");
            var lstDrawing = getDrawingNoList(item.Project);
            var list = TempData["PartList"] as List<SP_IPI_GETHBOMLIST_VW_Result>;
            var childerns = list.Where(x => item.Part == x.ParentPart).OrderBy(x => x.FindNo).ToList();

            //if (childerns.Count > 0)
            //    AddHtml(item, false, HLevel, item.Part);
            //else
            AddHtml(item, false, HLevel, item.Part, getParentSectionDesc(item, list), Manager.getDrawingNoByFindNo(item.FindNo, lstDrawing));

            foreach (var child in childerns)
            {
                var hasChilderns = list.Any(x => child.Part == x.ParentPart);
                if (hasChilderns)
                {
                    GenerateExpandedChildPartHTML(child, true, HLevel + 1);
                }
                else
                {
                    AddHtml(child, false, HLevel + 1, child.Part, getParentSectionDesc(child, list), Manager.getDrawingNoByFindNo(child.FindNo, lstDrawing));
                }
            }
        }
        private void AddHtml(SP_IPI_GETHBOMLIST_VW_Result item, bool isParent, int HLevel, string parentName, string[] parentSection,string drawingNo)
        {
            html.Append(isParent ? "<tr><td>" : "<tr><td style=\"padding-left: " + ((HLevel) * 15) + "px\">");
            string itemPart = parentName == string.Empty ? item.ParentPart : item.Part;
            var nodeData = db.FKM102.FirstOrDefault(w => w.FindNo == item.FindNo && w.Project == item.Project);
            if (nodeData != null)
                html.Append("<span class='borderleft" + nodeData.NodeColor + "'>&nbsp;</span>");

            var statusData = db.FKM102.Where(w => w.FindNo == item.FindNo && w.Project == item.Project && w.NodeKey == item.Part && (w.ProductType == ASMENUM || w.ProductType == TJFENUM)).Select(x => ((string.IsNullOrEmpty(x.KitStatus)) ? "PENDING" : (x.KitStatus == RequestENUM ? "REQUESTED" : x.KitStatus))).FirstOrDefault();

            html.Append(itemPart + "</td>"+
                                "<td>" + drawingNo + "</td>" +
                                "<td title=\"" + parentSection[0] + "\">" + parentSection[1] + "</td>" +
                                "<td>" + item.Description + "</td>" +
                                "<td>" + item.ItemGroup + "</td>" +
                                "<td>" + item.RevisionNo + "</td>" +
                                "<td><span title="+ item.ProductType +">"+ item.FindNo +"</span></td>"+
                                "<td>" + statusData + "</td>" +
                                "<td>" + item.MaterialSpecification + "</td>" +
                                "<td>" + item.MaterialDescription + "</td>" +
                                "<td>" + item.Length + "</td>" +
                                "<td>" + item.Width + "</td>" +
                                "<td>" + item.Thickness + "</td>" +
                                "<td>" + item.NoOfPieces + "</td>" +
                                "<td>" + item.Weight + "</td>" +
                                "<td>" + item.UOM + "</td></tr>");
            TempData.Keep("PartList");
        }

        public ActionResult GetHBOMHierarchyPartial(string Project)
        {
            int Id = 1;

            listProjectLinkDtlEnt = new List<ProjectLinkDtlEnt>();
            listProjectNodesDtlEnt = new List<ProjectNodesDtlEnt>();
            ViewBag.Project = Project;
            listProjectNodesDtlEnt.Add(new ProjectNodesDtlEnt { Description = Project, key = 0, category = "topparent" });

            var listProjectDetails = db.SP_IPI_GETHBOMLIST_VW(Project).OrderBy(x => x.PartLevel).ToList();
            foreach (var item in listProjectDetails)
            {
                listProjectNodesDtlEnt.Add(new ProjectNodesDtlEnt
                {
                    Description = item.Description,
                    FindNo = item.FindNo,
                    ItemGroup = item.ItemGroup,
                    Length = item.Length,
                    MaterialCode = item.MaterialCode,
                    MaterialDescription = item.MaterialDescription,
                    MaterialSpecification = item.MaterialSpecification,
                    NoOfPieces = item.NoOfPieces,
                    ParentPart = item.ParentPart,
                    Part = item.Part,
                    PartLevel = item.PartLevel,
                    ProductType = item.ProductType,
                    Project = item.Project,
                    RevisionNo = item.RevisionNo,
                    Thickness = item.Thickness,
                    UOM = item.UOM,
                    Weight = item.Weight,
                    Width = item.Width,
                    key = Id,
                    figurename = (item.ProductType == "ASM" || item.ProductType == "TJF") ? "Package" : "Gear",
                    category = "parts"
                });
                Id++;
            }

            var initialNodes = listProjectNodesDtlEnt.Where(i => i.PartLevel == 1).ToList();

            foreach (var item in initialNodes)
            {
                listProjectLinkDtlEnt.Add(new ProjectLinkDtlEnt { from = "0", to = item.key.ToString(), category = "parts" });
                GetChildsNodes(item);
            }

            //var seamlist = (from t in db.QMS012
            //                group t by new { t.Project, t.SeamNo }
            //                                    into grp
            //                where grp.Key.Project == Project
            //                select new
            //                {
            //                    Project = grp.Key.Project,
            //                    SeamNo = grp.Key.SeamNo,
            //                    Position = grp.Max(t => t.Position)
            //                }
            //               ).ToList();
            //foreach (var item in seamlist)
            //{
            //    listProjectNodesDtlEnt.Add(new ProjectNodesDtlEnt
            //    {
            //        seamNo=item.SeamNo,
            //        category= "seams",
            //        key = Id,
            //    });
            //    var arrayPositions = item.Position.Split(',');
            //    var listPositions = listProjectNodesDtlEnt.Where(i => arrayPositions.Contains(i.FindNo)).Select(i => i.key).ToList();
            //    foreach (var part in listPositions)
            //    {
            //        listProjectLinkDtlEnt.Add(new ProjectLinkDtlEnt { from = Id.ToString(), to = part.ToString(), category = "seams" });
            //    }
            //    Id++;
            //}

            ViewBag.listProjectLinkDtlEnt = listProjectLinkDtlEnt;
            return PartialView("_GetHBOMHierarchyPartial", listProjectNodesDtlEnt);
        }

        public void GetChildsNodes(ProjectNodesDtlEnt item)
        {
            var childerns = listProjectNodesDtlEnt.Where(x => x.ParentPart == item.Part).OrderBy(x => x.FindNo).ToList();
            foreach (var childitem in childerns)
            {
                listProjectLinkDtlEnt.Add(new ProjectLinkDtlEnt { from = item.key.ToString(), to = childitem.key.ToString(), category = "parts" });
                GetChildsNodes(childitem);
            }
        }
    }

    public class ProjectLinkDtlEnt
    {
        public string from { get; set; }
        public string to { get; set; }
        public string category { get; set; }
    }

    public class ProjectNodesDtlEnt : SP_IPI_GETHBOMLIST_VW_Result
    {
        public int key { get; set; }
        public string figurename { get; set; }
        public string category { get; set; }
        public string seamNo { get; set; }
    }
}