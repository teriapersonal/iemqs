﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class NDERequestController : clsBase
    {
        // GET: IPI/NDERequest
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetProjectResult(string term)
        {
            var lstQualityProject = (from a in db.QMS060
                                     where a.QualityProject.Contains(term) && a.HeaderId != null
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).OrderBy(x => x.qualityCode).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //public ActionResult GetRequestNoResult(string term)
        //{
        //    var lstRequestNo = (from a in db.QMS060
        //                             where a.RequestNo.Contains(term)
        //                             select new { requestNo = a.RequestNo }).Distinct().ToList();
        //    return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult GetSeamNoResult(string term, string fromQProject, string toQProject)
        {
            var lstAllQualityProject = (from a in db.QMS060
                                                 where a.HeaderId != null
                                                 select a).Select(i => new { QualityProject = i.QualityProject }).OrderBy(x => x.QualityProject.ToString()).Distinct().ToList();

            List<string> newQualityProjectList = new List<string>();
            bool start = false;

            foreach (var proj in lstAllQualityProject)
            {
                if (proj.QualityProject == fromQProject)
                {
                    start = true;
                }

                if (start)
                    newQualityProjectList.Add(proj.QualityProject);

                if (proj.QualityProject == toQProject)
                {
                    break;
                }
            }
            newQualityProjectList = newQualityProjectList.Distinct().ToList();
            var lstRequestNo = (from a in db.QMS060
                                where a.SeamNo.Contains(term) && newQualityProjectList.Contains(a.QualityProject)
                                select new { seamNo = a.SeamNo }).Distinct().OrderBy(x => x.seamNo.ToString()).Take(100).ToList();
            lstRequestNo.Insert(0, new { seamNo = "All" });
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetPartNoResult(string term)
        {
            var lstRequestNo = (from a in db.QMS065
                                where a.PartAssemblyFindNumber.Contains(term)
                                select new { partNo = a.PartAssemblyFindNumber }).Distinct().OrderBy(x => x.partNo.ToString()).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetStagecodeResult(string term)
        {
            var lstRequestNo = (from a in db.QMS060
                                where a.StageCode.Contains(term)
                                select new { stageCode = a.StageCode }).Distinct().OrderBy(x => x.stageCode.ToString()).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetShopResult(string term)
        {
            var lstRequestNo = (from a in db.QMS060
                                where a.Shop.Contains(term)
                                select new { shop = a.Shop }).Distinct().OrderBy(x => x.shop.ToString()).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetLocationResult(string term)
        {
            var lstRequestNo = (from a in db.QMS060
                                where a.Location.Contains(term)
                                select new { location = a.Location }).Distinct().OrderBy(x => x.location.ToString()).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetRequestNoResult(string term, string fromQProject, string toQProject)
        {
            var lstAllQualityProject = (from a in db.QMS060
                                        where a.HeaderId != null
                                        select a).Select(i => new { QualityProject = i.QualityProject }).OrderBy(x => x.QualityProject.ToString()).Distinct().ToList();

            List<string> newQualityProjectList = new List<string>();
            bool start = false;

            foreach (var proj in lstAllQualityProject)
            {
                if (proj.QualityProject == fromQProject)
                {
                    start = true;
                }

                if (start)
                    newQualityProjectList.Add(proj.QualityProject);

                if (proj.QualityProject == toQProject)
                {
                    break;
                }
            }
            newQualityProjectList = newQualityProjectList.Distinct().ToList();
            var lstRequestNo = (from a in db.QMS060
                                where SqlFunctions.StringConvert((double)a.RequestNo).Contains(term) && newQualityProjectList.Contains(a.QualityProject)
                                select new { requestNo = a.RequestNo }).Distinct().OrderBy(x => x.requestNo).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetStageSequenceNoResult(string term)
        {
            var lstRequestNo = (from a in db.QMS060
                                where SqlFunctions.StringConvert((double)a.StageSequence).Contains(term)
                                select new { StageSequence = a.StageSequence }).Distinct().OrderBy(x => x.StageSequence).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetIterationNoResult(string term)
        {
            var lstRequestNo = (from a in db.QMS060
                                where SqlFunctions.StringConvert((double)a.IterationNo).Contains(term)
                                select new { iterationNo =a.IterationNo }).Distinct().OrderBy(x => x.iterationNo).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetStageTypeResult(string term)
        {
            var lstRequestNo = (from a in db.QMS002
                                where a.StageType.Contains(term)
                                select new { stageType = a.StageType }).Distinct().OrderBy(x => x.stageType.ToString()).Take(100).ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InsertInspectionCertificate(string fromQualityProject, string toQualityProject, string fromSeam, string toSeam, string fromStageCode, string toStageCode, string stageType)
        {
            int MaxCertiNo = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var lstSeamDtl = db.SP_QMS_GET_SEAM_FOR_INSPECTION_CERTIFICATE_GENERATE(fromQualityProject, toQualityProject, fromSeam, toSeam, fromStageCode, toStageCode, stageType).ToList();
                if (lstSeamDtl != null && lstSeamDtl.Count() > 0)
                {
                    var lstGenerateCerti = lstSeamDtl.Select(i => new { QualityProject = i.QualityProject, StageCode = i.StageCode, NDETechniqueNo = i.NDETechniqueNo, NDETechniqueRevisionNo = i.NDETechniqueRevisionNo }).Distinct().ToList();

                    foreach (var cert in lstGenerateCerti)
                    {
                        if (db.QMS101.Any(i => i.QualityProject == cert.QualityProject && i.StageCode == cert.StageCode))
                        {
                            //var lstQMS101 = db.QMS101.Where(i => i.QualityProject == cert.QualityProject && i.StageCode == cert.StageCode).OrderByDescending(i => i.CertiNo).ToList();
                            //if(lstQMS101!=null && lstQMS101.Count > 0)
                            //{
                            //    MaxCertiNo = lstQMS101.FirstOrDefault().CertiNo;
                            //}
                            MaxCertiNo = db.QMS101.Where(i => i.QualityProject == cert.QualityProject && i.StageCode == cert.StageCode).Max(i => i.CertiNo);
                        }
                        else
                        {
                            MaxCertiNo = 0;
                        }

                        var lstSeam = lstSeamDtl.Where(i => i.QualityProject == cert.QualityProject && i.StageCode == cert.StageCode && i.NDETechniqueNo == cert.NDETechniqueNo && i.NDETechniqueRevisionNo == cert.NDETechniqueRevisionNo).ToList();
                        foreach (var seam in lstSeam)
                        {
                            if (!db.QMS101.Any(i => i.QualityProject == seam.QualityProject && i.StageCode == seam.StageCode && i.TechniqueNo == seam.NDETechniqueNo && i.TechniqueRevNo == seam.NDETechniqueRevisionNo && i.Seam_Part_No == seam.SeamNo))
                            {
                                QMS101 objQMS101 = new QMS101();
                                objQMS101.CertiNo = MaxCertiNo + 1;
                                objQMS101.CreatedBy = objClsLoginInfo.UserName;
                                objQMS101.CreatedOn = DateTime.Now;
                                objQMS101.InspectionCertiNo = seam.QualityProject + "-" + seam.StageCode + "-" + (MaxCertiNo + 1).ToString();
                                objQMS101.PrintedBy = objClsLoginInfo.UserName;
                                objQMS101.PrintedOn = DateTime.Now;
                                objQMS101.QualityProject = seam.QualityProject;
                                objQMS101.Seam_Part_No = seam.SeamNo;
                                objQMS101.StageCode = seam.StageCode;
                                objQMS101.TechniqueNo = seam.NDETechniqueNo;
                                objQMS101.TechniqueRevNo = seam.NDETechniqueRevisionNo.Value;
                                db.QMS101.Add(objQMS101);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult NDEPrintReportInProcess()
        {
            return View();
        }

        public ActionResult GetNDEProjectResult(string term)
        {
            var lstQualityProject = (from a in db.LTF002
                                     where a.QualityProject.Contains(term)
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetNDESeamNoResult(string term)
        {
            var lstRequestNo = (from a in db.LTF002
                                where a.SeamNo.Contains(term)
                                select new { seamNo = a.SeamNo }).Distinct().ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
    }
}