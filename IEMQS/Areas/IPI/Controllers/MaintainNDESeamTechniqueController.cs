﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainNDESeamTechniqueController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult AscanUTHeader(int id = 0)
        {
            QMS080 objQMS080 = db.QMS080.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objQMS080 != null)
            {
                NDEModels objNDEModels = new NDEModels();
                objQMS080.Project = Manager.GetProjectAndDescription(objQMS080.Project);
                objQMS080.ScanTechnique = objNDEModels.GetCategory("Scan Technique", objQMS080.ScanTechnique, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
                objQMS080.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS080.CouplantUsed, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
                objQMS080.SimulationBlock1 = objNDEModels.GetCategory("Simulation Block", objQMS080.SimulationBlock1, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
                objQMS080.ScanningSensitivity = objNDEModels.GetCategory("Scanning Sensitivity", objQMS080.ScanningSensitivity, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
                objQMS080.BasicCalibrationBlock = objNDEModels.GetCategory("Basic Calibration Block", objQMS080.BasicCalibrationBlock, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
                objQMS080.RecordableIndication = objNDEModels.GetCategory("Recordable Indication At Reference Gain", objQMS080.RecordableIndication, objQMS080.BU, objQMS080.Location, true).CategoryDescription;

                var listSurfaceCategory = Manager.GetSubCatagories("Scanning Surface", objQMS080.BU, objQMS080.Location).Select(i => new { CatID = i.Code, CatDesc = i.Description }).ToList();
                if (objQMS080.ScanPlan != null)
                {
                    ViewBag.ScanningSurface = listSurfaceCategory.Where(i => i.CatID == objQMS080.ScanPlan).FirstOrDefault().CatDesc;
                }
                ViewBag.ListScanningSurface = listSurfaceCategory;
                objQMS080.Location = db.COM002.Where(x => x.t_dimx == objQMS080.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS080.BU = db.COM002.Where(x => x.t_dimx == objQMS080.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
                ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                objQMS080.AfterTestCalibrationrechecked = string.IsNullOrWhiteSpace(objQMS080.AfterTestCalibrationrechecked) ? objQMS080.AfterTestCalibrationrechecked = "Yes" : objQMS080.AfterTestCalibrationrechecked;
                objQMS080.AnySpecialAccessoriesUsed = string.IsNullOrWhiteSpace(objQMS080.AnySpecialAccessoriesUsed) ? objQMS080.AnySpecialAccessoriesUsed = "No" : objQMS080.AnySpecialAccessoriesUsed;
                objQMS080.AnyRecordableIndication = string.IsNullOrWhiteSpace(objQMS080.AnyRecordableIndication) ? objQMS080.AnyRecordableIndication = "No" : objQMS080.AnyRecordableIndication;
                if (ISRequestAttended(objQMS080.RefHeaderId))
                {
                    ViewBag.isDisabled = "disabled";
                }
                return View(objQMS080);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        [HttpPost]
        public ActionResult AscanUTLinesPartial(int headerId, int lineId)
        {
            QMS081 objQMS081 = db.QMS081.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefault();
            if (objQMS081 != null)
            {
                ViewBag.DesReferenceReflector = Manager.GetCategoryOnlyDescription(objQMS081.ReferenceReflector, objQMS081.BU, objQMS081.Location, "Reference Reflector");
                ViewBag.DesCableType = Manager.GetCategoryOnlyDescription(objQMS081.CableType, objQMS081.BU, objQMS081.Location, "Cable Type");
                ViewBag.DesCableLength = Manager.GetCategoryOnlyDescription(objQMS081.CableLength, objQMS081.BU, objQMS081.Location, "Cable Length");
                ViewBag.SearchUnitDesc= db.NDE017.Where(x => x.Id == objQMS081.SearchUnit).FirstOrDefault().SearchUnit;
            }
            return PartialView("_AscanUTLinesPartial", objQMS081);
        }
        public ActionResult loadAscanUTLinesDataTable(JQueryDataTableParamModel param, int id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and HeaderId = " + id;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechnicNo", "qms.NDETechnicRevisionNo" };
                    //whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstHeader = db.SP_IPI_QMS081(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {                              
                        Convert.ToString(h.ROW_NO),
                        "",
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        Convert.ToString(h.UTTechNo),
                        Convert.ToString(h.RevNo),
                        Convert.ToString(h.SearchUnit),
                        Convert.ToString(h.ReferenceReflector),
                        Convert.ToString(h.CableType),
                        Convert.ToString(h.CableLength),
                        Convert.ToString(h.ReferencegaindB),
                        Convert.ToString(h.Range),
                        Convert.ToString(h.AmpInd1),
                        Convert.ToString(h.BPInd1),
                        Convert.ToString(h.AmpInd2),
                        Convert.ToString(h.BPInd2),
                        Convert.ToString(h.AmpInd3),
                        Convert.ToString(h.BPInd3),
                        Convert.ToString(h.AmpInd4),
                        Convert.ToString(h.BPInd4),
                        Convert.ToString(h.AmpInd5),
                        Convert.ToString(h.BPInd5),
                        Convert.ToString(h.AmpInd6),
                        Convert.ToString(h.BPInd6),                       
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveAscanUTHeader(QMS080 qms080)
        {
            string testInstrument = Request.Form["testInstrument1"];
            if(!string.IsNullOrEmpty(testInstrument))
            {
                qms080.TestInstrument = testInstrument;
            }
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS080 objQMS080 = await db.QMS080.Where(x => x.HeaderId == qms080.HeaderId).FirstOrDefaultAsync();
                if (objQMS080 != null)
                {
                    objQMS080.TestInstrument = qms080.TestInstrument;
                    objQMS080.ExaminationDate = DateTime.Now;
                    objQMS080.NDTDuration = qms080.NDTDuration;
                    objQMS080.Rejectcontrol = qms080.Rejectcontrol;
                    objQMS080.Damping = qms080.Damping;
                    objQMS080.RestrictedAccessAreaInaccessibleWelds = qms080.RestrictedAccessAreaInaccessibleWelds;
                    objQMS080.TransferCorrection = qms080.TransferCorrection;
                    objQMS080.TransferCorrectiondB = qms080.TransferCorrectiondB;
                    objQMS080.Noofpoints = qms080.Noofpoints;
                    objQMS080.AveragedB = qms080.AveragedB;
                    objQMS080.AfterTestCalibrationrechecked = qms080.AfterTestCalibrationrechecked;
                    objQMS080.AnySpecialAccessoriesUsed = qms080.AnySpecialAccessoriesUsed;
                    objQMS080.AnyRecordableIndication = qms080.AnyRecordableIndication;
                    objQMS080.NDTremarks = qms080.NDTremarks;
                    objQMS080.ScanPlan = qms080.ScanPlan;
                    objQMS080.EditedBy = objClsLoginInfo.UserName;
                    objQMS080.EditedOn = DateTime.Now;
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Document updated successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> SaveAscanUTLines(QMS081 qms081)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS081 objQMS081 = await db.QMS081.Where(x => x.LineId == qms081.LineId).FirstOrDefaultAsync();
                if (objQMS081 != null)
                {
                    objQMS081.ReferencegaindB = qms081.ReferencegaindB;
                    objQMS081.ProbeSerialNo = qms081.ProbeSerialNo;
                    objQMS081.SearchUnit = qms081.SearchUnit;
                    objQMS081.ReferencegaindB = qms081.ReferencegaindB;
                    objQMS081.Range = qms081.Range;
                    objQMS081.AmpInd1 = qms081.AmpInd1;
                    objQMS081.BPInd1 = qms081.BPInd1;
                    objQMS081.AmpInd2 = qms081.AmpInd2;
                    objQMS081.BPInd2 = qms081.BPInd2;
                    objQMS081.AmpInd3 = qms081.AmpInd3;
                    objQMS081.BPInd3 = qms081.BPInd3;
                    objQMS081.AmpInd4 = qms081.AmpInd4;
                    objQMS081.BPInd4 = qms081.BPInd4;
                    objQMS081.AmpInd5 = qms081.AmpInd5;
                    objQMS081.BPInd5 = qms081.BPInd5;
                    objQMS081.AmpInd6 = qms081.AmpInd6;
                    objQMS081.BPInd6 = qms081.BPInd6;
                    await db.SaveChangesAsync();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Line update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TOFDUTHeader(int id)
        {
            QMS082 objQMS082 = db.QMS082.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objQMS082 != null)
            {
                List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
                ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                objQMS082.Project = Manager.GetProjectAndDescription(objQMS082.Project);
                NDEModels objNDEModels = new NDEModels();
                objQMS082.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS082.CouplantUsed, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
                objQMS082.CableType = objNDEModels.GetCategory("Cable Type", objQMS082.CableType, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
                objQMS082.CableLength = objNDEModels.GetCategory("Cable Length", objQMS082.CableLength, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
                objQMS082.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objQMS082.ScanningDetail, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
                objQMS082.ReferenceReflectorSize = objNDEModels.GetCategory("Reference Reflector Size", objQMS082.ReferenceReflectorSize, objQMS082.BU, objQMS082.Location, true).CategoryDescription;


                var listSurfaceCategory = Manager.GetSubCatagories("Scanning Surface", objQMS082.BU, objQMS082.Location).Select(i => new { CatID = i.Code, CatDesc = i.Description }).ToList();
                if (objQMS082.ScanningSurface != null)
                {
                    ViewBag.ScanningSurface = listSurfaceCategory.Where(i => i.CatID == objQMS082.ScanningSurface).FirstOrDefault().CatDesc;
                }
                ViewBag.ListScanningSurface = listSurfaceCategory;
                ViewBag.TestInstrument = db.NDE019.Where(x => x.Location == objQMS082.Location && x.TestInstrument == objQMS082.TestInstrument).Select(x => x.TestInstrument).FirstOrDefault();
                objQMS082.Location = db.COM002.Where(x => x.t_dimx == objQMS082.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS082.BU = db.COM002.Where(x => x.t_dimx == objQMS082.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                //ViewBag.TestInstrument = db.IMS002.Where(x => x.Location == objQMS082.Location && x.InstrumentNumber == objQMS082.TestInstrument).Select(x => x.InstrumentNumber + "-" + x.InstrumentDescription).FirstOrDefault();

                if (ISRequestAttended(objQMS082.RefHeaderId))
                {
                    ViewBag.isDisabled = "disabled";
                }
                return View(objQMS082);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        [HttpPost]
        public ActionResult GetTestInstrumentResult(string term, string location)
        {
            //List<TestInstrument> lstTestInstrument = Manager.getTestInstrumentsByLocationAutocomplete(location, term).ToList();
            List<TestInstrument> lstTestInstrument = new List<TestInstrument>();
            if (string.IsNullOrEmpty(term))
            {
                lstTestInstrument = db.NDE019.Where(i => i.Location == location).Select(i => new TestInstrument { IDescription = i.TestInstrument, INumber = i.TestInstrument, Value = i.TestInstrument }).Distinct().Take(10).ToList();
                //lstTestInstrument = db.NDE019.Where(i => i.Location == location).Select(i => new TestInstrument { Id = i.Id }).Distinct().Take(10).ToList();
            }
            else
            {
                lstTestInstrument = db.NDE019.Where(i => i.Location == location && i.TestInstrument.Contains(term)).Select(i => new TestInstrument { IDescription = i.TestInstrument, INumber = i.TestInstrument, Value = i.TestInstrument }).Distinct().ToList();
            }
            return Json(lstTestInstrument, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TOFDUTLinesPartial(int headerId, int lineId)
        {
            QMS083 objQMS083 = db.QMS083.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefault();
            return PartialView("_TOFDUTLinesPartial", objQMS083);
        }
        public ActionResult loadTOFDUTLinesDataTable(JQueryDataTableParamModel param, int? id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and HeaderId = " + id;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechnicNo", "qms.NDETechnicRevisionNo" };
                    //    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_QMS083(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.ROW_NO),
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        Convert.ToString(h.UTTechNo),
                        Convert.ToString(h.RevNo),
                        Convert.ToString(h.ScanNo),
                        Convert.ToString(h.SearchUnit),
                        Convert.ToString(h.ScanType),
                        Convert.ToString(h.PCS),
                        Convert.ToString(h.ReferencegaindB),
                        Convert.ToString(h.QualityProject),
                        Convert.ToString(h.Project),
                        Convert.ToString(h.BU),
                        Convert.ToString(h.Location),
                        Convert.ToString(h.SeamNo),
                        Convert.ToString(h.StageCode),
                        Convert.ToString(h.StageSequence),
                        Convert.ToString(h.IterationNo),
                        Convert.ToString(h.RequestNo),
                        Convert.ToString(h.RequestNoSequence),
                        Convert.ToString(h.CreatedBy),
                        Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        Convert.ToString(h.EditedBy),
                        Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
                        ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveTOFDUTHeader(QMS082 qms082, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS082 objQMS082 = await db.QMS082.Where(x => x.HeaderId == qms082.HeaderId).FirstOrDefaultAsync();
                if (objQMS082 != null)
                {
                    objQMS082.TestInstrument = qms082.TestInstrument;
                    objQMS082.ExaminationDate = DateTime.Now;
                    objQMS082.NDTDuration = qms082.NDTDuration;
                    objQMS082.DataSamplingSpacing = qms082.DataSamplingSpacing;
                    objQMS082.DataStorageLocation = qms082.DataStorageLocation;
                    objQMS082.ScanningStartLocation = qms082.ScanningStartLocation;
                    objQMS082.EncoderCalibration = qms082.EncoderCalibration;
                    objQMS082.Software = qms082.Software;
                    objQMS082.Version = qms082.Version;
                    objQMS082.ScanningSurface = qms082.ScanningSurface;// Convert.ToString(fc["Damping"]);
                    objQMS082.FinalDisplayProcessingLevels = qms082.FinalDisplayProcessingLevels;
                    objQMS082.CompAscanUT = qms082.CompAscanUT;
                    objQMS082.AfterTestCalibrationrechecked = qms082.AfterTestCalibrationrechecked;
                    objQMS082.AnySpecialAccessoriesUsed = qms082.AnySpecialAccessoriesUsed;
                    objQMS082.AnyRecordableIndication = qms082.AnyRecordableIndication;
                    objQMS082.NDTremarks = qms082.NDTremarks;
                    objQMS082.EditedBy = objClsLoginInfo.UserName;
                    objQMS082.EditedOn = DateTime.Now;
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Document updated successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> SaveTOFDUTLines(QMS083 qms083)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS083 objQMS083 = await db.QMS083.Where(x => x.LineId == qms083.LineId).FirstOrDefaultAsync();
                if (objQMS083 != null)
                {
                    objQMS083.ReferencegaindB = qms083.ReferencegaindB;
                    objQMS083.SearchUnit = qms083.SearchUnit;
                    objQMS083.SearchUnitSerialNo = qms083.SearchUnitSerialNo;
                    objQMS083.EditedBy = objClsLoginInfo.UserName;
                    objQMS083.EditedOn = DateTime.Now;
                    await db.SaveChangesAsync();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Line update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PAUTHeader(int id)
        {
            QMS084 objQMS084 = db.QMS084.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objQMS084 != null)
            {
                objQMS084.Project = Manager.GetProjectAndDescription(objQMS084.Project);
                NDEModels objNDEModels = new NDEModels();

                objQMS084.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS084.CouplantUsed, objQMS084.BU, objQMS084.Location, true).CategoryDescription;
                objQMS084.ReferenceReflectorSize = objNDEModels.GetCategory("Reflector Size", objQMS084.ReferenceReflectorSize, objQMS084.BU, objQMS084.Location, true).CategoryDescription;
                objQMS084.CableType = objNDEModels.GetCategory("Cable Type", objQMS084.CableType, objQMS084.BU, objQMS084.Location, true).CategoryDescription;
                objQMS084.CableLength = objNDEModels.GetCategory("Cable Length", objQMS084.CableLength, objQMS084.BU, objQMS084.Location, true).CategoryDescription;

                objQMS084.Location = db.COM002.Where(x => x.t_dimx == objQMS084.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS084.BU = db.COM002.Where(x => x.t_dimx == objQMS084.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
                ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                if (ISRequestAttended(objQMS084.RefHeaderId))
                {
                    ViewBag.isDisabled = "disabled";
                }
                return View(objQMS084);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }
        //public ActionResult PAUTLinesIndex(int id)
        //{
        //    QMS084 objQMS084 = db.QMS084.Where(x => x.HeaderId == id).FirstOrDefault();
        //    return View(objQMS084);
        //}
        public ActionResult loadPAUTLinesDataTable(JQueryDataTableParamModel param, int? id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += " and HeaderId = " + id;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechnicNo", "qms.NDETechnicRevisionNo" };
                    //    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_QMS085(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.ROW_NO),
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        //Convert.ToString(h.UTTechNo),
                        //Convert.ToString(h.RevNo),
                        Convert.ToString(h.ZoneNo),
                        Convert.ToString(h.Probe),
                        Convert.ToString(h.Wedge),
                        Convert.ToString(h.NearRefractedArea),
                        Convert.ToString(h.Skip),
                        Convert.ToString(h.ScanType),
                        Convert.ToString(h.ScannedSpots),
                        Convert.ToString(h.ScannedLength),
                        Convert.ToString(h.ScanningSurface),
                        Convert.ToString(h.ReferencegaindB),
                        Convert.ToString(h.TCGIndicationAmplitude),
                        //Convert.ToString(h.QualityProject),
                        //Convert.ToString(h.Project),
                        //Convert.ToString(h.BU),
                        //Convert.ToString(h.Location),
                        //Convert.ToString(h.SeamNo),
                        //Convert.ToString(h.StageCode),
                        //Convert.ToString(h.StageSequence),
                        //Convert.ToString(h.IterationNo),
                        //Convert.ToString(h.RequestNo),
                        //Convert.ToString(h.RequestNoSequence),
                        //Convert.ToString(h.CreatedBy),
                        //Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        //Convert.ToString(h.EditedBy),
                        //Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
                        ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PAUTLinesView(int? lineId, int? headerId)
        {
            QMS085 objQMS085 = db.QMS085.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_PAUTLinesView", objQMS085);
        }
        public ActionResult PAUTSubLinesView(int sublineId, int lineId, int headerId)
        {
            QMS086 objQMS086 = db.QMS086.Where(x => x.SubLineId == sublineId && x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_PAUTSubLinesView", objQMS086);
        }

        public ActionResult loadPAUTSubLinesDataTable(JQueryDataTableParamModel param, int? id, int? lineId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and HeaderId = " + id + " and LineId = " + lineId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.GroupNo",
                        "qms.Angle",
                        "qms.DepthCoverage",
                        "qms.FocalDepth",
                        "qms.TCGIndicationAmplitude",
                        "qms.StartElement",
                        "qms.NumberofElements",
                        "qms.AngularIncrementalChange",
                        "qms.FocalPlane"};
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_QMS086(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.GroupNo),
                        Convert.ToString(h.Angle),
                        Convert.ToString(h.DepthCoverage),
                        Convert.ToString(h.FocalDepth),
                        Convert.ToString(h.TCGIndicationAmplitude),
                        Convert.ToString(h.StartElement),
                        Convert.ToString(h.NumberofElements),
                        Convert.ToString(h.AngularIncrementalChange),
                        Convert.ToString(h.FocalPlane),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> SavePAUTHeader(QMS084 qms084, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS084 objQMS084 = await db.QMS084.Where(x => x.HeaderId == qms084.HeaderId).FirstOrDefaultAsync();
                if (objQMS084 != null)
                {
                    objQMS084.TestInstrument = qms084.TestInstrument;
                    objQMS084.ExaminationDate = DateTime.Now;
                    objQMS084.NDTDuration = qms084.NDTDuration;
                    objQMS084.DataSamplingSpacing = qms084.DataSamplingSpacing;
                    objQMS084.DataStorageLocation = qms084.DataStorageLocation;
                    objQMS084.ScanningStartLocation = qms084.ScanningStartLocation;
                    objQMS084.EncoderCalibration = qms084.EncoderCalibration;
                    objQMS084.Software = qms084.Software;
                    objQMS084.Version = qms084.Version;
                    objQMS084.ScanningSurface = Convert.ToString(fc["Damping"]);
                    objQMS084.FinalDisplayProcessingLevels = qms084.FinalDisplayProcessingLevels;
                    objQMS084.CompAscanUT = qms084.CompAscanUT;
                    objQMS084.AfterTestCalibrationrechecked = qms084.AfterTestCalibrationrechecked;
                    objQMS084.AnySpecialAccessoriesUsed = qms084.AnySpecialAccessoriesUsed;
                    objQMS084.AnyRecordableIndication = qms084.AnyRecordableIndication;
                    objQMS084.NDTremarks = qms084.NDTremarks;
                    objQMS084.EditedBy = objClsLoginInfo.UserName;
                    objQMS084.EditedOn = DateTime.Now;

                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Document updated successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePAUTLine(QMS085 qms085)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS085 objQMS085 = db.QMS085.Where(x => x.LineId == qms085.LineId).FirstOrDefault();
                if (objQMS085 != null)
                {
                    objQMS085.ScannedSpots = qms085.ScannedSpots;
                    objQMS085.ScannedLength = qms085.ScannedLength;
                    objQMS085.ScanningSurface = qms085.ScanningSurface;
                    objQMS085.ReferencegaindB = qms085.ReferencegaindB;
                    objQMS085.TCGIndicationAmplitude = qms085.TCGIndicationAmplitude;
                    objQMS085.ProbeSrNo = qms085.ProbeSrNo;
                    objQMS085.EditedBy = objClsLoginInfo.UserName;
                    objQMS085.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Line update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> SavePAUTSubLine(QMS086 qms086)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS086 objQMS086 = await db.QMS086.Where(x => x.SubLineId == qms086.SubLineId).FirstOrDefaultAsync();
                if (objQMS086 != null)
                {
                    objQMS086.GroupNo = qms086.GroupNo;
                    objQMS086.Angle = qms086.Angle;
                    objQMS086.DepthCoverage = qms086.DepthCoverage;
                    objQMS086.FocalDepth = qms086.FocalDepth;
                    objQMS086.TCGIndicationAmplitude = qms086.TCGIndicationAmplitude;
                    objQMS086.StartElement = qms086.StartElement;
                    objQMS086.NumberofElements = qms086.NumberofElements;
                    objQMS086.AngularIncrementalChange = qms086.AngularIncrementalChange;
                    objQMS086.FocalPlane = qms086.FocalPlane;
                    objQMS086.EditedBy = objClsLoginInfo.UserName;
                    objQMS086.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Subline update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool ISRequestAttended(int? reqId)
        {
            bool flag = false;
            string attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
            var attendedreq = db.QMS060.Where(x => x.RequestId == reqId && x.RequestStatus == attended).FirstOrDefault();
            if (attendedreq != null)
            { flag = true; }

            return flag;
        }
    }
}