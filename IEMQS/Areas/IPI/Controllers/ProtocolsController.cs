﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ProtocolsController : clsBase
    {
        // GET: IPI/Protocols
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain Protocols";
            return View();
        }

        public ActionResult LoadProtocolPartial()
        {
            ViewBag.lstProtocolType = GetSubCatagory("Protocol Type", objClsLoginInfo.Location, string.Empty);
            return PartialView("_LoadProtocolData");
        }


        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult MaintainProtocol()
        {
            ViewBag.Title = "Maintain Protocols";
            return View();
        }

        public ActionResult LoadProtocolDetailPartial()
        {
            var ProtocolType = GetSubCatagory("Protocol Type", objClsLoginInfo.Location, string.Empty);
            string otherfiles = clsImplementationEnum.ProtocolType.Other_Files.GetStringValue();
            ViewBag.lstProtocolType = ProtocolType.Where(x => x.Value.ToLower() != otherfiles.ToLower());

            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            var lstBUs = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocations = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);

            ViewBag.lstBu = (from ath1 in db.ATH001
                             join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                             where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && ath1.BU != ""
                             select new { Value = ath1.BU, Code = ath1.BU, CategoryDescription = com2.t_desc }).Distinct().ToList();
            ViewBag.lstlocation = (from ath1 in db.ATH001
                                   join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                                   where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                                   select new CategoryData { Value = ath1.Location, Code = ath1.Location, CategoryDescription = com2.t_desc }).Distinct().ToList();


            return PartialView("_LoadMaintainProtocolData");
        }

        public ActionResult GetAllLocations(string selectedBU)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData = (from ath1 in db.ATH001
                               join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                               where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && (selectedBU != "" ? selectedBU.Contains(ath1.BU) : true)
                               select new CategoryData { Value = ath1.Location, Code = ath1.Location, CategoryDescription = com2.t_desc }).Distinct().ToList();
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMaintainProtocolGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetMaintainProtocolGridDataPartial");
        }

        public ActionResult LoadProtocolsMaintainProtocolDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1 ";

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayColumn = { "pro000.StageCode", "dbo.fun_GETCATEGORYONLYDESCRIPTION(ProtocolType, pro000.Location, pro000.BU, 'Protocol Type')", "Description" };
                    whereCondition += arrayColumn.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                var lstHeader = db.SP_IPI_MAINTAIN_PROTOCOLS_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                           a.ROW_NO.ToString(),
                           a.StageDesc,
                           a.ProtocolDescription,
                           a.Description,
                           Helper.GenerateActionIcon(a.Id, "View", "View Protocol Details", "fa fa-eye", "",WebsiteURL +"/PROTOCOL/"+a.ProtocolType+"/Details/"+Convert.ToString(a.ProtocolHeaderId)+"?m=1",false)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetAllStagesType(string bu, string loc)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData = db.QMS002.Where(i => i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase)
                                               && i.Location.Equals(loc, StringComparison.OrdinalIgnoreCase)
                                               && i.IsProtocolApplicable == true)
                                                .OrderBy(o => o.SequenceNo)
                                                .Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).Distinct().ToList();
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveProtocolDetail(PRO000 pro000)
        {

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO000 objPRO000 = new PRO000();
                if (pro000.Id > 0)
                {
                    objPRO000 = db.PRO000.Where(x => x.Id == pro000.Id).FirstOrDefault();
                    //objADD001.DocumentName = add001.DocumentName;
                }
                else
                {
                    int ProtocolHeaderId = Manager.LinkProtocolDetailWithStage(pro000.ProtocolType, string.Empty);
                    if (ProtocolHeaderId > 0)
                    {
                        objPRO000.BU = pro000.BU;
                        objPRO000.Location = pro000.Location;
                        objPRO000.StageCode = pro000.StageCode;
                        objPRO000.ProtocolType = pro000.ProtocolType;
                        objPRO000.Description = pro000.Description;
                        objPRO000.ProtocolHeaderId = ProtocolHeaderId;
                        objPRO000.CreatedBy = objClsLoginInfo.UserName;
                        objPRO000.CreatedOn = DateTime.Now;
                        db.PRO000.Add(objPRO000);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id, string type)
        {
            PRO001 objPRO001 = new PRO001();
            if (!id.HasValue)
            {
                objPRO001.ProtocolNo = string.Empty;
                objPRO001.CreatedBy = objClsLoginInfo.UserName;
                objPRO001.CreatedOn = DateTime.Now;
                db.PRO001.Add(objPRO001);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO001.HeaderId;
            }
            else
            {
                ViewBag.HeaderId = id.Value;
            }
            return View(objPRO001);
        }

        [HttpPost]
        public ActionResult LoadProtocolHeaderDataPartial(string headerId)
        {
            int _headerId;
            PRO001 objPRO001 = new PRO001();
            if (int.TryParse(headerId, out _headerId))
            {
                objPRO001 = db.PRO001.FirstOrDefault(c => c.HeaderId == _headerId);
            }
            ViewBag.YesNoEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" } };
            ViewBag.YesNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.YesNoNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.RTUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "SPOT", Code = "SPOT", CategoryDescription = "SPOT" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.TOFDUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";

            return PartialView("_GetProtocol_PRO001_DataPartial", objPRO001);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRO001.ProtocolNo = Convert.ToString(fc["ProtocolNo"]);
                    objPRO001.IdentificationMarking = Convert.ToString(fc["IdentificationMarking"]);
                    objPRO001.PtcRequired = Convert.ToString(fc["PtcRequired"]);
                    objPRO001.WepUTMTPTCUSO4Clear = Convert.ToString(fc["WepUTMTPTCUSO4Clear"]);
                    objPRO001.SeamNoPunchingAtEvery900mm = Convert.ToString(fc["SeamNoPunchingAtEvery900mm"]);
                    objPRO001.ExtentOfNDTRT = Convert.ToString(fc["ExtentOfNDTRT"]);
                    objPRO001.ExtentOfNDTUT = Convert.ToString(fc["ExtentOfNDTUT"]);
                    objPRO001.ExtentOfNDTTOFD = Convert.ToString(fc["ExtentOfNDTTOFD"]);
                    objPRO001.WEPAngleis = Convert.ToString(fc["WEPAngleis"]);
                    objPRO001.WEPAngleos = Convert.ToString(fc["WEPAngleos"]);
                    objPRO001.TacksFreeFromCrack = Convert.ToString(fc["TacksFreeFromCrack"]);
                    objPRO001.RequriedRootGap = GetDecimalValue(Convert.ToString(fc["RequriedRootGap"]));
                    objPRO001.RequriedRootFace = GetDecimalValue(Convert.ToString(fc["RequriedRootFace"]));
                    objPRO001.ActMaxRootGap = GetDecimalValue(Convert.ToString(fc["ActMaxRootGap"]));
                    objPRO001.ActMaxRootFace = GetDecimalValue(Convert.ToString(fc["ActMaxRootFace"]));
                    objPRO001.ActMinRootGap = GetDecimalValue(Convert.ToString(fc["ActMinRootGap"]));
                    objPRO001.ActMinRootFace = GetDecimalValue(Convert.ToString(fc["ActMinRootFace"]));
                    objPRO001.ShellThiknessRequired = GetDecimalValue(Convert.ToString(fc["ShellThiknessRequired"]));
                    objPRO001.OffsetAllowed = GetDecimalValue(Convert.ToString(fc["OffsetAllowed"]));
                    objPRO001.RrequriedActual1 = GetDecimalValue(Convert.ToString(fc["RrequriedActual1"]));
                    objPRO001.ActMaxActual1 = GetDecimalValue(Convert.ToString(fc["ActMaxActual1"]));
                    objPRO001.ActMinActual1 = GetDecimalValue(Convert.ToString(fc["ActMinActual1"]));
                    objPRO001.RrequiredActual2 = GetDecimalValue(Convert.ToString(fc["RrequiredActual2"]));
                    objPRO001.ActMaxActual2 = GetDecimalValue(Convert.ToString(fc["ActMaxActual2"]));
                    objPRO001.ActMinActual2 = GetDecimalValue(Convert.ToString(fc["ActMinActual2"]));
                    objPRO001.RequriedActual3 = GetDecimalValue(Convert.ToString(fc["RequriedActual3"]));
                    objPRO001.ActMaxActual3 = GetDecimalValue(Convert.ToString(fc["ActMaxActual3"]));
                    objPRO001.ActMinActual3 = GetDecimalValue(Convert.ToString(fc["ActMinActual3"]));
                    objPRO001.RrequriedActual4 = GetDecimalValue(Convert.ToString(fc["RrequriedActual4"]));
                    objPRO001.ActMaxActual4 = GetDecimalValue(Convert.ToString(fc["ActMaxActual4"]));
                    objPRO001.ActMinActual4 = GetDecimalValue(Convert.ToString(fc["ActMinActual4"]));
                    objPRO001.RequredActual5 = GetDecimalValue(Convert.ToString(fc["RequredActual5"]));
                    objPRO001.ActMaxActual5 = GetDecimalValue(Convert.ToString(fc["ActMaxActual5"]));
                    objPRO001.ActMinActual5 = GetDecimalValue(Convert.ToString(fc["ActMinActual5"]));
                    objPRO001.TotalShellHeightRequried = GetDecimalValue(Convert.ToString(fc["TotalShellHeightRequried"]));
                    objPRO001.ReqIdCFTop = Convert.ToString(fc["ReqIdCFTop"]);
                    objPRO001.ReqIdCFMiddle = Convert.ToString(fc["ReqIdCFMiddle"]);
                    objPRO001.ReqIdCFBottom = Convert.ToString(fc["ReqIdCFBottom"]);
                    objPRO001.ActReqIdCFTop = Convert.ToString(fc["ActReqIdCFTop"]);
                    objPRO001.ActReqIdCFMiddle = Convert.ToString(fc["ActReqIdCFMiddle"]);
                    objPRO001.ActReqIdCFBottom = Convert.ToString(fc["ActReqIdCFBottom"]);
                    objPRO001.ReqODCFTop = Convert.ToString(fc["ReqODCFTop"]);
                    objPRO001.ReqODCFTopMiddle = Convert.ToString(fc["ReqODCFTopMiddle"]);
                    objPRO001.ReqODCFTopBottom = Convert.ToString(fc["ReqODCFTopBottom"]);
                    objPRO001.ActReqODCFTop = Convert.ToString(fc["ActReqODCFTop"]);
                    objPRO001.ActReqODCFTopMiddle = Convert.ToString(fc["ActReqODCFTopMiddle"]);
                    objPRO001.ActReqODCFTopBottom = Convert.ToString(fc["ActReqODCFTopBottom"]);
                    objPRO001.ActualAt0 = GetDecimalValue(Convert.ToString(fc["ActualAt0"]));
                    objPRO001.ActualAt90 = GetDecimalValue(Convert.ToString(fc["ActualAt90"]));
                    objPRO001.ActualAt180 = GetDecimalValue(Convert.ToString(fc["ActualAt180"]));
                    objPRO001.ActualAt270 = GetDecimalValue(Convert.ToString(fc["ActualAt270"]));
                    objPRO001.SeamNo1 = Convert.ToString(fc["SeamNo1"]);
                    objPRO001.SeamNo1Required = Convert.ToString(fc["SeamNo1Required"]);
                    objPRO001.SeamNo1Actual = Convert.ToString(fc["SeamNo1Actual"]);
                    objPRO001.SeamNo1Remark = Convert.ToString(fc["SeamNo1Remark"]);
                    objPRO001.SeamNo2 = Convert.ToString(fc["SeamNo2"]);
                    objPRO001.SeamNo2Required = Convert.ToString(fc["SeamNo2Required"]);
                    objPRO001.SeamNo2Actual = Convert.ToString(fc["SeamNo2Actual"]);
                    objPRO001.SeamNo2Remark = Convert.ToString(fc["SeamNo2Remark"]);
                    objPRO001.SeamNo3 = Convert.ToString(fc["SeamNo3"]);
                    objPRO001.SeamNo3Required = Convert.ToString(fc["SeamNo3Required"]);
                    objPRO001.SeamNo3Actual = Convert.ToString(fc["SeamNo3Actual"]);
                    objPRO001.SeamNo3Remark = Convert.ToString(fc["SeamNo3Remark"]);
                    objPRO001.NoteLine1 = Convert.ToString(fc["NoteLine1"]);
                    objPRO001.NoteLine2_1 = Convert.ToString(fc["NoteLine2_1"]);
                    objPRO001.NoteLine2_2 = GetDatetimeValue(Convert.ToString(fc["NoteLine2_2"]));
                    objPRO001.NoteLine3 = Convert.ToString(fc["NoteLine3"]);
                    objPRO001.NoteLine4 = Convert.ToString(fc["NoteLine4"]);
                    objPRO001.NoteLine5 = Convert.ToString(fc["NoteLine5"]);
                    objPRO001.NoteLine6 = Convert.ToString(fc["NoteLine6"]);
                    objPRO001.NoteLine7 = Convert.ToString(fc["NoteLine7"]);
                    objPRO001.NoteLine9 = Convert.ToString(fc["NoteLine9"]);
                    objPRO001.NoteLine10 = Convert.ToString(fc["NoteLine10"]);
                    objPRO001.NoteLine11 = Convert.ToString(fc["NoteLine11"]);
                    objPRO001.NoteLine12 = Convert.ToString(fc["NoteLine12"]);
                    objPRO001.NoteLine13 = Convert.ToString(fc["NoteLine13"]);
                    objPRO001.NoteLine14 = Convert.ToString(fc["NoteLine14"]);
                    objPRO001.ReqTopID = GetDecimalValue(Convert.ToString(fc["ReqTopID"]));
                    objPRO001.ActTopID1 = GetDecimalValue(Convert.ToString(fc["ActTopID1"]));
                    objPRO001.ActTopID2 = GetDecimalValue(Convert.ToString(fc["ActTopID2"]));
                    objPRO001.ActTopID3 = GetDecimalValue(Convert.ToString(fc["ActTopID3"]));
                    objPRO001.ActTopID4 = GetDecimalValue(Convert.ToString(fc["ActTopID4"]));
                    objPRO001.ActTopID5 = GetDecimalValue(Convert.ToString(fc["ActTopID5"]));
                    objPRO001.ActTopID6 = GetDecimalValue(Convert.ToString(fc["ActTopID6"]));
                    objPRO001.ActTopID7 = GetDecimalValue(Convert.ToString(fc["ActTopID7"]));
                    objPRO001.ActTopID8 = GetDecimalValue(Convert.ToString(fc["ActTopID8"]));
                    objPRO001.ActTopID9 = GetDecimalValue(Convert.ToString(fc["ActTopID9"]));
                    objPRO001.ReqBotID = GetDecimalValue(Convert.ToString(fc["ReqBotID"]));
                    objPRO001.ActBottomID1 = GetDecimalValue(Convert.ToString(fc["ActBottomID1"]));
                    objPRO001.ActBottomID2 = GetDecimalValue(Convert.ToString(fc["ActBottomID2"]));
                    objPRO001.ActBottomID3 = GetDecimalValue(Convert.ToString(fc["ActBottomID3"]));
                    objPRO001.ActBottomID4 = GetDecimalValue(Convert.ToString(fc["ActBottomID4"]));
                    objPRO001.ActBottomID5 = GetDecimalValue(Convert.ToString(fc["ActBottomID5"]));
                    objPRO001.ActBottomID6 = GetDecimalValue(Convert.ToString(fc["ActBottomID6"]));
                    objPRO001.ActBottomID7 = GetDecimalValue(Convert.ToString(fc["ActBottomID7"]));
                    objPRO001.ActBottomID8 = GetDecimalValue(Convert.ToString(fc["ActBottomID8"]));
                    objPRO001.ActBottomID9 = GetDecimalValue(Convert.ToString(fc["ActBottomID9"]));
                    objPRO001.Remarks = Convert.ToString(fc["Remarks"]);
                    objPRO001.ProfileMeasurementTol = GetDecimalValue(Convert.ToString(fc["ProfileMeasurementTol"]));
                    objPRO001.ProfileMeasurementSeamNo1 = Convert.ToString(fc["ProfileMeasurementSeamNo1"]);
                    objPRO001.ProfileMeasurementSeamNo2 = Convert.ToString(fc["ProfileMeasurementSeamNo2"]);
                    objPRO001.ProfileMeasurementSeamNo3 = Convert.ToString(fc["ProfileMeasurementSeamNo3"]);
                    objPRO001.TopPeakIn1 = Convert.ToString(fc["TopPeakIn1"]);
                    objPRO001.TopPeakIn2 = Convert.ToString(fc["TopPeakIn2"]);
                    objPRO001.TopPeakIn3 = Convert.ToString(fc["TopPeakIn3"]);
                    objPRO001.TopPeakOut1 = Convert.ToString(fc["TopPeakOut1"]);
                    objPRO001.TopPeakOut2 = Convert.ToString(fc["TopPeakOut2"]);
                    objPRO001.TopPeakOut3 = Convert.ToString(fc["TopPeakOut3"]);
                    objPRO001.MidPeakIn1 = Convert.ToString(fc["MidPeakIn1"]);
                    objPRO001.MidPeakIn2 = Convert.ToString(fc["MidPeakIn2"]);
                    objPRO001.MidPeakIn3 = Convert.ToString(fc["MidPeakIn3"]);
                    objPRO001.MidPeakOut1 = Convert.ToString(fc["MidPeakOut1"]);
                    objPRO001.MidPeakOut2 = Convert.ToString(fc["MidPeakOut2"]);
                    objPRO001.MidPeakOut3 = Convert.ToString(fc["MidPeakOut3"]);
                    objPRO001.BottomPeakIn1 = Convert.ToString(fc["BottomPeakIn1"]);
                    objPRO001.BottomPeakIn2 = Convert.ToString(fc["BottomPeakIn2"]);
                    objPRO001.BottomPeakIn3 = Convert.ToString(fc["BottomPeakIn3"]);
                    objPRO001.BottomPeakOut1 = Convert.ToString(fc["BottomPeakOut1"]);
                    objPRO001.BottomPeakOut2 = Convert.ToString(fc["BottomPeakOut2"]);
                    objPRO001.BottomPeakOut3 = Convert.ToString(fc["BottomPeakOut3"]);
                    objPRO001.ProfileMeasurementRemarks = Convert.ToString(fc["ProfileMeasurementRemarks"]);
                    objPRO001.ChordLengthReq = Convert.ToString(fc["ChordLengthReq"]);
                    objPRO001.ChordLengthAct = Convert.ToString(fc["ChordLengthAct"]);
                    objPRO001.RadiusReq = Convert.ToString(fc["RadiusReq"]);
                    objPRO001.RadiusAct = Convert.ToString(fc["RadiusAct"]);
                    objPRO001.GapAllowable1 = Convert.ToString(fc["GapAllowable1"]);
                    objPRO001.GapAllowable2 = Convert.ToString(fc["GapAllowable2"]);
                    objPRO001.GapAct = Convert.ToString(fc["GapAct"]);
                    objPRO001.EValueTemplateInspectionRemarks = Convert.ToString(fc["EValueTemplateInspectionRemarks"]);
                    objPRO001.InspectionRemark = Convert.ToString(fc["InspectionRemark"]);
                    objPRO001.WidthRequiredMax = GetDecimalValue(Convert.ToString(fc["WidthRequiredMax"]));
                    objPRO001.WidthRequiredMin = GetDecimalValue(Convert.ToString(fc["WidthRequiredMin"]));
                    objPRO001.DepthRequriedMax = GetDecimalValue(Convert.ToString(fc["DepthRequriedMax"]));
                    objPRO001.DepthRequriedMin = GetDecimalValue(Convert.ToString(fc["DepthRequriedMin"]));
                    objPRO001.Note1 = Convert.ToString(fc["Note1"]);
                    objPRO001.Note2 = Convert.ToString(fc["Note2"]);
                    objPRO001.Note3 = Convert.ToString(fc["Note3"]);
                    objPRO001.CreatedBy = objClsLoginInfo.UserName;
                    objPRO001.CreatedOn = DateTime.Now;
                    objPRO001.EditedBy = objClsLoginInfo.UserName;
                    objPRO001.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRO001.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO001 != null && string.IsNullOrWhiteSpace(objPRO001.ProtocolNo))
                    {
                        db.PRO002.RemoveRange(objPRO001.PRO002.ToList());
                        db.PRO003.RemoveRange(objPRO001.PRO003.ToList());
                        db.PRO001.Remove(objPRO001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO001.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro002.SpotNo like '%" + param.sSearch
                        + "%' or pro002.Remarks like '%" + param.sSearch
                        + "%' or pro002.SpotWidth =" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotWidth", "", "", false, "", false, "10"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1();")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.SpotNo),
                               Convert.ToString(uc.SpotWidth),
                               Convert.ToString(uc.Remarks),
                               HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLine1WithConfirmation("+ uc.LineId +","+ uc.HeaderId +")")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO002 objPRO002 = new PRO002();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + newRowIndex]) ? Convert.ToString(fc["SpotNo" + newRowIndex]).Trim() : "";
                    decimal? strSpotWidth = GetDecimalValue(Convert.ToString(fc["SpotWidth" + newRowIndex]));//  !string.IsNullOrEmpty(fc["SpotWidth" + newRowIndex]) ? Convert.ToDecimal(fc["SpotWidth" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : string.Empty;


                    #region Add New Spot Line1
                    objPRO002.HeaderId = objPRO001.HeaderId;
                    objPRO002.SpotNo = intSpotNo;
                    objPRO002.SpotWidth = strSpotWidth;
                    objPRO002.Remarks = strRemarks;
                    objPRO002.CreatedBy = objClsLoginInfo.UserName;
                    objPRO002.CreatedOn = DateTime.Now;
                    db.PRO002.Add(objPRO002);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Spot added successfully";
                    objResponseMsg.HeaderId = objPRO002.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO002 objPRO002 = db.PRO002.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO002 != null)
                {
                    db.PRO002.Remove(objPRO002);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro003.SpotNo like '%" + param.sSearch
                        + "%' or pro003.Remarks like '%" + param.sSearch
                        + "%' or pro003.SpotDepth =" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotDepth2", "", "", false, "", false, "10"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2();")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.SpotNo),
                               Convert.ToString(uc.SpotDepth),
                               Convert.ToString(uc.Remarks),
                               HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLine2WithConfirmation("+ uc.LineId +","+ uc.HeaderId +")")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO003 objPRO003 = new PRO003();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + newRowIndex]) ? Convert.ToString(fc["SpotNo2" + newRowIndex]).Trim() : "";
                    decimal? strSpotDepth = GetDecimalValue(Convert.ToString(fc["SpotDepth2" + newRowIndex])); // !string.IsNullOrEmpty(fc["SpotDepth2" + newRowIndex]) ? Convert.ToDecimal(fc["SpotDepth2" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks2" + newRowIndex]) ? Convert.ToString(fc["Remarks2" + newRowIndex]).Trim() : string.Empty;


                    #region Add New Spot Line2
                    objPRO003.HeaderId = objPRO001.HeaderId;
                    objPRO003.SpotNo = intSpotNo;
                    objPRO003.SpotDepth = strSpotDepth;
                    objPRO003.Remarks = strRemarks;
                    objPRO003.CreatedBy = objClsLoginInfo.UserName;
                    objPRO003.CreatedOn = DateTime.Now;
                    db.PRO003.Add(objPRO003);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Spot added successfully";
                    objResponseMsg.HeaderId = objPRO003.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO003 objPRO003 = db.PRO003.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO003 != null)
                {
                    db.PRO003.Remove(objPRO003);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Common Functions
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        public IQueryable<CategoryData> GetSubCatagory(string Key, string strLoc, string BU = "")
        {
            IQueryable<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct();
            }
            else //Get Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct();
            }
            return lstGLB002;
        }

        public int? GetIntegerValue(string value)
        {
            int outValue;
            if (int.TryParse(value, out outValue))
            {
                return outValue;
            }
            else
            {
                return null;
            }
        }
        public decimal? GetDecimalValue(string value)
        {
            decimal outValue;
            if (decimal.TryParse(value, out outValue))
            {
                return outValue;
            }
            else
            {
                return null;
            }
        }
        public DateTime? GetDatetimeValue(string value)
        {
            DateTime outValue;
            if (DateTime.TryParse(value, out outValue))
            {
                return outValue;
            }
            else
            {
                return null;
            }
        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + WebsiteURL + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
        #endregion

        #region Index Page
        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadProtocolsIndexDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {


                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1 AND ISNULL(ProtocolNo,'') <> ''";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");

                //if (status.ToUpper() == "PENDING")
                //{

                //    whereCondition += " and ltf001.Status in('" + clsImplementationEnum.Status.Draft.GetStringValue() + "','" + clsImplementationEnum.Status.Returned.GetStringValue() + "')";
                //}

                //if (!string.IsNullOrEmpty(param.sSearch))
                //{
                //    whereCondition += " and (ltf001.QualityProject like '%" + param.sSearch + "%' or (ltf001.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(ltf001.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc))) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(ltf001.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc))) like '%" + param.sSearch + "%')";
                //}

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_PROTOCOLS_GET_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.ProtocolNo,
                        a.ProtocolType,
                        HTMLActionString(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-pencil", "", "/PROTOCOL/"+a.ProtocolCode+"/Details/" + Convert.ToString(a.HeaderId))
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

    }
}