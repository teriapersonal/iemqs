﻿using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;


namespace IEMQS.Areas.IPI.Controllers
{
    public class ProductionReportController : clsBase
    {
        // GET: IPI/ProductionReport

        #region Main Page
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();           
        }

        public ActionResult ReportGPA()
        {
            //return View();
            return View("ReportGPA");
        }

        public IQueryable<CategoryData> GetSubCatagory(string Key, string strLoc, string BU = "")
        {
            IQueryable<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Code = i.Code, Description = i.Code + " - " + i.Description }).Distinct();
            }
            else //Get Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Code = i.Code, Description = i.Code + " - " + i.Description }).Distinct();
            }
            return lstGLB002;
        }
      
        #endregion

        [HttpPost]
        public ActionResult GetSeamFromQualityProject(string term,string fromQProj, string toQProj)
        {
            try
            {
                var newSeamList = db.SP_FETCH_SEAM_BY_QUALITYPROJECT(fromQProj, toQProj).Distinct().ToList();
                List<CategoryData> lstSeamList = new List<CategoryData>();
                if (!string.IsNullOrWhiteSpace(term))
                {
                     lstSeamList =  (from lst in newSeamList
                                       where lst.SeamNo.ToLower().Contains(term.ToLower())
                                       select new CategoryData
                                       {
                                           id = lst.SeamNo,
                                           text = lst.SeamNo,
                                       }).ToList();
                }
                else
                {
                    lstSeamList = (from lst in newSeamList
                                   select new CategoryData
                                   {
                                       id = lst.SeamNo,
                                       text = lst.SeamNo,
                                   }).ToList();
                }
                return Json(lstSeamList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetUTTypes()
        {
            try
            {
                var lstUTTech = clsImplementationEnum.GetUTTypes().ToList();

                var lstUTTypes = (from lst in lstUTTech
                                  select new
                                  {
                                      id = clsImplementationEnum.convertUTTypes(lst),
                                      text = lst
                                  }).ToList();

                return Json(lstUTTypes, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

    }
}