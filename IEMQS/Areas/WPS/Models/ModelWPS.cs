﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;

namespace IEMQS.Areas.WPS.Models
{
    public class ModelWPS
    {
        public string ProjectNo { get; set; }
        public bool isWPSDocGenerated { get; set; }

        public string WeldingProcess1 { get; set; }
        public string WeldingProcess2 { get; set; }
        public string WeldingProcess3 { get; set; }
        public string WeldingProcess4 { get; set; }

        public string JointType { get; set; }
        public List<CategoryData> CategoryData { get; set; }
        public int HeaderId { get; set; }
    }

    public class ModelWPSLines
    {
        public string ANo { get; set; }
        public string SizeOfFillerMetal { get; set; }
        public string SFANo { get; set; }
        public string FNo { get; set; }
        public string Current { get; set; }
        public string TravelSpeed { get; set; }
        public string Voltage { get; set; }
        public string BeadLength { get; set; }
        public string HeatInput { get; set; }

        public List<CategoryData> FillerMetalTradeName { get; set; }
        public List<CategoryData> FluxTradeName { get; set; }

    }

    public class clsBackingEnum
    {
        public enum Backing
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
            [StringValue("Yes/No *")]
            YesNoA,
            [StringValue("Yes/No")]
            YesNo,
        }

        public static List<string> GetBacking()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<Backing>();
            return items;
        }
    }

    public class ModelWPSHeader
    {
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
    }


}