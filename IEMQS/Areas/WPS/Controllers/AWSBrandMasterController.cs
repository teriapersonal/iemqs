﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;

namespace IEMQS.Areas.WPS.Controllers
{
    public class AWSBrandMasterController : clsBase
    {
        // GET: WPS/AWSBrandMaster
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadAWSBrandListDataTable(JQueryDataTableParamModel param)
        {
            try
            {

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "wps1.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps1.Location", lstLoc);

                //whereCondition += " and wps1.Location IN('HZW', 'HZW')";

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and ((wps1.Location+'-'+lcom2.t_desc) like '%" + param.sSearch + "%' or wps1.Producttype like '%" + param.sSearch + "%' or wps1.AWSClass like '%" + param.sSearch + "%' or wps1.BrandName like '%" + param.sSearch + "%' or (wps1.Supplier+'-'+com6.t_nama) like '%" + param.sSearch + "%' or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GetAWSBrandList(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {

                        Convert.ToString(a.Id),
                        a.Producttype,
                        a.AWSClass,
                        a.BrandName,
                        a.Supplier,
                        a.CreatedBy,
                        Convert.ToDateTime(a.CreatedOn).ToString("dd/MM/yyyy"),
                        a.Location,
                        a.InUsed.ToString().ToLower()
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AWSBrandMasterPartial(int? id)
        {
            string loc = string.Empty;
            WPS001 objWPS001 = new WPS001();
            if (id > 0)
            {
                objWPS001 = db.WPS001.Where(i => i.Id == id).FirstOrDefault();
                loc = (from a in db.COM002
                       where a.t_dtyp == 1 && a.t_dimx.Equals(objWPS001.Location, StringComparison.OrdinalIgnoreCase)
                       select a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                loc = (from a in db.COM003
                       join b in db.COM002 on a.t_loca equals b.t_dimx
                       where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                       select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            }
            ViewBag.Location = loc;

            var lstManufacturingCode = GetSubCatagory("Product Type", loc.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            ViewBag.ProductType = lstManufacturingCode;
            //var lstSupplier = db.COM006.Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).Take(15).ToList();
            //var lstSupplier = db.COM006.Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).ToList();
            //ViewBag.Supplier = lstSupplier;

            if (id > 0)
            {

                //ViewBag.SupplierDefault = lstSupplier.FirstOrDefault(i => i.Value == objWPS001.Supplier).CategoryDescription;
                if (!string.IsNullOrWhiteSpace(objWPS001.Supplier) && objWPS001.Supplier.Trim().ToUpper() != "ANY")
                {
                    ViewBag.SupplierDefault = (from a in db.COM006
                                               where a.t_bpid.Equals(objWPS001.Supplier, StringComparison.OrdinalIgnoreCase)
                                               select a.t_bpid + "-" + a.t_nama).FirstOrDefault();
                }
                else
                {
                    ViewBag.SupplierDefault = objWPS001.Supplier;
                }
                ViewBag.ManufacturingCodeDefault = lstManufacturingCode.FirstOrDefault(i => i.Value == objWPS001.Producttype).CategoryDescription;
            }

            return PartialView("_AWSBrandMasterPartial", objWPS001);
        }

        [HttpPost]
        public ActionResult Save(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS001 objWPS001 = new WPS001();
            try
            {
                if (fc != null)
                {
                    int id = Convert.ToInt32(fc["Id"]);
                    string location = fc["Location"].Split('-')[0];
                    if (id > 0)
                    {
                        var objInUsed = db.SP_WPS_GetAWSBrandList(0, 0, "", "wps1.Id=" + id).FirstOrDefault();
                        if (objInUsed.InUsed.HasValue && objInUsed.InUsed.Value)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "You Can Not Modify This AWS Brand Because Already Used In Filler Metal Master.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (!isExistAWSBrand(location, fc["Producttype"], fc["AWSClass"], fc["BrandName"], fc["Supplier"], id))
                    {
                        if (id > 0)
                        {
                            objWPS001 = db.WPS001.Where(i => i.Id == id).FirstOrDefault();
                            objWPS001.Location = fc["Location"].Split('-')[0];
                            objWPS001.Producttype = fc["Producttype"];
                            objWPS001.AWSClass = fc["AWSClass"];
                            objWPS001.BrandName = fc["BrandName"];
                            objWPS001.Supplier = fc["Supplier"];
                            objWPS001.EditedBy = objClsLoginInfo.UserName;
                            objWPS001.EditedOn = DateTime.Now;

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.AWSBrandMaster.Update;
                        }
                        else
                        {
                            objWPS001.Location = fc["Location"].Split('-')[0];
                            objWPS001.Producttype = fc["Producttype"];
                            objWPS001.AWSClass = fc["AWSClass"];
                            objWPS001.BrandName = fc["BrandName"];
                            objWPS001.Supplier = fc["Supplier"];
                            objWPS001.CreatedBy = objClsLoginInfo.UserName;
                            objWPS001.CreatedOn = DateTime.Now;

                            db.WPS001.Add(objWPS001);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.AWSBrandMaster.Save;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "AWS Brand Already Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAWSBrand(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS001 objWPS001 = new WPS001();
            try
            {
                if (id > 0)
                {
                    var objInUsed = db.SP_WPS_GetAWSBrandList(0, 0, "", "wps1.Id=" + id).FirstOrDefault();
                    if(objInUsed.InUsed.HasValue && objInUsed.InUsed.Value)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You Can Not Delete This AWS Brand Because Already Used In Filler Metal Master.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objWPS001 = db.WPS001.Where(i => i.Id == id).FirstOrDefault();
                    if (objWPS001 != null)
                    {
                        if (isExistInWPS(objWPS001.AWSClass, objWPS001.Location))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.AWSBrandMaster.RestrictDelete, objWPS001.AWSClass);
                        }
                        else
                        {
                            db.WPS001.Remove(objWPS001);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.AWSBrandMaster.Delete, objWPS001.AWSClass);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "AWS Brand Does Not Exist";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "AWS Brand Does Not Exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public List<GLB002> GetSubCatagory(string Key, string strLoc)
        {
            List<GLB002> lstGLB002 = (from glb002 in db.GLB002
                                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                                      select glb002).ToList();

            return lstGLB002;
        }

        public bool isExistAWSBrand(string location, string productType, string awsClass, string brandName, string supplier, int id)
        {
            bool isExistAWSBrand = false;
            var lstWPS1 = db.WPS001.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Producttype.Equals(productType, StringComparison.OrdinalIgnoreCase)
                                          && i.AWSClass.Equals(awsClass, StringComparison.OrdinalIgnoreCase) &&
                                          i.BrandName.Equals(brandName, StringComparison.OrdinalIgnoreCase) && i.Supplier.Equals(supplier, StringComparison.OrdinalIgnoreCase)
                                          && i.Id != id).ToList();

            if (lstWPS1.Any())
                isExistAWSBrand = true;

            return isExistAWSBrand;
        }

        public bool isExistInWPS(string aws, string Location)
        {
            bool isExistInWPS = true;

            var objWPS011 = db.WPS011.Where(i => i.AWSClass == aws && i.Location == Location).FirstOrDefault();
            if (objWPS011 == null)
            {
                isExistInWPS = false;
            }

            return isExistInWPS;
        }
        public ActionResult GetSupplier(string term)
        {
            List<CategoryData> supplier = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                supplier = db.COM006.Where(i => i.t_bpid.ToLower().Contains(term.ToLower()) || i.t_nama.ToLower().Contains(term.ToLower())).Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).Distinct().ToList();
            }
            else
            {
                supplier = db.COM006.Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).Take(15).ToList();
            }
            supplier.Add(new CategoryData { Value = "ANY", CategoryDescription = "ANY" });
            return Json(supplier, JsonRequestBehavior.AllowGet);
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                List<SP_WPS_GetAWSBrandList_Result> lst = db.SP_WPS_GetAWSBrandList(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Producttype = li.Producttype,
                                  AWSClass = li.AWSClass,
                                  BrandName = li.BrandName,
                                  Supplier = li.Supplier,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  Location = li.Location
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Json 
        public ActionResult getBrandNameForAutoComplete(string Loc, string term)
        {
            var lstData = db.WPS001.Where(x => x.Location == Loc && x.BrandName.Contains(term))
                .Select(s => new AutoCompleteList { id = s.BrandName, value = s.BrandName, label = s.BrandName }).Distinct().Take(10).ToList();
            return Json(lstData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}