﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Excel;
using IEMQS.Areas.DES.Models;
using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQS.DESServices;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class JEPController : Controller
    {
        IJEPService _JEPService;
        ICommonService _CommonService;
        IProductService _ProductService;
        IGlobalJEPTemplateService _GlobalJEPTemplateService;
        IProjectService _ProjectService;
        public JEPController(IJEPService JEPService, ICommonService CommonService, IProductService ProductService, IGlobalJEPTemplateService GlobalJEPTemplateService, IProjectService ProjectService)
        {
            _JEPService = JEPService;
            _CommonService = CommonService;
            _ProductService = ProductService;
            _GlobalJEPTemplateService = GlobalJEPTemplateService;
            _ProjectService = ProjectService;
        }

        // GET: DES/JEP
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _JEPService.GetJEPListByProject(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, SessionMgtModel.ProjectFolderList.ProjectNo, SessionMgtModel.ProjectFolderList.FolderId, Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public ActionResult Add()
        {
            var temp = _ProjectService.GetJEPProjectByProjectNo(SessionMgtModel.ProjectFolderList.ProjectNo);
            if (temp != null && !string.IsNullOrEmpty(temp.DrawingSeriesNo))
            {
                if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
                {
                    JEPModel model = new JEPModel();
                    model.Licensor = temp.Licensor;
                    model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
                    model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                    model = _JEPService.SetDefaultAddData(model);
                    model.Department = CommonService.objClsLoginInfo.Department;
                    model.ProjectManager = temp.ProjectPMG;
                    model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                    ViewBag.ProductList = new SelectList(_JEPService.GetAllProductByBUId((Int64)SessionMgtModel.ProjectFolderList.BUId), "ProductId", "Product");
                    ProjectModel getDataOfProject = _JEPService.GetProjectDetails(SessionMgtModel.ProjectFolderList.ProjectNo);
                    ViewBag.Purchaser = getDataOfProject.ProductEndUser;
                    ViewBag.ProjectWiseDocument = new SelectList(_JEPService.GetJEPTemplatebyProject(SessionMgtModel.ProjectFolderList.ProjectNo), "JEPTemplateId", "TemplateName");
                    ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
                    ViewBag.GetDocumentList = JsonConvert.SerializeObject(_CommonService.GetLookupData(LookupType.DOCType.ToString()));
                    // ViewBag.GetDocumentList = new SelectList(_CommonService.GetDocumentTypes(LookupType.DOCType.ToString()));  //Get DocumentList 
                    //ViewBag.Role = new SelectList(new clsManager().getBUsByID(3));
                    //ViewBag.GetDocumentList = new SelectList(_CommonService.GetDocumentTypes(LookupType.DOCType.ToString()));
                    ViewBag.GetLifeCycle = _JEPService.GetPolicySteps(ObjectName.JEP.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, model.JEPId);
                    ViewBag.GetContractProjectList = new SelectList(_JEPService.GetContractProjectList(model.ContractNo), "Value", "Text");

                    return View(model);
                }
                else
                {
                    TempData["Error"] = "Select folder";
                    return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
                }
            }
            else
            {
                TempData["Info"] = "Drawing Series Number is not exist";
                return RedirectToAction("JEPProject", "Project");
            }
        }

        [HttpPost]
        public ActionResult Add(JEPModel model)
        {
            string GetCheckInPsName;
            model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            model.BUId = SessionMgtModel.ProjectFolderList.BUId;
            bool IsChekIn = _JEPService.IsCheckInJEP(CommonService.objClsLoginInfo.UserName, model.JEPId,out GetCheckInPsName);

            var str = model.RoleGroup.Split(',');
            foreach (var item in str)
            {
                if (CommonService.objClsLoginInfo.UserRoles.Contains(item.Trim()))
                {
                    if (SessionMgtModel.ProjectFolderList.IsInprocess)
                    {
                        model.IsDEENGGUSer = true;
                        break;
                    }
                }
            }

            if (IsChekIn != true)
            {
                return Json(new { InfoMsg = true, Status = true, Msg = "JEP is already in use by "+ GetCheckInPsName }, JsonRequestBehavior.AllowGet);
            }

            Int64 status = _JEPService.AddEdit(model);
            if (status > 0)
            {
                if (model.JEPId > 0)
                    return Json(new { model, InfoMsg = false, Status = true, Msg = "JEP updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                {
                    model.JEPId = status;
                    return Json(new { model, InfoMsg = false, Status = true, Msg = "JEP added successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { Status = false, InfoMsg = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(Int64 Id)
        {
            JEPModel model = new JEPModel();

            ViewBag.ProductList = new SelectList(_JEPService.GetAllProductByBUId((Int64)SessionMgtModel.ProjectFolderList.BUId), "ProductId", "Product");
            //ViewBag.TemplateList = new SelectList(_GlobalJEPTemplateService.GetAllGlobalJEPTemplate(SessionMgtModel.ProjectFolderList.ProjectNo, (Int64)SessionMgtModel.ProjectFolderList.BUId), "JEPTemplateId", "TemplateName");

            ViewBag.ProjectWiseDocument = new SelectList(_JEPService.GetJEPTemplatebyProject(SessionMgtModel.ProjectFolderList.ProjectNo), "JEPTemplateId", "TemplateName");

            ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
            ViewBag.GetDocumentList = JsonConvert.SerializeObject(_CommonService.GetLookupData(LookupType.DOCType.ToString()));
            //ViewBag.Role = new SelectList(new clsManager().getBUsByID(3));
            //ViewBag.GetDocumentList = new SelectList(_CommonService.GetDocumentTypes(LookupType.DOCType.ToString()));
            //ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
            //ViewBag.GetDocumentList =new SelectList(_CommonService.GetDocumentTypes(LookupType.DOCType.ToString()));  //Get DocumentList 

            if (Id > 0)
            {
                model = _JEPService.GetManitainJEPbyId(Id);
                var psno = CommonService.objClsLoginInfo.UserName;
                if (model.JEPId != null)
                {
                    string GetCheckInPsName;
                    bool IsChekIn = _JEPService.IsCheckInJEP(psno, model.JEPId,out GetCheckInPsName);

                    if (IsChekIn != false)
                    {
                        model.IsJEPCheckIn = false;

                    }
                    else
                    {
                        TempData["Info"] = "JEP is already use by "+ GetCheckInPsName;
                        return RedirectToAction("Index");

                    }
                }
                bool HasPsNo = true;
                bool UpdateCheckIN = _JEPService.UpdateIsCheckInJEP(Id, HasPsNo);
                if (model == null)
                    return RedirectToAction("Add");
                ViewBag.GetLifeCycle = _JEPService.GetPolicySteps(ObjectName.JEP.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, model.JEPId);
                ViewBag.GetContractProjectList = new SelectList(_JEPService.GetContractProjectList(model.ContractNo), "Value", "Text");
                ViewBag.Purchaser = model.Purchaser;
                var str = model.RoleGroup.Split(',');
                foreach (var item in str)
                {
                    if (CommonService.objClsLoginInfo.UserRoles.Contains(item.Trim()))
                    {
                        if (SessionMgtModel.ProjectFolderList.IsInprocess)
                        {
                            model.IsDEENGGUSer = true;
                            break;
                        }
                    }
                }
            }
            else
                return RedirectToAction("Add");

            return View("Add", model);
        }

        [HttpGet]
        public ActionResult DeleteJEP(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _JEPService.DeleteMaintainJEP(Id, out errorMsg);
            if (status > 0)
                return Json(new { Status = true, Msg = "JEP template deleted successfully" }, JsonRequestBehavior.AllowGet);
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult TemplateListByProductId(Int64 ProductId)
        {
            return Json(_GlobalJEPTemplateService.GetAllGlobalJEPTemplateByProductId(ProductId, Models.SessionMgtModel.ProjectFolderList.BUId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDocumentList(DataTableParamModel parm, Int64 JEPId)
        {
            int recordsTotal = 0;
            var list = _JEPService.GetDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, JEPId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddDocument(JEPDocumentModel model)
        {
            string errorMsg = "";

            Int64 status = _JEPService.AddEditDocumentList(model, (Int64)SessionMgtModel.ProjectFolderList.BUId, SessionMgtModel.ProjectFolderList.ProjectNo, out errorMsg);
            if (status > 0)
            {
                if (model.JEPDocumentDetailsId > 0)
                    return Json(new { Status = true, Msg = "Document updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                {
                    model.JEPDocumentDetailsId = status;
                    return Json(new { Status = true, Msg = "Document added successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteDocument(Int64? Id)
        {
            string errorMsg = "";
            Int64? Status = _JEPService.DeleteDocument(Id, out errorMsg);
            if (Status > 0)
                return Json(new { Status = true, Msg = "Document deleted successfully" }, JsonRequestBehavior.AllowGet);
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddDcoumentByProductANDTemplate(Int64 Product, Int64 JEPTemplateId, Int64 JEPId)
        {
            string Msg = "";
            string project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            bool Status = _JEPService.AddDocumentListByProductANDTemplate(SessionMgtModel.ProjectFolderList.ProjectNo, Product, JEPTemplateId, JEPId, project, BUId, out Msg);
            if (Status)
                return Json(new { Status = true, Msg = Msg }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddDcoumentByProjectwise(Int64 JEPDocumentDetailsId, Int64 JEPId)
        {
            string Msg = "";
            string project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            bool Status = _JEPService.AddDocumentListByProjectwise(JEPDocumentDetailsId, JEPId, project, BUId, out Msg);
            if (Status)
                return Json(new { Status = true, Msg = Msg }, JsonRequestBehavior.AllowGet);

            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddEditDocumnet(Int64 JEPId)
        {
            string Msg = "";
            var UserName = new clsManager().GetUserNameFromPsNo(CommonService.objClsLoginInfo.UserName);
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            bool Status = _JEPService.AddEditJEPStatus(JEPId, Project, out Msg);
            if (Status)
            {
                string rurl = string.Empty;
                if (SessionMgtModel.ProjectFolderList.FolderId != null && SessionMgtModel.ProjectFolderList.FolderId > 0)
                {
                    rurl = "/DES/Project/Folder?q=" + CommonService.Encrypt("Id=" + Models.SessionMgtModel.ProjectFolderList.FolderId);
                }
                else
                {
                    rurl = "/DES/JEP";
                }
                return Json(new { Status = true, Msg = "JEP is submitted for approval", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllSearchDocumnet(DataTableParamModel parm, string Project, string DocumentNo)
        {
            int recordsTotal = 0;
            var list = _JEPService.GetAllGlobalDocumentListByProject(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project, DocumentNo, SessionMgtModel.ProjectFolderList.ProjectNo);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult BindpreviewProductJEPDoc(DataTableParamModel parm, Int64 JEPTemplateId)
        {
            int recordsTotal = 0;
            var list = _JEPService.BindpreviewProductJEPDoc(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, JEPTemplateId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddJEPLinkDocuments(JEPGlobalDoc Model)
        {
            string Msg = "";
            string SuccessMsg = "";
            bool Status = _JEPService.AddJEPLinkDocuments(Model, SessionMgtModel.ProjectFolderList.ProjectNo, out Msg, out SuccessMsg);
            if (Status)
                return Json(new { Status = true, Msg = Msg, SuccessMsg = SuccessMsg }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddLinkDocument(Int64 DocumentId, Int64 JEPId)
        {
            string Msg = "";
            bool Status = _JEPService.AddLinkDocument(DocumentId, JEPId, out Msg);
            if (Status)
                return Json(new { Status = true, Msg = "Added successfully" }, JsonRequestBehavior.AllowGet);
            else
            {
                if (!string.IsNullOrEmpty(Msg))
                    return Json(new { Status = false, Msg = Msg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(Int64? JEPId)
        {

            JEPModel model = new JEPModel();

            var psno = CommonService.objClsLoginInfo.UserName;
            model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            model = _JEPService.GetJEPdetail(JEPId);

            var str = model.RoleGroup.Split(',');
            foreach (var item in str)
            {
                if (CommonService.objClsLoginInfo.UserRoles.Contains(item.Trim()))
                {
                    if (SessionMgtModel.ProjectFolderList.IsInprocess)
                    {
                        model.IsDEENGGUSer = true;
                        _JEPService.UpdateIsCheckInJEP(model.JEPId, true);
                        break;
                    }
                }
            }

            if(model.IsDEENGGUSer)
            {
                string GetCheckInPsName;
                bool IsChekIn = _JEPService.IsCheckInJEP(psno, JEPId,out GetCheckInPsName);
                if (IsChekIn != false)
                {
                    model.IsJEPCheckIn = false;
                }
                else
                {
                    TempData["Info"] = "JEP is already use by "+ GetCheckInPsName;
                    return RedirectToAction("Index");
                }
            }

            
            model.Department = CommonService.objClsLoginInfo.Department;
            //model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
            model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            ViewBag.ProductList = new SelectList(_JEPService.GetAllProductByBUId((Int64)SessionMgtModel.ProjectFolderList.BUId), "ProductId", "Product");
            //ViewBag.TemplateList = new SelectList(_GlobalJEPTemplateService.GetAllGlobalJEPTemplate( (Int64)SessionMgtModel.ProjectFolderList.BUId), "JEPTemplateId", "TemplateName");

            ViewBag.ProjectWiseDocument = new SelectList(_JEPService.GetJEPTemplatebyProject(SessionMgtModel.ProjectFolderList.ProjectNo), "JEPTemplateId", "TemplateName");

            ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
            ViewBag.GetDocumentList = JsonConvert.SerializeObject(_CommonService.GetLookupData(LookupType.DOCType.ToString()));

            //ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            ViewBag.GetPolicySteps = _JEPService.GetJEPUserLevel(model.RoleGroup, Roles, JEPId);
            ViewBag.GetLifeCycle = _JEPService.GetPolicySteps(ObjectName.JEP.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, model.JEPId);
            //ViewBag.GetDocumentList = JsonConvert.SerializeObject(_CommonService.GetDocumentTypes(LookupType.DOCType.ToString()));  //Get DocumentList 
            ViewBag.GetContractProjectList = new SelectList(_JEPService.GetContractProjectList(model.ContractNo), "Value", "Text");

            if (ViewBag.GetPolicySteps.Count > 0)
            {
                if (ViewBag.GetPolicySteps[0].PolicyOrderID == 1)
                {
                    ViewBag.IsFirstStep = ViewBag.GetPolicySteps[0].PolicyOrderID;
                }
            }

            
            return View(model);
        }

        public ActionResult JEPLifeCycle(Int64 Id, Int64 JEPId)
        {
            string errorMsg = "";
            string infoMsg = "";
            string Qstring = string.Empty;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;

            bool validate = _JEPService.calculateWeightage(JEPId, out errorMsg);
            if (validate)
            {
                Int64? status = _JEPService.UpdateJEPPolicy(Id, Project, out errorMsg, out infoMsg);
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    TempData["Info"] = errorMsg;
                }
                else if (status > 0 && !string.IsNullOrEmpty(infoMsg))
                {
                    TempData["Success"] = infoMsg;
                }
                if (status != null)
                {
                    Qstring = CommonService.Encrypt("JEPId=" + status);
                }
            }
            else
            {
                Qstring = CommonService.Encrypt("JEPId=" + JEPId);
                TempData["Info"] = errorMsg;
            }
            // return RedirectToAction("Detail", new { q = Qstring });
            return RedirectToAction("Index");
        }

        #region CustomerFeedback
        [HttpGet]
        public ActionResult GetCustomerFeedback(Int64 Id, Int64? CFId)
        {
            var model = new CustomerFeedback();
            //ViewBag.AgencyList = new SelectList(_JEPService.GetAllAgency(), "AgencyId", "AgencyName");
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            ViewBag.ApprovalList = new SelectList(_CommonService.GetLookupData(LookupType.ApprovalCode.ToString()), "Id", "Lookup");
            ViewBag.GetDocumentList = JsonConvert.SerializeObject(_CommonService.GetLookupData(LookupType.DOCType.ToString()));  //Get DocumentList 
            ViewBag.AgencyList = new SelectList(_JEPService.GetTransmittalAgencyList(SessionMgtModel.ProjectFolderList.ProjectNo), "AgencyId", "AgencyName");
            model = _JEPService.JEPGetCustomerDetail(Id, CFId);
            ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
            model.GroupId = Guid.NewGuid().ToString();
            model.CurrentLocationIp = ipLocation.Ip;
            model.FilePath = ipLocation.File_Path;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCustomerFeedback(CustomerFeedback model)
        {
            string errorMsg = "";
            model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo; // // Get PRoject No. For generat  Doc No.
            Int64 status = _JEPService.AddEditCustomerFeedback(model, out errorMsg);
            if (status > 0)
            {
                if (model.JEPCustomerFeedbackId > 0)
                    return Json(new { model, Status = true, Msg = "JEP Customer Feedback updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                {
                    model.JEPCustomerFeedbackId = status;
                    return Json(new { model, Status = true, Msg = "JEP Customer Feedback added successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("List");
        }
        [HttpPost]
        public ActionResult UploadCustomerFeedbackFiles(DocumentMapping model)
        {
            string errorMsg = "";
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            model.FileLocation = ipLocation.Location;

            Int64 status = _JEPService.UploadCustomerFeedbackFiles(model, out errorMsg);
            if (status > 0)
                return Json(new { model, Status = true, Msg = "File uploaded successfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetCustomerFeedbackDocumentList(DataTableParamModel parm, string GroupId, Int64? JEPcustomerFeedbackId)
        {
            int recordsTotal = 0;
            var list = _JEPService.GetCustomerFeedbackDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, GroupId, JEPcustomerFeedbackId);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public ActionResult GETAgencyWiseDocuments(Int64? AgencyId, Int64? JEPId)
        {
            var result = _JEPService.GetDocumentDetailsWithRevision(AgencyId, JEPId, SessionMgtModel.ProjectFolderList.ProjectNo);
            if (result == null)
                return Json("", JsonRequestBehavior.AllowGet);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GET_JEP_CustomerFeedbackByDOCId_AGSId(Int64? JEPDocumentDetailsId, Int64? AgencyId, Int64? JEPId)
        {
            CustomerFeedback result = _JEPService.GetCustomerFeedbackByDOCId_AGSId(JEPDocumentDetailsId, AgencyId, Models.SessionMgtModel.ProjectFolderList.ProjectNo);
            result.RevisionNostr = "R" + result.RevisionNo;
            if (result == null)
                return Json("", JsonRequestBehavior.AllowGet);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteCustomerFeedbackDocs(Int64? Id, Int64? JEPId)
        {
            string errorMsg = "";
            bool Status = _JEPService.DeleteCustomerFeedbackDocs(Id, out errorMsg, JEPId);
            if (Status)
                return Json(new { Status = true, Msg = "Document deleted successfully" }, JsonRequestBehavior.AllowGet);
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = true, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GET_JEPCustomerFeedbackList()
        {
            // ViewBag.JEPId = model.JEPId;
            return PartialView("_partialJEPCustomerFeedbackList");
        }

        [HttpPost]
        public ActionResult GetAllCustomerFeedbackList(DataTableParamModel parm, Int64 JepId)
        {
            int recordsTotal = 0;
            var list = _JEPService.GetAllCustomerFeedbackList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, JepId, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpPost]
        public JsonResult AddReturnRemarks(ReturnRemark model)
        {
            string errorMsg = "";
            string RedirectAction = string.Empty;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? status = _JEPService.ReviseJEPPolicy(model.ReturnNotes, model.JEPId, Project, out errorMsg);

            if (status > 0)
            {
                RedirectAction = ("/DES/JEP/Detail?q=" + CommonService.Encrypt("JEPId=" + status));
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return Json(new { RedirectAction, Status = true, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json(new { RedirectAction, Status = true, Msg = "JEP return successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult EditJEPDocument(JEPModel model, string btnSubmit)
        {
            string errmsg = "";
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            bool status = true;

            if (model.IsUpdateChanges)
            {
                string GetCheckInPsName;
                bool IsChekIn = _JEPService.IsCheckInJEP(CommonService.objClsLoginInfo.UserName, model.JEPId,out GetCheckInPsName);
                if (IsChekIn != true)
                {
                    return Json(new { Info = true, Status = false, Msg = "JEP is already in use by " + GetCheckInPsName }, JsonRequestBehavior.AllowGet);
                }

                model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
                model.BUId = SessionMgtModel.ProjectFolderList.BUId;
                status = _JEPService.EditJEPDocument(model);
            }

            if (btnSubmit == "btnSubmit" && model.JEPId > 0)
            {
                bool status1 = _JEPService.AddEditJEPStatus(model.JEPId.Value, Project, out errmsg);
                if (status1)
                {
                    _JEPService.UpdateIsCheckInJEP(model.JEPId, false);
                    string rurl = string.Empty;
                    if (SessionMgtModel.ProjectFolderList.FolderId != null && SessionMgtModel.ProjectFolderList.FolderId > 0)
                    {
                        rurl = "/DES/Project/Folder?q=" + CommonService.Encrypt("Id=" + Models.SessionMgtModel.ProjectFolderList.FolderId);
                    }
                    else
                    {
                        rurl = "/DES/JEP";
                    }
                    return Json(new { Status = true, Submitted = true, Msg = "JEP is submitted for approval", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrEmpty(errmsg))
                        return Json(new { Status = false, Submitted = false, Msg = errmsg }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { Status = false, Submitted = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }
            }

            if (status)
            {
                string rurl = string.Empty;
                rurl = "/DES/JEP"; bool Submitted = false;
                if (btnSubmit == "btnSavechanges")
                {
                    _JEPService.UpdateIsCheckInJEP(model.JEPId, false);
                    Submitted = true;
                }
                return Json(new { Status = true, Submitted = Submitted, Msg = "JEP documents updated successfully", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errmsg))
                    return Json(new { Status = false, Submitted = false, Msg = errmsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Submitted = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetWeightage(Int64? JEPId)
        {
            List<Tuple<Int64, double>> list = _JEPService.GetWeightage(JEPId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCustomerFeedbackGrid(bool IsDetail = false)
        {
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            ViewBag.GetDocumentList = JsonConvert.SerializeObject(_CommonService.GetLookupData(LookupType.DOCType.ToString()));
            ViewBag.Role = JsonConvert.SerializeObject(new clsManager().getBUsByID(3));
            ViewBag.AgencyList = new SelectList(_JEPService.GetTransmittalAgencyList(SessionMgtModel.ProjectFolderList.ProjectNo), "AgencyId", "AgencyName");
            ViewBag.ApprovalList = JsonConvert.SerializeObject(_CommonService.GetLookupData(LookupType.ApprovalCode.ToString()));
            var model = new CustomerFeedback();
            model.GroupId = Guid.NewGuid().ToString();
            model.CurrentLocationIp = ipLocation.Ip;
            model.FilePath = ipLocation.File_Path;
            model.IsDetail = IsDetail;
            return View(model);
        }

        [HttpPost]
        public ActionResult GetCustomerFeedbackGrid(DataTableParamModel parm, Int64? Agency)
        {
            int recordsTotal = 0;
            var Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _JEPService.GetCustomerFeedbackGrid(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, SessionMgtModel.ProjectFolderList.ProjectNo, Agency, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddCustomerFeedbackGrid(CustomerFeedback model, string btnSubmit)
        {
            if (model.JEPCustFeedbackList == null)
            {
                return Json(new { Status = false, Submitted = false, Msg = "Documents not found" }, JsonRequestBehavior.AllowGet);
            }
            if (model.JEPCustFeedbackList.Count() > 0)
            {
                List<string> infoMsg = new List<string>();
                foreach (var item in model.JEPCustFeedbackList)
                {
                    if (item.ApprovalId != null)
                    {
                        if (item.ReceiptDate == null)
                        {
                            string Msg = "Actual Receipt Date is required for " + item.DocNo+" - R"+item.RevisionNo;
                            infoMsg.Add(Msg);
                        }
                    }
                    if (item.ReceiptDate != null)
                    {
                        if (item.ApprovalId == null)
                        {
                            string Msg = "Approval Code is required for " + item.DocNo+" - R" + item.RevisionNo; ;
                            infoMsg.Add(Msg);
                        }
                    }
                }
                if (infoMsg.Count > 0)
                {
                    return Json(new { Status = false, Submitted = false, Msg = infoMsg }, JsonRequestBehavior.AllowGet);
                }
            }

            bool status1 = false;
            string errmsg = "";
            if (model.IsUpdateChanges)
            {
                model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
                status1 = _JEPService.EditCustFeedback(model, out errmsg);
            }



            if (btnSubmit == "btnSubmit")
                return Json(new { Status = true, Submitted = true, Msg = "JEP Customer Feedback updated successfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Status = true, Submitted = false, Msg = "JEP Customer Feedback updated successfully" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult AddCustFeedbackByAgency(Int64? Agency)
        {
            bool IsChekIn = _JEPService.IsCheckInAgency(CommonService.objClsLoginInfo.UserName, Agency);

            if (!IsChekIn)
            {
                var ag = _JEPService.AgencyCheckInByPersonName(Agency);
                return Json(new { InfoMsg = true, Status = true, Msg = "Agency is already in use by " + ag }, JsonRequestBehavior.AllowGet);
            }

            var status = _JEPService.AddCustFeedbackByAgency(Agency);
            return Json(new { InfoMsg = false, Status = true, Msg = "Customer feedback added successfully" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ImportExcel(HttpPostedFileBase file, Int64 JEPId)
        {
            try
            {
                List<string> errmsg = new List<string>();
                JEPModel Model = new JEPModel();
                if (file != null && file.ContentLength > 0)
                {

                    string extension = Path.GetExtension(file.FileName);
                    if (extension == ".xlsx" || extension == ".xls")
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        _FileName = DateTime.Now.ToString("ddMMyyyymmss") + _FileName;
                        string _path = Path.Combine(Server.MapPath("~/Uploads/JEPUploadedFiles"), _FileName);
                        file.SaveAs(_path);

                        DataTable dt = new DataTable();
                        //Open the Excel file using ClosedXML.
                        using (XLWorkbook workBook = new XLWorkbook(_path))
                        {
                            //Read the first Sheet from Excel file.
                            IXLWorksheet workSheet = workBook.Worksheet(1);

                            //Create a new DataTable.

                            //Loop through the Worksheet rows.
                            bool firstRow = true;
                            foreach (IXLRow row in workSheet.Rows())
                            {
                                //Use the first row to add columns to DataTable.
                                if (!row.IsEmpty())
                                {


                                    if (firstRow)
                                    {
                                        foreach (IXLCell cell in row.Cells())
                                        {
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                            {
                                                if (cell.Value.ToString() == "Type" || cell.Value.ToString() == "DocNo" || cell.Value.ToString() == "Revision" || cell.Value.ToString() == "Description" || cell.Value.ToString() == "Source Folder" || cell.Value.ToString() == "FileName" || cell.Value.ToString() == "Destination Folder" || cell.Value.ToString() == "Customer DOC No" || cell.Value.ToString() == "Customer Rev No" || cell.Value.ToString() == "Planned Date" || cell.Value.ToString() == "Department" || cell.Value.ToString() == "Milestone" || cell.Value.ToString() == "Return Notes")
                                                {

                                                    dt.Columns.Add(cell.Value.ToString());
                                                }
                                                else
                                                {
                                                    return Json(new
                                                    {
                                                        Status = false,
                                                        Msg = cell.Value.ToString() + " is not correct field in uploded excel",
                                                        IsSwal = false
                                                    }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        firstRow = false;
                                    }
                                    else
                                    {
                                        int i = 0;
                                        DataRow toInsert = dt.NewRow();
                                        foreach (IXLCell cell in row.Cells(1, dt.Columns.Count))
                                        {
                                            try
                                            {
                                                toInsert[i] = cell.Value.ToString();
                                            }
                                            catch
                                            {

                                            }
                                            i++;
                                        }
                                        dt.Rows.Add(toInsert);
                                    }
                                }
                                else
                                {
                                    row.Delete();
                                }
                            }
                            Model.JEPId = JEPId;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            IpLocation ipLocation = CommonService.GetUseIPConfig;
                            string DocumentMsg = "";
                            var getMSG = _JEPService.AddImportedData(dt, SessionMgtModel.ProjectFolderList.ProjectNo, ipLocation, Models.SessionMgtModel.ProjectFolderList.RoleGroup, CommonService.objClsLoginInfo.UserName, SessionMgtModel.ProjectFolderList.BUId, Convert.ToInt32(_CommonService.GetLocation().FirstOrDefault().Value), JEPId, file.FileName, CommonService.objClsLoginInfo.Department, out errmsg,out DocumentMsg);

                            if (errmsg != null && errmsg.Count > 0)
                            {
                                return Json(new { Status = true, Msg = string.Join("<br/>", errmsg), IsSwal = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { Status = true, Msg = getMSG, DocumentMsg= DocumentMsg, IsSwal = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { Status = false, Msg = "Data not found in excel file", IsSwal = false }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { Status = false, Msg = "Please upload only Excel format file", IsSwal = false }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(new { Status = false, Msg = "Please select excel file!" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Msg = "ERROR:" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateJEPCheckOut(int JEPId)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string errorMsg = "";

            var psno = CommonService.objClsLoginInfo.UserName;
            if (JEPId != null)
            {
                bool IsChekIn = _JEPService.UpdateIsCheckInJEP(JEPId, false);
                return Json(new { Status = true, Msg = "JEP has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public JsonResult IsJEPHasDocument(Int64? JEPId)
        {
            string errorMsg = "";
            bool status = _JEPService.IsJEPHasDocument(JEPId, CommonService.objClsLoginInfo.Department, out errorMsg);
            if (status != false)
            {
                return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public void ExportToExcel(Int64? JEPId)
        {
            bool status = _JEPService.ExportToExcel(JEPId);
        }
    }
}