﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.DES.Models;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class JEPTemplateController : Controller
    {
        IGlobalJEPTemplateService _GlobalJEPTemplateService;
        IBUService _BUService;
        IProductService _ProductService;

        public JEPTemplateController(IGlobalJEPTemplateService GlobalJEPTemplateService, IBUService BUService, IProductService ProductService)
        {
            _GlobalJEPTemplateService = GlobalJEPTemplateService;
            _BUService = BUService;
            _ProductService = ProductService;
        }

        // GET: DES/JEPTemplate
        public ActionResult Index()
        {
            return View();
        }

        #region Global JEP Template
        public ActionResult AddGlobal()
        {
            GlobalJEPTemplateModel model = new GlobalJEPTemplateModel();

            ViewBag.BuList = new SelectList(_BUService.GetAllBU(), "BUId", "BU");
            ViewBag.ProductList = new SelectList(_ProductService.GetAllProduct(), "ProductId", "Product");
            //  ViewBag.TemplateList = new SelectList(_GlobalJEPTemplateService.GetAllGlobalJEPTemplateByProductId(SessionMgtModel.ProjectFolderList.ProjectNo, (Int64)SessionMgtModel.ProjectFolderList.BUId), "JEPTemplateId", "TemplateName");

            if (model.JEPTemplateDocList == null || model.JEPTemplateDocList.Count == 0)
                model.JEPTemplateDocList = new List<JEPTemplateDocumentModel> { new JEPTemplateDocumentModel() };
            return View(model);
        }

        [HttpPost]
        public ActionResult GetGlobalJEPTemplateList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _GlobalJEPTemplateService.GetGlobalJEPTemplateList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult EditGlobal(Int64 Id)
        {
            GlobalJEPTemplateModel model = new GlobalJEPTemplateModel();

            ViewBag.BuList = new SelectList(_BUService.GetAllBU(), "BUId", "BU");
            ViewBag.ProductList = new SelectList(_ProductService.GetAllProduct(), "ProductId", "Product");
            //ViewBag.TemplateList = new SelectList(_GlobalJEPTemplateService.GetAllGlobalJEPTemplate(SessionMgtModel.ProjectFolderList.ProjectNo, (Int64)SessionMgtModel.ProjectFolderList.BUId), "JEPTemplateId", "TemplateName");

            if (Id > 0)
            {
                _GlobalJEPTemplateService.UpdateIsCheckInJEPTemplate(Id, true);
                model = _GlobalJEPTemplateService.GetGlobalJEPTemplatebyId(Id);
                if (model == null)
                    model = new GlobalJEPTemplateModel();

            }
            if (model.JEPTemplateDocList == null || model.JEPTemplateDocList.Count == 0)
                model.JEPTemplateDocList = new List<JEPTemplateDocumentModel> { new JEPTemplateDocumentModel() };
            return View("AddGlobal", model);
        }

        [HttpPost]
        public ActionResult AddGlobal(GlobalJEPTemplateModel model)
        {
            Int64 status = _GlobalJEPTemplateService.AddEdit(model);
            _GlobalJEPTemplateService.UpdateIsCheckInJEPTemplate(model.JEPTemplateId, false);
            if (status > 0)
            {
                if (model.JEPTemplateId > 0)
                    TempData["Success"] = "Global JEP template updated successfully";
                else
                    TempData["Success"] = "Global JEP template added successfully";
            }
            else
            {
                TempData["Error"] = "something went wrong try again later";
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteGlobalJEP(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _GlobalJEPTemplateService.DeleteGlobalJEPTemplate(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Global JEP template deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddDocumentList(int TNo)
        {
            GlobalJEPTemplateModel model = new GlobalJEPTemplateModel();
            TempData["NoTask"] = TNo;
            return PartialView("_DocumentList", model);
        }

        public ActionResult CopyDocumentList(Int64 Id)
        {
            GlobalJEPTemplateModel model = new GlobalJEPTemplateModel();
            model = _GlobalJEPTemplateService.GetGlobalJEPTemplatebyId(Id);
            return PartialView("_DocumentList", model);
        }

        [HttpPost]
        public JsonResult CheckTemplateNameAlreadyExist(string TemplateName, Int64? JEPTemplateId)
        {
            return Json(!_GlobalJEPTemplateService.CheckTemplateNameAlreadyExist(TemplateName, JEPTemplateId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProductList(Int64 BuId)
        {
            return Json(_GlobalJEPTemplateService.GetAllGlobalJEPByProduct(BuId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TemplateList(Int64 ProductId)
        {
            return Json(_GlobalJEPTemplateService.GetAllGlobalJEPTemplateByProductId(ProductId, Models.SessionMgtModel.ProjectFolderList.BUId), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region JEP Template

        public ActionResult List()
        {
            return View();
        }
        public ActionResult Add()
        {
            GlobalJEPTemplateModel model = new GlobalJEPTemplateModel();

            ViewBag.ProductList = new SelectList(_ProductService.GetAllProduct(), "ProductId", "Product");

            if (model.JEPTemplateDocList == null || model.JEPTemplateDocList.Count == 0)
                model.JEPTemplateDocList = new List<JEPTemplateDocumentModel> { new JEPTemplateDocumentModel() };
            return View(model);
        }

        [HttpPost]
        public ActionResult GetJEPTemplateList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _GlobalJEPTemplateService.GetJEPTemplateList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, (SessionMgtModel.ProjectFolderList.ProjectNo ?? string.Empty));

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult Edit(Int64 Id)
        {
            GlobalJEPTemplateModel model = new GlobalJEPTemplateModel();

            ViewBag.ProductList = new SelectList(_ProductService.GetAllProduct(), "ProductId", "Product");

            if (Id > 0)
            {
                _GlobalJEPTemplateService.UpdateIsCheckInJEPTemplate(Id, true);
                model = _GlobalJEPTemplateService.GetJEPTemplatebyId(Id);
                if (model == null)
                    model = new GlobalJEPTemplateModel();

            }
            if (model.JEPTemplateDocList == null || model.JEPTemplateDocList.Count == 0)
                model.JEPTemplateDocList = new List<JEPTemplateDocumentModel> { new JEPTemplateDocumentModel() };
            return View("Add", model);
        }

        [HttpPost]
        public ActionResult Add(GlobalJEPTemplateModel model)
        {
            model.Project = SessionMgtModel.ProjectFolderList.ProjectNo ?? string.Empty;
            Int64 status = _GlobalJEPTemplateService.AddEditProjectTemplate(model);
            _GlobalJEPTemplateService.UpdateIsCheckInJEPTemplate(model.JEPTemplateId, false);
            if (status > 0)
            {
                if (model.JEPTemplateId > 0)
                    TempData["Success"] = "JEP template updated successfully";
                else
                    TempData["Success"] = "JEP template added successfully";
            }
            else
            {
                TempData["Error"] = "something went wrong try again later";
            }
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult DeleteJEP(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _GlobalJEPTemplateService.DeleteJEPTemplate(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "JEP template deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateJEPTemplateCheckOut(Int64 JEPTemplateId)
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            if (JEPTemplateId > 0)
            {
                bool IsChekIn = _GlobalJEPTemplateService.UpdateIsCheckInJEPTemplate(JEPTemplateId, false);
                return Json(new { Status = true, Msg = "JEP Template has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}