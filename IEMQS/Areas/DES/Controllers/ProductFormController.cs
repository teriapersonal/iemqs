﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    public class ProductFormController : Controller
    {
        IProductFormCodeService _ProductFormCodeService;
        IBUService _BUService;

        public ProductFormController(IProductFormCodeService ProductFormCodeService, IBUService BUService)
        {
            _ProductFormCodeService = ProductFormCodeService;
            _BUService = BUService;
        }

        // GET: DES/ProductForm
        public ActionResult Index()
        {
            ProductFormCodeModel model = new ProductFormCodeModel();
            ViewBag.BuList = JsonConvert.SerializeObject(_BUService.GetAllBU());          
            ViewBag.ProdList = JsonConvert.SerializeObject(_ProductFormCodeService.GetProdctFormList());
            ViewBag.RoleGroup = CommonService.objClsLoginInfo.UserRoles;
            var RoleGroup = _ProductFormCodeService.GetRoleGroup(CommonService.objClsLoginInfo.UserName);
            model.IsITRoleGroup = RoleGroup;
            return View(model);
        }

        #region Product Form Code
        [HttpPost]
        public ActionResult GetProductFormList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ProductFormCodeService.GetProductFormList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddProductForm(ProductFormCodeModel model)
        {
            string errorMsg = "";
            Int64 status = _ProductFormCodeService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.ProductFormCodeId > 0)
                    return Json(new { Status = true, Msg = "Product form code updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Product form code added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteProductForm(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ProductFormCodeService.DeleteProductForm(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Product form code deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateSubProduct(ProductFormCodeModel model)
        {
            //string errorMsg = "";
            bool status = _ProductFormCodeService.UpdateSubProduct(model);
            if (status == true)
            {
                return Json(new { Status = true, Msg = "SubProductForm is already exist." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}