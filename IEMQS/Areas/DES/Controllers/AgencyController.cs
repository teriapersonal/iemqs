﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    public class AgencyController : Controller
    {
        IBUService _BUService;
        IAgencyService _AgencyService;

        public AgencyController(IBUService BUService, IAgencyService AgencyService)
        {
            _BUService = BUService;
            _AgencyService = AgencyService;
        }

        // GET: DES/Agency
        public ActionResult Index()
        {
            ViewBag.BuList = JsonConvert.SerializeObject(_BUService.GetAllBU());
            return View();
        }

        #region Maintain Agency

        [HttpPost]
        public ActionResult GetAgencyList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _AgencyService.GetAgencyList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddAgency(AgencyModel model)
        {
            string errorMsg = "";
            Int64 status = _AgencyService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.AgencyId > 0)
                    return Json(new { Status = true, Msg = "Agency updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Agency added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteAgency(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _AgencyService.DeleteAgency(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Agency deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult CheckAgencyCheckIn(Int64 AgencyId)
        {
            if (AgencyId > 0)
            {
                bool IsChekIn = _AgencyService.IsCheckInAgency(CommonService.objClsLoginInfo.UserName, AgencyId);
                if (!IsChekIn)
                {
                    var list = _AgencyService.GetAllAgency();
                    AgencyModel _AgencyModel = new AgencyModel();
                    string strAgencyCheckInBy = string.Empty;
                    if (list != null && list.Count > 0)
                    {
                        _AgencyModel = list.Where(x => x.AgencyId == AgencyId).FirstOrDefault();
                        if (!string.IsNullOrEmpty(_AgencyModel.AgencyCheckInBy))
                        {
                            strAgencyCheckInBy = new clsManager().GetUserNameFromPsNo(_AgencyModel.AgencyCheckInBy);
                        }
                    }
                    return Json(new { Status = true, Msg = "Agency is already in use by " + strAgencyCheckInBy }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}