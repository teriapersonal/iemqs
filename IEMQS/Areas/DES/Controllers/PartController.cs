﻿using IEMQS.Areas.DES.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class PartController : Controller
    {
        IProjectService _ProjectService;
        IPartService _PartService;
        ICommonService _CommonService;
        IPartSheetService _PartSheetService;
        List<PowerViewModel> listPowerviewtempList = null;
        public PartController(IProjectService ProjectService, IPartService PartService, ICommonService CommonService, IPartSheetService PartSheetService)
        {
            _ProjectService = ProjectService;
            _PartService = PartService;
            _CommonService = CommonService;
            _PartSheetService = PartSheetService;
        }

        // GET: DES/Part
        public ActionResult Index(PartColumnModel obj)
        {
            obj = _PartService.GetSavePartColumn();
            obj.Project= Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            int? isengg = CommonService.objClsLoginInfo.ListRoles.Where(x => x.StartsWith("ENGG")).Count();
            int? ispmg = CommonService.objClsLoginInfo.ListRoles.Where(x => x.StartsWith("PMG")).Count();
            int? isplng = CommonService.objClsLoginInfo.ListRoles.Where(x => x.StartsWith("PLNG")).Count();
            if (isengg > 0)
            { 
                TempData["IsENGG"] = true;
            }
            if (ispmg > 0)
            {
                TempData["IsPMG"] = true;
            }
            if (isplng > 0)
            {
                TempData["IsPLNG"] = true;
            }
            return View(obj);
        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            int recordsTotal = 0;
            var list = _PartService.GetPartListByProject(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, _ProjectFolderList.ProjectNo, Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult ItemBOMIndex(DataTableParamModel parm)
        {
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            int recordsTotal = 0;
            var list = _PartService.GetRelatedPartListByProject(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, _ProjectFolderList.ProjectNo, Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult BOMIndex(DataTableParamModel parm)
        {
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            int recordsTotal = 0;
            var list = _PartService.GetBOMPartListByProject(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, _ProjectFolderList.ProjectNo, Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult Add()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                PartModel model = new PartModel();
                model.BOMModel = new BOMModel();
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
                var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;

                ViewBag.ItemKeyTypeList = new SelectList(_CommonService.GetItemKeyType(), "Text", "Text");
                ViewBag.OrderPolicyList = new SelectList(_CommonService.GetOrderPolicy(), "Text", "Text");
                ViewBag.PartActionList = new SelectList(_CommonService.GetPartAction(), "Text", "Text");
                ViewBag.ItemTypeList = new SelectList(_CommonService.GetItemType(null), "Text", "Text");
                ViewBag.ActionforPCRelationList = new SelectList(_CommonService.GetActionforPCRelation(), "Text", "Text");
                ViewBag.ItemGroupList = new SelectList(new List<SelectListItem>(), "Value", "Text");
                ViewBag.ARMSetList = new SelectList(_PartService.GetARMSet(), "Arms", "Desc");
                ViewBag.MaterialList = new SelectList(new List<SelectListItem>(), "Value", "Text");
                ViewBag.GetParentData = new SelectList(_PartService.GetProjectManufacturedPartList(_ProjectFolderList.ProjectNo), "Value", "Text");  //Need to get manufactured parts 
                ViewBag.GetProductFormData = new SelectList(_PartService.GetProductFormCodeList(_ProjectFolderList.BUId), "ProductFormCodeId", "ProductForm");
                ViewBag.GetSizeCodeData = new SelectList(_PartService.GetAllSizeCode(), "SizeCode", "SizeCode");
                ViewBag.DRGNoList = new SelectList(_PartService.GetCountractJepDOCbyType((int)DOCType.Manufacturing_Drawing, SessionMgtModel.ProjectFolderList.Contract), "Value", "Text");
                ViewBag.ProcurementDRGNoList = new SelectList(_PartService.GetCountractDOCbyType((int)DOCType.Procurement_Drawing, SessionMgtModel.ProjectFolderList.Contract), "Value", "Text");

                ViewBag.ItemGroupUOMList = _PartService.GetItemGroupUOMModelList();
                ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());

                model.RevNo = 0;
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        [HttpGet]
        public ActionResult Edit(Int64 ItemId, Int64? BOMId)
        {
            PartModel model = new PartModel();
            model = _PartService.GetPartByID(ItemId, BOMId);
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;

            ViewBag.ItemKeyTypeList = new SelectList(_CommonService.GetItemKeyType(), "Text", "Text");
            ViewBag.OrderPolicyList = new SelectList(_CommonService.GetOrderPolicy(), "Text", "Text");
            ViewBag.PartActionList = new SelectList(_CommonService.GetPartAction(), "Text", "Text");
            ViewBag.ItemTypeList = new SelectList(_CommonService.GetItemType(model.ProductFormCodeId), "Text", "Text");
            ViewBag.ActionforPCRelationList = new SelectList(_CommonService.GetActionforPCRelation(), "Text", "Text");
           // ViewBag.ItemGroupList = new SelectList(_PartService.GetItemGroupFromMaterial(model.Material)?.ItemGroupUOM, "t_citg", "t_deca");
            ViewBag.ARMSetList = new SelectList(_PartService.GetARMSet(), "Arms", "Desc");
            ViewBag.MaterialList = new SelectList(_PartService.GetProductFormDataById(model.ProductFormCodeId)?.MaterialList, "Value", "Text");
            ViewBag.GetParentData = new SelectList(_PartService.GetProjectManufacturedPartList(_ProjectFolderList.ProjectNo), "Value", "Text");  //Need to get manufactured parts 
            ViewBag.GetProductFormData = new SelectList(_PartService.GetProductFormCodeList(_ProjectFolderList.BUId), "ProductFormCodeId", "ProductForm");
            ViewBag.GetSizeCodeData = new SelectList(_PartService.GetAllSizeCode(), "SizeCode", "SizeCode");
            ViewBag.DRGNoList = new SelectList(_PartService.GetCountractJepDOCbyType((int)DOCType.Manufacturing_Drawing, SessionMgtModel.ProjectFolderList.Contract), "Value", "Text");
            ViewBag.ProcurementDRGNoList = new SelectList(_PartService.GetCountractDOCbyType((int)DOCType.Procurement_Drawing, SessionMgtModel.ProjectFolderList.Contract), "Value", "Text");

            ViewBag.ItemGroupUOMList = _PartService.GetItemGroupUOMModelList();
            ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());

            return View("Add", model);
        }

        [HttpPost]
        public ActionResult AddEdit(PartModel model)
        {
            string ItemKey = "",
            errorMsg = "",
            SuccessMsg = "",
            ItemKeyConflict = "";
            bool MultiFindNoItemKey = false;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64 ItemId = 0,
             BomId = 0;
            var status = 0;//_PartService.AddEdit(model, out ItemKey, out errorMsg, out SuccessMsg, out ItemKeyConflict, out MultiFindNoItemKey, out ItemId, out BomId);
            if (status > 0)
            {
                if (MultiFindNoItemKey)
                {
                    return Json(new { Status = false, Msg = "Item already exist with same find no" }, JsonRequestBehavior.AllowGet);
                }
                else if (!string.IsNullOrEmpty(ItemKeyConflict))
                {
                    return Json(new { Status = false, Msg = ItemKeyConflict }, JsonRequestBehavior.AllowGet);
                }
                else if (ItemId > 0)
                {
                    string rurl = "/DES/Part";
                    //if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
                    //    rurl = "/DES/Project/Folder?q=" + CommonService.Encrypt("Id=" + Models.SessionMgtModel.ProjectFolderList.FolderId);
                    var getNotfication = _PartService.NFPartCreate(Project, ItemId);
                    if (!string.IsNullOrEmpty(errorMsg))
                        return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        if (model.ItemId == ItemId)
                            return Json(new { Status = true, Msg = "Item id " + ItemKey + " updated successfully", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { Status = true, Msg = "Item id " + ItemKey + " added successfully", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);

                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(errorMsg))
                        return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult CheckItemKeyIsvalid(string ItemKey)
        {

            return Json(!_PartService.CheckItemKeyIsvalid(ItemKey, Models.SessionMgtModel.ProjectFolderList.Contract), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _PartService.Delete(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Part deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(Int64 ItemId)
        {
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            var model = _PartService.GetItemDetailByID(ItemId);
            ViewBag.GetPolicySteps = _PartService.GetUserLevel(model.RoleGroup, Roles, ItemId);
            string type = ObjectName.Part.ToString();
            if (model.Type == ObjectName.Jigfix.ToString())
                type = ObjectName.Jigfix.ToString();
            ViewBag.GetLifeCycle = _PartService.GetPolicySteps(type, Models.SessionMgtModel.ProjectFolderList.BUId, ItemId);
            if (ViewBag.GetPolicySteps.Count > 0)
            {
                if (ViewBag.GetPolicySteps[0].PolicyOrderId == 1)
                {
                    ViewBag.IsFirstStep = ViewBag.GetPolicySteps[0].PolicyOrderId;
                }
            }
            return View(model);
        }

        public ActionResult BOMDetail(Int64 ItemId, Int64 BOMId)
        {
            var model = _PartService.GetItemBOMDetailByID(ItemId, BOMId);
            return View(model);
        }

        public ActionResult BackToReview(Int64? Id)
        {
            string errorMsg = "";
            string Qstring = string.Empty;

            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? status = _PartService.ReviseItemPolicy(Id, Project, out errorMsg);

            if (status > 0)
            {
                TempData["Success"] = "Part has been revised successfully";
                Qstring = CommonService.Encrypt("ItemId=" + status);
            }

            return RedirectToAction("Detail", new { q = Qstring });
        }

        public ActionResult ItemLifeCycle(Int64 Id, int PolicyOrderId, string PolicyStep)
        {
            List<string> errorMsg = new List<string>();
            List<string> infoMsg = new List<string>();
            var psno = CommonService.objClsLoginInfo.UserName;
            GetListOfItmeID model = new GetListOfItmeID { ItemId = new List<long?> { Id }, PolicyOrderId = PolicyOrderId, BtnText = PolicyStep };
            var project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            bool status = _PartService.MassPromote(project,model, psno, out errorMsg, out infoMsg);
            if (status)
            {
                if (errorMsg != null && errorMsg.Count > 0)
                {
                    TempData["Info"] = MvcHtmlString.Create(string.Join("<br />", errorMsg));
                }
                if (infoMsg != null && infoMsg.Count > 0)
                {
                    TempData["Success"] = MvcHtmlString.Create(string.Join("<br />", infoMsg));
                }
            }
            return RedirectToAction("Detail", new { q = CommonService.Encrypt("ItemId=" + Id) });

        }

        [HttpPost]
        public ActionResult GetAllHistory(PartModel getId)
        {
            ViewBag.PartHistoryId = getId.ItemId;
            return PartialView("_partialPartHistory");
        }

        [HttpPost]
        public ActionResult GetBOMHistory(BOMModel getId)
        {
            ViewBag.BOMHistoryId = getId.BOMId;
            return PartialView("_partialBOMHistory");
        }



        [HttpPost]
        public ActionResult GetAllRevision(PartModel getId)
        {
            ViewBag.PartRevisionId = getId.ItemKey;
            return PartialView("_partialPartRevision");
        }

        [HttpPost]
        public ActionResult GetAllPartHistory(DataTableParamModel parm, string iId)
        {
            int recordsTotal = 0;
            var list = _PartService.GetPartHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, iId, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }



        [HttpPost]
        public ActionResult GetAllBOMHistory(DataTableParamModel parm, string iId)
        {
            int recordsTotal = 0;
            var list = _PartService.GetBOMHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, iId, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpPost]
        public ActionResult GetAllPartRevision(DataTableParamModel parm, string ItemKey)
        {
            int recordsTotal = 0;
            string project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _PartService.GetPartRevision(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, project, ItemKey, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        #region Part Sheet

        [HttpGet]
        public ActionResult AddPartSheet()
        {
            PartSheetModel model = new PartSheetModel();
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            model.Project = _ProjectFolderList.ProjectNo;
            model.FolderId = _ProjectFolderList.FolderId;
            model.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            return PartialView("_AddPartSheet", model);
        }
        [HttpPost]
        public ActionResult AddPartSheet(PartSheetModel model)
        {
            string errormsg = "";
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            model.FolderId = _ProjectFolderList.FolderId;
            Int64 status = _PartSheetService.AddEdit(model, out errormsg);
            if (status > 0)
            {
                TempData["Success"] = "Part Sheet created successfully";
                string qstring = CommonService.Encrypt("Id=" + status);
                return RedirectToAction("PartSheet", new { q = qstring });
            }
            else
            {
                TempData["Error"] = "something went wrong try again later";
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateSheetCheckOut(int ItemSheetId)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string errorMsg = "";

            var psno = CommonService.objClsLoginInfo.UserName;
            if (ItemSheetId > 0)
            {
                bool IsCheckOut = true;
                bool IsChekIn = _PartSheetService.UpdateIsCheckInSheet(ItemSheetId, IsCheckOut);
                return Json(new { Status = true, Msg = "PartSheet has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult CheckSheetNameAlreadyExist(string SheetName, string Project, Int64? ItemSheetId)
        {
            return Json(!_PartSheetService.CheckSheetNameAlreadyExist(SheetName, Project, ItemSheetId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPartSheetList(DataTableParamModel parm, string Project, long FolderId)
        {
            int recordsTotal = 0;
            var list = _PartSheetService.GetPartSheetList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, Project, FolderId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult PartSheet(Int64 Id)
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                TempPartModel model = new TempPartModel();
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;

                var data = _PartSheetService.GetPartSheetById(Id);
                bool IsCheckout = false;
                var updateIsCheckIn = _PartSheetService.UpdateIsCheckInSheet(Id, IsCheckout);
                model.ItemSheetId = data.ItemSheetId;
                model.ItemSheetName = data.SheetName;
                model.Project = data.Project;

                string errorMsg;

                model.PartList = _PartService.GetTempPartListById(data.ItemSheetId, data.Project, Models.SessionMgtModel.ProjectFolderList.Contract, Models.SessionMgtModel.ProjectFolderList.BUId, out errorMsg); //new List<TempPartModel>();
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    TempData["Error"] = errorMsg;
                    return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
                }
                ViewBag.ItemGroupUOMList = _PartService.GetItemGroupUOMModelList();
                ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());
                ViewBag.ManufacturingItembyProject = new SelectList(_PartService.GetManufacturingItembyProject(data.Project), "ItemId", "ItemKey");
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        [HttpPost]
        public ActionResult PartSheet(TempPartModel model, string btnSubmit)
        {
            string errormsg = "";
            bool status = _PartService.AddEditPartSheet(model, out errormsg);

            if (status)
            {
                bool MaintainPart = false;
                List<DESCore.Data.SP_DES_MAINTAIN_ITEMSHEET_Result> list = new List<DESCore.Data.SP_DES_MAINTAIN_ITEMSHEET_Result>();
                if (btnSubmit == "MaintainPart")
                {
                    MaintainPart = true;
                    bool IsCheckOut = true;
                    bool IsChekIn = _PartSheetService.UpdateIsCheckInSheet(model.ItemSheetId, IsCheckOut);
                    list = _PartService.MaintainTimesheet(model.ItemSheetId.Value, CommonService.objClsLoginInfo.UserName, model.Department, model.RoleGroup, model.FolderId);
                }
                else
                {
                    bool IsCheckOut = false;
                    bool IsChekIn = _PartSheetService.UpdateIsCheckInSheet(model.ItemSheetId, IsCheckOut);
                }
                return Json(new { Status = true, MaintainPart = MaintainPart, MaintainPartList = list, Msg = "Prepare Part/BOM data save successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errormsg))
                    return Json(new { Status = false, Msg = errormsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetAllMaterialList(DataTableParamModel parm, string search, Int64 ProductFormId)
        {
            parm.sSearch = search;
            int recordsTotal = 0;
            var list = _PartService.GetAllMaterialList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, ProductFormId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult GetAllMaterial(string Id, string Val, string ProductFormCodeId)
        {
            TempData["Material"] = Val;
            TempData["MaterialId"] = Id;
            TempData["ProductFormCodeId"] = ProductFormCodeId;
            return PartialView("_partialAllMaterial");
        }

        [HttpGet]
        public ActionResult GetAllARM(string arm)
        {
            arm = string.IsNullOrEmpty(arm) ? arm : arm.ToLower();
            var model = _PartService.GetARMSet().Where(x => (x.Desc.ToLower()).Contains(arm));
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllSizeCode(string SizeCode)
        {
            var model = _PartService.GetAllSizeCode().Where(x => x.SizeCode.Contains(SizeCode));
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public ActionResult GetMaterialById(string Id,string Val)
        //{
        //    var data = _PartService.GetMaterialById(Id);
        //    return PartialView("_partialAllMaterial", data);
        //    //return PartialView("_Material", data);
        //}

        [HttpPost]
        public ActionResult GetAllItemGroupList(DataTableParamModel parm, string searchitem, string materialId)
        {
            //if (!string.IsNullOrEmpty(searchitem))
            parm.sSearch = searchitem;
            int recordsTotal = 0;
            var list = _PartService.GetAllItemGroupList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, materialId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult GetAllItemGroup(string Id, string Val, string MaterialId)
        {
            TempData["ItemGroup"] = Val;
            TempData["ItemGroupId"] = Id;
            TempData["MaterialId"] = MaterialId;
            //ViewBag.ItemGroup = Val;
            return PartialView("_partialAllItemGroup");
        }

        //[HttpGet]
        //public ActionResult GetItemGrpById(string Id)
        //{
        //    // data = null;// _PartService.GetItemGroupById(Id);
        //    return PartialView("_ItemGroup");//, data);
        //}

        [HttpPost]
        public ActionResult GetAllSizeCodeList(DataTableParamModel parm, string searchsizecode)
        {
            parm.sSearch = searchsizecode;
            int recordsTotal = 0;
            var list = _PartService.GetAllSizeList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult GetAllSize(string Id, string Val)
        {
            TempData["SizeCodeId"] = Id;
            TempData["SizeCode"] = Val;
            //var data = _PartService.GetSizeById(Id); , ViewBag.SizeCode //ViewBag.SizeCode = Val;
            return PartialView("_partialAllSizeCode");
        }

        //[HttpGet]
        //public ActionResult GetSizeCodeById(string Id)
        //{
        //    var data = _PartService.GetSizeById(Id);
        //    return PartialView("_SizeCode", data);
        //}

        [HttpGet]
        public ActionResult GetSizeCodeDetailsById(string Id)
        {
            var data = _PartService.GetSizeById(Id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllARMSetList(DataTableParamModel parm, string searcharmset)
        {
            parm.sSearch = searcharmset;
            int recordsTotal = 0;
            var list = _PartService.GetAllARMSetList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult GetAllARMSet(string Id, string Val)
        {
            TempData["ARMSetId"] = Id;
            TempData["ARMSet"] = Val;
            //ViewBag.ARMSet = Val;
            //var data = _PartService.GetARMSetById(Id); , ViewBag.ARMSet
            return PartialView("_partialAllARMSet");
        }

        //[HttpGet]
        //public ActionResult GetARMSetById(string Id)
        //{
        //    var data = _PartService.GetARMSetById(Id);
        //    return PartialView("_ARMSet", data);
        //}

        [HttpGet]
        public ActionResult GetARMSetDetailsById(string Id)
        {
            var data = _PartService.GetARMSetById(Id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMoreRow(int TNo, Int64 ItemSheetId, string Project)
        {
            TempPartModel model = new TempPartModel();
            TempData["NoTask"] = TNo;

            string errorMsg;
            model.PartList = _PartService.Add5Row(TNo, 5, ItemSheetId, Project, Models.SessionMgtModel.ProjectFolderList.Contract, Models.SessionMgtModel.ProjectFolderList.BUId, out errorMsg); //new List<TempPartModel>();
            if (!string.IsNullOrEmpty(errorMsg))
            {
                TempData["Error"] = errorMsg;
                return Json("");
            }
            return PartialView("_PartSheetList", model);
        }

        public JsonResult GetUserDefinedItemKey(string ItemKey, string Contract)
        {
            PartModel model = new PartModel();
            model = _PartService.GetUserDefinedItemKey(ItemKey, Contract);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataByItemAndParentItem(long ItemId, long ParentItemId, string Project, int? FindNumber)
        {
            BOMModel model = new BOMModel();
            model = _PartService.GetDataByItemAndParentItem(ItemId, ParentItemId, Project, FindNumber);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetItemGroupFromMaterial(string Material,string ProductForm)
        {
            var getMat = _PartService.GetItemGroupFromMaterial(Material,  ProductForm);
            return Json(getMat, JsonRequestBehavior.AllowGet);
        }

        #endregion

        //Get Policy 

        public JsonResult GePolicyByType(string Type)
        {
            Int64? BUID = Models.SessionMgtModel.ProjectFolderList.BUId;
            if (Type == ItemType.Jigfix.ToString())
            {
                return Json(_PartService.GetPartPolicyList(BUID).Where(x => x.ObjectName == ItemType.Jigfix.ToString()).FirstOrDefault(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(_PartService.GetPartPolicyList(BUID).Where(x => x.ObjectName == ItemType.Part.ToString()).FirstOrDefault(), JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetProductFomDesc(Int64? productFormId)
        {
            var getDec = _PartService.GetProductFormDataById(productFormId);
            return Json(getDec, JsonRequestBehavior.AllowGet);
        }


        #region Power View

        public ActionResult PowerView(long? ItemId = null)
        {
            PartPowerViewModel obj = new PartPowerViewModel();
            obj.ItemId = ItemId;
            obj.QString = CommonService.Encrypt("ItemId=" + ItemId);
            string Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            List<PowerViewModel> objPowerviewList = new List<PowerViewModel>();
            objPowerviewList = _PartService.GetPowerViewList(Project, ItemId);

            var MainProjectDtl = objPowerviewList.Where(i => !(i.ParentItemId > 0)).First();
            if (MainProjectDtl != null)
            {

                listPowerviewtempList = new List<PowerViewModel>();
                listPowerviewtempList.Add(new PowerViewModel
                {
                    BOMId = MainProjectDtl.BOMId,
                    ItemId = MainProjectDtl.ItemId,
                    ParentItemId = MainProjectDtl.ParentItemId,
                    ItemKey = MainProjectDtl.ItemKey,
                    Type = MainProjectDtl.Type,
                    Rev = MainProjectDtl.Rev,
                    policy = MainProjectDtl.policy,
                    FindNumber = MainProjectDtl.FindNumber,
                    ExtFindNumber = MainProjectDtl.ExtFindNumber,
                    ItemName = MainProjectDtl.ItemName,
                    Quantity = MainProjectDtl.Quantity,
                    Length = MainProjectDtl.Length,
                    Width = MainProjectDtl.Width,
                    NumberOfPieces = MainProjectDtl.NumberOfPieces,
                    UOM = MainProjectDtl.UOM,
                    Material = MainProjectDtl.Material,
                    State = MainProjectDtl.State,
                    ItemGroup = MainProjectDtl.ItemGroup,
                    // TableCount = MainProjectDtl.TableCount,
                });

                GetChildItems(objPowerviewList, MainProjectDtl.ItemId);
            }
            obj.PowerViewModelList = listPowerviewtempList;

            return View(obj);

        }





        public void GetChildItems(List<PowerViewModel> objPowerviewList, Int64? ParentId)
        {
            var listChilds = objPowerviewList.Where(i => i.ParentItemId == ParentId).ToList();
            foreach (var item in listChilds)
            {
                listPowerviewtempList.Add(new PowerViewModel
                {
                    BOMId = item.BOMId,
                    ItemId = item.ItemId,
                    ParentItemId = item.ParentItemId,
                    ItemKey = item.ItemKey,
                    Type = item.Type,
                    Rev = item.Rev,
                    policy = item.policy,
                    FindNumber = item.FindNumber,
                    ExtFindNumber = item.ExtFindNumber,
                    ItemName = item.ItemName,
                    Quantity = item.Quantity,
                    Length = item.Length,
                    Width = item.Width,
                    NumberOfPieces = item.NumberOfPieces,
                    UOM = item.UOM,
                    Material = item.Material,
                    State = item.State,
                    ItemGroup = item.ItemGroup,
                    //TableCount = item.TableCount,
                });
                GetChildItems(objPowerviewList, item.ItemId);
            }

        }

        public ActionResult TreeView(long? ItemId = null)
        {
            PartTreeViewModel obj = new PartTreeViewModel();
            obj.ItemId = ItemId;
            string Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            obj.QString = CommonService.Encrypt("ItemId=" + ItemId);
            obj.TreeViewModelList = _PartService.GetTreeViewList(Project, ItemId);
            return View(obj);

        }

        [HttpPost]
        public ActionResult SaveColumnUserwise(PartColumnModel model)
        {

            Int64 status = _PartService.SavePartColumn(model);
            return RedirectToAction("index");
        }
        #endregion


        [HttpPost]
        public ActionResult ParentItemList(DataTableParamModel parm, Int64 ItemId, string Project)
        {
            int recordsTotal = 0;
            var list = _PartService.GetParentItemBOM(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, Project, out recordsTotal, ItemId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult ChildItemList(DataTableParamModel parm, Int64 ItemId, string Project)
        {
            int recordsTotal = 0;
            var list = _PartService.GetChildItemBOM(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, Project, out recordsTotal, ItemId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public ActionResult CreateXML()
        {
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            string errorMsg = "";
            string CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            string filename = _PartService.CreateXMLFile(_ProjectFolderList.ProjectNo, CurrentPsNo, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
            {
                return Json(new { Status = true, Msg = "Push To ERP LN Successful" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        protected void DownloadXML(string filename)
        {
            if (System.IO.File.Exists(Server.MapPath("~/Uploads/XMLFiles/" + filename)))
            {
                DownloadFile(Server.MapPath("~/Uploads/XMLFiles/" + filename), filename);
            }
        }

        private void DownloadFile(string filePath, string filename)
        {
            Response.ContentType = "application/xml";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
            Response.WriteFile(filePath);
            Response.End();
        }


        public ActionResult BOMListPopup(long ItemId)
        {
            ViewBag.ItemId = ItemId;
            return PartialView("_BOMListPopup");
        }

        [HttpPost]
        public ActionResult BOMListByItemId(DataTableParamModel parm, Int64 ItemId)
        {
            int recordsTotal = 0;
            var list = _PartService.GetItemBOMList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, ItemId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }




        ////Fill Gird
        //[HttpPost]
        //public ActionResult GetPendingPart(DataTableParamModel parm)
        //{
        //    int recordsTotal = 0;
        //    var psno = CommonService.objClsLoginInfo.UserName;
        //    string Roles = CommonService.objClsLoginInfo.UserRoles;
        //    var list = _PartService.GetPendingPartList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, psno, Roles, out recordsTotal);


        //    return Json(new
        //    {
        //        Echo = parm.sEcho,
        //        iTotalRecord = recordsTotal,
        //        iTotalDisplayRecords = recordsTotal,
        //        data = list
        //    });
        //}


        //[HttpPost]  //On Check box change Status
        //public JsonResult MASKLifeCyclePart(GetListOfItmeID ItemIds)
        //{
        //    List<string> errorMsg = new List<string>();
        //    List<string> infoMsg = new List<string>();
        //    var psno = CommonService.objClsLoginInfo.UserName;
        //    string Roles = CommonService.objClsLoginInfo.UserRoles;
        //    var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
        //    var data = _PartService.GetMassPromoteUserPolicy(psno, Roles, ItemIds.BtnText.Trim());
        //    Int64? status = 0;
        //    if (data != null)
        //    {
        //        foreach (var item in data)
        //        {
        //            if (ItemIds.ItemId.Contains(item.ItemId))
        //            {
        //                string emsg, imsg;

        //                status = _PartService.UpdateItemPolicy(item.PartLifeCycleId, Project, out emsg, out imsg);
        //                if (!string.IsNullOrEmpty(emsg))
        //                    errorMsg.Add(emsg);
        //                if (!string.IsNullOrEmpty(imsg))
        //                    infoMsg.Add(imsg);

        //            }
        //        }
        //        if (errorMsg != null && errorMsg.Count > 0)
        //        {
        //            TempData["Error"] = MvcHtmlString.Create(string.Join("<br />", errorMsg));
        //        }
        //        if (infoMsg != null && infoMsg.Count > 0)
        //        {
        //            TempData["Success"] = MvcHtmlString.Create(string.Join("<br />", infoMsg));
        //        }
        //        return Json(new { Status = true, Msg = "Mass" + ItemIds.BtnText + " successfully" }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(new { Status = false, Msg = "Data not found" }, JsonRequestBehavior.AllowGet);
        //    }
        //}


        //[HttpPost]  //On Check box change Status
        //public JsonResult ReturnMASKPart(GetListOfItmeID ItemIds)
        //{
        //    string errorMsg = "";
        //    string Roles = CommonService.objClsLoginInfo.UserRoles;
        //    Int64? status = 0;
        //    var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
        //    foreach (var item in ItemIds.ItemId)
        //    {
        //        status = _PartService.ReviseItemPolicy(item, Project, out errorMsg);

        //    }
        //    if (status != null)
        //    {
        //        return Json(new { Status = true, Msg = "Document return successfully" }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(errorMsg))
        //            return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
        //        else
        //            return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        //    }
        //}



        [HttpPost]
        public JsonResult GetManufacturingItembyProject(string Project)
        {
            return Json(_PartService.GetManufacturingItembyProject(Project), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult PartCopyToSheet(Int64 ManufacturingItem, Int64 ItemSheetId)
        {
            int status = _PartService.PartCopyToSheet(ManufacturingItem, ItemSheetId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #region CopyPart
        public ActionResult CopyPart()
        {
            PartGroupModel model = new PartGroupModel();
            string ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            ViewBag.ProjectList = new SelectList(_PartService.GetProjects(), "Project", "ProjectDesc", ProjectNo);
            //ViewBag.ContactProjectList = new SelectList(_PartService.GetContractProjects(ProjectNo), "id", "text");
            if (model != null && (model.CopyPartGroupList == null || model.CopyPartGroupList.Count() == 0))
            {
                model.CopyPartGroupList = new List<CopyPartGroupDepartmentModel> {
                    new CopyPartGroupDepartmentModel
                    {
                          ProjectList = _PartService.GetProjects().Select(x => new SelectListItem
                            {
                                Text = x.ProjectDesc,
                                Value = x.Project,
                            }).ToList(),
                     },
                };
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult GetProjectsManufactureUniqueItem(string Project)
        {
            List<string> Projects = Project.Split(',').ToList();
            List<string> ProjectTextTrim = Projects.Select(s => s.Trim()).ToList();
            return Json(_PartService.GetProjectsManufactureUniqueItem(ProjectTextTrim), JsonRequestBehavior.AllowGet);
        }

        //public ActionResult AddCopyPart() //, string dept
        //{
        //    M
        //    ViewBag.ProjectList = new SelectList(_PartService.GetProjects(), "Project", "ProjectDesc");
        //    return PartialView("_partialDept_PersonView", model);
        //}

        public ActionResult AddCopyPart(int TNo) //, string dept
        {
            PartGroupModel model = new PartGroupModel();
            ViewBag.ProjectList = new SelectList(_PartService.GetProjects(), "Project", "ProjectDesc");
            //ViewBag.ManufactureItemList = new SelectList(_DepartmentGroupService.GetPSNumberList(dept), "t_psno", "t_name");
            TempData["NoTask"] = TNo;
            return PartialView("_partialAddCopyPart", model);
        }

        [HttpPost]
        public ActionResult AddCopyPartGroup(PartGroupModel model)
        {

            string errorMsg = "";
            model.Department = CommonService.objClsLoginInfo.Department;
            model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
            model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            model.PsNo = CommonService.objClsLoginInfo.UserName;

            var status = _PartService.Insert_CopyPart_Source_To_destination(model, out errorMsg);

            return Json(new { Status = true, Msg = status }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public void ExportToExcel(Int64? ItemSheetId)
        {
            _PartService.ExportToExcel(ItemSheetId);
            //return null;
            //var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            //return RedirectToAction("PartSheet", "Part", new { Id = model.ItemSheetId });
        }
        #endregion

        [HttpGet]
        public ActionResult ClearCache()
        {
            return Json(_PartService.ClearCache(), JsonRequestBehavior.AllowGet);
        }



        #region ITEM
        [HttpGet]
        public ActionResult Item()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
                PartModel model = new PartModel();
                model.ActionForPart = PartAction.Create.ToString();
                model.BOMModel = new BOMModel();
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = _ProjectFolderList.RoleGroup;
                model.FolderId = _ProjectFolderList.FolderId;
                model.Project = _ProjectFolderList.ProjectNo;
                model.Contract = _ProjectFolderList.Contract;
                model.MaterialList = new List<SelectListItem>();
                model.ItemGroupList = new List<SelectListItem>();
                model.ARMSetList = new List<SelectListItem>();
                model.SizeCodeList = new List<SelectListItem>();
                model.ProcurementDRGNoList = new List<SelectListItem>();
                model.RevNo = 0;

                ViewBag.ItemKeyTypeList = new SelectList(_CommonService.GetItemKeyType(), "Text", "Text");
                ViewBag.OrderPolicyList = new SelectList(_CommonService.GetOrderPolicy(), "Text", "Text");
                ViewBag.ItemTypeList = new SelectList(_CommonService.GetItemType(null), "Text", "Text");
                ViewBag.GetProductFormData = new SelectList(_PartService.GetProductFormCodeList(_ProjectFolderList.BUId), "ProductFormCodeId", "ProductForm");
                ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());

                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        [HttpGet]
        public ActionResult EditItem(Int64 ItemId)
        {
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            PartModel model = new PartModel();
            model = _PartService.GetPartDetailsForEdit(ItemId);
            if (model == null)
            {
                TempData["Info"] = "Item data not found";
                return RedirectToAction("Item");
            }

            ViewBag.ItemKeyTypeList = new SelectList(_CommonService.GetItemKeyType(), "Text", "Text");
            ViewBag.OrderPolicyList = new SelectList(_CommonService.GetOrderPolicy(), "Text", "Text");
            ViewBag.ItemTypeList = new SelectList(_CommonService.GetItemType(null), "Text", "Text");
            ViewBag.GetProductFormData = new SelectList(_PartService.GetProductFormCodeList(_ProjectFolderList.BUId), "ProductFormCodeId", "ProductForm");
            ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());

            return View("Item", model);

        }

        [HttpPost]
        public ActionResult AddEditItem(PartModel model)
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            var Department = CommonService.objClsLoginInfo.Department;
            var RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
            Int64 ItemId; string ItemKey; string errorMsg; string SuccessMsg; string ItemKeyConflict; string ExistingItemkey; bool ConfirmExistingItemkey; bool ConfirmProductFormCode;
            string RevisionConfirmMsg; bool RevisionConfirm;
            bool status = _PartService.AddEditItem(model, psno, out ItemId, out ItemKey, out errorMsg, out SuccessMsg, out ItemKeyConflict, out ExistingItemkey, out ConfirmExistingItemkey, out ConfirmProductFormCode, out RevisionConfirmMsg, out RevisionConfirm);
            if (status)
            {
                if (RevisionConfirm)
                {
                    return Json(new { Status = false, ConfirmExistingItemkey = false, ConfirmProductFormCode = false, RevisionConfirm = true, Msg = RevisionConfirmMsg, RedirectAction = "" }, JsonRequestBehavior.AllowGet);
                }
                else if (ConfirmExistingItemkey)
                {
                    var data = _PartService.GetPartListByItemkeys(ExistingItemkey);
                    return Json(new { Status = false, ConfirmExistingItemkey = true, ExistingItemkeyData = data, ConfirmProductFormCode = false, RevisionConfirm = false, Msg = ItemKeyConflict, RedirectAction = "" }, JsonRequestBehavior.AllowGet);
                }
                else if (ConfirmProductFormCode)
                {
                    return Json(new { Status = false, ConfirmExistingItemkey = false, ConfirmProductFormCode = true, RevisionConfirm = false, Msg = ItemKeyConflict, RedirectAction = "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrEmpty(errorMsg))
                        return Json(new { Status = false, ConfirmExistingItemkey = false, ConfirmProductFormCode = false, RevisionConfirm = false, Msg = errorMsg, RedirectAction = "" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        TempData["Success"] = SuccessMsg;
                        return Json(new { Status = true, ConfirmExistingItemkey = false, ConfirmProductFormCode = false, RevisionConfirm = false, Msg = SuccessMsg, RedirectAction = "/DES/Part" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { Status = false, ConfirmExistingItemkey = false, ConfirmProductFormCode = false, RevisionConfirm = false, Msg = "something went wrong try again later", RedirectAction = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult BOM()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                PartModel model = new PartModel();
                model.BOMModel = new BOMModel();
                var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
                model.Project = _ProjectFolderList.ProjectNo;
                model.Contract = _ProjectFolderList.Contract;
                model.ItemList = new List<SelectListItem>();
                model.BOMModel.ParentItemList = new List<SelectListItem>();
                model.BOMModel.DRGNoList = new List<SelectListItem>();
                ViewBag.ActionforPCRelationList = new SelectList(_CommonService.GetActionforPCRelation(), "Text", "Text");
                ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        [HttpGet]
        public ActionResult EditBOM(Int64 ItemId, Int64 BOMId)
        {

            PartModel model = new PartModel();
            model = _PartService.GetPartByID(ItemId, BOMId);
            if (model == null || model.BOMModel == null)
            {
                TempData["Info"] = "BOM data not found";
                return RedirectToAction("BOM");
            }
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            model.Project = _ProjectFolderList.ProjectNo;
            model.Contract = _ProjectFolderList.Contract;
            ViewBag.ActionforPCRelationList = new SelectList(_CommonService.GetActionforPCRelation(), "Text", "Text");
            ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());
            return View("BOM", model);

        }

        [HttpPost]
        public ActionResult AddEditBOM(PartModel model)
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            var Department = CommonService.objClsLoginInfo.Department;
            var RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
            Int64 BOMId; string errorMsg; string SuccessMsg; string RevisionConfirmMsg; bool RevisionConfirm;
            bool status = _PartService.AddEditBOM(model, psno, Department, RoleGroup, out BOMId, out errorMsg, out SuccessMsg, out RevisionConfirmMsg, out RevisionConfirm);
            if (status)
            {
                string rurl = "/DES/Part";
                if (RevisionConfirm)
                {
                    return Json(new { Status = false, Confirm = true, Msg = RevisionConfirmMsg, RedirectAction = "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrEmpty(errorMsg))
                        return Json(new { Status = false, Confirm = false, Msg = errorMsg, RedirectAction = "" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        TempData["Success"] = SuccessMsg;
                        return Json(new { Status = true, Confirm = false, Msg = SuccessMsg, RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { Status = false, Confirm = false, Msg = "something went wrong try again later", RedirectAction = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ItemBOM()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                PartModel model = new PartModel();
                model.BOMModel = new BOMModel();
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
                var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;

                ViewBag.ItemKeyTypeList = new SelectList(_CommonService.GetItemKeyType(), "Text", "Text");
                ViewBag.OrderPolicyList = new SelectList(_CommonService.GetOrderPolicy(), "Text", "Text");
                //ViewBag.PartActionList = new SelectList(_CommonService.GetPartAction(), "Text", "Text");
                ViewBag.ItemTypeList = new SelectList(_CommonService.GetItemType(null), "Text", "Text");
                ViewBag.ActionforPCRelationList = new SelectList(_CommonService.GetActionforPCRelation(), "Text", "Text");
                ViewBag.ItemGroupList = new SelectList(new List<SelectListItem>(), "Value", "Text");
                ViewBag.ARMSetList = new SelectList(new List<SelectListItem>(), "Value", "Text");
                //ViewBag.ARMSetList = new SelectList(_PartService.GetARMSet(), "Arms", "Desc");
                ViewBag.MaterialList = new SelectList(new List<SelectListItem>(), "Value", "Text");
                ViewBag.GetParentData = new SelectList(_PartService.GetProjectManufacturedPartList(_ProjectFolderList.ProjectNo), "Value", "Text");  //Need to get manufactured parts 
                ViewBag.GetProductFormData = new SelectList(_PartService.GetProductFormCodeList(_ProjectFolderList.BUId), "ProductFormCodeId", "ProductForm");
                ViewBag.GetSizeCodeData = new SelectList(new List<SelectListItem>(), "Value", "Text");
                //ViewBag.GetSizeCodeData = new SelectList(_PartService.GetAllSizeCode(), "SizeCode", "SizeCode");
                ViewBag.DRGNoList = new SelectList(_PartService.GetCountractJepDOCbyType((int)DOCType.Manufacturing_Drawing, SessionMgtModel.ProjectFolderList.Contract), "Value", "Text");
                ViewBag.ProcurementDRGNoList = new SelectList(_PartService.GetCountractDOCbyType((int)DOCType.Procurement_Drawing, SessionMgtModel.ProjectFolderList.Contract), "Value", "Text");

                ViewBag.ItemGroupUOMList = _PartService.GetItemGroupUOMModelList();
                ViewBag.PartFixList = _CommonService.GetLookupData(LookupType.PartFix.ToString());

                model.RevNo = 0;
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        public ActionResult GetItemListForBOM(string Contract, string Item)
        {
            return Json(_PartService.GetItemListForBOM(Contract, Item), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetParentItemListForBOM(string Project, string Item)
        {
            return Json(_PartService.GetParentItemListForBOM(Project, Item), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetManufacturingDrawing(string Contract, string DocNo)
        {
            return Json(_PartService.GetJEPDOCbyType((int)DOCType.Manufacturing_Drawing, Contract, DocNo), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProcurementDrawing(string Contract, string DocNo)
        {
            return Json(_PartService.GetDOCbyType((int)DOCType.Procurement_Drawing, Contract, DocNo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetItemDataById(Int64 ItemId)
        {
            return Json(_PartService.GetItemDetailByID(ItemId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMaterialByProductFormCode(string Material, string ProductForm)
        {
            return Json(_PartService.GetMaterialByProductFormCode(Material, ProductForm), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSizeCode(string SizeCode)
        {
            return Json(_PartService.GetSizeCode(SizeCode), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetItemGroupDetail(string ItemGroup)
        {
            return Json(_PartService.GetItemGroupDetail(ItemGroup), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}