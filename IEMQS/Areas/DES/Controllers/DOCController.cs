﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class DOCController : Controller
    {
        IProjectService _ProjectService;
        IDOCService _DOCService;
        IDOCFileService _DOCMappingService;
        ICommonService _CommonService;
        List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
        DOCModel model = new DOCModel();
        // GET: DES/DOC
        public DOCController(IDOCService DOCService, IDOCFileService DOCMappingService, ICommonService CommonService, IProjectService ProjectService)
        {
            _ProjectService = ProjectService;
            _DOCService = DOCService;
            _DOCMappingService = DOCMappingService;
            _CommonService = CommonService;
        }

        #region  Document
        [HttpGet]
        public ActionResult Index()
        {

            DOCModel model = new DOCModel();
            model.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            return View(model);
        }

        //Fill Gird
        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            string project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? folderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _DOCService.GetDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, project, folderId, Roles, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public ActionResult Add()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                IpLocation ipLocation = CommonService.GetUseIPConfig;
                model.IsProjectRights = Models.SessionMgtModel.ProjectFolderList.IsProjectRight;
                model.CurrentLocationIp = ipLocation.Ip;
                model.FilePath = ipLocation.File_Path;
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                model.GetCurrentUser = CommonService.objClsLoginInfo.UserName;
                model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;  // Get PRoject No. For generat  Doc No.
                ViewBag.GetDocumentList = new SelectList(_CommonService.GetLookupData(LookupType.DOCType.ToString()), "Id", "Lookup"); //Get DocumentList 
                ViewBag.GetJEPDocumentNo = new SelectList(_DOCService.GetJEPDocumentNo(model.Department, model.Project), "JEPDocumentDetailsId", "DocNo"); //Get DocumentList 
                model.PolicySteps = _DOCService.GetPolicySteps(ObjectName.DOC.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, null); // Get Poloicy Steps
                model.DocumentRevision = 0;
                model.DocumentRevisionStr = "R" + model.DocumentRevision;
                var Roles = CommonService.objClsLoginInfo.UserRoles;
                model.IsProjectRights = Models.SessionMgtModel.ProjectFolderList.IsProjectRight;
                model.IsDocDownloadRights = true;//_DOCService.GetDownloadRigts(Roles, Models.SessionMgtModel.ProjectFolderList.RoleGroup, Id, model.GetCurrentUser);
                ViewBag.GetPolicySteps = _DOCService.GetUserLeavel(model.RoleGroup, Roles, 0);
                ViewBag.GetLifeCycle = _DOCService.GetPolicySteps(ObjectName.DOC.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, 0);
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        // Document Edit DOC
        public ActionResult Edit(Int64 Id, string Revision = null)
        {
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            if (Revision == "Yes")
            {
                bool isExistInDCR = _DOCService.IsExistINDCR(Id);
                if (isExistInDCR)
                {
           
                    DOCModel model = _DOCService.GetDocumentByID(Id);
                    model.IsProjectRights = Models.SessionMgtModel.ProjectFolderList.IsProjectRight;
                    model.CurrentLocationIp = ipLocation.Ip;
                    model.FilePath = ipLocation.File_Path;
                    model.DocumentRevision = model.DocumentRevision + 1;
                    model.DocumentRevisionStr = "R" + model.DocumentRevision;
                    model.GetCurrentUser = CommonService.objClsLoginInfo.UserName;
                    model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo; // Get PRoject No. For generat  Doc No.
                    ViewBag.GetDocumentList = new SelectList(_CommonService.GetLookupData(LookupType.DOCType.ToString()), "Id", "Lookup", model.DocumentTypeId);
                    ViewBag.GetJEPDocumentNo = new SelectList(_DOCService.GetJEPDocumentNo(model.Department, model.Project), "JEPDocumentDetailsId", "DocNo"); //Get DocumentList 
                    model.IsRevision = true;
                    model.IsLatestRevision = true;
                    var Roles = CommonService.objClsLoginInfo.UserRoles;
                    model.IsDocDownloadRights = _DOCService.GetDownloadRigts(Roles, Models.SessionMgtModel.ProjectFolderList.RoleGroup, Id, model.GetCurrentUser);  
                    ViewBag.GetPolicySteps = _DOCService.GetUserLeavel(model.RoleGroup, Roles, Id);
                    ViewBag.GetLifeCycle = _DOCService.GetPolicySteps(ObjectName.DOC.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, Id);
                    return View("Add", model);
                }
                else
                {
                    TempData["Info"] = "Related Document Is not released yet in DCR";
                    return View("Index");
                }
            }
            else
            {
                DOCModel model = _DOCService.GetDocumentByID(Id);
                model.CurrentLocationIp = ipLocation.Ip;
                model.FilePath = ipLocation.File_Path;
                model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo; // Get PRoject No. For generat  Doc No.
                model.GetCurrentUser = CommonService.objClsLoginInfo.UserName;
                ViewBag.GetDocumentList = new SelectList(_CommonService.GetLookupData(LookupType.DOCType.ToString()), "Id", "Lookup", model.DocumentTypeId);
                ViewBag.GetJEPDocumentNo = new SelectList(_DOCService.GetJEPDocumentNo(model.Department, model.Project), "JEPDocumentDetailsId", "DocNo"); //Get DocumentList 
                ViewBag.GetDocumentNoAsJEP = model.DocumentNo;
                var Roles = CommonService.objClsLoginInfo.UserRoles;
                ViewBag.GetPolicySteps = _DOCService.GetUserLeavel(model.RoleGroup, Roles, Id);
                ViewBag.GetLifeCycle = _DOCService.GetPolicySteps(ObjectName.DOC.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, Id);
                model.IsEdit = true;
                //this is for is user can see next tab or not 
                var psno = CommonService.objClsLoginInfo.UserName;
                if (model.DocumentId != null)
                {
                    string errorMsg;
                    bool IsChekIn = _DOCService.IsCheckInDOC(psno, model.DocumentId,out errorMsg);

                    if (IsChekIn != false)
                    {
                        model.IsDOCCheckIn = false;
                    }
                    else
                    {
                        TempData["Info"] = errorMsg;
                        return RedirectToAction("Index");
                    }


                }
                bool HasPsNo = true;
                bool UpdateCheckIN = _DOCService.UpdateIsCheckInDOC(Id, HasPsNo);
                model.IsProjectRights = Models.SessionMgtModel.ProjectFolderList.IsProjectRight;
                model.IsDocDownloadRights = _DOCService.GetDownloadRigts(Roles, Models.SessionMgtModel.ProjectFolderList.RoleGroup, Id, model.GetCurrentUser);
                return View("Add", model);
            }
        }

        public ActionResult Detail(Int64 Id)
        {

            DOCModel docModel = _DOCService.GetDocumentByID(Id);
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            docModel.IsProjectRights = Models.SessionMgtModel.ProjectFolderList.IsProjectRight;
            var psno = CommonService.objClsLoginInfo.UserName;
            string errorMsg = "";
            #region IsCheckIn
            bool IsChekIn = _DOCService.IsCheckInDOC(psno, Id,out errorMsg);
            if (IsChekIn != false)
            {
                docModel.IsDOCCheckIn = false;

            }
            else
            {
                TempData["Info"] = errorMsg;
                return RedirectToAction("Index");
            }

            #endregion


            docModel.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            docModel.GetCurrentUser = CommonService.objClsLoginInfo.UserName;

            docModel.CurrentLocationIp = ipLocation.Ip;
            docModel.FilePath = ipLocation.File_Path;

            ViewBag.GetDocumentList = new SelectList(_CommonService.GetLookupData(LookupType.DOCType.ToString()), "Id", "Lookup", docModel.DocumentTypeId);
            ViewBag.GetJEPDocumentNo = new SelectList(_DOCService.GetJEPDocumentNo(docModel.Department, docModel.Project), "DocNo", "DocNo"); //Get DocumentList 
            #region Policy

            var Roles = CommonService.objClsLoginInfo.UserRoles;
            //ViewBag.GetActivePolicy = _DOCService.GetUserLeavel(docModel.RoleGroup, Roles, Id).FirstOrDefault();
            ViewBag.GetPolicySteps = _DOCService.GetUserLeavel(docModel.RoleGroup, Roles, Id);
            ViewBag.GetLifeCycle = _DOCService.GetPolicySteps(ObjectName.DOC.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, Id);

            if (ViewBag.GetPolicySteps.Count > 0)
            {
                if (ViewBag.GetPolicySteps[0].PolicyOrderID == 1)
                {
                    ViewBag.IsFirstStep = ViewBag.GetPolicySteps[0].PolicyOrderID;
                }
            }

            if (Models.SessionMgtModel.ProjectFolderList.RoleGroup != null)
            {
                docModel.IsDocDownloadRights = _DOCService.GetDownloadRigts(Roles, Models.SessionMgtModel.ProjectFolderList.RoleGroup, Id, docModel.CurrentPsNo);
            }
            else
            {
                var getRoleGroupByPsNo = _DOCService.GetRoleGroupByPSNo(CommonService.objClsLoginInfo.UserName);
                docModel.IsDocDownloadRights = _DOCService.GetDownloadRigts(Roles, getRoleGroupByPsNo, Id, docModel.CurrentPsNo);
            }

            #endregion


            docModel.IsEdit = true;
            docModel.DocumentRevisionStr = "R" + docModel.DocumentRevision;
            return View(docModel);
        }
        public ActionResult DOCLifeCycle(Int64 Id)
        {
            string errorMsg = "";
            string infoMsg = "";
            string armMsg = "";
            string Qstring = string.Empty;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? status = _DOCMappingService.UpdateDOCPolicy(Id, Project, out errorMsg, out infoMsg, out armMsg);

            if (!string.IsNullOrEmpty(errorMsg))
            {
                TempData["Info"] = errorMsg;
            }
            else if (status > 0 && !string.IsNullOrEmpty(infoMsg))
            {
                TempData["Success"] = infoMsg;
                if (!string.IsNullOrEmpty(armMsg))
                {
                    TempData["Info"] = armMsg;
                }
            }

            if (status != null)
            {
                Qstring = CommonService.Encrypt("Id=" + status);
            }
            return RedirectToAction("Detail", new { q = Qstring });

        }
        [HttpPost]
        public ActionResult Detail(DataTableParamModel parm, Int64? DocId, string VersionGUID, string currentUser)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            bool IsLatest = true; // To get Latest Revison of Document
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _DOCMappingService.GetDocumentMappingList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, DocId, IsLatest, VersionGUID, currentUser, Roles, out recordsTotal, out errorMsg);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });

        }

        // Document Delete DOC
        public ActionResult Delete(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _DOCService.Delete(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = " file deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  // Document Add/Edit
        public ActionResult AddEditDoc(DOCModel model)
        {
            string errorMsg = "";
            model.BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            model.Location = Convert.ToInt32(_CommonService.GetLocation().FirstOrDefault().Value);
            var psno = CommonService.objClsLoginInfo.UserName;
            if (model.DocumentId != null && model.IsRevision != true)
            {
                bool IsChekIn = _DOCService.IsCheckInDOC(psno, model.DocumentId,out errorMsg);

                if (IsChekIn != true)
                {
                    return Json(new { Info = true, Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
            }

            Int64 NewDocID = _DOCService.AddEdit(model, out errorMsg);
            if (NewDocID > 0)
            {

                if (model.DocumentId > 0 && model.IsRevision == false)
                {
                    return Json(new { model, Status = true, Msg = "Document Update successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.OldDocIDForRevision = model.DocumentId; //For OldDocId
                    model.DocumentId = NewDocID; // For new DocID


                    if (model.IsRevision == true)
                    {
                        var getDocMappingData = _DOCMappingService.GetDocumentListByDocId(model.OldDocIDForRevision); //Get Data From Doc-Mapping By Old Doc ID 
                        model.NewDocumentPathForRevison = new List<string>();
                        foreach (var docMappingData in getDocMappingData)
                        {
                            string getDocuemtnUniqNamewith = docMappingData.MainDocumentPath; //GetDocument Path
                            model.NewDocumentPathForRevison.Add(getDocuemtnUniqNamewith);

                        }
                        return Json(new { model, Status = true, Msg = "Document-Revesion added successfully1" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { model, Status = true, Msg = "Document added successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CheckDocumentNo(string DocumentNo, Int64? DocumentId, string Project, int? DocumentRevision)
        {
            return Json(!_DOCService.CheckDocumentNo(DocumentNo, DocumentId, Project, DocumentRevision), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Document File Mapping
        [HttpPost]//Document File Mapping Add/Edit
        public JsonResult CreateDocumentMapping(DOCFileModel model)
        {
            string errorMsg = "";
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            model.CurrentLocationIp = ipLocation.Ip;
            model.FileLocation = ipLocation.Location;
            Int64 status = _DOCMappingService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.DocumentMappingId > 0)
                {
                    return Json(new { Status = true, Msg = "Document updated successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.DocumentId = status;
                    return Json(new { model, Status = true, Msg = "Document uploaded successfully" }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        // Document File Mapping Delete
        public JsonResult DeleteDocFile(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _DOCMappingService.DeleteDocFile(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = " Document deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  //On Check box change Status
        public JsonResult UpdateMainFileStatus(DOCFileModel model)
        {
            string errorMsg = "";
            Int64 status = _DOCMappingService.UpdateIsMainFileStatus(model, out errorMsg);
            if (status > 0)
            {
                if (model.IsMainDocument != true)
                {
                    return Json(new { Status = true, Msg = "Main document unassigned successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = true, Msg = "Main document assigned successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateDataInLifCycle(int DocumentID)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string errorMsg = "";

            var psno = CommonService.objClsLoginInfo.UserName;
            if (DocumentID != null)
            {
                bool IsChekIn = _DOCService.IsCheckInDOC(psno, DocumentID,out errorMsg);

                if (IsChekIn != true)
                {
                    return Json(new { Info = true, Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
            }

            Int64? role = _DOCMappingService.UpdateDOCLifeCycle(DocumentID, Project, out errorMsg);
            if (role > 0)
            {
                string rurl = string.Empty;
                if (Models.SessionMgtModel.ProjectFolderList.FolderId != null)
                {
                    rurl = "/DES/Project/Folder?q=" + CommonService.Encrypt("Id=" + Models.SessionMgtModel.ProjectFolderList.FolderId);
                }
                else
                {
                    rurl = "/DES/DOC/Detail?q=" + CommonService.Encrypt("Id=" + role);
                }
                TempData["Success"] = "Document is submitted for approval";
                return Json(new { Project = "", Status = true, Msg = "Document is submitted for approval", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult AddCheckedInOut(CheckeINOUT checkIn)
        {
            string infoMsg = "";
            //if (checkIn.CheckedIn_Remark != null)
            //{
            Int64 status = _DOCMappingService.AddCheckedInOUT(checkIn, out infoMsg);
            if (status > 0)
            {
                return Json(new { infoMsg, Status = true, Msg = "CheckOut completed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { model, Status = true, Msg = "Data added successfully" }, JsonRequestBehavior.AllowGet);
            }
            //}
            //else
            //{
            //    return Json(new { infoMsg, Status = true, Msg = }, JsonRequestBehavior.AllowGet);
            //}
        }

        [HttpPost]  // On CheckOut
        public JsonResult UpdateFileOnCheckOutMark(DOCFileModel checkIn)
        {
            string errorMsg = "";
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            checkIn.CurrentLocationIp = ipLocation.Ip;
            checkIn.FileLocation = ipLocation.Location;
            checkIn.IsFileFromAnotherDropzone = true;
            Int64 status = _DOCMappingService.UpdateFileOnCheckOutMark(checkIn);

            if (status > 0)
            {
                return Json(new { Status = true, Msg = "CheckIn completed" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddReturnRemarks(DOCModel addReturnRemark)
        {
            string errorMsg = "";
            string RedirectAction = string.Empty;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? status = _DOCMappingService.ReviseDOCPolicy(addReturnRemark.ReturnNotes, addReturnRemark.DocumentId, Project, out errorMsg);

            if (status > 0)
            {
                RedirectAction = "/DES/DOC/Detail/Id?q=" + CommonService.Encrypt("Id=" + status);
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    TempData["Info"] = errorMsg;
                    return Json(new { RedirectAction, Status = true, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["Success"] = "Document return successfully";
                    return Json(new { RedirectAction, Status = true, Msg = "Document return successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult UpdateDOCRemark(DOCFileModel model)
        {
            string errorMsg = "";
            Int64 status = _DOCMappingService.UpdateDOCRemark(model);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Remark has been updated successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]//Document File Mapping Add/Edit
        public ActionResult GetAllRevision(DOCModel docId)
        {
            ViewBag.DocumentNo = docId.DocumentNo;
            return PartialView("_partialRevisionByDOC");

        }

        [HttpPost]
        public ActionResult GetAllRevisions(DataTableParamModel parm, string DocumentNo)
        {
            //bool isAll = true;
            int recordsTotal = 0;
            string project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _DOCService.GetDocumentListByDOCNO(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, project, DocumentNo, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]//Document File Mapping Add/Edit
        public ActionResult GetAllVersion(DOCFileModel docMapping)
        {
            ViewBag.VersionGUID = docMapping.VersionGUID;
            ViewBag.DocumentId = docMapping.DocumentId;
            ViewBag.UserId = CommonService.objClsLoginInfo.UserName;
            return PartialView("_partialVersionByDOC");

        }

        [HttpPost]
        public ActionResult GetAllVersions(DataTableParamModel parm, Int64? DocId, string VersionGUID, string currentUser)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            bool IsLatest = false; // To get All Revison of Document
            var list = _DOCMappingService.GetDocumentMappingList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, DocId, IsLatest, VersionGUID, currentUser, Roles, out recordsTotal, out errorMsg);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]//Document File Mapping Add/Edit
        public ActionResult GetAllHistory(DOCModel docId)
        {
            ViewBag.DocumentId = docId.DocumentId;
            return PartialView("_partialDOCHistory");
        }

        [HttpPost]
        public ActionResult GetAllDOCVersions(DataTableParamModel parm, Int64? DocId)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            var list = _DOCService.GetDocumentHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, DocId, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }




        ////Fill Gird
        //[HttpPost]
        //public ActionResult GetPendingDoc(DataTableParamModel parm, string RoleGroup, string Role)
        //{
        //    int recordsTotal = 0;
        //    var psno = CommonService.objClsLoginInfo.UserName;
        //    string Roles = CommonService.objClsLoginInfo.UserRoles;

        //    var list = _DOCService.GetPendingDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, psno, Roles, out recordsTotal);


        //    return Json(new
        //    {
        //        Echo = parm.sEcho,
        //        iTotalRecord = recordsTotal,
        //        iTotalDisplayRecords = recordsTotal,
        //        data = list
        //    });
        //}


        //[HttpPost]  //On Check box change Status
        //public JsonResult MASKLifeCycleDOC(GetListOfDocumentId DocIds)
        //{
        //    List<string> errorMsg = new List<string>();
        //    List<string> infoMsg = new List<string>();
        //    var psno = CommonService.objClsLoginInfo.UserName;
        //    string Roles = CommonService.objClsLoginInfo.UserRoles;
        //    var data = _DOCService.GetMassPromoteUserPolicy(psno, Roles, DocIds.BtnText.Trim());
        //    var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
        //    Int64? status = 0;
        //    if (data != null)
        //    {
        //        foreach (var item in data)
        //        {
        //            if (DocIds.DocumentId.Contains(item.DocumentId))
        //            {
        //                string emsg, imsg;

        //                status = _DOCMappingService.UpdateDOCPolicy(item.DocumentLifeCycleId, Project, out emsg, out imsg);
        //                if (!string.IsNullOrEmpty(emsg))
        //                    errorMsg.Add(emsg);
        //                if (!string.IsNullOrEmpty(imsg))
        //                    infoMsg.Add(imsg);
        //            }
        //        }
        //        if (errorMsg != null && errorMsg.Count > 0)
        //        {
        //            TempData["Error"] = MvcHtmlString.Create(string.Join("<br />", errorMsg));
        //        }
        //        if (infoMsg != null && infoMsg.Count > 0)
        //        {
        //            TempData["Success"]= MvcHtmlString.Create(string.Join("<br />", infoMsg));
        //        }
        //        return Json(new { Status = true, Msg = "Mass " + DocIds.BtnText + " successfully" }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(new { Status = false, Msg = "Data not found" }, JsonRequestBehavior.AllowGet);
        //    }
        //}


        //[HttpPost]  //On Check box change Status
        //public JsonResult ReturnMASKDOC(GetListOfDocumentId DocIds)
        //{
        //    string errorMsg = "";
        //    var UserName = new clsManager().GetUserNameFromPsNo(CommonService.objClsLoginInfo.UserName);
        //    var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
        //    Int64? status = 0;
        //    foreach (var item in DocIds.DocumentId)
        //    {
        //        status = _DOCMappingService.ReviseDOCPolicy(DocIds.BtnText, item, Project, out errorMsg);
        //    }
        //    if (status != null)
        //    {
        //        return Json(new { Status = true, Msg = "Document return successfully" }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(errorMsg))
        //            return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
        //        else
        //            return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //Fill Gird
        [HttpPost]
        public JsonResult GetJEPData(DOCModel model)
        {
            string errorMsg = "";
            DOCModel modelData = _DOCService.GetJEPData(model.JEPDocumentDetailsId);
            if (modelData != null)
            {
                return Json(new { modelData, Status = true, Msg = "JEP data get Successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong while getting JEP data try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Fill Gird
        [HttpPost]
        public JsonResult NewRevision(DOCFileModel addDocMapp)
        {
            string errorMsg = "";
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            addDocMapp.FileLocation = ipLocation.Location;
            bool modelData = _DOCMappingService.AddRevision(addDocMapp, out errorMsg);
            if (modelData)
            {
                bool existINJEP = _DOCService.IsExistINJEP(addDocMapp);
                return Json(new { modelData, Status = true, Msg = "Document-Revesion added successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong while getting JEP data try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateDOCCheckOut(int DocumentID)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string errorMsg = "";

            var psno = CommonService.objClsLoginInfo.UserName;
            if (DocumentID != null)
            {
                bool HasPsNo = false;
                bool IsChekIn = _DOCService.UpdateIsCheckInDOC(DocumentID, HasPsNo);
                return Json(new { Status = true, Msg = "Document has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}