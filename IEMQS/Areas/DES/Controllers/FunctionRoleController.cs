﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    public class FunctionRoleController : Controller
    {
        IFunctionRoleService _FunctionRoleService;

        public FunctionRoleController(IFunctionRoleService FunctionRoleService)
        {
            _FunctionRoleService = FunctionRoleService;
        }

        // GET: DES/FunctionRole
        public ActionResult Index()
        {
            ViewBag.FunctionList = JsonConvert.SerializeObject(_FunctionRoleService.GetTypeList());
            ViewBag.RoleList = JsonConvert.SerializeObject(_FunctionRoleService.GetRoleList());
            return View();
        }

        #region Function Role Mapping

        [HttpPost]
        public ActionResult GetFunctionRoleList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _FunctionRoleService.GetFunctionRoleList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetTypeList(DataTableParamModel parm)
        {
            var list = _FunctionRoleService.GetTypeList();

            return Json(new
            {
                Echo = parm.sEcho,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetRoleList(DataTableParamModel parm)
        {
            var list = _FunctionRoleService.GetRoleList();

            return Json(new
            {
                Echo = parm.sEcho,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddFunctionRole(FunctionRoleModel model, string RoleList)
        {
            string errorMsg = "";
            bool status = _FunctionRoleService.AddEdit(model, RoleList, out errorMsg);
            if (status)
            {
                if (model.FunctionRoleMappingId > 0)
                    return Json(new { Status = true, Msg = "Function Role updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Function Role added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteFunctionRole(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _FunctionRoleService.DeleteFunctionRole(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Function Role deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetRoleByFunctionId(string FunctionId)
        {
            List<FunctionRoleModel> _lstRole = _FunctionRoleService.GetAllRolebyFunctionId(FunctionId);
            string strRole = "";
            foreach (var obj in _lstRole)
            {
                if (strRole == "")
                    strRole = obj.RoleId + "";
                else
                    strRole += "," + obj.RoleId;
            }
            return Json(new { Status = false, Msg = strRole }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}