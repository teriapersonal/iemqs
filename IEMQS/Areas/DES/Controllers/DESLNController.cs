﻿using IEMQS.Areas.DES.Models;
using IEMQS.DESServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IEMQS.Areas.DES.Controllers
{
    [CustomHandleError]
    public class DESLNController : ApiController
    {
        // GET: api/LN
        [Route("api/DESLN/Test")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/DESLN/TestPost")]
        [HttpPost]
        public IEnumerable<string> Post()
        {
            return new string[] { "Postvalue1", "Postvalue2" };
        }


        [Route("api/DESLN/AddProject")]
        [HttpPost]
        public ResultClass ApiProjectData([FromBody]ApiProjectModel model)
        {
            if (ModelState.IsValid)
            {
                string errorMsg = "";
                var status = DESProjectApiService.Add(model, out errorMsg);
                if (status)
                {
                    return new ResultClass { Status = true, Message = "Project Created successfully" };
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMsg))
                        return new ResultClass { Status = false, Message = "something went wrong try again later" };
                    else
                        return new ResultClass { Status = true, Message = errorMsg };
                }
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        [Route("api/DESLN/UpdateProject")]
        [HttpPost]
        public ResultClass ApiUpdateProjectData([FromBody]ApiProjectModel model)
        {
            if (ModelState.IsValid)
            {
                string errorMsg = "";
                var status = DESProjectApiService.Update(model, out errorMsg);
                if (status)
                {
                    return new ResultClass { Status = true, Message = "Project Updated successfully" };
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMsg))
                        return new ResultClass { Status = false, Message = "something went wrong try again later" };
                    else
                        return new ResultClass { Status = true, Message = errorMsg };
                }
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }


        [Route("api/DESLN/AddPart")]
        [HttpPost]
        public ResultClass ApiPartDeliverablesData([FromBody]ApiPartDeliverablesModel model)
        {
            if (ModelState.IsValid)
            {
                string errorMsg = "";
                var status = DESApiPartDeliverablesService.Add(model, out errorMsg);
                if (status)
                {
                    if (!string.IsNullOrEmpty(errorMsg))
                        return new ResultClass { Status = false, Message = errorMsg };
                    else
                        return new ResultClass { Status = true, Message = "Part Created successfully" };
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMsg))
                        return new ResultClass { Status = false, Message = "something went wrong try again later" };
                    else
                        return new ResultClass { Status = false, Message = errorMsg };
                }
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        [Route("api/DESLN/AddBOM")]
        [HttpPost]
        public ResultClass ApiCreateBOMData([FromBody]ApiCreateBOMModel model)
        {
            if (ModelState.IsValid)
            {
                string errorMsg = "";
                var status = DESApiCreateBOMService.Add(model, out errorMsg);
                if (status)
                {
                    if (model.action.Trim() == "ADD")
                        return new ResultClass { Status = true, Message = "BOM created successfully" };
                    else
                        return new ResultClass { Status = true, Message = "BOM updated successfully" };
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMsg))
                        return new ResultClass { Status = false, Message = "something went wrong try again later" };
                    else
                        return new ResultClass { Status = false, Message = errorMsg };
                }
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        public class ResultClass
        {
            public bool Status { get; set; }
            public string Message { get; set; }
        }
    }
}
