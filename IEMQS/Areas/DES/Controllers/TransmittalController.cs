﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQS.Areas.DES.Models;
//using Aspose.Email;
using System.IO;
using System.Globalization;
using System.Text;
using IEMQSImplementation;
using System.Reflection;
using System.Net.Mail;
using System.Linq;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class TransmittalController : Controller
    {
        ITransmittalService _TransmittalService;
        IAgencyService _AgencyService;
        public TransmittalController(ITransmittalService transmittalservice, IAgencyService agencyservice)
        {
            _TransmittalService = transmittalservice;
            _AgencyService = agencyservice;
        }
        // GET: DES/Transmittal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            AgencyDocumentModel model = new AgencyDocumentModel();
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            ViewBag.AgencyList = new SelectList(_TransmittalService.AgnecywithEmailTemplate(Project), "AgencyId", "AgencyName");
            return View(model);
        }

        [HttpPost]
        public ActionResult ShowEmailHistory(Int64 TransmittalId)
        {
            ViewBag.TransmittalId = TransmittalId;
            return PartialView("_partialEmailHistory");
        }

        [HttpPost]
        public ActionResult ShowTransmittalDetail(Int64 TransmittalId)
        {
            ViewBag.TransmittalId = TransmittalId;
            return PartialView("_partialTransmittalDetail");
        }

        public ActionResult Edit(Int64 Id)
        {
            AgencyDocumentModel model = new AgencyDocumentModel();
            model = _TransmittalService.GetAgencyDocumentById(Id);
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            ViewBag.AgencyList = new SelectList(_TransmittalService.AgnecywithEmailTemplate(Project), "AgencyId", "AgencyName");
            return View("Add", model);
        }

        [HttpGet]
        public ActionResult TransmittalEdit(Int64 Id, Int64 TransmittalId)
        {
            //AgencyDocModel agencyDetail = _TransmittalService.TransmittalEditDetail(Id);
            //agencyDetail.CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
            //return View("GenerateTransmittal", agencyDetail);
            AgencyDocModel agencyDetail = _TransmittalService.AgencyDetail(Id);
            agencyDetail.CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
            agencyDetail.TransmittalId = TransmittalId;
            ViewBag.selectedAgencyDocIDs = _TransmittalService.GetTransmittalDocList(TransmittalId);
            return View("GenerateTransmittal", agencyDetail);
        }


        [HttpPost]
        public ActionResult GetAgencyDocList(DataTableParamModel parm, Int64? AgencyID)
        {
            if (AgencyID > 0)
            {
                _AgencyService.UpdateIsCheckInAgency(AgencyID, true);
            }
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            int recordsTotal = 0;
            var list = _TransmittalService.GetAgencyDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, AgencyID, out recordsTotal, Project);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }
        [HttpPost]
        public ActionResult GetJEPDocList(DataTableParamModel parm, Int64 AgencyID)
        {
            int recordsTotal = 0;
            var list = _TransmittalService.GetJEPDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, SessionMgtModel.ProjectFolderList.ProjectNo, AgencyID, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetGlobalJEPDocList(DataTableParamModel parm, string Project, string DocumentNo, Int64 AgencyID)
        {
            int recordsTotal = 0;
            var list = _TransmittalService.GetGlobalJEPDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project == null ? "" : Project, DocumentNo == null ? "" : DocumentNo, SessionMgtModel.ProjectFolderList.ProjectNo, AgencyID);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpPost]
        public ActionResult AddAgencyDoc(AgencyDocumentModel model)
        {
            model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            model.FolderId = SessionMgtModel.ProjectFolderList.FolderId;
            model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
 
            string errorMsg = "";
            Int64 status = _TransmittalService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.AgencyDocId > 0)
                    return Json(new { Status = true, Msg = "Agency Document updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Agency Document added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteAgencyDoc(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _TransmittalService.DeleteAgencyDoc(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Agency Document Deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteAgencyWiseAllDoc(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _TransmittalService.DeleteAgencyWiseAllDoc(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Agency Document Deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteTransmittalWiseDoc(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _TransmittalService.DeleteTransmittalWiseDoc(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Transmittal Document Deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult UpdateAgencyDoc(AgencyDocumentModel model, string btnSubmit)
        {
            string errorMsg = "";
            bool status = true;
            //if (model.IsCommentChanges)
            {
                model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                status = _TransmittalService.UpdateAgencyDoc(model, out errorMsg);
            }
            if (status)
            {
                string successMsg = "Agency document updated successfully";
                if (btnSubmit == "Submit")
                {
                    _AgencyService.UpdateIsCheckInAgency(model.AgencyId, false);
                     var project = SessionMgtModel.ProjectFolderList.ProjectNo;
                    bool checkWeight = _TransmittalService.CheckWeight(model.AgencyId, project, out errorMsg);
                    if(checkWeight!=true)
                    {
                        if (!string.IsNullOrEmpty(errorMsg))
                            return Json(new { Status = false, Submitted = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { Status = true, Submitted = true, Msg = successMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = true, Submitted = false, Msg = successMsg }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Submitted = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Submitted = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateEmailFlag(AgencyDocumentModel model)
        {
            bool status = _TransmittalService.UpdateEmailFlag(model);
            if (status)
            {
                return Json(new { Status = true, Msg = "Email flag updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult BindAgnecywiseDocs(DataTableParamModel parm, Int64 AgencyID)
        //{
        //    List<AgencyModel> listagency = new List<AgencyModel>();
        //    listagency = _TransmittalService.AgnecywithEmailTemplate();
        //    ViewBag.AgencyList = Newtonsoft.Json.JsonConvert.SerializeObject(listagency);
        //    return RedirectToAction("BindAgencywideDocs", new
        //    {
        //        ID = AgencyID
        //    });
        //}

        [HttpGet]
        public ActionResult GenerateTransmittal(Int64 AgencyID)
        {
            string errorMsg = "";
            string project = SessionMgtModel.ProjectFolderList.ProjectNo;
             bool checkWeight = _TransmittalService.CheckWeight(AgencyID, project, out errorMsg);
            if (checkWeight != false)
            {
                AgencyDocModel agencyDetail = _TransmittalService.AgencyDetail(AgencyID);
                agencyDetail.CurrentBU = Models.SessionMgtModel.ProjectFolderList.BUId;
                agencyDetail.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
                agencyDetail.CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
                ViewBag.selectedAgencyDocIDs = new List<Int64>();
                return View(agencyDetail);
            }
            else
            {
                TempData["Info"] = errorMsg;
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult GetAgencywiseDocs(DataTableParamModel parm, Int64? AgencyID, Int64? TransmittalId)
        {
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            int recordsTotal = 0;
            var list = _TransmittalService.BindAgencywiseDocs(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, AgencyID, TransmittalId, out recordsTotal, Project);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult BindEmailHistory(DataTableParamModel parm, Int64? TransmittalId)
        {
            int recordsTotal = 0;
            var list = _TransmittalService.BindEmailHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, TransmittalId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }
        [HttpPost]
        public ActionResult BindTransmittalDetail(DataTableParamModel parm, Int64? TransmittalId)
        {
            int recordsTotal = 0;
            var list = _TransmittalService.BindTransmittlDetail(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, TransmittalId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpPost]
        public ActionResult AddTransmittal(TransmittalModel model)
        {
            string errorMsg = "", actionName = "", newGeneratedTransmittalNo = "";
            List<string> PathList;
            if (model.TransmittalId != 0)
            {
                actionName = "Edit";
            }
            else
            {
                actionName = "Add";
            }
            model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            model.FolderId = SessionMgtModel.ProjectFolderList.FolderId;
            model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
            Int64 status = _TransmittalService.AddTransmittal(model, out PathList, out newGeneratedTransmittalNo, out errorMsg);
            if (status > 0)
            {
                if (model.Status == "Created")
                {
                    return Json(new { Status = true, DocPathList = PathList, TransmittalNo = newGeneratedTransmittalNo, Msg = "Transmittal Generated Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else if (actionName == "Edit")
                {
                    return Json(new { Status = true, Msg = "Transmittal Updated Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = true, Msg = "Transmittal Saved as Draft successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, TransmittalNo = newGeneratedTransmittalNo, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, TransmittalNo = newGeneratedTransmittalNo, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("GenerateTransmittal");
        }


        [HttpPost]
        public ActionResult GetTransmittalAgency(DataTableParamModel parm)
        {
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            long? FolderId = SessionMgtModel.ProjectFolderList.FolderId;
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            int recordsTotal = 0;
            var list = _TransmittalService.GetTransmittalAgencyList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project, FolderId, Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpPost]
        public ActionResult GetGenerateTransmittalList(DataTableParamModel parm)
        {
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            long? FolderId = SessionMgtModel.ProjectFolderList.FolderId;
            int recordsTotal = 0;
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _TransmittalService.GetGenerateTransmittalList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project, FolderId,Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        private void ToEmlStream(MailMessage msg, Stream str, string dummyEmail)
        {
            using (var client = new SmtpClient())
            {
                var id = Guid.NewGuid();

                //var tempFolder = Path.Combine(Path.GetTempPath(), Assembly.GetExecutingAssembly().GetName().Name);

                //tempFolder = Path.Combine(tempFolder, "MailMessageToEMLTemp");

                //// create a temp folder to hold just this .eml file so that we can find it easily.
                //tempFolder = Path.Combine(tempFolder, id.ToString());
                string tempFolder = Server.MapPath("~/Uploads/TransmittalEmails/" + id);
                if (!Directory.Exists(tempFolder))
                {
                    Directory.CreateDirectory(tempFolder);
                }

                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                client.PickupDirectoryLocation = tempFolder;
                client.Send(msg);

                // tempFolder should contain 1 eml file
                var filePath = Directory.GetFiles(tempFolder).Single();

                // create new file and remove all lines that start with 'X-Sender:' or 'From:'
                string newFile = Path.Combine(tempFolder, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_Email.eml");
                using (var sr = new StreamReader(filePath))
                {
                    using (var sw = new StreamWriter(newFile))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (!line.StartsWith("X-Sender:") &&
                                !line.StartsWith("From:") &&
                                // dummy email which is used if receiver address is empty
                                !line.StartsWith("X-Receiver: " + dummyEmail) &&
                                // dummy email which is used if receiver address is empty
                                !line.StartsWith("To: " + dummyEmail))
                            {
                                sw.WriteLine(line);
                            }
                        }
                    }
                }

                // stream out the contents
                using (var fs = new FileStream(newFile, FileMode.Open))
                {
                    fs.CopyTo(str);
                }
            }
        }

        [HttpGet]
        public ActionResult OpenTransmittalEmail(Int64? TransmittalId)
        {


            string dummyEmail = "IEMQS@larsentoubro.com";
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            //var list = _TransmittalService.GetEmailTemplateByAgency(AgencyId, Project)
            var data = _TransmittalService.GetEmailTemplateByAgency(TransmittalId).FirstOrDefault();
            if (data != null)
            {
                List<string> ToEmailMerge = new List<string>();
                if (!string.IsNullOrEmpty(data.AgencyTo))
                {
                    List<string> email = data.AgencyTo.Split(';').ToList();
                    ToEmailMerge.AddRange(email);
                }
                if (!string.IsNullOrEmpty(data.LandTTo))
                {
                    List<string> email = data.LandTTo.Split(';').ToList();
                    ToEmailMerge.AddRange(email);
                }

                List<string> CCEmailMerge = new List<string>();
                if (!string.IsNullOrEmpty(data.LandTCC))
                {
                    List<string> email = data.LandTCC.Split(';').ToList();
                    CCEmailMerge.AddRange(email);
                }
                if (!string.IsNullOrEmpty(data.AgencyCC))
                {
                    List<string> email = data.AgencyCC.Split(';').ToList();
                    CCEmailMerge.AddRange(email);
                }

                //((!string.IsNullOrEmpty(list[0].AgencyTo)) ? list[0].AgencyTo + ";" : "") + ((!string.IsNullOrEmpty(list[0].LandTTo)) ? list[0].LandTTo + ";" : "");
                //string CCEmailMerge = ((!string.IsNullOrEmpty(list[0].LandTCC)) ? list[0].LandTCC + ";" : "") + ((!string.IsNullOrEmpty(list[0].AgencyCC)) ? list[0].AgencyCC + ";" : "");

                //Microsoft.Office.Interop.Outlook.Application objOutlook = new Microsoft.Office.Interop.Outlook.Application();

                // Creating a new Outlook message from the Outlook Application instance
                //Microsoft.Office.Interop.Outlook.MailItem msgInterop = (Microsoft.Office.Interop.Outlook.MailItem)(objOutlook.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem));

                // Set recipient information
                // msgInterop.To = ToEmailMerge;
                // msgInterop.CC = CCEmailMerge;

                // Set the message subject
                // msgInterop.Subject = list[0].Subject;

                // Set some HTML text in the HTML body
                StringBuilder sb = new StringBuilder(data.EmailBody);
                sb.Replace("@@PONO", data.PONO);
                sb.Replace("@@Owner", new clsManager().GetUserNameFromPsNo((data.Owner)));
                sb.Replace("@@@Project", data.ProjectDescription);
                sb.Replace("@@Purchase", data.Purchaser);
                sb.Replace("@@ProjectNo", data.LntProjectNo);
                sb.Replace("@@TransmittalNo", data.TransmittalNo);

                // msgInterop.HTMLBody = sb.ToString();

                // Save the MSG file in local disk
                //string strDate = DateTime.Now.ToString("dd-MM-yyyy");
                //string strHH = DateTime.Now.ToString("HH");
                //string strMM = DateTime.Now.ToString("mm");
                //string strSS = DateTime.Now.ToString("ss");
                //string Path = list[0].AgencyName.Trim() + "_EmailTemplate_" + strDate + "_" + strHH + "_" + strMM + "_" + strSS;
                //string strMsg = Server.MapPath(@"\Uploads\" + Path + ".msg");
                //msgInterop.SaveAs(strMsg, Microsoft.Office.Interop.Outlook.OlSaveAsType.olMSG);
                //return File(strMsg, "application/vnd.ms-outlook", "" + list[0].AgencyName + "_EmailTemplate.msg");



                var mailMessage = new MailMessage();

                mailMessage.From = new MailAddress(dummyEmail);
                if (ToEmailMerge != null)
                    foreach (string email in ToEmailMerge)
                        mailMessage.To.Add(email);
                if (CCEmailMerge != null)
                    foreach (string email in CCEmailMerge)
                        mailMessage.CC.Add(email);
                mailMessage.Subject = data.Subject;
                mailMessage.Body = sb.ToString();
                mailMessage.IsBodyHtml = true;

                // mark as draft
                mailMessage.Headers.Add("X-Unsent", "1");

                // download image and save it as attachment
                //using (var httpClient = new HttpClient())
                //{
                //    var imageStream = await httpClient.GetStreamAsync(new Uri("http://dcaric.com/favicon.ico"));
                //    mailMessage.Attachments.Add(new Attachment(imageStream, "favicon.ico"));
                //}

                var stream = new MemoryStream();
                ToEmlStream(mailMessage, stream, dummyEmail);

                stream.Position = 0;

                return File(stream, "message/rfc822", data.TransmittalNo + ".eml");
            }
            return null;
            //Running Code (Aspose Email dll)
            //MailMessage msg = new MailMessage();
            //msg.To = ToEmailMerge;
            //msg.CC = CCEmailMerge;
            //msg.Subject = list.Subject;
            //msg.HtmlBody = list.EmailBody;

            //string strDate = DateTime.Now.ToString("dd-MM-yyyy");
            //string strHH = DateTime.Now.ToString("HH");
            //string strMM = DateTime.Now.ToString("mm");
            //string strSS = DateTime.Now.ToString("ss");
            //string strMsg = Server.MapPath(@"\Uploads\" + list.AgencyName + "_EmailTemplate_" + strDate + "_" + strHH + "_" + strMM + "_" + strSS + ".msg");
            //msg.Save(strMsg, SaveOptions.DefaultMsg);


            ////var fs_Final_Download = new FileStream(strMsg, FileMode.Open);
            //return File(strMsg, "application/vnd.ms-outlook", "" + list.AgencyName + "_EmailTemplate.msg");
        }

        [HttpPost]
        public ActionResult AddEmailHistory(TransmittalEmailModel model)
        {

            Int64 status = _TransmittalService.AddEmailHistory(model);
            if (status > 0)
            {
                return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateAgencyCheckOut(Int64 AgencyId)
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            if (AgencyId > 0)
            {
                bool IsChekIn = _AgencyService.UpdateIsCheckInAgency(AgencyId, false);
                return Json(new { Status = true, Msg = "Agency has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult GetDownloadFiles(TransmittalModel model)
        {
            List<string> DocPathList = _TransmittalService.GetDownloadFiles(model.TransmittalId);
            var CurrentBU = Models.SessionMgtModel.ProjectFolderList.BUId;
            var CurrentProject = SessionMgtModel.ProjectFolderList.ProjectNo;

            if (DocPathList.Count > 0)
            {
                string LocationIp = CommonService.GetUseIPConfig.Ip;
                return Json(new { DocPathList = DocPathList, TransmittalNo = model.TransmittalNo, BU = CurrentBU, Project = CurrentProject, AgencyId = model.AgencyId, CurrentLocationIp = LocationIp, Status = true, Msg = "Agency has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { Status = false, TransmittalNo = model.TransmittalNo, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetWeightage(Int64? AgencyId)
        {
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            List<Tuple<Int64, double>> list = _TransmittalService.GetWeightage(AgencyId, Project);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}