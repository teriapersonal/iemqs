﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.DES.Models;
using System.IO;
using System.Text.RegularExpressions;
//using Microsoft.Office.Interop;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class AgencyEmailController : Controller
    {
        //IBUService _BUService;
        IAgencyService _AgencyService;
        IAgencyEmailService _AgencyEmailService;

        public AgencyEmailController(IAgencyService AgencyService, IAgencyEmailService AgencyEmailService)
        {
            //_BUService = BUService;
            _AgencyService = AgencyService;
            _AgencyEmailService = AgencyEmailService;
        }

        // GET: DES/AgencyEmail
        public ActionResult Index()
        {
            return View();
        }

        #region Agency Transmittal Email Configuration

        public ActionResult AgencyEmail()
        {
            string body = string.Empty;

            try
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Areas/DES/HTMLMailTemplate/SampleEmailTemplate.html")))
                {
                    //model.EmailBody = reader.ReadToEnd();
                    body = reader.ReadToEnd();
                }
            }
            catch (Exception)
            {
                throw;
            }


            //Microsoft.Office.Interop.Outlook.Application oApp = new Microsoft.Office.Interop.Outlook.Application();
            //Microsoft.Office.Interop.Outlook.MailItem oMsg = (Microsoft.Office.Interop.Outlook.MailItem)oApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);

            //oMsg.Subject = "something deployment package instructions";
            //oMsg.BodyFormat = OlBodyFormat.olFormatHTML;
            //oMsg.HTMLBody = //Here comes your body;
            //oMsg.Display(false); //In order to display it in modal inspector change the argument to true


            AgencyEmailModel model = new AgencyEmailModel();
            model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            //model.Subject = "Your subject write here.....";
            model.EmailBody = body;
            ViewBag.AgencyList = new SelectList(_AgencyService.GetAgencybyBU(Convert.ToInt64(SessionMgtModel.ProjectFolderList.BUId)), "AgencyId", "AgencyName");
            return View(model);

        }

        [HttpPost]
        public ActionResult GetAgencyEmailList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            string Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _AgencyEmailService.GetAgencyEmailList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AgencyEmail(AgencyEmailModel model)
        {
            Int64 status = _AgencyEmailService.AddEdit(model);
            if (status > 0)
            {
                if (model.TransmittalEmailId > 0)
                    TempData["Success"] = "Transmittal Email updated successfully";
                else
                    TempData["Success"] = "Transmittal Email added successfully";
            }
            else
            {
                TempData["Error"] = "something went wrong try again later";
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditAgencyEmail(Int64 Id)
        {
            AgencyEmailModel model = new AgencyEmailModel();

            ViewBag.AgencyList = new SelectList(_AgencyService.GetAgencybyBU(Convert.ToInt64(SessionMgtModel.ProjectFolderList.BUId)), "AgencyId", "AgencyName");

            if (Id > 0)
            {
                model = _AgencyEmailService.GetAgencyEmailbyId(Id);
                if (model == null)
                    model = new AgencyEmailModel();

            }
            return View("AgencyEmail", model);
        }

        [HttpGet]
        public ActionResult DeleteAgencyEmail(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _AgencyEmailService.DeleteAgencyEmail(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Transmittal Email deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CheckAgencyEmailAlreadyExist(Int64? AgencyId, Int64? TransmittalEmailId, string Project)
        {
            return Json(!_AgencyEmailService.CheckAgencyEmailAlreadyExist(AgencyId, Project, TransmittalEmailId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckEmailDomain(string LandTTo, string LandTCC)
        {
            string EmailId = string.Empty;
            if (!string.IsNullOrEmpty(LandTTo))
            {
                EmailId = LandTTo;
            }
            else if(!string.IsNullOrEmpty(LandTCC))
            {
                EmailId = LandTCC;
            }

            string errmsg = string.Empty;
            bool isEmail = Regex.IsMatch(EmailId, "^(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+\\s?,)*(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+)$", RegexOptions.IgnoreCase);
            if (isEmail)
            {
                bool strContains = EmailId.ToLower().Contains("@larsentoubro.com");
                if (!strContains)
                {
                    errmsg = "Enter valid L&T email id ending with '@larsentoubro.com'";
                    return Json(errmsg, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                errmsg = "Enter valid Email-Id";
                return Json(errmsg, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        
        #endregion
    }
}