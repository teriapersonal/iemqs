﻿using ClosedXML.Excel;
using Excel;
using IEMQS.DESCore;
using IEMQS.DESServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace IEMQS.Areas.DES.Controllers
{
    public class PartImportController : Controller
    {
        IPartService _PartService;
        public PartImportController(IPartService PartService)
        {
            _PartService = PartService;
        }

        private const string colItemKeyType = "Item Key Type";
        private const string colOrderPolicy = "Order Policy";
        private const string colProductForm = "Product Form";
        private const string colSubProductForm = "Sub Product Form";
        private const string colType = "Type";
        private const string colItemId = "Item Id";
        private const string colDrgNo = "Drg No";
        private const string colParentItem = "Parent Item";
        private const string colBOMItemId = "Item Id";
        private const string colFindNo = "Find No";
        private const string colExtFindNo = "Ext Find No";
        private const string colFindNoDesc = "Find No Desc";
        private const string colActionforPCRelation = "Action for PC Relation";
        private const string colActionforPart = "Action for Part";
        private const string colMaterial = "Material";
        private const string colItemGroup = "Item Group";
        private const string colItemDescription = "Item Description";
        private const string colProcurementDrg = "Procurement Drg";
        private const string colString2 = "String2";
        private const string colString3 = "String3";
        private const string colString4 = "String4";
        private const string colARMSet = "ARM Set";
        private const string colARMRev = "ARM Rev";
        private const string colSizeCode = "Size Code";
        private const string colThickness = "Thickness";
        private const string colCladPlatePartThickness1 = "Clad PlatePart Thickness 1";
        private const string colCladSpecificGravity1 = "Clad Specific Gravity 1";
        private const string colIsDoubleClad = "IsDoubleClad";
        private const string colCladPlatePartThickness2 = "Clad PlatePart Thickness 2";
        private const string colCladSpecificGravity2 = "Clad Specific Gravity 2";
        private const string colItemWeightkg = "Item Weight (kg)";
        private const string colBOMWeightkg = "BOM Weight (kg)";
        private const string colCRS = "CRS";
        private const string colJobQty = "Job Qty";
        private const string colCommissioningSpareQty = "Commissioning Spare Qty";
        private const string colMandatorySpareQty = "Mandatory Spare Qty";
        private const string colOperationSpareQty = "Operation Spare Qty";
        private const string colExtraQty = "Extra Qty";
        private const string colQuantity = "Quantity For BOM";
        private const string colLengthmm = "Length (mm)";
        private const string colWidth = "Width";
        private const string colNumberOfPieces = "Number Of Pieces";
        private const string colBOMReportSize = "BOM Report Size";
        private const string colDrawingBOMSize = "Drawing BOM Size";
        private const string colSpecificGravity = "Specific Gravity";
        private const string colUOM = "UOM";
        private const string colWeightFactor = "Weight Factor";
        private const string colRemarks = "Remarks";
        private const string colProduct_Form_ID = "Product_Form_ID";
        private const string colDrg_No_ID = "Drg_No_ID";
        private const string colParent_Item_ID = "Parent_Item_ID";
        private const string colProcurement_Drg_ID = "Procurement_Drg_ID";
        private const string colERROR = "ERROR";
        private const string colHdnBOMReportSize = "HdnBOM Report Size";
        private const string colHdnDrawingBOMSize = "HdnDrawing BOM Size";
        private const string colReviseMsg = "ReviseMsg";
        private const string sBOMRevise = "BOMRevise";
        private const string colItem_Id = "Item_ID";
        private const string colConfirmRevision = "ConfirmRevision";
        private const string colSuccessMsg = "SuccessMsg";

        private const string UploadFilePath = "~/Uploads/PartImport/";

        // GET: DES/PartImport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {

            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                ViewBag.ManufacturingItembyProject = new SelectList(_PartService.GetManufacturingItembyProject(Models.SessionMgtModel.ProjectFolderList.ProjectNo), "ItemId", "ItemKey");
                return View();
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }

        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            string _FileName = "";
            string _path = "";
            DataTable dtPart = null;
            DataTable dtPartCloned = null;
            List<string> listPartImportExcelColumn = new List<string>();
            try
            {
                if (file == null)
                {
                    return Json(new { error = "Please select file." }, JsonRequestBehavior.AllowGet);
                }
                if (file.ContentLength > 0)
                {
                    // Create a directory
                    if (!Directory.Exists(Server.MapPath(UploadFilePath)))
                    {
                        Directory.CreateDirectory(Server.MapPath(UploadFilePath));
                    }
                    _FileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetFileName(file.FileName);
                    _path = Path.Combine(Server.MapPath(UploadFilePath), _FileName);
                    file.SaveAs(_path);
                }
                else
                {
                    return Json(new { error = "Please select file." }, JsonRequestBehavior.AllowGet);
                }

                DataSet result;
                //string path = System.Web.HttpContext.Current.Server.MapPath(_path);

                FileStream stream = System.IO.File.Open(_path, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader;
                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                //...
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                if (Path.GetExtension(_path).ToLower() == ".xlsx")
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                result = excelReader.AsDataSet();

                if (result != null && result.Tables.Count > 0)
                {
                    listPartImportExcelColumn.Add(colItemKeyType);
                    listPartImportExcelColumn.Add(colOrderPolicy);
                    listPartImportExcelColumn.Add(colProductForm);
                    listPartImportExcelColumn.Add(colSubProductForm);
                    listPartImportExcelColumn.Add(colType);
                    listPartImportExcelColumn.Add(colItemId);
                    listPartImportExcelColumn.Add(colDrgNo);
                    listPartImportExcelColumn.Add(colParentItem);
                    listPartImportExcelColumn.Add(colFindNo);
                    listPartImportExcelColumn.Add(colExtFindNo);
                    listPartImportExcelColumn.Add(colFindNoDesc);
                    listPartImportExcelColumn.Add(colActionforPCRelation);
                    listPartImportExcelColumn.Add(colMaterial);
                    listPartImportExcelColumn.Add(colItemGroup);
                    listPartImportExcelColumn.Add(colItemDescription);
                    listPartImportExcelColumn.Add(colProcurementDrg);
                    listPartImportExcelColumn.Add(colString2);
                    listPartImportExcelColumn.Add(colString3);
                    listPartImportExcelColumn.Add(colString4);
                    listPartImportExcelColumn.Add(colARMSet);
                    listPartImportExcelColumn.Add(colARMRev);
                    listPartImportExcelColumn.Add(colSizeCode);
                    listPartImportExcelColumn.Add(colThickness);
                    listPartImportExcelColumn.Add(colCladPlatePartThickness1);
                    listPartImportExcelColumn.Add(colCladSpecificGravity1);
                    listPartImportExcelColumn.Add(colIsDoubleClad);
                    listPartImportExcelColumn.Add(colCladPlatePartThickness2);
                    listPartImportExcelColumn.Add(colCladSpecificGravity2);
                    listPartImportExcelColumn.Add(colItemWeightkg);
                    listPartImportExcelColumn.Add(colBOMWeightkg);
                    listPartImportExcelColumn.Add(colCRS);
                    listPartImportExcelColumn.Add(colJobQty);
                    listPartImportExcelColumn.Add(colCommissioningSpareQty);
                    listPartImportExcelColumn.Add(colMandatorySpareQty);
                    listPartImportExcelColumn.Add(colOperationSpareQty);
                    listPartImportExcelColumn.Add(colExtraQty);
                    listPartImportExcelColumn.Add(colQuantity);
                    listPartImportExcelColumn.Add(colLengthmm);
                    listPartImportExcelColumn.Add(colWidth);
                    listPartImportExcelColumn.Add(colNumberOfPieces);
                    listPartImportExcelColumn.Add(colBOMReportSize);
                    listPartImportExcelColumn.Add(colDrawingBOMSize);
                    listPartImportExcelColumn.Add(colSpecificGravity);
                    listPartImportExcelColumn.Add(colUOM);
                    listPartImportExcelColumn.Add(colWeightFactor);
                    listPartImportExcelColumn.Add(colRemarks);

                    dtPart = result.Tables[0];
                    for (int i = 0; i < dtPart.Columns.Count; i++)
                    {
                        dtPart.Columns[i].ColumnName = dtPart.Rows[0][i].ToString().Replace("*", "").Trim();
                    }

                    dtPartCloned = dtPart.Clone();
                    for (int i = 0; i < dtPart.Columns.Count; i++)
                    {
                        dtPartCloned.Columns[i].DataType = typeof(string);
                    }
                    foreach (DataRow row in dtPart.Rows)
                    {
                        dtPartCloned.ImportRow(row);
                    }
                }
                if (dtPart.Rows.Count >= 1)
                {
                    dtPartCloned.Rows.RemoveAt(0);
                    var listColumns = dtPartCloned.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();
                    if (listPartImportExcelColumn.Where(i => !listColumns.Contains(i)).Any())
                    {
                        //TempData["Error"] = "Your File format is not match with template.";
                        //return RedirectToAction("Add", "PartImport");
                        return Json(new { error = "Your File format is not match with template." }, JsonRequestBehavior.AllowGet);
                    }

                    foreach (string dc in listColumns)
                    {
                        if (!listPartImportExcelColumn.Any(i => dc.ToLower() == i.ToLower()))
                        {
                            dtPartCloned.Columns.Remove(dc);
                            dtPartCloned.AcceptChanges();
                        }
                    }
                    dtPartCloned.Columns.Add(colProduct_Form_ID, typeof(int));
                    dtPartCloned.Columns.Add(colDrg_No_ID, typeof(int));
                    dtPartCloned.Columns.Add(colParent_Item_ID, typeof(int));
                    dtPartCloned.Columns.Add(colProcurement_Drg_ID, typeof(int));
                    if (!dtPartCloned.Columns.Contains(colERROR))
                    {
                        dtPartCloned.Columns.Add(colERROR, typeof(string));
                    }
                    if (dtPartCloned.Columns.Contains("System Info"))
                    {
                        dtPartCloned.Columns.Remove("System Info");
                    }
                    dtPartCloned.Columns.Add(colHdnBOMReportSize, typeof(string));
                    dtPartCloned.Columns.Add(colHdnDrawingBOMSize, typeof(string));

                    DataColumn colRevNo = new DataColumn("RevNo", typeof(int));
                    colRevNo.DefaultValue = 0;
                    dtPartCloned.Columns.Add(colRevNo);

                    DataColumn colReviseWithBOM = new DataColumn("ReviseWithBOM", typeof(bool));
                    colReviseWithBOM.DefaultValue = false;
                    dtPartCloned.Columns.Add(colReviseWithBOM);

                    DataColumn colPolicyId = new DataColumn("PolicyId", typeof(int));
                    colPolicyId.DefaultValue = 0;
                    dtPartCloned.Columns.Add(colPolicyId);
                }

                // File Delete
                if (System.IO.File.Exists(_path))
                {
                    System.IO.File.Delete(_path);
                }
                var ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
                var Contract = Models.SessionMgtModel.ProjectFolderList.Contract;
                string psno = CommonService.objClsLoginInfo.UserName;
                var FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                var Department = CommonService.objClsLoginInfo.Department;
                var RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;

                var dtPartReturn = _PartService.ImportPartAndBOMItem(ProjectNo, Contract, psno, FolderId, Department, RoleGroup, dtPartCloned);

                List<KeyValuePair<string, string>> errorlist = new List<KeyValuePair<string, string>>();

                if (dtPartReturn.Rows.Count > 1)
                {

                    GridView GridView1 = new GridView();

                    GridView1.AllowPaging = false;
                    GridView1.DataSource = dtPartReturn;
                    GridView1.DataBind();
                    for (int i = 0; i < GridView1.Rows.Count; i++)
                    {
                        List<string> listError = dtPartReturn.Rows[i]["Error"].ToString().Split('|').Where(x => !string.IsNullOrEmpty(x)).ToList();
                        var ErrorReplaceText = string.Empty;
                        for (int k = 0; k < listError.Count; k++)
                        {
                            var error = listError[k].Trim();
                            var errorcolumnname = listError[k].Split(' ')[0];
                            string errorcolumn = errorcolumnname.Replace("_", " ");
                            if (error.ToLower() == "success")
                            {
                                ErrorReplaceText = error;

                            }
                            else if (error.ToLower().StartsWith(errorcolumnname.ToLower()))
                            {
                                if (ErrorReplaceText.Length == 0)
                                {
                                    ErrorReplaceText = error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                                else
                                {
                                    ErrorReplaceText += "|" + error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                            }
                            for (int j = 0; j <= (GridView1.HeaderRow.Cells.Count - 1); j++)
                            {
                                if (GridView1.HeaderRow.Cells[j].Text.ToLower() == errorcolumn.ToLower())
                                    GridView1.Rows[i].Cells[j].BackColor = Color.Red;

                                if (GridView1.HeaderRow.Cells[j].Text.ToLower() == "error")
                                {
                                    var listInfo = ErrorReplaceText.Replace("Information :", "").Split('|').Distinct().ToList();
                                    GridView1.Rows[i].Cells[j].Text = string.Join("|", listInfo);
                                }
                            }
                        }

                        if (ErrorReplaceText != null && ErrorReplaceText.ToLower() != "success")
                        {
                            errorlist.Add(new KeyValuePair<string, string>((i + 1).ToString(), ErrorReplaceText));

                        }
                    }

                    using (ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook())
                    {

                        var ws = workbook.Worksheets.Add("Sheet1");
                        for (int l = 0; l < GridView1.HeaderRow.Cells.Count; l++)
                        {
                            ws.Cell(1, l + 1).Value = GridView1.HeaderRow.Cells[l].Text.ToLower() == "error" ? "System Info" : GridView1.HeaderRow.Cells[l].Text;
                            ws.Cell(1, l + 1).Style.Font.Bold = true;

                        }
                        for (int k = 0; k < GridView1.Rows.Count; k++)
                        {
                            for (int i = 0; i < GridView1.Rows[k].Cells.Count; i++)
                            {
                                ws.Cell(k + 2, i + 1).Value = GridView1.Rows[k].Cells[i].Text == "&nbsp;" ? "" : GridView1.Rows[k].Cells[i].Text;
                                if (GridView1.Rows[k].Cells[i].BackColor.ToString() != "Color [Empty]")
                                {
                                    ws.Cell(k + 2, i + 1).Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.Red;
                                }
                            }
                        }

                        string FileName = UploadFilePath + "PartImport" + DateTime.Now.Ticks + ".xlsx";
                        workbook.SaveAs(Server.MapPath(FileName));

                        string msg = "Excel all part Import successfully";
                        bool status = true;
                        if (errorlist != null && errorlist.Count > 0)
                        {
                            msg = string.Join("<br/>", errorlist.Select(x => "Row No:" + x.Key + ", Information:" + x.Value));
                            status = false;
                        }

                        return Json(new { filename = FileName, error = msg, status = status }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    //TempData["Error"] = "Please enter atleast one details for import part/bom.";
                    //return RedirectToAction("Add", "PartImport");
                    return Json(new { error = "Please enter atleast one details for import part/bom.", status = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //TempData["Error"] = ex.Message.ToString();
                //return RedirectToAction("Add", "PartImport");
                return Json(new { error = ex.Message.ToString(), status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadPartAndBOM(HttpPostedFileBase file)
        {
            string _FileName = "";
            string _path = "";
            DataTable dtPart = null;
            DataTable dtBOM = null;
            DataTable dtPartCloned = null;
            DataTable dtBOMCloned = null;
            List<string> listPartImportExcelColumn = new List<string>();
            List<string> listBOMImportExcelColumn = new List<string>();
            try
            {
                if (file == null)
                {
                    return Json(new { error = "Please select file." }, JsonRequestBehavior.AllowGet);
                }
                if (file.ContentLength > 0)
                {
                    // Create a directory
                    if (!Directory.Exists(Server.MapPath(UploadFilePath)))
                    {
                        Directory.CreateDirectory(Server.MapPath(UploadFilePath));
                    }
                    _FileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetFileName(file.FileName);
                    _path = Path.Combine(Server.MapPath(UploadFilePath), _FileName);
                    file.SaveAs(_path);
                }
                else
                {
                    return Json(new { error = "Please select file." }, JsonRequestBehavior.AllowGet);
                }

                DataSet result;
                //string path = System.Web.HttpContext.Current.Server.MapPath(_path);

                FileStream stream = System.IO.File.Open(_path, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader;
                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                //...
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                if (Path.GetExtension(_path).ToLower() == ".xlsx")
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                result = excelReader.AsDataSet();
                if (result != null && result.Tables.Count >= 2)
                {
                    listPartImportExcelColumn.Add(colActionforPart);
                    listPartImportExcelColumn.Add(colItemKeyType);
                    listPartImportExcelColumn.Add(colOrderPolicy);
                    listPartImportExcelColumn.Add(colProductForm);
                    listPartImportExcelColumn.Add(colSubProductForm);
                    listPartImportExcelColumn.Add(colType);
                    listPartImportExcelColumn.Add(colItemId);
                    listPartImportExcelColumn.Add(colMaterial);
                    listPartImportExcelColumn.Add(colItemGroup);
                    listPartImportExcelColumn.Add(colItemDescription);
                    listPartImportExcelColumn.Add(colProcurementDrg);
                    listPartImportExcelColumn.Add(colString2);
                    listPartImportExcelColumn.Add(colString3);
                    listPartImportExcelColumn.Add(colString4);
                    listPartImportExcelColumn.Add(colARMSet);
                    //listPartImportExcelColumn.Add(colARMRev);
                    listPartImportExcelColumn.Add(colSizeCode);
                    listPartImportExcelColumn.Add(colThickness);
                    listPartImportExcelColumn.Add(colCladPlatePartThickness1);
                    //listPartImportExcelColumn.Add(colCladSpecificGravity1);
                    listPartImportExcelColumn.Add(colIsDoubleClad);
                    listPartImportExcelColumn.Add(colCladPlatePartThickness2);
                    listPartImportExcelColumn.Add(colCladSpecificGravity2);
                    listPartImportExcelColumn.Add(colItemWeightkg);
                    listPartImportExcelColumn.Add(colCRS);
                    //listPartImportExcelColumn.Add(colSpecificGravity);


                    listBOMImportExcelColumn.Add(colParentItem);
                    listBOMImportExcelColumn.Add(colBOMItemId);
                    listBOMImportExcelColumn.Add(colActionforPCRelation);
                    listBOMImportExcelColumn.Add(colDrgNo);
                    listBOMImportExcelColumn.Add(colFindNo);
                    listBOMImportExcelColumn.Add(colExtFindNo);
                    listBOMImportExcelColumn.Add(colFindNoDesc);
                    listBOMImportExcelColumn.Add(colJobQty);
                    listBOMImportExcelColumn.Add(colCommissioningSpareQty);
                    listBOMImportExcelColumn.Add(colMandatorySpareQty);
                    listBOMImportExcelColumn.Add(colOperationSpareQty);
                    listBOMImportExcelColumn.Add(colExtraQty);
                    listBOMImportExcelColumn.Add(colQuantity);
                    listBOMImportExcelColumn.Add(colLengthmm);
                    listBOMImportExcelColumn.Add(colWidth);
                    listBOMImportExcelColumn.Add(colNumberOfPieces);
                    //listBOMImportExcelColumn.Add(colUOM);
                    //listBOMImportExcelColumn.Add(colWeightFactor);
                    listBOMImportExcelColumn.Add(colRemarks);
                    listBOMImportExcelColumn.Add(colBOMWeightkg);
                    listBOMImportExcelColumn.Add(colBOMReportSize);
                    listBOMImportExcelColumn.Add(colDrawingBOMSize);

                    dtPart = result.Tables[0];
                    for (int i = 0; i < dtPart.Columns.Count; i++)
                    {
                        dtPart.Columns[i].ColumnName = dtPart.Rows[0][i].ToString().Replace("*", "").Trim();
                    }

                    dtPartCloned = dtPart.Clone();
                    for (int i = 0; i < dtPart.Columns.Count; i++)
                    {
                        dtPartCloned.Columns[i].DataType = typeof(string);
                    }
                    foreach (DataRow row in dtPart.Rows)
                    {
                        if (row.ItemArray.Any(i => i is DBNull != true))
                        {
                            dtPartCloned.ImportRow(row);
                        }
                    }


                    dtBOM = result.Tables[1];
                    for (int i = 0; i < dtBOM.Columns.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dtBOM.Rows[0][i].ToString().Replace("*", "").Trim()))
                        {
                            dtBOM.Columns[i].ColumnName = dtBOM.Rows[0][i].ToString().Replace("*", "").Trim();
                        }
                    }

                    dtBOMCloned = dtBOM.Clone();
                    for (int i = 0; i < dtBOM.Columns.Count; i++)
                    {
                        dtBOMCloned.Columns[i].DataType = typeof(string);
                    }
                    foreach (DataRow row in dtBOM.Rows)
                    {
                        if (row.ItemArray.Any(i => i is DBNull != true))
                        {
                            dtBOMCloned.ImportRow(row);
                        }
                    }
                }
                else
                {
                    return Json(new { error = "Your File format is not match with template." }, JsonRequestBehavior.AllowGet);
                }

                if (dtPart.Rows.Count >= 1)
                {
                    dtPartCloned.Rows.RemoveAt(0);
                    var listColumns = dtPartCloned.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();
                    if (listPartImportExcelColumn.Where(i => !listColumns.Contains(i)).Any())
                    {
                        return Json(new { error = "Your File format is not match with Part template." }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (string dc in listColumns)
                    {
                        if (!listPartImportExcelColumn.Any(i => dc.ToLower() == i.ToLower()))
                        {
                            dtPartCloned.Columns.Remove(dc);
                            dtPartCloned.AcceptChanges();
                        }
                    }
                    dtPartCloned.Columns.Add(colProduct_Form_ID, typeof(int));
                    //dtPartCloned.Columns.Add(colDrg_No_ID, typeof(int));
                    //dtPartCloned.Columns.Add(colParent_Item_ID, typeof(int));
                    dtPartCloned.Columns.Add(colProcurement_Drg_ID, typeof(int));
                    if (!dtPartCloned.Columns.Contains(colERROR))
                    {
                        dtPartCloned.Columns.Add(colERROR, typeof(string));
                    }
                    if (dtPartCloned.Columns.Contains("System Info"))
                    {
                        dtPartCloned.Columns.Remove("System Info");
                    }
                    //dtPartCloned.Columns.Add(colHdnBOMReportSize, typeof(string));
                    //dtPartCloned.Columns.Add(colHdnDrawingBOMSize, typeof(string));

                    DataColumn colRevNo = new DataColumn("RevNo", typeof(int));
                    colRevNo.DefaultValue = 0;
                    dtPartCloned.Columns.Add(colRevNo);

                    //DataColumn colReviseWithBOM = new DataColumn("ReviseWithBOM", typeof(bool));
                    //colReviseWithBOM.DefaultValue = false;
                    //dtPartCloned.Columns.Add(colReviseWithBOM);

                    DataColumn colPolicyId = new DataColumn("PolicyId", typeof(int));
                    colPolicyId.DefaultValue = 0;
                    dtPartCloned.Columns.Add(colPolicyId);
                }


                if (dtBOMCloned.Rows.Count >= 1)
                {
                    dtBOMCloned.Rows.RemoveAt(0);
                    var listColumns = dtBOMCloned.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();
                    if (listBOMImportExcelColumn.Where(i => !listColumns.Contains(i)).Any())
                    {
                        return Json(new { error = "Your File format is not match with BOM template." }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (string dc in listColumns)
                    {
                        if (!listBOMImportExcelColumn.Any(i => dc.ToLower() == i.ToLower()))
                        {
                            dtBOMCloned.Columns.Remove(dc);
                            dtBOMCloned.AcceptChanges();
                        }
                    }
                    //dtBOMCloned.Columns.Add(colProduct_Form_ID, typeof(int));
                    dtBOMCloned.Columns.Add(colDrg_No_ID, typeof(int));
                    dtBOMCloned.Columns.Add(colParent_Item_ID, typeof(int));
                    //dtBOMCloned.Columns.Add(colProcurement_Drg_ID, typeof(int));
                    if (!dtBOMCloned.Columns.Contains(colERROR))
                    {
                        dtBOMCloned.Columns.Add(colERROR, typeof(string));
                    }
                    if (dtBOMCloned.Columns.Contains("System Info"))
                    {
                        dtBOMCloned.Columns.Remove("System Info");
                    }
                    dtBOMCloned.Columns.Add(colHdnBOMReportSize, typeof(string));
                    dtBOMCloned.Columns.Add(colHdnDrawingBOMSize, typeof(string));

                    DataColumn colItem_ID = new DataColumn(colItem_Id, typeof(Int64));
                    colItem_ID.DefaultValue = 0;
                    dtBOMCloned.Columns.Add(colItem_ID);

                    DataColumn col_ConfirmRevision = new DataColumn(colConfirmRevision, typeof(bool));
                    col_ConfirmRevision.DefaultValue = false;
                    dtBOMCloned.Columns.Add(col_ConfirmRevision);
                }

                // File Delete
                if (System.IO.File.Exists(_path))
                {
                    System.IO.File.Delete(_path);
                }
                if (dtPartCloned.Rows.Count == 0 && dtBOMCloned.Rows.Count == 0)
                {
                    return Json(new { error = "Please enter atleast one details for import part/bom.", status = false }, JsonRequestBehavior.AllowGet);
                }
                var ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
                var Contract = Models.SessionMgtModel.ProjectFolderList.Contract;
                string psno = CommonService.objClsLoginInfo.UserName;
                var FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                var Department = CommonService.objClsLoginInfo.Department;
                var RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;


                var dtPartReturn = _PartService.ImportPartItem(ProjectNo, Contract, psno, FolderId, Department, RoleGroup, dtPartCloned);
                var dtBOMReturn = _PartService.ImportBOMItem(ProjectNo, Contract, psno, FolderId, Department, RoleGroup, dtBOMCloned);

                List<KeyValuePair<string, string>> errorlist = new List<KeyValuePair<string, string>>();
                List<KeyValuePair<string, string>> errorBOMlist = new List<KeyValuePair<string, string>>();

                if (dtPartReturn.Rows.Count >= 1 || dtBOMReturn.Rows.Count >= 1)
                {
                    var NoRecordsInPart = false;
                    var NoRecordsInBOM = false;
                    GridView grvPart = new GridView();
                    GridView grvBOM = new GridView();

                    if (dtPartReturn.Rows.Count == 0)
                    {
                        NoRecordsInPart = true;
                        dtPartReturn.Rows.Add(dtPartReturn.NewRow());
                    }
                    grvPart.AllowPaging = false;
                    grvPart.DataSource = dtPartReturn;
                    grvPart.DataBind();
                    for (int i = 0; i < grvPart.Rows.Count; i++)
                    {
                        List<string> listError = dtPartReturn.Rows[i]["Error"].ToString().Split('|').Where(x => !string.IsNullOrEmpty(x)).ToList();
                        var ErrorReplaceText = string.Empty;
                        for (int k = 0; k < listError.Count; k++)
                        {
                            var error = listError[k].Trim();
                            var errorcolumnname = listError[k].Split(' ')[0];
                            string errorcolumn = errorcolumnname.Replace("_", " ");
                            if (error.ToLower() == "success")
                            {
                                ErrorReplaceText = error;

                            }
                            else if (error.ToLower().StartsWith(errorcolumnname.ToLower()))
                            {
                                if (ErrorReplaceText.Length == 0)
                                {
                                    ErrorReplaceText = error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                                else
                                {
                                    ErrorReplaceText += "|" + error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                            }
                            for (int j = 0; j <= (grvPart.HeaderRow.Cells.Count - 1); j++)
                            {
                                if (grvPart.HeaderRow.Cells[j].Text.ToLower() == errorcolumn.ToLower())
                                    grvPart.Rows[i].Cells[j].BackColor = Color.Red;

                                if (grvPart.HeaderRow.Cells[j].Text.ToLower() == "error")
                                {
                                    var listInfo = ErrorReplaceText.Replace("Information :", "").Split('|').Distinct().ToList();
                                    grvPart.Rows[i].Cells[j].Text = string.Join("|", listInfo);
                                }
                            }
                        }



                        if (!string.IsNullOrEmpty(ErrorReplaceText) && ErrorReplaceText.ToLower() != "success")
                        {
                            var listInfo = ErrorReplaceText.Replace("Information :", "").Split('|').Distinct().ToList();
                            errorlist.Add(new KeyValuePair<string, string>((i + 1).ToString(), string.Join("|", listInfo)));

                        }
                    }

                    if (dtBOMReturn.Rows.Count == 0)
                    {
                        NoRecordsInBOM = true;
                        dtBOMReturn.Rows.Add(dtBOMReturn.NewRow());
                    }
                    grvBOM.AllowPaging = false;
                    grvBOM.DataSource = dtBOMReturn;
                    grvBOM.DataBind();
                    for (int i = 0; i < grvBOM.Rows.Count; i++)
                    {
                        List<string> listError = dtBOMReturn.Rows[i]["Error"].ToString().Split('|').Where(x => !string.IsNullOrEmpty(x)).ToList();
                        var ErrorReplaceText = string.Empty;
                        for (int k = 0; k < listError.Count; k++)
                        {
                            var error = listError[k].Trim();
                            var errorcolumnname = listError[k].Split(' ')[0];
                            string errorcolumn = errorcolumnname.Replace("_", " ");
                            if (error.ToLower() == "success")
                            {
                                ErrorReplaceText = error;

                            }
                            else if (error.ToLower().StartsWith(errorcolumnname.ToLower()))
                            {
                                if (ErrorReplaceText.Length == 0)
                                {
                                    ErrorReplaceText = error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                                else
                                {
                                    ErrorReplaceText += "|" + error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                            }
                            for (int j = 0; j <= (grvBOM.HeaderRow.Cells.Count - 1); j++)
                            {
                                if (grvBOM.HeaderRow.Cells[j].Text.ToLower() == errorcolumn.ToLower())
                                    grvBOM.Rows[i].Cells[j].BackColor = Color.Red;

                                if (grvBOM.HeaderRow.Cells[j].Text.ToLower() == "error")
                                {
                                    var listInfo = ErrorReplaceText.Replace("Information :", "").Split('|').Distinct().ToList();
                                    grvBOM.Rows[i].Cells[j].Text = string.Join("|", listInfo);
                                }
                            }
                        }



                        if (!string.IsNullOrEmpty(ErrorReplaceText) && ErrorReplaceText.ToLower() != "success")
                        {
                            var listInfo = ErrorReplaceText.Replace("Information :", "").Split('|').Distinct().ToList();
                            errorBOMlist.Add(new KeyValuePair<string, string>((i + 1).ToString(), string.Join("|", listInfo)));
                        }
                    }


                    using (ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook())
                    {
                        var ws = workbook.Worksheets.Add("Part");
                        for (int l = 0; l < grvPart.HeaderRow.Cells.Count; l++)
                        {
                            ws.Cell(1, l + 1).Value = grvPart.HeaderRow.Cells[l].Text.ToLower() == "error" ? "System Info" : grvPart.HeaderRow.Cells[l].Text;
                            ws.Cell(1, l + 1).Style.Font.Bold = true;

                        }
                        for (int k = (NoRecordsInPart ? 1 : 0); k < grvPart.Rows.Count; k++)
                        {
                            for (int i = 0; i < grvPart.Rows[k].Cells.Count; i++)
                            {
                                ws.Cell(k + 2, i + 1).Value = grvPart.Rows[k].Cells[i].Text == "&nbsp;" ? "" : grvPart.Rows[k].Cells[i].Text;
                                if (grvPart.Rows[k].Cells[i].BackColor.ToString() != "Color [Empty]")
                                {
                                    ws.Cell(k + 2, i + 1).Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.Red;
                                }
                            }
                        }

                        var wsBOM = workbook.Worksheets.Add("BOM");
                        for (int l = 0; l < grvBOM.HeaderRow.Cells.Count; l++)
                        {
                            wsBOM.Cell(1, l + 1).Value = grvBOM.HeaderRow.Cells[l].Text.Replace("&#160;", " ").ToLower() == "error" ? "System Info" : grvBOM.HeaderRow.Cells[l].Text.Replace("&#160;", " ");
                            wsBOM.Cell(1, l + 1).Style.Font.Bold = true;
                        }
                        for (int k = (NoRecordsInBOM ? 1 : 0); k < grvBOM.Rows.Count; k++)
                        {
                            for (int i = 0; i < grvBOM.Rows[k].Cells.Count; i++)
                            {
                                wsBOM.Cell(k + 2, i + 1).Value = grvBOM.Rows[k].Cells[i].Text == "&nbsp;" ? "" : grvBOM.Rows[k].Cells[i].Text;
                                if (grvBOM.Rows[k].Cells[i].BackColor.ToString() != "Color [Empty]")
                                {
                                    wsBOM.Cell(k + 2, i + 1).Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.Red;
                                }
                            }
                        }

                        string FileName = UploadFilePath + "PartAndBOMImport" + DateTime.Now.Ticks + ".xlsx";
                        workbook.SaveAs(Server.MapPath(FileName));

                        string msg = string.Empty;
                        bool status = false;
                        if ((errorlist != null && errorlist.Count > 0) || (errorBOMlist != null && errorBOMlist.Count > 0))
                        {
                            if ((errorlist != null && errorlist.Count > 0))
                            {
                                msg += "Part : - " + string.Join("<br/>", errorlist.Select(x => "Row No:" + x.Key + ", Information:" + x.Value));
                                status = false;
                            }
                            if (errorBOMlist != null && errorBOMlist.Count > 0)
                            {
                                msg += "<br/>BOM : - " + string.Join("<br/>", errorBOMlist.Select(x => "Row No:" + x.Key + ", Information:" + x.Value));
                                status = false;
                            }
                        }
                        else
                        {
                            msg = "Excel all part Import successfully";
                            status = true;
                        }

                        if (dtBOMReturn != null && dtBOMReturn.Columns.Count > 0)
                        {
                            for (int i = 0; i < dtBOMReturn.Columns.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(dtBOMReturn.Columns[i].ColumnName.ToString().Trim()))
                                {
                                    var tmp = HttpUtility.HtmlEncode(dtBOMReturn.Columns[i].ColumnName);
                                    string h1 = tmp.Replace("&nbsp;", " ");
                                    h1 = h1.Replace("&#160;", " ");
                                    dtBOMReturn.Columns[i].ColumnName = h1.Trim();
                                }
                            }
                        }

                        var dv = new DataView(dtBOMReturn);
                        dv.RowFilter = "ReviseMsg<>''";
                        if (dv.Count > 0)
                        {
                            var RowNo = 1;
                            List<DESCore.BOMImport> listBOM = new List<DESCore.BOMImport>();
                            foreach (DataRowView drv in dv)
                            {
                                if (!string.IsNullOrEmpty(drv[colReviseMsg].ToString()))
                                {
                                    listBOM.Add(new DESCore.BOMImport
                                    {
                                        RowNo = RowNo,
                                        ActionForPCRelation = drv[colActionforPCRelation].ToString(),
                                        BOMReportSize = drv[colBOMReportSize].ToString(),
                                        BOMWeight = drv[colBOMWeightkg].ToString(),
                                        CommissioningSpareQty = drv[colCommissioningSpareQty].ToString(),
                                        DrawingBOMSize = drv[colDrawingBOMSize].ToString(),
                                        DrgNo = drv[colDrgNo].ToString(),
                                        ExtFindNo = drv[colExtFindNo].ToString(),
                                        ExtraQty = drv[colExtraQty].ToString(),
                                        FindNo = drv[colFindNo].ToString(),
                                        FindNoDesc = drv[colFindNoDesc].ToString(),
                                        ItemId = drv[colBOMItemId].ToString(),
                                        JobQty = drv[colJobQty].ToString(),
                                        Length = drv[colLengthmm].ToString(),
                                        MandatorySpareQty = drv[colMandatorySpareQty].ToString(),
                                        NumberOfPieces = drv[colNumberOfPieces].ToString(),
                                        OperationSpareQty = drv[colOperationSpareQty].ToString(),
                                        ParentItem = drv[colParentItem].ToString(),
                                        QuantityForBOM = drv[colQuantity].ToString(),
                                        Remarks = drv[colRemarks].ToString(),
                                        Width = drv[colWidth].ToString(),
                                        ReviseMsg = drv[colReviseMsg].ToString(),
                                    });
                                    RowNo = RowNo + 1;
                                }
                            }
                            if (listBOM != null && listBOM.Count > 0)
                            {
                                Session[sBOMRevise] = listBOM;
                                return Json(new { filename = FileName, error = msg, status = status, isbomrevise = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        return Json(new { filename = FileName, error = msg, status = status }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    //TempData["Error"] = "Please enter atleast one details for import part/bom.";
                    //return RedirectToAction("Add", "PartImport");
                    return Json(new { error = "Please enter atleast one details for import part/bom.", status = false }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { error = "", status = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //TempData["Error"] = ex.Message.ToString();
                //return RedirectToAction("Add", "PartImport");
                return Json(new { error = ex.Message.ToString(), status = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public FileResult DownloadTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/DES/Views/PartImport/PartAndBOMImport.xlsx"));
            string fileName = "PartAndBOMImport.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public ActionResult Autocomplit(string username)
        {
            List<string> customers = new List<string>();

            for (int i = 1; i < 10000; i++)
            {

                customers.Add("Row" + i.ToString() + "/Row");
            }

            customers = customers.Where(x => x.ToLower().Contains(username.ToLower())).ToList();
            return Json(customers);
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download, 
        public ActionResult Download(string file)
        {
            string fullPath = Server.MapPath(file);

            return File(fullPath, "application/vnd.ms-excel", Path.GetFileName(fullPath));
        }

        [HttpPost]
        public ActionResult GetReviseBOMList(DataTableParamModel parm)
        {
            List<DESCore.BOMImport> listBOM = new List<DESCore.BOMImport>();
            int recordsTotal = 0;
            if (Session[sBOMRevise] != null)
            {
                listBOM = (List<DESCore.BOMImport>)Session[sBOMRevise];
                Session[sBOMRevise] = null;
            }

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = listBOM
            });
        }

        public JsonResult SubmitReviseBOM(List<DESCore.BOMImport> listBOM)
        {
            StringBuilder successmsg = new StringBuilder();
            StringBuilder errormsg = new StringBuilder();
            var finalmsg = string.Empty;
            var dt = GetBOMTable();
            foreach (var item in listBOM)
            {
                DataRow dr = dt.NewRow();
                dr[colParentItem] = item.ParentItem;
                dr[colBOMItemId] = item.ItemId;
                dr[colActionforPCRelation] = item.ActionForPCRelation;
                dr[colDrgNo] = item.DrgNo;
                dr[colFindNo] = item.FindNo;
                dr[colExtFindNo] = item.ExtFindNo;
                dr[colFindNoDesc] = item.FindNoDesc;
                dr[colJobQty] = item.JobQty;
                dr[colCommissioningSpareQty] = item.CommissioningSpareQty;
                dr[colMandatorySpareQty] = item.MandatorySpareQty;
                dr[colOperationSpareQty] = item.OperationSpareQty;
                dr[colExtraQty] = item.ExtraQty;
                dr[colQuantity] = item.QuantityForBOM;
                dr[colLengthmm] = item.Length;
                dr[colWidth] = item.Width;
                dr[colNumberOfPieces] = item.NumberOfPieces;
                dr[colRemarks] = item.Remarks;
                dr[colBOMWeightkg] = item.BOMWeight;
                dr[colBOMReportSize] = item.BOMReportSize;
                dr[colDrawingBOMSize] = item.DrawingBOMSize;
                dr[colConfirmRevision] = true;
                dt.Rows.Add(dr);
            }
            if (dt.Rows.Count > 0)
            {
                var ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
                var Contract = Models.SessionMgtModel.ProjectFolderList.Contract;
                string psno = CommonService.objClsLoginInfo.UserName;
                var FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                var Department = CommonService.objClsLoginInfo.Department;
                var RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;

                var dtBOMReturn = _PartService.ImportBOMItem(ProjectNo, Contract, psno, FolderId, Department, RoleGroup, dt);

                foreach (DataRow dr in dtBOMReturn.Rows)
                {
                    if (dr[colERROR].ToString().ToLower() == "success")
                    {
                        if (!string.IsNullOrEmpty(dr[colSuccessMsg].ToString()))
                        {
                            if (successmsg.ToString().Length > 0)
                            {
                                successmsg.Append("," + dr[colSuccessMsg].ToString());
                            }
                            else
                            {
                                successmsg.Append(dr[colSuccessMsg].ToString());
                            }
                        }

                    }
                    else
                    {
                        List<string> listError = dr[colERROR].ToString().Split('|').Where(x => !string.IsNullOrEmpty(x)).ToList();
                        var ErrorReplaceText = string.Empty;
                        for (int k = 0; k < listError.Count; k++)
                        {
                            var error = listError[k].Trim();
                            var errorcolumnname = listError[k].Split(' ')[0];
                            string errorcolumn = errorcolumnname.Replace("_", " ");
                            if (error.ToLower() == "success")
                            {
                                ErrorReplaceText = error;
                            }
                            else if (error.ToLower().StartsWith(errorcolumnname.ToLower()))
                            {
                                if (ErrorReplaceText.Length == 0)
                                {
                                    ErrorReplaceText = error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                                else
                                {
                                    ErrorReplaceText += "|" + error.Substring(errorcolumnname.Length, error.Length - errorcolumnname.Length);
                                }
                            }
                        }
                        var listInfo = ErrorReplaceText.Replace("Information :", "").Split('|').Distinct().ToList();
                        errormsg.Append(string.Join(",", listInfo));
                    }
                }
            }
            if (successmsg.ToString() != string.Empty)
            {
                finalmsg += "Success : " + successmsg.ToString();
            }
            if (errormsg.ToString() != string.Empty)
            {
                finalmsg += (finalmsg.Length > 0 ? ", " : "") + "Error : " + errormsg.ToString();
            }
            return Json(new { msg = finalmsg }, JsonRequestBehavior.AllowGet);
        }

        public DataTable GetBOMTable()
        {
            var dt = new DataTable();
            dt.Columns.Add(colParentItem, typeof(string));
            dt.Columns.Add(colBOMItemId, typeof(string));
            dt.Columns.Add(colActionforPCRelation, typeof(string));
            dt.Columns.Add(colDrgNo, typeof(string));
            dt.Columns.Add(colFindNo, typeof(string));
            dt.Columns.Add(colExtFindNo, typeof(string));
            dt.Columns.Add(colFindNoDesc, typeof(string));
            dt.Columns.Add(colJobQty, typeof(string));
            dt.Columns.Add(colCommissioningSpareQty, typeof(string));
            dt.Columns.Add(colMandatorySpareQty, typeof(string));
            dt.Columns.Add(colOperationSpareQty, typeof(string));
            dt.Columns.Add(colExtraQty, typeof(string));
            dt.Columns.Add(colQuantity, typeof(string));
            dt.Columns.Add(colLengthmm, typeof(string));
            dt.Columns.Add(colWidth, typeof(string));
            dt.Columns.Add(colNumberOfPieces, typeof(string));
            dt.Columns.Add(colRemarks, typeof(string));
            dt.Columns.Add(colBOMWeightkg, typeof(string));
            dt.Columns.Add(colBOMReportSize, typeof(string));
            dt.Columns.Add(colDrawingBOMSize, typeof(string));
            dt.Columns.Add(colDrg_No_ID, typeof(string));
            dt.Columns.Add(colParent_Item_ID, typeof(string));

            //dt.Columns.Add(colDrg_No_ID, typeof(int));
            //dt.Columns.Add(colParent_Item_ID, typeof(int));
            if (!dt.Columns.Contains(colERROR))
            {
                dt.Columns.Add(colERROR, typeof(string));
            }
            if (dt.Columns.Contains("System Info"))
            {
                dt.Columns.Remove("System Info");
            }
            dt.Columns.Add(colHdnBOMReportSize, typeof(string));
            dt.Columns.Add(colHdnDrawingBOMSize, typeof(string));

            DataColumn colItem_ID = new DataColumn(colItem_Id, typeof(Int64));
            colItem_ID.DefaultValue = 0;
            dt.Columns.Add(colItem_ID);

            DataColumn col_ConfirmRevision = new DataColumn(colConfirmRevision, typeof(bool));
            col_ConfirmRevision.DefaultValue = false;
            dt.Columns.Add(col_ConfirmRevision);

            return dt;
        }

        public ActionResult GetPartBOMExcel(Int64 ManufacturingItem)
        {
            var partList = _PartService.GetChildPart(ManufacturingItem);
            var bomList = _PartService.GetChildBom(ManufacturingItem);
            if (partList != null && partList.Count > 0)
            {
                using (ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook())
                {
                    var ws = workbook.Worksheets.Add("Part");

                    ws.Cell(1, 1).Value = colActionforPart;
                    ws.Cell(1, 2).Value = colItemKeyType;
                    ws.Cell(1, 3).Value = colOrderPolicy;
                    ws.Cell(1, 4).Value = colProductForm;
                    ws.Cell(1, 5).Value = colSubProductForm;
                    ws.Cell(1, 6).Value = colType;
                    ws.Cell(1, 7).Value = colItemId;
                    ws.Cell(1, 8).Value = colMaterial;
                    ws.Cell(1, 9).Value = colItemGroup;
                    ws.Cell(1, 10).Value = colItemDescription;
                    ws.Cell(1, 11).Value = colString2;
                    ws.Cell(1, 12).Value = colString3;
                    ws.Cell(1, 13).Value = colString4;
                    ws.Cell(1, 14).Value = colARMSet;
                    ws.Cell(1, 15).Value = colProcurementDrg;
                    ws.Cell(1, 16).Value = colSizeCode;
                    ws.Cell(1, 17).Value = colThickness;
                    ws.Cell(1, 18).Value = colCladPlatePartThickness1;
                    ws.Cell(1, 19).Value = colIsDoubleClad;
                    ws.Cell(1, 20).Value = colCladPlatePartThickness2;
                    ws.Cell(1, 21).Value = colCladSpecificGravity2;
                    ws.Cell(1, 22).Value = colItemWeightkg;
                    ws.Cell(1, 23).Value = colCRS;

                    ws.Cell(1, 1).Style.Font.Bold = true;
                    ws.Cell(1, 2).Style.Font.Bold = true;
                    ws.Cell(1, 3).Style.Font.Bold = true;
                    ws.Cell(1, 4).Style.Font.Bold = true;
                    ws.Cell(1, 5).Style.Font.Bold = true;
                    ws.Cell(1, 6).Style.Font.Bold = true;
                    ws.Cell(1, 7).Style.Font.Bold = true;
                    ws.Cell(1, 8).Style.Font.Bold = true;
                    ws.Cell(1, 9).Style.Font.Bold = true;
                    ws.Cell(1, 10).Style.Font.Bold = true;
                    ws.Cell(1, 11).Style.Font.Bold = true;
                    ws.Cell(1, 12).Style.Font.Bold = true;
                    ws.Cell(1, 13).Style.Font.Bold = true;
                    ws.Cell(1, 14).Style.Font.Bold = true;
                    ws.Cell(1, 15).Style.Font.Bold = true;
                    ws.Cell(1, 16).Style.Font.Bold = true;
                    ws.Cell(1, 17).Style.Font.Bold = true;
                    ws.Cell(1, 18).Style.Font.Bold = true;
                    ws.Cell(1, 19).Style.Font.Bold = true;
                    ws.Cell(1, 20).Style.Font.Bold = true;
                    ws.Cell(1, 21).Style.Font.Bold = true;
                    ws.Cell(1, 22).Style.Font.Bold = true;
                    ws.Cell(1, 23).Style.Font.Bold = true;

                    int Partrowcount = 2;
                    foreach (var part in partList)
                    {
                        ws.Cell(Partrowcount, 1).Value = "Update";
                        ws.Cell(Partrowcount, 2).Value = part.ItemKeyType;
                        ws.Cell(Partrowcount, 3).Value = part.OrderPolicy;
                        ws.Cell(Partrowcount, 4).Value = part.ProductForm;
                        ws.Cell(Partrowcount, 5).Value = part.SubProductForm;
                        ws.Cell(Partrowcount, 6).Value = part.Type;
                        ws.Cell(Partrowcount, 7).Value = part.ItemKey;
                        ws.Cell(Partrowcount, 8).Value = part.Material;
                        ws.Cell(Partrowcount, 9).Value = part.ItemGroup;
                        ws.Cell(Partrowcount, 10).Value = part.ItemName;
                        ws.Cell(Partrowcount, 11).Value = part.String2;
                        ws.Cell(Partrowcount, 12).Value = part.String3;
                        ws.Cell(Partrowcount, 13).Value = part.String4;
                        ws.Cell(Partrowcount, 14).Value = part.ARMSet;
                        ws.Cell(Partrowcount, 15).Value = part.ProcurementDrgDocumentNo;
                        ws.Cell(Partrowcount, 16).Value = part.SizeCode;
                        ws.Cell(Partrowcount, 17).Value = part.Thickness;
                        ws.Cell(Partrowcount, 18).Value = part.CladPlatePartThickness1;
                        ws.Cell(Partrowcount, 19).Value = part.IsDoubleClad;
                        ws.Cell(Partrowcount, 20).Value = part.CladPlatePartThickness2;
                        ws.Cell(Partrowcount, 21).Value = part.CladSpecificGravity2;
                        ws.Cell(Partrowcount, 22).Value = part.ItemWeight;
                        ws.Cell(Partrowcount, 23).Value = part.CRSYN;
                        Partrowcount++;
                    }



                    var wsBOM = workbook.Worksheets.Add("BOM");

                    wsBOM.Cell(1, 1).Value = colParentItem;
                    wsBOM.Cell(1, 2).Value = colItemId;
                    wsBOM.Cell(1, 3).Value = colActionforPCRelation;
                    wsBOM.Cell(1, 4).Value = colDrgNo;
                    wsBOM.Cell(1, 5).Value = colFindNo;
                    wsBOM.Cell(1, 6).Value = colExtFindNo;
                    wsBOM.Cell(1, 7).Value = colFindNoDesc;
                    wsBOM.Cell(1, 8).Value = colJobQty;
                    wsBOM.Cell(1, 9).Value = colCommissioningSpareQty;
                    wsBOM.Cell(1, 10).Value = colMandatorySpareQty;
                    wsBOM.Cell(1, 11).Value = colOperationSpareQty;
                    wsBOM.Cell(1, 12).Value = colExtraQty;
                    wsBOM.Cell(1, 13).Value = colQuantity;
                    wsBOM.Cell(1, 14).Value = colLengthmm;
                    wsBOM.Cell(1, 15).Value = colWidth;
                    wsBOM.Cell(1, 16).Value = colNumberOfPieces;
                    wsBOM.Cell(1, 17).Value = colRemarks;
                    wsBOM.Cell(1, 18).Value = colBOMWeightkg;
                    wsBOM.Cell(1, 19).Value = colBOMReportSize;
                    wsBOM.Cell(1, 20).Value = colDrawingBOMSize;

                    wsBOM.Cell(1, 1).Style.Font.Bold = true;
                    wsBOM.Cell(1, 2).Style.Font.Bold = true;
                    wsBOM.Cell(1, 3).Style.Font.Bold = true;
                    wsBOM.Cell(1, 4).Style.Font.Bold = true;
                    wsBOM.Cell(1, 5).Style.Font.Bold = true;
                    wsBOM.Cell(1, 6).Style.Font.Bold = true;
                    wsBOM.Cell(1, 7).Style.Font.Bold = true;
                    wsBOM.Cell(1, 8).Style.Font.Bold = true;
                    wsBOM.Cell(1, 9).Style.Font.Bold = true;
                    wsBOM.Cell(1, 10).Style.Font.Bold = true;
                    wsBOM.Cell(1, 11).Style.Font.Bold = true;
                    wsBOM.Cell(1, 12).Style.Font.Bold = true;
                    wsBOM.Cell(1, 13).Style.Font.Bold = true;
                    wsBOM.Cell(1, 14).Style.Font.Bold = true;
                    wsBOM.Cell(1, 15).Style.Font.Bold = true;
                    wsBOM.Cell(1, 16).Style.Font.Bold = true;
                    wsBOM.Cell(1, 17).Style.Font.Bold = true;
                    wsBOM.Cell(1, 18).Style.Font.Bold = true;
                    wsBOM.Cell(1, 19).Style.Font.Bold = true;
                    wsBOM.Cell(1, 20).Style.Font.Bold = true;



                    int Bomrowcount = 2;
                    if (bomList != null && bomList.Count > 0)
                        foreach (var bom in bomList)
                        {
                            wsBOM.Cell(Bomrowcount, 1).Value = bom.ParentItemKey;
                            wsBOM.Cell(Bomrowcount, 2).Value = bom.ItemKey;
                            wsBOM.Cell(Bomrowcount, 3).Value = "Update";
                            wsBOM.Cell(Bomrowcount, 4).Value = bom.DRGNoDocumentNo;
                            wsBOM.Cell(Bomrowcount, 5).Value = bom.FindNumber;
                            wsBOM.Cell(Bomrowcount, 6).Value = bom.ExtFindNumber;
                            wsBOM.Cell(Bomrowcount, 7).Value = bom.FindNumberDescription;
                            wsBOM.Cell(Bomrowcount, 8).Value = bom.JobQty;
                            wsBOM.Cell(Bomrowcount, 9).Value = bom.CommissioningSpareQty;
                            wsBOM.Cell(Bomrowcount, 10).Value = bom.MandatorySpareQty;
                            wsBOM.Cell(Bomrowcount, 11).Value = bom.OperationSpareQty;
                            wsBOM.Cell(Bomrowcount, 12).Value = bom.ExtraQty;
                            wsBOM.Cell(Bomrowcount, 13).Value = bom.Quantity;
                            wsBOM.Cell(Bomrowcount, 14).Value = bom.Length;
                            wsBOM.Cell(Bomrowcount, 15).Value = bom.Width;
                            wsBOM.Cell(Bomrowcount, 16).Value = bom.NumberOfPieces;
                            wsBOM.Cell(Bomrowcount, 17).Value = bom.Remarks;
                            wsBOM.Cell(Bomrowcount, 18).Value = bom.Weight;
                            wsBOM.Cell(Bomrowcount, 19).Value = bom.BomReportSize;
                            wsBOM.Cell(Bomrowcount, 20).Value = bom.DrawingBomSize;
                            Bomrowcount++;
                        }

                    string FileName = UploadFilePath + "PartAndBOMExport" + DateTime.Now.Ticks + ".xlsx";
                    workbook.SaveAs(Server.MapPath(FileName));

                    return Json(new { Status = true, FilePath = FileName }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { Status = false, FilePath = "" }, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            //convert the current filter context to file and get the file path
            string filePath = (filterContext.Result as FilePathResult).FileName;

            //delete the file after download
            System.IO.File.Delete(filePath);
        }
    }
}