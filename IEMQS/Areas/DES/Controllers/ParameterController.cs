﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.DES.Controllers
{
    
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class ParameterController : Controller
    {
        IBUService _BUService;
        ICategoryActionService _CategoryActionService;
        ICommonService _CommonService;
        IObjectService _ObjectService;
        IObjectPolicyMappingService _ObjectPolicyMappingService;
        IPolicyService _PolicyService;
        IFolderStructureService _FolderStructureService;
        public ParameterController(IBUService BUService, ICategoryActionService CategoryActionService, ICommonService CommonService, IObjectService ObjectService, IObjectPolicyMappingService ObjectPolicyMappingService, IPolicyService PolicyService, IFolderStructureService FolderStructureService)
        {
            _BUService = BUService;
            _CategoryActionService = CategoryActionService;
            _CommonService = CommonService;
            _ObjectService = ObjectService;
            _ObjectPolicyMappingService = ObjectPolicyMappingService;
            _PolicyService = PolicyService;
            _FolderStructureService = FolderStructureService;
        }

        // GET: DES/Parameter
        public ActionResult Index()
        {
            ViewBag.MenuTypeList = JsonConvert.SerializeObject(_CommonService.GetMenuType());
            //_BUService.GetBUbyId(1);
            ViewBag.BuList = JsonConvert.SerializeObject(_BUService.GetAllBU());
            ViewBag.CategoryList = JsonConvert.SerializeObject(_CategoryActionService.GetAllCategory().Where(x => x.TypeId == (int)MenuType.Category).ToList());
            ViewBag.ActionList = JsonConvert.SerializeObject(_CategoryActionService.GetAllCategory().Where(x => x.TypeId == (int)MenuType.Action).ToList());
            ViewBag.ObjectList = JsonConvert.SerializeObject(_ObjectService.GetAllObject());
            ViewBag.PolicyList = JsonConvert.SerializeObject(_PolicyService.GetAllPolicy());
            return View();
        }

        #region BUPBU

        [HttpPost]
        public ActionResult GetBUPBUList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _BUService.GetBUPBUList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddBU(BUModel model)
        {
            string errorMsg = "";
            Int64 status = _BUService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.BUId > 0)
                    return Json(new { Status = true, Msg = "BU-PBU updated successfully", BUlist = _BUService.GetAllBU() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "BU-PBU added successfully", BUlist = _BUService.GetAllBU() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteBU(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _BUService.DeleteBU(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "BU-PBU deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Maintain Objects

        [HttpPost]
        public ActionResult GetObjectList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ObjectService.GetObjectList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddObject(IEMQS.DESCore.ObjectModel model)
        {
            string errorMsg = "";
            model.BUIdList = JsonConvert.DeserializeObject<List<Int64>>(model.BUIds);
            model.CategoryIdList = JsonConvert.DeserializeObject<List<Int64>>(model.CategoryIds);
            model.ActionIdList = JsonConvert.DeserializeObject<List<Int64>>(model.ActionIds);

            Int64 status = _ObjectService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.ObjectId > 0)
                    return Json(new { Status = true, Msg = "Object updated successfully", Objectlist = _ObjectService.GetAllObject() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Object added successfully", Objectlist = _ObjectService.GetAllObject() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteObject(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ObjectService.DeleteObject(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Object deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult UpdateApplyBU(Int64 Id, bool ApplyBU)
        {
            bool status = _ObjectService.UpdateApplyBU(Id, ApplyBU);
            if (status)
            {
                if (ApplyBU)
                    return Json(new { Status = true, Msg = "Object activated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Object deactivated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Category & Action Mapping

        [HttpPost]
        public ActionResult GetCategoryActionList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _CategoryActionService.GetCategoryActionList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        //[HttpPost]
        //public ActionResult AddCategory(CategoryActionModel model)
        //{
        //    string errorMsg = "";
        //    Int64 status = _CategoryActionService.AddEdit(model, out errorMsg);
        //    if (status > 0)
        //    {
        //        if (model.MappingId > 0)
        //            return Json(new { Status = true, Msg = "Category & action updated successfully", Catlist = _CategoryActionService.GetAllCategory(), Actlist = _CategoryActionService.GetAllCategory() }, JsonRequestBehavior.AllowGet);
        //        else
        //            return Json(new { Status = true, Msg = "Category & action added successfully", Catlist = _CategoryActionService.GetAllCategory(), Actlist = _CategoryActionService.GetAllCategory() }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(errorMsg))
        //            return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
        //        else
        //            return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public ActionResult AddCategory(CategoryActionModel model)
        {
            string errorMsg = "";
            Int64 status = _CategoryActionService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                var list = _CategoryActionService.GetAllCategory();
                var Catlist = list.Where(x => x.TypeId == (int)MenuType.Category).ToList();
                var Actlist = list.Where(x => x.TypeId == (int)MenuType.Action).ToList();
                if (model.MappingId > 0)
                    return Json(new { Status = true, Msg = (model.TypeId == 1?"Category":"Action") +" updated successfully", Catlist = Catlist, Actlist = Actlist }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = (model.TypeId == 1 ? "Category" : "Action") + " added successfully", Catlist = Catlist, Actlist = Actlist }, JsonRequestBehavior.AllowGet);
            }
           
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteCategory(Int64 Id)
        {
            string errorMsg = "";
            int TypeId;
            Int64 status = _CategoryActionService.DeleteCategory(Id, out TypeId, out errorMsg);
            if (status > 0)
            {
                string ty = "";
                if (TypeId > 0)
                    ty = Enum.GetName(typeof(MenuType), TypeId);
                return Json(new { Status = true, Msg = ty + " deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ParentList(int TypeId)
        {
            return Json(_CategoryActionService.GetAllNameByType(TypeId), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region  Maintain Policy 

        [HttpPost]
        public ActionResult GetPolicyList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _PolicyService.GetPolicyList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult AddPolicy(Int64? Id)
        {
            PolicyModel model = new PolicyModel();
            if (Id > 0)
            {
                model = _PolicyService.GetPolicybyId(Id.Value);
                if (model == null)
                    model = new PolicyModel();

            }
            if (model.PolicyStepList == null || model.PolicyStepList.Count == 0)
                model.PolicyStepList = new List<PolicyStepModel> { new PolicyStepModel() };
            return PartialView("_partialAddPolicyView", model);
        }

        public ActionResult AddPolicyStep(int TNo)
        {
            PolicyModel model = new PolicyModel();
            TempData["NoTask"] = TNo;
            return PartialView("_partialPolicyStepView", model);
        }

        [HttpPost]
        public ActionResult AddPolicy(PolicyModel model)
        {
            string errorMsg = "";
            Int64 status = _PolicyService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.PolicyId > 0)
                    return Json(new { Status = true, Msg = "Policy updated successfully", Policylist = _PolicyService.GetAllPolicy() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Policy added successfully", Policylist = _PolicyService.GetAllPolicy() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeletePolicy(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _PolicyService.DeletePolicy(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Policy deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CheckPolicyNameAlreadyExist(string Name, Int64? PolicyId)
        {
            return Json(!_PolicyService.CheckPolicyNameAlreadyExist(Name, PolicyId), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Object Policy Mapping

        [HttpPost]
        public ActionResult GetObjectPolicyMappingList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ObjectPolicyMappingService.GetObjectPolicyMappingList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddObjectPolicyMapping(ObjectPolicyMappingModel model)
        {
            string errorMsg = "";
            Int64 status = _ObjectPolicyMappingService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.MappingId > 0)
                    return Json(new { Status = true, Msg = "Object policy mapping updated successfully", Objlist = _ObjectService.GetAllObject(), Policylist = _PolicyService.GetAllPolicy() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Object policy mapping added successfully", Objlist = _ObjectService.GetAllObject(), Policylist = _PolicyService.GetAllPolicy() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteObjectPolicyMapping(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ObjectPolicyMappingService.DeleteObjectPolicyMapping(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Object policy mapping deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Maintain Folder Structure

        [HttpGet]
        public ActionResult _partialCreateNewFolder(Int64 BUId, Int64 ParentFolderId = 0, bool EditModeOnFlag = false)
        {
            FolderStructureModel model = new FolderStructureModel();
            model = _FolderStructureService.GetCategoryActionByBUIdParentId(BUId, ParentFolderId, EditModeOnFlag);
            ViewBag.FunctionList = new SelectList(_FolderStructureService.GetRoleGroup().Select(x => new { GroupName = x }), "GroupName", "GroupName");
            return PartialView("_partialCreateNewFolder", model);
        }

        [HttpPost]
        public ActionResult _partialCreateNewFolder(FolderStructureModel model)
        {
            string errorMsg = "";
            Int64 status = _FolderStructureService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                TempData["OpenFolderId"] = status;
                if (model.FolderId > 0)
                    return Json(new { Status = true, Msg = "Folder updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "New folder added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult _partialFolderTreeView(Int64 BUId)
        {
            List<FolderStructureModel> _lstFolderStructureModel = _FolderStructureService.GetAllFolderStructureByBUId(BUId);
            return PartialView("_partialFolderTreeView", _lstFolderStructureModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult IsAlreadyExist(string FolderName, long? ParentFolderId, long? BUId)
        {
            return Json(!_FolderStructureService.IsAlreadyExist(FolderName, ParentFolderId, BUId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteFolder(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _FolderStructureService.DeleteFolder(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Folder deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}