﻿using IEMQS.DESCore;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using IEMQS.DESServices;

namespace IEMQS.Areas.DES.Models
{
    public class SessionMgtModel
    {


        public static ProjectFolderModel ProjectFolderList
        {
            get
            {
                var s = System.Web.HttpContext.Current.Session["ProjectFolderList"];
                if (s != null)
                {
                    return (ProjectFolderModel)s;
                }
                //  HttpContext.Current.Response.Redirect("/DES/Dashboard");
                return new ProjectFolderModel();

            }
            set
            {
                System.Web.HttpContext.Current.Session["ProjectFolderList"] = value;
            }

        }

        public static List<MasterMenu> MasterMenu
        {
            get
            {
                var s = (List<MasterMenu>)System.Web.HttpContext.Current.Session["MasterMenu"];
                if (s != null && s.Count() > 0)
                {
                    return (List<MasterMenu>)s;
                }
                else
                {
                    //var data = ;
                    var data = CommonService.GetMasterMenu(CommonService.objClsLoginInfo.UserRoles, CommonService.objClsLoginInfo.Location);
                    System.Web.HttpContext.Current.Session["MasterMenu"] = data;
                    return data;
                }

            }
        }

        public static List<LookupModel> ReportMenu
        {
            get
            {
                var s = (List<LookupModel>)System.Web.HttpContext.Current.Session["ReportMenu"];
                if (s != null && s.Count() > 0)
                {
                    return (List<LookupModel>)s;
                }
                else
                {
                    var data = new CommonService().GetLookupData(LookupType.Report.ToString());
                    System.Web.HttpContext.Current.Session["ReportMenu"] = data;
                    return data;
                }

            }
        }
    }
    public class ProjectFolderAuthorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (SessionMgtModel.ProjectFolderList == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Dashboard" }, { "action", "Index" }, { "area", "DES" } });
            }
            else if (SessionMgtModel.ProjectFolderList != null && string.IsNullOrEmpty(SessionMgtModel.ProjectFolderList.ProjectNo))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Dashboard" }, { "action", "Index" }, { "area", "DES" } });
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }
    }
}