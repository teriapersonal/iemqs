﻿using IEMQS.DESServices;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.DES.Notifications
{
    public class NotificationComponent
    {
        static string LoggedinUserPSNo = CommonService.objClsLoginInfo.UserName;
        //Added function for register notification (addded sql dependency)
        public void RegisterNotification(DateTime currentTime)
        {
            LoggedinUserPSNo = LoggedinUserPSNo ?? CommonService.objClsLoginInfo.UserName;
            string conStr = ConfigurationManager.ConnectionStrings["SQLNoti"].ConnectionString;
            //[AddedOn]> @AddedOn And 
            string sqlCommand = @"Select [NotificationMsg],[RedirectionPath] from [dbo].[GLB010] where [IsRead]=@IsRead AND [CreatedOn]>@CreatedOn AND [PSNo]=@PSNo";

            using (SqlConnection con = new SqlConnection(conStr))
            {
                SqlCommand cmd = new SqlCommand(sqlCommand, con);
                cmd.Parameters.AddWithValue("@IsRead", 0);
                cmd.Parameters.AddWithValue("@CreatedOn", currentTime.AddDays(-1));
                cmd.Parameters.AddWithValue("@PSNo", LoggedinUserPSNo ?? "20030804");

                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                cmd.Notification = null;
                SqlDependency sqlDep = new SqlDependency(cmd);
                sqlDep.OnChange += sqlDep_OnChange;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {

                }
            }
        }

        void sqlDep_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                SqlDependency sqlDep = sender as SqlDependency;
                sqlDep.OnChange -= sqlDep_OnChange;

                if (e.Info == SqlNotificationInfo.Insert)
                {
                    var notificationHub = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                    notificationHub.Clients.All.notify("added");
                }

                RegisterNotification(DateTime.Now);
            }
        }
    }
}