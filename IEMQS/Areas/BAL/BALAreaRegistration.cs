﻿using System.Web.Mvc;

namespace IEMQS.Areas.BAL
{
    public class BALAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BAL";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BAL_default",
                "BAL/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}