﻿using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.BAL.Controllers
{
    public class MaintainBALController : clsBase
    {
        // GET: BAL/MaintainBAL
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain BAL";

            return View();
        }


        [SessionExpireFilter]
        public ActionResult Details(int headerId = 0)
        {
            var Role = clsImplementationEnum.UserRoleName.QC2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QA1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QA2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QA3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.WE1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.WE2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.WE3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.ENGG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.ENGG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.ENGG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.ENGG4.GetStringValue() + "," + clsImplementationEnum.UserRoleName.AUX1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.AUX2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.AUX3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.NDE2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.NDE3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFGK.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.NDE1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
            ViewBag.Users = (new GeneralController()).GetRoleWiseEmployee(Role).ToList();
            ViewBag.Recipient = GetAllEmployee();
            ViewBag.HeaderId = headerId;

            ViewBag.Type = db.BAL001.Select(x => new ddlValue { id = x.Type, text = x.Type }).Distinct().ToList();
            List<SelectListItem> lstyesno = new List<SelectListItem>();
            lstyesno.Add(new SelectListItem { Value = "true", Text = "Yes" });
            lstyesno.Add(new SelectListItem { Value = "false", Text = "NA" });
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            if (headerId == 0)
            {
                ViewBag.Status = clsImplementationEnum.BALStatus.Draft.GetStringValue();
                ViewBag.MasterLineData = db.BAL001.ToList().Select(x => new BAL011
                {
                    SrNo = x.SrNo,
                    Activity = x.Activity,
                    Type = x.Type
                }).ToList();
                ViewBag.Revisionno = "0";
            }
            else
            {
                BAL010 bal010 = db.BAL010.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (bal010 != null)
                {
                    ViewBag.Project = bal010.Project;
                    ViewBag.txtTitle = bal010.Title;
                    ViewBag.txtRecipient = bal010.Recipient;
                    ViewBag.Status = bal010.Status;
                    ViewBag.Revisionno = bal010.RevNo;
                }

                db.Configuration.ProxyCreationEnabled = false;
                var bal011List = db.BAL011.Where(x => x.HeaderId == headerId).ToList();
                foreach (var item in bal011List)
                {
                    item.BAL010 = null;
                }
                ViewBag.MasterLineData = bal011List;
            }

            //ViewBag.MasterLineData = db.BAL001.ToList();

            return View();
        }


        public ActionResult SaveHeaderLines([System.Web.Http.FromUri] string project, [System.Web.Http.FromBody] List<BAL011> lineList, [System.Web.Http.FromUri] int headerId = 0, [System.Web.Http.FromUri] string status = "", [System.Web.Http.FromUri] string title = "", [System.Web.Http.FromUri] string recipient = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            if (headerId > 0)
            {
                BAL010 bal010 = db.BAL010.Where(x => x.HeaderId == headerId).FirstOrDefault();
                var currentStatus = bal010.Status;
                var bAL010_LogId = 0;
                if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                {
                    //Insert log in BAL010_Log
                    BAL010_Log bAL010_Log = new BAL010_Log();
                    bAL010_Log.CreatedBy = bal010.CreatedBy;
                    bAL010_Log.CreatedOn = bal010.CreatedOn;
                    bAL010_Log.DateOfSubmit = bal010.DateOfSubmit;
                    bAL010_Log.EditedBy = bal010.EditedBy;
                    bAL010_Log.EditedOn = bal010.EditedOn;
                    bAL010_Log.HeaderId = bal010.HeaderId;
                    bAL010_Log.Project = bal010.Project;
                    bAL010_Log.Title = title;
                    bAL010_Log.Recipient = recipient;
                    bAL010_Log.RevNo = bal010.RevNo;
                    bAL010_Log.Status = status;
                    db.BAL010_Log.Add(bAL010_Log);
                    db.SaveChanges();
                    bAL010_LogId = bAL010_Log.Id;
                    //Log end
                }

                bal010.Project = project;
                bal010.Title = title;
                bal010.Recipient = recipient;
                bal010.Status = status;
                bal010.EditedBy = objClsLoginInfo.UserName;
                bal010.EditedOn = DateTime.Now;
                if (currentStatus == clsImplementationEnum.BALStatus.Submitted.GetStringValue() && status == clsImplementationEnum.BALStatus.Draft.GetStringValue())
                {
                    bal010.RevNo = bal010.RevNo + 1;
                    bal010.DateOfSubmit = DateTime.Now;
                }

                db.SaveChanges();

                var lineGroup = lineList.GroupBy(x => x.Type);
                {
                    int idx = 0;
                    foreach (var grp in lineGroup)
                    {
                        foreach (var line in grp)
                        {
                            idx = idx + 1;
                            BAL011 bal011 = db.BAL011.Where(x => x.LineId == line.LineId).FirstOrDefault();
                            if (bal011 == null)
                            {
                                bal011 = new BAL011();
                                bal011.CreatedBy = objClsLoginInfo.UserName;
                                bal011.CreatedOn = DateTime.Now;
                                db.BAL011.Add(bal011);
                            }

                            bal011.ActionBy = line.ActionBy;
                            bal011.Activity = line.Activity;
                            bal011.HeaderId = bal010.HeaderId;
                            bal011.IsCompleted = line.IsCompleted;
                            bal011.Remarks = line.Remarks;
                            bal011.SrNo = idx;
                            bal011.IsApplicable = line.IsApplicable;
                            bal011.Type = line.Type;
                            bal011.EditedBy = objClsLoginInfo.UserName;
                            bal011.EditedOn = DateTime.Now;

                            db.SaveChanges();

                            //log
                            if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                            {
                                BAL011_Log bAL011_Log = new BAL011_Log();
                                bAL011_Log.RefId = bAL010_LogId;
                                bAL011_Log.ActionBy = bal011.ActionBy;
                                bAL011_Log.Activity = bal011.Activity;
                                bAL011_Log.CreatedBy = bal011.CreatedBy;
                                bAL011_Log.CreatedOn = bal011.CreatedOn;
                                bAL011_Log.EditedBy = bal011.EditedBy;
                                bAL011_Log.EditedOn = bal011.EditedOn;
                                bAL011_Log.HeaderId = bal011.HeaderId;
                                bAL011_Log.IsCompleted = bal011.IsCompleted;
                                bAL011_Log.LineId = bal011.LineId;
                                bAL011_Log.Remarks = bal011.Remarks;
                                bAL011_Log.SrNo = bal011.SrNo;
                                bAL011_Log.Type = bal011.Type;
                                bAL011_Log.IsApplicable = bal011.IsApplicable;
                                db.BAL011_Log.Add(bAL011_Log);
                                db.SaveChanges();
                            }
                            //log end

                        }
                    }
                }


                if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                {
                    List<string> emailto = new List<string>();
                    var personlist = lineList.Where(x => x.IsCompleted == false).ToList();

                    foreach (var item in personlist)
                    {
                        if (!string.IsNullOrEmpty(item.ActionBy))
                        {
                            if (item.ActionBy.Contains(","))
                            {
                                string[] psnolist = item.ActionBy.Split(',');
                                foreach (var ps in psnolist)
                                {
                                    var email = db.COM007.Where(x => x.t_emno == ps).FirstOrDefault().t_mail;
                                    if (!string.IsNullOrEmpty(email))
                                        emailto.Add(email);
                                }
                            }
                            else
                            {
                                var email = db.COM007.Where(x => x.t_emno == item.ActionBy).FirstOrDefault().t_mail;
                                if (!string.IsNullOrEmpty(email))
                                    emailto.Add(email);
                            }
                        }
                    }

                    List<string> emailCC = new List<string>();
                    string[] arrRecipient = recipient.Split(',');

                    foreach (var ps in arrRecipient)
                    {
                        var email = db.COM007.Where(x => x.t_emno == ps).FirstOrDefault().t_mail;
                        if (!string.IsNullOrEmpty(email))
                            emailCC.Add(email);
                    }

                    SendReportEmail(bal010.HeaderId, emailto, emailCC);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                objResponseMsg.headerid = bal010.HeaderId.ToString();

                return Json(objResponseMsg,
                            "application/json",
                            Encoding.UTF8,
                            JsonRequestBehavior.AllowGet);
            }
            else
            {
                BAL010 bal010 = new BAL010();
                if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                {
                    bal010.DateOfSubmit = DateTime.Now;
                }

                bal010.Project = project;
                bal010.Title = title;
                bal010.Recipient = recipient;
                bal010.RevNo = 0;
                bal010.Status = status;
                bal010.CreatedBy = objClsLoginInfo.UserName;
                bal010.CreatedOn = DateTime.Now;

                db.BAL010.Add(bal010);
                db.SaveChanges();

                var currentStatus = bal010.Status;
                var bAL010_LogId = 0;
                if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                {
                    //Insert log in BAL010_Log
                    BAL010_Log bAL010_Log = new BAL010_Log();
                    bAL010_Log.CreatedBy = bal010.CreatedBy;
                    bAL010_Log.CreatedOn = bal010.CreatedOn;
                    bAL010_Log.DateOfSubmit = bal010.DateOfSubmit;
                    bAL010_Log.EditedBy = bal010.EditedBy;
                    bAL010_Log.EditedOn = bal010.EditedOn;
                    bAL010_Log.HeaderId = bal010.HeaderId;
                    bAL010_Log.Project = bal010.Project;
                    bAL010_Log.Title = title;
                    bAL010_Log.Recipient = recipient;
                    bAL010_Log.RevNo = bal010.RevNo;
                    bAL010_Log.Status = status;
                    db.BAL010_Log.Add(bAL010_Log);
                    db.SaveChanges();
                    bAL010_LogId = bAL010_Log.Id;
                    //Log end
                }



                var lineGroup = lineList.GroupBy(x => x.Type);
                {
                    int idx = 0;
                    foreach (var grp in lineGroup)
                    {
                        foreach (var line in grp)
                        {
                            idx = idx + 1;
                            BAL011 bal011 = new BAL011();

                            bal011.ActionBy = line.ActionBy;
                            bal011.Activity = line.Activity;
                            bal011.CreatedBy = objClsLoginInfo.UserName;
                            bal011.CreatedOn = DateTime.Now;
                            bal011.HeaderId = bal010.HeaderId;
                            bal011.IsCompleted = line.IsCompleted;
                            bal011.Remarks = line.Remarks;
                            bal011.SrNo = idx;
                            bal011.Type = line.Type;

                            db.BAL011.Add(bal011);
                            db.SaveChanges();

                            //log
                            if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                            {
                                BAL011_Log bAL011_Log = new BAL011_Log();
                                bAL011_Log.RefId = bAL010_LogId;
                                bAL011_Log.ActionBy = bal011.ActionBy;
                                bAL011_Log.Activity = bal011.Activity;
                                bAL011_Log.CreatedBy = bal011.CreatedBy;
                                bAL011_Log.CreatedOn = bal011.CreatedOn;
                                bAL011_Log.EditedBy = bal011.EditedBy;
                                bAL011_Log.EditedOn = bal011.EditedOn;
                                bAL011_Log.HeaderId = bal011.HeaderId;
                                bAL011_Log.IsCompleted = bal011.IsCompleted;
                                bAL011_Log.LineId = bal011.LineId;
                                bAL011_Log.Remarks = bal011.Remarks;
                                bAL011_Log.SrNo = bal011.SrNo;
                                bAL011_Log.Type = bal011.Type;

                                db.BAL011_Log.Add(bAL011_Log);
                                db.SaveChanges();
                            }
                            //log end

                        }
                    }
                }

                if (status == clsImplementationEnum.BALStatus.Submitted.GetStringValue())
                {
                    List<string> emailto = new List<string>();
                    var personlist = lineList.Where(x => x.IsCompleted == false).ToList();

                    foreach (var item in personlist)
                    {
                        if (!string.IsNullOrEmpty(item.ActionBy))
                        {
                            if (item.ActionBy.Contains(","))
                            {
                                string[] psnolist = item.ActionBy.Split(',');
                                foreach (var ps in psnolist)
                                {
                                    var email = db.COM007.Where(x => x.t_emno == ps).FirstOrDefault().t_mail;
                                    if (!string.IsNullOrEmpty(email))
                                        emailto.Add(email);
                                }
                            }
                            else
                            {
                                var email = db.COM007.Where(x => x.t_emno == item.ActionBy).FirstOrDefault().t_mail;
                                if (!string.IsNullOrEmpty(email))
                                    emailto.Add(email);
                            }
                        }
                    }

                    List<string> emailCC = new List<string>();
                    string[] arrRecipient = recipient.Split(',');

                    foreach (var ps in arrRecipient)
                    {
                        var email = db.COM007.Where(x => x.t_emno == ps).FirstOrDefault().t_mail;
                        if (!string.IsNullOrEmpty(email))
                            emailCC.Add(email);
                    }


                    SendReportEmail(bal010.HeaderId, emailto, emailCC);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                objResponseMsg.headerid = bal010.HeaderId.ToString();

                return Json(objResponseMsg,
                            "application/json",
                            Encoding.UTF8,
                            JsonRequestBehavior.AllowGet);
            }



        }



        [NonAction]
        public void SendReportEmail(int strHeaderId, List<string> emailTo, List<string> emailCC)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DateTime? actionon = DateTime.Now;
                //List<string> emailTo = new List<string>();
                string emailfrom = "";
                emailfrom = objClsLoginInfo.UserName;


                BAL010 bAL010 = db.BAL010.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();

                #region Send Mail

                string BALNo = bAL010.HeaderId.ToString();
                #region generate pdf

                string filenameformat = bAL010.HeaderId.ToString();
                string downloadpath = "~/Resources/BALEmailAttachments/";
                string Reporturl = "/BAL/BAL_Report_All";

                List<SSRSParam> reportParams = new List<SSRSParam>();
                reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(bAL010.HeaderId) });

                string reportFilename = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);

                #endregion

                if (emailTo.Count > 0)
                {
                    try
                    {
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        //_ht["[CC]"] = ccTo;
                        _ht["[FromEmail]"] = Manager.GetUserNameFromPsNo(emailfrom);
                        _ht["[ProjectNo]"] = bAL010.Project;
                        _ht["[Title]"] = bAL010.Title;

                        System.Net.Mail.AttachmentCollection lstAttch = _objEmail.MailAttachMentCollection;

                        lstAttch.Add(new System.Net.Mail.Attachment(reportFilename));

                        MAIL001 objTemplateMaster = new MAIL001();
                        objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.BAL.BALReport).SingleOrDefault();
                        _objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
                        _objEmail.MailCc = string.Join(",", emailCC.Distinct());
                        _objEmail.MailAttachMentCollection = lstAttch;
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {

            }
        }



        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }


        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)

        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 ";
                if (param.Status.ToLower() == "pending")
                {
                    strWhereCondition += " and Status  in ('" + clsImplementationEnum.CFARStatus.Draft.GetStringValue() + "') and CreatedBy = '" + objClsLoginInfo.UserName.Trim() + "'";
                }
                //search Condition 

                string[] columnName = {
                           "Project",
                           "Status",
                           "RevNo",
                           "CreatedOn"};
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_BAL_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                //var lstResult = db.BAL010.ToList();

                var data = (from bal in lstResult
                            select new[]
                           {
                            Convert.ToString(bal.HeaderId),
                            Convert.ToString(bal.Project),
                            Convert.ToString(bal.Title),
                            Convert.ToString(bal.RevNo),
                            Convert.ToString(bal.Status),
                            bal.CreatedOn==null || bal.CreatedOn.Value==DateTime.MinValue?"":bal.CreatedOn.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                            Helper.GenerateActionIcon(bal.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/BAL/MaintainBAL/Details?headerId="+bal.HeaderId ,false)

                          }).ToList();


                //return Json(new
                //{
                //    sEcho = Convert.ToInt32(param.sEcho),
                //    iTotalRecords = (lstResult.Count > 0 && lstResult.Count() > 0 ? lstResult.Count() : 0),
                //    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.Count() > 0 ? lstResult.Count() : 0),
                //    aaData = data,
                //    whereCondition = strWhereCondition,
                //    strSortOrder = strSortOrder
                //}, JsonRequestBehavior.AllowGet);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        [NonAction]
        public List<ddlValue> GetAllEmployee(string param = "")
        {
            string[] arrayRoles = param.Split(',');
            var lstApproverList = db.ATH001.
                    Join(db.COM003, x => x.Employee, y => y.t_psno,
                    (x, y) => new { x, y })
                    .Where(z => z.y.t_actv == 1)
                    .Distinct()
                    .Select(z => new ddlValue
                    {
                        id = z.x.Employee,
                        text = z.x.Employee + " - " + z.y.t_name
                    }).Distinct().ToList();

            return lstApproverList;
        }


    }
}