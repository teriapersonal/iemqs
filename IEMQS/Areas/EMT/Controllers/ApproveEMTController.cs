﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.EMT.Controllers
{
    public class ApproveEMTController : clsBase
    {
        // GET: EMT/ApproveEMT
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetEMTGridDataPartial");
        }
        [HttpPost]
        public JsonResult LoadEMTHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";
                    strWhere += " and ApprovedBy=" + user;
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //strWhere += " and ApprovedBy=" + user;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_EMT_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                int flag = 0;
                foreach (var a in lstResult)
                {
                    if (!string.IsNullOrWhiteSpace(a.ApprovedBy))
                    {
                        string[] s = a.ApprovedBy.Split('-');
                        flag = s[0].Trim() == user ? 1 : 0;
                    }
                    else
                    {
                        flag = 0;
                        break;
                    }
                }
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                Convert.ToString(uc.Document),
                                Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("yyyy-MM-dd") : " "),
                                Convert.ToString(uc.Product),
                                Convert.ToString(uc.Status),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                uc.ApprovedBy ?? string.Empty,
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.CreatedBy==user),
                                Convert.ToString(uc.ApprovedBy==user),

                                Convert.ToString(flag)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetHistoryView(int headerid)
        {
            EMT001_Log objEMT001 = new EMT001_Log();
            Session["Headerid"] = Convert.ToInt32(headerid);
            return PartialView("EMTHistoryGridPartial", objEMT001);
        }
        [HttpPost]
        public ActionResult SaveEMTHeader(EMT001 objEMT001, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string project = objEMT001.Project.Split('-')[0].Trim();
                int? HeaderId = db.EMT001.Where(i => i.Project == project).Select(i => i.HeaderId).FirstOrDefault();

                if (HeaderId > 0)
                {
                    objEMT001 = db.EMT001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (!objEMT001.CDD.HasValue || (objEMT001.CDD.HasValue && objEMT001.CDD.Value.Year <= 1754))
                        objEMT001.CDD = null;
                    else
                        objEMT001.CDD = objEMT001.CDD;
                    if (string.IsNullOrEmpty(objEMT001.Customer))
                        objEMT001.Customer = " ";
                    else
                        objEMT001.Customer = objEMT001.Customer.Split('-')[0].Trim();
                    objEMT001.Project = project;
                    string rev = Regex.Replace(objEMT001.RevNo.ToString(), "[^0-9]+", string.Empty);
                    if (objEMT001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {

                        objEMT001.RevNo = Convert.ToInt32(rev) + 1;
                        objEMT001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    }
                    else
                    {
                        objEMT001.RevNo = Convert.ToInt32(Regex.Replace(objEMT001.RevNo.ToString(), "[^0-9]+", string.Empty));
                        // objEMT001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    }
                    objEMT001.Product = objEMT001.Product;
                    objEMT001.ProcessLicensor = objEMT001.ProcessLicensor;
                    objEMT001.ApprovedBy = objEMT001.ApprovedBy.Split('-')[0].Trim();
                    objEMT001.Document = objEMT001.Document;
                    objEMT001.EditedBy = objClsLoginInfo.UserName;
                    objEMT001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Status = objEMT001.Status;
                    objResponseMsg.RevNo = objEMT001.RevNo.ToString();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    var folderPath = "EMT001/" + HeaderId.ToString();
                    Manager.ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document, 0);
                }
                else
                {
                    EMT001 OBJEMT001 = new EMT001();
                    if (!objEMT001.CDD.HasValue || (objEMT001.CDD.HasValue && objEMT001.CDD.Value.Year <= 1754))
                        OBJEMT001.CDD = null;
                    else
                        OBJEMT001.CDD = objEMT001.CDD;
                    if (string.IsNullOrEmpty(objEMT001.Customer))
                        OBJEMT001.Customer = " ";
                    else
                        OBJEMT001.Customer = objEMT001.Customer.Split('-')[0].Trim();
                    OBJEMT001.Project = project;
                    string rev = Regex.Replace(objEMT001.RevNo.ToString(), "[^0-9]+", string.Empty);
                    OBJEMT001.RevNo = 0;
                    OBJEMT001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    OBJEMT001.Product = objEMT001.Product;
                    OBJEMT001.ProcessLicensor = objEMT001.ProcessLicensor;
                    OBJEMT001.ApprovedBy = objEMT001.ApprovedBy.Split('-')[0].Trim();
                    OBJEMT001.Document = objEMT001.Document;
                    OBJEMT001.CreatedBy = objClsLoginInfo.UserName;
                    OBJEMT001.CreatedOn = DateTime.Now;
                    db.EMT001.Add(OBJEMT001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    objResponseMsg.Status = OBJEMT001.Status;
                    objResponseMsg.RevNo = OBJEMT001.RevNo.ToString();
                    int Headerid = (from a in db.EMT001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();
                    var folderPath = "EMT001/" + Headerid.ToString();
                    Manager.ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document, 0);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public string getHeaderID(string proj)
        {
            return (from a in db.EMT001 where a.Project == proj select a.HeaderId).FirstOrDefault().ToString();
        }
        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Initiate_Exit_Meeting, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveSelectedEMT(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_EMT_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.EMT001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document, newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                       objEMT001.Project,
                                                       "",
                                                       "",
                                                       Manager.GetPDINDocumentNotificationMsg(objEMT001.Project, clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(), objEMT001.RevNo.Value.ToString(), objEMT001.Status),
                                                       clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                       Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(), objEMT001.HeaderId.ToString(), false),
                                                       objEMT001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ReturnSelectedEMT(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objEMT001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objEMT001.ApprovedBy = objClsLoginInfo.UserName;
                    objEMT001.ApprovedOn = DateTime.Now;
                    objEMT001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objEMT001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objEMT001.Project, clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(), objEMT001.RevNo.Value.ToString(), objEMT001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(), objEMT001.HeaderId.ToString(), false),
                                                        objEMT001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadEMTHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;

                //if (param.CTQCompileStatus != null && param.CTQCompileStatus.ToUpper() == "PENDING")
                //{
                //    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                //strWhere += "(CreatedBy= '" + user + "' or  ApprovedBy='" + user +"')";

                strWhere += "HeaderId = '" + Convert.ToInt32(Session["Headerid"]) + "'";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + strSortOrder + sortDirection + " ";
                }
                var lstResult = db.SP_EMT_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                            Convert.ToString(uc.Document),
                            Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                            Convert.ToString(uc.Product),
                            Convert.ToString(uc.ProcessLicensor),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                            Convert.ToString(uc.ApprovedBy),
                            Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [SessionExpireFilter]
        public ActionResult AddEMT(int Id = 0)
        {
            EMT001 objEMT001 = new EMT001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objEMT001 = db.EMT001.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objEMT001.ApprovedBy != null)
                {
                    if (objEMT001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        //return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objEMT001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue());
            }
            return View(objEMT001);
        }
        [SessionExpireFilter]
        public ActionResult EMTHistoryDetail(int Id = 0)
        {
            EMT001_Log objEMT001 = new EMT001_Log();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objEMT001 = db.EMT001_Log.Where(x => x.Id == Id).FirstOrDefault();
                //if (objEMT001.ApprovedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            ViewBag.RevPrev = (db.EMT001_Log.Any(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo - 1))) ? urlPrefix + db.EMT001_Log.Where(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo - 1))).FirstOrDefault().Id : null);
            ViewBag.RevNext = (db.EMT001_Log.Any(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo + 1))) ? urlPrefix + db.EMT001_Log.Where(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo + 1))).FirstOrDefault().Id : null);
            return View(objEMT001);
        }

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {//header grid data

                    var lst = db.SP_EMT_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      CDD = Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("yyyy-MM-dd") : " "),
                                      Product = Convert.ToString(uc.Product),
                                      Status = Convert.ToString(uc.Status),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //history header
                    var lst = db.SP_EMT_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      CDD = uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? " " : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = Convert.ToString(uc.Product),
                                      Status = Convert.ToString(uc.Status),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}