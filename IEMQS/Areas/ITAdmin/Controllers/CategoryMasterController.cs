﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ITAdmin.Controllers
{
    public class CategoryMasterController : clsBase
    {
        // GET: ITAdmin/CategoryMaster
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {
                // IEnumerable<ATH001> _List = new List<ATH001>();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                //var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);

                //string SortingFields = //param.iSortCols;

                var lstResult = db.SP_ATH_GET_Category_DETAILS
                                (
                                StartIndex, EndIndex, "" , ""
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Category),
                           Convert.ToString(uc.Description),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.FirstOrDefault().TotalCount,
                    iTotalDisplayRecords = lstResult.FirstOrDefault().TotalCount,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            GLB001 objGLB001 = new GLB001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var Category = (from a in db.GLB001
                            where a.Id == Id
                            select new { a.Category }).FirstOrDefault(); 
         

            var Description = (from a in db.GLB001
                               where a.Id == Id
                               select new { a.Description }).FirstOrDefault();


             objResponseMsg.Value =  Category + "|" + Description + "|" + Id;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            //return PartialView("GetGlobalDataHtml", objGLB001);
        }

        [HttpPost]
        public ActionResult SaveCategory(GLB001 glb001, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (glb001.Id > 0)
                {
                    GLB001 objGLB001 = db.GLB001.Where(x => x.Id == glb001.Id).FirstOrDefault();
                    objGLB001.Category = fc["txtCode"].ToString().Trim();
                    objGLB001.Description = fc["txtDesc"].ToString().Trim();
                    objGLB001.EditedBy = objClsLoginInfo.UserName;
                    objGLB001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {

                    GLB001 objGLB001 = new GLB001();
                    objGLB001.Category = fc["txtCode"].ToString().Trim();
                    objGLB001.Description = fc["txtDesc"].ToString().Trim();
                    objGLB001.IsActive = true;
                    objGLB001.CreatedBy = objClsLoginInfo.UserName;
                    objGLB001.CreatedOn = DateTime.Now;
                    db.GLB001.Add(objGLB001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteGDMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                GLB001 objGLB001 = db.GLB001.Where(x => x.Id == Id).FirstOrDefault();
                objGLB001.IsActive = false;
                objGLB001.EditedBy = objClsLoginInfo.UserName;
                objGLB001.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateGDMatrix(GLB001 glb001, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var id = Convert.ToInt32(fc["hfId"].ToString());
                if (Convert.ToInt32(fc["hfId"].ToString()) > 0)
                {
                    GLB001 objGLB001 = db.GLB001.Where(x => x.Id == id).FirstOrDefault();
                    objGLB001.Category = fc["txtCode"].ToString().Trim();
                    objGLB001.Description = fc["txtDesc"].ToString().Trim();
                    objGLB001.EditedBy = objClsLoginInfo.UserName;
                    objGLB001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}