﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.WQ.Models;

namespace IEMQS.Areas.WPP.Controllers
{
    public class WPPKeyMasterController : clsBase
    {
        // GET: WPP/WPPKeyMaster
        #region Grid
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = "WPP Key Master";
            return View();
        }

        public ActionResult LoadWPPKeyListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wpp3.Location", lstLoc);

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (wpp3.Location like '%" + param.sSearch + "%' or wpp3.Key1 like '%" + param.sSearch + "%' or wpp3.Key2 like '%" + param.sSearch + "%' or wpp3.JointType like '%" + param.sSearch + "%' or wpp3.CreatedBy like '%" + param.sSearch + "%')";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPP_GET_WPPKEY(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.Id),
                        a.Location,
                        Convert.ToString(a.Key1),
                        Convert.ToString(a.Key2),
                        a.JointType,
                        a.CreatedBy,
                        Convert.ToDateTime(a.CreatedOn).ToString("dd/MM/yyyy"),
                         "<center>"+ Helper.GenerateActionIcon(a.Id, "Edit", "Edit Record", "fa fa-pencil-square-o iconspace editline", "AddWPPKeyMaster("+a.Id+",'"+ a.InUsed.ToString().ToLower()+"')","",false)+
                        Helper.GenerateActionIcon(a.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteWPPKeyMaster("+a.Id+")","",(a.InUsed.ToString().ToLower() == "false"?false:true))+"</center>"
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]

        public ActionResult WPPKeyMasterPartial(int id, string InUsed)
        {
            WPP003 objWPP003 = new WPP003();
            NDEModels objNDEModels = new NDEModels();
            string location = string.Empty;
            if (id > 0)
            {
                objWPP003 = db.WPP003.Where(i => i.Id == id).FirstOrDefault();

                ViewBag.ParentMetal1Selected = GetParentMaterialDesc(objWPP003.Parentmetal1);
                ViewBag.ParentMetal2Selected = GetParentMaterialDesc(objWPP003.Parentmetal2);
                ViewBag.ParentMetal3Selected = GetParentMaterialDesc(objWPP003.Parentmetal3);

                ViewBag.WeldingProcess1Selected = objNDEModels.GetCategory(objWPP003.WeldingProcess1).CategoryDescription;
                ViewBag.WeldingProcess2Selected = objNDEModels.GetCategory(objWPP003.WeldingProcess2).CategoryDescription;
                ViewBag.WeldingProcess3Selected = objNDEModels.GetCategory(objWPP003.WeldingProcess3).CategoryDescription;
                ViewBag.JointTypeSelected = objNDEModels.GetCategory(objWPP003.JointType).CategoryDescription;
                ViewBag.PWHTSelected = objNDEModels.GetCategory(objWPP003.PWHT).CategoryDescription;

                location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objWPP003.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                objWPP003.Location = objClsLoginInfo.Location;
            }
            else
            {
                objWPP003.Location = objClsLoginInfo.Location;
                location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault(); ;
            }
            ModelWQRequest objModelWQRequest = new ModelWQRequest();

            ViewBag.Location = location;
            ViewBag.ParentMetal = GetParentMaterial().Select(i => new { i.PNO1, i.DESCRIPTION }).Distinct().Select(i => new CategoryData { Value = i.PNO1, CategoryDescription = i.PNO1 + "-" + i.DESCRIPTION });
            //GetSubCatagory("Parent Metal", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.WeldingProcess = GetSubCatagory("Welding Process", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();
            var jointtype = GetSubCatagory("Joint Type", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct();
            ViewBag.JointType = jointtype.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.PWHT = GetSubCatagory("PWHT", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });

            ViewBag.InUsed = InUsed;

            return PartialView("_AddWPPKeyPartial", objWPP003);
        }

        [HttpPost]
        public ActionResult SaveDeatils(WPP003 obj)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPP003 objWPP003 = new WPP003();
            try
            {
                if (!isWPPKeyExist(obj.Location.Split('-')[0], Convert.ToInt32(obj.Key1), obj.Id))
                {
                    if (obj.Id > 0)
                    {
                        #region Update WPP Key Existing
                        objWPP003 = db.WPP003.Where(i => i.Id == obj.Id).FirstOrDefault();

                        if (isWPPKeyExistInWPPNoIndex(obj.Location.Split('-')[0], objWPP003.Key1))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPPKeyMaster.RestrictUpdate, objWPP003.Key1);
                        }
                        else
                        {
                            objWPP003.Parentmetal1 = obj.Parentmetal1;
                            objWPP003.Parentmetal2 = obj.Parentmetal2;
                            objWPP003.Parentmetal3 = obj.Parentmetal3;

                            objWPP003.WeldingProcess1 = obj.WeldingProcess1;
                            objWPP003.WeldingProcess2 = obj.WeldingProcess2;
                            objWPP003.WeldingProcess3 = obj.WeldingProcess3;

                            objWPP003.JointType = obj.JointType;
                            objWPP003.PWHT = obj.PWHT;
                            objWPP003.Remarks = obj.Remarks;
                            objWPP003.EditedBy = objClsLoginInfo.UserName;
                            objWPP003.EditedOn = DateTime.Now;

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.WPPKeyMaster.Save;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Add New WPP Key
                        objWPP003.Location = obj.Location.Split('-')[0];
                        objWPP003.Key1 = obj.Key1;
                        objWPP003.Key2 = obj.Key2;
                        objWPP003.Parentmetal1 = obj.Parentmetal1;
                        objWPP003.Parentmetal2 = obj.Parentmetal2;
                        objWPP003.Parentmetal3 = obj.Parentmetal3;

                        objWPP003.WeldingProcess1 = obj.WeldingProcess1;
                        objWPP003.WeldingProcess2 = obj.WeldingProcess2;
                        objWPP003.WeldingProcess3 = obj.WeldingProcess3;

                        objWPP003.JointType = obj.JointType;
                        objWPP003.PWHT = obj.PWHT;
                        objWPP003.Remarks = obj.Remarks;
                        objWPP003.CreatedBy = objClsLoginInfo.UserName;
                        objWPP003.CreatedOn = DateTime.Now;

                        db.WPP003.Add(objWPP003);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.WPPKeyMaster.Save;
                        #endregion
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = obj.Key1 + " Key Combination Already Exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWPPKeyMaster(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPP003 objWPP003 = new WPP003();
            try
            {
                if (id > 0)
                {
                    objWPP003 = db.WPP003.Where(i => i.Id == id).FirstOrDefault();
                    if (objWPP003 != null)
                    {
                        if (isWPPKeyExistInWPPNoIndex(objWPP003.Location, objWPP003.Key1))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPPKeyMaster.RestrictDelete, objWPP003.Key1);
                        }
                        else
                        {
                            db.WPP003.Remove(objWPP003);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPPKeyMaster.Delete, objWPP003.Key1);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record Doesn't Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common function
        public bool isWPPKeyExist(string location, int key1, int id = 0)
        {
            bool isWPPKeyExist = false;
            var WPP3 = db.WPP003.Where(i => i.Location.Trim().Equals(location.Trim(), StringComparison.OrdinalIgnoreCase) && i.Key1 == key1 && i.Id != id).ToList();
            if (WPP3.Any())
                isWPPKeyExist = true;

            return isWPPKeyExist;
        }

        public bool isWPPKeyExistInWPPNoIndex(string location, int? key1)//, int? key2)
        {
            bool isWPPKeyExistInWPPNoIndex = false;
            if (db.WPP004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Key1 == key1).Any())// && i.Key2 == key2).Any())
                isWPPKeyExistInWPPNoIndex = true;
            return isWPPKeyExistInWPPNoIndex;
        }
        public List<GLB002> GetSubCatagory(string Key, string strLoc)
        {
            List<GLB002> lstGLB002 = (from glb002 in db.GLB002
                                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                                      select glb002).ToList();
            //var category = lstGLB002.Select(i => new { i.Code, i.Description }).Distinct().ToList();
            return lstGLB002;
        }
        public List<WPS005> GetParentMaterial()
        {
            List<WPS005> lstWPS005 = (from wps in db.WPS005
                                      select wps).ToList();
            return lstWPS005;
        }
        public string GetParentMaterialDesc(string code)
        {
            string parentmaterial = (from wps in db.WPS005
                                     where wps.PNO1.Trim() == code.Trim()
                                     select wps.PNO1 + "-" + wps.DESCRIPTION).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(parentmaterial))
            {
                parentmaterial = code;
            }
            return parentmaterial;
        }

        #endregion

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                List<SP_WPP_GET_WPPKEY_Result> lst = db.SP_WPP_GET_WPPKEY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Key1 = li.Key1,
                                  JointType = li.JointType,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  Location = li.Location
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}