﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;

namespace IEMQS.Areas.WPP.Controllers
{
    public class WPPNoIndexMasterController : clsBase
    {
        // GET: WPP/WPPNoIndexMaster

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }
        #region Grid 
        public ActionResult LoadWPPNoIndexListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += Manager.MakeStringInCondition("wpp4.Location", lstLoc);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (wpp4.Location like '%" + param.sSearch + "%' or wpp4.Key1 like '%" + param.sSearch + "%' or wpp4.Key2 like '%" + param.sSearch + "%' or wpp4.QualityProject like '%" + param.sSearch + "%' or wpp4.CreatedBy like '%" + param.sSearch + "%')";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPP_GET_NUMBER_INDEX(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                                Convert.ToString(a.Id),
                                 a.Location,
                                Convert.ToString(a.Key1),
                                Convert.ToString(a.Key2),
                                a.QualityProject,
                                a.WPPNumber,
                                a.CreatedBy,
                                Convert.ToDateTime(a.CreatedOn).ToString("dd/MM/yyyy"),
                               
                              "<center>"+  Helper.GenerateActionIcon(a.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteWPPIndexMaster("+a.Id+")","",(a.InUsed.ToString().ToLower() == "false"?false:true))+"</center>"
                              };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add/Update Pop Up
        [SessionExpireFilter]
        public ActionResult WPPNoIndexMasterPartial()
        {
            WPP004 objWPP004 = new WPP004();
            var location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            objWPP004.Location = objClsLoginInfo.Location;
            ViewBag.Location = location;
            var loc = location.Split('-')[0];
            var lstKey1 = db.WPP003.Where(i => i.Location.Trim().Equals(objWPP004.Location.Trim(), StringComparison.Ordinal)).OrderBy(i => i.Key1).Select(i => i.Key1).Distinct().ToList();
            ViewBag.Key1 = lstKey1.Select(i => new CategoryData { Value = i.Value.ToString() }).Distinct().ToList();
            return PartialView("_AddWPPNoIndexPartial", objWPP004);
        }

        [HttpPost]
        public ActionResult SaveDetails(WPP004 wpp004)
         {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPP004 objWPP004 = new WPP004();
            try
            {

                int id = wpp004.Id;
                string location = wpp004.Location;
                if (!isWPPIndexExist(location, Convert.ToInt32(wpp004.Key1), wpp004.QualityProject, id))
                {
                    if (id > 0)
                    {
                        #region WPP Number Update
                        if (!isWPPNoExistInWPP(objWPP004.Location, objWPP004.WPPNumber))
                        {
                            objWPP004 = db.WPP004.Where(i => i.Id == id).FirstOrDefault();
                            objWPP004.Key1 = wpp004.Key1;
                            objWPP004.Key2 = wpp004.Key2;
                            objWPP004.QualityProject = wpp004.QualityProject;
                            objWPP004.WPPNumber = wpp004.WPPNumber;
                            objWPP004.EditedBy = objClsLoginInfo.UserName;
                            objWPP004.EditedOn = DateTime.Now;
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.WPPNoIndexMaster.Save;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format("WPP : {0} cannot be modified, it is already used to generate WPP", objWPP004.WPPNumber);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Add New WPP Number Index
                        objWPP004.Location = location;
                        objWPP004.Key1 = wpp004.Key1;
                        objWPP004.Key2 = wpp004.Key2;
                        objWPP004.QualityProject = wpp004.QualityProject;
                        objWPP004.WPPNumber = wpp004.WPPNumber;
                        objWPP004.CreatedBy = objClsLoginInfo.UserName;
                        objWPP004.CreatedOn = DateTime.Now;
                        db.WPP004.Add(objWPP004);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.WPPNoIndexMaster.Save;
                        #endregion
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "WPP No Already Exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWPPNoIndex(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPP004 objWPP004 = new WPP004();
            try
            {
                if (id > 0)
                {
                    objWPP004 = db.WPP004.Where(i => i.Id == id).FirstOrDefault();
                    if (!isWPPNoExistInWPP(objWPP004.Location, objWPP004.WPPNumber))
                    {
                        if (objWPP004 != null)
                        {
                            db.WPP004.Remove(objWPP004);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPPNoIndexMaster.Delete, objWPP004.WPPNumber);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Record Doesn't Exist";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPPNoIndexMaster.RestrictDelete, objWPP004.WPPNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common Function
        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term, string location)
        {
            List<Projects> lstQualityProject = new List<Projects>();
            if (string.IsNullOrWhiteSpace(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                                                ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                    && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                                                   ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetKey2(int key1, string location)
        {
            var Key2 = db.WPP003.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) &&
                                                             i.Key1 == key1).OrderBy(i => i.Key2).Select(i => i.Key2).Distinct().ToList();

            List<CategoryData> lstKey2 = Key2.Select(i => new CategoryData { Value = i.Value.ToString() }).Distinct().ToList();

            return Json(lstKey2, JsonRequestBehavior.AllowGet);
        }

        public bool isWPPNoExistInWPP(string location, string WPPNo)
        {
            bool isWPPNoExistInWPP = false;
            //if (db.WPP010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.WPPNumber.Equals(WPPNo, StringComparison.OrdinalIgnoreCase)).Any())
            //    isWPPNoExistInWPP = true;

            return isWPPNoExistInWPP;
        }

        public bool isWPPIndexExist(string location, int key1, string qualityProject, int id)
        {
            bool isWPPIndexExist = false;
            if (db.WPP004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Key1 == key1 && i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Id != id).Any())
                isWPPIndexExist = true;

            return isWPPIndexExist;
        }
        #endregion

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                List<SP_WPP_GET_NUMBER_INDEX_Result> lst = db.SP_WPP_GET_NUMBER_INDEX(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Key1 = li.Key1,
                                  Key2 = li.Key2,
                                  QualityProject = li.QualityProject,
                                  WPPNumber = li.WPPNumber,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  Location = li.Location
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}