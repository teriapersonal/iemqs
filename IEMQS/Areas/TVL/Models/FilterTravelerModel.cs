﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.TVL.Models
{
    public class FilterTravelerModel
    {
        public string TravelerNo { get; set; }
        public int TravelRev { get; set; }
        public string Activity { get; set; }
        public string identification { get; set; }
        public string identificationno { get; set; }
        public string TravelerRevDetails { get; set; }
        public string TravelerDetails { get; set; }
        public string ProjectNo { get; set; }
        public string Customer { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string ProjectDesc { get; set; }

        public string TravelerDescription { get; set; }
    }

    public class FilterReqTravelerModel
    {
        public string ProjectNo { get; set; }
        public string Customer { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string TravelerNo { get; set; }
        public int TravelRev { get; set; }
        public string IdentificationNo { get; set; }
        public string Activity { get; set; }
        public decimal OperationNo { get; set; }
        public int OperationRevNo { get; set; }
        public int SubAssemblyNo { get; set; }
        public int IterationNo { get; set; }
        public int HeaderId { get; set; }

    }
}