﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.TVL.Controllers
{
    public class AttendTPIInspectionController : clsBase
    {
        // GET: TVL/AttendTPIInspection
        #region Index Grid
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index(string tab)
        {
            ViewBag.Title = "Attend TPI Inspection";
            ViewBag.Tab = tab;
            return View();
        }

        [HttpPost]
        public ActionResult LoadTPIInspectionIndexDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetIndexGridDataPartial");
        }
        [HttpPost]
        public JsonResult LoadInspectionIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                bool IsCR1 = objClsLoginInfo.GetUserRoleList().Any(x => x.ToString() == clsImplementationEnum.UserRoleName.CR1.GetStringValue());
                bool IsCR2 = objClsLoginInfo.GetUserRoleList().Any(x => x.ToString() == clsImplementationEnum.UserRoleName.CR2.GetStringValue());
                string tpiReturned = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
                string tpiCleared = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
                string tpiWaved = clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue();

                strWhereCondition = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhereCondition += " and TestResult='" + tpiCleared + "'";
                    if (IsCR1 && IsCR2)
                    {
                        strWhereCondition += " and (ISNULL(I1Intervention, '') != '' or ISNULL(I2Intervention,'')!= '' or ISNULL(I3Intervention,'')!= '' or ISNULL(I4Intervention,'')!= '' )"
                                                + " and ((ISNULL(I1Intervention, '') != '' and (ISNULL(I1Result, '') != '" + tpiCleared + "'  and ISNULL(I1Result, '') != '" + tpiWaved + "')) or"
                                                + "(ISNULL(I2Intervention, '') != '' and (ISNULL(I2Result, '') != '" + tpiCleared + "'  and ISNULL(I2Result, '') != '" + tpiWaved + "')) or"
                                                + "(ISNULL(I3Intervention, '') != '' and (ISNULL(I3Result, '') != '" + tpiCleared + "'  and ISNULL(I3Result, '') != '" + tpiWaved + "')) or"
                                                + "(ISNULL(I4Intervention, '') != '' and (ISNULL(I4Result, '') != '" + tpiCleared + "'  and ISNULL(I4Result, '') != '" + tpiWaved + "')))"
                                                + " and (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "' and ISNULL(I4Result, '') != '" + tpiReturned + "') ";
                    }
                    else
                    {
                        if (IsCR1)
                        {
                            strWhereCondition += " and ((ISNULL(I1Intervention, '') != '' and(ISNULL(I1Result, '') != '" + tpiCleared + "'  and ISNULL(I1Result, '') != '" + tpiWaved + "'))"
                                                 + " or ((ISNULL(I1Intervention, '') != '' and(ISNULL(I1Result, '') != '" + tpiCleared + "'  and ISNULL(I1Result, '') != '" + tpiWaved + "'))) "
                                                 + " or(ISNULL(I2Intervention, '') != '' and(ISNULL(I2Result, '') != '" + tpiCleared + "'  and ISNULL(I2Result, '') != '" + tpiWaved + "') and(ISNULL(I3Intervention, '') != '' or ISNULL(I4Intervention, '') != ''))"
                                                 + " or(ISNULL(I3Intervention, '') != '' and(ISNULL(I3Result, '') != '" + tpiCleared + "'  and ISNULL(I3Result, '') != '" + tpiWaved + "') and(ISNULL(I4Intervention, '') != ''))) "
                                                 + " and(ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "' and ISNULL(I4Result, '') != '" + tpiReturned + "')";
                        }
                        if (IsCR2)
                        {
                            strWhereCondition += "and  ((((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != '') or(ISNULL(I1Intervention, '') = '' and ISNULL(I1Result, '') = '')) and(ISNULL(I2Intervention, '') != '' and(ISNULL(I2Result, '') != '" + tpiCleared + "'  and ISNULL(I2Result, '') != '" + tpiWaved + "')) and(ISNULL(I3Intervention, '') = '' and ISNULL(I4Intervention, '') = ''))  "//-Condition for I2
                                              + " or (((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != '') or(ISNULL(I1Intervention, '') = '' and ISNULL(I1Result, '') = '')) and((ISNULL(I2Intervention, '') != '' and ISNULL(I2Result, '') != '') or(ISNULL(I2Intervention, '') = '' and ISNULL(I2Result, '') = '')) and(ISNULL(I3Intervention, '') != '' and(ISNULL(I3Result, '') != '" + tpiCleared + "'  and ISNULL(I3Result, '') != '" + tpiWaved + "')) and ISNULL(I4Intervention, '') = '' )  "//Condition for I3
                                              + " or (((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != '') or(ISNULL(I1Intervention, '') = '' and ISNULL(I1Result, '') = '')) and((ISNULL(I2Intervention, '') != '' and ISNULL(I2Result, '') != '') or(ISNULL(I2Intervention, '') = '' and ISNULL(I2Result, '') = ''))  and((ISNULL(I3Intervention, '') != '' and ISNULL(I3Result, '') != '') or(ISNULL(I3Intervention, '') = '' and ISNULL(I3Result, '') = '')) and(ISNULL(I4Intervention, '') != '' and(ISNULL(I4Result, '') != '" + tpiCleared + "'  and ISNULL(I4Result, '') != '" + tpiWaved + "'))))"//Condition for I4
                                              + " and  (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "'  and ISNULL(I4Result, '') != '" + tpiReturned + "')and (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "'  and ISNULL(I4Result, '') != '" + tpiReturned + "')";
                        }
                    }
                }

                //search Condition 
                string[] columnName = { "ProjectNo", "TravelerNo", "TravelerRevNo", "BU", "AssemblyNo", "Location" };
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl50.BU", "tvl50.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_INSPECTION_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString("R"+uc.TravelerRevNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendTPIInspection/AttendTPIInspectionDetails/"+uc.HeaderId,false)
                            +   Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.Project+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.AssemblyNo+"','"+uc.SubAssemblyNo+"')","",false)

                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Inspection detail form
        [SessionExpireFilter]
        public ActionResult AttendTPIInspectionDetails(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            TVL050 objTVL050 = new TVL050();
            ViewBag.Title = "Attend TPI Inspection";
            objTVL050 = db.TVL050.Where(x => x.RequestId == Id).FirstOrDefault();
            if (objTVL050 != null)
            {
                objTVL050.BU = db.COM002.Where(x => x.t_dimx == objTVL050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.Location = db.COM002.Where(x => x.t_dimx == objTVL050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                ViewBag.ProjectNo = objTVL050.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL050.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault();
            }
            return View(objTVL050);
        }

        [HttpPost]
        public ActionResult LoadTPIInspectionDataPartial(string status, TVL050 objTVL050, TVL040 objTVL040)
        {
            if (status != "All")
            {
                objTVL050.BU = objTVL050.BU.Split('-')[0];
                objTVL050.Location = objTVL050.Location.Split('-')[0];
                ViewBag.Status = status;
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL050.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4";
                    }

                }
                return PartialView("_GetTPIInspectionGridDataPartial", objTVL050);
            }
            else
            {
                objTVL040.BU = objTVL040.BU.Split('-')[0];
                objTVL040.Location = objTVL040.Location.Split('-')[0];
                ViewBag.Status = status;
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL040.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4";
                    }
                }
            }
            return PartialView("_GetOfferOperationGridDataPartial", objTVL040);


        }

        [HttpPost]
        public JsonResult LoadTPIInspectionData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                string tpiReturned = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
                string tpiCleared = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
                string tpiWaved = clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue();
                strWhereCondition = "1=1";
                bool IsCR1 = objClsLoginInfo.GetUserRoleList().Any(x => x.ToString() == clsImplementationEnum.UserRoleName.CR1.GetStringValue());
                bool IsCR2 = objClsLoginInfo.GetUserRoleList().Any(x => x.ToString() == clsImplementationEnum.UserRoleName.CR2.GetStringValue());
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhereCondition += " and TestResult='" + tpiCleared + "'";
                    if (IsCR1 && IsCR2)
                    {
                        strWhereCondition += " and (ISNULL(I1Intervention, '') != '' or ISNULL(I2Intervention,'')!= '' or ISNULL(I3Intervention,'')!= '' or ISNULL(I4Intervention,'')!= '' )"
                                          + " and ((ISNULL(I1Intervention, '') != '' and (ISNULL(I1Result, '') != '" + tpiCleared + "'  and ISNULL(I1Result, '') != '" + tpiWaved + "')) or"
                                          + "(ISNULL(I2Intervention, '') != '' and (ISNULL(I2Result, '') != '" + tpiCleared + "'  and ISNULL(I2Result, '') != '" + tpiWaved + "')) or"
                                          + "(ISNULL(I3Intervention, '') != '' and (ISNULL(I3Result, '') != '" + tpiCleared + "'  and ISNULL(I3Result, '') != '" + tpiWaved + "')) or"
                                          + "(ISNULL(I4Intervention, '') != '' and (ISNULL(I4Result, '') != '" + tpiCleared + "'  and ISNULL(I4Result, '') != '" + tpiWaved + "')))"
                                          + " and (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "' and ISNULL(I4Result, '') != '" + tpiReturned + "') ";
                    }
                    else
                    {
                        if (IsCR1)
                        {
                            strWhereCondition += " and ((ISNULL(I1Intervention, '') != '' and(ISNULL(I1Result, '') != '" + tpiCleared + "'  and ISNULL(I1Result, '') != '" + tpiWaved + "'))"
                                               + " or ((ISNULL(I1Intervention, '') != '' and(ISNULL(I1Result, '') != '" + tpiCleared + "'  and ISNULL(I1Result, '') != '" + tpiWaved + "'))) "
                                               + " or (ISNULL(I2Intervention, '') != '' and(ISNULL(I2Result, '') != '" + tpiCleared + "'  and ISNULL(I2Result, '') != '" + tpiWaved + "') and(ISNULL(I3Intervention, '') != '' or ISNULL(I4Intervention, '') != ''))"
                                               + " or (ISNULL(I3Intervention, '') != '' and(ISNULL(I3Result, '') != '" + tpiCleared + "'  and ISNULL(I3Result, '') != '" + tpiWaved + "') and(ISNULL(I4Intervention, '') != ''))) "
                                               + " and (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "' and ISNULL(I4Result, '') != '" + tpiReturned + "')";

                            //strWhereCondition += " and(((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention, '') = '') AND(ISNULL(I1Intervention, '') != '' and (ISNULL(I1Result, '') != '"+tpiCleared+"'  and ISNULL(I1Result, '') != '" + tpiWaved + "')))"
                            //                  + " or((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention, '') != ''  and  ISNULL(I3Intervention, '') = '') and((ISNULL(I1Intervention, '') != '' and (ISNULL(I1Result, '') != '"+tpiCleared+"'  and ISNULL(I1Result, '') != '" + tpiWaved + "')))) "
                            //                  + " or(((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention, '') != ''  and  ISNULL(I3Intervention, '') != '' and  ISNULL(I4Intervention, '') = '')"
                            //                  + " and(ISNULL(I2Intervention, '') != '' and ISNULL(I2Intervention, '') != '' and (ISNULL(I2Result, '') != '"+tpiCleared+"'  and ISNULL(I2Result, '') != '" + tpiWaved + "'))))"
                            //                  + " OR(((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention, '') != ''  and  ISNULL(I3Intervention, '') != '' and  ISNULL(I4Intervention, '') != '')"
                            //                  + " and(ISNULL(I3Intervention, '') != '' and ISNULL(I3Intervention, '') != '' and (ISNULL(I3Result, '') != '"+tpiCleared+"'  and ISNULL(I3Result, '') != '" + tpiWaved + "')))))"
                            //                  + " and(ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "' and ISNULL(I4Result, '') != '" + tpiReturned + "')";

                        }
                        if (IsCR2)
                        {
                            strWhereCondition += "and  ((((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != '') or(ISNULL(I1Intervention, '') = '' and ISNULL(I1Result, '') = '')) and(ISNULL(I2Intervention, '') != '' and(ISNULL(I2Result, '') != '" + tpiCleared + "'  and ISNULL(I2Result, '') != '" + tpiWaved + "')) and(ISNULL(I3Intervention, '') = '' and ISNULL(I4Intervention, '') = ''))  "//-Condition for I2
                           + " or (((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != '') or(ISNULL(I1Intervention, '') = '' and ISNULL(I1Result, '') = '')) and((ISNULL(I2Intervention, '') != '' and ISNULL(I2Result, '') != '') or(ISNULL(I2Intervention, '') = '' and ISNULL(I2Result, '') = '')) and(ISNULL(I3Intervention, '') != '' and(ISNULL(I3Result, '') != '" + tpiCleared + "'  and ISNULL(I3Result, '') != '" + tpiWaved + "')) and ISNULL(I4Intervention, '') = '' )  "//Condition for I3
                           + " or (((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != '') or(ISNULL(I1Intervention, '') = '' and ISNULL(I1Result, '') = '')) and((ISNULL(I2Intervention, '') != '' and ISNULL(I2Result, '') != '') or(ISNULL(I2Intervention, '') = '' and ISNULL(I2Result, '') = ''))  and((ISNULL(I3Intervention, '') != '' and ISNULL(I3Result, '') != '') or(ISNULL(I3Intervention, '') = '' and ISNULL(I3Result, '') = '')) and(ISNULL(I4Intervention, '') != '' and(ISNULL(I4Result, '') != '" + tpiCleared + "'  and ISNULL(I4Result, '') != '" + tpiWaved + "'))))"//Condition for I4
                           + " and  (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "'  and ISNULL(I4Result, '') != '" + tpiReturned + "')and (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "'  and ISNULL(I4Result, '') != '" + tpiReturned + "')";

                            //strWhereCondition += "and  (((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention,'')!= ''  and  ISNULL(I3Intervention,'') = '') and ((ISNULL(I1Intervention, '') != '' and  (ISNULL(I1Result, '') != '"+tpiCleared+"'  and ISNULL(I1Result, '') != '" + tpiWaved + "') ) "
                            //                  + "and (ISNULL(I2Intervention, '') != '' and (ISNULL(I2Result, '') != '"+tpiCleared+"'  and ISNULL(I2Result, '') != '" + tpiWaved + "') ) )) or  ((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention,'')!= ''  and  ISNULL(I3Intervention,'') != '' and  ISNULL(I4Intervention,'')= '') "
                            //                  + "and ((ISNULL(I1Intervention, '') != '' and (ISNULL(I1Result, '') != '"+tpiCleared+"'  and ISNULL(I1Result, '') != '" + tpiWaved + "') and ISNULL(I2Intervention, '') != '' and (ISNULL(I2Result, '') != '"+tpiCleared+"'  and ISNULL(I2Result, '') != '" + tpiWaved + "') and ISNULL(I3Intervention, '') != '' and (ISNULL(I3Result, '') != '"+tpiCleared+"'  and ISNULL(I3Result, '') != '" + tpiWaved + "') ))) "
                            //                  + "or  ((ISNULL(I1Intervention, '') != '' and ISNULL(I2Intervention,'')!= ''  and  ISNULL(I3Intervention,'') != '' and  ISNULL(I4Intervention,'') != '') and ((ISNULL(I1Intervention, '') != '' and (ISNULL(I1Result, '') != '"+tpiCleared+"'  and ISNULL(I1Result, '') != '" + tpiWaved + "') and ISNULL(I2Intervention, '') != '' and (ISNULL(I2Result, '') != '"+tpiCleared+"'  and ISNULL(I2Result, '') != '" + tpiWaved + "') "
                            //                  + "and ISNULL(I3Intervention, '') != '' and (ISNULL(I3Result, '') != '"+tpiCleared+"'  and ISNULL(I3Result, '') != '" + tpiWaved + "')  and ISNULL(I4Intervention, '') != '' and (ISNULL(I4Result, '') != '"+tpiCleared+"'  and ISNULL(I4Result, '') != '" + tpiWaved + "') ))))"
                            //                  + "  and  (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "'  and ISNULL(I4Result, '') != '" + tpiReturned + "')and (ISNULL(I1Result, '') != '" + tpiReturned + "' and ISNULL(I2Result, '') != '" + tpiReturned + "' and ISNULL(I3Result, '') != '" + tpiReturned + "'  and ISNULL(I4Result, '') != '" + tpiReturned + "')";
                        }
                    }
                }
                strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                  + " and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                  + " and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                  + " and SubAssemblyNo=" + Convert.ToInt32(param.subassembly);
                //search Condition 
                string[] columnName = { "RequestNo","ReqSequenceNo","ReqIterationNo","OperationNo","OperationRevisionNo",
                                        "Activity","RefDocument","RefDocRevNo","TestResult","OfferedBy","OfferedOn",
                                        "InspectedBy","InspectedOn","QCRemark","OfferedtoCusomerBy",  "OfferedtoCusomerOn",
                                        "I1Intervention","I1Result","I1ResultOn","I2Intervention",
                                        "I2Result","I2ResultOn","I3Intervention","I3Result","I3ResultOn",
                                        "I4Intervention","I4Result","I4ResultOn"
                                        };

                //strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl050.BU", "tvl050.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_INSPECTIONDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ColorFlag),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.RequestId),
                                Convert.ToString(uc.RequestNo),
                                Convert.ToString(uc.ReqSequenceNo),
                                Convert.ToString(uc.ReqIterationNo),
                                uc.OperationNo.Value.ToString("G29"),
                                Convert.ToString("R"+uc.OperationRevisionNo),
                                Convert.ToString(uc.Activity),
                                 (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                             (!string.IsNullOrEmpty(uc.RefDocRevNo)) ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                                Convert.ToString(uc.TestResult),
                                Convert.ToString(uc.OfferedBy),
                                Convert.ToString(uc.OfferedOn),
                                Convert.ToString(uc.InspectedBy),
                                Convert.ToString(uc.InspectedOn),
                                Convert.ToString(uc.OfferedtoCusomerBy),
                                Convert.ToString(uc.OfferedtoCusomerOn),
                                  Convert.ToString(uc.LTIntervention) != null ?GetInterventionDetails(Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),uc.HeaderId,uc.RequestId):"",
                            Convert.ToString(uc.I1Intervention) != null ?GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.RequestId):"",
                            Convert.ToString(uc.I2Intervention) != null ?GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.RequestId):"",
                                //Convert.ToString(uc.LTIntervention),
                                //Convert.ToString(Intervention (uc.I1Intervention,uc.I1ResultBy,uc.I1Result,uc.I1ResultOn)),
                                //Convert.ToString(Intervention (uc.I2Intervention,uc.I2ResultBy,uc.I2Result,uc.I2ResultOn)),
                               // Convert.ToString(Intervention (uc.I3Intervention,uc.I3ResultBy,uc.I3Result,uc.I3ResultOn)),
                               // Convert.ToString(Intervention (uc.I4Intervention,uc.I4ResultBy,uc.I4Result,uc.I4ResultOn)),
                                Convert.ToString(uc.QCRemark),
                          "<nobr><center>"
                            +  Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendTPIInspection/Details/"+uc.RequestId,false)
                            + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                            +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId)
                            +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                             +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //public JsonResult LoadTPIAllInspectionData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        string strWhereCondition = string.Empty;

        //        #region Datatable Sorting 
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
        //        string sortDirection = Convert.ToString(Request["sSortDir_0"]);
        //        string strSortOrder = string.Empty;
        //        if (!string.IsNullOrWhiteSpace(sortColumnName))
        //        {
        //            strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
        //        }
        //        #endregion
        //        string tpiReturned = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
        //        string tpiCleared = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
        //        strWhereCondition = "1=1";
        //        if (param.Status.ToUpper() == "PENDING")
        //        {
        //            strWhereCondition += " and TestResult='" + clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue() + "'";

        //            strWhereCondition += " and (ISNULL(I1Intervention, '') != '' or ISNULL(I2Intervention,'')!= '' or ISNULL(I3Intervention,'')!= '' or ISNULL(I4Intervention,'')!= '' )"
        //                                + " and ((ISNULL(I1Intervention, '') != '' and ISNULL(I1Result, '') != 'Cleared') or"
        //                                + "(ISNULL(I2Intervention, '') != '' and ISNULL(I2Result, '') != 'Cleared') or"
        //                                + "(ISNULL(I3Intervention, '') != '' and ISNULL(I3Result, '') != 'Cleared') or"
        //                                + "(ISNULL(I4Intervention, '') != '' and ISNULL(I4Result, '') != 'Cleared'))"
        //                                + " and (ISNULL(I1Result, '') != 'Returned' and ISNULL(I2Result, '') != 'Returned' and ISNULL(I3Result, '') != 'Returned' and ISNULL(I4Result, '') != 'Returned') ";
        //        }
        //        strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
        //                          + " and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
        //                          + " and BU='" + param.BU + "' and Location='" + param.Location + "'"
        //                           + " and SubAssemblyNo=" + Convert.ToInt32(param.subassembly);

        //        //search Condition 
        //        string[] columnName = { "RequestNo","ReqSequenceNo","ReqIterationNo","OperationNo","OperationRevisionNo",
        //                                "Activity","RefDocument","RefDocRevNo","TestResult","OfferedBy","OfferedOn",
        //                                "InspectedBy","InspectedOn","QCRemark","OfferedtoCusomerBy",  "OfferedtoCusomerOn",
        //                                "I1Intervention","I1Result","I1ResultOn","I2Intervention",
        //                                "I2Result","I2ResultOn","I3Intervention","I3Result","I3ResultOn",
        //                                "I4Intervention","I4Result","I4ResultOn"
        //                                };

        //        strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
        //        strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl050.BU", "tvl050.Location");
        //        var lstResult = db.SP_TVL_GET_ATTEND_INSPECTIONDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                   Convert.ToString(uc.ColorFlag),
        //                        Convert.ToString(uc.HeaderId),
        //                        Convert.ToString(uc.RequestId),
        //                        Convert.ToString(uc.RequestNo),
        //                        Convert.ToString(uc.ReqSequenceNo),
        //                        Convert.ToString(uc.ReqIterationNo),
        //                        uc.OperationNo.Value.ToString("G29"),
        //                        Convert.ToString("R"+uc.OperationRevisionNo),
        //                        Convert.ToString(uc.Activity),
        //                         (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "",
        //                        (!string.IsNullOrEmpty(uc.RefDocRevNo)) ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "",
        //                        Convert.ToString(uc.TestResult),
        //                        Convert.ToString(uc.OfferedBy),
        //                        Convert.ToString(uc.OfferedOn),
        //                        Convert.ToString(uc.InspectedBy),
        //                        Convert.ToString(uc.InspectedOn),
        //                        Convert.ToString(uc.OfferedtoCusomerBy),
        //                        Convert.ToString(uc.OfferedtoCusomerOn),
        //                          Convert.ToString(uc.LTIntervention) != null ?GetInterventionDetails(Convert.ToString(uc.LTIntervention),uc.HeaderId,uc.ReqSequenceNo):"",
        //                    Convert.ToString(uc.I1Intervention) != null ?GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.ReqSequenceNo):"",
        //                    Convert.ToString(uc.I2Intervention) != null ?GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.ReqSequenceNo):"",
        //                        //Convert.ToString(uc.LTIntervention),
        //                        //Convert.ToString(Intervention (uc.I1Intervention,uc.I1ResultBy,uc.I1Result,uc.I1ResultOn)),
        //                        //Convert.ToString(Intervention (uc.I2Intervention,uc.I2ResultBy,uc.I2Result,uc.I2ResultOn)),
        //                       // Convert.ToString(Intervention (uc.I3Intervention,uc.I3ResultBy,uc.I3Result,uc.I3ResultOn)),
        //                       // Convert.ToString(Intervention (uc.I4Intervention,uc.I4ResultBy,uc.I4Result,uc.I4ResultOn)),
        //                        Convert.ToString(uc.QCRemark),
        //                  "<nobr><center>"
        //                    + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendTPIInspection/Details/"+uc.RequestId,false)
        //                    + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
        //                      +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId)

        //                    + "<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
        //                    + "</center></nobr>"
        //                    }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data,
        //            whereCondition = strWhereCondition,
        //            strSortOrder = strSortOrder
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public JsonResult LoadAllInspectionData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                //if (param.Status.ToUpper() == clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue().ToUpper())
                //{
                //    strWhereCondition += "1=1 and tvl040.InspectionStatus  in ( '" + clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() + "')";
                //}
                //else
                //{
                strWhereCondition += "1=1";
                //}
                strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                    + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                    + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                    + "and SubAssemblyNo=" + Convert.ToInt32(param.subassembly) + " ";

                //search Condition 
                string[] columnName = { "OperationNo", "OperationRevisionNo", "Activity", "RefDocument", "RefDocRevNo", "itenrationNo", "SubAssemblyNo", "InspectionStatus" };
                //  strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ColorFlag),
                           uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  (!string.IsNullOrEmpty(uc.RefDocRevNo))? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Convert.ToString(uc.itenrationNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Convert.ToString(uc.SubAssemblyNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Convert.ToString(uc.InspectionStatus),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Convert.ToString(uc.LTIntervention) != null ?GetInterventionDetails(Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),uc.HeaderId,uc.RequestId):"",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Convert.ToString(uc.I1Intervention) != null ?GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.RequestId):"",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Convert.ToString(uc.I2Intervention) != null ?GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.RequestId):"",
                           // Convert.ToString(uc.LTIntervention),
                            //Convert.ToString(uc.I1Intervention),
                            //Convert.ToString(uc.I2Intervention),
                           // Convert.ToString(uc.I3Intervention),
                            //Convert.ToString(uc.I4Intervention),                       
                            GetOperationRemarks(Convert.ToString(uc.LTIntervention),uc.HeaderId,uc.RequestId),
                             "<nobr><center>"
                            + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendTPIInspection/Details/"+uc.RequestId,uc.RequestId == null ? true : false)
                            + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                            +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId,uc.IsHeaderLine)
                            + "<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                            + "</center></nobr>"}).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public string Intervention(string intervantion = "", string resultby = "", string result = "", DateTime? resulton = null)
        {
            string strresult = string.Empty;

            if (!string.IsNullOrWhiteSpace(intervantion))
            {
                strresult += intervantion;
            }
            if (!string.IsNullOrWhiteSpace(resultby))
            {
                strresult += "<br />Result By :" + Manager.GetUserNameFromPsNo(resultby);
            }
            if (!string.IsNullOrWhiteSpace(result))
            {
                strresult += ("<br />Result :" + result);
            }
            if (resulton.HasValue)
            {
                strresult += "<br />Result On :" + resulton;
            }
            return strresult;
        }

        #endregion

        #region Detail Page
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Details(int? Id)
        {
            int count = 0;
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            NDEModels objNDEModels = new NDEModels();
            TVL050 objTVL050 = new TVL050();
            string strCleared = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
            string strWaved = clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue();
            string strReturned = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
            ViewBag.Title = "Attend TPI Request";
            objTVL050 = db.TVL050.Where(x => x.RequestId == Id).FirstOrDefault();
            if (objTVL050 != null)
            {

                ViewBag.OperationId = db.TVL050.Where(x => x.TravelerNo == objTVL050.TravelerNo
                             && x.TravelerRevNo == objTVL050.TravelerRevNo
                             && x.ProjectNo == objTVL050.ProjectNo
                             && x.BU == objTVL050.BU
                             && x.Location == objTVL050.Location
                             && x.AssemblyNo == objTVL050.AssemblyNo
                             && x.SubAssemblyNo == objTVL050.SubAssemblyNo)
                             .OrderByDescending(x => x.RequestId)
                             .Select(x => x.RequestId).FirstOrDefault();

                string status = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
                ViewBag.ReworkTravelerRevNo = db.TVL010_Log.Where(x => x.ProjectNo.Trim() == objTVL050.ProjectNo.Trim()
                                           && x.IdentificationNo.Trim() == objTVL050.AssemblyNo.Trim()
                                           && x.BU.Trim() == objTVL050.BU.Trim()
                                           && x.Location.Trim() == objTVL050.Location.Trim()
                                           && x.TravelerNo.Trim() != objTVL050.ReworkTravelerNo.Trim()
                                           && x.Status == status).Select(x => x.TravelerRevNo).FirstOrDefault();

                objTVL050.LTIntervention = objTVL050.LTIntervention == "E" ? Manager.TVLIntervention(objTVL050.LTIntervention) : objNDEModels.GetCategory("Traveler Intervention", objTVL050.LTIntervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I1Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I1Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I2Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I2Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I3Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I3Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I4Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I4Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.BU = db.COM002.Where(x => x.t_dimx == objTVL050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.Location = db.COM002.Where(x => x.t_dimx == objTVL050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.OfferedBy = objTVL050.OfferedBy != null ? Manager.GetPsidandDescription(objTVL050.OfferedBy) : string.Empty;
                objTVL050.OfferedtoCusomerBy = objTVL050.OfferedtoCusomerBy != null ? Manager.GetPsidandDescription(objTVL050.OfferedtoCusomerBy) : string.Empty;
                objTVL050.InspectedBy = objTVL050.InspectedBy != null ? Manager.GetPsidandDescription(objTVL050.InspectedBy) : string.Empty;
                bool IsCR1 = objClsLoginInfo.GetUserRoleList().Any(x => x.ToString() == clsImplementationEnum.UserRoleName.CR1.GetStringValue());
                bool IsCR2 = objClsLoginInfo.GetUserRoleList().Any(x => x.ToString() == clsImplementationEnum.UserRoleName.CR2.GetStringValue());

                if (IsCR1 && IsCR2)
                {
                    if ((!string.IsNullOrEmpty(objTVL050.I1Intervention) || !string.IsNullOrEmpty(objTVL050.I2Intervention) || !string.IsNullOrEmpty(objTVL050.I3Intervention) || !string.IsNullOrEmpty(objTVL050.I4Intervention)) &&
                                    ((!string.IsNullOrEmpty(objTVL050.I1Intervention) && (objTVL050.I1Result != strCleared && objTVL050.I1Result != strWaved)) ||
                                     (!string.IsNullOrEmpty(objTVL050.I2Intervention) && (objTVL050.I2Result != strCleared && objTVL050.I2Result != strWaved)) ||
                                     (!string.IsNullOrEmpty(objTVL050.I3Intervention) && (objTVL050.I3Result != strCleared && objTVL050.I3Result != strWaved)) ||
                                     (!string.IsNullOrEmpty(objTVL050.I4Intervention) && (objTVL050.I4Result != strCleared && objTVL050.I4Result != strWaved))) &&
                                     ((objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue())) &&
                                     (objTVL050.I1Result != strReturned && objTVL050.I2Result != strReturned && objTVL050.I3Result != strReturned && objTVL050.I4Result != strReturned)
                                     )
                    {
                        ViewBag.visible = "true";
                    }
                    else
                    {
                        ViewBag.visible = "false";
                    }
                }
                else
                {

                    if ((!string.IsNullOrEmpty(objTVL050.I1Intervention) && string.IsNullOrEmpty(objTVL050.I1Result)))
                    {
                        count = 1;
                    }
                    else if ((!string.IsNullOrEmpty(objTVL050.I2Intervention) && string.IsNullOrEmpty(objTVL050.I2Result)))
                    {
                        count = 2;
                    }
                    else if ((!string.IsNullOrEmpty(objTVL050.I3Intervention) && string.IsNullOrEmpty(objTVL050.I3Result)))
                    {
                        count = 3;
                    }
                    else if ((!string.IsNullOrEmpty(objTVL050.I4Intervention) && string.IsNullOrEmpty(objTVL050.I4Result)))
                    {
                        count = 4;
                    }
                    if (count == 1)
                    {
                        if ((!string.IsNullOrEmpty(objTVL050.I1Intervention)) && ((!string.IsNullOrEmpty(objTVL050.I1Intervention) && (objTVL050.I1Result != strCleared && objTVL050.I1Result != strWaved)) && (objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue()) && (objTVL050.I1Result != strReturned)) && IsCR1)
                        {
                            ViewBag.visible = "true";
                        }
                        else
                        {
                            ViewBag.visible = "false";
                        }
                    }
                    else if (count == 2)
                    {
                        if (string.IsNullOrEmpty(objTVL050.I3Intervention) && string.IsNullOrEmpty(objTVL050.I4Intervention))
                        {
                            if ((!string.IsNullOrEmpty(objTVL050.I2Intervention)) && ((!string.IsNullOrEmpty(objTVL050.I2Intervention) && (objTVL050.I2Result != strCleared && objTVL050.I2Result != strWaved)) && (objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue()) && (objTVL050.I1Result != strReturned && objTVL050.I2Result != strReturned)) && IsCR2)
                            {
                                ViewBag.visible = "true";
                            }
                            else
                            {
                                ViewBag.visible = "false";
                            }
                        }
                        else
                        {
                            if ((!string.IsNullOrEmpty(objTVL050.I2Intervention)) && ((!string.IsNullOrEmpty(objTVL050.I2Intervention) && (objTVL050.I2Result != strCleared && objTVL050.I2Result != strWaved)) && (objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue()) && (objTVL050.I1Result != strReturned && objTVL050.I2Result != strReturned)) && IsCR1)
                            {
                                ViewBag.visible = "true";
                            }
                            else
                            {
                                ViewBag.visible = "false";
                            }
                        }
                    }

                    else if (count == 3)
                    {
                        if (string.IsNullOrEmpty(objTVL050.I4Intervention))
                        {
                            if ((!string.IsNullOrEmpty(objTVL050.I3Intervention)) && ((!string.IsNullOrEmpty(objTVL050.I3Intervention) && (objTVL050.I3Result != strCleared && objTVL050.I3Result != strWaved)) && (objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue()) && (objTVL050.I1Result != strReturned && objTVL050.I2Result != strReturned && objTVL050.I3Result != strReturned)) && IsCR2)
                            {
                                ViewBag.visible = "true";
                            }
                            else
                            {
                                ViewBag.visible = "false";
                            }
                        }
                        else
                        {
                            if ((!string.IsNullOrEmpty(objTVL050.I3Intervention)) && ((!string.IsNullOrEmpty(objTVL050.I3Intervention) && (objTVL050.I3Result != strCleared && objTVL050.I3Result != strWaved)) && (objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue()) && (objTVL050.I1Result != strReturned && objTVL050.I2Result != strReturned && objTVL050.I3Result != strReturned)) && IsCR1)
                            {
                                ViewBag.visible = "true";
                            }
                            else
                            {
                                ViewBag.visible = "false";
                            }
                        }

                    }
                    else if (count == 4)
                    {
                        if ((!string.IsNullOrEmpty(objTVL050.I4Intervention)) && ((!string.IsNullOrEmpty(objTVL050.I4Intervention) && (objTVL050.I4Result != strCleared && objTVL050.I4Result != strWaved)) && (objTVL050.TestResult == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue()) && (objTVL050.I1Result != strReturned && objTVL050.I2Result != strReturned && objTVL050.I3Result != strReturned && objTVL050.I4Result != strReturned)) && IsCR2)
                        {
                            ViewBag.visible = "true";
                        }
                        else
                        {
                            ViewBag.visible = "false";
                        }

                    }
                }
                ViewBag.ProjectNo = objTVL050.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL050.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault();
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL050.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1Intervention";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2Intervention";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3Intervention";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4Intervention";
                    }

                }
            }
            return View(objTVL050);
        }

        [HttpPost]
        public ActionResult RespondToTPIInspectionAction(int requestId, string type, string tpiRemark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL050 objTVL050 = db.TVL050.FirstOrDefault(x => x.RequestId == requestId);
                string strCleared = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                string strReturned = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();

                if (type == clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue() || type == clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue())
                {
                    #region Clear Request
                    //update test result
                    objResponseMsg1 = updateTPIIntervention(objTVL050, true, type); // update intervention
                    #endregion
                }
                else
                {
                    #region Return Request
                    objTVL050.TPIRemarks = tpiRemark;
                    updateTPIIntervention(objTVL050, false, type);

                    if (type == strReturned)
                    {
                        //in reject/return state system will auto insert new record
                        objResponseMsg1 = InsertRequestOnReturn(objTVL050, clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue(), true);
                        objTVL050.EditedBy = objClsLoginInfo.UserName;
                        objTVL050.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objTVL050.BU, objTVL050.Location,
                     "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been Returned by TPI"
                     , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objResponseMsg1.RequestId);
                    #endregion


                    #endregion
                }
                if (objResponseMsg1.Key)
                {
                    objResponseMsg1.Value = "Data " + type + "  Successfully ";
                }
                else
                {
                    objResponseMsg1.Value = "Error Occured, Please try again";
                }
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg1.Key = false;
                objResponseMsg1.Value = "Error Occured, Please try again";
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
        }

        #region Update Intervention's with result Clear/reject and maintain datetime
        public clsHelper.ResponseMsgWithStatus updateTPIIntervention(TVL050 objTVL050, bool IsClear, string type)
        {
            string strClear = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (IsClear)
                {
                    #region Clear
                    //if (!string.IsNullOrWhiteSpace(objTVL050.I1Intervention))
                    //{
                    //    if (string.IsNullOrWhiteSpace(objTVL050.I1Result))
                    //    {
                    //        objTVL050.I1Result = type;
                    //        objTVL050.I1ResultBy = objClsLoginInfo.UserName;
                    //        objTVL050.I1ResultOn = DateTime.Now;
                    //        db.SaveChanges();
                    //        if (string.IsNullOrWhiteSpace(objTVL050.I2Intervention))
                    //        {
                    //            ClearIntervention(objTVL050, strClear);
                    //        }
                    //        else
                    //        {
                    //            #region Send Notification
                    //            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                    //             "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                    //             , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                    //            #endregion
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(objTVL050.I2Intervention))
                    //        {
                    //            if (string.IsNullOrWhiteSpace(objTVL050.I2Result))
                    //            {
                    //                objTVL050.I2Result = type;
                    //                objTVL050.I2ResultBy = objClsLoginInfo.UserName;
                    //                objTVL050.I2ResultOn = DateTime.Now;
                    //                db.SaveChanges();
                    //                if (string.IsNullOrWhiteSpace(objTVL050.I3Intervention))
                    //                {
                    //                    ClearIntervention(objTVL050, strClear);
                    //                }
                    //                else
                    //                {
                    //                    #region Send Notification
                    //                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                    //                     "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                    //                     , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                    //                    #endregion
                    //                }
                    //            }
                    //            else
                    //            {
                    //                if (!string.IsNullOrWhiteSpace(objTVL050.I3Intervention))
                    //                {
                    //                    if (string.IsNullOrWhiteSpace(objTVL050.I3Result))
                    //                    {
                    //                        objTVL050.I3Result = type;
                    //                        objTVL050.I3ResultBy = objClsLoginInfo.UserName;
                    //                        objTVL050.I3ResultOn = DateTime.Now;
                    //                        db.SaveChanges();
                    //                        if (string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                    //                        {
                    //                            ClearIntervention(objTVL050, strClear);
                    //                        }
                    //                        else
                    //                        {
                    //                            #region Send Notification
                    //                            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                    //                             "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                    //                             , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                    //                            #endregion
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        if (!string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                    //                        {
                    //                            if (string.IsNullOrWhiteSpace(objTVL050.I4Result))
                    //                            {
                    //                                objTVL050.I4Result = type;
                    //                                objTVL050.I4ResultBy = objClsLoginInfo.UserName;
                    //                                objTVL050.I4ResultOn = DateTime.Now;
                    //                                db.SaveChanges();
                    //                                ClearIntervention(objTVL050, strClear);
                    //                            }
                    //                            else
                    //                            {
                    //                                #region Send Notification
                    //                                (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                    //                                 "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                    //                                 , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                    //                                #endregion
                    //                            }
                    //                        }
                    //                        else
                    //                        {
                    //                            ClearIntervention(objTVL050, strClear);
                    //                        }
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    ClearIntervention(objTVL050, strClear);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    ClearIntervention(objTVL050, strClear);
                    //}
                    #endregion


                    #region 19106 Clear

                    if (!string.IsNullOrWhiteSpace(objTVL050.I1Intervention) && string.IsNullOrWhiteSpace(objTVL050.I1Result))
                    {
                        objTVL050.I1Result = type;
                        objTVL050.I1ResultBy = objClsLoginInfo.UserName;
                        objTVL050.I1ResultOn = DateTime.Now;
                        db.SaveChanges();
                        if (string.IsNullOrWhiteSpace(objTVL050.I2Intervention) && string.IsNullOrWhiteSpace(objTVL050.I3Intervention) && string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                        {
                            ClearIntervention(objTVL050, strClear);
                        }
                        else
                        {
                            #region Send Notification
                            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                             "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                             , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                            #endregion
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(objTVL050.I2Intervention) && string.IsNullOrWhiteSpace(objTVL050.I2Result))
                    {
                        objTVL050.I2Result = type;
                        objTVL050.I2ResultBy = objClsLoginInfo.UserName;
                        objTVL050.I2ResultOn = DateTime.Now;
                        db.SaveChanges();
                        if (string.IsNullOrWhiteSpace(objTVL050.I3Intervention) && string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                        {
                            ClearIntervention(objTVL050, strClear);
                        }
                        else
                        {
                            #region Send Notification
                            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                             "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                             , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                            #endregion
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(objTVL050.I3Intervention) && string.IsNullOrWhiteSpace(objTVL050.I3Result))
                    {
                        objTVL050.I3Result = type;
                        objTVL050.I3ResultBy = objClsLoginInfo.UserName;
                        objTVL050.I3ResultOn = DateTime.Now;
                        db.SaveChanges();
                        if (string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                        {
                            ClearIntervention(objTVL050, strClear);
                        }
                        else
                        {
                            #region Send Notification
                            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                             "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                             , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                            #endregion
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(objTVL050.I4Intervention) && string.IsNullOrWhiteSpace(objTVL050.I4Result))
                    {
                        if (string.IsNullOrWhiteSpace(objTVL050.I4Result))
                        {
                            objTVL050.I4Result = type;
                            objTVL050.I4ResultBy = objClsLoginInfo.UserName;
                            objTVL050.I4ResultOn = DateTime.Now;
                            db.SaveChanges();
                            ClearIntervention(objTVL050, strClear);
                        }
                        else
                        {
                            #region Send Notification
                            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                             "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr. No " + objTVL050.SubAssemblyNo + "  & Project No: " + objTVL050.ProjectNo + " has been cleared by L&T Inspector and offered for TPI Inspection"
                             , clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                            #endregion
                        }
                    }
                    else
                    {
                        ClearIntervention(objTVL050, strClear);
                    }
                    #endregion
                }
                else
                {
                    #region Reject
                    //if (!string.IsNullOrWhiteSpace(objTVL050.I1Intervention))
                    //{
                    //    if (string.IsNullOrWhiteSpace(objTVL050.I1Result))
                    //    {
                    //        objTVL050.I1Result = type;
                    //        objTVL050.I1ResultBy = objClsLoginInfo.UserName;
                    //        objTVL050.I1ResultOn = DateTime.Now;
                    //        db.SaveChanges();
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(objTVL050.I2Intervention))
                    //        {
                    //            if (string.IsNullOrWhiteSpace(objTVL050.I2Result))
                    //            {
                    //                objTVL050.I2Result = type;
                    //                objTVL050.I2ResultBy = objClsLoginInfo.UserName;
                    //                objTVL050.I2ResultOn = DateTime.Now;
                    //                db.SaveChanges();
                    //            }
                    //            else
                    //            {
                    //                if (!string.IsNullOrWhiteSpace(objTVL050.I3Intervention))
                    //                {
                    //                    if (string.IsNullOrWhiteSpace(objTVL050.I3Result))
                    //                    {
                    //                        objTVL050.I3Result = type;
                    //                        objTVL050.I3ResultBy = objClsLoginInfo.UserName;
                    //                        objTVL050.I3ResultOn = DateTime.Now;
                    //                        db.SaveChanges();
                    //                    }
                    //                    else
                    //                    {
                    //                        if (!string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                    //                        {
                    //                            if (string.IsNullOrWhiteSpace(objTVL050.I4Result))
                    //                            {
                    //                                objTVL050.I4Result = type;
                    //                                objTVL050.I4ResultOn = DateTime.Now;
                    //                                objTVL050.I4ResultBy = objClsLoginInfo.UserName;
                    //                                db.SaveChanges();
                    //                            }
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                    #region 19106 Reject
                    if (!string.IsNullOrWhiteSpace(objTVL050.I1Intervention) && string.IsNullOrWhiteSpace(objTVL050.I1Result))
                    {
                        objTVL050.I1Result = type;
                        objTVL050.I1ResultBy = objClsLoginInfo.UserName;
                        objTVL050.I1ResultOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else if (!string.IsNullOrWhiteSpace(objTVL050.I2Intervention) && string.IsNullOrWhiteSpace(objTVL050.I2Result))
                    {

                        objTVL050.I2Result = type;
                        objTVL050.I2ResultBy = objClsLoginInfo.UserName;
                        objTVL050.I2ResultOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else if (!string.IsNullOrWhiteSpace(objTVL050.I3Intervention) && string.IsNullOrWhiteSpace(objTVL050.I3Result))
                    {
                        objTVL050.I3Result = type;
                        objTVL050.I3ResultBy = objClsLoginInfo.UserName;
                        objTVL050.I3ResultOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else if (!string.IsNullOrWhiteSpace(objTVL050.I4Intervention) && string.IsNullOrWhiteSpace(objTVL050.I4Result))
                    {
                        objTVL050.I4Result = type;
                        objTVL050.I4ResultOn = DateTime.Now;
                        objTVL050.I4ResultBy = objClsLoginInfo.UserName;
                        db.SaveChanges();
                    }
                    #endregion
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return objResponseMsg;
        }
        #endregion

        #region Auto Clear if next Intervension not found
        public void ClearIntervention(TVL050 objTVL050, string strStatus)
        {
            TVL040 objTVL040 = db.TVL040.FirstOrDefault(x => x.HeaderId == objTVL050.HeaderId);
            try
            {
                if (objTVL040 != null)
                {
                    objTVL040.InspectionStatus = strStatus;
                    objTVL040.LnTInspectorResult = strStatus;
                    objTVL040.LnTInspectorResultOn = DateTime.Now;

                    objTVL040.EditedBy = objClsLoginInfo.UserName;
                    objTVL040.EditedOn = DateTime.Now;
                }
                objTVL050.OfferedtoCusomerBy = objClsLoginInfo.UserName;
                objTVL050.OfferedtoCusomerOn = DateTime.Now;
                objTVL050.TestResult = strStatus;
                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PROD3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG2.GetStringValue()), objTVL050.BU, objTVL050.Location,
                 "Traveler: Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " &Identification Sr.No " + objTVL050.SubAssemblyNo + " &Project No: " + objTVL050.ProjectNo + " has been Cleared", clsImplementationEnum.NotificationType.Information.GetStringValue());
                #endregion
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        #endregion

        #region Auto Insert new record in Reject/Return Case
        public clsHelper.ResponseMsgWithStatus InsertRequestOnReturn(TVL050 objsrcTVL050, string Status, bool isTPI = false)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int IterationNo = 0;
                int ReqSeqNo = 0;
                bool copyAttachment = false;
                string newfolderPath = "";
                string oldfolderPath = "";

                string strReturneByTPI = clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue();
                if (Status == clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue())
                {
                    IterationNo = Convert.ToInt32(objsrcTVL050.ReqIterationNo);
                    ReqSeqNo = Convert.ToInt32(objsrcTVL050.ReqSequenceNo + 1);
                    copyAttachment = true;
                }
                TVL050 objTVL050 = db.TVL050.Add(new TVL050
                {
                    RequestId = objsrcTVL050.RequestId,
                    HeaderId = objsrcTVL050.HeaderId,
                    RequestNo = objsrcTVL050.RequestNo,
                    ReqIterationNo = IterationNo,
                    ReqSequenceNo = ReqSeqNo,
                    ProjectNo = objsrcTVL050.ProjectNo,
                    Customer = objsrcTVL050.Customer,
                    TravelerNo = objsrcTVL050.TravelerNo,
                    TravelerRevNo = objsrcTVL050.TravelerRevNo,
                    BU = objsrcTVL050.BU,
                    Location = objsrcTVL050.Location,
                    Identification = objsrcTVL050.Identification,
                    AssemblyNo = objsrcTVL050.AssemblyNo,
                    OperationNo = objsrcTVL050.OperationNo,
                    OperationRevisionNo = objsrcTVL050.OperationRevisionNo,
                    Activity = objsrcTVL050.Activity,
                    RefDocument = objsrcTVL050.RefDocument,
                    RefDocRevNo = objsrcTVL050.RefDocRevNo,
                    LTIntervention = objsrcTVL050.LTIntervention,
                    I1Intervention = objsrcTVL050.I1Intervention != null ? objsrcTVL050.I1Intervention : null,
                    I2Intervention = objsrcTVL050.I2Intervention != null ? objsrcTVL050.I2Intervention : null,
                    I3Intervention = objsrcTVL050.I3Intervention != null ? objsrcTVL050.I3Intervention : null,
                    I4Intervention = objsrcTVL050.I4Intervention != null ? objsrcTVL050.I4Intervention : null,
                    InspectionRecordRequired = objsrcTVL050.InspectionRecordRequired,
                    OfferLocation = objsrcTVL050.OfferLocation,
                    OfferedBy = isTPI ? objsrcTVL050.OfferedBy : null,
                    OfferedOn = isTPI ? objsrcTVL050.OfferedOn : null,
                    TestResult = isTPI ? strReturneByTPI : null,
                    InspectedBy = null,
                    InspectedOn = null,
                    QCRemark = null,
                    TPIRemarks = null,
                    OfferedtoCusomerBy = null,
                    OfferedtoCusomerOn = null,
                    SubAssemblyNo = objsrcTVL050.SubAssemblyNo,
                    IsReworkTraveler = objsrcTVL050.IsReworkTraveler,
                    ReworkTravelerNo = objsrcTVL050.ReworkTravelerNo,
                    IsNCRNo = objsrcTVL050.IsNCRNo,
                    NCRNo = objsrcTVL050.NCRNo,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                    ShopUsedRefDocRevNo = objsrcTVL050.ShopUsedRefDocRevNo,
                    QCUsedRefDocRevNo = objsrcTVL050.QCUsedRefDocRevNo,
                    ShopRemark = objsrcTVL050.ShopRemark
                });
                db.SaveChanges();

                #region copy documents
                if (copyAttachment)
                {
                    oldfolderPath = "TVL050/" + objsrcTVL050.HeaderId + "/" + objsrcTVL050.ReqIterationNo + "/" + objsrcTVL050.ReqSequenceNo;
                    newfolderPath = "TVL050/" + objsrcTVL050.HeaderId + "/" + IterationNo + "/" + ReqSeqNo;
                    (new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.RequestId = objTVL050.RequestId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return objResponseMsg;
        }
        #endregion

        #endregion

        #region Common Functions
        public string GetInterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            if (InspectionStatus == "S")
            {
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == Headerid).FirstOrDefault();
                if (objtvl040 != null)
                {
                    if (!string.IsNullOrEmpty(objtvl040.LnTInspectorResult))
                    {
                        InspectionStatus = objtvl040.LTIntervention + "<br>" + objtvl040.LnTInspectorResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl040.EditedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl040.LnTInspectorResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl040.LnTInspectorResultOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = objtvl040.LTIntervention;
                    }
                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            else
            {
                string strTPI = clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue();
                TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
                TVL050 objpretvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId != RequestId).OrderByDescending(c => c.RequestId).FirstOrDefault();

                if (objtvl050 != null)
                {
                    if (!string.IsNullOrEmpty(objtvl050.TestResult))
                    {
                        if (objtvl050.TestResult == strTPI)
                        {
                            if (objpretvl050 != null ? (!string.IsNullOrEmpty(objpretvl050.TestResult) && !string.IsNullOrEmpty(objpretvl050.InspectedBy)) : false)
                            {
                                InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention) + "<br>" + objpretvl050.TestResult + "<br>" + Manager.GetUserNameFromPsNo(objpretvl050.InspectedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.InspectedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.InspectedOn).ToString("dd/MM/yyyy"));
                            }
                            else
                            {
                                InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention) + "<br>" + objtvl050.TestResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.InspectedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy"));
                            }
                        }
                        else
                        {
                            InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention) + "<br>" + objtvl050.TestResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.InspectedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy"));
                        }
                    }
                    else
                    {
                        if (objpretvl050 != null ? (!string.IsNullOrEmpty(objpretvl050.TestResult) && !string.IsNullOrEmpty(objpretvl050.InspectedBy)) : false)
                        {
                            InspectionStatus = Manager.TVLIntervention(objpretvl050.LTIntervention) + "<br>" + objpretvl050.TestResult + "<br>" + Manager.GetUserNameFromPsNo(objpretvl050.InspectedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.InspectedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.InspectedOn).ToString("dd/MM/yyyy"));
                        }
                        else
                        {
                            InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention);
                        }
                    }
                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            return InspectionStatus;
        }

        public string GetI1InterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            string clear = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
            TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
            if (objtvl050 != null)
            {
                if (objtvl050.TestResult != clear && string.IsNullOrWhiteSpace(objtvl050.InspectedBy))
                {
                    TVL050 objpretvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId != RequestId).OrderByDescending(c => c.RequestId).FirstOrDefault();
                    if (objpretvl050 != null ? !string.IsNullOrEmpty(objpretvl050.I1Result) : false)
                    {
                        if (objtvl050.I1Result == clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue())
                        {
                            InspectionStatus = objpretvl050.I1Intervention + "<br>" + objpretvl050.I1Result + "<br>" + Manager.GetUserNameFromPsNo(objpretvl050.I1ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.I1ResultOn).ToString("dd/MM/yyyy"));

                            //InspectionStatus = objpretvl050.I1Intervention + "<br>" + objpretvl050.I1Result + "<br>" + Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.WavedUserName.GetStringValue()) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.I1ResultOn).ToString("dd/MM/yyyy"));
                        }
                        else
                        {
                            InspectionStatus = objpretvl050.I1Intervention + "<br>" + objpretvl050.I1Result + "<br>" + Manager.GetUserNameFromPsNo(objpretvl050.I1ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.I1ResultOn).ToString("dd/MM/yyyy"));
                        }
                    }
                    else
                    {
                        InspectionStatus = objtvl050.I1Intervention;
                    }
                }
                else if (!string.IsNullOrEmpty(objtvl050.I1Result))
                {
                    if (objtvl050.I1Result == clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue())
                    {
                        InspectionStatus = objtvl050.I1Intervention + "<br>" + objtvl050.I1Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I1ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy"));

                        //InspectionStatus = objtvl050.I1Intervention + "<br>" + objtvl050.I1Result + "<br>" + Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.WavedUserName.GetStringValue()) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = objtvl050.I1Intervention + "<br>" + objtvl050.I1Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I1ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy"));
                    }
                }
                else
                {
                    InspectionStatus = objtvl050.I1Intervention;
                }
                //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");

            }

            return InspectionStatus;
        }
        public string GetI2InterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            string clear = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
            TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
            if (objtvl050 != null)
            {
                if (objtvl050.TestResult != clear && string.IsNullOrWhiteSpace(objtvl050.InspectedBy))
                {
                    TVL050 objpretvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId != RequestId).OrderByDescending(c => c.RequestId).FirstOrDefault();
                    if (objpretvl050 != null ? !string.IsNullOrEmpty(objpretvl050.I2Result) : false)
                    {
                        if (objtvl050.I2Result == clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue())
                        {
                            InspectionStatus = objpretvl050.I2Intervention + "<br>" + objpretvl050.I2Result + "<br>" + Manager.GetUserNameFromPsNo(objpretvl050.I2ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.I2ResultOn).ToString("dd/MM/yyyy"));

                            //InspectionStatus = objpretvl050.I2Intervention + "<br>" + objpretvl050.I2Result + "<br>" + Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.WavedUserName.GetStringValue()) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                        }
                        else
                        {
                            InspectionStatus = objpretvl050.I2Intervention + "<br>" + objpretvl050.I2Result + "<br>" + Manager.GetUserNameFromPsNo(objpretvl050.I2ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objpretvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objpretvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                        }
                    }
                    else
                    {
                        InspectionStatus = objtvl050.I2Intervention;
                    }
                }
                else if (!string.IsNullOrEmpty(objtvl050.I2Result))
                {
                    if (objtvl050.I2Result == clsImplementationEnum.TravelerInspectionStatus.Waved.GetStringValue())
                    {
                        InspectionStatus = objtvl050.I2Intervention + "<br>" + objtvl050.I2Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I2ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                        //InspectionStatus = objtvl050.I2Intervention + "<br>" + objtvl050.I2Result + "<br>" + Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.WavedUserName.GetStringValue()) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = objtvl050.I2Intervention + "<br>" + objtvl050.I2Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I2ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                    }
                }
                else
                {
                    InspectionStatus = objtvl050.I2Intervention;
                }
                //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");

            }

            return InspectionStatus;
        }

        public string GetOperationRemarks(string InspectionStatus, int Headerid, int? RequestId)
        {
            if (InspectionStatus == "S")
            {
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == Headerid).FirstOrDefault();
                if (objtvl040 != null)
                {
                    InspectionStatus = objtvl040.ShopRemark;

                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            else
            {
                TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
                if (objtvl050 != null)
                {
                    InspectionStatus = objtvl050.QCRemark;

                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            return InspectionStatus;
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_TVL_GET_INSPECTIONDATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.ProjectNo),
                                      TravelerNo = Convert.ToString(uc.TravelerNo),
                                      TravelerRevNo = Convert.ToString(uc.TravelerRevNo),
                                      TestResult = Convert.ToString(uc.TestResult)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.CR1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.CR1.GetStringValue();

                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.CR2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.CR2.GetStringValue();

                }


            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion
    }
}