﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace IEMQS.Areas.TVL.Controllers
{
    public class AttendInspectionController : clsBase
    {
        // GET: TVL/AttendInspection
        #region Index Grid
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index(string tab)
        {
            ViewBag.Title = "Attend Request";
            ViewBag.Tab = tab;

            return View();
        }

        [HttpPost]
        public ActionResult LoadInspectionDataIndexPartial(string status)
        {
            if (status == "NCR")
            {
                ViewBag.Status = status;
                return PartialView("_GetIndexNCRGridDataPartial");
            }
            else
            {
                ViewBag.Status = status;
                string location = objClsLoginInfo.Location;
                if (location == "HZW")
                {
                    ViewBag.location = "HAZIRA";
                }
                else if (location == "PEW")
                {
                    ViewBag.location = "POWAI";
                }
                else if (location == "RNW")
                {
                    ViewBag.location = "VADODARA";
                }
                return PartialView("_GetIndexGridDataPartial");
            }
        }
        [HttpPost]
        public JsonResult LoadInspectionIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhereCondition += " and (TestResult IS NULL or TestResult in ('" + clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.NCR_Raised.GetStringValue() + "')) and OfferedBy IS NOT NULL ";
                }



                //search Condition 
                string[] columnName = { "ProjectNo", "TravelerNo", "TravelerRevNo", "BU", "AssemblyNo", "Location" };
               // strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl50.BU", "tvl50.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_INSPECTION_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString("R"+uc.TravelerRevNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendInspection/AttendInspectionDetails/"+uc.HeaderId,false)
                        +   Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.Project+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.AssemblyNo+"','"+uc.SubAssemblyNo+"')","",false)

                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LoadNCRInspectionIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition = "1=1";
                string cleared = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
                if (param.Status.ToUpper() == "NCR")
                {
                    strWhereCondition += " and ISNULL(LnTInspectorResult,'') <> '" + cleared + "' ";
                }

                //search Condition 
                string[] columnName = { "ProjectNo", "TravelerNo", "TravelerRevNo", "BU", "AssemblyNo", "Location", "SubAssemblyNo",
                                        "OperationNo", "OperationRevisionNo" ,"Activity","InspectionStatus"};
             //   strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                // var lstResult = db.SP_TVL_INSPECTION_NCR_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ColorFlag),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.ProjectNo),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString("R"+uc.TravelerRevNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            Convert.ToString(uc.InspectionStatus),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Inspection Detail
        [SessionExpireFilter]
        public ActionResult AttendInspectionDetails(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            TVL050 objTVL050 = new TVL050();
            ViewBag.Title = "Attend Inspection";
            objTVL050 = db.TVL050.Where(x => x.RequestId == Id).FirstOrDefault();
            if (objTVL050 != null)
            {
                objTVL050.BU = db.COM002.Where(x => x.t_dimx == objTVL050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.Location = db.COM002.Where(x => x.t_dimx == objTVL050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                ViewBag.ProjectNo = objTVL050.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL050.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault();
            }
            return View(objTVL050);
        }

        [HttpPost]
        public ActionResult LoadInspectionDataPartial(string status, TVL050 objTVL050, TVL040 objTVL040)
        {
            if (status != "All")
            {
                objTVL050.BU = objTVL050.BU.Split('-')[0];
                objTVL050.Location = objTVL050.Location.Split('-')[0];
                ViewBag.Status = status;
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL050.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4";
                    }
                }
                return PartialView("_GetInspectionGridDataPartial", objTVL050);
            }
            else
            {
                objTVL040.BU = objTVL040.BU.Split('-')[0];
                objTVL040.Location = objTVL040.Location.Split('-')[0];
                ViewBag.Status = status;
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL040.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4";
                    }
                }
            }
            return PartialView("_GetOfferOperationGridDataPartial", objTVL040);
        }
        [HttpPost]
        public JsonResult LoadInspectionData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhereCondition += " and (TestResult IS NULL or TestResult in ('" + clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.NCR_Raised.GetStringValue() + "')) and OfferedBy IS NOT NULL ";
                }
                strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                 + " and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                 + " and BU='" + param.BU + "' and Location='" + param.Location + "'"
                + " and SubAssemblyNo=" + Convert.ToInt32(param.subassembly);

                //search Condition 
                string[] columnName = { "RequestNo","ReqSequenceNo","ReqIterationNo","OperationNo","OperationRevisionNo",
                                        "Activity","RefDocument","RefDocRevNo","TestResult","OfferedBy","OfferedOn",
                                        "InspectedBy","InspectedOn","QCRemark","OfferedtoCusomerBy",  "OfferedtoCusomerOn",
                                        "I1Intervention","I1Result","I1ResultOn","I2Intervention",
                                        "I2Result","I2ResultOn","I3Intervention","I3Result","I3ResultOn",
                                        "I4Intervention","I4Result","I4ResultOn"
                                       };
              //  strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl050.BU", "tvl050.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_INSPECTIONDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ColorFlag),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.RequestId),
                                Convert.ToString(uc.RequestNo),
                                Convert.ToString(uc.ReqSequenceNo),
                                Convert.ToString(uc.ReqIterationNo),
                                uc.OperationNo.Value.ToString("G29"),
                                Convert.ToString("R"+uc.OperationRevisionNo),
                                Convert.ToString(uc.Activity),
                                 (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                             (!string.IsNullOrEmpty(uc.RefDocRevNo)) ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                                Convert.ToString(uc.TestResult),
                                Convert.ToString(uc.OfferedBy),
                                Convert.ToString(uc.OfferedOn),
                                Convert.ToString(uc.InspectedBy),
                                Convert.ToString(uc.InspectedOn),
                                Convert.ToString(uc.OfferedtoCusomerBy),
                                Convert.ToString(uc.OfferedtoCusomerOn),
                                Convert.ToString(uc.LTIntervention) != null ? (new AttendTPIInspectionController()).GetInterventionDetails(Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),uc.HeaderId,uc.RequestId):"",
                                Convert.ToString(uc.I1Intervention) != null ? (new AttendTPIInspectionController()).GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.RequestId):"",
                                Convert.ToString(uc.I2Intervention) != null ? (new AttendTPIInspectionController()).GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.RequestId):"",
                                // Convert.ToString(uc.LTIntervention),
                               // Convert.ToString(Intervention (uc.I1Intervention,uc.I1ResultBy,uc.I1Result,uc.I1ResultOn)),
                               // Convert.ToString(Intervention (uc.I2Intervention,uc.I2ResultBy,uc.I2Result,uc.I2ResultOn)),
                               // Convert.ToString(Intervention (uc.I3Intervention,uc.I3ResultBy,uc.I3Result,uc.I3ResultOn)),
                               // Convert.ToString(Intervention (uc.I4Intervention,uc.I4ResultBy,uc.I4Result,uc.I4ResultOn)),
                                Helper.GenerateTextArea(uc.HeaderId, "QCRemark", Convert.ToString(uc.QCRemark),"UpdateData(this, "+ uc.RequestId+" );",((uc.TestResult == clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue() && uc.TestResult == clsImplementationEnum.TravelerInspectionStatus.NCR_Raised.GetStringValue() && string.IsNullOrWhiteSpace(uc.TestResult))?true:false),"","1000"),

                                "<nobr><center>"
                                   +  Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendInspection/Details/"+uc.RequestId,false)
                                   +  Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo, uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                                   +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId)
                                   +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                               +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string Intervention(string intervantion = "", string resultby = "", string result = "", DateTime? resulton = null)
        {
            string strresult = string.Empty;

            if (!string.IsNullOrWhiteSpace(intervantion))
            {
                strresult += intervantion;
            }
            if (!string.IsNullOrWhiteSpace(resultby))
            {
                strresult += "<br />Result By :" + Manager.GetUserNameFromPsNo(resultby);
            }
            if (!string.IsNullOrWhiteSpace(result))
            {
                strresult += ("<br />Result :" + result);
            }
            if (resulton.HasValue)
            {
                strresult += "<br />Result On :" + resulton;
            }
            return strresult;
        }
        [HttpPost]
        //public JsonResult LoadAllInspectionData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        string strWhereCondition = string.Empty;

        //        #region Datatable Sorting 
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
        //        string sortDirection = Convert.ToString(Request["sSortDir_0"]);
        //        string strSortOrder = string.Empty;
        //        if (!string.IsNullOrWhiteSpace(sortColumnName))
        //        {
        //            strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
        //        }
        //        #endregion

        //        strWhereCondition = "1=1";
        //        if (param.Status.ToUpper() == "PENDING")
        //        {
        //            strWhereCondition += " and (TestResult IS NULL or TestResult in ('" + clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.NCR_Raised.GetStringValue() + "')) and OfferedBy IS NOT NULL ";
        //        }
        //        strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
        //                         + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
        //                         + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
        //                           + "and SubAssemblyNo=" + Convert.ToInt32(param.subassembly);

        //        //search Condition 
        //        string[] columnName = { "RequestNo","ReqSequenceNo","ReqIterationNo","OperationNo","OperationRevisionNo",
        //                                "Activity","RefDocument","RefDocRevNo","TestResult","OfferedBy","OfferedOn",
        //                                "InspectedBy","InspectedOn","QCRemark","OfferedtoCusomerBy",  "OfferedtoCusomerOn",
        //                                "I1Intervention","I1Result","I1ResultOn","I2Intervention",
        //                                "I2Result","I2ResultOn","I3Intervention","I3Result","I3ResultOn",
        //                                "I4Intervention","I4Result","I4ResultOn"
        //                               };
        //        strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
        //        strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl050.BU", "tvl050.Location");
        //      var lstResult = db.SP_TVL_GET_ATTEND_INSPECTIONDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
        //      //  var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                        Convert.ToString(uc.ColorFlag),
        //                        Convert.ToString(uc.HeaderId),
        //                        Convert.ToString(uc.RequestId),
        //                        Convert.ToString(uc.RequestNo),
        //                        Convert.ToString(uc.ReqSequenceNo),
        //                        Convert.ToString(uc.ReqIterationNo),
        //                        uc.OperationNo.Value.ToString("G29"),
        //                        Convert.ToString("R"+uc.OperationRevisionNo),
        //                        Convert.ToString(uc.Activity),
        //                         (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "",
        //                         (!string.IsNullOrEmpty(uc.RefDocRevNo)) ?"* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "",
        //                        Convert.ToString(uc.TestResult),
        //                        Convert.ToString(uc.OfferedBy),
        //                        Convert.ToString(uc.OfferedOn),
        //                        Convert.ToString(uc.InspectedBy),
        //                        Convert.ToString(uc.InspectedOn),
        //                        //Convert.ToString(uc.QCRemark),
        //                        Convert.ToString(uc.OfferedtoCusomerBy),
        //                        Convert.ToString(uc.OfferedtoCusomerOn),
        //                         //Convert.ToString(uc.LTIntervention),
        //                            Convert.ToString(uc.LTIntervention) != null ?GetInterventionDetails(Convert.ToString(uc.LTIntervention),uc.HeaderId,uc.ReqSequenceNo):"",
        //                    Convert.ToString(uc.I1Intervention) != null ?GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.ReqSequenceNo):"",
        //                    Convert.ToString(uc.I2Intervention) != null ?GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.ReqSequenceNo):"",
        //                       // Convert.ToString(Intervention (uc.I1Intervention,uc.I1ResultBy,uc.I1Result,uc.I1ResultOn)),
        //                        //Convert.ToString(Intervention (uc.I2Intervention,uc.I2ResultBy,uc.I2Result,uc.I2ResultOn)),
        //                       // Convert.ToString(Intervention (uc.I3Intervention,uc.I3ResultBy,uc.I3Result,uc.I3ResultOn)),
        //                       // Convert.ToString(Intervention (uc.I4Intervention,uc.I4ResultBy,uc.I4Result,uc.I4ResultOn)),
        //                         Helper.GenerateTextArea(uc.HeaderId, "QCRemark", Convert.ToString(uc.QCRemark),"UpdateData(this, "+ uc.RequestId+" );",((uc.TestResult != clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue() && uc.TestResult != clsImplementationEnum.TravelerInspectionStatus.NCR_Raised.GetStringValue() && !(string.IsNullOrWhiteSpace(uc.TestResult) && !string.IsNullOrWhiteSpace(uc.OfferedBy)))?true:false),"","1000"),

        //                        "<nobr><center>"
        //                           +  Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendInspection/Details/"+uc.RequestId,false)
        //                           +  Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
        //                           +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId)
        //                             +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
        //                      +"</center></nobr>"
        //                    }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data,
        //            whereCondition = strWhereCondition,
        //            strSortOrder = strSortOrder
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public JsonResult LoadAllInspectionData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                //if (param.Status.ToUpper() == clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue().ToUpper())
                //{
                //    strWhereCondition += "1=1 and tvl040.InspectionStatus  in ( '" + clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() + "')";
                //}
                //else
                //{
                strWhereCondition += "1=1";
                //}
                strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                    + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                    + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                    + "and SubAssemblyNo=" + Convert.ToInt32(param.subassembly) + " ";

                //search Condition 
                string[] columnName = { "OperationNo", "OperationRevisionNo", "Activity", "RefDocument", "RefDocRevNo", "itenrationNo", "SubAssemblyNo", "InspectionStatus" };
                // strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ColorFlag),
                           uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :(!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :(!string.IsNullOrEmpty(uc.RefDocRevNo))? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.itenrationNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.SubAssemblyNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.InspectionStatus),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.LTIntervention) != null ? (new AttendTPIInspectionController()).GetInterventionDetails(Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),uc.HeaderId,uc.RequestId):"",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.I1Intervention) != null ? (new AttendTPIInspectionController()).GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.RequestId):"",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.I2Intervention) != null ? (new AttendTPIInspectionController()).GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.RequestId):"",
                           // Convert.ToString(uc.LTIntervention),
                            //Convert.ToString(uc.I1Intervention),
                            //Convert.ToString(uc.I2Intervention),
                           // Convert.ToString(uc.I3Intervention),
                            //Convert.ToString(uc.I4Intervention),                         
                           Convert.ToBoolean(uc.IsHeaderLine) ? "" : GetOperationRemarks(Convert.ToString(uc.LTIntervention),uc.HeaderId,uc.RequestId),
                          "<nobr><center>"
                           +  Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL +"/TVL/AttendInspection/Details/"+uc.RequestId,uc.RequestId == null ? true : false)
                           +  Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                           +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId,uc.IsHeaderLine)
                           +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                           +"</center></nobr>"
                            }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult UpdateDetails(int headerId, string columnName, string columnValue, string table)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {

                    TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == headerId).FirstOrDefault();
                    if (objTVL050 != null)
                    {
                        if (columnName == "QCRemark")
                        {
                            objTVL050.QCRemark = columnValue;
                            objTVL050.EditedBy = objClsLoginInfo.UserName;
                            objTVL050.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRemark(int headerid, string remark, string QCUsedRefDocRevNo, int requestId, string reworktraveler, string ncrno)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == headerid).FirstOrDefault();
                if (objTVL050 != null)
                {
                    objTVL050.QCUsedRefDocRevNo = QCUsedRefDocRevNo;
                    objTVL050.QCRemark = remark;
                    objTVL050.EditedBy = objClsLoginInfo.UserName;
                    objTVL050.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                TVL050 objtraveler = db.TVL050.FirstOrDefault(x => x.RequestId == requestId);
                if (objtraveler != null)
                {
                    if (!string.IsNullOrWhiteSpace(reworktraveler))
                    {
                        objtraveler.ReworkTravelerNo = reworktraveler;
                    }
                    if (!string.IsNullOrWhiteSpace(ncrno))
                    {
                        objtraveler.NCRNo = ncrno;
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region Detail Page
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Details(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            TVL050 objTVL050 = new TVL050();
            ViewBag.Title = "Attend Request";
            objTVL050 = db.TVL050.Where(x => x.RequestId == Id).FirstOrDefault();
            if (objTVL050 != null)
            {
                ViewBag.OperationId = db.TVL050.Where(x => x.TravelerNo == objTVL050.TravelerNo
                             && x.TravelerRevNo == objTVL050.TravelerRevNo
                             && x.ProjectNo == objTVL050.ProjectNo
                             && x.BU == objTVL050.BU
                             && x.Location == objTVL050.Location
                             && x.AssemblyNo == objTVL050.AssemblyNo
                             && x.SubAssemblyNo == objTVL050.SubAssemblyNo)
                             .OrderByDescending(x => x.RequestId)
                             .Select(x => x.RequestId).FirstOrDefault();
                string status = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
                ViewBag.ReworkTravelerRevNo = db.TVL010_Log.Where(x => x.ProjectNo.Trim() == objTVL050.ProjectNo.Trim()
                                              && x.IdentificationNo.Trim() == objTVL050.AssemblyNo.Trim()
                                              && x.BU.Trim() == objTVL050.BU.Trim()
                                              && x.Location.Trim() == objTVL050.Location.Trim()
                                              && x.TravelerNo.Trim() != objTVL050.ReworkTravelerNo.Trim()
                                               && x.Status == status).Select(x => x.TravelerRevNo).FirstOrDefault();
                objTVL050.LTIntervention = objTVL050.LTIntervention == "E" ? Manager.TVLIntervention(objTVL050.LTIntervention) : objNDEModels.GetCategory("Traveler Intervention", objTVL050.LTIntervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I1Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I1Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I2Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I2Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I3Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I3Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I4Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I4Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.BU = db.COM002.Where(x => x.t_dimx == objTVL050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.Location = db.COM002.Where(x => x.t_dimx == objTVL050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.OfferedBy = objTVL050.OfferedBy != null ? Manager.GetPsidandDescription(objTVL050.OfferedBy) : string.Empty;
                objTVL050.OfferedtoCusomerBy = objTVL050.OfferedtoCusomerBy != null ? Manager.GetPsidandDescription(objTVL050.OfferedtoCusomerBy) : string.Empty;
                objTVL050.InspectedBy = objTVL050.InspectedBy != null ? Manager.GetPsidandDescription(objTVL050.InspectedBy) : string.Empty;
                var Returned_By_TPI = clsImplementationEnum.TravelerInspectionStatus.Returned_By_TPI.GetStringValue();

                if (objTVL050.TestResult == Returned_By_TPI)
                {
                    var tpiremark = db.TVL050.Where(x => x.RequestNo == objTVL050.RequestNo && (x.TestResult != Returned_By_TPI))
                                               .OrderByDescending(x => x.RequestId)
                                               .Select(x => x.TPIRemarks)
                                               .FirstOrDefault();
                    if (tpiremark != null)
                    {
                        objTVL050.TPIRemarks = tpiremark;
                    }
                }
                ViewBag.ProjectNo = objTVL050.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL050.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault();
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL050.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1Intervention";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2Intervention";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3Intervention";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4Intervention";
                    }

                }
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == objTVL050.HeaderId).FirstOrDefault();
                if(objtvl040!=null)
                {
                    if(objtvl040.IsCompleted==true)
                    {
                        ViewBag.attachment = true;
                    }
                    else
                    {
                        ViewBag.attachment = false;
                    }

                }
            }
            return View(objTVL050);
        }

        [HttpPost]
        public ActionResult GetRejectRequestPartial(int requestId)
        {
            TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == requestId).FirstOrDefault();
            return PartialView("_GetRejectTravelerPartial", objTVL050);
        }


        [HttpPost]
        public ActionResult GetNCRDetails(int requestId)
        {
            TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == requestId).FirstOrDefault();
            return PartialView("_GetRejectWithNCROptionPartial", objTVL050);
        }

        [HttpPost]
        public bool IsValidate(int requestId, string qcremark)
        {
            bool flag = true;
            TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == requestId).FirstOrDefault();
            string folderpath = "TVL050/" + objTVL050.HeaderId + "/" + objTVL050.ReqIterationNo + "/" + objTVL050.ReqSequenceNo;
            var doc = (new clsFileUpload()).GetDocuments(folderpath, true).Select(x => x.Name).ToList();
            if (string.IsNullOrWhiteSpace(qcremark) && doc.Count == 0)
            {
                flag = false;
            }
            return flag;
        }


        #region Clear
        [HttpPost]
        public ActionResult ClearOperation(int requestId, string QCUsedRefDocRevNo, string reworktraveler, string ncrno, string qcRemark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL050 objTVL050 = db.TVL050.FirstOrDefault(x => x.RequestId == requestId);
                string strCleared = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
                bool flagSendtoTPI = false;
                #region Clear request
                TVL040 objTVL040 = db.TVL040.FirstOrDefault(x => x.HeaderId == objTVL050.HeaderId);

                if (string.IsNullOrWhiteSpace(objTVL050.I1Intervention)
                   && string.IsNullOrWhiteSpace(objTVL050.I2Intervention)
                   && string.IsNullOrWhiteSpace(objTVL050.I3Intervention)
                   && string.IsNullOrWhiteSpace(objTVL050.I4Intervention))
                {
                    if (objTVL040 != null)
                    {
                        objTVL040.InspectionStatus = strCleared;
                        objTVL040.EditedBy = objClsLoginInfo.UserName;
                        objTVL040.EditedOn = DateTime.Now;
                    }
                }
                else
                {
                    if (objTVL040 != null)
                    {
                        objTVL040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                        objTVL040.EditedBy = objClsLoginInfo.UserName;
                        objTVL040.EditedOn = DateTime.Now;
                        flagSendtoTPI = true;
                    }
                }
                if (!string.IsNullOrWhiteSpace(reworktraveler))
                {
                    objTVL050.ReworkTravelerNo = reworktraveler;
                }
                if (!string.IsNullOrWhiteSpace(ncrno))
                {
                    objTVL050.NCRNo = ncrno;
                }
                objTVL050.QCUsedRefDocRevNo = QCUsedRefDocRevNo;
                objTVL050.QCRemark = qcRemark;
                objTVL050.TVL040.ColorFlag = null;
                objTVL050.TestResult = strCleared;
                objTVL050.TVL040.LnTInspectorResult = strCleared;
                objTVL050.TVL040.LnTInspectorResultOn = DateTime.Now;
                objTVL050.OfferedtoCusomerBy = objClsLoginInfo.UserName;
                objTVL050.OfferedtoCusomerOn = DateTime.Now;
                objTVL050.InspectedBy = objClsLoginInfo.UserName;
                objTVL050.InspectedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                if (flagSendtoTPI)
                {
                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.CR1.GetStringValue()), objTVL050.BU, objTVL050.Location,
                     "Traveler: Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + " & Identification Sr.No " + objTVL050.SubAssemblyNo + " & Project No:  " + objTVL050.ProjectNo + "  has been cleared by L & T Inspector and offered for TPI Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendTPIInspection/Details/" + objTVL050.RequestId);
                }
                else
                {
                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PROD3.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG2.GetStringValue()), objTVL050.BU, objTVL050.Location,
                       "Traveler: Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + "  & Identification Sr. No  " + objTVL050.SubAssemblyNo + "  & Project No:  " + objTVL050.ProjectNo + "  has been Cleared", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg1.Key = true;
                #endregion

                if (objResponseMsg1.Key)
                {
                    objResponseMsg1.Value = "Data Cleared  Successfully ";
                }
                else
                {
                    objResponseMsg1.Value = "Error Occured, Please try again";
                }
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg1.Key = false;
                objResponseMsg1.Value = "Error Occured, Please try again";
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Return request
        [HttpPost]
        public ActionResult ReturnOperation(int requestId, string qcRemark, string reworktraveler, string ncrno)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL050 objTVL050 = db.TVL050.FirstOrDefault(x => x.RequestId == requestId);
                string returned = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();

                objTVL050.TestResult = returned;
                objTVL050.QCRemark = qcRemark;
                if (!string.IsNullOrWhiteSpace(reworktraveler))
                {
                    objTVL050.ReworkTravelerNo = reworktraveler;
                }
                if (!string.IsNullOrWhiteSpace(ncrno))
                {
                    objTVL050.NCRNo = ncrno;
                }
                objTVL050.TVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
                objTVL050.TVL040.LnTInspectorResult = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
                objTVL050.TVL040.LnTInspectorResultOn = DateTime.Now;
                objTVL050.TVL040.EditedBy = objClsLoginInfo.UserName;
                objTVL050.TVL040.EditedOn = DateTime.Now;
                objTVL050.InspectedBy = objClsLoginInfo.UserName;
                objTVL050.InspectedOn = DateTime.Now;
                objResponseMsg1 = InsertRequestOnReturnReject(objTVL050, returned, null);

                if (objResponseMsg1.Key)
                {
                    objResponseMsg1.Value = "Data Returned Successfully ";
                }
                else
                {
                    objResponseMsg1.Value = "Error Occured, Please try again";
                }
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg1.Key = false;
                objResponseMsg1.Value = "Error Occured, Please try again";
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region reject request
        [HttpPost]
        public ActionResult RejectOperation(int requestId, string qcRemark, string type, string reworktraveler, string ncrno)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool flag = true;
                TVL050 objTVL050 = db.TVL050.FirstOrDefault(x => x.RequestId == requestId);
                string Notacceptable = clsImplementationEnum.TravelerInspectionStatus.Not_Acceptable.GetStringValue();
                string ncr_raised = clsImplementationEnum.TravelerInspectionStatus.NCR_Raised.GetStringValue();

                objTVL050.QCRemark = qcRemark;
                if (type == clsImplementationEnum.TravelerInspectionStatus.NCR_No.GetStringValue())
                {
                    objTVL050.IsNCRNo = true;
                    objTVL050.NCRNo = !string.IsNullOrWhiteSpace(ncrno) ? ncrno : null;
                    if (string.IsNullOrWhiteSpace(ncrno))
                    {
                        objTVL050.TestResult = ncr_raised;
                        objTVL050.TVL040.LnTInspectorResult = ncr_raised;
                        flag = false;
                        db.SaveChanges();
                        objResponseMsg1.Key = true;
                        objResponseMsg1.RequestId = objTVL050.RequestId;
                    }
                    else
                    { objTVL050.TVL040.ColorFlag = null; }
                }

                if (type == clsImplementationEnum.TravelerInspectionStatus.Rework_Traveler.GetStringValue())
                {
                    objTVL050.TVL040.ColorFlag = clsImplementationEnum.ColorFlagForTraveler.Orange.GetStringValue();
                    objTVL050.IsReworkTraveler = true;
                    objTVL050.ReworkTravelerNo = !string.IsNullOrWhiteSpace(reworktraveler) ? reworktraveler : null;
                }
                if (flag)
                {

                    objTVL050.TestResult = Notacceptable;
                    objTVL050.TVL040.itenrationNo = objTVL050.ReqIterationNo;
                    objTVL050.TVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Rework.GetStringValue();
                    objTVL050.TVL040.LnTInspectorResult = Notacceptable;
                    objTVL050.TVL040.LnTInspectorResultOn = DateTime.Now;
                    objTVL050.TVL040.EditedBy = objClsLoginInfo.UserName;
                    objTVL050.TVL040.EditedOn = DateTime.Now;
                    objTVL050.InspectedBy = objClsLoginInfo.UserName;
                    objTVL050.InspectedOn = DateTime.Now;

                    objResponseMsg1 = InsertRequestOnReturnReject(objTVL050, Notacceptable, type);
                }
                if (objResponseMsg1.Key)
                {
                    objResponseMsg1.Value = "Data Not Acceptable Successfully ";
                }
                else
                {
                    objResponseMsg1.Value = "Error Occured, Please try again";
                }
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg1.Key = false;
                objResponseMsg1.Value = "Error Occured, Please try again";
                return Json(objResponseMsg1, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Auto Insert new record in Reject/Return Case
        public clsHelper.ResponseMsgWithStatus InsertRequestOnReturnReject(TVL050 objsrcTVL050, string type, string rejectoption)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int IterationNo = 0;
                int ReqSeqNo = 0;

                string newfolderPath = "";
                string oldfolderPath = "";
                if (type == clsImplementationEnum.TravelerInspectionStatus.Not_Acceptable.GetStringValue())
                {
                    IterationNo = Convert.ToInt32(objsrcTVL050.ReqIterationNo + 1);
                    ReqSeqNo = Convert.ToInt32(objsrcTVL050.ReqSequenceNo);
                }
                else if (type == clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue())
                {
                    IterationNo = Convert.ToInt32(objsrcTVL050.ReqIterationNo);
                    ReqSeqNo = Convert.ToInt32(objsrcTVL050.ReqSequenceNo + 1);
                }
                TVL050 objTVL050 = db.TVL050.Add(new TVL050
                {
                    RequestId = objsrcTVL050.RequestId,
                    HeaderId = objsrcTVL050.HeaderId,
                    RequestNo = objsrcTVL050.RequestNo,
                    ReqIterationNo = IterationNo,
                    ReqSequenceNo = ReqSeqNo,
                    ProjectNo = objsrcTVL050.ProjectNo,
                    Customer = objsrcTVL050.Customer,
                    TravelerNo = objsrcTVL050.TravelerNo,
                    TravelerRevNo = objsrcTVL050.TravelerRevNo,
                    BU = objsrcTVL050.BU,
                    Location = objsrcTVL050.Location,
                    Identification = objsrcTVL050.Identification,
                    AssemblyNo = objsrcTVL050.AssemblyNo,
                    OperationNo = objsrcTVL050.OperationNo,
                    OperationRevisionNo = objsrcTVL050.OperationRevisionNo,
                    Activity = objsrcTVL050.Activity,
                    RefDocument = objsrcTVL050.RefDocument,
                    RefDocRevNo = objsrcTVL050.RefDocRevNo,
                    LTIntervention = objsrcTVL050.LTIntervention,
                    I1Intervention = objsrcTVL050.I1Intervention,
                    I2Intervention = objsrcTVL050.I2Intervention,
                    I3Intervention = objsrcTVL050.I3Intervention,
                    I4Intervention = objsrcTVL050.I4Intervention,
                    InspectionRecordRequired = objsrcTVL050.InspectionRecordRequired,
                    OfferLocation = objsrcTVL050.OfferLocation,
                    OfferedBy = null,
                    OfferedOn = null,
                    TestResult = null,
                    InspectedBy = null,
                    InspectedOn = null,
                    QCRemark = null,
                    OfferedtoCusomerBy = null,
                    OfferedtoCusomerOn = null,
                    I1Result = objsrcTVL050.I1Result,
                    I1ResultOn = objsrcTVL050.I1ResultOn,
                    I2Result = objsrcTVL050.I2Result,
                    I2ResultOn = objsrcTVL050.I2ResultOn,
                    I3Result = objsrcTVL050.I3Result,
                    I3ResultOn = objsrcTVL050.I3ResultOn,
                    I4Result = objsrcTVL050.I4Result,
                    I4ResultOn = objsrcTVL050.I4ResultOn,
                    SubAssemblyNo = objsrcTVL050.SubAssemblyNo,
                    IsReworkTraveler = objsrcTVL050.IsReworkTraveler == true ? true : false,
                    ReworkTravelerNo = objsrcTVL050.ReworkTravelerNo,
                    //IsNCRNo = objsrcTVL050.IsNCRNo,
                    //NCRNo = objsrcTVL050.NCRNo,
                    ShopUsedRefDocRevNo = objsrcTVL050.ShopUsedRefDocRevNo,
                    QCUsedRefDocRevNo = objsrcTVL050.QCUsedRefDocRevNo,
                    ShopRemark = objsrcTVL050.ShopRemark,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                db.SaveChanges();
                #region copy documents
                oldfolderPath = "TVL050/" + objsrcTVL050.HeaderId + "/" + objsrcTVL050.ReqIterationNo + "/" + objsrcTVL050.ReqSequenceNo;
                newfolderPath = "TVL050/" + objsrcTVL050.HeaderId + "/" + IterationNo + "/" + ReqSeqNo;
                (new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);
                #endregion

                #region Send Notification

                if (type == clsImplementationEnum.TravelerInspectionStatus.Not_Acceptable.GetStringValue())
                {
                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PROD3.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG2.GetStringValue()), objTVL050.BU, objTVL050.Location,
                     "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + "  & Identification Sr. No  " + objTVL050.SubAssemblyNo + "  & Project No:  " + objTVL050.ProjectNo + "   has been Not Acceptable with Rework Traveler", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/OfferInspection/OfferOperation/" + objTVL050.TVL040.HeaderId);
                }
                else if (type == clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue())
                {
                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PROD3.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG2.GetStringValue()), objTVL050.BU, objTVL050.Location,
                    "Traveler: Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " for Identification: " + objTVL050.AssemblyNo + "  &Identification Sr.No " + objTVL050.SubAssemblyNo + " &Project No:  " + objTVL050.ProjectNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/OfferInspection/ReofferDetails/" + objTVL050.RequestId);
                }

                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.RequestId = objTVL050.RequestId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return objResponseMsg;
        }
        #endregion
        #endregion

        #region Common Functions

        [HttpPost]
        public ActionResult Updatetravelercolor(string strHeader, string Strcolor)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == headerId).FirstOrDefault();
                        if (Strcolor == "Red")
                        {
                            objtvl040.ColorFlag = clsImplementationEnum.ColorFlagForTraveler.Red.GetStringValue();
                            objtvl040.EditedBy = objClsLoginInfo.UserName;
                            objtvl040.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        else if (Strcolor == "Yellow")
                        {
                            objtvl040.ColorFlag = clsImplementationEnum.ColorFlagForTraveler.Yellow.GetStringValue();
                            objtvl040.EditedBy = objClsLoginInfo.UserName;
                            objtvl040.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            objtvl040.ColorFlag = null;
                            objtvl040.EditedBy = objClsLoginInfo.UserName;
                            objtvl040.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Submit Successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Atleast One Record ";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetTravelerNo(string term, string project, string identification, string bu, string location, string travelerno)
        {
            string status = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
            List<CategoryData> lstTravelerno = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstTravelerno = db.TVL040.Where(x => x.ProjectNo.Trim() == project.Trim()
                                                  && x.AssemblyNo.Trim() == identification.Trim()
                                                  && x.BU.Trim() == bu.Trim()
                                                  && x.Location.Trim() == location.Trim()
                                                  && x.TravelerNo.Contains(term)
                                                  && x.TravelerNo.Trim() != travelerno.Trim())
                       .Select(x => new CategoryData { id = x.TravelerNo, text = x.TravelerNo, Value = x.TravelerRevNo.ToString() }).Distinct().ToList();
            }
            else
            {
                lstTravelerno = db.TVL040.Where(x => x.ProjectNo.Trim() == project.Trim()
                                                && x.AssemblyNo.Trim() == identification.Trim()
                                                && x.BU.Trim() == bu.Trim()
                                                && x.Location.Trim() == location.Trim()
                                                && x.TravelerNo.Trim() != travelerno.Trim())
                    .Select(x => new CategoryData { id = x.TravelerNo, text = x.TravelerNo, Value = x.TravelerRevNo.ToString() }).Distinct().Take(10).ToList();
            }
            return Json(lstTravelerno, JsonRequestBehavior.AllowGet);
        }
        public string GetInterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            if (InspectionStatus == "S")
            {
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == Headerid).FirstOrDefault();
                if (objtvl040 != null)
                {
                    if (!string.IsNullOrEmpty(objtvl040.LnTInspectorResult))
                    {
                        InspectionStatus = objtvl040.LTIntervention + "<br>" + objtvl040.LnTInspectorResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl040.EditedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl040.LnTInspectorResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl040.LnTInspectorResultOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = objtvl040.LTIntervention;
                    }
                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            else
            {
                TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
                if (objtvl050 != null)
                {
                    if (!string.IsNullOrEmpty(objtvl050.TestResult))
                    {
                        InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention) + "<br>" + objtvl050.TestResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.InspectedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention);
                    }
                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            return InspectionStatus;
        }

        public string GetI1InterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
            if (objtvl050 != null)
            {
                if (!string.IsNullOrEmpty(objtvl050.I1Result))
                {
                    InspectionStatus = objtvl050.I1Intervention + "<br>" + objtvl050.I1Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I1ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy"));
                }
                else
                {
                    InspectionStatus = objtvl050.I1Intervention;
                }
                //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");

            }

            return InspectionStatus;
        }
        public string GetI2InterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
            if (objtvl050 != null)
            {
                if (!string.IsNullOrEmpty(objtvl050.I2Result))
                {
                    InspectionStatus = objtvl050.I2Intervention + "<br>" + objtvl050.I2Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I2ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                }
                else
                {
                    InspectionStatus = objtvl050.I2Intervention;
                }
                //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");

            }

            return InspectionStatus;
        }
        public string GetOperationRemarks(string InspectionStatus, int Headerid, int? RequestId)
        {
            if (InspectionStatus == "S")
            {
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == Headerid).FirstOrDefault();
                if (objtvl040 != null)
                {
                    InspectionStatus = objtvl040.ShopRemark;

                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            else
            {
                TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
                if (objtvl050 != null)
                {
                    InspectionStatus = objtvl050.QCRemark;

                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            return InspectionStatus;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_TVL_GET_INSPECTIONDATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.ProjectNo),
                                      TravelerNo = Convert.ToString(uc.TravelerNo),
                                      TravelerRevNo = Convert.ToString("R" + uc.TravelerRevNo),
                                      TestResult = Convert.ToString(uc.TestResult)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}