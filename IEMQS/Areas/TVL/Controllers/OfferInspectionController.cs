﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.TVL.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TVL.Controllers
{
    public class OfferInspectionController : clsBase
    {
        // GET: TVL/OfferInspection
        #region Index Grid
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index(string tab)
        {

            ViewBag.Title = "Offer Operations";
            ViewBag.Tab = tab;
            return View();
        }

        [HttpPost]
        public ActionResult LoadOfferIndexPartial(string status)
        {
            if (status.ToLower() == "initiate")
            {
                List<string> lstProCat = clsImplementationEnum.getTravelerIdentification().ToList();
                ViewBag.Identification = lstProCat.Select(i => new CategoryData { Value = i.ToString(), Code = i.ToString(), CategoryDescription = i.ToString() }).ToList();
                ViewBag.Status = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
                return PartialView("_GetIndexGridInitiateDataPartial");
            }
            else
            {
                ViewBag.Status = status;
                return PartialView("_GetIndexGridDataPartial");
            }
        }

        //bind data for Initiate data tab
        [HttpPost]
        public JsonResult LoadInitiateData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 and tvl010.Status='" + clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue() + "'";
                //search Condition 
                string[] columnName = { "ProjectNo", "Identification", "IdentificationNo", "TravelerNo", "TravelerRevNo" };


                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl010.BU", "tvl010.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_TVL_GET_INITIATEDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                         {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.ProjectNo),
                           // Convert.ToString(uc.Identification),
                           (uc.Identification==clsImplementationEnum.TravelerIdentificationDropdown.StandardTraveler.GetStringValue() ) ?  Helper.HTMLAutoComplete(uc.HeaderId, "txtidentification", uc.Identification, "", false, "","Testidentification",false, "50") : uc.Identification,
                           (uc.Identification==clsImplementationEnum.TravelerIdentificationDropdown.StandardTraveler.GetStringValue() ) ?  Helper.GenerateHTMLTextbox(uc.HeaderId, "IdentificationNo" ,"", "", false, "",false,"50") : uc.IdentificationNo,
                           // Convert.ToString(uc.IdentificationNo),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString("R"+uc.TravelerRevNo),
                            Helper.GenerateHTMLTextbox(uc.HeaderId, "SerialNoFrom" ,"", "", false, "",false,"3"),
                            Helper.GenerateHTMLTextbox(uc.HeaderId, "SerialNoTo" ,"", "", false, "",false,"3"),

                           Helper.GenerateGridButton(uc.HeaderId, "Initiate", "Initiate Traveler", "fa fa-paper-plane", "InitiateTraveler("+uc.HeaderId+");")
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //bind data for offer Index tab
        [HttpPost]
        public JsonResult LoadOfferIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition40 = string.Empty;
                string strWhereCondition50 = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "ProjectNo", "TravelerNo", "BU", "Location", "AssemblyNo", "Customer", "TravelerRevNo" };
                strWhereCondition40 += "1=1 and  tvl40.InspectionStatus in ('" + clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() + "') ";

                strWhereCondition40 += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl40.BU", "tvl40.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition40 += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                else
                {
                    strWhereCondition40 += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhereCondition50 += "1=1";

                strWhereCondition50 += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl50.BU", "tvl50.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition50 += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                else
                {
                    strWhereCondition50 += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_OFFER_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition40, strWhereCondition50).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString("R"+uc.TravelerRevNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/OfferInspection/OfferOperation/"+uc.HeaderId,false)
                        +   Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.Project+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.AssemblyNo+"','"+uc.SubAssemblyNo+"')","",false)
                          
                           //null,null,
                           // uc.HeaderId,"Traveler Details",40,"true","Traveler Details","Traveler Request Details")
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition40,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //bind data for offer Index tab
        [HttpPost]
        public JsonResult LoadOfferAllTabIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "ProjectNo", "TravelerNo", "BU", "Location", "AssemblyNo", "Customer", "TravelerRevNo" };
                strWhereCondition += "1=1";
                // strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl40.BU", "tvl40.Location");

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_TVL_OFFER_INDEX_GETALLDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString("R"+uc.TravelerRevNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/OfferInspection/OfferOperation/"+uc.HeaderId,false)
                        +   Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.Project+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.AssemblyNo+"','"+uc.SubAssemblyNo+"')","",false)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Offer Operations Page
        [SessionExpireFilter]
        public ActionResult OfferOperation(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            TVL040 objTVL040 = new TVL040();
            ViewBag.Title = "Offer Operations";
            objTVL040 = db.TVL040.Where(x => x.HeaderId == Id).FirstOrDefault();
            if (objTVL040 != null)
            {
                TVL010 objTVL010 = db.TVL010.Where(x => x.ProjectNo == objTVL040.ProjectNo && x.BU == objTVL040.BU
                                                        && x.Customer == objTVL040.Customer && x.Location == objTVL040.Location
                                                        && x.Identification == objTVL040.Identification && x.TravelerNo == objTVL040.TravelerNo && x.IdentificationNo == objTVL040.AssemblyNo).FirstOrDefault();
                if (objTVL010 != null)
                {
                    ViewBag.Note1 = objTVL010.Note1;
                    ViewBag.Note2 = objTVL010.Note2;
                    ViewBag.Note3 = objTVL010.Note3;
                    ViewBag.Note4 = objTVL010.Note4;
                    ViewBag.Note5 = objTVL010.Note5;
                    ViewBag.Note6 = objTVL010.Note6;
                    ViewBag.Note7 = objTVL010.Note7;
                    ViewBag.Note8 = objTVL010.Note8;
                    ViewBag.Note9 = objTVL010.Note9;
                    ViewBag.Note10 = objTVL010.Note10;
                    ViewBag.Note11 = objTVL010.Note11;
                    ViewBag.Note12 = objTVL010.Note12;
                    ViewBag.Note13 = objTVL010.Note13;
                    ViewBag.Note14 = objTVL010.Note14;
                    ViewBag.Note15 = objTVL010.Note15;
                }

                ViewBag.ProjectNo = objTVL040.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL040.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault();
                objTVL040.BU = db.COM002.Where(x => x.t_dimx == objTVL040.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL040.Location = db.COM002.Where(x => x.t_dimx == objTVL040.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();

            }
            return View(objTVL040);
        }

        [HttpPost]
        public ActionResult GetOfferDataPartial(string status, TVL040 objTVL040)
        {
            objTVL040.BU = objTVL040.BU.Split('-')[0];
            objTVL040.Location = objTVL040.Location.Split('-')[0];
            TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL040.ProjectNo).FirstOrDefault();
            if (objtvl001 != null)
            {

                if (objtvl001.I1Intervention != null)
                {
                    ViewBag.I1Intervention = objtvl001.I1Intervention;
                }
                else
                {
                    ViewBag.I1Intervention = "I1";
                }
                if (objtvl001.I2Intervention != null)
                {
                    ViewBag.I2Intervention = objtvl001.I2Intervention;
                }
                else
                {
                    ViewBag.I2Intervention = "I2";
                }
                if (objtvl001.I3Intervention != null)
                {
                    ViewBag.I3Intervention = objtvl001.I3Intervention;
                }
                else
                {
                    ViewBag.I3Intervention = "I3";
                }
                if (objtvl001.I4Intervention != null)
                {
                    ViewBag.I4Intervention = objtvl001.I4Intervention;
                }
                else
                {
                    ViewBag.I4Intervention = "I4";
                }

            }
            if (status.ToLower() == "reoffer")
            {
                ViewBag.Status = "Pending";
                return PartialView("_GetIndexGridReOfferDataPartial", objTVL040);
            }
            else
            {
                ViewBag.Status = status;
                return PartialView("_GetOfferOperationGridDataPartial", objTVL040);
            }
        }

        //bind data for offer/All Detail tab
        [HttpPost]
        public JsonResult LoadOfferData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                if (param.Status.ToUpper() == clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue().ToUpper())
                {
                    strWhereCondition += "1=1 and tvl040.InspectionStatus  in ( '" + clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() + "','" + clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() + "')";
                }
                else
                {
                    strWhereCondition += "1=1";
                }
                strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                    + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                    + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                    + "and SubAssemblyNo=" + Convert.ToInt32(param.subassembly) + " ";

                //search Condition 
                string[] columnName = { "OperationNo", "OperationRevisionNo", "Activity", "RefDocument", "RefDocRevNo", "itenrationNo", "SubAssemblyNo", "InspectionStatus" };
                // strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ColorFlag),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : (!string.IsNullOrEmpty(uc.RefDocRevNo))? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  Convert.ToString(uc.itenrationNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  Convert.ToString(uc.SubAssemblyNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  Convert.ToString(uc.InspectionStatus),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  Convert.ToString(uc.LTIntervention) != null ? (new AttendTPIInspectionController()).GetInterventionDetails(Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),uc.HeaderId,uc.RequestId):"",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  Convert.ToString(uc.I1Intervention) != null ? (new AttendTPIInspectionController()).GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.RequestId):"",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :  Convert.ToString(uc.I2Intervention) != null ? (new AttendTPIInspectionController()).GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.RequestId):"",
                           // Convert.ToString(uc.LTIntervention),
                            //Convert.ToString(uc.I1Intervention),
                            //Convert.ToString(uc.I2Intervention),
                           // Convert.ToString(uc.I3Intervention),
                            //Convert.ToString(uc.I4Intervention),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" :   Helper.GenerateTextArea(uc.HeaderId, "ShopRemark", Convert.ToString(uc.ShopRemark),"UpdateData(this, "+ uc.HeaderId+" );",((uc.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() && uc.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue())?true:false),"","1000"),
                           "<nobr><center>" +
                             Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/OfferInspection/Details/"+uc.HeaderId,false)
                            + Helper.GenerateActionIcon(uc.HeaderId, "RequestClear", "Request Clear", "fa fa-check-square-o", "RequestClear("+uc.HeaderId+")","",uc.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue(),false,"color:green")
                            + Helper.GenerateActionIcon(uc.HeaderId, "notApplicable", "Not Applicable", "fa fa-close", "OfferApplicable("+uc.HeaderId+")","",uc.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue(),false,"color:red")
                            + Helper.GenerateActionIcon(uc.HeaderId, "RequestOffer", "Request Offer", "fa fa-paper-plane", "RequestOffer("+uc.HeaderId+")","",uc.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue())
                            + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                            + Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId,uc.IsHeaderLine)
                            +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                            +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateDetails(int headerId, string columnName, string columnValue, int? RequestId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {

                    TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    if (objTVL040 != null)
                    {
                        if (columnName == "ShopRemark")
                        {
                            objTVL040.ShopRemark = columnValue;
                            objTVL040.EditedBy = objClsLoginInfo.UserName;
                            objTVL040.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                    if (RequestId > 0)
                    {
                        TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == RequestId).FirstOrDefault();
                        if (objTVL050 != null)
                        {
                            if (columnName == "ShopRemark")
                            {
                                objTVL050.ShopRemark = columnValue;
                                objTVL050.EditedBy = objClsLoginInfo.UserName;
                                objTVL050.EditedOn = DateTime.Now;
                                db.SaveChanges();
                            }
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //bind data for reoffer tab
        [HttpPost]
        public JsonResult LoadReofferData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += " 1=1 and (TestResult in('') or TestResult is null) and OfferedBy is null and OfferedOn is null ";
                strWhereCondition += "and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                  + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                  + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                  + "and SubAssemblyNo=" + Convert.ToInt32(param.Sequenceno) + " ";

                //search Condition 
                string[] columnName = { "OperationNo", "OperationRevisionNo", "Activity", "RefDocRevNo", "TestResult" };
                // strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl050.BU", "tvl050.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_INSPECTIONDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ColorFlag),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            (!string.IsNullOrEmpty(uc.RefDocRevNo))? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToString(uc.TestResult),
                            //Convert.ToString(uc.LTIntervention),
                           // Convert.ToString(uc.I1Intervention),
                           // Convert.ToString(uc.I2Intervention),
                            Convert.ToString(uc.LTIntervention) != null ? (new AttendTPIInspectionController()).GetInterventionDetails(Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),uc.HeaderId,uc.RequestId):"",
                            Convert.ToString(uc.I1Intervention) != null ? (new AttendTPIInspectionController()).GetI1InterventionDetails(Convert.ToString(uc.I1Intervention),uc.HeaderId,uc.RequestId):"",
                            Convert.ToString(uc.I2Intervention) != null ? (new AttendTPIInspectionController()).GetI2InterventionDetails(Convert.ToString(uc.I2Intervention),uc.HeaderId,uc.RequestId):"",
                            //Convert.ToString(uc.I3Intervention),
                          //  Convert.ToString(uc.I4Intervention),
                            Helper.GenerateTextArea(uc.HeaderId, "ShopRemark", Convert.ToString(uc.ShopRemark),"UpdateData(this, "+ uc.HeaderId+","+uc.RequestId+");",(string.IsNullOrWhiteSpace(uc.OfferedBy)?false:true),"","1000"),

                             "<nobr><center>" +
                                Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/OfferInspection/ReofferDetails/"+uc.RequestId,false)
                                +  Helper.GenerateActionIcon(uc.HeaderId, "RequestOffer", "Request Offer", "fa fa-paper-plane", "RequestOffer("+uc.RequestId+",true,'"+uc.ColorFlag+"')","",false)
                                + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                                +  Helper.ViewTVLRefDocument(uc.ProjectNo,uc.TravelerNo,uc.AssemblyNo,uc.BU,uc.Location,uc.OperationNo,uc.HeaderId)
                                +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                
                                //+  Helper.GenerateActionIcon(uc.HeaderId, "Timeline", "View Timeline", "fa fa-clock-o", "","/TVL/AttendInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) ,false)
                                +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Detail Page

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Details(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            TVL040 objTVL040 = new TVL040();
            ViewBag.Title = "Offer Request";
            objTVL040 = db.TVL040.Where(x => x.HeaderId == Id).FirstOrDefault();
            if (objTVL040 != null)
            {
                ViewBag.OperationId = db.TVL040.Where(x => x.TravelerNo == objTVL040.TravelerNo
                                          && x.TravelerRevNo == objTVL040.TravelerRevNo
                                          && x.ProjectNo == objTVL040.ProjectNo
                                          && x.BU == objTVL040.BU
                                          && x.Location == objTVL040.Location
                                          && x.AssemblyNo == objTVL040.AssemblyNo
                                          && x.SubAssemblyNo == objTVL040.SubAssemblyNo)
                                          .OrderByDescending(x => x.HeaderId)
                                          .Select(x => x.HeaderId).FirstOrDefault();
                objTVL040.LTIntervention = objTVL040.LTIntervention == "E" ? Manager.TVLIntervention(objTVL040.LTIntervention) : objNDEModels.GetCategory("Traveler Intervention", objTVL040.LTIntervention, objTVL040.BU, objTVL040.Location, true).CategoryDescription;
                objTVL040.I1Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL040.I1Intervention, objTVL040.BU, objTVL040.Location, true).CategoryDescription;
                objTVL040.I2Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL040.I2Intervention, objTVL040.BU, objTVL040.Location, true).CategoryDescription;
                objTVL040.I3Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL040.I3Intervention, objTVL040.BU, objTVL040.Location, true).CategoryDescription;
                objTVL040.I4Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL040.I4Intervention, objTVL040.BU, objTVL040.Location, true).CategoryDescription;
                objTVL040.BU = db.COM002.Where(x => x.t_dimx == objTVL040.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL040.Location = db.COM002.Where(x => x.t_dimx == objTVL040.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                ViewBag.ProjectNo = objTVL040.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL040.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault();
                TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == objTVL040.ProjectNo).FirstOrDefault();
                if (objtvl001 != null)
                {

                    if (objtvl001.I1Intervention != null)
                    {
                        ViewBag.I1Intervention = objtvl001.I1Intervention;
                    }
                    else
                    {
                        ViewBag.I1Intervention = "I1Intervention";
                    }
                    if (objtvl001.I2Intervention != null)
                    {
                        ViewBag.I2Intervention = objtvl001.I2Intervention;
                    }
                    else
                    {
                        ViewBag.I2Intervention = "I2Intervention";
                    }
                    if (objtvl001.I3Intervention != null)
                    {
                        ViewBag.I3Intervention = objtvl001.I3Intervention;
                    }
                    else
                    {
                        ViewBag.I3Intervention = "I3Intervention";
                    }
                    if (objtvl001.I4Intervention != null)
                    {
                        ViewBag.I4Intervention = objtvl001.I4Intervention;
                    }
                    else
                    {
                        ViewBag.I4Intervention = "I4Intervention";
                    }

                }

            }
            return View(objTVL040);
        }

        //View for Re offer Request
        [SessionExpireFilter]
        public ActionResult ReofferDetails(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            TVL050 objTVL050 = new TVL050();
            ViewBag.Title = "Re-Offer Request";
            objTVL050 = db.TVL050.Where(x => x.RequestId == Id).FirstOrDefault();
            if (objTVL050 != null)
            {
                //objTVL050.ProjectNo = Manager.GetProjectAndDescription(objTVL050.ProjectNo);
                ViewBag.OperationId = db.TVL050.Where(x => x.TravelerNo == objTVL050.TravelerNo
                                        && x.TravelerRevNo == objTVL050.TravelerRevNo
                                        && x.ProjectNo == objTVL050.ProjectNo
                                        && x.BU == objTVL050.BU
                                        && x.Location == objTVL050.Location
                                        && x.AssemblyNo == objTVL050.AssemblyNo)
                                        .OrderByDescending(x => x.HeaderId)
                                        .Select(x => x.HeaderId).FirstOrDefault();
                objTVL050.LTIntervention = objTVL050.LTIntervention == "E" ? Manager.TVLIntervention(objTVL050.LTIntervention) : objNDEModels.GetCategory("Traveler Intervention", objTVL050.LTIntervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I1Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I1Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I2Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I2Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I3Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I3Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.I4Intervention = objNDEModels.GetCategory("Traveler TPI Intervention", objTVL050.I4Intervention, objTVL050.BU, objTVL050.Location, true).CategoryDescription;
                objTVL050.BU = db.COM002.Where(x => x.t_dimx == objTVL050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.Location = db.COM002.Where(x => x.t_dimx == objTVL050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objTVL050.OfferedBy = objTVL050.OfferedBy != null ? Manager.GetPsidandDescription(objTVL050.OfferedBy) : string.Empty;
                objTVL050.OfferedtoCusomerBy = objTVL050.OfferedtoCusomerBy != null ? Manager.GetPsidandDescription(objTVL050.OfferedtoCusomerBy) : string.Empty;
                objTVL050.InspectedBy = objTVL050.InspectedBy != null ? Manager.GetPsidandDescription(objTVL050.InspectedBy) : string.Empty;
                var qcremark = db.TVL050.Where(x => x.RequestNo == objTVL050.RequestNo && (x.TestResult != "" && x.TestResult != null))
                                              .OrderByDescending(x => x.RequestId)
                                              .Select(x => x.QCRemark)
                                              .FirstOrDefault();
                ViewBag.ProjectNo = objTVL050.ProjectNo + "-" + db.TVL001.Where(x => x.ProjectNo == objTVL050.ProjectNo).Select(x => x.ProjectDescription).FirstOrDefault(); ;
                if (qcremark != null)
                {
                    objTVL050.QCRemark = qcremark;
                }
            }
            return View(objTVL050);
        }

        [HttpPost]
        public ActionResult GetOfferPartial(int headerId)
        {
            TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == headerId).FirstOrDefault();
            TVL050 objTVL050 = new TVL050();
            objTVL050.RequestId = 0;
            objTVL050.HeaderId = objTVL040.HeaderId;
            ViewBag.headerid = headerId;
            return PartialView("_GetRequestOfferPartial", objTVL050);
        }

        //Offer data to next level


        [HttpPost]
        public ActionResult RequestClear(int? headerid, string usedrefrevno, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objTVL040 != null)
                {
                    objTVL040.ShopUsedRefDocRevNo = usedrefrevno;
                    if (!string.IsNullOrWhiteSpace(remark))
                    {
                        objTVL040.ShopRemark = remark;
                    }
                    objTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
                    objTVL040.LnTInspectorResult = clsImplementationEnum.TravelerInspectionStatus.Cleared.GetStringValue();
                    objTVL040.LnTInspectorResultOn = DateTime.Now;
                    objTVL040.EditedBy = objClsLoginInfo.UserName;
                    objTVL040.EditedOn = DateTime.Now;
                    db.SaveChanges();


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Cleared Successfully";
                    objResponseMsg.Status = objTVL040.InspectionStatus;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult OperationApplicable(int? headerid, string remark, bool IsHeaderLine)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objTVL040 != null)
                {
                    objTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Not_Applicatble.GetStringValue();
                    objTVL040.LnTInspectorResult = clsImplementationEnum.TravelerInspectionStatus.Not_Applicatble.GetStringValue();
                    objTVL040.LnTInspectorResultOn = DateTime.Now;
                    objTVL040.NotApplicableBy = objClsLoginInfo.UserName;
                    objTVL040.NotApplicableOn = DateTime.Now;
                    objTVL040.NotApplicableRemark = remark;
                    objTVL040.EditedBy = objClsLoginInfo.UserName;
                    objTVL040.EditedOn = DateTime.Now;
                    objTVL040.IsHeaderLine = IsHeaderLine;
                    db.SaveChanges();

                    #region Send Notification
                    //(new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objTVL040.Project, objTVL040.BU, objTVL040.Location, "Stage: " + objTVL040.StageCode + " of Part: " + objTVL040.PartNo + " & Project No: " + objTVL040.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ViewOfferDetails?RequestId=" + objTVL050.RequestId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Operation has been updated successfully";
                    objResponseMsg.Status = objTVL040.InspectionStatus;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult SaveRemark(int headerid, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objTVL040 != null)
                {
                    objTVL040.ShopRemark = remark;
                    objTVL040.EditedBy = objClsLoginInfo.UserName;
                    objTVL040.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult SaveShopRemark(int requestid, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == requestid).FirstOrDefault();
                if (objTVL050 != null)
                {
                    objTVL050.TVL040.ShopRemark = remark;
                    objTVL050.ShopRemark = remark;
                    objTVL050.EditedBy = objClsLoginInfo.UserName;
                    objTVL050.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }
        [HttpPost]
        public ActionResult RequestOffered(string OfferLocation, string remark, string ShopUsedRefDocRevNo, int? requestId, bool? isReoffer)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == requestId).FirstOrDefault();
                if (isReoffer != null && !Convert.ToBoolean(isReoffer))
                {
                    if (objTVL040 != null)
                    {
                        if (!string.IsNullOrWhiteSpace(remark))
                        {
                            objTVL040.ShopRemark = remark;
                        }
                        objTVL040.ShopUsedRefDocRevNo = ShopUsedRefDocRevNo;
                        #region Offer Entry
                        long RequestNo = 0;
                        int ReqSeqNo = 1;

                        ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                        var SPResult = db.SP_TVL_GET_NEXT_REQUEST_NO(objTVL040.BU, objTVL040.Location, outParm);
                        RequestNo = (long)outParm.Value;

                        TVL050 objTVL050 = db.TVL050.Add(new TVL050
                        {
                            HeaderId = objTVL040.HeaderId,
                            ProjectNo = objTVL040.ProjectNo,
                            Customer = objTVL040.Customer,
                            RequestNo = RequestNo,
                            ReqSequenceNo = ReqSeqNo,
                            ReqIterationNo = 1,
                            TravelerNo = objTVL040.TravelerNo,
                            TravelerRevNo = objTVL040.TravelerRevNo,
                            BU = objTVL040.BU,
                            Location = objTVL040.Location,
                            Identification = objTVL040.Identification,
                            AssemblyNo = objTVL040.AssemblyNo,
                            OperationNo = objTVL040.OperationNo,
                            OperationRevisionNo = objTVL040.OperationRevisionNo,
                            Activity = objTVL040.Activity,
                            RefDocument = objTVL040.RefDocument,
                            RefDocRevNo = objTVL040.RefDocRevNo,
                            LTIntervention = objTVL040.LTIntervention,
                            I1Intervention = objTVL040.I1Intervention,
                            I2Intervention = objTVL040.I2Intervention,
                            I3Intervention = objTVL040.I3Intervention,
                            I4Intervention = objTVL040.I4Intervention,
                            OfferLocation = OfferLocation,
                            ShopRemark = objTVL040.ShopRemark,
                            ShopUsedRefDocRevNo = objTVL040.ShopUsedRefDocRevNo,
                            InspectionRecordRequired = objTVL040.InspectionRecordRequired,
                            SubAssemblyNo = objTVL040.SubAssemblyNo,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            OfferedBy = objClsLoginInfo.UserName,
                            OfferedOn = DateTime.Now
                        });

                        objTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.UnderInspection.GetStringValue();
                        objTVL040.EditedBy = objClsLoginInfo.UserName;
                        objTVL040.EditedOn = DateTime.Now;
                        db.SaveChanges();


                        #region Send Notification
                        (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objTVL040.BU, objTVL040.Location,
                            "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " & Project No: " + objTVL050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objTVL050.RequestId);
                        #endregion

                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data Offered Successfully";
                        objResponseMsg.Status = objTVL040.InspectionStatus;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Data not available.";
                    }
                }
                else
                {
                    #region Re offer request
                    TVL050 objTVL050 = db.TVL050.Where(x => x.RequestId == requestId).FirstOrDefault();

                    if (!string.IsNullOrWhiteSpace(remark))
                    {
                        objTVL050.TVL040.ShopRemark = remark;
                        objTVL050.ShopRemark = remark;
                    }
                    objTVL050.ShopUsedRefDocRevNo = ShopUsedRefDocRevNo;
                    objTVL050.TVL040.ShopUsedRefDocRevNo = ShopUsedRefDocRevNo;
                    objTVL050.QCRemark = null;
                    objTVL050.OfferedBy = objClsLoginInfo.UserName;
                    objTVL050.OfferedOn = DateTime.Now;
                    db.SaveChanges();
                    #region Send Notification
                    (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objTVL050.BU, objTVL050.Location,
                        "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " & Project No: " + objTVL050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objTVL050.RequestId);
                    #endregion
                    #endregion
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Re-Offered Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        #endregion

        #region Initiate Data
        [HttpPost]
        public ActionResult Initiatetraveler(int headerId, int SFrom, int STo, string IdentificationNo, string IdentificationStatus)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string releaseStatus = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
                TVL010_Log objtvl010 = db.TVL010_Log.Where(m => m.HeaderId == headerId && m.Status == releaseStatus).FirstOrDefault();
                List<TVL011_Log> objtvl011 = db.TVL011_Log.Where(m => m.RefId == objtvl010.Id && m.LineStatus == releaseStatus).ToList();
                int count = STo - SFrom + 1;
                int totCount = 0;
                List<string> lstExistAssembly = new List<string>();
                for (int i = SFrom; i <= STo; i++)
                {
                    TVL040 objtv40 = db.TVL040.Where(m => m.SubAssemblyNo == i && m.AssemblyNo == objtvl010.IdentificationNo && m.ProjectNo == objtvl010.ProjectNo && m.TravelerNo == objtvl010.TravelerNo).FirstOrDefault();
                    if (objtv40 == null)
                    {
                        foreach (var tvl11 in objtvl011)
                        {
                            TVL040 objtvl040 = new TVL040();
                            objtvl040.ProjectNo = objtvl010.ProjectNo;
                            objtvl040.Customer = objtvl010.Customer;
                            objtvl040.TravelerRevNo = objtvl010.TravelerRevNo;
                            objtvl040.TravelerNo = objtvl010.TravelerNo;
                            if (string.IsNullOrEmpty(IdentificationNo))
                            {
                                objtvl040.AssemblyNo = objtvl010.IdentificationNo;
                            }
                            else
                            {
                                objtvl040.AssemblyNo = IdentificationNo;
                            }
                            if (string.IsNullOrEmpty(IdentificationStatus))
                            {
                                objtvl040.Identification = objtvl010.Identification;
                            }
                            else
                            {
                                objtvl040.Identification = IdentificationStatus;
                            }
                            objtvl040.BU = objtvl010.BU;
                            objtvl040.Location = objtvl010.Location;
                            objtvl040.OperationNo = (decimal)tvl11.OperationNo;
                            objtvl040.OperationRevisionNo = tvl11.OperationRevisionNo;
                            objtvl040.Activity = tvl11.Activity;
                            objtvl040.RefDocument = tvl11.RefDocument;
                            objtvl040.RefDocRevNo = tvl11.RefDocRevNo;
                            objtvl040.LTIntervention = tvl11.LTIntervention;
                            objtvl040.I1Intervention = tvl11.I1Intervention;
                            objtvl040.I2Intervention = tvl11.I2Intervention;
                            objtvl040.I3Intervention = tvl11.I3Intervention;
                            objtvl040.I4Intervention = tvl11.I4Intervention;
                            objtvl040.InspectionRecordRequired = tvl11.InspectionRecordRequired;
                            if (objtvl040.LTIntervention == "S")
                            {
                                objtvl040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue();
                            }
                            else
                            { objtvl040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue(); }
                            objtvl040.LnTInspectorResult = null;
                            objtvl040.LnTInspectorResultOn = null;
                            objtvl040.CreatedBy = objClsLoginInfo.UserName;
                            objtvl040.CreatedOn = DateTime.Now;
                            objtvl040.itenrationNo = 1;
                            objtvl040.SubAssemblyNo = i;
                            db.TVL040.Add(objtvl040);
                            db.SaveChanges();
                            #region Send Notification
                            (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PROD3.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.MFG2.GetStringValue()), objtvl010.BU, objtvl010.Location,
                                "Traveler:  Operations of Identification: " + objtvl040.AssemblyNo + " & Identification Sr. No " + objtvl040.SubAssemblyNo + " for Project No: " + objtvl040.ProjectNo + " has been initiated and ready to offer.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/OfferInspection/OfferOperation/" + objtvl040.HeaderId);
                            #endregion
                        }
                        totCount = totCount + 1;
                    }
                    else
                    { lstExistAssembly.Add(i.ToString()); }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = totCount + " Traveler initiated successfully";

                if (lstExistAssembly.Any())
                {
                    objResponseMsg.dataKey = true;
                    objResponseMsg.dataValue = string.Format("The Identification Serial No. {0} already initiated with {1},{2},{3} Traveler", string.Join(",", lstExistAssembly), objtvl010.ProjectNo, objtvl010.IdentificationNo, objtvl010.TravelerNo);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region partial view and methods for grid actions 

        [HttpPost]
        public ActionResult LoadOfferFilteredDataPartial(FilterReqTravelerModel filterparamodel)
        {
            return PartialView("~/Areas/TVL/Views/Shared/_LoadFilteredDataPartial.cshtml", filterparamodel);
        }
        [HttpPost]
        public ActionResult LoadRequestFilteredDataPartial(FilterReqTravelerModel requestfilterparamodel)
        {
            TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == requestfilterparamodel.ProjectNo).FirstOrDefault();
            if (objtvl001 != null)
            {

                if (objtvl001.I1Intervention != null)
                {
                    ViewBag.I1Intervention = objtvl001.I1Intervention;
                }
                else
                {
                    ViewBag.I1Intervention = "I1Intervention";
                }
                if (objtvl001.I2Intervention != null)
                {
                    ViewBag.I2Intervention = objtvl001.I2Intervention;
                }
                else
                {
                    ViewBag.I2Intervention = "I2Intervention";
                }
                if (objtvl001.I3Intervention != null)
                {
                    ViewBag.I3Intervention = objtvl001.I3Intervention;
                }
                else
                {
                    ViewBag.I3Intervention = "I3Intervention";
                }
                if (objtvl001.I4Intervention != null)
                {
                    ViewBag.I4Intervention = objtvl001.I4Intervention;
                }
                else
                {
                    ViewBag.I4Intervention = "I4Intervention";
                }

            }

            return PartialView("~/Areas/TVL/Views/Shared/_LoadRequestFilteredDataPartial.cshtml", requestfilterparamodel);
        }

        [HttpPost]
        public JsonResult LoadFilteredOfferData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += " 1=1 and TravelerNo='" + param.TravelerNo + "' and AssemblyNo='" + param.IdentifictionNo + "' and  SubAssemblyNo=" + param.subassembly + " ";

                //search Condition 
                string[] columnName = { "OperationNo", "OperationRevisionNo", "Activity", "RefDocument", "RefDocRevNo", "itenrationNo", "InspectionStatus" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                           (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                           (!string.IsNullOrEmpty(uc.RefDocRevNo)) ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToString(uc.itenrationNo),
                            Convert.ToString(uc.InspectionStatus),
                            Convert.ToString(uc.LnTInspectorResult),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadRequestFilteredOfferData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += " 1=1 and HeaderId=" + Convert.ToInt32(param.Headerid);
                strWhereCondition += " and TravelerNo='" + param.TravelerNo + "' and AssemblyNo='" + param.IdentifictionNo + "' and  SubAssemblyNo=" + param.subassembly + " ";
                //search Condition 
                string[] columnName = { "RequestNo","ReqSequenceNo","ReqIterationNo","OperationNo","OperationRevisionNo",
                                        "Activity","RefDocument","RefDocRevNo","TestResult","OfferedBy","OfferedOn",
                                        "InspectedBy","InspectedOn","QCRemark","OfferedtoCusomerBy",  "OfferedtoCusomerOn",
                                        "I1Intervention","I1Result","I1ResultOn","I2Intervention",
                                        "I2Result","I2ResultOn","I3Intervention","I3Result","I3ResultOn",
                                        "I4Intervention","I4Result","I4ResultOn"
                                        };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_TVL_GET_INSPECTIONDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.RequestId),
                                Convert.ToString(uc.RequestNo),
                                Convert.ToString(uc.ReqSequenceNo),
                                Convert.ToString(uc.ReqIterationNo),
                                uc.OperationNo.Value.ToString("G29"),
                                Convert.ToString("R"+uc.OperationRevisionNo),
                                Convert.ToString(uc.Activity),
                                (!string.IsNullOrEmpty(uc.RefDocument)) ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                                (!string.IsNullOrEmpty(uc.RefDocRevNo)) ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                                Convert.ToString(uc.TestResult),
                                Convert.ToString(uc.OfferedBy),
                                Convert.ToString(uc.OfferedOn),
                                Convert.ToString(uc.InspectedBy),
                                Convert.ToString(uc.InspectedOn),
                                Convert.ToString(uc.QCRemark),
                                Convert.ToString(uc.OfferedtoCusomerBy),
                                Convert.ToString(uc.OfferedtoCusomerOn),
                                 Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),
                                Convert.ToString(uc.I1Intervention),
                                Convert.ToString(uc.I1Result),
                                Convert.ToString(uc.I1ResultOn),
                                Convert.ToString(uc.I2Intervention),
                                Convert.ToString(uc.I2Result),
                                Convert.ToString(uc.I2ResultOn),
                                Convert.ToString(uc.I3Intervention),
                                Convert.ToString(uc.I3Result),
                                Convert.ToString(uc.I3ResultOn),
                                Convert.ToString(uc.I4Intervention),
                                Convert.ToString(uc.I4Result),
                                Convert.ToString(uc.I4ResultOn),
                              }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Common Functions
        public ActionResult ShowTimelineOfferInspectionPartial(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            TVL040 objTVL040 = db.TVL040.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            TVL050 objTVL050 = db.TVL050.Where(x => x.HeaderId == HeaderId).OrderByDescending(x => x.RequestId).FirstOrDefault();

            model.TimelineTitle = "Traveler Offer Operation History";
            model.Title = "Traveler Offer Operation";
            if (objTVL040 != null)
            {
                model.CreatedBy = Manager.GetUserNameFromPsNo(objTVL040.CreatedBy);
                model.CreatedOn = objTVL040.CreatedOn;
            }
            if (objTVL050 != null)
            {
                string ret = clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue();
                string rej = clsImplementationEnum.TravelerInspectionStatus.Rejected.GetStringValue();
                if (objTVL040.LnTInspectorResult == ret || objTVL040.LnTInspectorResult == rej)
                {
                    TVL050 objpretvl050 = db.TVL050.Where(m => m.HeaderId == objTVL040.HeaderId && m.RequestId != objTVL050.RequestId).OrderByDescending(c => c.RequestId).FirstOrDefault();
                    if (objpretvl050 != null)
                    {
                        model.OfferedBy = Manager.GetUserNameFromPsNo(objpretvl050.OfferedBy);
                        model.OfferedOn = objpretvl050.OfferedOn;
                        model.InspectBy = Manager.GetUserNameFromPsNo(objpretvl050.InspectedBy);
                        if (objTVL040.LnTInspectorResult == ret) { model.ReturnedOn = objpretvl050.InspectedOn; }
                        if (objTVL040.LnTInspectorResult == rej) { model.RespDeptOn = objpretvl050.InspectedOn; }
                        model.Status = objpretvl050.TestResult;
                    }
                }
                else
                {
                    model.OfferedBy = Manager.GetUserNameFromPsNo(objTVL050.OfferedBy);
                    model.OfferedOn = objTVL050.OfferedOn;
                    model.InspectBy = Manager.GetUserNameFromPsNo(objTVL050.InspectedBy);
                    model.InspectedOn = objTVL050.InspectedOn;
                    model.Status = objTVL050.TestResult;
                }
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_TVL_GET_OFFERDATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.ProjectNo),
                                      TravelerNo = Convert.ToString(uc.TravelerNo),
                                      TravelerRevNo = Convert.ToString(uc.TravelerRevNo),
                                      InspectionStatus = Convert.ToString(uc.InspectionStatus),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.ReOffered.GetStringValue())
                {
                    var lst = db.SP_TVL_GET_INSPECTIONDATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.ProjectNo),
                                      TravelerNo = Convert.ToString(uc.TravelerNo),
                                      TravelerRevNo = Convert.ToString(uc.TravelerRevNo),
                                      TestResult = Convert.ToString(uc.TestResult),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ReleaseTraveler(int? headerid)
        {
            clsHelper.ResponseMsg objResponseMsg1 = new clsHelper.ResponseMsg();
            try
            {

                var objTVL010 = db.TVL010.Where(x => x.HeaderId == headerid).FirstOrDefault();
                var approveStatus = clsImplementationEnum.TravelerLineStatus.Approved.GetStringValue();
                var lstOperations = db.TVL011.Where(x => x.HeaderId == objTVL010.HeaderId).ToList();

                if (lstOperations.Any())
                {
                    //objResponseMsg1 = InsertUpdateOperation(objTVL010.HeaderId, lstOperations);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg1.Key = false;
                objResponseMsg1.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg1);

        }
        public string GetInterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            if (InspectionStatus == "S")
            {
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == Headerid).FirstOrDefault();

                if (objtvl040 != null)
                {
                    if (!string.IsNullOrEmpty(objtvl040.LnTInspectorResult))
                    {
                        InspectionStatus = objtvl040.LTIntervention + "<br>" + objtvl040.LnTInspectorResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl040.EditedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl040.LnTInspectorResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl040.LnTInspectorResultOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = objtvl040.LTIntervention;
                    }
                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            else
            {
                TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
                if (objtvl050 != null)
                {
                    if (!string.IsNullOrEmpty(objtvl050.TestResult))
                    {
                        InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention) + "<br>" + objtvl050.TestResult + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.InspectedBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.InspectedOn).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        InspectionStatus = Manager.TVLIntervention(objtvl050.LTIntervention);
                    }
                    //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");
                }
            }
            return InspectionStatus;
        }

        public string GetI1InterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
            if (objtvl050 != null)
            {
                if (!string.IsNullOrEmpty(objtvl050.I1Result))
                {
                    InspectionStatus = objtvl050.I1Intervention + "<br>" + objtvl050.I1Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I1ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I1ResultOn).ToString("dd/MM/yyyy"));
                }
                else
                {
                    InspectionStatus = objtvl050.I1Intervention;
                }
                //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");

            }

            return InspectionStatus;
        }
        public string GetI2InterventionDetails(string InspectionStatus, int Headerid, int? RequestId)
        {
            TVL050 objtvl050 = db.TVL050.Where(m => m.HeaderId == Headerid && m.RequestId == RequestId).FirstOrDefault();
            if (objtvl050 != null)
            {
                if (!string.IsNullOrEmpty(objtvl050.I2Result))
                {
                    InspectionStatus = objtvl050.I2Intervention + "<br>" + objtvl050.I2Result + "<br>" + Manager.GetUserNameFromPsNo(objtvl050.I2ResultBy) + "<br>" + Convert.ToString(Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(objtvl050.I2ResultOn).ToString("dd/MM/yyyy"));
                }
                else
                {
                    InspectionStatus = objtvl050.I2Intervention;
                }
                //InspectionStatus = Convert.ToString(InspectionStatus).Replace(",", "<br>");

            }

            return InspectionStatus;
        }
        public JsonResult GetRefrevnodetails(int Headerid, string status)
        {
            string Revno = string.Empty;
            string Refdocument = string.Empty;
            StringBuilder builder = new StringBuilder();
            if (status != "Offer")
            {
                TVL050 objtvl050 = db.TVL050.Where(m => m.RequestId == Headerid).FirstOrDefault();
                if (!string.IsNullOrEmpty(objtvl050.RefDocument))
                {
                    var lstPosition = objtvl050.RefDocument.Split(',').ToList();
                    lstPosition = lstPosition.Select(x => x.Trim()).ToList();
                    //var refdocvno = db.TVL002.Where(m => m.ProjectNo == project && lstPosition.Contains(m.RefDocument)).ToList().Max(m=>m.RefDocRevNo);
                    var refdocvno = db.TVL002.Where(m => m.ProjectNo == objtvl050.ProjectNo && lstPosition.Contains(m.RefDocument.Trim())).Select(m => m.RefDocument).Distinct();


                    foreach (var item in refdocvno) // Loop through all strings
                    {
                        var refno = db.TVL002.Where(m => m.ProjectNo == objtvl050.ProjectNo && m.RefDocument.Trim() == item.Trim()).ToList().Max(m => m.RefDocRevNo);
                        builder.Append(refno).Append(","); // Append string to StringBuilder
                    }
                }
                else
                {
                    Refdocument = null;
                }
            }
            else
            {
                TVL040 objtvl040 = db.TVL040.Where(m => m.HeaderId == Headerid).FirstOrDefault();
                if (!string.IsNullOrEmpty(objtvl040.RefDocument))
                {
                    var lstPosition = objtvl040.RefDocument.Split(',').ToList();
                    lstPosition = lstPosition.Select(x => x.Trim()).ToList();
                    //var refdocvno = db.TVL002.Where(m => m.ProjectNo == project && lstPosition.Contains(m.RefDocument)).ToList().Max(m=>m.RefDocRevNo);
                    var refdocvno = db.TVL002.Where(m => m.ProjectNo == objtvl040.ProjectNo && lstPosition.Contains(m.RefDocument.Trim())).Select(m => m.RefDocument).Distinct();


                    foreach (var item in refdocvno) // Loop through all strings
                    {
                        var refno = db.TVL002.Where(m => m.ProjectNo == objtvl040.ProjectNo && m.RefDocument.Trim() == item.Trim()).ToList().Max(m => m.RefDocRevNo);
                        builder.Append(refno).Append(",");
                    }
                }
                else
                {
                    Refdocument = null;
                }


            }
            Revno = (Refdocument != null ? builder.ToString() : null); // Get string from 
            var Refrevno = (!string.IsNullOrWhiteSpace(Revno) ? Revno.Substring(0, Revno.Length - 1) : null);

            var data = new
            {
                Refrevno = Refrevno,

            };



            return Json(data, JsonRequestBehavior.AllowGet);

        }
        #endregion


    }
}