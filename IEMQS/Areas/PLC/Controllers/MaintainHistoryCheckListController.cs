﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLC.Controllers
{
    public class MaintainHistoryCheckListController : clsBase
    {
        // GET: PLC/MaintainHistoryCheckList

        #region fetch history data
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(int Id)
        {
            PCL001_Log objLog = new PCL001_Log();
            objLog = db.PCL001_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_GetHeaderGridDataPartial", objLog);
        }

        //datatable function for load grid
        [HttpPost]
        public JsonResult LoadPCLHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string strWhere = string.Empty;

                strWhere += "1=1 and HeaderId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Document like '%" + param.sSearch + "%' or Customer like '%" + param.sSearch + "%' or (pcl.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or ProcessLicensor like '%" + param.sSearch + "%' or ProductType like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                var lstResult = db.SP_PCL_HEADER_HISTORY_GETDETAILS
                               (
                               StartIndex, EndIndex, strSortOrder, strWhere
                               ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.Document),
                            Convert.ToString(uc.Customer),
                            Convert.ToString(uc.ProcessLicensor),
                            Convert.ToString(uc.ProductType),
                            Convert.ToString(uc.EquipmentNo),
                            //Convert.ToString(uc.ApprovedBy),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                             Convert.ToString(uc.ApprovedBy),
                             uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                             //Convert.ToString(uc.HeaderId)
                           "<center><a href=\""+WebsiteURL+"/PLC/MaintainHistoryCheckList/ViewLogDetails?Id=" + uc.Id + "\"><i class=\"iconspace fa fa-eye\"></i></a> <i class=\"iconspace fa fa-clock-o\" title=\"Show Timeline\"  onclick=ShowTimeline('/PLC/MaintainCheckList/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i></center>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region view detail page
        //view detail page
        [SessionExpireFilter]
        [AllowAnonymous]
        // [UserPermissions]
        //Main View
        public ActionResult ViewLogDetails(int? id)
        {
            //log Id is used to fetch data
            ViewBag.id = id;
            ViewBag.urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);

            return View();
        }

        //Load Partial for header data
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadHeaderData(int Id, string urlPrefix)
        {
            PCL001_Log objPCL001 = new PCL001_Log();
            if (Id > 0)
            {
                objPCL001 = db.PCL001_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objPCL001 != null)
                {
                    objPCL001.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.Project = objPCL001.Project;
                    string ApproverName = db.COM003.Where(x => x.t_psno == objPCL001.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                    if (objPCL001.ApprovedBy != null)
                    {
                        ViewBag.ApproverName = objPCL001.ApprovedBy + " - " + ApproverName;
                    }
                    else { ViewBag.ApproverName = ""; }
                    ViewBag.Action = "Headeredit";
                }
                //var urlPrefix = Request.Url.AbsolutePath.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
                if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
                {
                    ViewBag.RevPrev = (db.PCL001_Log.Any(q => (q.HeaderId == objPCL001.HeaderId && q.RevNo == (objPCL001.RevNo - 1))) ? urlPrefix + db.PCL001_Log.Where(q => (q.HeaderId == objPCL001.HeaderId && q.RevNo == (objPCL001.RevNo - 1))).FirstOrDefault().Id : null);
                    ViewBag.RevNext = (db.PCL001_Log.Any(q => (q.HeaderId == objPCL001.HeaderId && q.RevNo == (objPCL001.RevNo + 1))) ? urlPrefix + db.PCL001_Log.Where(q => (q.HeaderId == objPCL001.HeaderId && q.RevNo == (objPCL001.RevNo + 1))).FirstOrDefault().Id : null);
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == objPCL001.HeaderId && x.DocumentNo == objPCL001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                //else
                //{ return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" }); }
            }
            else
            {
                objPCL001.RevNo = 0;
                objPCL001.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
            }
            ViewBag.Products = clsImplementationEnum.getpclProductType().ToArray();
            return PartialView("_LoadHistoryPLCHeaderData", objPCL001);
        }

        //load check list partial accodring to product type
        [SessionExpireFilter]
        public ActionResult LoadCheckListData(int refId, string ProductType)
        {
            ViewBag.RefID = refId;
            ViewBag.ProductType = ProductType;
            return PartialView("_LoadHistoryCheckListHtmlPartial");
        }
        #endregion

        #region FORGED REACTOR (according to Ref id of History Id)
        //Section 1
        [SessionExpireFilter]
        public ActionResult LoadConstructionData(int refID)
        {
            PCL002_Log objPCL002 = new PCL002_Log();
            objPCL002 = db.PCL002_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;
            if (objPCL002 != null)
            {
                ViewBag.Action = "constructoredit";
            }
            return PartialView("_LoadHistoryConstructionDataPartial", objPCL002);
        }
        //Section 2
        [SessionExpireFilter]
        public ActionResult LoadAllowancesData(int refID)
        {
            PCL003_Log objPCL003 = new PCL003_Log();
            objPCL003 = db.PCL003_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;
            if (objPCL003 != null)
            {
                ViewBag.Action = "allowanceedit";
            }
            return PartialView("_LoadHistoryAllowanceHtmlPartial", objPCL003);
        }
        //Section 3
        [SessionExpireFilter]
        public ActionResult LoadScopeofworkData(int refID)
        {
            PCL004_Log objPCL004 = new PCL004_Log();
            objPCL004 = db.PCL004_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;

            if (objPCL004 != null)
            {
                ViewBag.Action = "scopeedit";
            }

            return PartialView("_LoadHistoryScopeofworkHtmlPartial", objPCL004);
        }
        //Section 4
        [SessionExpireFilter]
        public ActionResult LoadFixturerequirementData(int refID)
        {
            PCL005_Log objPCL005 = new PCL005_Log();
            objPCL005 = db.PCL005_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL005 != null)
            {
                objPCL005 = db.PCL005_Log.Where(i => i.LineId == objPCL005.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "fixtureedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryFixturerequirementHtmlPartial", objPCL005);
        }
        //Section 5
        [SessionExpireFilter]
        public ActionResult LoadDocumentData(int refID)
        {
            PCL006_Log objPCL006 = new PCL006_Log();
            objPCL006 = db.PCL006_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL006 != null)
            {
                objPCL006 = db.PCL006_Log.Where(i => i.LineId == objPCL006.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "fixtureedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryDocumentHtmlPartial", objPCL006);
        }
        //Section 6
        [SessionExpireFilter]
        public ActionResult LoadDimData(int refID)
        {
            PCL007_Log objPCL007 = new PCL007_Log();
            objPCL007 = db.PCL007_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL007 != null)
            {
                objPCL007 = db.PCL007_Log.Where(i => i.LineId == objPCL007.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "dimedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryDimHtmlPartial", objPCL007);
        }
        #endregion

        #region Urea Rx Check List
        [SessionExpireFilter]
        public ActionResult LoadUreaConstructionData(int refID)
        {
            PCL008_Log objPCL008 = new PCL008_Log();
            objPCL008 = db.PCL008_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL008 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryUreaConstructionData", objPCL008);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaAllowancesData(int refID)
        {
            PCL009_Log objPCL009 = new PCL009_Log();
            objPCL009 = db.PCL009_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL009 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryUreaAllowancesData", objPCL009);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaScopeofworkData(int refID)
        {
            PCL010_Log objPCL010 = new PCL010_Log();
            objPCL010 = db.PCL010_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL010 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryUreaScopeofworkData", objPCL010);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaFixturerequirementData(int refID)
        {
            PCL011_Log objPCL011 = new PCL011_Log();
            objPCL011 = db.PCL011_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL011 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryUreaFixturerequirementData", objPCL011);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaDocumentData(int refID)
        {
            PCL012_Log objPCL012 = new PCL012_Log();
            objPCL012 = db.PCL012_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL012 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryUreaDocumentData", objPCL012);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaDimData(int refID)
        {
            PCL013_Log objPCL013 = new PCL013_Log();
            objPCL013 = db.PCL013_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL013 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryUreaDimData", objPCL013);
        }
        #endregion

        #region HSC Check points' category (accordind to History id)
        [SessionExpireFilter]
        public ActionResult LoadHSCCategoryData(int refID)
        {
            PCL017_Log objPCL017 = new PCL017_Log();
            objPCL017 = db.PCL017_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL017 != null)
            {
                objPCL017 = db.PCL017_Log.Where(i => i.LineId == objPCL017.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "docedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }

            return PartialView("_LoadHistoryHSCCategoryData", objPCL017);
        }
        #endregion

        #region   Coke Drum  
        [SessionExpireFilter]
        public ActionResult LoadCDConstructionData(int refID)
        {
            PCL018_Log objPCL018 = new PCL018_Log();
            objPCL018 = db.PCL018_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL018 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "constructedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryCDConstructionData", objPCL018);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDAllowancesData(int refID)
        {
            PCL019_Log objPCL019 = new PCL019_Log();
            objPCL019 = db.PCL019_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL019 != null)
            {
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryCDAllowancesData", objPCL019);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDScopeofworkData(int refID)
        {
            PCL020_Log objPCL020 = new PCL020_Log();
            objPCL020 = db.PCL020_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL020 != null)
            {
                objPCL020 = db.PCL020_Log.Where(i => i.LineId == objPCL020.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryCDScopeofworkData", objPCL020);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDFixturerequirementData(int refID)
        {
            PCL023_Log objPCL023 = new PCL023_Log();
            objPCL023 = db.PCL023_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL023 != null)
            {
                objPCL023 = db.PCL023_Log.Where(i => i.LineId == objPCL023.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryCDFixturerequirementData", objPCL023);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDDocumentData(int refID)
        {
            PCL021_Log objPCL021 = new PCL021_Log();
            objPCL021 = db.PCL021_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL021 != null)
            {
                objPCL021 = db.PCL021_Log.Where(i => i.LineId == objPCL021.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryCDDocumentData", objPCL021);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDDimData(int refID)
        {
            PCL022_Log objPCL022 = new PCL022_Log();
            objPCL022 = db.PCL022_Log.Where(i => i.HeaderId == refID).FirstOrDefault();
            if (objPCL022 != null)
            {
                objPCL022 = db.PCL022_Log.Where(i => i.LineId == objPCL022.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryCDDimData", objPCL022);
        }
        #endregion

        #region  Column 
        [SessionExpireFilter]
        public ActionResult LoadColumnConstructionData(int refID)
        {
            PCL024_Log objPCL024 = new PCL024_Log();
            objPCL024 = db.PCL024_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL024 != null)
            {
                objPCL024 = db.PCL024_Log.Where(i => i.LineId == objPCL024.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryColumnConstructionData", objPCL024);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnAllowancesData(int refID)
        {
            PCL025_Log objPCL025 = new PCL025_Log();
            objPCL025 = db.PCL025_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL025 != null)
            {
                objPCL025 = db.PCL025_Log.Where(i => i.LineId == objPCL025.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryColumnAllowancesData", objPCL025);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnScopeofworkData(int refID)
        {
            PCL026_Log objPCL026 = new PCL026_Log();
            objPCL026 = db.PCL026_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL026 != null)
            {
                objPCL026 = db.PCL026_Log.Where(i => i.LineId == objPCL026.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryColumnScopeofworkData", objPCL026);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnFixturerequirementData(int refID)
        {
            PCL029_Log objPCL029 = new PCL029_Log();
            objPCL029 = db.PCL029_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL029 != null)
            {
                objPCL029 = db.PCL029_Log.Where(i => i.LineId == objPCL029.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryColumnFixturerequirementData", objPCL029);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnDocumentData(int refID)
        {
            PCL027_Log objPCL027 = new PCL027_Log();
            objPCL027 = db.PCL027_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL027 != null)
            {
                objPCL027 = db.PCL027_Log.Where(i => i.LineId == objPCL027.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryColumnDocumentData", objPCL027);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnDimData(int refID)
        {
            PCL028_Log objPCL028 = new PCL028_Log();
            objPCL028 = db.PCL028_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL028 != null)
            {
                objPCL028 = db.PCL028_Log.Where(i => i.LineId == objPCL028.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryColumnDimData", objPCL028);
        }
        #endregion

        #region  EO Reactor 
        [SessionExpireFilter]
        public ActionResult LoadEOConstructionData(int refID)
        {
            PCL030_Log objPCL030 = new PCL030_Log();
            objPCL030 = db.PCL030_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL030 != null)
            {
                objPCL030 = db.PCL030_Log.Where(i => i.LineId == objPCL030.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryEOConstructionData", objPCL030);
        }
        [SessionExpireFilter]
        public ActionResult LoadEOAllowancesData(int refID)
        {
            PCL031_Log objPCL031 = new PCL031_Log();
            objPCL031 = db.PCL031_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL031 != null)
            {
                objPCL031 = db.PCL031_Log.Where(i => i.LineId == objPCL031.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryEOAllowancesData", objPCL031);
        }
        [SessionExpireFilter]
        public ActionResult LoadEOScopeofworkData(int refID)
        {
            PCL032_Log objPCL032 = new PCL032_Log();
            objPCL032 = db.PCL032_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL032 != null)
            {
                objPCL032 = db.PCL032_Log.Where(i => i.LineId == objPCL032.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryEOScopeofworkData", objPCL032);
        }
        [SessionExpireFilter]
        public ActionResult LoadEOFixturerequirementData(int refID)
        {
            PCL033_Log objPCL033 = new PCL033_Log();
            objPCL033 = db.PCL033_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL033 != null)
            {
                objPCL033 = db.PCL033_Log.Where(i => i.LineId == objPCL033.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryEOFixturerequirementData", objPCL033);
        }
        [SessionExpireFilter]
        public ActionResult LoadEODocumentData(int refID)
        {
            PCL034_Log objPCL034 = new PCL034_Log();
            objPCL034 = db.PCL034_Log.Where(i => i.HeaderId == refID).FirstOrDefault();
            if (objPCL034 != null)
            {
                objPCL034 = db.PCL034_Log.Where(i => i.LineId == objPCL034.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryEODocumentData", objPCL034);
        }
        [SessionExpireFilter]
        public ActionResult LoadEODimensionalData(int refID)
        {
            PCL035_Log objPCL035 = new PCL035_Log();
            objPCL035 = db.PCL035_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL035 != null)
            {
                objPCL035 = db.PCL035_Log.Where(i => i.LineId == objPCL035.LineId).FirstOrDefault();
                ViewBag.HeaderId = refID;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = refID;
            }
            return PartialView("_LoadHistoryEODimensionalData", objPCL035);
        }
        #endregion

        #region  RR Reactor 
        [SessionExpireFilter]
        public ActionResult LoadRRConstructionData(int refID)
        {
            PCL036_Log objPCL036 = new PCL036_Log();
            objPCL036 = db.PCL036_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;
            if (objPCL036 != null)
            {
                ViewBag.Action = "ucedit";
            }

            return PartialView("_LoadHistoryRRConstructionData", objPCL036);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRAllowancesData(int refID)
        {
            PCL037_Log objPCL037 = new PCL037_Log();
            objPCL037 = db.PCL037_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;
            if (objPCL037 != null)
            {
                ViewBag.Action = "ucedit";
            }

            return PartialView("_LoadHistoryRRAllowancesData", objPCL037);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRScopeofworkData(int refID)
        {
            PCL038_Log objPCL038 = new PCL038_Log();
            objPCL038 = db.PCL038_Log.Where(i => i.RefId == refID).FirstOrDefault(); ViewBag.HeaderId = refID;
            if (objPCL038 != null)
            {
                ViewBag.Action = "ucedit";
            }

            return PartialView("_LoadHistoryRRcopeofworkData", objPCL038);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRFixturerequirementData(int refID)
        {
            PCL039_Log objPCL039 = new PCL039_Log();
            objPCL039 = db.PCL039_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;
            if (objPCL039 != null)
            {
                ViewBag.Action = "ucedit";
            }
            return PartialView("_LoadHistoryRRFixturerequirementData", objPCL039);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRDocumentData(int refID)
        {
            PCL040_Log objPCL040 = new PCL040_Log();
            objPCL040 = db.PCL040_Log.Where(i => i.RefId == refID).FirstOrDefault();
            if (objPCL040 != null)
            {
                ViewBag.Action = "ucedit";
            }

            ViewBag.HeaderId = refID;
            return PartialView("_LoadHistoryRRDocumentData", objPCL040);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRDimData(int refID)
        {
            PCL041_Log objPCL041 = new PCL041_Log();
            objPCL041 = db.PCL041_Log.Where(i => i.RefId == refID).FirstOrDefault();
            ViewBag.HeaderId = refID;
            if (objPCL041 != null)
            {
                ViewBag.Action = "ucedit";
            }

            return PartialView("_LoadHistoryRRDimData", objPCL041);
        }
        #endregion

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
            public int LineID;
            public string Status;
            public int? Revno;
        }



        [SessionExpireFilter]
        public ActionResult PrintRolledForgedFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();
            return View(objPCL001);
        }

        [SessionExpireFilter]
        public ActionResult PrintHSCFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();

            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();

            return View(objPCL001);
        }
        [SessionExpireFilter]
        public ActionResult PrintEOFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();

            return View(objPCL001);
        }
        [SessionExpireFilter]
        public ActionResult PrintCDFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();

            return View(objPCL001);
        }
        [SessionExpireFilter]
        public ActionResult PrintRRFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();

            return View(objPCL001);
        }
        [SessionExpireFilter]
        public ActionResult PrintUreaFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();

            return View(objPCL001);
        }
        [SessionExpireFilter]
        public ActionResult PrintColumnFormat(int? headerId)
        {
            PCL001 objPCL001 = db.PCL001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            objPCL001.CreatedBy = Manager.GetUserNameFromPsNo(objPCL001.CreatedBy);
            objPCL001.ApprovedBy = Manager.GetUserNameFromPsNo(objPCL001.ApprovedBy);
            ViewBag.RevNo = objPCL001.RevNo;
            ViewBag.CreatedBy = objPCL001.CreatedBy;
            ViewBag.CreatedOn = objPCL001.CreatedOn;
            ViewBag.ApprovedBy = objPCL001.ApprovedBy;
            ViewBag.Document = objPCL001.Document;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_dsca).FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPCL001.Customer + "'").FirstOrDefault();

            return View(objPCL001);
        }

        #region Export Excel
        // Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_PCL_HEADER_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      ProductType = uc.ProductType,
                                      EquipmentNo = uc.EquipmentNo,
                                      //Convert.ToString(uc.ApprovedBy),
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}

