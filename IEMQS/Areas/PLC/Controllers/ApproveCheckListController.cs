﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLC.Controllers
{
    public class ApproveCheckListController : clsBase
    {
        // GET: PLC/ApproveCheckList
        #region Index page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetApproverHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetApproverHeaderGridDataPartial");
        }
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        [HttpPost]
        public JsonResult LoadPCLHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and ApprovedBy=" + user + " and status in('" + clsImplementationEnum.PLCStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += "1=1";
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                //strWhere += " and ApprovedBy='" + objClsLoginInfo.UserName + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "Customer", "Document", "RevNo", "[Status]", "pcl.Project +' - '+ com1.t_dsca", "pcl.CreatedBy +' - '+com003.t_name", "pcl.EditedBy +' - '+com003.t_name", "pcl.ApprovedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_PCL_HEADER_GETDETAILS
                               (
                                  StartIndex, EndIndex, strSortOrder, strWhere
                               ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.Document),
                            Convert.ToString(uc.Customer),
                            Convert.ToString(uc.ProcessLicensor),
                            Convert.ToString(uc.ProductType),
                            Convert.ToString(uc.EquipmentNo),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                             uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                             Convert.ToString(uc.ApprovedBy),
                             uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                             "<nobr><center>"+
                             "<a class=\"iconspace\" href=\""+WebsiteURL+"/PLC/ApproveCheckList/ViewHeader?HeaderId=" + uc.HeaderId + "\"><i class=\"fa fa-eye\"></i></a>"+
                             //"<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId+ ",'" + uc.ProductType + "')\"></i> "+
                             "<i class='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLC/MaintainCheckList/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            (uc.RevNo > 0 ?  "<i title=\"History\" class=\"iconspace fa fa-history\" onClick=\"GetHistoryDetails(" + uc.HeaderId+ ")\"></i>" : "<i title=\"History\" class=\"disabledicon fa fa-history\" ></i>" ) +
                               "<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId+ ",'" + uc.ProductType + "')\"></i>"+
                              "</center></nobr>",    //Convert.ToString(uc.HeaderId)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Export Excel
        // Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PCL_HEADER_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      ProductType = uc.ProductType,
                                      EquipmentNo = uc.EquipmentNo,
                                      //Convert.ToString(uc.ApprovedBy),
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Detail Page for Header
        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult ViewHeader(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult ViewHeaderData(int HeaderID)
        {
            
            PCL001 objPCL001 = new PCL001();
            List<string> lstProdunctType = clsImplementationEnum.getpclProductType().ToList();
            ViewBag.ProductType = lstProdunctType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objPCL001 = db.PCL001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                if (objPCL001 != null)
                {
                    if (objPCL001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else {
                        ViewBag.IsDisplayMode = false;
                    }
                    objPCL001.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.Project = objPCL001.Project;
                    ViewBag.Action = "edit";

                    var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPCL001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                    ViewBag.PlanningDinID = PlanningDinID;
                    ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue());
                }
            }

            return PartialView("_LoadViewHeaderData", objPCL001);
        }

        //not in use
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadHeaderData(int HeaderID, string Status)
        {
            PCL001 objPCL001 = new PCL001();
            List<string> lstProdunctType = clsImplementationEnum.getpclProductType().ToList();
            ViewBag.ProductType = lstProdunctType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objPCL001 = db.PCL001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                objPCL001.Project = db.COM001.Where(i => i.t_cprj == objPCL001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Project = objPCL001.Project;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPCL001.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.ApproverName = objPCL001.ApprovedBy + " - " + ApproverName;
                ViewBag.Action = "Headeredit";
            }
            else
            {
                objPCL001.RevNo = 0;
                objPCL001.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
            }
            return PartialView("_LoadPLCHeaderData", objPCL001);
        }

        [HttpPost]
        public ActionResult getCustomer(string projectcode)
        {
            ResponceMsgProjectCust objResponseMsg = new ResponceMsgProjectCust();
            PCL001 objPCL001 = new PCL001();

            string Project = projectcode.Split('-')[0];
            objResponseMsg.Customer = Manager.GetCustomerCodeAndNameByProject(Project);
            objResponseMsg.Project = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPCL001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        public class ResponceMsgProjectCust : clsHelper.ResponseMsg
        {
            public string Project;
            public string Customer;
        }

        [SessionExpireFilter]
        public ActionResult LoadCheckListData(int HeaderID, string ProductType, string Status)
        {
            ViewBag.HeaderID = HeaderID;
            ViewBag.ProductType = ProductType;
            ViewBag.Status = Status;
            return PartialView("_LoadApproveCheckListHtmlPartial");
        }


        #endregion
        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Planning_Checklist, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        #region  Approval prpocess
        [HttpPost]
        public ActionResult ApproveHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_PCL_APPROVE(headerId, objClsLoginInfo.UserName);
                PCL001 objPLC001 = db.PCL001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                int newId = db.PCL001_Log.Where(q => q.HeaderId == headerId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                Manager.UpdatePDN002(objPLC001.HeaderId, objPLC001.Status, objPLC001.RevNo, objPLC001.Project, objPLC001.Document, newId);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();


                #region Send Notification
                string rolePLNG3 = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                (new clsManager()).SendNotification(rolePLNG3,
                                                    objPLC001.Project,
                                                    "",
                                                    "",
                                                    Manager.GetPDINDocumentNotificationMsg(objPLC001.Project, clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(), objPLC001.RevNo.Value.ToString(), objPLC001.Status),
                                                    clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                    Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(), objPLC001.HeaderId.ToString(), false),
                                                    objPLC001.CreatedBy);
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_PCL_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    PCL001 objPLC001 = db.PCL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.PCL001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLC001.HeaderId, objPLC001.Status, objPLC001.RevNo, objPLC001.Project, objPLC001.Document, newId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();

                    #region Send Notification
                    string rolePLNG3 = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    (new clsManager()).SendNotification(rolePLNG3,
                                                        objPLC001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLC001.Project, clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(), objPLC001.RevNo.Value.ToString(), objPLC001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(), objPLC001.HeaderId.ToString(), false),
                                                        objPLC001.CreatedBy);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnHeader(FormCollection fc, PCL001 pcl001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string returnStatus = clsImplementationEnum.PLCStatus.Returned.GetStringValue();
            try
            {
                PCL001 objpcl001 = db.PCL001.Where(u => u.HeaderId == pcl001.HeaderId).SingleOrDefault();
                if (objpcl001 != null)
                {
                    objpcl001.Status = returnStatus;
                    objpcl001.ReturnRemark = pcl001.ReturnRemark;
                    objpcl001.ApprovedBy = objClsLoginInfo.UserName;
                    objpcl001.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Returned.ToString();
                    Manager.UpdatePDN002(objpcl001.HeaderId, objpcl001.Status, objpcl001.RevNo, objpcl001.Project, objpcl001.Document);

                    #region Send Notification
                    string rolePLNG3 = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    (new clsManager()).SendNotification(rolePLNG3,
                                                        objpcl001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objpcl001.Project, clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(), objpcl001.RevNo.Value.ToString(), objpcl001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(), objpcl001.HeaderId.ToString(), false),
                                                        objpcl001.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [SessionExpireFilter]
        #region FORGED REACTOR
        public ActionResult LoadConstructionData(int HeaderID, string Status)
        {
            PCL002 objPCL002 = new PCL002();
            objPCL002 = db.PCL002.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL002 != null)
            {
                objPCL002 = db.PCL002.Where(i => i.LineId == objPCL002.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID;
                ViewBag.Status = Status;
                ViewBag.Action = "constructoredit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveConstructionDataPartial", objPCL002);
        }
        [SessionExpireFilter]
        public ActionResult LoadAllowancesData(int HeaderID, string Status)
        {
            PCL003 objPCL003 = new PCL003();
            objPCL003 = db.PCL003.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL003 != null)
            {
                objPCL003 = db.PCL003.Where(i => i.LineId == objPCL003.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "allowanceedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveAllowanceHtmlPartial", objPCL003);
        }

        [SessionExpireFilter]
        public ActionResult LoadScopeofworkData(int HeaderID, string Status)
        {
            PCL004 objPCL004 = new PCL004();
            objPCL004 = db.PCL004.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL004 != null)
            {
                objPCL004 = db.PCL004.Where(i => i.LineId == objPCL004.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "scopeedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveScopeofworkHtmlPartial", objPCL004);
        }
        [SessionExpireFilter]
        public ActionResult LoadFixturerequirementData(int HeaderID, string Status)
        {
            PCL005 objPCL005 = new PCL005();
            objPCL005 = db.PCL005.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL005 != null)
            {
                objPCL005 = db.PCL005.Where(i => i.LineId == objPCL005.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "fixtureedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveFixturerequirementHtmlPartial", objPCL005);
        }

        [SessionExpireFilter]
        public ActionResult LoadDocumentData(int HeaderID, string Status)
        {
            PCL006 objPCL006 = new PCL006();
            objPCL006 = db.PCL006.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL006 != null)
            {
                objPCL006 = db.PCL006.Where(i => i.LineId == objPCL006.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "fixtureedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveDocumentHtmlPartial", objPCL006);
        }

        [SessionExpireFilter]
        public ActionResult LoadDimData(int HeaderID, string Status)
        {
            PCL007 objPCL007 = new PCL007();
            objPCL007 = db.PCL007.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL007 != null)
            {
                objPCL007 = db.PCL007.Where(i => i.LineId == objPCL007.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "dimedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveDimHtmlPartial", objPCL007);
        }
        #endregion

        [SessionExpireFilter]
        #region Urea Rx Check List
        [SessionExpireFilter]
        public ActionResult LoadUreaConstructionData(int HeaderID, string Status)
        {
            PCL008 objPCL008 = new PCL008();
            objPCL008 = db.PCL008.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL008 != null)
            {
                objPCL008 = db.PCL008.Where(i => i.LineId == objPCL008.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveUreaConstructionData", objPCL008);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaAllowancesData(int HeaderID, string Status)
        {
            PCL009 objPCL009 = new PCL009();
            objPCL009 = db.PCL009.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL009 != null)
            {
                objPCL009 = db.PCL009.Where(i => i.LineId == objPCL009.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveUreaAllowancesData", objPCL009);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaScopeofworkData(int HeaderID, string Status)
        {
            PCL010 objPCL010 = new PCL010();
            objPCL010 = db.PCL010.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL010 != null)
            {
                objPCL010 = db.PCL010.Where(i => i.LineId == objPCL010.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveUreaScopeofworkData", objPCL010);
        }
        [SessionExpireFilter]
        public ActionResult LoadUreaFixturerequirementData(int HeaderID, string Status)
        {
            PCL011 objPCL011 = new PCL011();
            objPCL011 = db.PCL011.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL011 != null)
            {
                objPCL011 = db.PCL011.Where(i => i.LineId == objPCL011.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveUreaFixturerequirementData", objPCL011);
        }

        public ActionResult LoadUreaDocumentData(int HeaderID, string Status)
        {
            PCL012 objPCL012 = new PCL012();
            objPCL012 = db.PCL012.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL012 != null)
            {
                objPCL012 = db.PCL012.Where(i => i.LineId == objPCL012.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveUreaDocumentData", objPCL012);
        }

        public ActionResult LoadUreaDimData(int HeaderID, string Status)
        {
            PCL013 objPCL013 = new PCL013();
            objPCL013 = db.PCL013.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL013 != null)
            {
                objPCL013 = db.PCL013.Where(i => i.LineId == objPCL013.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveUreaDimData", objPCL013);
        }
        #endregion


        #region HSC Check points' category
        [SessionExpireFilter]
        public ActionResult LoadHSCCategoryData(int HeaderID, string Status)
        {
            PCL017 objPCL017 = new PCL017();
            objPCL017 = db.PCL017.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL017 != null)
            {
                objPCL017 = db.PCL017.Where(i => i.LineId == objPCL017.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "docedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }

            return PartialView("_LoadApproveHSCCategoryData", objPCL017);
        }
        #endregion

        #region   Coke Drum  
        [SessionExpireFilter]
        public ActionResult LoadCDConstructionData(int HeaderID, string Status)
        {
            PCL018 objPCL018 = new PCL018();
            objPCL018 = db.PCL018.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL018 != null)
            {
                objPCL018 = db.PCL018.Where(i => i.LineId == objPCL018.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "constructedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveCDConstructionData", objPCL018);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDAllowancesData(int HeaderID, string Status)
        {
            PCL019 objPCL019 = new PCL019();
            objPCL019 = db.PCL019.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL019 != null)
            {
                objPCL019 = db.PCL019.Where(i => i.LineId == objPCL019.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveCDAllowancesData", objPCL019);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDScopeofworkData(int HeaderID, string Status)
        {
            PCL020 objPCL020 = new PCL020();
            objPCL020 = db.PCL020.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL020 != null)
            {
                objPCL020 = db.PCL020.Where(i => i.LineId == objPCL020.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveCDScopeofworkData", objPCL020);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDFixturerequirementData(int HeaderID, string Status)
        {
            PCL023 objPCL023 = new PCL023();
            objPCL023 = db.PCL023.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL023 != null)
            {
                objPCL023 = db.PCL023.Where(i => i.LineId == objPCL023.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveCDFixturerequirementData", objPCL023);
        }
        [SessionExpireFilter]
        public ActionResult LoadCDDocumentData(int HeaderID, string Status)
        {
            PCL021 objPCL021 = new PCL021();
            objPCL021 = db.PCL021.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL021 != null)
            {
                objPCL021 = db.PCL021.Where(i => i.LineId == objPCL021.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveCDDocumentData", objPCL021);
        }
        [SessionExpireFilter]

        public ActionResult LoadCDDimData(int HeaderID, string Status)
        {
            PCL022 objPCL022 = new PCL022();
            objPCL022 = db.PCL022.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL022 != null)
            {
                objPCL022 = db.PCL022.Where(i => i.LineId == objPCL022.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveCDDimData", objPCL022);
        }
        #endregion

        #region  Column 
        [SessionExpireFilter]
        public ActionResult LoadColumnConstructionData(int HeaderID, string Status)
        {
            PCL024 objPCL024 = new PCL024();
            objPCL024 = db.PCL024.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL024 != null)
            {
                objPCL024 = db.PCL024.Where(i => i.LineId == objPCL024.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveColumnConstructionData", objPCL024);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnAllowancesData(int HeaderID, string Status)
        {
            PCL025 objPCL025 = new PCL025();
            objPCL025 = db.PCL025.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL025 != null)
            {
                objPCL025 = db.PCL025.Where(i => i.LineId == objPCL025.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveColumnAllowancesData", objPCL025);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnScopeofworkData(int HeaderID, string Status)
        {
            PCL026 objPCL026 = new PCL026();
            objPCL026 = db.PCL026.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL026 != null)
            {
                objPCL026 = db.PCL026.Where(i => i.LineId == objPCL026.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveColumnScopeofworkData", objPCL026);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnFixturerequirementData(int HeaderID, string Status)
        {
            PCL029 objPCL029 = new PCL029();
            objPCL029 = db.PCL029.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL029 != null)
            {
                objPCL029 = db.PCL029.Where(i => i.LineId == objPCL029.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveColumnFixturerequirementData", objPCL029);
        }
        [SessionExpireFilter]
        public ActionResult LoadColumnDocumentData(int HeaderID, string Status)
        {
            PCL027 objPCL027 = new PCL027();
            objPCL027 = db.PCL027.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL027 != null)
            {
                objPCL027 = db.PCL027.Where(i => i.LineId == objPCL027.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveColumnDocumentData", objPCL027);
        }

        [SessionExpireFilter]
        public ActionResult LoadColumnDimData(int HeaderID, string Status)
        {
            PCL028 objPCL028 = new PCL028();
            objPCL028 = db.PCL028.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL028 != null)
            {
                objPCL028 = db.PCL028.Where(i => i.LineId == objPCL028.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveColumnDimData", objPCL028);
        }
        #endregion

        #region  EO Reactor 
        [SessionExpireFilter]
        public ActionResult LoadEOConstructionData(int HeaderID, string Status)
        {
            PCL030 objPCL030 = new PCL030();
            objPCL030 = db.PCL030.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL030 != null)
            {
                objPCL030 = db.PCL030.Where(i => i.LineId == objPCL030.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveEOConstructionData", objPCL030);
        }
        [SessionExpireFilter]
        public ActionResult LoadEOAllowancesData(int HeaderID, string Status)
        {
            PCL031 objPCL031 = new PCL031();
            objPCL031 = db.PCL031.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL031 != null)
            {
                objPCL031 = db.PCL031.Where(i => i.LineId == objPCL031.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveEOAllowancesData", objPCL031);
        }
        [SessionExpireFilter]
        public ActionResult LoadEOScopeofworkData(int HeaderID, string Status)
        {
            PCL032 objPCL032 = new PCL032();
            objPCL032 = db.PCL032.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL032 != null)
            {
                objPCL032 = db.PCL032.Where(i => i.LineId == objPCL032.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveEOScopeofworkData", objPCL032);
        }
        [SessionExpireFilter]
        public ActionResult LoadEOFixturerequirementData(int HeaderID, string Status)
        {
            PCL033 objPCL033 = new PCL033();
            objPCL033 = db.PCL033.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL033 != null)
            {
                objPCL033 = db.PCL033.Where(i => i.LineId == objPCL033.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveEOFixturerequirementData", objPCL033);
        }
        [SessionExpireFilter]
        public ActionResult LoadEODocumentData(int HeaderID, string Status)
        {
            PCL034 objPCL034 = new PCL034();
            objPCL034 = db.PCL034.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL034 != null)
            {
                objPCL034 = db.PCL034.Where(i => i.LineId == objPCL034.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveEODocumentData", objPCL034);
        }
        [SessionExpireFilter]
        public ActionResult LoadEODimensionalData(int HeaderID, string Status)
        {
            PCL035 objPCL035 = new PCL035();
            objPCL035 = db.PCL035.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL035 != null)
            {
                objPCL035 = db.PCL035.Where(i => i.LineId == objPCL035.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveEODimensionalData", objPCL035);
        }
        #endregion

        #region  RR Reactor 
        [SessionExpireFilter]
        public ActionResult LoadRRConstructionData(int HeaderID, string Status)
        {
            PCL036 objPCL036 = new PCL036();
            objPCL036 = db.PCL036.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL036 != null)
            {
                objPCL036 = db.PCL036.Where(i => i.LineId == objPCL036.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveRRConstructionData", objPCL036);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRAllowancesData(int HeaderID, string Status)
        {
            PCL037 objPCL037 = new PCL037();
            objPCL037 = db.PCL037.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL037 != null)
            {
                objPCL037 = db.PCL037.Where(i => i.LineId == objPCL037.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveRRAllowancesData", objPCL037);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRScopeofworkData(int HeaderID, string Status)
        {
            PCL038 objPCL038 = new PCL038();
            objPCL038 = db.PCL038.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL038 != null)
            {
                objPCL038 = db.PCL038.Where(i => i.LineId == objPCL038.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveRRScopeofworkData", objPCL038);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRFixturerequirementData(int HeaderID, string Status)
        {
            PCL039 objPCL039 = new PCL039();
            objPCL039 = db.PCL039.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL039 != null)
            {
                objPCL039 = db.PCL039.Where(i => i.LineId == objPCL039.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveRRFixturerequirementData", objPCL039);
        }
        [SessionExpireFilter]
        public ActionResult LoadRRDocumentData(int HeaderID, string Status)
        {
            PCL040 objPCL040 = new PCL040();
            objPCL040 = db.PCL040.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL040 != null)
            {
                objPCL040 = db.PCL040.Where(i => i.LineId == objPCL040.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveRRDocumentData", objPCL040);
        }

        [SessionExpireFilter]
        public ActionResult LoadRRDimData(int HeaderID, string Status)
        {
            PCL041 objPCL041 = new PCL041();
            objPCL041 = db.PCL041.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
            if (objPCL041 != null)
            {
                objPCL041 = db.PCL041.Where(i => i.LineId == objPCL041.LineId).FirstOrDefault();
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
                ViewBag.Action = "ucedit";
            }
            else
            {
                ViewBag.HeaderId = HeaderID; ViewBag.Status = Status;
            }
            return PartialView("_LoadApproveRRDimData", objPCL041);
        }
        #endregion

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
            public int LineID;
            public string Status;
        }

        #region Forged Reactor
        [HttpPost]
        public ActionResult SaveHeader(PCL001 pcl001, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (pcl001.HeaderId > 0)
                {
                    PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl001.HeaderId).FirstOrDefault();

                    objPCL001.Document = pcl001.Document;
                    objPCL001.ProcessLicensor = pcl001.ProcessLicensor;
                    objPCL001.EquipmentNo = pcl001.EquipmentNo;
                    objPCL001.EditedBy = objClsLoginInfo.UserName;
                    objPCL001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Status = objPCL001.Status;
                    Manager.UpdatePDN002(objPCL001.HeaderId, objPCL001.Status, objPCL001.RevNo, objPCL001.Project, objPCL001.Document);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveConstructor(PCL002 pcl002, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl002.HeaderId).FirstOrDefault();
                PCL002 objPCL002 = new PCL002();
                if (pcl002.LineId > 0)
                {
                    objPCL002 = db.PCL002.Where(x => x.LineId == pcl002.LineId).FirstOrDefault();
                }
                objPCL002.HeaderId = objPCL001.HeaderId;
                objPCL002.Project = objPCL001.Project;
                objPCL002.Document = objPCL001.Document;
                objPCL002.ReactorID = pcl002.ReactorID;
                objPCL002.ReactorThk1 = pcl002.ReactorThk1;
                objPCL002.ReactorThk2 = pcl002.ReactorThk2;
                objPCL002.ReactorTLTL = pcl002.ReactorTLTL;
                objPCL002.ReactorLg = pcl002.ReactorLg;
                objPCL002.ReactorFabWt = pcl002.ReactorFabWt;
                objPCL002.ReactorHydroWt = pcl002.ReactorHydroWt;
                objPCL002.ReactorMaxShellWd = pcl002.ReactorMaxShellWd;
                objPCL002.ReactorJobMaterial = pcl002.ReactorJobMaterial;
                objPCL002.ReactoreDesigned = pcl002.ReactoreDesigned;
                objPCL002.ShellConst1Seg = pcl002.ShellConst1Seg;
                objPCL002.ShellConst2Seg = pcl002.ShellConst2Seg;
                objPCL002.ShellConstMorethan2Seg = pcl002.ShellConstMorethan2Seg;
                objPCL002.ShellConstForgedShell = pcl002.ShellConstForgedShell;
                objPCL002.TypeRollCold = pcl002.TypeRollCold;
                objPCL002.TypeRollWarm = pcl002.TypeRollWarm;
                objPCL002.TypeRollHot = pcl002.TypeRollHot;
                objPCL002.GasCuttingPreHeat = pcl002.GasCuttingPreHeat;
                objPCL002.GasCuttingPreHeatNR = pcl002.GasCuttingPreHeatNR;
                objPCL002.CSeamWEPPlate = pcl002.CSeamWEPPlate;
                objPCL002.CSeamWEPAfter = pcl002.CSeamWEPAfter;
                objPCL002.PTCRequiredforLS = pcl002.PTCRequiredforLS;
                objPCL002.PTCRequiredforCS = pcl002.PTCRequiredforCS;
                objPCL002.PTCRequiredforDend = pcl002.PTCRequiredforDend;
                objPCL002.PTCRequiredforSpool = pcl002.PTCRequiredforSpool;
                objPCL002.ISRRequiredforLS = pcl002.ISRRequiredforLS;
                objPCL002.ISRRequiredforCS = pcl002.ISRRequiredforCS;
                objPCL002.ISRRequiredforNozzle = pcl002.ISRRequiredforNozzle;
                objPCL002.ISRRequiredforNub = pcl002.ISRRequiredforNub;
                objPCL002.ISRRequiredforSpool = pcl002.ISRRequiredforSpool;
                objPCL002.PTMTAfterWelding = pcl002.PTMTAfterWelding;
                objPCL002.PTMTAfterOverlay = pcl002.PTMTAfterOverlay;
                objPCL002.PTMTAfterPWHT = pcl002.PTMTAfterPWHT;
                objPCL002.PTMTAfterHydro = pcl002.PTMTAfterHydro;
                objPCL002.RTAfterWelding = pcl002.RTAfterWelding;
                objPCL002.RTAfterOverlay = pcl002.RTAfterOverlay;
                objPCL002.RTAfterPWHT = pcl002.RTAfterPWHT;
                objPCL002.RTAfterHydro = pcl002.RTAfterHydro;
                objPCL002.UTAfterRecordable = pcl002.UTAfterRecordable;
                objPCL002.UTAfterConventional = pcl002.UTAfterConventional;
                objPCL002.UTAfterWelding = pcl002.UTAfterWelding;
                objPCL002.UTAfterOverlay = pcl002.UTAfterOverlay;
                objPCL002.UTAfterPWHT = pcl002.UTAfterPWHT;
                objPCL002.UTAfterHydro = pcl002.UTAfterHydro;
                objPCL002.FerAfterWelding = pcl002.FerAfterWelding;
                objPCL002.FerAfterOverlay = pcl002.FerAfterOverlay;
                objPCL002.FerAfterPWHT = pcl002.FerAfterPWHT;
                objPCL002.FerAfterHydro = pcl002.FerAfterHydro;
                objPCL002.SOTESSCSingle = pcl002.SOTESSCSingle;
                objPCL002.SOTESSCDouble = pcl002.SOTESSCDouble;
                objPCL002.SOTESSCClade = pcl002.SOTESSCClade;
                objPCL002.SOTESSCOlayNR = pcl002.SOTESSCOlayNR;
                objPCL002.DCPetalWCrown = pcl002.DCPetalWCrown;
                objPCL002.DC4PetalWoCrown = pcl002.DC4PetalWoCrown;
                objPCL002.DCSinglePeice = pcl002.DCSinglePeice;
                objPCL002.DCTwoHalves = pcl002.DCTwoHalves;
                objPCL002.DRCFormedPetals = pcl002.DRCFormedPetals;
                objPCL002.DRCFormedSetup = pcl002.DRCFormedSetup;
                objPCL002.DRCFormedWelded = pcl002.DRCFormedWelded;
                objPCL002.DOTESSCSingle = pcl002.DOTESSCSingle;
                objPCL002.DOTESSCDouble = pcl002.DOTESSCDouble;
                objPCL002.DOTESSCClade = pcl002.DOTESSCClade;
                objPCL002.DOTESSCOlayNR = pcl002.DOTESSCOlayNR;
                objPCL002.SVJYring = pcl002.SVJYring;
                objPCL002.SVJBuildup = pcl002.SVJBuildup;
                objPCL002.SVJLapJoint = pcl002.SVJLapJoint;
                objPCL002.CSWeldBuildup = pcl002.CSWeldBuildup;
                objPCL002.CSForgedShell = pcl002.CSForgedShell;
                objPCL002.CSPlateWOlay = pcl002.CSPlateWOlay;
                objPCL002.CSPlate = pcl002.CSPlate;
                objPCL002.CSSSPlate = pcl002.CSSSPlate;
                objPCL002.CSNoofGussets = pcl002.CSNoofGussets;
                objPCL002.CSNR = pcl002.CSNR;
                objPCL002.TSWeldBuildup = pcl002.TSWeldBuildup;
                objPCL002.TSForgedShell = pcl002.TSForgedShell;
                objPCL002.TSPlateWOlay = pcl002.TSPlateWOlay;
                objPCL002.TSPlate = pcl002.TSPlate;
                objPCL002.TSSSPlate = pcl002.TSSSPlate;
                objPCL002.TSNoofGussets = pcl002.TSNoofGussets;
                objPCL002.TSNR = pcl002.TSNR;
                objPCL002.NFCRaised = pcl002.NFCRaised;
                objPCL002.NFCGroove = pcl002.NFCGroove;
                objPCL002.NFCOther = pcl002.NFCOther;
                objPCL002.NGZBfPWHT = pcl002.NGZBfPWHT;
                objPCL002.NGZAfPWHT = pcl002.NGZAfPWHT;
                objPCL002.SpoolMatSS = pcl002.SpoolMatSS;
                objPCL002.SpoolMatLAS = pcl002.SpoolMatLAS;
                objPCL002.JSInConnel = pcl002.JSInConnel;
                objPCL002.JSBiMetalic = pcl002.JSBiMetalic;
                objPCL002.PTCRequiredforESSC = pcl002.PTCRequiredforESSC;
                objPCL002.JSCladRestoration = pcl002.JSCladRestoration;
                objPCL002.SSWBfPWHT = pcl002.SSWBfPWHT;
                objPCL002.SSWAfPWHT = pcl002.SSWAfPWHT;
                objPCL002.SSWBoth = pcl002.SSWBoth;
                objPCL002.VessleSSkirt = pcl002.VessleSSkirt;
                objPCL002.VessleSSupp = pcl002.VessleSSupp;
                objPCL002.VessleSLegs = pcl002.VessleSLegs;
                objPCL002.VessleSSaddle = pcl002.VessleSSaddle;
                objPCL002.SkirtMadein1 = pcl002.SkirtMadein1;
                objPCL002.SkirtMadein2 = pcl002.SkirtMadein2;
                objPCL002.FPNWeldNuts = pcl002.FPNWeldNuts;
                objPCL002.FPNLeaveRow = pcl002.FPNLeaveRow;
                objPCL002.PWHTSinglePiece = pcl002.PWHTSinglePiece;
                objPCL002.PWHTLSRJoint = pcl002.PWHTLSRJoint;
                objPCL002.PTCMTCptc = pcl002.PTCMTCptc;
                objPCL002.PTCMTCmtc = pcl002.PTCMTCmtc;
                objPCL002.PTCMTCptcSim = pcl002.PTCMTCptcSim;
                objPCL002.PTCMTCmtcSim = pcl002.PTCMTCmtcSim;
                objPCL002.SaddlesModified = pcl002.SaddlesModified;
                objPCL002.SaddlesNew = pcl002.SaddlesNew;
                objPCL002.PWHTinFurnacePFS = pcl002.PWHTinFurnacePFS;
                objPCL002.PWHTinFurnaceHFS1 = pcl002.PWHTinFurnaceHFS1;
                objPCL002.PWHTinFurnaceLEMF = pcl002.PWHTinFurnaceLEMF;
                objPCL002.SectionWtMore230 = pcl002.SectionWtMore230;
                objPCL002.SectionWt200To230 = pcl002.SectionWt200To230;
                objPCL002.SectionWt150To200 = pcl002.SectionWt150To200;
                objPCL002.SectionWtLess150 = pcl002.SectionWtLess150;
                objPCL002.SectionWtBlankSec = pcl002.SectionWtBlankSec;
                objPCL002.SectionWt1Sec = pcl002.SectionWt1Sec;
                objPCL002.SectionWt9Sec = pcl002.SectionWt9Sec;
                objPCL002.SectionWt1aSec = pcl002.SectionWt1aSec;
                objPCL002.ARMRequired = pcl002.ARMRequired;
                objPCL002.ARMRT = pcl002.ARMRT;
                objPCL002.ARMPWHT = pcl002.ARMPWHT;
                objPCL002.ARMSB = pcl002.ARMSB;
                objPCL002.CLRH50ppm = pcl002.CLRH50ppm;
                objPCL002.CLRHNoLimit = pcl002.CLRHNoLimit;
                objPCL002.TemplateForReq = pcl002.TemplateForReq;
                objPCL002.TemplateForNReq = pcl002.TemplateForNReq;
                objPCL002.N2FillReq = pcl002.N2FillReq;
                objPCL002.N2FillNotReq = pcl002.N2FillNotReq;
                objPCL002.N2FillOther = pcl002.N2FillOther;
                objPCL002.ESTPlate = pcl002.ESTPlate;
                objPCL002.ESTShell = pcl002.ESTShell;
                objPCL002.ESTSection = pcl002.ESTSection;
                objPCL002.ESTEquip = pcl002.ESTEquip;
                objPCL002.ISTPlate = pcl002.ISTPlate;
                objPCL002.ISTShell = pcl002.ISTShell;
                objPCL002.ISTSection = pcl002.ISTSection;
                objPCL002.ISTEquip = pcl002.ISTEquip;
                objPCL002.ISTAfBfPWHT = pcl002.ISTAfBfPWHT;
                objPCL002.ISTAfHydro = pcl002.ISTAfHydro;
                objPCL002.CDNSS = pcl002.CDNSS;
                objPCL002.CDNLAS = pcl002.CDNLAS;
                objPCL002.CDNCS = pcl002.CDNCS;
                objPCL002.CDNNotReq = pcl002.CDNNotReq;
                objPCL002.MaxSBLess1_38 = pcl002.MaxSBLess1_38;
                objPCL002.MaxSBLess1_78 = pcl002.MaxSBLess1_78;
                objPCL002.MaxSBLess2_14 = pcl002.MaxSBLess2_14;
                objPCL002.MaxSBLess4 = pcl002.MaxSBLess4;
                objPCL002.MaxSBMore4 = pcl002.MaxSBMore4;
                objPCL002.ShellConstForgedShellVal = pcl002.ShellConstForgedShellVal;
                objPCL002.TypeRollWarmVal = pcl002.TypeRollWarmVal;
                objPCL002.TypeRollHotVal = pcl002.TypeRollHotVal;
                objPCL002.TypeRollHotmin = pcl002.TypeRollHotmin;
                objPCL002.CSVal = pcl002.CSVal;
                objPCL002.TSVal = pcl002.TSVal;
                objPCL002.SaddlesModifiedVal = pcl002.SaddlesModifiedVal;
                objPCL002.SaddlesNewVal = pcl002.SaddlesNewVal;

                if (pcl002.LineId > 0)
                {
                    objPCL002.EditedBy = objClsLoginInfo.UserName;
                    objPCL002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL002.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL002.CreatedBy = objClsLoginInfo.UserName;
                    objPCL002.CreatedOn = DateTime.Now;
                    db.PCL002.Add(objPCL002);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL002.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAllowance(PCL003 pcl003, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl003.HeaderId).FirstOrDefault();
                PCL003 objPCL003 = new PCL003();
                if (pcl003.LineId > 0)
                {
                    objPCL003 = db.PCL003.Where(x => x.LineId == pcl003.LineId).FirstOrDefault();
                }
                objPCL003.HeaderId = objPCL001.HeaderId;
                objPCL003.Project = objPCL001.Project;
                objPCL003.Document = objPCL001.Document;
                objPCL003.ShellShrinkage = pcl003.ShellShrinkage;
                objPCL003.ShellWidth = pcl003.ShellWidth;
                objPCL003.ShellOvality = pcl003.ShellOvality;
                objPCL003.SkirtBCD = pcl003.SkirtBCD;
                objPCL003.DEndMcing = pcl003.DEndMcing;
                objPCL003.DEndSeamShrinkage = pcl003.DEndSeamShrinkage;
                objPCL003.DEndESSC = pcl003.DEndESSC;
                objPCL003.YRingESSC = pcl003.YRingESSC;
                objPCL003.SkirtLevel = pcl003.SkirtLevel;

                if (pcl003.LineId > 0)
                {
                    objPCL003.EditedBy = objClsLoginInfo.UserName;
                    objPCL003.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL003.CreatedBy = objClsLoginInfo.UserName;
                    objPCL003.CreatedOn = DateTime.Now;
                    db.PCL003.Add(objPCL003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL003.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveScopeOfWork(PCL004 pcl004, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl004.HeaderId).FirstOrDefault();
                PCL004 objPCL004 = new PCL004();
                if (pcl004.LineId > 0)
                {
                    objPCL004 = db.PCL004.Where(x => x.LineId == pcl004.LineId).FirstOrDefault();
                }
                objPCL004.HeaderId = objPCL001.HeaderId;
                objPCL004.Project = objPCL001.Project;
                objPCL004.Document = objPCL001.Document;
                objPCL004.EquipmentNotReq = pcl004.EquipmentNotReq;
                objPCL004.EquipmentShop = pcl004.EquipmentShop;
                objPCL004.EquipmentMC = pcl004.EquipmentMC;
                objPCL004.EquipmentLEMF = pcl004.EquipmentLEMF;
                objPCL004.EquipmentRNW = pcl004.EquipmentRNW;
                objPCL004.EquipmentOutside = pcl004.EquipmentOutside;
                objPCL004.DEndFormingNotReq = pcl004.DEndFormingNotReq;
                objPCL004.DEndFormingShop = pcl004.DEndFormingShop;
                objPCL004.DEndFormingMC = pcl004.DEndFormingMC;
                objPCL004.DEndFormingLEMF = pcl004.DEndFormingLEMF;
                objPCL004.DEndFormingRNW = pcl004.DEndFormingRNW;
                objPCL004.DEndFormingOutside = pcl004.DEndFormingOutside;
                objPCL004.DEndSetupNotReq = pcl004.DEndSetupNotReq;
                objPCL004.DEndSetupShop = pcl004.DEndSetupShop;
                objPCL004.DEndSetupMC = pcl004.DEndSetupMC;
                objPCL004.DEndSetupLEMF = pcl004.DEndSetupLEMF;
                objPCL004.DEndSetupRNW = pcl004.DEndSetupRNW;
                objPCL004.DEndSetupOutside = pcl004.DEndSetupOutside;
                objPCL004.DEndWeldingNotReq = pcl004.DEndWeldingNotReq;
                objPCL004.DEndWeldingShop = pcl004.DEndWeldingShop;
                objPCL004.DEndWeldingMC = pcl004.DEndWeldingMC;
                objPCL004.DEndWeldingLEMF = pcl004.DEndWeldingLEMF;
                objPCL004.DEndWeldingRNW = pcl004.DEndWeldingRNW;
                objPCL004.DEndWeldingOutside = pcl004.DEndWeldingOutside;
                objPCL004.SkirtNotReq = pcl004.SkirtNotReq;
                objPCL004.SkirtShop = pcl004.SkirtShop;
                objPCL004.SkirtMC = pcl004.SkirtMC;
                objPCL004.SkirtLEMF = pcl004.SkirtLEMF;
                objPCL004.SkirtRNW = pcl004.SkirtRNW;
                objPCL004.SkirtOutside = pcl004.SkirtOutside;
                objPCL004.YRingMachiningNotReq = pcl004.YRingMachiningNotReq;
                objPCL004.YRingMachiningShop = pcl004.YRingMachiningShop;
                objPCL004.YRingMachiningMC = pcl004.YRingMachiningMC;
                objPCL004.YRingMachiningLEMF = pcl004.YRingMachiningLEMF;
                objPCL004.YRingMachiningRNW = pcl004.YRingMachiningRNW;
                objPCL004.YRingMachiningOutside = pcl004.YRingMachiningOutside;
                objPCL004.NozMachiningNotReq = pcl004.NozMachiningNotReq;
                objPCL004.NozMachiningShop = pcl004.NozMachiningShop;
                objPCL004.NozMachiningMC = pcl004.NozMachiningMC;
                objPCL004.NozMachiningLEMF = pcl004.NozMachiningLEMF;
                objPCL004.NozMachiningRNW = pcl004.NozMachiningRNW;
                objPCL004.NozMachiningOutside = pcl004.NozMachiningOutside;
                objPCL004.NozOverlayNotReq = pcl004.NozOverlayNotReq;
                objPCL004.NozOverlayMC = pcl004.NozOverlayMC;
                objPCL004.NozOverlayShop = pcl004.NozOverlayShop;
                objPCL004.NozOverlayLEMF = pcl004.NozOverlayLEMF;
                objPCL004.NozOverlayRNW = pcl004.NozOverlayRNW;
                objPCL004.NozOverlayOutside = pcl004.NozOverlayOutside;
                objPCL004.ManOverlayNotReq = pcl004.ManOverlayNotReq;
                objPCL004.ManOverlayShop = pcl004.ManOverlayShop;
                objPCL004.ManOverlayMC = pcl004.ManOverlayMC;
                objPCL004.ManOverlayLEMF = pcl004.ManOverlayLEMF;
                objPCL004.ManOverlayRNW = pcl004.ManOverlayRNW;
                objPCL004.ManOverlayOutside = pcl004.ManOverlayOutside;
                objPCL004.NozFinMachiningNotReq = pcl004.NozFinMachiningNotReq;
                objPCL004.NozFinMachiningShop = pcl004.NozFinMachiningShop;
                objPCL004.NozFinMachiningMC = pcl004.NozFinMachiningMC;
                objPCL004.NozFinMachiningLEMF = pcl004.NozFinMachiningLEMF;
                objPCL004.NozFinMachiningRNW = pcl004.NozFinMachiningRNW;
                objPCL004.NozFinMachiningOutside = pcl004.NozFinMachiningOutside;
                objPCL004.NozWEPNotReq = pcl004.NozWEPNotReq;
                objPCL004.NozWEPShop = pcl004.NozWEPShop;
                objPCL004.NozWEPMC = pcl004.NozWEPMC;
                objPCL004.NozWEPLEMF = pcl004.NozWEPLEMF;
                objPCL004.NozWEPRNW = pcl004.NozWEPRNW;
                objPCL004.NozWEPOutside = pcl004.NozWEPOutside;
                objPCL004.LongWEPNotReq = pcl004.LongWEPNotReq;
                objPCL004.LongWEPShop = pcl004.LongWEPShop;
                objPCL004.LongWEPMC = pcl004.LongWEPMC;
                objPCL004.LongWEPLEMF = pcl004.LongWEPLEMF;
                objPCL004.LongWEPRNW = pcl004.LongWEPRNW;
                objPCL004.LongWEPOutside = pcl004.LongWEPOutside;
                objPCL004.CircWEPNotReq = pcl004.CircWEPNotReq;
                objPCL004.CircWEPShop = pcl004.CircWEPShop;
                objPCL004.CircWEPMC = pcl004.CircWEPMC;
                objPCL004.CircWEPLEMF = pcl004.CircWEPLEMF;
                objPCL004.CircWEPRNW = pcl004.CircWEPRNW;
                objPCL004.CircWEPOutside = pcl004.CircWEPOutside;
                objPCL004.LiftMachiningNotReq = pcl004.LiftMachiningNotReq;
                objPCL004.LiftMachiningShop = pcl004.LiftMachiningShop;
                objPCL004.LiftMachiningMC = pcl004.LiftMachiningMC;
                objPCL004.LiftMachiningLEMF = pcl004.LiftMachiningLEMF;
                objPCL004.LiftMachiningRNW = pcl004.LiftMachiningRNW;
                objPCL004.LiftMachiningOutside = pcl004.LiftMachiningOutside;
                objPCL004.LiftFabricationNotReq = pcl004.LiftFabricationNotReq;
                objPCL004.LiftFabricationShop = pcl004.LiftFabricationShop;
                objPCL004.LiftFabricationMC = pcl004.LiftFabricationMC;
                objPCL004.LiftFabricationLEMF = pcl004.LiftFabricationLEMF;
                objPCL004.LiftFabricationRNW = pcl004.LiftFabricationRNW;
                objPCL004.LiftFabricationOutside = pcl004.LiftFabricationOutside;
                objPCL004.WeldExFabNotReq = pcl004.WeldExFabNotReq;
                objPCL004.WeldExFabShop = pcl004.WeldExFabShop;
                objPCL004.WeldExFabMC = pcl004.WeldExFabMC;
                objPCL004.WeldExFabLEMF = pcl004.WeldExFabLEMF;
                objPCL004.WeldExFabRNW = pcl004.WeldExFabRNW;
                objPCL004.WeldExFabOutside = pcl004.WeldExFabOutside;
                objPCL004.WeldInFabOutside = pcl004.WeldInFabOutside;
                objPCL004.WeldInFabNotReq = pcl004.WeldInFabNotReq;
                objPCL004.WeldInFabShop = pcl004.WeldInFabShop;
                objPCL004.WeldInFabMC = pcl004.WeldInFabMC;
                objPCL004.WeldInFabLEMF = pcl004.WeldInFabLEMF;
                objPCL004.WeldInFabRNW = pcl004.WeldInFabRNW;
                objPCL004.NubFabNotReq = pcl004.NubFabNotReq;
                objPCL004.NubFabShop = pcl004.NubFabShop;
                objPCL004.NubFabMC = pcl004.NubFabMC;
                objPCL004.NubFabLEMF = pcl004.NubFabLEMF;
                objPCL004.NubFabRNW = pcl004.NubFabRNW;
                objPCL004.NubFabOutside = pcl004.NubFabOutside;
                objPCL004.NubBeforeNotReq = pcl004.NubBeforeNotReq;
                objPCL004.NubBeforeShop = pcl004.NubBeforeShop;
                objPCL004.NubBeforeMC = pcl004.NubBeforeMC;
                objPCL004.NubBeforeLEMF = pcl004.NubBeforeLEMF;
                objPCL004.NubBeforeRNW = pcl004.NubBeforeRNW;
                objPCL004.NubBeforeOutside = pcl004.NubBeforeOutside;
                objPCL004.NubAfterNotReq = pcl004.NubAfterNotReq;
                objPCL004.NubAfterShop = pcl004.NubAfterShop;
                objPCL004.NubAfterMC = pcl004.NubAfterMC;
                objPCL004.NubAfterLEMF = pcl004.NubAfterLEMF;
                objPCL004.NubAfterRNW = pcl004.NubAfterRNW;
                objPCL004.NubAfterOutside = pcl004.NubAfterOutside;
                objPCL004.BraketWEPNotReq = pcl004.BraketWEPNotReq;
                objPCL004.BraketWEPShop = pcl004.BraketWEPShop;
                objPCL004.BraketWEPMC = pcl004.BraketWEPMC;
                objPCL004.BraketWEPLEMF = pcl004.BraketWEPLEMF;
                objPCL004.BraketWEPRNW = pcl004.BraketWEPRNW;
                objPCL004.BraketWEPOutside = pcl004.BraketWEPOutside;
                objPCL004.NubTrayFabNotReq = pcl004.NubTrayFabNotReq;
                objPCL004.NubTrayFabShop = pcl004.NubTrayFabShop;
                objPCL004.NubTrayFabMC = pcl004.NubTrayFabMC;
                objPCL004.NubTrayFabLEMF = pcl004.NubTrayFabLEMF;
                objPCL004.NubTrayFabRNW = pcl004.NubTrayFabRNW;
                objPCL004.NubTrayFabOutside = pcl004.NubTrayFabOutside;
                objPCL004.InsFabNotReq = pcl004.InsFabNotReq;
                objPCL004.InsFabShop = pcl004.InsFabShop;
                objPCL004.InsFabMC = pcl004.InsFabMC;
                objPCL004.InsFabLEMF = pcl004.InsFabLEMF;
                objPCL004.InsFabRNW = pcl004.InsFabRNW;
                objPCL004.InsFabOutside = pcl004.InsFabOutside;
                objPCL004.CatalystBeamWEPNotReq = pcl004.CatalystBeamWEPNotReq;
                objPCL004.CatalystBeamWEPShop = pcl004.CatalystBeamWEPShop;
                objPCL004.CatalystBeamWEPMC = pcl004.CatalystBeamWEPMC;
                objPCL004.CatalystBeamWEPLEMF = pcl004.CatalystBeamWEPLEMF;
                objPCL004.CatalystBeamWEPRNW = pcl004.CatalystBeamWEPRNW;
                objPCL004.CatalystBeamWEPOutside = pcl004.CatalystBeamWEPOutside;
                objPCL004.CatalystBeamFabricationNotReq = pcl004.CatalystBeamFabricationNotReq;
                objPCL004.CatalystBeamFabricationShop = pcl004.CatalystBeamFabricationShop;
                objPCL004.CatalystBeamFabricationMC = pcl004.CatalystBeamFabricationMC;
                objPCL004.CatalystBeamFabricationLEMF = pcl004.CatalystBeamFabricationLEMF;
                objPCL004.CatalystBeamFabricationRNW = pcl004.CatalystBeamFabricationRNW;
                objPCL004.CatalystBeamFabricationOutside = pcl004.CatalystBeamFabricationOutside;
                objPCL004.InternalTrayFabricationNotReq = pcl004.InternalTrayFabricationNotReq;
                objPCL004.InternalTrayFabricationShop = pcl004.InternalTrayFabricationShop;
                objPCL004.InternalTrayFabricationMC = pcl004.InternalTrayFabricationMC;
                objPCL004.InternalTrayFabricationLEMF = pcl004.InternalTrayFabricationLEMF;
                objPCL004.InternalTrayFabricationRNW = pcl004.InternalTrayFabricationRNW;
                objPCL004.InternalTrayFabricationOutside = pcl004.InternalTrayFabricationOutside;
                objPCL004.InletDiffuserFabricationNotReq = pcl004.InletDiffuserFabricationNotReq;
                objPCL004.InletDiffuserFabricationShop = pcl004.InletDiffuserFabricationShop;
                objPCL004.InletDiffuserFabricationMC = pcl004.InletDiffuserFabricationMC;
                objPCL004.InletDiffuserFabricationLEMF = pcl004.InletDiffuserFabricationLEMF;
                objPCL004.InletDiffuserFabricationRNW = pcl004.InletDiffuserFabricationRNW;
                objPCL004.InletDiffuserFabricationOutside = pcl004.InletDiffuserFabricationOutside;
                objPCL004.OutletCollectorNotReq = pcl004.OutletCollectorNotReq;
                objPCL004.OutletCollectorShop = pcl004.OutletCollectorShop;
                objPCL004.OutletCollectorMC = pcl004.OutletCollectorMC;
                objPCL004.OutletCollectorLEMF = pcl004.OutletCollectorLEMF;
                objPCL004.OutletCollectorRNW = pcl004.OutletCollectorRNW;
                objPCL004.OutletCollectorOutside = pcl004.OutletCollectorOutside;
                objPCL004.InternalNozzlesNotReq = pcl004.InternalNozzlesNotReq;
                objPCL004.InternalNozzlesShop = pcl004.InternalNozzlesShop;
                objPCL004.InternalNozzlesMC = pcl004.InternalNozzlesMC;
                objPCL004.InternalNozzlesLEMF = pcl004.InternalNozzlesLEMF;
                objPCL004.InternalNozzlesRNW = pcl004.InternalNozzlesRNW;
                objPCL004.InternalNozzlesOutside = pcl004.InternalNozzlesOutside;
                objPCL004.SaddlesNotReq = pcl004.SaddlesNotReq;
                objPCL004.SaddlesShop = pcl004.SaddlesShop;
                objPCL004.SaddlesMC = pcl004.SaddlesMC;
                objPCL004.SaddlesLEMF = pcl004.SaddlesLEMF;
                objPCL004.SaddlesRNW = pcl004.SaddlesRNW;
                objPCL004.SaddlesOutside = pcl004.SaddlesOutside;
                objPCL004.SkirtTemplateNotReq = pcl004.SkirtTemplateNotReq;
                objPCL004.SkirtTemplateShop = pcl004.SkirtTemplateShop;
                objPCL004.SkirtTemplateMC = pcl004.SkirtTemplateMC;
                objPCL004.SkirtTemplateLEMF = pcl004.SkirtTemplateLEMF;
                objPCL004.SkirtTemplateRNW = pcl004.SkirtTemplateRNW;
                objPCL004.SkirtTemplateOutside = pcl004.SkirtTemplateOutside;
                objPCL004.PlatformFabricationNotReq = pcl004.PlatformFabricationNotReq;
                objPCL004.PlatformFabricationShop = pcl004.PlatformFabricationShop;
                objPCL004.PlatformFabricationMC = pcl004.PlatformFabricationMC;
                objPCL004.PlatformFabricationLEMF = pcl004.PlatformFabricationLEMF;
                objPCL004.PlatformFabricationRNW = pcl004.PlatformFabricationRNW;
                objPCL004.PlatformFabricationOutside = pcl004.PlatformFabricationOutside;
                objPCL004.PlatformTrialAssemblyNotReq = pcl004.PlatformTrialAssemblyNotReq;
                objPCL004.PlatformTrialAssemblyShop = pcl004.PlatformTrialAssemblyShop;
                objPCL004.PlatformTrialAssemblyMC = pcl004.PlatformTrialAssemblyMC;
                objPCL004.PlatformTrialAssemblyLEMF = pcl004.PlatformTrialAssemblyLEMF;
                objPCL004.PlatformTrialAssemblyRNW = pcl004.PlatformTrialAssemblyRNW;
                objPCL004.PlatformTrialAssemblyOutside = pcl004.PlatformTrialAssemblyOutside;
                objPCL004.PWHTNotReq = pcl004.PWHTNotReq;
                objPCL004.PWHTShop = pcl004.PWHTShop;
                objPCL004.PWHTMC = pcl004.PWHTMC;
                objPCL004.PWHTLEMF = pcl004.PWHTLEMF;
                objPCL004.PWHTRNW = pcl004.PWHTRNW;
                objPCL004.PWHTOutside = pcl004.PWHTOutside;
                objPCL004.InternalInstallationNotReq = pcl004.InternalInstallationNotReq;
                objPCL004.InternalInstallationShop = pcl004.InternalInstallationShop;
                objPCL004.InternalInstallationMC = pcl004.InternalInstallationMC;
                objPCL004.InternalInstallationLEMF = pcl004.InternalInstallationLEMF;
                objPCL004.InternalInstallationRNW = pcl004.InternalInstallationRNW;
                objPCL004.InternalInstallationOutside = pcl004.InternalInstallationOutside;
                objPCL004.NFillingNotReq = pcl004.NFillingNotReq;
                objPCL004.NFillingShop = pcl004.NFillingShop;
                objPCL004.NFillingMC = pcl004.NFillingMC;
                objPCL004.NFillingLEMF = pcl004.NFillingLEMF;
                objPCL004.NFillingRNW = pcl004.NFillingRNW;
                objPCL004.NFillingOutside = pcl004.NFillingOutside;
                objPCL004.SandBlastingNotReq = pcl004.SandBlastingNotReq;
                objPCL004.SandBlastingShop = pcl004.SandBlastingShop;
                objPCL004.SandBlastingMC = pcl004.SandBlastingMC;
                objPCL004.SandBlastingLEMF = pcl004.SandBlastingLEMF;
                objPCL004.SandBlastingRNW = pcl004.SandBlastingRNW;
                objPCL004.SandBlastingOutside = pcl004.SandBlastingOutside;
                objPCL004.PaintingNotReq = pcl004.PaintingNotReq;
                objPCL004.PaintingShop = pcl004.PaintingShop;
                objPCL004.PaintingMC = pcl004.PaintingMC;
                objPCL004.PaintingLEMF = pcl004.PaintingLEMF;
                objPCL004.PaintingRNW = pcl004.PaintingRNW;
                objPCL004.PaintingOutside = pcl004.PaintingOutside;
                objPCL004.AcidCleaningNotReq = pcl004.AcidCleaningNotReq;
                objPCL004.AcidCleaningShop = pcl004.AcidCleaningShop;
                objPCL004.AcidCleaningMC = pcl004.AcidCleaningMC;
                objPCL004.AcidCleaningLEMF = pcl004.AcidCleaningLEMF;
                objPCL004.AcidCleaningRNW = pcl004.AcidCleaningRNW;
                objPCL004.AcidCleaningOutside = pcl004.AcidCleaningOutside;
                objPCL004.WeightMeasurementNotReq = pcl004.WeightMeasurementNotReq;
                objPCL004.WeightMeasurementShop = pcl004.WeightMeasurementShop;
                objPCL004.WeightMeasurementMC = pcl004.WeightMeasurementMC;
                objPCL004.WeightMeasurementLEMF = pcl004.WeightMeasurementLEMF;
                objPCL004.WeightMeasurementRNW = pcl004.WeightMeasurementRNW;
                objPCL004.WeightMeasurementOutside = pcl004.WeightMeasurementOutside;
                objPCL004.ManwayDavitNotReq = pcl004.ManwayDavitNotReq;
                objPCL004.ManwayDavitShop = pcl004.ManwayDavitShop;
                objPCL004.ManwayDavitMC = pcl004.ManwayDavitMC;
                objPCL004.ManwayDavitLEMF = pcl004.ManwayDavitLEMF;
                objPCL004.ManwayDavitRNW = pcl004.ManwayDavitRNW;
                objPCL004.ManwayDavitOutside = pcl004.ManwayDavitOutside;
                objPCL004.VesselDavitNotReq = pcl004.VesselDavitNotReq;
                objPCL004.VesselDavitShop = pcl004.VesselDavitShop;
                objPCL004.VesselDavitMC = pcl004.VesselDavitMC;
                objPCL004.VesselDavitLEMF = pcl004.VesselDavitLEMF;
                objPCL004.VesselDavitRNW = pcl004.VesselDavitRNW;
                objPCL004.VesselDavitOutside = pcl004.VesselDavitOutside;
                objPCL004.LappingRingNotReq = pcl004.LappingRingNotReq;
                objPCL004.LappingRingShop = pcl004.LappingRingShop;
                objPCL004.LappingRingMC = pcl004.LappingRingMC;
                objPCL004.LappingRingLEMF = pcl004.LappingRingLEMF;
                objPCL004.LappingRingRNW = pcl004.LappingRingRNW;
                objPCL004.LappingRingOutside = pcl004.LappingRingOutside;
                objPCL004.ShippingNotReq = pcl004.ShippingNotReq;
                objPCL004.ShippingShop = pcl004.ShippingShop;
                objPCL004.ShippingMC = pcl004.ShippingMC;
                objPCL004.ShippingLEMF = pcl004.ShippingLEMF;
                objPCL004.ShippingRNW = pcl004.ShippingRNW;
                objPCL004.ShippingOutside = pcl004.ShippingOutside;

                if (pcl004.LineId > 0)
                {
                    objPCL004.EditedBy = objClsLoginInfo.UserName;
                    objPCL004.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL004.CreatedBy = objClsLoginInfo.UserName;
                    objPCL004.CreatedOn = DateTime.Now;
                    db.PCL004.Add(objPCL004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL004.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveFixture(PCL005 pcl005, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl005.HeaderId).FirstOrDefault();
                PCL005 objPCL005 = new PCL005();
                if (pcl005.LineId > 0)
                {
                    objPCL005 = db.PCL005.Where(x => x.LineId == pcl005.LineId).FirstOrDefault();
                }

                objPCL005.HeaderId = objPCL001.HeaderId;
                objPCL005.Project = objPCL001.Project;
                objPCL005.Document = objPCL001.Document;
                objPCL005.TempRollExternalReq = pcl005.TempRollExternalReq;
                string TempRollExternalReqDate = fc["TempRollExternalReqDate"];
                if (!string.IsNullOrWhiteSpace(TempRollExternalReqDate))
                {
                    objPCL005.TempRollExternalReqDate = DateTime.ParseExact(TempRollExternalReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else { objPCL005.TempRollExternalReqDate = pcl005.TempRollExternalReqDate; }
                objPCL005.TempRollExternalNoFix = pcl005.TempRollExternalNoFix;
                objPCL005.TempRollExternalAvlQty = pcl005.TempRollExternalAvlQty;
                objPCL005.TempRollExternalDrg = pcl005.TempRollExternalDrg;
                objPCL005.TempRollExternalMtrl = pcl005.TempRollExternalMtrl;
                objPCL005.TempRollExternalFab = pcl005.TempRollExternalFab;
                objPCL005.TempRollExternalDel = pcl005.TempRollExternalDel;
                objPCL005.TempRollPlateReq = pcl005.TempRollPlateReq;
                string TempRollPlateReqDate = fc["TempRollPlateReqDate"];
                if (!string.IsNullOrWhiteSpace(TempRollPlateReqDate))
                {
                    objPCL005.TempRollPlateReqDate = DateTime.ParseExact(TempRollPlateReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL005.TempRollPlateReqDate = pcl005.TempRollPlateReqDate; }
                objPCL005.TempRollPlateNoFix = pcl005.TempRollPlateNoFix;
                objPCL005.TempRollPlateAvlQty = pcl005.TempRollPlateAvlQty;
                objPCL005.TempRollPlateDrg = pcl005.TempRollPlateDrg;
                objPCL005.TempRollPlateMtrl = pcl005.TempRollPlateMtrl;
                objPCL005.TempRollPlateFab = pcl005.TempRollPlateFab;
                objPCL005.TempRollPlateDel = pcl005.TempRollPlateDel;
                objPCL005.GasketCoverReq = pcl005.GasketCoverReq;
                string GasketCoverReqDate = fc["GasketCoverReqDate"];
                if (!string.IsNullOrWhiteSpace(GasketCoverReqDate))
                {
                    objPCL005.GasketCoverReqDate = DateTime.ParseExact(GasketCoverReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.GasketCoverReqDate = pcl005.GasketCoverReqDate;
                }
                objPCL005.GasketCoverNoFix = pcl005.GasketCoverNoFix;
                objPCL005.GasketCoverAvlQty = pcl005.GasketCoverAvlQty;
                objPCL005.GasketCoverDrg = pcl005.GasketCoverDrg;
                objPCL005.GasketCoverMtrl = pcl005.GasketCoverMtrl;
                objPCL005.GasketCoverFab = pcl005.GasketCoverFab;
                objPCL005.GasketCoverDel = pcl005.GasketCoverDel;
                objPCL005.RunInRunOutPlateReq = pcl005.RunInRunOutPlateReq;
                string RunInRunOutPlateReqDate = fc["RunInRunOutPlateReqDate"];
                if (!string.IsNullOrWhiteSpace(RunInRunOutPlateReqDate))
                {
                    objPCL005.RunInRunOutPlateReqDate = DateTime.ParseExact(RunInRunOutPlateReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.RunInRunOutPlateReqDate = pcl005.RunInRunOutPlateReqDate;
                }
                objPCL005.RunInRunOutPlateNoFix = pcl005.RunInRunOutPlateNoFix;
                objPCL005.RunInRunOutPlateAvlQty = pcl005.RunInRunOutPlateAvlQty;
                objPCL005.RunInRunOutPlateDrg = pcl005.RunInRunOutPlateDrg;
                objPCL005.RunInRunOutPlateMtrl = pcl005.RunInRunOutPlateMtrl;
                objPCL005.RunInRunOutPlateFab = pcl005.RunInRunOutPlateFab;
                objPCL005.RunInRunOutPlateDel = pcl005.RunInRunOutPlateDel;
                objPCL005.SpiderShellReq = pcl005.SpiderShellReq;
                string SpiderShellReqDate = fc["SpiderShellReqDate"];
                if (!string.IsNullOrWhiteSpace(SpiderShellReqDate))
                {
                    objPCL005.SpiderShellReqDate = DateTime.ParseExact(SpiderShellReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.SpiderShellReqDate = pcl005.SpiderShellReqDate;
                }
                objPCL005.SpiderShellNoFix = pcl005.SpiderShellNoFix;
                objPCL005.SpiderShellAvlQty = pcl005.SpiderShellAvlQty;
                objPCL005.SpiderShellDrg = pcl005.SpiderShellDrg;
                objPCL005.SpiderShellMtrl = pcl005.SpiderShellMtrl;
                objPCL005.SpiderShellFab = pcl005.SpiderShellFab;
                objPCL005.SpiderShellDel = pcl005.SpiderShellDel;
                objPCL005.WrapperPlateSkirtReq = pcl005.WrapperPlateSkirtReq;
                string WrapperPlateSkirtReqDate = fc["WrapperPlateSkirtReqDate"];
                if (!string.IsNullOrWhiteSpace(WrapperPlateSkirtReqDate))
                {
                    objPCL005.WrapperPlateSkirtReqDate = DateTime.ParseExact(WrapperPlateSkirtReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.WrapperPlateSkirtReqDate = pcl005.WrapperPlateSkirtReqDate;
                }
                objPCL005.WrapperPlateSkirtNoFix = pcl005.WrapperPlateSkirtNoFix;
                objPCL005.WrapperPlateSkirtAvlQty = pcl005.WrapperPlateSkirtAvlQty;
                objPCL005.WrapperPlateSkirtDrg = pcl005.WrapperPlateSkirtDrg;
                objPCL005.WrapperPlateSkirtMtrl = pcl005.WrapperPlateSkirtMtrl;
                objPCL005.WrapperPlateSkirtFab = pcl005.WrapperPlateSkirtFab;
                objPCL005.WrapperPlateSkirtDel = pcl005.WrapperPlateSkirtDel;
                objPCL005.CounterWeightReq = pcl005.CounterWeightReq;
                string CounterWeightReqDate = fc["CounterWeightReqDate"];
                if (!string.IsNullOrWhiteSpace(CounterWeightReqDate))
                {
                    objPCL005.CounterWeightReqDate = DateTime.ParseExact(CounterWeightReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.CounterWeightReqDate = pcl005.CounterWeightReqDate;
                }
                objPCL005.CounterWeightNoFix = pcl005.CounterWeightNoFix;
                objPCL005.CounterWeightAvlQty = pcl005.CounterWeightAvlQty;
                objPCL005.CounterWeightDrg = pcl005.CounterWeightDrg;
                objPCL005.CounterWeightMtrl = pcl005.CounterWeightMtrl;
                objPCL005.CounterWeightFab = pcl005.CounterWeightFab;
                objPCL005.CounterWeightDel = pcl005.CounterWeightDel;
                objPCL005.FixturePositionerReq = pcl005.FixturePositionerReq;
                string FixturePositionerReqDate = fc["FixturePositionerReqDate"];
                if (!string.IsNullOrWhiteSpace(FixturePositionerReqDate))
                {
                    objPCL005.FixturePositionerReqDate = DateTime.ParseExact(FixturePositionerReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.FixturePositionerReqDate = pcl005.FixturePositionerReqDate;
                }
                objPCL005.FixturePositionerNoFix = pcl005.FixturePositionerNoFix;
                objPCL005.FixturePositionerAvlQty = pcl005.FixturePositionerAvlQty;
                objPCL005.FixturePositionerDrg = pcl005.FixturePositionerDrg;
                objPCL005.FixturePositionerMtrl = pcl005.FixturePositionerMtrl;
                objPCL005.FixturePositionerFab = pcl005.FixturePositionerFab;
                objPCL005.FixturePositionerDel = pcl005.FixturePositionerDel;
                objPCL005.SpiderPWHTReq = pcl005.SpiderPWHTReq;
                string SpiderPWHTReqDate = fc["SpiderPWHTReqDate"];
                if (!string.IsNullOrWhiteSpace(SpiderPWHTReqDate))
                {
                    objPCL005.SpiderPWHTReqDate = DateTime.ParseExact(SpiderPWHTReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.SpiderPWHTReqDate = pcl005.SpiderPWHTReqDate;
                }
                objPCL005.SpiderPWHTNoFix = pcl005.SpiderPWHTNoFix;
                objPCL005.SpiderPWHTAvlQty = pcl005.SpiderPWHTAvlQty;
                objPCL005.SpiderPWHTDrg = pcl005.SpiderPWHTDrg;
                objPCL005.SpiderPWHTMtrl = pcl005.SpiderPWHTMtrl;
                objPCL005.SpiderPWHTFab = pcl005.SpiderPWHTFab;
                objPCL005.SpiderPWHTDel = pcl005.SpiderPWHTDel;
                objPCL005.SaddlePWHTReq = pcl005.SaddlePWHTReq;
                string SaddlePWHTReqDate = fc["SaddlePWHTReqDate"];
                if (!string.IsNullOrWhiteSpace(SaddlePWHTReqDate))
                {
                    objPCL005.SaddlePWHTReqDate = DateTime.ParseExact(SaddlePWHTReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.SaddlePWHTReqDate = pcl005.SaddlePWHTReqDate;
                }
                objPCL005.SaddlePWHTNoFix = pcl005.SaddlePWHTNoFix;
                objPCL005.SaddlePWHTAvlQty = pcl005.SaddlePWHTAvlQty;
                objPCL005.SaddlePWHTDrg = pcl005.SaddlePWHTDrg;
                objPCL005.SaddlePWHTMtrl = pcl005.SaddlePWHTMtrl;
                objPCL005.SaddlePWHTFab = pcl005.SaddlePWHTFab;
                objPCL005.SaddlePWHTDel = pcl005.SaddlePWHTDel;
                objPCL005.FixtureChipBackReq = pcl005.FixtureChipBackReq;
                string FixtureChipBackReqDate = fc["FixtureChipBackReqDate"];
                if (!string.IsNullOrWhiteSpace(FixtureChipBackReqDate))
                {
                    objPCL005.FixtureChipBackReqDate = DateTime.ParseExact(FixtureChipBackReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.FixtureChipBackReqDate = pcl005.FixtureChipBackReqDate;
                }
                objPCL005.FixtureChipBackNoFix = pcl005.FixtureChipBackNoFix;
                objPCL005.FixtureChipBackAvlQty = pcl005.FixtureChipBackAvlQty;
                objPCL005.FixtureChipBackDrg = pcl005.FixtureChipBackDrg;
                objPCL005.FixtureChipBackMtrl = pcl005.FixtureChipBackMtrl;
                objPCL005.FixtureChipBackFab = pcl005.FixtureChipBackFab;
                objPCL005.FixtureChipBackDel = pcl005.FixtureChipBackDel;
                objPCL005.SpiderDEndReq = pcl005.SpiderDEndReq;
                string SpiderDEndReqDate = fc["SpiderDEndReqDate"];
                if (!string.IsNullOrWhiteSpace(SpiderDEndReqDate))
                {
                    objPCL005.SpiderDEndReqDate = DateTime.ParseExact(SpiderDEndReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.SpiderDEndReqDate = pcl005.SpiderDEndReqDate;
                }
                objPCL005.SpiderDEndNoFix = pcl005.SpiderDEndNoFix;
                objPCL005.SpiderDEndAvlQty = pcl005.SpiderDEndAvlQty;
                objPCL005.SpiderDEndDrg = pcl005.SpiderDEndDrg;
                objPCL005.SpiderDEndMtrl = pcl005.SpiderDEndMtrl;
                objPCL005.SpiderDEndFab = pcl005.SpiderDEndFab;
                objPCL005.SpiderDEndDel = pcl005.SpiderDEndDel;
                objPCL005.CClampReq = pcl005.CClampReq;
                string CClampReqDate = fc["CClampReqDate"];
                if (!string.IsNullOrWhiteSpace(CClampReqDate))
                {
                    objPCL005.CClampReqDate = DateTime.ParseExact(CClampReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.CClampReqDate = pcl005.CClampReqDate;
                }
                objPCL005.CClampNoFix = pcl005.CClampNoFix;
                objPCL005.CClampAvlQty = pcl005.CClampAvlQty;
                objPCL005.CClampDrg = pcl005.CClampDrg;
                objPCL005.CClampMtrl = pcl005.CClampMtrl;
                objPCL005.CClampFab = pcl005.CClampFab;
                objPCL005.CClampDel = pcl005.CClampDel;
                objPCL005.FixtInternalInstalReq = pcl005.FixtInternalInstalReq;
                string FixtInternalInstalReqDate = fc["FixtInternalInstalReqDate"];
                if (!string.IsNullOrWhiteSpace(FixtInternalInstalReqDate))
                {
                    objPCL005.FixtInternalInstalReqDate = DateTime.ParseExact(FixtInternalInstalReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.FixtInternalInstalReqDate = pcl005.FixtInternalInstalReqDate;
                }
                objPCL005.FixtInternalInstalNoFix = pcl005.FixtInternalInstalNoFix;
                objPCL005.FixtInternalInstalAvlQty = pcl005.FixtInternalInstalAvlQty;
                objPCL005.FixtInternalInstalDrg = pcl005.FixtInternalInstalDrg;
                objPCL005.FixtInternalInstalMtrl = pcl005.FixtInternalInstalMtrl;
                objPCL005.FixtInternalInstalFab = pcl005.FixtInternalInstalFab;
                objPCL005.FixtInternalInstalDel = pcl005.FixtInternalInstalDel;
                objPCL005.FixtOutletCollectorReq = pcl005.FixtOutletCollectorReq;
                string FixtOutletCollectorReqDate = fc["FixtOutletCollectorReqDate"];
                if (!string.IsNullOrWhiteSpace(FixtOutletCollectorReqDate))
                {
                    objPCL005.FixtOutletCollectorReqDate = DateTime.ParseExact(FixtOutletCollectorReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.FixtOutletCollectorReqDate = pcl005.FixtOutletCollectorReqDate;
                }
                objPCL005.FixtOutletCollectorNoFix = pcl005.FixtOutletCollectorNoFix;
                objPCL005.FixtOutletCollectorAvlQty = pcl005.FixtOutletCollectorAvlQty;
                objPCL005.FixtOutletCollectorDrg = pcl005.FixtOutletCollectorDrg;
                objPCL005.FixtOutletCollectorMtrl = pcl005.FixtOutletCollectorMtrl;
                objPCL005.FixtOutletCollectorFab = pcl005.FixtOutletCollectorFab;
                objPCL005.FixtOutletCollectorDel = pcl005.FixtOutletCollectorDel;
                objPCL005.FixtBeamReq = pcl005.FixtBeamReq;
                string FixtBeamReqDate = fc["FixtBeamReqDate"];
                if (!string.IsNullOrWhiteSpace(FixtBeamReqDate))
                {
                    objPCL005.FixtBeamReqDate = DateTime.ParseExact(FixtBeamReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.FixtBeamReqDate = pcl005.FixtBeamReqDate;
                }
                objPCL005.FixtBeamNoFix = pcl005.FixtBeamNoFix;
                objPCL005.FixtBeamAvlQty = pcl005.FixtBeamAvlQty;
                objPCL005.FixtBeamDrg = pcl005.FixtBeamDrg;
                objPCL005.FixtBeamMtrl = pcl005.FixtBeamMtrl;
                objPCL005.FixtBeamFab = pcl005.FixtBeamFab;
                objPCL005.FixtBeamDel = pcl005.FixtBeamDel;
                objPCL005.EvenerBeamReq = pcl005.EvenerBeamReq;
                string EvenerBeamReqDate = fc["EvenerBeamReqDate"];
                if (!string.IsNullOrWhiteSpace(EvenerBeamReqDate))
                {
                    objPCL005.EvenerBeamReqDate = DateTime.ParseExact(EvenerBeamReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.EvenerBeamReqDate = pcl005.EvenerBeamReqDate;
                }
                objPCL005.EvenerBeamNoFix = pcl005.EvenerBeamNoFix;
                objPCL005.EvenerBeamAvlQty = pcl005.EvenerBeamAvlQty;
                objPCL005.EvenerBeamDrg = pcl005.EvenerBeamDrg;
                objPCL005.EvenerBeamMtrl = pcl005.EvenerBeamMtrl;
                objPCL005.EvenerBeamFab = pcl005.EvenerBeamFab;
                objPCL005.EvenerBeamDel = pcl005.EvenerBeamDel;
                objPCL005.DummyShellDEndReq = pcl005.DummyShellDEndReq;
                string DummyShellDEndReqDate = fc["DummyShellDEndReqDate"];
                if (!string.IsNullOrWhiteSpace(DummyShellDEndReqDate))
                {
                    objPCL005.DummyShellDEndReqDate = DateTime.ParseExact(DummyShellDEndReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.DummyShellDEndReqDate = pcl005.DummyShellDEndReqDate;
                }
                objPCL005.DummyShellDEndNoFix = pcl005.DummyShellDEndNoFix;
                objPCL005.DummyShellDEndAvlQty = pcl005.DummyShellDEndAvlQty;
                objPCL005.DummyShellDEndDrg = pcl005.DummyShellDEndDrg;
                objPCL005.DummyShellDEndMtrl = pcl005.DummyShellDEndMtrl;
                objPCL005.DummyShellDEndFab = pcl005.DummyShellDEndFab;
                objPCL005.DummyShellDEndDel = pcl005.DummyShellDEndDel;
                objPCL005.DummyShellRotationReq = pcl005.DummyShellRotationReq;
                string DummyShellRotationReqDate = fc["DummyShellRotationReqDate"];
                if (!string.IsNullOrWhiteSpace(DummyShellRotationReqDate))
                {
                    objPCL005.DummyShellRotationReqDate = DateTime.ParseExact(DummyShellRotationReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.DummyShellRotationReqDate = pcl005.DummyShellRotationReqDate;
                }
                objPCL005.DummyShellRotationNoFix = pcl005.DummyShellRotationNoFix;
                objPCL005.DummyShellRotationAvlQty = pcl005.DummyShellRotationAvlQty;
                objPCL005.DummyShellRotationDrg = pcl005.DummyShellRotationDrg;
                objPCL005.DummyShellRotationMtrl = pcl005.DummyShellRotationMtrl;
                objPCL005.DummyShellRotationFab = pcl005.DummyShellRotationFab;
                objPCL005.DummyShellRotationDel = pcl005.DummyShellRotationDel;
                objPCL005.NubBuildupReq = pcl005.NubBuildupReq;
                string NubBuildupReqDate = fc["NubBuildupReqDate"];
                if (!string.IsNullOrWhiteSpace(NubBuildupReqDate))
                {
                    objPCL005.NubBuildupReqDate = DateTime.ParseExact(NubBuildupReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.NubBuildupReqDate = pcl005.NubBuildupReqDate;
                }
                objPCL005.NubBuildupNoFix = pcl005.NubBuildupNoFix;
                objPCL005.NubBuildupAvlQty = pcl005.NubBuildupAvlQty;
                objPCL005.NubBuildupDrg = pcl005.NubBuildupDrg;
                objPCL005.NubBuildupMtrl = pcl005.NubBuildupMtrl;
                objPCL005.NubBuildupFab = pcl005.NubBuildupFab;
                objPCL005.NubBuildupDel = pcl005.NubBuildupDel;
                objPCL005.ExtOvalCorrectionReq = pcl005.ExtOvalCorrectionReq;
                string ExtOvalCorrectionReqDate = fc["ExtOvalCorrectionReqDate"];
                if (!string.IsNullOrWhiteSpace(ExtOvalCorrectionReqDate))
                {
                    objPCL005.ExtOvalCorrectionReqDate = DateTime.ParseExact(ExtOvalCorrectionReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.ExtOvalCorrectionReqDate = pcl005.ExtOvalCorrectionReqDate;
                }
                objPCL005.ExtOvalCorrectionNoFix = pcl005.ExtOvalCorrectionNoFix;
                objPCL005.ExtOvalCorrectionAvlQty = pcl005.ExtOvalCorrectionAvlQty;
                objPCL005.ExtOvalCorrectionDrg = pcl005.ExtOvalCorrectionDrg;
                objPCL005.ExtOvalCorrectionMtrl = pcl005.ExtOvalCorrectionMtrl;
                objPCL005.ExtOvalCorrectionFab = pcl005.ExtOvalCorrectionFab;
                objPCL005.ExtOvalCorrectionDel = pcl005.ExtOvalCorrectionDel;
                objPCL005.PreHeatiArrgReq = pcl005.PreHeatiArrgReq;
                string PreHeatiArrgReqDate = fc["PreHeatiArrgReqDate"];
                if (!string.IsNullOrWhiteSpace(PreHeatiArrgReqDate))
                {
                    objPCL005.PreHeatiArrgReqDate = DateTime.ParseExact(PreHeatiArrgReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.PreHeatiArrgReqDate = pcl005.PreHeatiArrgReqDate;
                }
                objPCL005.PreHeatiArrgNoFix = pcl005.PreHeatiArrgNoFix;
                objPCL005.PreHeatiArrgAvlQty = pcl005.PreHeatiArrgAvlQty;
                objPCL005.PreHeatiArrgDrg = pcl005.PreHeatiArrgDrg;
                objPCL005.PreHeatiArrgMtrl = pcl005.PreHeatiArrgMtrl;
                objPCL005.PreHeatiArrgFab = pcl005.PreHeatiArrgFab;
                objPCL005.PreHeatiArrgDel = pcl005.PreHeatiArrgDel;
                objPCL005.FixturePWHTReq = pcl005.FixturePWHTReq;
                string FixturePWHTReqDate = fc["FixturePWHTReqDate"];
                if (!string.IsNullOrWhiteSpace(FixturePWHTReqDate))
                {
                    objPCL005.FixturePWHTReqDate = DateTime.ParseExact(FixturePWHTReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.FixturePWHTReqDate = pcl005.FixturePWHTReqDate;
                }
                objPCL005.FixturePWHTNoFix = pcl005.FixturePWHTNoFix;
                objPCL005.FixturePWHTAvlQty = pcl005.FixturePWHTAvlQty;
                objPCL005.FixturePWHTDrg = pcl005.FixturePWHTDrg;
                objPCL005.FixturePWHTMtrl = pcl005.FixturePWHTMtrl;
                objPCL005.FixturePWHTFab = pcl005.FixturePWHTFab;
                objPCL005.FixturePWHTDel = pcl005.FixturePWHTDel;
                objPCL005.ClosChipReq = pcl005.ClosChipReq;
                string ClosChipReqDate = fc["ClosChipReqDate"];
                if (!string.IsNullOrWhiteSpace(ClosChipReqDate))
                {
                    objPCL005.ClosChipReqDate = DateTime.ParseExact(ClosChipReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.ClosChipReqDate = pcl005.ClosChipReqDate;
                }
                objPCL005.ClosChipNoFix = pcl005.ClosChipNoFix;
                objPCL005.ClosChipAvlQty = pcl005.ClosChipAvlQty;
                objPCL005.ClosChipDrg = pcl005.ClosChipDrg;
                objPCL005.ClosChipMtrl = pcl005.ClosChipMtrl;
                objPCL005.ClosChipFab = pcl005.ClosChipFab;
                objPCL005.ClosChipDel = pcl005.ClosChipDel;
                objPCL005.SleeveStudReq = pcl005.SleeveStudReq;
                string SleeveStudReqDate = fc["SleeveStudReqDate"];
                if (!string.IsNullOrWhiteSpace(SleeveStudReqDate))
                {
                    objPCL005.SleeveStudReqDate = DateTime.ParseExact(SleeveStudReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL005.SleeveStudReqDate = pcl005.SleeveStudReqDate;
                }
                objPCL005.SleeveStudNoFix = pcl005.SleeveStudNoFix;
                objPCL005.SleeveStudAvlQty = pcl005.SleeveStudAvlQty;
                objPCL005.SleeveStudDrg = pcl005.SleeveStudDrg;
                objPCL005.SleeveStudMtrl = pcl005.SleeveStudMtrl;
                objPCL005.SleeveStudFab = pcl005.SleeveStudFab;
                objPCL005.SleeveStudDel = pcl005.SleeveStudDel;

                if (pcl005.LineId > 0)
                {
                    objPCL005.EditedBy = objClsLoginInfo.UserName;
                    objPCL005.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL005.CreatedBy = objClsLoginInfo.UserName;
                    objPCL005.CreatedOn = DateTime.Now;
                    db.PCL005.Add(objPCL005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL005.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveDocument(PCL006 pcl006, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl006.HeaderId).FirstOrDefault();
                PCL006 objPCL006 = new PCL006();
                if (pcl006.LineId > 0)
                {
                    objPCL006 = db.PCL006.Where(x => x.LineId == pcl006.LineId).FirstOrDefault();
                }
                objPCL006.HeaderId = objPCL001.HeaderId;
                objPCL006.Project = objPCL001.Project;
                objPCL006.Document = objPCL001.Document;
                objPCL006.JPPReq = pcl006.JPPReq;
                string JPPReqDate = fc["JPPReqDate"];
                if (!string.IsNullOrWhiteSpace(JPPReqDate))
                {
                    objPCL006.JPPReqDate = DateTime.ParseExact(JPPReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.JPPReqDate = pcl006.JPPReqDate;
                }
                objPCL006.JPPDocNo = pcl006.JPPDocNo;
                string JPPReleasedOn = fc["JPPReleasedOn"];
                if (!string.IsNullOrWhiteSpace(JPPReleasedOn))
                {
                    objPCL006.JPPReleasedOn = DateTime.ParseExact(JPPReleasedOn.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.JPPReleasedOn = pcl006.JPPReleasedOn;
                }
                objPCL006.BoxPlotReq = pcl006.BoxPlotReq;
                string BoxPlotReqDate = fc["BoxPlotReqDate"];
                if (!string.IsNullOrWhiteSpace(BoxPlotReqDate))
                {
                    objPCL006.BoxPlotReqDate = DateTime.ParseExact(BoxPlotReqDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.BoxPlotReqDate = pcl006.BoxPlotReqDate;
                }
                objPCL006.BoxPlotDocNo = pcl006.BoxPlotDocNo;
                string BoxPlotReleasedOn = fc["BoxPlotReleasedOn"] != null ? fc["BoxPlotReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BoxPlotReleasedOn))
                {
                    objPCL006.BoxPlotReleasedOn = DateTime.ParseExact(BoxPlotReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.BoxPlotReleasedOn = pcl006.BoxPlotReleasedOn;
                }
                objPCL006.RollingCapaReq = pcl006.RollingCapaReq;
                string RollingCapaReqDate = fc["RollingCapaReqDate"] != null ? fc["RollingCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReqDate))
                {
                    objPCL006.RollingCapaReqDate = DateTime.ParseExact(RollingCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.RollingCapaReqDate = pcl006.RollingCapaReqDate;
                }

                objPCL006.RollingCapaDocNo = pcl006.RollingCapaDocNo;

                string RollingCapaReleasedOn = fc["RollingCapaReleasedOn"] != null ? fc["RollingCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReleasedOn))
                {
                    objPCL006.RollingCapaReleasedOn = DateTime.ParseExact(RollingCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.RollingCapaReleasedOn = pcl006.RollingCapaReleasedOn;
                }

                objPCL006.ImprovBudgetReq = pcl006.ImprovBudgetReq;
                string ImprovBudgetReqDate = fc["ImprovBudgetReqDate"] != null ? fc["ImprovBudgetReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReqDate))
                {
                    objPCL006.ImprovBudgetReqDate = DateTime.ParseExact(ImprovBudgetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.ImprovBudgetReqDate = pcl006.ImprovBudgetReqDate;
                }
                objPCL006.ImprovBudgetDocNo = pcl006.ImprovBudgetDocNo;
                string ImprovBudgetReleasedOn = fc["ImprovBudgetReleasedOn"] != null ? fc["ImprovBudgetReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReleasedOn))
                {
                    objPCL006.ImprovBudgetReleasedOn = DateTime.ParseExact(ImprovBudgetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.ImprovBudgetReleasedOn = pcl006.ImprovBudgetReleasedOn;
                }

                objPCL006.RCCPBreakUpReq = pcl006.RCCPBreakUpReq;
                string RCCPBreakUpReqDate = fc["RCCPBreakUpReqDate"] != null ? fc["RCCPBreakUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReqDate))
                {
                    objPCL006.RCCPBreakUpReqDate = DateTime.ParseExact(RCCPBreakUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.RCCPBreakUpReqDate = pcl006.RCCPBreakUpReqDate;
                }

                objPCL006.RCCPBreakUpDocNo = pcl006.RCCPBreakUpDocNo;
                string RCCPBreakUpReleasedOn = fc["RCCPBreakUpReleasedOn"] != null ? fc["RCCPBreakUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReleasedOn))
                {
                    objPCL006.RCCPBreakUpReleasedOn = DateTime.ParseExact(RCCPBreakUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.RCCPBreakUpReleasedOn = pcl006.RCCPBreakUpReleasedOn;
                }
                objPCL006.SchedConcertoReq = pcl006.SchedConcertoReq;
                string SchedConcertoReqDate = fc["SchedConcertoReqDate"] != null ? fc["SchedConcertoReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReqDate))
                {
                    objPCL006.SchedConcertoReqDate = DateTime.ParseExact(SchedConcertoReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.SchedConcertoReqDate = pcl006.SchedConcertoReqDate;
                }

                objPCL006.SchedConcertoDocNo = pcl006.SchedConcertoDocNo;
                string SchedConcertoReleasedOn = fc["SchedConcertoReleasedOn"] != null ? fc["SchedConcertoReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReleasedOn))
                {
                    objPCL006.SchedConcertoReleasedOn = DateTime.ParseExact(SchedConcertoReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.SchedConcertoReleasedOn = pcl006.SchedConcertoReleasedOn;
                }


                objPCL006.SchedExlReq = pcl006.SchedExlReq;

                string SchedExlReqDate = fc["SchedExlReqDate"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReqDate))
                {
                    objPCL006.SchedExlReqDate = DateTime.ParseExact(SchedExlReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else { objPCL006.SchedExlReqDate = pcl006.SchedExlReqDate; }
                objPCL006.SchedExlDocNo = pcl006.SchedExlDocNo;
                string SchedExlReleasedOn = fc["SchedExlReleasedOn"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReleasedOn))
                {
                    objPCL006.SchedExlReleasedOn = DateTime.ParseExact(SchedExlReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    objPCL006.SchedExlReleasedOn = pcl006.SchedExlReleasedOn;
                }
                objPCL006.SubcontPlanReq = pcl006.SubcontPlanReq;
                string SubcontPlanReqDate = fc["SubcontPlanReqDate"] != null ? fc["SubcontPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReqDate))
                {
                    objPCL006.SubcontPlanReqDate = DateTime.ParseExact(SubcontPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL006.SubcontPlanReqDate = pcl006.SubcontPlanReqDate; }
                objPCL006.SubcontPlanDocNo = pcl006.SubcontPlanDocNo;

                string SubcontPlanReleasedOn = fc["SubcontPlanReleasedOn"] != null ? fc["SubcontPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReleasedOn))
                {
                    objPCL006.SubcontPlanReleasedOn = DateTime.ParseExact(SubcontPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL006.SubcontPlanReleasedOn = pcl006.SubcontPlanReleasedOn; }

                objPCL006.LongSeamReq = pcl006.LongSeamReq;

                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL006.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL006.LongSeamReqDate = pcl006.LongSeamReqDate; }

                objPCL006.LongSeamDocNo = pcl006.LongSeamDocNo;

                string LongSeamReleasedOn = fc["LongSeamReleasedOn"] != null ? fc["LongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReleasedOn))
                {
                    objPCL006.LongSeamReleasedOn = DateTime.ParseExact(LongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL006.LongSeamReleasedOn = pcl006.LongSeamReleasedOn; }

                objPCL006.CircSeamReq = pcl006.CircSeamReq;

                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL006.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL006.CircSeamReqDate = pcl006.CircSeamReqDate; }

                objPCL006.CircSeamDocNo = pcl006.CircSeamDocNo;
                string CircSeamReleasedOn = fc["CircSeamReleasedOn"] != null ? fc["CircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReleasedOn))
                {
                    objPCL006.CircSeamReleasedOn = DateTime.ParseExact(CircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objPCL006.CircSeamReleasedOn = pcl006.CircSeamReleasedOn; }

                objPCL006.NubBuildUpReq = pcl006.NubBuildUpReq;
                objPCL006.NubBuildUpReqDate = pcl006.NubBuildUpReqDate;
                string NubBuildUpReqDate = fc["NubBuildUpReqDate"] != null ? fc["NubBuildUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NubBuildUpReqDate))
                {
                    objPCL006.NubBuildUpReqDate = DateTime.ParseExact(NubBuildUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL006.NubBuildUpDocNo = pcl006.NubBuildUpDocNo;
                objPCL006.NubBuildUpReleasedOn = pcl006.NubBuildUpReleasedOn;
                string NubBuildUpReleasedOn = fc["NubBuildUpReleasedOn"] != null ? fc["NubBuildUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NubBuildUpReleasedOn))
                {
                    objPCL006.NubBuildUpReleasedOn = DateTime.ParseExact(NubBuildUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.NozzleShellReq = pcl006.NozzleShellReq;
                objPCL006.NozzleShellReqDate = pcl006.NozzleShellReqDate;
                string NozzleShellReqDate = fc["NozzleShellReqDate"] != null ? fc["NozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReqDate))
                {
                    objPCL006.NozzleShellReqDate = DateTime.ParseExact(NozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.NozzleShellDocNo = pcl006.NozzleShellDocNo;
                objPCL006.NozzleShellReleasedOn = pcl006.NozzleShellReleasedOn;
                string NozzleShellReleasedOn = fc["NozzleShellReleasedOn"] != null ? fc["NozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReleasedOn))
                {
                    objPCL006.NozzleShellReleasedOn = DateTime.ParseExact(NozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.RollCapaReq = pcl006.RollCapaReq;
                objPCL006.RollCapaReqDate = pcl006.RollCapaReqDate;
                string RollCapaReqDate = fc["RollCapaReqDate"] != null ? fc["RollCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReqDate))
                {
                    objPCL006.RollCapaReqDate = DateTime.ParseExact(RollCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL006.RollCapaDocNo = pcl006.RollCapaDocNo;
                objPCL006.RollCapaReleasedOn = pcl006.RollCapaReleasedOn;
                string RollCapaReleasedOn = fc["RollCapaReleasedOn"] != null ? fc["RollCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReleasedOn))
                {
                    objPCL006.RollCapaReleasedOn = DateTime.ParseExact(RollCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TablesNozReq = pcl006.TablesNozReq;
                objPCL006.TablesNozReqDate = pcl006.TablesNozReqDate;
                string TablesNozReqDate = fc["TablesNozReqDate"] != null ? fc["TablesNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReqDate))
                {
                    objPCL006.TablesNozReqDate = DateTime.ParseExact(TablesNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TablesNozDocNo = pcl006.TablesNozDocNo;
                objPCL006.TablesNozReleasedOn = pcl006.TablesNozReleasedOn;
                string TablesNozReleasedOn = fc["TablesNozReleasedOn"] != null ? fc["TablesNozReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReleasedOn))
                {
                    objPCL006.TablesNozReleasedOn = DateTime.ParseExact(TablesNozReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TableIntReq = pcl006.TableIntReq;
                objPCL006.TableIntReqDate = pcl006.TableIntReqDate;
                string TableIntReqDate = fc["TableIntReqDate"] != null ? fc["TableIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReqDate))
                {
                    objPCL006.TableIntReqDate = DateTime.ParseExact(TableIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TableIntDocNo = pcl006.TableIntDocNo;
                objPCL006.TableIntReleasedOn = pcl006.TableIntReleasedOn;
                string TableIntReleasedOn = fc["TableIntReleasedOn"] != null ? fc["TableIntReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReleasedOn))
                {
                    objPCL006.TableIntReleasedOn = DateTime.ParseExact(TableIntReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TableExtReq = pcl006.TableExtReq;
                objPCL006.TableExtReqDate = pcl006.TableExtReqDate;
                string TableExtReqDate = fc["TableExtReqDate"] != null ? fc["TableExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReqDate))
                {
                    objPCL006.TableExtReqDate = DateTime.ParseExact(TableExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TableExtDocNo = pcl006.TableExtDocNo;
                objPCL006.TableExtReleasedOn = pcl006.TableExtReleasedOn;
                string TableExtReleasedOn = fc["TableExtReleasedOn"] != null ? fc["TableExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReleasedOn))
                {
                    objPCL006.TableExtReleasedOn = DateTime.ParseExact(TableExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTREdgeReq = pcl006.HTREdgeReq;
                objPCL006.HTREdgeReqDate = pcl006.HTREdgeReqDate;
                string HTREdgeReqDate = fc["HTREdgeReqDate"] != null ? fc["HTREdgeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReqDate))
                {
                    objPCL006.HTREdgeReqDate = DateTime.ParseExact(HTREdgeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTREdgeDocNo = pcl006.HTREdgeDocNo;
                objPCL006.HTREdgeReleasedOn = pcl006.HTREdgeReleasedOn;
                string HTREdgeReleasedOn = fc["HTREdgeReleasedOn"] != null ? fc["HTREdgeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReleasedOn))
                {
                    objPCL006.HTREdgeReleasedOn = DateTime.ParseExact(HTREdgeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRRollReq = pcl006.HTRRollReq;
                objPCL006.HTRRollReqDate = pcl006.HTRRollReqDate;
                string HTRRollReqDate = fc["HTRRollReqDate"] != null ? fc["HTRRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReqDate))
                {
                    objPCL006.HTRRollReqDate = DateTime.ParseExact(HTRRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRRollDocNo = pcl006.HTRRollDocNo;
                objPCL006.HTRRollReleasedOn = pcl006.HTRRollReleasedOn;
                string HTRRollReleasedOn = fc["HTRRollReleasedOn"] != null ? fc["HTRRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReleasedOn))
                {
                    objPCL006.HTRRollReleasedOn = DateTime.ParseExact(HTRRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRReRollReq = pcl006.HTRReRollReq;
                objPCL006.HTRReRollReqDate = pcl006.HTRReRollReqDate;
                string HTRReRollReqDate = fc["HTRReRollReqDate"] != null ? fc["HTRReRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReqDate))
                {
                    objPCL006.HTRReRollReqDate = DateTime.ParseExact(HTRReRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRReRollDocNo = pcl006.HTRReRollDocNo;
                objPCL006.HTRReRollReleasedOn = pcl006.HTRReRollReleasedOn;
                string HTRReRollReleasedOn = fc["HTRReRollReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HTRReRollReleasedOn))
                {
                    objPCL006.HTRReRollReleasedOn = DateTime.ParseExact(HTRReRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.RefLineSketchReq = pcl006.RefLineSketchReq;
                objPCL006.RefLineSketchReqDate = pcl006.RefLineSketchReqDate;
                string RefLineSketchReqDate = fc["RefLineSketchReqDate"];
                if (!string.IsNullOrWhiteSpace(RefLineSketchReqDate))
                {
                    objPCL006.RefLineSketchReqDate = DateTime.ParseExact(RefLineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.RefLineSketchDocNo = pcl006.RefLineSketchDocNo;
                objPCL006.RefLineSketchReleasedOn = pcl006.RefLineSketchReleasedOn;
                string RefLineSketchReleasedOn = fc["RefLineSketchReleasedOn"];
                if (!string.IsNullOrWhiteSpace(RefLineSketchReleasedOn))
                {
                    objPCL006.RefLineSketchReleasedOn = DateTime.ParseExact(RefLineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRISRNozzleShellReq = pcl006.HTRISRNozzleShellReq;
                objPCL006.HTRISRNozzleShellReqDate = pcl006.HTRISRNozzleShellReqDate;
                string HTRISRNozzleShellReqDate = fc["HTRISRNozzleShellReqDate"];
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReqDate))
                {
                    objPCL006.HTRISRNozzleShellReqDate = DateTime.ParseExact(HTRISRNozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRISRNozzleShellDocNo = pcl006.HTRISRNozzleShellDocNo;
                objPCL006.HTRISRNozzleShellReleasedOn = pcl006.HTRISRNozzleShellReleasedOn;
                string HTRISRNozzleShellReleasedOn = fc["HTRISRNozzleShellReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReleasedOn))
                {
                    objPCL006.HTRISRNozzleShellReleasedOn = DateTime.ParseExact(HTRISRNozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRISRNozzleDEndReq = pcl006.HTRISRNozzleDEndReq;
                objPCL006.HTRISRNozzleDEndReqDate = pcl006.HTRISRNozzleDEndReqDate;
                string HTRISRNozzleDEndReqDate = fc["HTRISRNozzleDEndReqDate"];
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReqDate))
                {
                    objPCL006.HTRISRNozzleDEndReqDate = DateTime.ParseExact(HTRISRNozzleDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRISRNozzleDEndDocNo = pcl006.HTRISRNozzleDEndDocNo;
                objPCL006.HTRISRNozzleDEndReleasedOn = pcl006.HTRISRNozzleDEndReleasedOn;
                string HTRISRNozzleDEndReleasedOn = fc["HTRISRNozzleDEndReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReleasedOn))
                {
                    objPCL006.HTRISRNozzleDEndReleasedOn = DateTime.ParseExact(HTRISRNozzleDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRISRNubBuildUpReq = pcl006.HTRISRNubBuildUpReq;
                objPCL006.HTRISRNubBuildUpReqDate = pcl006.HTRISRNubBuildUpReqDate;
                string HTRISRNubBuildUpReqDate = fc["HTRISRNubBuildUpReqDate"];
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReqDate))
                {
                    objPCL006.HTRISRNubBuildUpReqDate = DateTime.ParseExact(HTRISRNubBuildUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRISRNubBuildUpDocNo = pcl006.HTRISRNubBuildUpDocNo;
                objPCL006.HTRISRNubBuildUpReleasedOn = pcl006.HTRISRNubBuildUpReleasedOn;
                string HTRISRNubBuildUpReleasedOn = fc["HTRISRNubBuildUpReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReleasedOn))
                {
                    objPCL006.HTRISRNubBuildUpReleasedOn = DateTime.ParseExact(HTRISRNubBuildUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRPWHTReq = pcl006.HTRPWHTReq;
                objPCL006.HTRPWHTReqDate = pcl006.HTRPWHTReqDate;
                string HTRPWHTReqDate = fc["HTRPWHTReqDate"];
                if (!string.IsNullOrWhiteSpace(HTRPWHTReqDate))
                {
                    objPCL006.HTRPWHTReqDate = DateTime.ParseExact(HTRPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRPWHTDocNo = pcl006.HTRPWHTDocNo;
                objPCL006.HTRPWHTReleasedOn = pcl006.HTRPWHTReleasedOn;
                string HTRPWHTReleasedOn = fc["HTRPWHTReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HTRPWHTReleasedOn))
                {
                    objPCL006.HTRPWHTReleasedOn = DateTime.ParseExact(HTRPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRLSRReq = pcl006.HTRLSRReq;
                objPCL006.HTRLSRReqDate = pcl006.HTRLSRReqDate;
                string HTRLSRReqDate = fc["HTRLSRReqDate"];
                if (!string.IsNullOrWhiteSpace(HTRLSRReqDate))
                {
                    objPCL006.HTRLSRReqDate = DateTime.ParseExact(HTRLSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HTRLSRDocNo = pcl006.HTRLSRDocNo;
                objPCL006.HTRLSRReleasedOn = pcl006.HTRLSRReleasedOn;
                string HTRLSRReleasedOn = fc["HTRLSRReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HTRLSRReleasedOn))
                {
                    objPCL006.HTRLSRReleasedOn = DateTime.ParseExact(HTRLSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HandlingPlanReq = pcl006.HandlingPlanReq;
                objPCL006.HandlingPlanReqDate = pcl006.HandlingPlanReqDate;
                string HandlingPlanReqDate = fc["HandlingPlanReqDate"];
                if (!string.IsNullOrWhiteSpace(HandlingPlanReqDate))
                {
                    objPCL006.HandlingPlanReqDate = DateTime.ParseExact(HandlingPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HandlingPlanDocNo = pcl006.HandlingPlanDocNo;
                objPCL006.HandlingPlanReleasedOn = pcl006.HandlingPlanReleasedOn;
                string HandlingPlanReleasedOn = fc["HandlingPlanReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HandlingPlanReleasedOn))
                {
                    objPCL006.HandlingPlanReleasedOn = DateTime.ParseExact(HandlingPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.LocationTankRotatorReq = pcl006.LocationTankRotatorReq;
                objPCL006.LocationTankRotatorReqDate = pcl006.LocationTankRotatorReqDate;
                string LocationTankRotatorReqDate = fc["LocationTankRotatorReqDate"];
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReqDate))
                {
                    objPCL006.LocationTankRotatorReqDate = DateTime.ParseExact(LocationTankRotatorReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.LocationTankRotatorDocNo = pcl006.LocationTankRotatorDocNo;
                objPCL006.LocationTankRotatorReleasedOn = pcl006.LocationTankRotatorReleasedOn;
                string LocationTankRotatorReleasedOn = fc["LocationTankRotatorReleasedOn"];
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReleasedOn))
                {
                    objPCL006.LocationTankRotatorReleasedOn = DateTime.ParseExact(LocationTankRotatorReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HydrotestReq = pcl006.HydrotestReq;
                objPCL006.HydrotestReqDate = pcl006.HydrotestReqDate;
                string HydrotestReqDate = fc["HydrotestReqDate"];
                if (!string.IsNullOrWhiteSpace(HydrotestReqDate))
                {
                    objPCL006.HydrotestReqDate = DateTime.ParseExact(HydrotestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.HydrotestDocNo = pcl006.HydrotestDocNo;
                objPCL006.HydrotestReleasedOn = pcl006.HydrotestReleasedOn;
                string HydrotestReleasedOn = fc["HydrotestReleasedOn"];
                if (!string.IsNullOrWhiteSpace(HydrotestReleasedOn))
                {
                    objPCL006.HydrotestReleasedOn = DateTime.ParseExact(HydrotestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TubeSheetReq = pcl006.TubeSheetReq;
                objPCL006.TubeSheetReqDate = pcl006.TubeSheetReqDate;
                string TubeSheetReqDate = fc["TubeSheetReqDate"];
                if (!string.IsNullOrWhiteSpace(TubeSheetReqDate))
                {
                    objPCL006.TubeSheetReqDate = DateTime.ParseExact(TubeSheetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.TubeSheetDocNo = pcl006.TubeSheetDocNo;
                objPCL006.TubeSheetReleasedOn = pcl006.TubeSheetReleasedOn;
                string TubeSheetReleasedOn = fc["TubeSheetReleasedOn"];
                if (!string.IsNullOrWhiteSpace(TubeSheetReleasedOn))
                {
                    objPCL006.TubeSheetReleasedOn = DateTime.ParseExact(TubeSheetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.BaffleTubeReq = pcl006.BaffleTubeReq;
                objPCL006.BaffleTubeReqDate = pcl006.BaffleTubeReqDate;
                string BaffleTubeReqDate = fc["BaffleTubeReqDate"];
                if (!string.IsNullOrWhiteSpace(BaffleTubeReqDate))
                {
                    objPCL006.BaffleTubeReqDate = DateTime.ParseExact(BaffleTubeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.BaffleTubeDocNo = pcl006.BaffleTubeDocNo;
                objPCL006.BaffleTubeReleasedOn = pcl006.BaffleTubeReleasedOn;
                string BaffleTubeReleasedOn = fc["BaffleTubeReleasedOn"];
                if (!string.IsNullOrWhiteSpace(BaffleTubeReleasedOn))
                {
                    objPCL006.BaffleTubeReleasedOn = DateTime.ParseExact(BaffleTubeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.SurfaceTreatRequestReq = pcl006.SurfaceTreatRequestReq;
                objPCL006.SurfaceTreatRequestReqDate = pcl006.SurfaceTreatRequestReqDate;
                string SurfaceTreatRequestReqDate = fc["SurfaceTreatRequestReqDate"];
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReqDate))
                {
                    objPCL006.SurfaceTreatRequestReqDate = DateTime.ParseExact(SurfaceTreatRequestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL006.SurfaceTreatRequestDocNo = pcl006.SurfaceTreatRequestDocNo;
                objPCL006.SurfaceTreatRequestReleasedOn = pcl006.SurfaceTreatRequestReleasedOn;
                string SurfaceTreatRequestReleasedOn = fc["SurfaceTreatRequestReleasedOn"];
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReleasedOn))
                {
                    objPCL006.SurfaceTreatRequestReleasedOn = DateTime.ParseExact(SurfaceTreatRequestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }


                if (pcl006.LineId > 0)
                {
                    objPCL006.EditedBy = objClsLoginInfo.UserName;
                    objPCL006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL006.CreatedBy = objClsLoginInfo.UserName;
                    objPCL006.CreatedOn = DateTime.Now;
                    db.PCL006.Add(objPCL006);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL006.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveDim(PCL007 pcl007, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl007.HeaderId).FirstOrDefault();
                PCL007 objPCL007 = new PCL007();
                if (pcl007.LineId > 0)
                {
                    objPCL007 = db.PCL007.Where(x => x.LineId == pcl007.LineId).FirstOrDefault();
                }
                objPCL007.HeaderId = objPCL001.HeaderId;
                objPCL007.Project = objPCL001.Project;
                objPCL007.Document = objPCL001.Document;
                objPCL007.RollDataReq = pcl007.RollDataReq;
                objPCL007.RollDataReqDate = pcl007.RollDataReqDate;
                string RollDataReqDate = fc["RollDataReqDate"] != null ? fc["RollDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReqDate))
                {
                    objPCL007.RollDataReqDate = DateTime.ParseExact(RollDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.RollDataDocNo = pcl007.RollDataDocNo;
                objPCL007.RollDataReleasedOn = pcl007.RollDataReleasedOn;
                string RollDataReleasedOn = fc["RollDataReleasedOn"] != null ? fc["RollDataReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReleasedOn))
                {
                    objPCL007.RollDataReleasedOn = DateTime.ParseExact(RollDataReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ShellLongReq = pcl007.ShellLongReq;
                objPCL007.ShellLongReqDate = pcl007.ShellLongReqDate;
                string ShellLongReqDate = fc["ShellLongReqDate"] != null ? fc["ShellLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReqDate))
                {
                    objPCL007.ShellLongReqDate = DateTime.ParseExact(ShellLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ShellLongDocNo = pcl007.ShellLongDocNo;
                objPCL007.ShellLongReleasedOn = pcl007.ShellLongReleasedOn;
                string ShellLongReleasedOn = fc["ShellLongReleasedOn"] != null ? fc["ShellLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReleasedOn))
                {
                    objPCL007.ShellLongReleasedOn = DateTime.ParseExact(ShellLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ESSCShellReq = pcl007.ESSCShellReq;
                objPCL007.ESSCShellReqDate = pcl007.ESSCShellReqDate;
                string ESSCShellReqDate = fc["ESSCShellReqDate"] != null ? fc["ESSCShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReqDate))
                {
                    objPCL007.ESSCShellReqDate = DateTime.ParseExact(ESSCShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ESSCShellDocNo = pcl007.ESSCShellDocNo;
                objPCL007.ESSCShellReleasedOn = pcl007.ESSCShellReleasedOn;
                string ESSCShellReleasedOn = fc["ESSCShellReleasedOn"] != null ? fc["ESSCShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReleasedOn))
                {
                    objPCL007.ESSCShellReleasedOn = DateTime.ParseExact(ESSCShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ESSCDEndReq = pcl007.ESSCDEndReq;
                objPCL007.ESSCDEndReqDate = pcl007.ESSCDEndReqDate;
                string ESSCDEndReqDate = fc["ESSCDEndReqDate"] != null ? fc["ESSCDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCDEndReqDate))
                {
                    objPCL007.ESSCDEndReqDate = DateTime.ParseExact(ESSCDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ESSCDEndDocNo = pcl007.ESSCDEndDocNo;
                objPCL007.ESSCDEndReleasedOn = pcl007.ESSCDEndReleasedOn;
                string ESSCDEndReleasedOn = fc["ESSCDEndReleasedOn"] != null ? fc["ESSCDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCDEndReleasedOn))
                {
                    objPCL007.ESSCDEndReleasedOn = DateTime.ParseExact(ESSCDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ChangeOvalityExtReq = pcl007.ChangeOvalityExtReq;
                objPCL007.ChangeOvalityExtReqDate = pcl007.ChangeOvalityExtReqDate;
                string ChangeOvalityExtReqDate = fc["ChangeOvalityExtReqDate"] != null ? fc["ChangeOvalityExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReqDate))
                {
                    objPCL007.ChangeOvalityExtReqDate = DateTime.ParseExact(ChangeOvalityExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ChangeOvalityExtDocNo = pcl007.ChangeOvalityExtDocNo;
                objPCL007.ChangeOvalityExtReleasedOn = pcl007.ChangeOvalityExtReleasedOn;
                string ChangeOvalityExtReleasedOn = fc["ChangeOvalityExtReleasedOn"] != null ? fc["ChangeOvalityExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReleasedOn))
                {
                    objPCL007.ChangeOvalityExtReleasedOn = DateTime.ParseExact(ChangeOvalityExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ChangeOvalityNubReq = pcl007.ChangeOvalityNubReq;
                objPCL007.ChangeOvalityNubReqDate = pcl007.ChangeOvalityNubReqDate;
                string ChangeOvalityNubReqDate = fc["ChangeOvalityNubReqDate"] != null ? fc["ChangeOvalityNubReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityNubReqDate))
                {
                    objPCL007.ChangeOvalityNubReqDate = DateTime.ParseExact(ChangeOvalityNubReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ChangeOvalityNubDocNo = pcl007.ChangeOvalityNubDocNo;

                objPCL007.ChangeOvalityNubReleasedOn = pcl007.ChangeOvalityNubReleasedOn;
                string ChangeOvalityNubReleasedOn = fc["ChangeOvalityNubReleasedOn"] != null ? fc["ChangeOvalityNubReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityNubReleasedOn))
                {
                    objPCL007.ChangeOvalityNubReleasedOn = DateTime.ParseExact(ChangeOvalityNubReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ShrinkTSRReq = pcl007.ShrinkTSRReq;
                objPCL007.ShrinkTSRReqDate = pcl007.ShrinkTSRReqDate;
                string ShrinkTSRReqDate = fc["ShrinkTSRReqDate"] != null ? fc["ShrinkTSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReqDate))
                {
                    objPCL007.ShrinkTSRReqDate = DateTime.ParseExact(ShrinkTSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ShrinkTSRDocNo = pcl007.ShrinkTSRDocNo;
                objPCL007.ShrinkTSRReleasedOn = pcl007.ShrinkTSRReleasedOn;
                string ShrinkTSRReleasedOn = fc["ShrinkTSRReleasedOn"] != null ? fc["ShrinkTSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReleasedOn))
                {
                    objPCL007.ShrinkTSRReleasedOn = DateTime.ParseExact(ShrinkTSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ChangeOvalityISRReq = pcl007.ChangeOvalityISRReq;
                objPCL007.ChangeOvalityISRReqDate = pcl007.ChangeOvalityISRReqDate;
                string ChangeOvalityISRReqDate = fc["ChangeOvalityISRReqDate"] != null ? fc["ChangeOvalityISRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReqDate))
                {
                    objPCL007.ChangeOvalityISRReqDate = DateTime.ParseExact(ChangeOvalityISRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.ChangeOvalityISRDocNo = pcl007.ChangeOvalityISRDocNo;
                objPCL007.ChangeOvalityISRReleasedOn = pcl007.ChangeOvalityISRReleasedOn;
                string ChangeOvalityISRReleasedOn = fc["ChangeOvalityISRReleasedOn"] != null ? fc["ChangeOvalityISRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReleasedOn))
                {
                    objPCL007.ChangeOvalityISRReleasedOn = DateTime.ParseExact(ChangeOvalityISRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.DEendLongSeamReq = pcl007.DEendLongSeamReq;
                objPCL007.DEendLongSeamReqDate = pcl007.DEendLongSeamReqDate;
                string DEendLongSeamReqDate = fc["DEendLongSeamReqDate"] != null ? fc["DEendLongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReqDate))
                {
                    objPCL007.DEendLongSeamReqDate = DateTime.ParseExact(DEendLongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL007.DEendLongSeamDocNo = pcl007.DEendLongSeamDocNo;
                objPCL007.DEendLongSeamReleasedOn = pcl007.DEendLongSeamReleasedOn;
                string DEendLongSeamReleasedOn = fc["DEendLongSeamReleasedOn"] != null ? fc["DEendLongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReleasedOn))
                {
                    objPCL007.DEendLongSeamReleasedOn = DateTime.ParseExact(DEendLongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }


                if (pcl007.LineId > 0)
                {
                    objPCL007.EditedBy = objClsLoginInfo.UserName;
                    objPCL007.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL007.CreatedBy = objClsLoginInfo.UserName;
                    objPCL007.CreatedOn = DateTime.Now;
                    db.PCL007.Add(objPCL007);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL007.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region  Urea Rx Check List 

        [HttpPost]
        public ActionResult SaveUreaConstructor(PCL008 pcl008, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl008.HeaderId).FirstOrDefault();
                PCL008 objPCL008 = new PCL008();
                if (pcl008.LineId > 0)
                {
                    objPCL008 = db.PCL008.Where(x => x.LineId == pcl008.LineId).FirstOrDefault();
                }

                objPCL008.HeaderId = objPCL001.HeaderId;
                objPCL008.Project = objPCL001.Project;
                objPCL008.Document = objPCL001.Document;
                #region Objects
                objPCL008.SectionWtLessVal = pcl008.SectionWtLessVal;
                objPCL008.BMID = pcl008.BMID;
                objPCL008.ReactorThk = pcl008.ReactorThk;
                objPCL008.ReactorTLTL = pcl008.ReactorTLTL;
                objPCL008.ReactorLg = pcl008.ReactorLg;
                objPCL008.ReactorFabWt = pcl008.ReactorFabWt;
                objPCL008.ReactorHydroWt = pcl008.ReactorHydroWt;
                objPCL008.ReactorHydroPrs = pcl008.ReactorHydroPrs;
                objPCL008.ReactorMaxShellWd = pcl008.ReactorMaxShellWd;
                objPCL008.ReactorJobMaterial = pcl008.ReactorJobMaterial;
                objPCL008.ReactoreDesigned = pcl008.ReactoreDesigned;
                objPCL008.ShellConst1Seg = pcl008.ShellConst1Seg;
                objPCL008.ShellConst2Seg = pcl008.ShellConst2Seg;
                objPCL008.ShellConstMorethan2Seg = pcl008.ShellConstMorethan2Seg;
                objPCL008.ShellConstForgedShell = pcl008.ShellConstForgedShell;
                objPCL008.TypeRollCold = pcl008.TypeRollCold;
                objPCL008.TypeRollWarm = pcl008.TypeRollWarm;
                objPCL008.TypeRollHot = pcl008.TypeRollHot;
                objPCL008.GasCuttingPreHeat = pcl008.GasCuttingPreHeat;
                objPCL008.GasCuttingPreHeatNR = pcl008.GasCuttingPreHeatNR;
                objPCL008.CSeamWEPPlate = pcl008.CSeamWEPPlate;
                objPCL008.CSeamWEPAfter = pcl008.CSeamWEPAfter;
                objPCL008.PTCRequiredforLS = pcl008.PTCRequiredforLS;
                objPCL008.PTCRequiredforCS = pcl008.PTCRequiredforCS;
                objPCL008.PTCRequiredforDend = pcl008.PTCRequiredforDend;
                objPCL008.PTCRequiredforLiner = pcl008.PTCRequiredforLiner;
                objPCL008.PTCRequiredforLSCSESSC = pcl008.PTCRequiredforLSCSESSC;
                objPCL008.ISRRequiredforLS = pcl008.ISRRequiredforLS;
                objPCL008.ISRRequiredforCS = pcl008.ISRRequiredforCS;
                objPCL008.ISRRequiredforNozzle = pcl008.ISRRequiredforNozzle;
                objPCL008.ISRRequiredforClosing = pcl008.ISRRequiredforClosing;
                objPCL008.ISRRequiredforNA = pcl008.ISRRequiredforNA;
                objPCL008.PTMTAfterWelding = pcl008.PTMTAfterWelding;
                objPCL008.PTMTAfterOverlay = pcl008.PTMTAfterOverlay;
                objPCL008.PTMTAfterPWHT = pcl008.PTMTAfterPWHT;
                objPCL008.PTMTAfterHydro = pcl008.PTMTAfterHydro;
                objPCL008.RTAfterWelding = pcl008.RTAfterWelding;
                objPCL008.RTAfterOverlay = pcl008.RTAfterOverlay;
                objPCL008.RTAfterPWHT = pcl008.RTAfterPWHT;
                objPCL008.RTAfterHydro = pcl008.RTAfterHydro;
                objPCL008.UTAfterRecordable = pcl008.UTAfterRecordable;
                objPCL008.UTAfterConventional = pcl008.UTAfterConventional;
                objPCL008.UTAfterWelding = pcl008.UTAfterWelding;
                objPCL008.UTAfterOverlay = pcl008.UTAfterOverlay;
                objPCL008.UTAfterPWHT = pcl008.UTAfterPWHT;
                objPCL008.UTAfterHydro = pcl008.UTAfterHydro;
                objPCL008.FerAfterWelding = pcl008.FerAfterWelding;
                objPCL008.FerAfterOverlay = pcl008.FerAfterOverlay;
                objPCL008.FerAfterPWHT = pcl008.FerAfterPWHT;
                objPCL008.FerAfterHydro = pcl008.FerAfterHydro;
                objPCL008.ShellLSSingle = pcl008.ShellLSSingle;
                objPCL008.ShellLSDouble = pcl008.ShellLSDouble;
                objPCL008.ShellLSManual = pcl008.ShellLSManual;
                objPCL008.ShellLSManualESSC = pcl008.ShellLSManualESSC;
                objPCL008.DCPetalWCrown = pcl008.DCPetalWCrown;
                objPCL008.DC4PetalWoCrown = pcl008.DC4PetalWoCrown;
                objPCL008.DCSinglePeice = pcl008.DCSinglePeice;
                objPCL008.DCTwoHalves = pcl008.DCTwoHalves;
                objPCL008.DRCFormedPetals = pcl008.DRCFormedPetals;
                objPCL008.DRCFormedSetup = pcl008.DRCFormedSetup;
                objPCL008.DRCFormedSingle = pcl008.DRCFormedSingle;
                objPCL008.DOTESSCSingle = pcl008.DOTESSCSingle;
                objPCL008.DOTESSCDouble = pcl008.DOTESSCDouble;
                objPCL008.DOTManual = pcl008.DOTManual;
                objPCL008.DOTFCAW = pcl008.DOTFCAW;
                objPCL008.SVJYring = pcl008.SVJYring;
                objPCL008.SVJBuildup = pcl008.SVJBuildup;
                objPCL008.SVJLapJoint = pcl008.SVJLapJoint;
                objPCL008.NLTForged = pcl008.NLTForged;
                objPCL008.NLTPlate = pcl008.NLTPlate;
                objPCL008.TSRRing = pcl008.TSRRing;
                objPCL008.TSRLS = pcl008.TSRLS;
                objPCL008.TSRPlate = pcl008.TSRPlate;
                objPCL008.TSRWeldBf = pcl008.TSRWeldBf;
                objPCL008.TSRSSPlate = pcl008.TSRSSPlate;
                objPCL008.TSRWeldAf = pcl008.TSRWeldAf;
                objPCL008.TSRNotReq = pcl008.TSRNotReq;
                objPCL008.NFFRaised = pcl008.NFFRaised;
                objPCL008.NFFGroove = pcl008.NFFGroove;
                objPCL008.NFFOther = pcl008.NFFOther;
                objPCL008.NGFBfPWHT = pcl008.NGFBfPWHT;
                objPCL008.NGFAfPWHT = pcl008.NGFAfPWHT;
                objPCL008.SpoolMatSS = pcl008.SpoolMatSS;
                objPCL008.SpoolMatUrea = pcl008.SpoolMatUrea;
                objPCL008.JSInConnel = pcl008.JSInConnel;
                objPCL008.JSBiMetalic = pcl008.JSBiMetalic;
                objPCL008.JSSSUrea = pcl008.JSSSUrea;
                objPCL008.TSRBfPWHT = pcl008.TSRBfPWHT;
                objPCL008.TSRAfPWHT = pcl008.TSRAfPWHT;
                objPCL008.TSRBoth = pcl008.TSRBoth;
                objPCL008.VessleSSkirt = pcl008.VessleSSkirt;
                objPCL008.VessleSSupp = pcl008.VessleSSupp;
                objPCL008.VessleSLegs = pcl008.VessleSLegs;
                objPCL008.VessleSSaddle = pcl008.VessleSSaddle;
                objPCL008.SkirtMadein1 = pcl008.SkirtMadein1;
                objPCL008.SkirtMadein2 = pcl008.SkirtMadein2;
                objPCL008.FPNWeldNuts = pcl008.FPNWeldNuts;
                objPCL008.FPNLeaveRow = pcl008.FPNLeaveRow;
                objPCL008.PWHTSinglePiece = pcl008.PWHTSinglePiece;
                objPCL008.PWHTLSRJoint = pcl008.PWHTLSRJoint;
                objPCL008.PTCMTCptc = pcl008.PTCMTCptc;
                objPCL008.PTCMTCmtc = pcl008.PTCMTCmtc;
                objPCL008.PTCMTCptcSim = pcl008.PTCMTCptcSim;
                objPCL008.PTCMTCmtcSim = pcl008.PTCMTCmtcSim;
                objPCL008.SaddlesTransSadd = pcl008.SaddlesTransSadd;
                objPCL008.SaddlesAdditional = pcl008.SaddlesAdditional;
                objPCL008.PWHTinFurnacePFS = pcl008.PWHTinFurnacePFS;
                objPCL008.PWHTinFurnaceHFS1 = pcl008.PWHTinFurnaceHFS1;
                objPCL008.PWHTinFurnaceLong = pcl008.PWHTinFurnaceLong;
                objPCL008.PWHTinFurnaceTall = pcl008.PWHTinFurnaceTall;
                objPCL008.SectionWtMore230 = pcl008.SectionWtMore230;
                objPCL008.SectionWt200To230 = pcl008.SectionWt200To230;
                objPCL008.SectionWt150To200 = pcl008.SectionWt150To200;
                objPCL008.SectionWtLess150 = pcl008.SectionWtLess150;
                objPCL008.SectionWtBlankSec = pcl008.SectionWtBlankSec;
                objPCL008.ARMRequired = pcl008.ARMRequired;
                objPCL008.ARMRT = pcl008.ARMRT;
                objPCL008.ARMPWHT = pcl008.ARMPWHT;
                objPCL008.ARMSB = pcl008.ARMSB;
                objPCL008.CLRH50ppm = pcl008.CLRH50ppm;
                objPCL008.CLRH510ppm = pcl008.CLRH510ppm;
                objPCL008.CLRHNoLimit = pcl008.CLRHNoLimit;
                objPCL008.TemplateForReq = pcl008.TemplateForReq;
                objPCL008.TemplateForNReq = pcl008.TemplateForNReq;
                objPCL008.N2FillReq = pcl008.N2FillReq;
                objPCL008.N2FillNotReq = pcl008.N2FillNotReq;
                objPCL008.N2FillOther = pcl008.N2FillOther;
                objPCL008.ESTPlate = pcl008.ESTPlate;
                objPCL008.ESTShell = pcl008.ESTShell;
                objPCL008.ESTSection = pcl008.ESTSection;
                objPCL008.ESTEquip = pcl008.ESTEquip;
                objPCL008.ISTPlate = pcl008.ISTPlate;
                objPCL008.ISTShell = pcl008.ISTShell;
                objPCL008.ISTSection = pcl008.ISTSection;
                objPCL008.ISTEquip = pcl008.ISTEquip;
                objPCL008.ISTAfBfPWHT = pcl008.ISTAfBfPWHT;
                objPCL008.ISTAfHydro = pcl008.ISTAfHydro;
                objPCL008.PickPassReq = pcl008.PickPassReq;
                objPCL008.MaxSBLess1_38 = pcl008.MaxSBLess1_38;
                objPCL008.MaxSBLess1_78 = pcl008.MaxSBLess1_78;
                objPCL008.MaxSBLess2_14 = pcl008.MaxSBLess2_14;
                objPCL008.MaxSBLess4 = pcl008.MaxSBLess4;
                objPCL008.MaxSBMore4 = pcl008.MaxSBMore4;

                #endregion
                if (pcl008.LineId > 0)
                {
                    objPCL008.EditedBy = objClsLoginInfo.UserName;
                    objPCL008.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL008.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL008.CreatedBy = objClsLoginInfo.UserName;
                    objPCL008.CreatedOn = DateTime.Now;
                    db.PCL008.Add(objPCL008);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL008.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUreaAllowance(PCL009 pcl009, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl009.HeaderId).FirstOrDefault();
                PCL009 objPCL009 = new PCL009();
                if (pcl009.LineId > 0)
                {
                    objPCL009 = db.PCL009.Where(x => x.LineId == pcl009.LineId).FirstOrDefault();
                }
                objPCL009.HeaderId = objPCL001.HeaderId;
                objPCL009.Project = objPCL001.Project;
                objPCL009.Document = objPCL001.Document;

                objPCL009.ShellAllowanceEB = pcl009.ShellAllowanceEB;
                objPCL009.ShellTolerance = pcl009.ShellTolerance;
                objPCL009.ShellAllowanceLong = pcl009.ShellAllowanceLong;
                objPCL009.ShellAllowanceCirc = pcl009.ShellAllowanceCirc;
                objPCL009.ShellNegAllowanceCirc = pcl009.ShellNegAllowanceCirc;
                objPCL009.DEndMcing = pcl009.DEndMcing;
                objPCL009.DEndSeamShrinkage = pcl009.DEndSeamShrinkage;
                objPCL009.DEndESSC = pcl009.DEndESSC;

                if (pcl009.LineId > 0)
                {
                    objPCL009.EditedBy = objClsLoginInfo.UserName;
                    objPCL009.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL009.CreatedBy = objClsLoginInfo.UserName;
                    objPCL009.CreatedOn = DateTime.Now;
                    db.PCL009.Add(objPCL009);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL009.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUreaScopeOfWork(PCL010 pcl010, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl010.HeaderId).FirstOrDefault();
                PCL010 objPCL010 = new PCL010();
                bool IsEdited = false;
                if (pcl010.LineId > 0)
                {
                    objPCL010 = db.PCL010.Where(x => x.LineId == pcl010.LineId).FirstOrDefault();
                    IsEdited = true;
                }
                objPCL010.HeaderId = objPCL001.HeaderId;
                objPCL010.Project = objPCL001.Project;
                objPCL010.Document = objPCL001.Document;
                objPCL010.EquipmentNotReq = pcl010.EquipmentNotReq;
                objPCL010.EquipmentShop = pcl010.EquipmentShop;
                objPCL010.EquipmentMC = pcl010.EquipmentMC;
                objPCL010.EquipmentLEMF = pcl010.EquipmentLEMF;
                objPCL010.EquipmentRNW = pcl010.EquipmentRNW;
                objPCL010.EquipmentOutside = pcl010.EquipmentOutside;
                objPCL010.DEndFormingNotReq = pcl010.DEndFormingNotReq;
                objPCL010.DEndFormingShop = pcl010.DEndFormingShop;
                objPCL010.DEndFormingMC = pcl010.DEndFormingMC;
                objPCL010.DEndFormingLEMF = pcl010.DEndFormingLEMF;
                objPCL010.DEndFormingRNW = pcl010.DEndFormingRNW;
                objPCL010.DEndFormingOutside = pcl010.DEndFormingOutside;
                objPCL010.DEndSetupNotReq = pcl010.DEndSetupNotReq;
                objPCL010.DEndSetupShop = pcl010.DEndSetupShop;
                objPCL010.DEndSetupMC = pcl010.DEndSetupMC;
                objPCL010.DEndSetupLEMF = pcl010.DEndSetupLEMF;
                objPCL010.DEndSetupRNW = pcl010.DEndSetupRNW;
                objPCL010.DEndSetupOutside = pcl010.DEndSetupOutside;
                objPCL010.DEndWeldingNotReq = pcl010.DEndWeldingNotReq;
                objPCL010.DEndWeldingShop = pcl010.DEndWeldingShop;
                objPCL010.DEndWeldingMC = pcl010.DEndWeldingMC;
                objPCL010.DEndWeldingLEMF = pcl010.DEndWeldingLEMF;
                objPCL010.DEndWeldingRNW = pcl010.DEndWeldingRNW;
                objPCL010.DEndWeldingOutside = pcl010.DEndWeldingOutside;
                objPCL010.SkirtNotReq = pcl010.SkirtNotReq;
                objPCL010.SkirtShop = pcl010.SkirtShop;
                objPCL010.SkirtMC = pcl010.SkirtMC;
                objPCL010.SkirtLEMF = pcl010.SkirtLEMF;
                objPCL010.SkirtRNW = pcl010.SkirtRNW;
                objPCL010.SkirtOutside = pcl010.SkirtOutside;
                objPCL010.YRingMachiningNotReq = pcl010.YRingMachiningNotReq;
                objPCL010.YRingMachiningShop = pcl010.YRingMachiningShop;
                objPCL010.YRingMachiningMC = pcl010.YRingMachiningMC;
                objPCL010.YRingMachiningLEMF = pcl010.YRingMachiningLEMF;
                objPCL010.YRingMachiningRNW = pcl010.YRingMachiningRNW;
                objPCL010.YRingMachiningOutside = pcl010.YRingMachiningOutside;
                objPCL010.NozMachiningNotReq = pcl010.NozMachiningNotReq;
                objPCL010.NozMachiningShop = pcl010.NozMachiningShop;
                objPCL010.NozMachiningMC = pcl010.NozMachiningMC;
                objPCL010.NozMachiningLEMF = pcl010.NozMachiningLEMF;
                objPCL010.NozMachiningRNW = pcl010.NozMachiningRNW;
                objPCL010.NozMachiningOutside = pcl010.NozMachiningOutside;
                objPCL010.NozOverlayNotReq = pcl010.NozOverlayNotReq;
                objPCL010.NozOverlayMC = pcl010.NozOverlayMC;
                objPCL010.NozOverlayShop = pcl010.NozOverlayShop;
                objPCL010.NozOverlayLEMF = pcl010.NozOverlayLEMF;
                objPCL010.NozOverlayRNW = pcl010.NozOverlayRNW;
                objPCL010.NozOverlayOutside = pcl010.NozOverlayOutside;
                objPCL010.ManOverlayNotReq = pcl010.ManOverlayNotReq;
                objPCL010.ManOverlayShop = pcl010.ManOverlayShop;
                objPCL010.ManOverlayMC = pcl010.ManOverlayMC;
                objPCL010.ManOverlayLEMF = pcl010.ManOverlayLEMF;
                objPCL010.ManOverlayRNW = pcl010.ManOverlayRNW;
                objPCL010.ManOverlayOutside = pcl010.ManOverlayOutside;
                objPCL010.NozFinMachiningNotReq = pcl010.NozFinMachiningNotReq;
                objPCL010.NozFinMachiningShop = pcl010.NozFinMachiningShop;
                objPCL010.NozFinMachiningMC = pcl010.NozFinMachiningMC;
                objPCL010.NozFinMachiningLEMF = pcl010.NozFinMachiningLEMF;
                objPCL010.NozFinMachiningRNW = pcl010.NozFinMachiningRNW;
                objPCL010.NozFinMachiningOutside = pcl010.NozFinMachiningOutside;
                objPCL010.NozWEPNotReq = pcl010.NozWEPNotReq;
                objPCL010.NozWEPShop = pcl010.NozWEPShop;
                objPCL010.NozWEPMC = pcl010.NozWEPMC;
                objPCL010.NozWEPLEMF = pcl010.NozWEPLEMF;
                objPCL010.NozWEPRNW = pcl010.NozWEPRNW;
                objPCL010.NozWEPOutside = pcl010.NozWEPOutside;
                objPCL010.LongWEPNotReq = pcl010.LongWEPNotReq;
                objPCL010.LongWEPShop = pcl010.LongWEPShop;
                objPCL010.LongWEPMC = pcl010.LongWEPMC;
                objPCL010.LongWEPLEMF = pcl010.LongWEPLEMF;
                objPCL010.LongWEPRNW = pcl010.LongWEPRNW;
                objPCL010.LongWEPOutside = pcl010.LongWEPOutside;
                objPCL010.CircWEPNotReq = pcl010.CircWEPNotReq;
                objPCL010.CircWEPShop = pcl010.CircWEPShop;
                objPCL010.CircWEPMC = pcl010.CircWEPMC;
                objPCL010.CircWEPLEMF = pcl010.CircWEPLEMF;
                objPCL010.CircWEPRNW = pcl010.CircWEPRNW;
                objPCL010.CircWEPOutside = pcl010.CircWEPOutside;
                objPCL010.ShellLineFabNotReq = pcl010.ShellLineFabNotReq;
                objPCL010.ShellLineFabShop = pcl010.ShellLineFabShop;
                objPCL010.ShellLineFabMC = pcl010.ShellLineFabMC;
                objPCL010.ShellLineFabLEMF = pcl010.ShellLineFabLEMF;
                objPCL010.ShellLineFabRNW = pcl010.ShellLineFabRNW;
                objPCL010.ShellLineFabOutside = pcl010.ShellLineFabOutside;
                objPCL010.NozLineFabNotReq = pcl010.NozLineFabNotReq;
                objPCL010.NozLineFabShop = pcl010.NozLineFabShop;
                objPCL010.NozLineFabMC = pcl010.NozLineFabMC;
                objPCL010.NozLineFabLEMF = pcl010.NozLineFabLEMF;
                objPCL010.NozLineFabRNW = pcl010.NozLineFabRNW;
                objPCL010.NozLineFabOutside = pcl010.NozLineFabOutside;
                objPCL010.WeldExFabNotReq = pcl010.WeldExFabNotReq;
                objPCL010.WeldExFabShop = pcl010.WeldExFabShop;
                objPCL010.WeldExFabMC = pcl010.WeldExFabMC;
                objPCL010.WeldExFabLEMF = pcl010.WeldExFabLEMF;
                objPCL010.WeldExFabRNW = pcl010.WeldExFabRNW;
                objPCL010.WeldExFabOutside = pcl010.WeldExFabOutside;
                objPCL010.WeldInFabOutside = pcl010.WeldInFabOutside;
                objPCL010.WeldInFabNotReq = pcl010.WeldInFabNotReq;
                objPCL010.WeldInFabShop = pcl010.WeldInFabShop;
                objPCL010.WeldInFabMC = pcl010.WeldInFabMC;
                objPCL010.WeldInFabLEMF = pcl010.WeldInFabLEMF;
                objPCL010.WeldInFabRNW = pcl010.WeldInFabRNW;
                objPCL010.WeeptubeFabNotReq = pcl010.WeeptubeFabNotReq;
                objPCL010.WeeptubeFabShop = pcl010.WeeptubeFabShop;
                objPCL010.WeeptubeFabMC = pcl010.WeeptubeFabMC;
                objPCL010.WeeptubeFabLEMF = pcl010.WeeptubeFabLEMF;
                objPCL010.WeeptubeFabRNW = pcl010.WeeptubeFabRNW;
                objPCL010.WeeptubeFabOutside = pcl010.WeeptubeFabOutside;
                objPCL010.IntFabNotReq = pcl010.IntFabNotReq;
                objPCL010.IntFabShop = pcl010.IntFabShop;
                objPCL010.IntFabMC = pcl010.IntFabMC;
                objPCL010.IntFabLEMF = pcl010.IntFabLEMF;
                objPCL010.IntFabRNW = pcl010.IntFabRNW;
                objPCL010.IntFabOutside = pcl010.IntFabOutside;
                objPCL010.IntWeldNotReq = pcl010.IntWeldNotReq;
                objPCL010.IntWeldShop = pcl010.IntWeldShop;
                objPCL010.IntWeldMC = pcl010.IntWeldMC;
                objPCL010.IntWeldLEMF = pcl010.IntWeldLEMF;
                objPCL010.IntWeldRNW = pcl010.IntWeldRNW;
                objPCL010.IntWeldOutside = pcl010.IntWeldOutside;
                objPCL010.LinerWeldNotReq = pcl010.LinerWeldNotReq;
                objPCL010.LinerWeldShop = pcl010.LinerWeldShop;
                objPCL010.LinerWeldMC = pcl010.LinerWeldMC;
                objPCL010.LinerWeldLEMF = pcl010.LinerWeldLEMF;
                objPCL010.LinerWeldRNW = pcl010.LinerWeldRNW;
                objPCL010.LinerWeldOutside = pcl010.LinerWeldOutside;
                objPCL010.HPMNotReq = pcl010.HPMNotReq;
                objPCL010.HPMShop = pcl010.HPMShop;
                objPCL010.HPMMC = pcl010.HPMMC;
                objPCL010.HPMLEMF = pcl010.HPMLEMF;
                objPCL010.HPMRNW = pcl010.HPMRNW;
                objPCL010.HPMOutside = pcl010.HPMOutside;
                objPCL010.InsFabNotReq = pcl010.InsFabNotReq;
                objPCL010.InsFabShop = pcl010.InsFabShop;
                objPCL010.InsFabMC = pcl010.InsFabMC;
                objPCL010.InsFabLEMF = pcl010.InsFabLEMF;
                objPCL010.InsFabRNW = pcl010.InsFabRNW;
                objPCL010.InsFabOutside = pcl010.InsFabOutside;
                objPCL010.BaffleFabNotReq = pcl010.BaffleFabNotReq;
                objPCL010.BaffleFabShop = pcl010.BaffleFabShop;
                objPCL010.BaffleFabMC = pcl010.BaffleFabMC;
                objPCL010.BaffleFabLEMF = pcl010.BaffleFabLEMF;
                objPCL010.BaffleFabRNW = pcl010.BaffleFabRNW;
                objPCL010.BaffleFabOutside = pcl010.BaffleFabOutside;
                objPCL010.NLFabNotReq = pcl010.NLFabNotReq;
                objPCL010.NLFabShop = pcl010.NLFabShop;
                objPCL010.NLFabMC = pcl010.NLFabMC;
                objPCL010.NLFabLEMF = pcl010.NLFabLEMF;
                objPCL010.NLFabRNW = pcl010.NLFabRNW;
                objPCL010.NLFabOutside = pcl010.NLFabOutside;
                objPCL010.MGSFabNotReq = pcl010.MGSFabNotReq;
                objPCL010.MGSFabShop = pcl010.MGSFabShop;
                objPCL010.MGSFabMC = pcl010.MGSFabMC;
                objPCL010.MGSFabLEMF = pcl010.MGSFabLEMF;
                objPCL010.MGSFabRNW = pcl010.MGSFabRNW;
                objPCL010.MGSFabOutside = pcl010.MGSFabOutside;
                objPCL010.MCPFabNotReq = pcl010.MCPFabNotReq;
                objPCL010.MCPFabShop = pcl010.MCPFabShop;
                objPCL010.MCPFabMC = pcl010.MCPFabMC;
                objPCL010.MCPFabLEMF = pcl010.MCPFabLEMF;
                objPCL010.MCPFabRNW = pcl010.MCPFabRNW;
                objPCL010.MCPFabOutside = pcl010.MCPFabOutside;
                objPCL010.MCLFabNotReq = pcl010.MCLFabNotReq;
                objPCL010.MCLFabShop = pcl010.MCLFabShop;
                objPCL010.MCLFabMC = pcl010.MCLFabMC;
                objPCL010.MCLFabLEMF = pcl010.MCLFabLEMF;
                objPCL010.MCLFabRNW = pcl010.MCLFabRNW;
                objPCL010.MCLFabOutside = pcl010.MCLFabOutside;
                objPCL010.SaddlesNotReq = pcl010.SaddlesNotReq;
                objPCL010.SaddlesShop = pcl010.SaddlesShop;
                objPCL010.SaddlesMC = pcl010.SaddlesMC;
                objPCL010.SaddlesLEMF = pcl010.SaddlesLEMF;
                objPCL010.SaddlesRNW = pcl010.SaddlesRNW;
                objPCL010.SaddlesOutside = pcl010.SaddlesOutside;
                objPCL010.SkirtTemplateNotReq = pcl010.SkirtTemplateNotReq;
                objPCL010.SkirtTemplateShop = pcl010.SkirtTemplateShop;
                objPCL010.SkirtTemplateMC = pcl010.SkirtTemplateMC;
                objPCL010.SkirtTemplateLEMF = pcl010.SkirtTemplateLEMF;
                objPCL010.SkirtTemplateRNW = pcl010.SkirtTemplateRNW;
                objPCL010.SkirtTemplateOutside = pcl010.SkirtTemplateOutside;
                objPCL010.PlatformFabricationNotReq = pcl010.PlatformFabricationNotReq;
                objPCL010.PlatformFabricationShop = pcl010.PlatformFabricationShop;
                objPCL010.PlatformFabricationMC = pcl010.PlatformFabricationMC;
                objPCL010.PlatformFabricationLEMF = pcl010.PlatformFabricationLEMF;
                objPCL010.PlatformFabricationRNW = pcl010.PlatformFabricationRNW;
                objPCL010.PlatformFabricationOutside = pcl010.PlatformFabricationOutside;
                objPCL010.PlatformTrialAssemblyNotReq = pcl010.PlatformTrialAssemblyNotReq;
                objPCL010.PlatformTrialAssemblyShop = pcl010.PlatformTrialAssemblyShop;
                objPCL010.PlatformTrialAssemblyMC = pcl010.PlatformTrialAssemblyMC;
                objPCL010.PlatformTrialAssemblyLEMF = pcl010.PlatformTrialAssemblyLEMF;
                objPCL010.PlatformTrialAssemblyRNW = pcl010.PlatformTrialAssemblyRNW;
                objPCL010.PlatformTrialAssemblyOutside = pcl010.PlatformTrialAssemblyOutside;
                objPCL010.PWHTNotReq = pcl010.PWHTNotReq;
                objPCL010.PWHTShop = pcl010.PWHTShop;
                objPCL010.PWHTMC = pcl010.PWHTMC;
                objPCL010.PWHTLEMF = pcl010.PWHTLEMF;
                objPCL010.PWHTRNW = pcl010.PWHTRNW;
                objPCL010.PWHTOutside = pcl010.PWHTOutside;
                objPCL010.HeliumNotReq = pcl010.HeliumNotReq;
                objPCL010.HeliumShop = pcl010.HeliumShop;
                objPCL010.HeliumMC = pcl010.HeliumMC;
                objPCL010.HeliumLEMF = pcl010.HeliumLEMF;
                objPCL010.HeliumRNW = pcl010.HeliumRNW;
                objPCL010.HeliumOutside = pcl010.HeliumOutside;
                objPCL010.InternalInstallationNotReq = pcl010.InternalInstallationNotReq;
                objPCL010.InternalInstallationShop = pcl010.InternalInstallationShop;
                objPCL010.InternalInstallationMC = pcl010.InternalInstallationMC;
                objPCL010.InternalInstallationLEMF = pcl010.InternalInstallationLEMF;
                objPCL010.InternalInstallationRNW = pcl010.InternalInstallationRNW;
                objPCL010.InternalInstallationOutside = pcl010.InternalInstallationOutside;
                objPCL010.NFillingNotReq = pcl010.NFillingNotReq;
                objPCL010.NFillingShop = pcl010.NFillingShop;
                objPCL010.NFillingMC = pcl010.NFillingMC;
                objPCL010.NFillingLEMF = pcl010.NFillingLEMF;
                objPCL010.NFillingRNW = pcl010.NFillingRNW;
                objPCL010.NFillingOutside = pcl010.NFillingOutside;
                objPCL010.SandBlastingNotReq = pcl010.SandBlastingNotReq;
                objPCL010.SandBlastingShop = pcl010.SandBlastingShop;
                objPCL010.SandBlastingMC = pcl010.SandBlastingMC;
                objPCL010.SandBlastingLEMF = pcl010.SandBlastingLEMF;
                objPCL010.SandBlastingRNW = pcl010.SandBlastingRNW;
                objPCL010.SandBlastingOutside = pcl010.SandBlastingOutside;
                objPCL010.PaintingNotReq = pcl010.PaintingNotReq;
                objPCL010.PaintingShop = pcl010.PaintingShop;
                objPCL010.PaintingMC = pcl010.PaintingMC;
                objPCL010.PaintingLEMF = pcl010.PaintingLEMF;
                objPCL010.PaintingRNW = pcl010.PaintingRNW;
                objPCL010.PaintingOutside = pcl010.PaintingOutside;
                objPCL010.AcidCleaningNotReq = pcl010.AcidCleaningNotReq;
                objPCL010.AcidCleaningShop = pcl010.AcidCleaningShop;
                objPCL010.AcidCleaningMC = pcl010.AcidCleaningMC;
                objPCL010.AcidCleaningLEMF = pcl010.AcidCleaningLEMF;
                objPCL010.AcidCleaningRNW = pcl010.AcidCleaningRNW;
                objPCL010.AcidCleaningOutside = pcl010.AcidCleaningOutside;
                objPCL010.WeightMeasurementNotReq = pcl010.WeightMeasurementNotReq;
                objPCL010.WeightMeasurementShop = pcl010.WeightMeasurementShop;
                objPCL010.WeightMeasurementMC = pcl010.WeightMeasurementMC;
                objPCL010.WeightMeasurementLEMF = pcl010.WeightMeasurementLEMF;
                objPCL010.WeightMeasurementRNW = pcl010.WeightMeasurementRNW;
                objPCL010.WeightMeasurementOutside = pcl010.WeightMeasurementOutside;
                objPCL010.ManwayDavitNotReq = pcl010.ManwayDavitNotReq;
                objPCL010.ManwayDavitShop = pcl010.ManwayDavitShop;
                objPCL010.ManwayDavitMC = pcl010.ManwayDavitMC;
                objPCL010.ManwayDavitLEMF = pcl010.ManwayDavitLEMF;
                objPCL010.ManwayDavitRNW = pcl010.ManwayDavitRNW;
                objPCL010.ManwayDavitOutside = pcl010.ManwayDavitOutside;
                objPCL010.VesselDavitNotReq = pcl010.VesselDavitNotReq;
                objPCL010.VesselDavitShop = pcl010.VesselDavitShop;
                objPCL010.VesselDavitMC = pcl010.VesselDavitMC;
                objPCL010.VesselDavitLEMF = pcl010.VesselDavitLEMF;
                objPCL010.VesselDavitRNW = pcl010.VesselDavitRNW;
                objPCL010.VesselDavitOutside = pcl010.VesselDavitOutside;
                objPCL010.LappingRingNotReq = pcl010.LappingRingNotReq;
                objPCL010.LappingRingShop = pcl010.LappingRingShop;
                objPCL010.LappingRingMC = pcl010.LappingRingMC;
                objPCL010.LappingRingLEMF = pcl010.LappingRingLEMF;
                objPCL010.LappingRingRNW = pcl010.LappingRingRNW;
                objPCL010.LappingRingOutside = pcl010.LappingRingOutside;
                objPCL010.ShippingNotReq = pcl010.ShippingNotReq;
                objPCL010.ShippingShop = pcl010.ShippingShop;
                objPCL010.ShippingMC = pcl010.ShippingMC;
                objPCL010.ShippingLEMF = pcl010.ShippingLEMF;
                objPCL010.ShippingRNW = pcl010.ShippingRNW;
                objPCL010.ShippingOutside = pcl010.ShippingOutside;

                if (IsEdited)
                {
                    objPCL010.EditedBy = objClsLoginInfo.UserName;
                    objPCL010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL010.CreatedBy = objClsLoginInfo.UserName;
                    objPCL010.CreatedOn = DateTime.Now;
                    db.PCL010.Add(objPCL010);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL010.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveUreaFixture(PCL011 pcl011, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl011.HeaderId).FirstOrDefault();
                PCL011 objPCL011 = new PCL011();
                if (pcl011.LineId > 0)
                {
                    objPCL011 = db.PCL011.Where(x => x.LineId == pcl011.LineId).FirstOrDefault();
                }
                objPCL011.HeaderId = objPCL001.HeaderId;
                objPCL011.Project = objPCL001.Project;
                objPCL011.Document = objPCL001.Document;
                objPCL011.TempRollExternalReqDate = pcl011.TempRollExternalReqDate;
                string TempRollExternalReqDate = fc["TempRollExternalReqDate"] != null ? fc["TempRollExternalReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempRollExternalReqDate))
                {
                    objPCL011.TempRollExternalReqDate = DateTime.ParseExact(TempRollExternalReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.TempRollExternalNoFix = pcl011.TempRollExternalNoFix;
                objPCL011.TempRollExternalAvlQty = pcl011.TempRollExternalAvlQty;
                objPCL011.TempRollExternalDrg = pcl011.TempRollExternalDrg;
                objPCL011.TempRollExternalMtrl = pcl011.TempRollExternalMtrl;
                objPCL011.TempRollExternalReq = pcl011.TempRollExternalReq;
                objPCL011.TempRollExternalFab = pcl011.TempRollExternalFab;
                objPCL011.TempRollExternalDel = pcl011.TempRollExternalDel;
                objPCL011.TempRollPlateReq = pcl011.TempRollPlateReq;
                objPCL011.TempRollPlateReqDate = pcl011.TempRollPlateReqDate;
                string TempRollPlateReqDate = fc["TempRollPlateReqDate"] != null ? fc["TempRollPlateReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempRollPlateReqDate))
                {
                    objPCL011.TempRollPlateReqDate = DateTime.ParseExact(TempRollPlateReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.TempRollPlateNoFix = pcl011.TempRollPlateNoFix;
                objPCL011.TempRollPlateAvlQty = pcl011.TempRollPlateAvlQty;
                objPCL011.TempRollPlateDrg = pcl011.TempRollPlateDrg;
                objPCL011.TempRollPlateMtrl = pcl011.TempRollPlateMtrl;
                objPCL011.TempRollPlateFab = pcl011.TempRollPlateFab;
                objPCL011.TempRollPlateDel = pcl011.TempRollPlateDel;
                objPCL011.GasketCoverReq = pcl011.GasketCoverReq;
                objPCL011.GasketCoverReqDate = pcl011.GasketCoverReqDate;
                string GasketCoverReqDate = fc["GasketCoverReqDate"] != null ? fc["GasketCoverReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(GasketCoverReqDate))
                {
                    objPCL011.GasketCoverReqDate = DateTime.ParseExact(GasketCoverReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.GasketCoverNoFix = pcl011.GasketCoverNoFix;
                objPCL011.GasketCoverAvlQty = pcl011.GasketCoverAvlQty;
                objPCL011.GasketCoverDrg = pcl011.GasketCoverDrg;
                objPCL011.GasketCoverMtrl = pcl011.GasketCoverMtrl;
                objPCL011.GasketCoverFab = pcl011.GasketCoverFab;
                objPCL011.GasketCoverDel = pcl011.GasketCoverDel;
                objPCL011.RunInRunOutPlateReq = pcl011.RunInRunOutPlateReq;
                objPCL011.RunInRunOutPlateReqDate = pcl011.RunInRunOutPlateReqDate;
                string RunInRunOutPlateReqDate = fc["RunInRunOutPlateReqDate"] != null ? fc["RunInRunOutPlateReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RunInRunOutPlateReqDate))
                {
                    objPCL011.RunInRunOutPlateReqDate = DateTime.ParseExact(RunInRunOutPlateReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.RunInRunOutPlateNoFix = pcl011.RunInRunOutPlateNoFix;
                objPCL011.RunInRunOutPlateAvlQty = pcl011.RunInRunOutPlateAvlQty;
                objPCL011.RunInRunOutPlateDrg = pcl011.RunInRunOutPlateDrg;
                objPCL011.RunInRunOutPlateMtrl = pcl011.RunInRunOutPlateMtrl;
                objPCL011.RunInRunOutPlateFab = pcl011.RunInRunOutPlateFab;
                objPCL011.RunInRunOutPlateDel = pcl011.RunInRunOutPlateDel;
                objPCL011.SpiderShellReq = pcl011.SpiderShellReq;
                objPCL011.SpiderShellReqDate = pcl011.SpiderShellReqDate;
                string SpiderShellReqDate = fc["SpiderShellReqDate"] != null ? fc["SpiderShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SpiderShellReqDate))
                {
                    objPCL011.SpiderShellReqDate = DateTime.ParseExact(SpiderShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.SpiderShellNoFix = pcl011.SpiderShellNoFix;
                objPCL011.SpiderShellAvlQty = pcl011.SpiderShellAvlQty;
                objPCL011.SpiderShellDrg = pcl011.SpiderShellDrg;
                objPCL011.SpiderShellMtrl = pcl011.SpiderShellMtrl;
                objPCL011.SpiderShellFab = pcl011.SpiderShellFab;
                objPCL011.SpiderShellDel = pcl011.SpiderShellDel;
                objPCL011.WrapperPlateSkirtReq = pcl011.WrapperPlateSkirtReq;
                objPCL011.WrapperPlateSkirtReqDate = pcl011.WrapperPlateSkirtReqDate;
                string WrapperPlateSkirtReqDate = fc["WrapperPlateSkirtReqDate"] != null ? fc["WrapperPlateSkirtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(WrapperPlateSkirtReqDate))
                {
                    objPCL011.WrapperPlateSkirtReqDate = DateTime.ParseExact(WrapperPlateSkirtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.WrapperPlateSkirtNoFix = pcl011.WrapperPlateSkirtNoFix;
                objPCL011.WrapperPlateSkirtAvlQty = pcl011.WrapperPlateSkirtAvlQty;
                objPCL011.WrapperPlateSkirtDrg = pcl011.WrapperPlateSkirtDrg;
                objPCL011.WrapperPlateSkirtMtrl = pcl011.WrapperPlateSkirtMtrl;
                objPCL011.WrapperPlateSkirtFab = pcl011.WrapperPlateSkirtFab;
                objPCL011.WrapperPlateSkirtDel = pcl011.WrapperPlateSkirtDel;
                objPCL011.CounterWeightReq = pcl011.CounterWeightReq;
                objPCL011.CounterWeightReqDate = pcl011.CounterWeightReqDate;
                string CounterWeightReqDate = fc["CounterWeightReqDate"] != null ? fc["CounterWeightReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CounterWeightReqDate))
                {
                    objPCL011.CounterWeightReqDate = DateTime.ParseExact(CounterWeightReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.CounterWeightNoFix = pcl011.CounterWeightNoFix;
                objPCL011.CounterWeightAvlQty = pcl011.CounterWeightAvlQty;
                objPCL011.CounterWeightDrg = pcl011.CounterWeightDrg;
                objPCL011.CounterWeightMtrl = pcl011.CounterWeightMtrl;
                objPCL011.CounterWeightFab = pcl011.CounterWeightFab;
                objPCL011.CounterWeightDel = pcl011.CounterWeightDel;
                objPCL011.FixturePositionerReq = pcl011.FixturePositionerReq;
                objPCL011.FixturePositionerReqDate = pcl011.FixturePositionerReqDate;
                string FixturePositionerReqDate = fc["FixturePositionerReqDate"] != null ? fc["FixturePositionerReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixturePositionerReqDate))
                {
                    objPCL011.FixturePositionerReqDate = DateTime.ParseExact(FixturePositionerReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL011.FixturePositionerNoFix = pcl011.FixturePositionerNoFix;
                objPCL011.FixturePositionerAvlQty = pcl011.FixturePositionerAvlQty;
                objPCL011.FixturePositionerDrg = pcl011.FixturePositionerDrg;
                objPCL011.FixturePositionerMtrl = pcl011.FixturePositionerMtrl;
                objPCL011.FixturePositionerFab = pcl011.FixturePositionerFab;
                objPCL011.FixturePositionerDel = pcl011.FixturePositionerDel;
                objPCL011.SpiderforNozzReq = pcl011.SpiderforNozzReq;
                objPCL011.SpiderforNozzReqDate = pcl011.SpiderforNozzReqDate;
                string SpiderforNozzReqDate = fc["SpiderforNozzReqDate"] != null ? fc["SpiderforNozzReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SpiderforNozzReqDate))
                {
                    objPCL011.SpiderforNozzReqDate = DateTime.ParseExact(SpiderforNozzReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.SpiderforNozzNoFix = pcl011.SpiderforNozzNoFix;
                objPCL011.SpiderforNozzAvlQty = pcl011.SpiderforNozzAvlQty;
                objPCL011.SpiderforNozzDrg = pcl011.SpiderforNozzDrg;
                objPCL011.SpiderforNozzMtrl = pcl011.SpiderforNozzMtrl;
                objPCL011.SpiderforNozzFab = pcl011.SpiderforNozzFab;
                objPCL011.SpiderforNozzDel = pcl011.SpiderforNozzDel;
                objPCL011.SaddlePWHTReq = pcl011.SaddlePWHTReq;
                objPCL011.SaddlePWHTReqDate = pcl011.SaddlePWHTReqDate;
                string SaddlePWHTReqDate = fc["SaddlePWHTReqDate"] != null ? fc["SaddlePWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddlePWHTReqDate))
                {
                    objPCL011.SaddlePWHTReqDate = DateTime.ParseExact(SaddlePWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.SaddlePWHTNoFix = pcl011.SaddlePWHTNoFix;
                objPCL011.SaddlePWHTAvlQty = pcl011.SaddlePWHTAvlQty;
                objPCL011.SaddlePWHTDrg = pcl011.SaddlePWHTDrg;
                objPCL011.SaddlePWHTMtrl = pcl011.SaddlePWHTMtrl;
                objPCL011.SaddlePWHTFab = pcl011.SaddlePWHTFab;
                objPCL011.SaddlePWHTDel = pcl011.SaddlePWHTDel;
                objPCL011.FixtureChipBackReq = pcl011.FixtureChipBackReq;
                objPCL011.FixtureChipBackReqDate = pcl011.FixtureChipBackReqDate;
                string FixtureChipBackReqDate = fc["FixtureChipBackReqDate"] != null ? fc["FixtureChipBackReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixtureChipBackReqDate))
                {
                    objPCL011.FixtureChipBackReqDate = DateTime.ParseExact(FixtureChipBackReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.FixtureChipBackNoFix = pcl011.FixtureChipBackNoFix;
                objPCL011.FixtureChipBackAvlQty = pcl011.FixtureChipBackAvlQty;
                objPCL011.FixtureChipBackDrg = pcl011.FixtureChipBackDrg;
                objPCL011.FixtureChipBackMtrl = pcl011.FixtureChipBackMtrl;
                objPCL011.FixtureChipBackFab = pcl011.FixtureChipBackFab;
                objPCL011.FixtureChipBackDel = pcl011.FixtureChipBackDel;
                objPCL011.SpiderDEndReq = pcl011.SpiderDEndReq;
                objPCL011.SpiderDEndReqDate = pcl011.SpiderDEndReqDate;
                string SpiderDEndReqDate = fc["SpiderDEndReqDate"] != null ? fc["SpiderDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SpiderDEndReqDate))
                {
                    objPCL011.SpiderDEndReqDate = DateTime.ParseExact(SpiderDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.SpiderDEndNoFix = pcl011.SpiderDEndNoFix;
                objPCL011.SpiderDEndAvlQty = pcl011.SpiderDEndAvlQty;
                objPCL011.SpiderDEndDrg = pcl011.SpiderDEndDrg;
                objPCL011.SpiderDEndMtrl = pcl011.SpiderDEndMtrl;
                objPCL011.SpiderDEndFab = pcl011.SpiderDEndFab;
                objPCL011.SpiderDEndDel = pcl011.SpiderDEndDel;
                objPCL011.CClampReq = pcl011.CClampReq;
                objPCL011.CClampReqDate = pcl011.CClampReqDate;
                string CClampReqDate = fc["CClampReqDate"] != null ? fc["CClampReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CClampReqDate))
                {
                    objPCL011.CClampReqDate = DateTime.ParseExact(CClampReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL011.CClampNoFix = pcl011.CClampNoFix;
                objPCL011.CClampAvlQty = pcl011.CClampAvlQty;
                objPCL011.CClampDrg = pcl011.CClampDrg;
                objPCL011.CClampMtrl = pcl011.CClampMtrl;
                objPCL011.CClampFab = pcl011.CClampFab;
                objPCL011.CClampDel = pcl011.CClampDel;
                objPCL011.FixtInternalInstalReq = pcl011.FixtInternalInstalReq;
                objPCL011.FixtInternalInstalReqDate = pcl011.FixtInternalInstalReqDate;
                string FixtInternalInstalReqDate = fc["FixtInternalInstalReqDate"] != null ? fc["FixtInternalInstalReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixtInternalInstalReqDate))
                {
                    objPCL011.FixtInternalInstalReqDate = DateTime.ParseExact(FixtInternalInstalReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.FixtInternalInstalNoFix = pcl011.FixtInternalInstalNoFix;
                objPCL011.FixtInternalInstalAvlQty = pcl011.FixtInternalInstalAvlQty;
                objPCL011.FixtInternalInstalDrg = pcl011.FixtInternalInstalDrg;
                objPCL011.FixtInternalInstalMtrl = pcl011.FixtInternalInstalMtrl;
                objPCL011.FixtInternalInstalFab = pcl011.FixtInternalInstalFab;
                objPCL011.FixtInternalInstalDel = pcl011.FixtInternalInstalDel;
                objPCL011.DummyShellHeadReq = pcl011.DummyShellHeadReq;
                objPCL011.DummyShellHeadReqDate = pcl011.DummyShellHeadReqDate;
                string DummyShellHeadReqDate = fc["DummyShellHeadReqDate"] != null ? fc["DummyShellHeadReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DummyShellHeadReqDate))
                {
                    objPCL011.DummyShellHeadReqDate = DateTime.ParseExact(DummyShellHeadReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.DummyShellHeadNoFix = pcl011.DummyShellHeadNoFix;
                objPCL011.DummyShellHeadAvlQty = pcl011.DummyShellHeadAvlQty;
                objPCL011.DummyShellHeadDrg = pcl011.DummyShellHeadDrg;
                objPCL011.DummyShellHeadMtrl = pcl011.DummyShellHeadMtrl;
                objPCL011.DummyShellHeadFab = pcl011.DummyShellHeadFab;
                objPCL011.DummyShellHeadDel = pcl011.DummyShellHeadDel;
                objPCL011.ShellLinerReq = pcl011.ShellLinerReq;
                objPCL011.ShellLinerReqDate = pcl011.ShellLinerReqDate;
                string ShellLinerReqDate = fc["ShellLinerReqDate"] != null ? fc["ShellLinerReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLinerReqDate))
                {
                    objPCL011.ShellLinerReqDate = DateTime.ParseExact(ShellLinerReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                }
                objPCL011.ShellLinerNoFix = pcl011.ShellLinerNoFix;
                objPCL011.ShellLinerAvlQty = pcl011.ShellLinerAvlQty;
                objPCL011.ShellLinerDrg = pcl011.ShellLinerDrg;
                objPCL011.ShellLinerMtrl = pcl011.ShellLinerMtrl;
                objPCL011.ShellLinerFab = pcl011.ShellLinerFab;
                objPCL011.ShellLinerDel = pcl011.ShellLinerDel;
                objPCL011.HeadLinerReq = pcl011.HeadLinerReq;
                objPCL011.HeadLinerReqDate = pcl011.HeadLinerReqDate;
                string HeadLinerReqDate = fc["HeadLinerReqDate"] != null ? fc["HeadLinerReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HeadLinerReqDate))
                {
                    objPCL011.HeadLinerReqDate = DateTime.ParseExact(HeadLinerReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL011.HeadLinerNoFix = pcl011.HeadLinerNoFix;
                objPCL011.HeadLinerAvlQty = pcl011.HeadLinerAvlQty;
                objPCL011.HeadLinerDrg = pcl011.HeadLinerDrg;
                objPCL011.HeadLinerMtrl = pcl011.HeadLinerMtrl;
                objPCL011.HeadLinerFab = pcl011.HeadLinerFab;
                objPCL011.HeadLinerDel = pcl011.HeadLinerDel;
                objPCL011.LockingFixBracketReq = pcl011.LockingFixBracketReq;
                objPCL011.LockingFixBracketReqDate = pcl011.LockingFixBracketReqDate;
                string LockingFixBracketReqDate = fc["LockingFixBracketReqDate"] != null ? fc["LockingFixBracketReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LockingFixBracketReqDate))
                {
                    objPCL011.LockingFixBracketReqDate = DateTime.ParseExact(LockingFixBracketReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.LockingFixBracketNoFix = pcl011.LockingFixBracketNoFix;
                objPCL011.LockingFixBracketAvlQty = pcl011.LockingFixBracketAvlQty;
                objPCL011.LockingFixBracketDrg = pcl011.LockingFixBracketDrg;
                objPCL011.LockingFixBracketMtrl = pcl011.LockingFixBracketMtrl;
                objPCL011.LockingFixBracketFab = pcl011.LockingFixBracketFab;
                objPCL011.LockingFixBracketDel = pcl011.LockingFixBracketDel;
                objPCL011.DummyShellRotationReq = pcl011.DummyShellRotationReq;
                objPCL011.DummyShellRotationReqDate = pcl011.DummyShellRotationReqDate;
                string DummyShellRotationReqDate = fc["DummyShellRotationReqDate"] != null ? fc["DummyShellRotationReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DummyShellRotationReqDate))
                {
                    objPCL011.DummyShellRotationReqDate = DateTime.ParseExact(DummyShellRotationReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.DummyShellRotationNoFix = pcl011.DummyShellRotationNoFix;
                objPCL011.DummyShellRotationAvlQty = pcl011.DummyShellRotationAvlQty;
                objPCL011.DummyShellRotationDrg = pcl011.DummyShellRotationDrg;
                objPCL011.DummyShellRotationMtrl = pcl011.DummyShellRotationMtrl;
                objPCL011.DummyShellRotationFab = pcl011.DummyShellRotationFab;
                objPCL011.DummyShellRotationDel = pcl011.DummyShellRotationDel;
                objPCL011.LockingFixManwayReq = pcl011.LockingFixManwayReq;
                objPCL011.LockingFixManwayReqDate = pcl011.LockingFixManwayReqDate;
                string LockingFixManwayReqDate = fc["LockingFixManwayReqDate"] != null ? fc["LockingFixManwayReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LockingFixManwayReqDate))
                {
                    objPCL011.LockingFixManwayReqDate = DateTime.ParseExact(LockingFixManwayReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.LockingFixManwayNoFix = pcl011.LockingFixManwayNoFix;
                objPCL011.LockingFixManwayAvlQty = pcl011.LockingFixManwayAvlQty;
                objPCL011.LockingFixManwayDrg = pcl011.LockingFixManwayDrg;
                objPCL011.LockingFixManwayMtrl = pcl011.LockingFixManwayMtrl;
                objPCL011.LockingFixManwayFab = pcl011.LockingFixManwayFab;
                objPCL011.LockingFixManwayDel = pcl011.LockingFixManwayDel;
                objPCL011.ExtRingReq = pcl011.ExtRingReq;
                objPCL011.ExtRingReqDate = pcl011.ExtRingReqDate;
                string ExtRingReqDate = fc["ExtRingReqDate"] != null ? fc["ExtRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ExtRingReqDate))
                {
                    objPCL011.ExtRingReqDate = DateTime.ParseExact(ExtRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.ExtRingNoFix = pcl011.ExtRingNoFix;
                objPCL011.ExtRingAvlQty = pcl011.ExtRingAvlQty;
                objPCL011.ExtRingDrg = pcl011.ExtRingDrg;
                objPCL011.ExtRingMtrl = pcl011.ExtRingMtrl;
                objPCL011.ExtRingFab = pcl011.ExtRingFab;
                objPCL011.ExtRingDel = pcl011.ExtRingDel;
                objPCL011.PreHeatiArrgReq = pcl011.PreHeatiArrgReq;
                objPCL011.PreHeatiArrgReqDate = pcl011.PreHeatiArrgReqDate;
                string PreHeatiArrgReqDate = fc["PreHeatiArrgReqDate"] != null ? fc["PreHeatiArrgReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PreHeatiArrgReqDate))
                {
                    objPCL011.PreHeatiArrgReqDate = DateTime.ParseExact(PreHeatiArrgReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.PreHeatiArrgNoFix = pcl011.PreHeatiArrgNoFix;
                objPCL011.PreHeatiArrgAvlQty = pcl011.PreHeatiArrgAvlQty;
                objPCL011.PreHeatiArrgDrg = pcl011.PreHeatiArrgDrg;
                objPCL011.PreHeatiArrgMtrl = pcl011.PreHeatiArrgMtrl;
                objPCL011.PreHeatiArrgFab = pcl011.PreHeatiArrgFab;
                objPCL011.PreHeatiArrgDel = pcl011.PreHeatiArrgDel;
                objPCL011.FixturePWHTReq = pcl011.FixturePWHTReq;
                objPCL011.FixturePWHTReqDate = pcl011.FixturePWHTReqDate;
                string FixturePWHTReqDate = fc["FixturePWHTReqDate"] != null ? fc["FixturePWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixturePWHTReqDate))
                {
                    objPCL011.FixturePWHTReqDate = DateTime.ParseExact(FixturePWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                }
                objPCL011.FixturePWHTNoFix = pcl011.FixturePWHTNoFix;
                objPCL011.FixturePWHTAvlQty = pcl011.FixturePWHTAvlQty;
                objPCL011.FixturePWHTDrg = pcl011.FixturePWHTDrg;
                objPCL011.FixturePWHTMtrl = pcl011.FixturePWHTMtrl;
                objPCL011.FixturePWHTFab = pcl011.FixturePWHTFab;
                objPCL011.FixturePWHTDel = pcl011.FixturePWHTDel;
                objPCL011.ClosChipReq = pcl011.ClosChipReq;
                objPCL011.ClosChipReqDate = pcl011.ClosChipReqDate;
                string ClosChipReqDate = fc["ClosChipReqDate"] != null ? fc["ClosChipReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ClosChipReqDate))
                {
                    objPCL011.ClosChipReqDate = DateTime.ParseExact(ClosChipReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.ClosChipNoFix = pcl011.ClosChipNoFix;
                objPCL011.ClosChipAvlQty = pcl011.ClosChipAvlQty;
                objPCL011.ClosChipDrg = pcl011.ClosChipDrg;
                objPCL011.ClosChipMtrl = pcl011.ClosChipMtrl;
                objPCL011.ClosChipFab = pcl011.ClosChipFab;
                objPCL011.ClosChipDel = pcl011.ClosChipDel;
                objPCL011.SleeveStudReq = pcl011.SleeveStudReq;
                objPCL011.SleeveStudReqDate = pcl011.SleeveStudReqDate;
                string SleeveStudReqDate = fc["SleeveStudReqDate"] != null ? fc["SleeveStudReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SleeveStudReqDate))
                {
                    objPCL011.SleeveStudReqDate = DateTime.ParseExact(SleeveStudReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.SleeveStudNoFix = pcl011.SleeveStudNoFix;
                objPCL011.SleeveStudAvlQty = pcl011.SleeveStudAvlQty;
                objPCL011.SleeveStudDrg = pcl011.SleeveStudDrg;
                objPCL011.SleeveStudMtrl = pcl011.SleeveStudMtrl;
                objPCL011.SleeveStudFab = pcl011.SleeveStudFab;
                objPCL011.SleeveStudDel = pcl011.SleeveStudDel;
                objPCL011.EvenerBeamReq = pcl011.EvenerBeamReq;
                objPCL011.EvenerBeamReqDate = pcl011.EvenerBeamReqDate;
                string EvenerBeamReqDate = fc["EvenerBeamReqDate"] != null ? fc["EvenerBeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(EvenerBeamReqDate))
                {
                    objPCL011.EvenerBeamReqDate = DateTime.ParseExact(EvenerBeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.EvenerBeamNoFix = pcl011.EvenerBeamNoFix;
                objPCL011.EvenerBeamAvlQty = pcl011.EvenerBeamAvlQty;
                objPCL011.EvenerBeamDrg = pcl011.EvenerBeamDrg;
                objPCL011.EvenerBeamMtrl = pcl011.EvenerBeamMtrl;
                objPCL011.EvenerBeamFab = pcl011.EvenerBeamFab;
                objPCL011.EvenerBeamDel = pcl011.EvenerBeamDel;
                objPCL011.WeepTubeReq = pcl011.WeepTubeReq;
                objPCL011.WeepTubeReqDate = pcl011.WeepTubeReqDate;
                string WeepTubeReqDate = fc["WeepTubeReqDate"] != null ? fc["WeepTubeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(WeepTubeReqDate))
                {
                    objPCL011.WeepTubeReqDate = DateTime.ParseExact(WeepTubeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.WeepTubeNoFix = pcl011.WeepTubeNoFix;
                objPCL011.WeepTubeAvlQty = pcl011.WeepTubeAvlQty;
                objPCL011.WeepTubeDrg = pcl011.WeepTubeDrg;
                objPCL011.WeepTubeMtrl = pcl011.WeepTubeMtrl;
                objPCL011.WeepTubeFab = pcl011.WeepTubeFab;
                objPCL011.WeepTubeDel = pcl011.WeepTubeDel;
                objPCL011.HeliumLeakReq = pcl011.HeliumLeakReq;
                objPCL011.HeliumLeakReqDate = pcl011.HeliumLeakReqDate;
                string HeliumLeakReqDate = fc["HeliumLeakReqDate"] != null ? fc["HeliumLeakReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HeliumLeakReqDate))
                {
                    objPCL011.HeliumLeakReqDate = DateTime.ParseExact(HeliumLeakReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.HeliumLeakNoFix = pcl011.HeliumLeakNoFix;
                objPCL011.HeliumLeakAvlQty = pcl011.HeliumLeakAvlQty;
                objPCL011.HeliumLeakDrg = pcl011.HeliumLeakDrg;
                objPCL011.HeliumLeakMtrl = pcl011.HeliumLeakMtrl;
                objPCL011.HeliumLeakFab = pcl011.HeliumLeakFab;
                objPCL011.HeliumLeakDel = pcl011.HeliumLeakDel;
                objPCL011.AngularNozReq = pcl011.AngularNozReq;
                objPCL011.AngularNozReqDate = pcl011.AngularNozReqDate;
                string AngularNozReqDate = fc["AngularNozReqDate"] != null ? fc["AngularNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(AngularNozReqDate))
                {
                    objPCL011.AngularNozReqDate = DateTime.ParseExact(AngularNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.AngularNozNoFix = pcl011.AngularNozNoFix;
                objPCL011.AngularNozAvlQty = pcl011.AngularNozAvlQty;
                objPCL011.AngularNozDrg = pcl011.AngularNozDrg;
                objPCL011.AngularNozMtrl = pcl011.AngularNozMtrl;
                objPCL011.AngularNozFab = pcl011.AngularNozFab;
                objPCL011.AngularNozDel = pcl011.AngularNozDel;
                objPCL011.RotationRingReq = pcl011.RotationRingReq;
                objPCL011.RotationRingReqDate = pcl011.RotationRingReqDate;
                string RotationRingReqDate = fc["RotationRingReqDate"] != null ? fc["RotationRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RotationRingReqDate))
                {
                    objPCL011.RotationRingReqDate = DateTime.ParseExact(RotationRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL011.RotationRingNoFix = pcl011.RotationRingNoFix;
                objPCL011.RotationRingAvlQty = pcl011.RotationRingAvlQty;
                objPCL011.RotationRingDrg = pcl011.RotationRingDrg;
                objPCL011.RotationRingMtrl = pcl011.RotationRingMtrl;
                objPCL011.RotationRingFab = pcl011.RotationRingFab;
                objPCL011.RotationRingDel = pcl011.RotationRingDel;
                objPCL011.NozMWReq = pcl011.NozMWReq;
                objPCL011.NozMWReqDate = pcl011.NozMWReqDate;
                string NozMWReqDate = fc["NozMWReqDate"] != null ? fc["NozMWReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozMWReqDate))
                {
                    objPCL011.NozMWReqDate = DateTime.ParseExact(NozMWReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.NozMWNoFix = pcl011.NozMWNoFix;
                objPCL011.NozMWAvlQty = pcl011.NozMWAvlQty;
                objPCL011.NozMWDrg = pcl011.NozMWDrg;
                objPCL011.NozMWMtrl = pcl011.NozMWMtrl;
                objPCL011.NozMWFab = pcl011.NozMWFab;
                objPCL011.NozMWDel = pcl011.NozMWDel;
                objPCL011.TaperBlockReq = pcl011.TaperBlockReq;
                objPCL011.TaperBlockReqDate = pcl011.TaperBlockReqDate;
                string TaperBlockReqDate = fc["TaperBlockReqDate"] != null ? fc["TaperBlockReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TaperBlockReqDate))
                {
                    objPCL011.TaperBlockReqDate = DateTime.ParseExact(TaperBlockReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL011.TaperBlockNoFix = pcl011.TaperBlockNoFix;
                objPCL011.TaperBlockAvlQty = pcl011.TaperBlockAvlQty;
                objPCL011.TaperBlockDrg = pcl011.TaperBlockDrg;
                objPCL011.TaperBlockMtrl = pcl011.TaperBlockMtrl;
                objPCL011.TaperBlockFab = pcl011.TaperBlockFab;
                objPCL011.TaperBlockDel = pcl011.TaperBlockDel;

                if (pcl011.LineId > 0)
                {
                    objPCL011.EditedBy = objClsLoginInfo.UserName;
                    objPCL011.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL011.CreatedBy = objClsLoginInfo.UserName;
                    objPCL011.CreatedOn = DateTime.Now;
                    db.PCL011.Add(objPCL011);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL011.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUreaDocument(PCL012 pcl012, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl012.HeaderId).FirstOrDefault();
                PCL012 objPCL012 = new PCL012();
                if (pcl012.LineId > 0)
                {
                    objPCL012 = db.PCL012.Where(x => x.LineId == pcl012.LineId).FirstOrDefault();
                }
                objPCL012.HeaderId = objPCL001.HeaderId;
                objPCL012.Project = objPCL001.Project;
                objPCL012.Document = objPCL001.Document;

                objPCL012.JPPReq = pcl012.JPPReq;
                objPCL012.JPPReqDate = pcl012.JPPReqDate;
                string JPPReqDate = fc["JPPReqDate"] != null ? fc["JPPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReqDate))
                {
                    objPCL012.JPPReqDate = DateTime.ParseExact(JPPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.JPPDocNo = pcl012.JPPDocNo;
                objPCL012.JPPReleasedOn = pcl012.JPPReleasedOn;
                string JPPReleasedOn = fc["JPPReleasedOn"] != null ? fc["JPPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReleasedOn))
                {
                    objPCL012.JPPReleasedOn = DateTime.ParseExact(JPPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.BoxPlotReq = pcl012.BoxPlotReq;
                objPCL012.BoxPlotReqDate = pcl012.BoxPlotReqDate;
                string BoxPlotReqDate = fc["BoxPlotReqDate"] != null ? fc["BoxPlotReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BoxPlotReqDate))
                {
                    objPCL012.BoxPlotReqDate = DateTime.ParseExact(BoxPlotReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.BoxPlotDocNo = pcl012.BoxPlotDocNo;
                objPCL012.BoxPlotReleasedOn = pcl012.BoxPlotReleasedOn;
                string BoxPlotReleasedOn = fc["BoxPlotReleasedOn"] != null ? fc["BoxPlotReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BoxPlotReleasedOn))
                {
                    objPCL012.BoxPlotReleasedOn = DateTime.ParseExact(BoxPlotReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RollingCapaReq = pcl012.RollingCapaReq;
                objPCL012.RollingCapaReqDate = pcl012.RollingCapaReqDate;
                string RollingCapaReqDate = fc["RollingCapaReqDate"] != null ? fc["RollingCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReqDate))
                {
                    objPCL012.RollingCapaReqDate = DateTime.ParseExact(RollingCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RollingCapaDocNo = pcl012.RollingCapaDocNo;
                objPCL012.RollingCapaReleasedOn = pcl012.RollingCapaReleasedOn;
                string RollingCapaReleasedOn = fc["RollingCapaReleasedOn"] != null ? fc["RollingCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReleasedOn))
                {
                    objPCL012.RollingCapaReleasedOn = DateTime.ParseExact(RollingCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.ImprovBudgetReq = pcl012.ImprovBudgetReq;
                objPCL012.ImprovBudgetReqDate = pcl012.ImprovBudgetReqDate;
                string ImprovBudgetReqDate = fc["ImprovBudgetReqDate"] != null ? fc["ImprovBudgetReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReqDate))
                {
                    objPCL012.ImprovBudgetReqDate = DateTime.ParseExact(ImprovBudgetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.ImprovBudgetDocNo = pcl012.ImprovBudgetDocNo;
                objPCL012.ImprovBudgetReleasedOn = pcl012.ImprovBudgetReleasedOn;
                string ImprovBudgetReleasedOn = fc["ImprovBudgetReleasedOn"] != null ? fc["ImprovBudgetReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReleasedOn))
                {
                    objPCL012.ImprovBudgetReleasedOn = DateTime.ParseExact(ImprovBudgetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RCCPBreakUpReq = pcl012.RCCPBreakUpReq;
                objPCL012.RCCPBreakUpReqDate = pcl012.RCCPBreakUpReqDate;
                string RCCPBreakUpReqDate = fc["RCCPBreakUpReqDate"] != null ? fc["RCCPBreakUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReqDate))
                {
                    objPCL012.RCCPBreakUpReqDate = DateTime.ParseExact(RCCPBreakUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RCCPBreakUpDocNo = pcl012.RCCPBreakUpDocNo;
                objPCL012.RCCPBreakUpReleasedOn = pcl012.RCCPBreakUpReleasedOn;
                string RCCPBreakUpReleasedOn = fc["RCCPBreakUpReleasedOn"] != null ? fc["RCCPBreakUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReleasedOn))
                {
                    objPCL012.RCCPBreakUpReleasedOn = DateTime.ParseExact(RCCPBreakUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SchedConcertoReq = pcl012.SchedConcertoReq;
                objPCL012.SchedConcertoReqDate = pcl012.SchedConcertoReqDate;
                string SchedConcertoReqDate = fc["SchedConcertoReqDate"] != null ? fc["SchedConcertoReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReqDate))
                {
                    objPCL012.SchedConcertoReqDate = DateTime.ParseExact(SchedConcertoReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SchedConcertoDocNo = pcl012.SchedConcertoDocNo;
                objPCL012.SchedConcertoReleasedOn = pcl012.SchedConcertoReleasedOn;
                string SchedConcertoReleasedOn = fc["SchedConcertoReleasedOn"] != null ? fc["SchedConcertoReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReleasedOn))
                {
                    objPCL012.SchedConcertoReleasedOn = DateTime.ParseExact(SchedConcertoReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SchedExlReq = pcl012.SchedExlReq;
                objPCL012.SchedExlReqDate = pcl012.SchedExlReqDate;
                string SchedExlReqDate = fc["SchedExlReqDate"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReqDate))
                {
                    objPCL012.SchedExlReqDate = DateTime.ParseExact(SchedExlReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SchedExlDocNo = pcl012.SchedExlDocNo;
                objPCL012.SchedExlReleasedOn = pcl012.SchedExlReleasedOn;
                string SchedExlReleasedOn = fc["SchedExlReleasedOn"] != null ? fc["SchedExlReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReleasedOn))
                {
                    objPCL012.SchedExlReleasedOn = DateTime.ParseExact(SchedExlReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SubcontPlanReq = pcl012.SubcontPlanReq;
                objPCL012.SubcontPlanReqDate = pcl012.SubcontPlanReqDate;
                string SubcontPlanReqDate = fc["SubcontPlanReqDate"] != null ? fc["SubcontPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReqDate))
                {
                    objPCL012.SubcontPlanReqDate = DateTime.ParseExact(SubcontPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SubcontPlanDocNo = pcl012.SubcontPlanDocNo;
                objPCL012.SubcontPlanReleasedOn = pcl012.SubcontPlanReleasedOn;
                string SubcontPlanReleasedOn = fc["SubcontPlanReleasedOn"] != null ? fc["SubcontPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReleasedOn))
                {
                    objPCL012.SubcontPlanReleasedOn = DateTime.ParseExact(SubcontPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.LongSeamReq = pcl012.LongSeamReq;
                objPCL012.LongSeamReqDate = pcl012.LongSeamReqDate;
                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL012.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.LongSeamDocNo = pcl012.LongSeamDocNo;
                objPCL012.LongSeamReleasedOn = pcl012.LongSeamReleasedOn;
                string LongSeamReleasedOn = fc["LongSeamReleasedOn"] != null ? fc["LongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReleasedOn))
                {
                    objPCL012.LongSeamReleasedOn = DateTime.ParseExact(LongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.CircSeamReq = pcl012.CircSeamReq;
                objPCL012.CircSeamReqDate = pcl012.CircSeamReqDate;
                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL012.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.CircSeamDocNo = pcl012.CircSeamDocNo;
                objPCL012.CircSeamReleasedOn = pcl012.CircSeamReleasedOn;
                string CircSeamReleasedOn = fc["CircSeamReleasedOn"] != null ? fc["CircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReleasedOn))
                {
                    objPCL012.CircSeamReleasedOn = DateTime.ParseExact(CircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.LineFabReq = pcl012.LineFabReq;
                objPCL012.LineFabReqDate = pcl012.LineFabReqDate;
                string LineFabReqDate = fc["LineFabReqDate"] != null ? fc["LineFabReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineFabReqDate))
                {
                    objPCL012.LineFabReqDate = DateTime.ParseExact(LineFabReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.LineFabDocNo = pcl012.LineFabDocNo;
                objPCL012.LineFabReleasedOn = pcl012.LineFabReleasedOn;
                string LineFabReleasedOn = fc["LineFabReleasedOn"] != null ? fc["LineFabReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineFabReleasedOn))
                {
                    objPCL012.LineFabReleasedOn = DateTime.ParseExact(LineFabReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.NozzleShellReq = pcl012.NozzleShellReq;
                objPCL012.NozzleShellReqDate = pcl012.NozzleShellReqDate;
                string NozzleShellReqDate = fc["NozzleShellReqDate"] != null ? fc["NozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReqDate))
                {
                    objPCL012.NozzleShellReqDate = DateTime.ParseExact(NozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.NozzleShellDocNo = pcl012.NozzleShellDocNo;
                objPCL012.NozzleShellReleasedOn = pcl012.NozzleShellReleasedOn;
                string NozzleShellReleasedOn = fc["NozzleShellReleasedOn"] != null ? fc["NozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReleasedOn))
                {
                    objPCL012.NozzleShellReleasedOn = DateTime.ParseExact(NozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RollCapaReq = pcl012.RollCapaReq;
                objPCL012.RollCapaReqDate = pcl012.RollCapaReqDate;
                string RollCapaReqDate = fc["RollCapaReqDate"] != null ? fc["RollCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReqDate))
                {
                    objPCL012.RollCapaReqDate = DateTime.ParseExact(RollCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RollCapaDocNo = pcl012.RollCapaDocNo;
                objPCL012.RollCapaReleasedOn = pcl012.RollCapaReleasedOn;
                string RollCapaReleasedOn = fc["RollCapaReleasedOn"] != null ? fc["RollCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReleasedOn))
                {
                    objPCL012.RollCapaReleasedOn = DateTime.ParseExact(RollCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.TablesNozReq = pcl012.TablesNozReq;
                objPCL012.TablesNozReqDate = pcl012.TablesNozReqDate;
                string TablesNozReqDate = fc["TablesNozReqDate"] != null ? fc["TablesNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReqDate))
                {
                    objPCL012.TablesNozReqDate = DateTime.ParseExact(TablesNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.TablesNozDocNo = pcl012.TablesNozDocNo;
                objPCL012.TablesNozReleasedOn = pcl012.TablesNozReleasedOn;
                string TablesNozReleasedOn = fc["TablesNozReleasedOn"] != null ? fc["TablesNozReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReleasedOn))
                {
                    objPCL012.TablesNozReleasedOn = DateTime.ParseExact(TablesNozReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.TableIntReq = pcl012.TableIntReq;
                objPCL012.TableIntReqDate = pcl012.TableIntReqDate;
                string TableIntReqDate = fc["TableIntReqDate"] != null ? fc["TableIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReqDate))
                {
                    objPCL012.TableIntReqDate = DateTime.ParseExact(TableIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.TableIntDocNo = pcl012.TableIntDocNo;
                objPCL012.TableIntReleasedOn = pcl012.TableIntReleasedOn;
                string TableIntReleasedOn = fc["TableIntReleasedOn"] != null ? fc["TableIntReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReleasedOn))
                {
                    objPCL012.TableIntReleasedOn = DateTime.ParseExact(TableIntReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.TableExtReq = pcl012.TableExtReq;
                objPCL012.TableExtReqDate = pcl012.TableExtReqDate;
                string TableExtReqDate = fc["TableExtReqDate"] != null ? fc["TableExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReqDate))
                {
                    objPCL012.TableExtReqDate = DateTime.ParseExact(TableExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.TableExtDocNo = pcl012.TableExtDocNo;
                objPCL012.TableExtReleasedOn = pcl012.TableExtReleasedOn;
                string TableExtReleasedOn = fc["TableExtReleasedOn"] != null ? fc["TableExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReleasedOn))
                {
                    objPCL012.TableExtReleasedOn = DateTime.ParseExact(TableExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTREdgeReq = pcl012.HTREdgeReq;
                objPCL012.HTREdgeReqDate = pcl012.HTREdgeReqDate;
                string HTREdgeReqDate = fc["HTREdgeReqDate"] != null ? fc["HTREdgeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReqDate))
                {
                    objPCL012.HTREdgeReqDate = DateTime.ParseExact(HTREdgeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTREdgeDocNo = pcl012.HTREdgeDocNo;
                objPCL012.HTREdgeReleasedOn = pcl012.HTREdgeReleasedOn;
                string HTREdgeReleasedOn = fc["HTREdgeReleasedOn"] != null ? fc["HTREdgeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReleasedOn))
                {
                    objPCL012.HTREdgeReleasedOn = DateTime.ParseExact(HTREdgeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRRollReq = pcl012.HTRRollReq;
                objPCL012.HTRRollReqDate = pcl012.HTRRollReqDate;
                string HTRRollReqDate = fc["HTRRollReqDate"] != null ? fc["HTRRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReqDate))
                {
                    objPCL012.HTRRollReqDate = DateTime.ParseExact(HTRRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRRollDocNo = pcl012.HTRRollDocNo;
                objPCL012.HTRRollReleasedOn = pcl012.HTRRollReleasedOn;
                string HTRRollReleasedOn = fc["HTRRollReleasedOn"] != null ? fc["HTRRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReleasedOn))
                {
                    objPCL012.HTRRollReleasedOn = DateTime.ParseExact(HTRRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRReRollReq = pcl012.HTRReRollReq;
                objPCL012.HTRReRollReqDate = pcl012.HTRReRollReqDate;
                string HTRReRollReqDate = fc["HTRReRollReqDate"] != null ? fc["HTRReRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReqDate))
                {
                    objPCL012.HTRReRollReqDate = DateTime.ParseExact(HTRReRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRReRollDocNo = pcl012.HTRReRollDocNo;
                objPCL012.HTRReRollReleasedOn = pcl012.HTRReRollReleasedOn;
                string HTRReRollReleasedOn = fc["HTRReRollReleasedOn"] != null ? fc["HTRReRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReleasedOn))
                {
                    objPCL012.HTRReRollReleasedOn = DateTime.ParseExact(HTRReRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RefLineSketchReq = pcl012.RefLineSketchReq;
                objPCL012.RefLineSketchReqDate = pcl012.RefLineSketchReqDate;
                string RefLineSketchReqDate = fc["RefLineSketchReqDate"] != null ? fc["RefLineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReqDate))
                {
                    objPCL012.RefLineSketchReqDate = DateTime.ParseExact(RefLineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.RefLineSketchDocNo = pcl012.RefLineSketchDocNo;
                objPCL012.RefLineSketchReleasedOn = pcl012.RefLineSketchReleasedOn;
                string RefLineSketchReleasedOn = fc["RefLineSketchReleasedOn"] != null ? fc["RefLineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReleasedOn))
                {
                    objPCL012.RefLineSketchReleasedOn = DateTime.ParseExact(RefLineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRSecPWHTReq = pcl012.HTRSecPWHTReq;
                objPCL012.HTRSecPWHTReqDate = pcl012.HTRSecPWHTReqDate;
                string HTRSecPWHTReqDate = fc["HTRSecPWHTReqDate"] != null ? fc["HTRSecPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRSecPWHTReqDate))
                {
                    objPCL012.HTRSecPWHTReqDate = DateTime.ParseExact(HTRSecPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRSecPWHTDocNo = pcl012.HTRSecPWHTDocNo;
                objPCL012.HTRSecPWHTReleasedOn = pcl012.HTRSecPWHTReleasedOn;
                string HTRSecPWHTReleasedOn = fc["HTRSecPWHTReleasedOn"] != null ? fc["HTRSecPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRSecPWHTReleasedOn))
                {
                    objPCL012.HTRSecPWHTReleasedOn = DateTime.ParseExact(HTRSecPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRClosingReq = pcl012.HTRClosingReq;
                objPCL012.HTRClosingReqDate = pcl012.HTRClosingReqDate;
                string HTRClosingReqDate = fc["HTRClosingReqDate"] != null ? fc["HTRClosingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRClosingReqDate))
                {
                    objPCL012.HTRClosingReqDate = DateTime.ParseExact(HTRClosingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRClosingDocNo = pcl012.HTRClosingDocNo;
                objPCL012.HTRClosingReleasedOn = pcl012.HTRClosingReleasedOn;
                string HTRClosingReleasedOn = fc["HTRClosingReleasedOn"] != null ? fc["HTRClosingReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRClosingReleasedOn))
                {
                    objPCL012.HTRClosingReleasedOn = DateTime.ParseExact(HTRClosingReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRNozzReq = pcl012.HTRNozzReq;
                objPCL012.HTRNozzReqDate = pcl012.HTRNozzReqDate;
                string HTRNozzReqDate = fc["HTRNozzReqDate"] != null ? fc["HTRNozzReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRNozzReqDate))
                {
                    objPCL012.HTRNozzReqDate = DateTime.ParseExact(HTRNozzReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRNozzDocNo = pcl012.HTRNozzDocNo;
                objPCL012.HTRNozzReleasedOn = pcl012.HTRNozzReleasedOn;
                string HTRNozzReleasedOn = fc["HTRNozzReleasedOn"] != null ? fc["HTRNozzReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRNozzReleasedOn))
                {
                    objPCL012.HTRNozzReleasedOn = DateTime.ParseExact(HTRNozzReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRPWHTReq = pcl012.HTRPWHTReq;
                objPCL012.HTRPWHTReqDate = pcl012.HTRPWHTReqDate;
                string HTRPWHTReqDate = fc["HTRPWHTReqDate"] != null ? fc["HTRPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReqDate))
                {
                    objPCL012.HTRPWHTReqDate = DateTime.ParseExact(HTRPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRPWHTDocNo = pcl012.HTRPWHTDocNo;
                objPCL012.HTRPWHTReleasedOn = pcl012.HTRPWHTReleasedOn;
                string HTRPWHTReleasedOn = fc["HTRPWHTReleasedOn"] != null ? fc["HTRPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReleasedOn))
                {
                    objPCL012.HTRPWHTReleasedOn = DateTime.ParseExact(HTRPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRLSRReq = pcl012.HTRLSRReq;
                objPCL012.HTRLSRReqDate = pcl012.HTRLSRReqDate;
                string HTRLSRReqDate = fc["HTRLSRReqDate"] != null ? fc["HTRLSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReqDate))
                {
                    objPCL012.HTRLSRReqDate = DateTime.ParseExact(HTRLSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HTRLSRDocNo = pcl012.HTRLSRDocNo;
                objPCL012.HTRLSRReleasedOn = pcl012.HTRLSRReleasedOn;
                string HTRLSRReleasedOn = fc["HTRLSRReleasedOn"] != null ? fc["HTRLSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReleasedOn))
                {
                    objPCL012.HTRLSRReleasedOn = DateTime.ParseExact(HTRLSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HandlingPlanReq = pcl012.HandlingPlanReq;
                objPCL012.HandlingPlanReqDate = pcl012.HandlingPlanReqDate;
                string HandlingPlanReqDate = fc["HandlingPlanReqDate"] != null ? fc["HandlingPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReqDate))
                {
                    objPCL012.HandlingPlanReqDate = DateTime.ParseExact(HandlingPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HandlingPlanDocNo = pcl012.HandlingPlanDocNo;
                objPCL012.HandlingPlanReleasedOn = pcl012.HandlingPlanReleasedOn;
                string HandlingPlanReleasedOn = fc["HandlingPlanReleasedOn"] != null ? fc["HandlingPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReleasedOn))
                {
                    objPCL012.HandlingPlanReleasedOn = DateTime.ParseExact(HandlingPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.LocationTankRotatorReq = pcl012.LocationTankRotatorReq;
                objPCL012.LocationTankRotatorReqDate = pcl012.LocationTankRotatorReqDate;
                string LocationTankRotatorReqDate = fc["LocationTankRotatorReqDate"] != null ? fc["LocationTankRotatorReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReqDate))
                {
                    objPCL012.LocationTankRotatorReqDate = DateTime.ParseExact(LocationTankRotatorReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.LocationTankRotatorDocNo = pcl012.LocationTankRotatorDocNo;
                objPCL012.LocationTankRotatorReleasedOn = pcl012.LocationTankRotatorReleasedOn;
                string LocationTankRotatorReleasedOn = fc["LocationTankRotatorReleasedOn"] != null ? fc["LocationTankRotatorReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReleasedOn))
                {
                    objPCL012.LocationTankRotatorReleasedOn = DateTime.ParseExact(LocationTankRotatorReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HydrotestReq = pcl012.HydrotestReq;
                objPCL012.HydrotestReqDate = pcl012.HydrotestReqDate;
                string HydrotestReqDate = fc["HydrotestReqDate"] != null ? fc["HydrotestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReqDate))
                {
                    objPCL012.HydrotestReqDate = DateTime.ParseExact(HydrotestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.HydrotestDocNo = pcl012.HydrotestDocNo;
                objPCL012.HydrotestReleasedOn = pcl012.HydrotestReleasedOn;
                string HydrotestReleasedOn = fc["HydrotestReleasedOn"] != null ? fc["HydrotestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReleasedOn))
                {
                    objPCL012.HydrotestReleasedOn = DateTime.ParseExact(HydrotestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.IntFabReq = pcl012.IntFabReq;
                objPCL012.IntFabReqDate = pcl012.IntFabReqDate;
                string IntFabReqDate = fc["IntFabReqDate"] != null ? fc["IntFabReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(IntFabReqDate))
                {
                    objPCL012.IntFabReqDate = DateTime.ParseExact(IntFabReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.IntFabDocNo = pcl012.IntFabDocNo;
                objPCL012.IntFabReleasedOn = pcl012.IntFabReleasedOn;
                string IntFabReleasedOn = fc["IntFabReleasedOn"] != null ? fc["IntFabReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(IntFabReleasedOn))
                {
                    objPCL012.IntFabReleasedOn = DateTime.ParseExact(IntFabReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.BaffleTubeReq = pcl012.BaffleTubeReq;
                objPCL012.BaffleTubeReqDate = pcl012.BaffleTubeReqDate;
                string BaffleTubeReqDate = fc["BaffleTubeReqDate"] != null ? fc["BaffleTubeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BaffleTubeReqDate))
                {
                    objPCL012.BaffleTubeReqDate = DateTime.ParseExact(BaffleTubeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.BaffleTubeDocNo = pcl012.BaffleTubeDocNo;
                objPCL012.BaffleTubeReleasedOn = pcl012.BaffleTubeReleasedOn;
                string BaffleTubeReleasedOn = fc["BaffleTubeReleasedOn"] != null ? fc["BaffleTubeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BaffleTubeReleasedOn))
                {
                    objPCL012.BaffleTubeReleasedOn = DateTime.ParseExact(BaffleTubeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SurfaceTreatRequestReq = pcl012.SurfaceTreatRequestReq;
                objPCL012.SurfaceTreatRequestReqDate = pcl012.SurfaceTreatRequestReqDate;
                string SurfaceTreatRequestReqDate = fc["SurfaceTreatRequestReqDate"] != null ? fc["SurfaceTreatRequestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReqDate))
                {
                    objPCL012.SurfaceTreatRequestReqDate = DateTime.ParseExact(SurfaceTreatRequestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL012.SurfaceTreatRequestDocNo = pcl012.SurfaceTreatRequestDocNo;
                objPCL012.SurfaceTreatRequestReleasedOn = pcl012.SurfaceTreatRequestReleasedOn;
                string SurfaceTreatRequestReleasedOn = fc["SurfaceTreatRequestReleasedOn"] != null ? fc["SurfaceTreatRequestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReleasedOn))
                {
                    objPCL012.SurfaceTreatRequestReleasedOn = DateTime.ParseExact(SurfaceTreatRequestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                if (pcl012.LineId > 0)
                {
                    objPCL012.EditedBy = objClsLoginInfo.UserName;
                    objPCL012.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL012.CreatedBy = objClsLoginInfo.UserName;
                    objPCL012.CreatedOn = DateTime.Now;
                    db.PCL012.Add(objPCL012);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL012.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUreaDim(PCL013 pcl013, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl013.HeaderId).FirstOrDefault();
                PCL013 objPCL013 = new PCL013();
                if (pcl013.LineId > 0)
                {
                    objPCL013 = db.PCL013.Where(x => x.LineId == pcl013.LineId).FirstOrDefault();
                }
                objPCL013.HeaderId = objPCL001.HeaderId;
                objPCL013.Project = objPCL001.Project;
                objPCL013.Document = objPCL001.Document;
                objPCL013.RollDataReq = pcl013.RollDataReq;

                objPCL013.RollDataReqDate = pcl013.RollDataReqDate;
                string RollDataReqDate = fc["RollDataReqDate"] != null ? fc["RollDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReqDate))
                {
                    objPCL013.RollDataReqDate = DateTime.ParseExact(RollDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.RollDataDocNo = pcl013.RollDataDocNo;
                objPCL013.RollDataReleasedOn = pcl013.RollDataReleasedOn;
                string RollDataReleasedOn = fc["RollDataReleasedOn"] != null ? fc["RollDataReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReleasedOn))
                {
                    objPCL013.RollDataReleasedOn = DateTime.ParseExact(RollDataReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellLongReq = pcl013.ShellLongReq;
                objPCL013.ShellLongReqDate = pcl013.ShellLongReqDate;
                string ShellLongReqDate = fc["ShellLongReqDate"] != null ? fc["ShellLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReqDate))
                {
                    objPCL013.ShellLongReqDate = DateTime.ParseExact(ShellLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellLongDocNo = pcl013.ShellLongDocNo;
                objPCL013.ShellLongReleasedOn = pcl013.ShellLongReleasedOn;
                string ShellLongReleasedOn = fc["ShellLongReleasedOn"] != null ? fc["ShellLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReleasedOn))
                {
                    objPCL013.ShellLongReleasedOn = DateTime.ParseExact(ShellLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellInsideReq = pcl013.ShellInsideReq;
                objPCL013.ShellInsideReqDate = pcl013.ShellInsideReqDate;
                string ShellInsideReqDate = fc["ShellInsideReqDate"] != null ? fc["ShellInsideReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellInsideReqDate))
                {
                    objPCL013.ShellInsideReqDate = DateTime.ParseExact(ShellInsideReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellInsideDocNo = pcl013.ShellInsideDocNo;
                objPCL013.ShellInsideReleasedOn = pcl013.ShellInsideReleasedOn;
                string ShellInsideReleasedOn = fc["ShellInsideReleasedOn"] != null ? fc["ShellInsideReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellInsideReleasedOn))
                {
                    objPCL013.ShellInsideReleasedOn = DateTime.ParseExact(ShellInsideReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.NozzReq = pcl013.NozzReq;
                objPCL013.NozzReqDate = pcl013.NozzReqDate;
                string NozzReqDate = fc["NozzReqDate"] != null ? fc["NozzReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzReqDate))
                {
                    objPCL013.NozzReqDate = DateTime.ParseExact(NozzReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.NozzDocNo = pcl013.NozzDocNo;
                objPCL013.NozzReleasedOn = pcl013.NozzReleasedOn;
                string NozzReleasedOn = fc["NozzReleasedOn"] != null ? fc["NozzReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzReleasedOn))
                {
                    objPCL013.NozzReleasedOn = DateTime.ParseExact(NozzReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ChangeOvalityExtReq = pcl013.ChangeOvalityExtReq;
                objPCL013.ChangeOvalityExtReqDate = pcl013.ChangeOvalityExtReqDate;
                string ChangeOvalityExtReqDate = fc["ChangeOvalityExtReqDate"] != null ? fc["ChangeOvalityExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReqDate))
                {
                    objPCL013.ChangeOvalityExtReqDate = DateTime.ParseExact(ChangeOvalityExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ChangeOvalityExtDocNo = pcl013.ChangeOvalityExtDocNo;
                objPCL013.ChangeOvalityExtReleasedOn = pcl013.ChangeOvalityExtReleasedOn;
                string ChangeOvalityExtReleasedOn = fc["ChangeOvalityExtReleasedOn"] != null ? fc["ChangeOvalityExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReleasedOn))
                {
                    objPCL013.ChangeOvalityExtReleasedOn = DateTime.ParseExact(ChangeOvalityExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.NozzleDimentionReq = pcl013.NozzleDimentionReq;
                objPCL013.NozzleDimentionReqDate = pcl013.NozzleDimentionReqDate;
                string NozzleDimentionReqDate = fc["NozzleDimentionReqDate"] != null ? fc["NozzleDimentionReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleDimentionReqDate))
                {
                    objPCL013.NozzleDimentionReqDate = DateTime.ParseExact(NozzleDimentionReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.NozzleDimentionDocNo = pcl013.NozzleDimentionDocNo;
                objPCL013.NozzleDimentionReleasedOn = pcl013.NozzleDimentionReleasedOn;
                string NozzleDimentionReleasedOn = fc["NozzleDimentionReleasedOn"] != null ? fc["NozzleDimentionReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleDimentionReleasedOn))
                {
                    objPCL013.NozzleDimentionReleasedOn = DateTime.ParseExact(NozzleDimentionReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShrinkTSRReq = pcl013.ShrinkTSRReq;
                objPCL013.ShrinkTSRReqDate = pcl013.ShrinkTSRReqDate;
                string ShrinkTSRReqDate = fc["ShrinkTSRReqDate"] != null ? fc["ShrinkTSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReqDate))
                {
                    objPCL013.ShrinkTSRReqDate = DateTime.ParseExact(ShrinkTSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShrinkTSRDocNo = pcl013.ShrinkTSRDocNo;
                objPCL013.ShrinkTSRReleasedOn = pcl013.ShrinkTSRReleasedOn;
                string ShrinkTSRReleasedOn = fc["ShrinkTSRReleasedOn"] != null ? fc["ShrinkTSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReleasedOn))
                {
                    objPCL013.ShrinkTSRReleasedOn = DateTime.ParseExact(ShrinkTSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ChangeOvalityISRReq = pcl013.ChangeOvalityISRReq;
                objPCL013.ChangeOvalityISRReqDate = pcl013.ChangeOvalityISRReqDate;
                string ChangeOvalityISRReqDate = fc["ChangeOvalityISRReqDate"] != null ? fc["ChangeOvalityISRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReqDate))
                {
                    objPCL013.ChangeOvalityISRReqDate = DateTime.ParseExact(ChangeOvalityISRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ChangeOvalityISRDocNo = pcl013.ChangeOvalityISRDocNo;
                objPCL013.ChangeOvalityISRReleasedOn = pcl013.ChangeOvalityISRReleasedOn;
                string ChangeOvalityISRReleasedOn = fc["ChangeOvalityISRReleasedOn"] != null ? fc["ChangeOvalityISRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReleasedOn))
                {
                    objPCL013.ChangeOvalityISRReleasedOn = DateTime.ParseExact(ChangeOvalityISRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.DEendLongSeamReq = pcl013.DEendLongSeamReq;
                objPCL013.DEendLongSeamReqDate = pcl013.DEendLongSeamReqDate;
                string DEendLongSeamReqDate = fc["DEendLongSeamReqDate"] != null ? fc["DEendLongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReqDate))
                {
                    objPCL013.DEendLongSeamReqDate = DateTime.ParseExact(DEendLongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.DEendLongSeamDocNo = pcl013.DEendLongSeamDocNo;
                objPCL013.DEendLongSeamReleasedOn = pcl013.DEendLongSeamReleasedOn;
                string DEendLongSeamReleasedOn = fc["DEendLongSeamReleasedOn"] != null ? fc["DEendLongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReleasedOn))
                {
                    objPCL013.DEendLongSeamReleasedOn = DateTime.ParseExact(DEendLongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.GapReq = pcl013.GapReq;
                objPCL013.GapReqDate = pcl013.GapReqDate;
                string GapReqDate = fc["GapReqDate"] != null ? fc["GapReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(GapReqDate))
                {
                    objPCL013.GapReqDate = DateTime.ParseExact(GapReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.GapDocNo = pcl013.GapDocNo;
                objPCL013.GapReleasedOn = pcl013.GapReleasedOn;
                string GapReleasedOn = fc["GapReleasedOn"] != null ? fc["GapReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(GapReleasedOn))
                {
                    objPCL013.GapReleasedOn = DateTime.ParseExact(GapReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellLinerReq = pcl013.ShellLinerReq;
                objPCL013.ShellLinerReqDate = pcl013.ShellLinerReqDate;
                string ShellLinerReqDate = fc["ShellLinerReqDate"] != null ? fc["ShellLinerReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLinerReqDate))
                {
                    objPCL013.ShellLinerReqDate = DateTime.ParseExact(ShellLinerReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellLinerDocNo = pcl013.ShellLinerDocNo;
                objPCL013.ShellLinerReleasedOn = pcl013.ShellLinerReleasedOn;
                string ShellLinerReleasedOn = fc["ShellLinerReleasedOn"] != null ? fc["ShellLinerReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLinerReleasedOn))
                {
                    objPCL013.ShellLinerReleasedOn = DateTime.ParseExact(ShellLinerReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellOvalityReq = pcl013.ShellOvalityReq;
                objPCL013.ShellOvalityReqDate = pcl013.ShellOvalityReqDate;
                string ShellOvalityReqDate = fc["ShellOvalityReqDate"] != null ? fc["ShellOvalityReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellOvalityReqDate))
                {
                    objPCL013.ShellOvalityReqDate = DateTime.ParseExact(ShellOvalityReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.ShellOvalityDocNo = pcl013.ShellOvalityDocNo;
                objPCL013.ShellOvalityReleasedOn = pcl013.ShellOvalityReleasedOn;
                string ShellOvalityReleasedOn = fc["ShellOvalityReleasedOn"] != null ? fc["ShellOvalityReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellOvalityReleasedOn))
                {
                    objPCL013.ShellOvalityReleasedOn = DateTime.ParseExact(ShellOvalityReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.SectionLengthReq = pcl013.SectionLengthReq;
                objPCL013.SectionLengthReqDate = pcl013.SectionLengthReqDate;
                string SectionLengthReqDate = fc["SectionLengthReqDate"] != null ? fc["SectionLengthReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SectionLengthReqDate))
                {
                    objPCL013.SectionLengthReqDate = DateTime.ParseExact(SectionLengthReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL013.SectionLengthDocNo = pcl013.SectionLengthDocNo;
                objPCL013.SectionLengthReleasedOn = pcl013.SectionLengthReleasedOn;
                string SectionLengthReleasedOn = fc["SectionLengthReleasedOn"] != null ? fc["SectionLengthReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SectionLengthReleasedOn))
                {
                    objPCL013.SectionLengthReleasedOn = DateTime.ParseExact(SectionLengthReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }


                if (pcl013.LineId > 0)
                {
                    objPCL013.EditedBy = objClsLoginInfo.UserName;
                    objPCL013.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL013.CreatedBy = objClsLoginInfo.UserName;
                    objPCL013.CreatedOn = DateTime.Now;
                    db.PCL013.Add(objPCL013);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL013.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Coke Drum

        [HttpPost]
        public ActionResult SaveCDConstructor(PCL018 pcl018, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl018.HeaderId).FirstOrDefault();
                PCL018 objPCL018 = new PCL018();
                if (pcl018.LineId > 0)
                {
                    objPCL018 = db.PCL018.Where(x => x.LineId == pcl018.LineId).FirstOrDefault();
                }
                objPCL018.HeaderId = objPCL001.HeaderId;
                objPCL018.Project = objPCL001.Project;
                objPCL018.Document = objPCL001.Document;

                objPCL018.ReactorID = pcl018.ReactorID;
                objPCL018.ReactorThk1 = pcl018.ReactorThk1;
                objPCL018.ReactorTLTL = pcl018.ReactorTLTL;
                objPCL018.ReactorLg = pcl018.ReactorLg;
                objPCL018.ReactorFabWt = pcl018.ReactorFabWt;
                objPCL018.ReactorHydroWt = pcl018.ReactorHydroWt;
                objPCL018.ReactorMaxShellWd = pcl018.ReactorMaxShellWd;
                objPCL018.IsDesigned = pcl018.IsDesigned;
                objPCL018.ShellConst1Seg = pcl018.ShellConst1Seg;
                objPCL018.ShellConst2Seg = pcl018.ShellConst2Seg;
                objPCL018.ShellConst3Seg = pcl018.ShellConst3Seg;
                objPCL018.TypeRollCold = pcl018.TypeRollCold;
                objPCL018.TypeRollWarm = pcl018.TypeRollWarm;
                objPCL018.TypeRollHot = pcl018.TypeRollHot;
                objPCL018.GasCuttingPreHeat = pcl018.GasCuttingPreHeat;
                objPCL018.GasCuttingPreHeatNR = pcl018.GasCuttingPreHeatNR;
                objPCL018.CSeamWEPPlate = pcl018.CSeamWEPPlate;
                objPCL018.CSeamWEPAfter = pcl018.CSeamWEPAfter;
                objPCL018.PTCRequiredforLS = pcl018.PTCRequiredforLS;
                objPCL018.PTCRequiredforCS = pcl018.PTCRequiredforCS;
                objPCL018.PTCRequiredforLSDend = pcl018.PTCRequiredforLSDend;
                objPCL018.PTCRequiredforCSDend = pcl018.PTCRequiredforCSDend;
                objPCL018.PTCRequiredforCSNozz = pcl018.PTCRequiredforCSNozz;
                objPCL018.ISRRequiredforLSToricone = pcl018.ISRRequiredforLSToricone;
                objPCL018.ISRRequiredforLSCrown = pcl018.ISRRequiredforLSCrown;
                objPCL018.ISRRequiredforCrown = pcl018.ISRRequiredforCrown;
                objPCL018.ISRRequiredforPetal = pcl018.ISRRequiredforPetal;
                objPCL018.ISRRequiredforNozzle = pcl018.ISRRequiredforNozzle;
                objPCL018.PTMTAfterWelding = pcl018.PTMTAfterWelding;
                objPCL018.PTMTAfterOverlay = pcl018.PTMTAfterOverlay;
                objPCL018.PTMTAfterPWHT = pcl018.PTMTAfterPWHT;
                objPCL018.PTMTAfterHydro = pcl018.PTMTAfterHydro;
                objPCL018.RTAfterWelding = pcl018.RTAfterWelding;
                objPCL018.RTAfterOverlay = pcl018.RTAfterOverlay;
                objPCL018.RTAfterPWHT = pcl018.RTAfterPWHT;
                objPCL018.RTAfterHydro = pcl018.RTAfterHydro;
                objPCL018.UTAfterWelding = pcl018.UTAfterWelding;
                objPCL018.UTAfterOverlay = pcl018.UTAfterOverlay;
                objPCL018.UTAfterPWHT = pcl018.UTAfterPWHT;
                objPCL018.UTAfterHydro = pcl018.UTAfterHydro;
                objPCL018.FerAfterWelding = pcl018.FerAfterWelding;
                objPCL018.FerAfterOverlay = pcl018.FerAfterOverlay;
                objPCL018.FerAfterPWHT = pcl018.FerAfterPWHT;
                objPCL018.FerAfterHydro = pcl018.FerAfterHydro;
                objPCL018.FerAfterNA = pcl018.FerAfterNA;
                objPCL018.SOTESSCSingle = pcl018.SOTESSCSingle;
                objPCL018.SOTESSCDouble = pcl018.SOTESSCDouble;
                objPCL018.SOTESSCClade = pcl018.SOTESSCClade;
                objPCL018.SOTESSCOlayNR = pcl018.SOTESSCOlayNR;
                objPCL018.DCPetalWCrown = pcl018.DCPetalWCrown;
                objPCL018.DC4PetalWoCrown = pcl018.DC4PetalWoCrown;
                objPCL018.DCSinglePeice = pcl018.DCSinglePeice;
                objPCL018.DCTwoHalves = pcl018.DCTwoHalves;
                objPCL018.DRCFormedPetals = pcl018.DRCFormedPetals;
                objPCL018.DRCFormedSetup = pcl018.DRCFormedSetup;
                objPCL018.DRCFormedWelded = pcl018.DRCFormedWelded;
                objPCL018.DOTESSCSingle = pcl018.DOTESSCSingle;
                objPCL018.DOTESSCDouble = pcl018.DOTESSCDouble;
                objPCL018.DOTESSCClade = pcl018.DOTESSCClade;
                objPCL018.DOTESSCOlayNR = pcl018.DOTESSCOlayNR;
                objPCL018.SVJYring = pcl018.SVJYring;
                objPCL018.SVJBuildup = pcl018.SVJBuildup;
                objPCL018.SVJLapJoint = pcl018.SVJLapJoint;
                objPCL018.YRTForgedShell = pcl018.YRTForgedShell;
                objPCL018.YRTForgedSegment = pcl018.YRTForgedSegment;
                objPCL018.YRTProfile = pcl018.YRTProfile;
                objPCL018.YROSingle = pcl018.YROSingle;
                objPCL018.YROAfter = pcl018.YROAfter;
                objPCL018.NFFRaised = pcl018.NFFRaised;
                objPCL018.NFFGroove = pcl018.NFFGroove;
                objPCL018.NFFOther = pcl018.NFFOther;
                objPCL018.NGFBfPWHT = pcl018.NGFBfPWHT;
                objPCL018.NGFAfPWHT = pcl018.NGFAfPWHT;
                objPCL018.NGFNR = pcl018.NGFNR;
                objPCL018.SpoolMatSS = pcl018.SpoolMatSS;
                objPCL018.SpoolMatLAS = pcl018.SpoolMatLAS;
                objPCL018.SpoolMatCS = pcl018.SpoolMatCS;
                objPCL018.JSInConnel = pcl018.JSInConnel;
                objPCL018.JSBiMetalic = pcl018.JSBiMetalic;
                objPCL018.JSCladRestoration = pcl018.JSCladRestoration;
                objPCL018.JSCS = pcl018.JSCS;
                objPCL018.VessleSSkirt = pcl018.VessleSSkirt;
                objPCL018.SkirtMadein1 = pcl018.SkirtMadein1;
                objPCL018.SkirtMadein2 = pcl018.SkirtMadein2;
                objPCL018.FPNWeldNuts = pcl018.FPNWeldNuts;
                objPCL018.FPNLeaveRow = pcl018.FPNLeaveRow;
                objPCL018.STWoSlot = pcl018.STWoSlot;
                objPCL018.STWiSlot = pcl018.STWiSlot;
                objPCL018.PWHTSinglePiece = pcl018.PWHTSinglePiece;
                objPCL018.PWHTLSRJoint = pcl018.PWHTLSRJoint;
                objPCL018.PTCMTCptc = pcl018.PTCMTCptc;
                objPCL018.PTCMTCmtc = pcl018.PTCMTCmtc;
                objPCL018.PTCMTCptcSim = pcl018.PTCMTCptcSim;
                objPCL018.PTCMTCmtcSim = pcl018.PTCMTCmtcSim;
                objPCL018.SaddlesTrans = pcl018.SaddlesTrans;
                objPCL018.SaddlesAddit = pcl018.SaddlesAddit;
                objPCL018.PWHTinFurnacePFS = pcl018.PWHTinFurnacePFS;
                objPCL018.PWHTinFurnaceHFS1 = pcl018.PWHTinFurnaceHFS1;
                objPCL018.PWHTinFurnaceLEMF = pcl018.PWHTinFurnaceLEMF;
                objPCL018.SectionWtMore230 = pcl018.SectionWtMore230;
                objPCL018.SectionWt200To230 = pcl018.SectionWt200To230;
                objPCL018.SectionWt150To200 = pcl018.SectionWt150To200;
                objPCL018.SectionWtLess150 = pcl018.SectionWtLess150;
                objPCL018.SectionWt1pc = pcl018.SectionWt1pc;
                objPCL018.SectionWt1Sec = pcl018.SectionWt1Sec;
                objPCL018.SectionWtTtl = pcl018.SectionWtTtl;
                objPCL018.ARMRequired = pcl018.ARMRequired;
                objPCL018.ARMRT = pcl018.ARMRT;
                objPCL018.ARMPWHT = pcl018.ARMPWHT;
                objPCL018.ARMSB = pcl018.ARMSB;
                objPCL018.CLRH50ppm = pcl018.CLRH50ppm;
                objPCL018.CLRHNoLimit = pcl018.CLRHNoLimit;
                objPCL018.TemplateForReq = pcl018.TemplateForReq;
                objPCL018.TemplateForNReq = pcl018.TemplateForNReq;
                objPCL018.N2FillReq = pcl018.N2FillReq;
                objPCL018.N2FillNotReq = pcl018.N2FillNotReq;
                objPCL018.N2FillOther = pcl018.N2FillOther;
                objPCL018.ESTPlate = pcl018.ESTPlate;
                objPCL018.ESTShell = pcl018.ESTShell;
                objPCL018.ESTSection = pcl018.ESTSection;
                objPCL018.ESTEquip = pcl018.ESTEquip;
                objPCL018.ISTPlate = pcl018.ISTPlate;
                objPCL018.ISTShell = pcl018.ISTShell;
                objPCL018.ISTSection = pcl018.ISTSection;
                objPCL018.ISTEquip = pcl018.ISTEquip;
                objPCL018.ISTAfBfPWHT = pcl018.ISTAfBfPWHT;
                objPCL018.ISTAfHydro = pcl018.ISTAfHydro;
                objPCL018.CDNSS = pcl018.CDNSS;
                objPCL018.CDNLAS = pcl018.CDNLAS;
                objPCL018.CDNCS = pcl018.CDNCS;
                objPCL018.CDNNotReq = pcl018.CDNNotReq;
                objPCL018.MDNAfterOverlay = pcl018.MDNAfterOverlay;
                objPCL018.MDNAfterPWHT = pcl018.MDNAfterPWHT;
                objPCL018.MDNAfterLSR = pcl018.MDNAfterLSR;
                objPCL018.MaxSBLess1_38 = pcl018.MaxSBLess1_38;
                objPCL018.MaxSBLess1_78 = pcl018.MaxSBLess1_78;
                objPCL018.MaxSBLess2_14 = pcl018.MaxSBLess2_14;
                objPCL018.MaxSBLess4 = pcl018.MaxSBLess4;
                objPCL018.MaxSBMore4 = pcl018.MaxSBMore4;
                objPCL018.TypeRollWarmVal = pcl018.TypeRollWarmVal;
                objPCL018.TypeRollHotVal = pcl018.TypeRollHotVal;
                objPCL018.TypeRollHotmin = pcl018.TypeRollHotmin;
                objPCL018.YRTForgedSegmentVal = pcl018.YRTForgedSegmentVal;
                objPCL018.STWiSlotVal = pcl018.STWiSlotVal;
                objPCL018.PWHTLSRJointVal = pcl018.PWHTLSRJointVal;
                objPCL018.SectionWtTtlVal = pcl018.SectionWtTtlVal;
                objPCL018.CLRH50ppmVal = pcl018.CLRH50ppmVal;



                if (pcl018.LineId > 0)
                {
                    objPCL018.EditedBy = objClsLoginInfo.UserName;
                    objPCL018.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL018.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL018.CreatedBy = objClsLoginInfo.UserName;
                    objPCL018.CreatedOn = DateTime.Now;
                    db.PCL018.Add(objPCL018);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL018.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCDAllowance(PCL019 pcl019, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl019.HeaderId).FirstOrDefault();
                PCL019 objPCL019 = new PCL019();
                if (pcl019.LineId > 0)
                {
                    objPCL019 = db.PCL019.Where(x => x.LineId == pcl019.LineId).FirstOrDefault();
                }
                objPCL019.HeaderId = objPCL001.HeaderId;
                objPCL019.Project = objPCL001.Project;
                objPCL019.Document = objPCL001.Document;
                objPCL019.ShellAllwEB = pcl019.ShellAllwEB;
                objPCL019.ShellShrinkage = pcl019.ShellShrinkage;
                objPCL019.ShellWidth = pcl019.ShellWidth;
                objPCL019.Shelllong = pcl019.Shelllong;
                objPCL019.Shellcirc = pcl019.Shellcirc;
                objPCL019.NegAllow = pcl019.NegAllow;
                objPCL019.AllowNub = pcl019.AllowNub;
                objPCL019.AllowTSR = pcl019.AllowTSR;
                objPCL019.TOvality = pcl019.TOvality;
                objPCL019.TOutRoundness = pcl019.TOutRoundness;
                objPCL019.TCircum = pcl019.TCircum;
                objPCL019.DEndMcing = pcl019.DEndMcing;
                objPCL019.DEndSeam = pcl019.DEndSeam;
                objPCL019.DEndCirc = pcl019.DEndCirc;
                objPCL019.YRingESSC = pcl019.YRingESSC;
                objPCL019.SkirtTol = pcl019.SkirtTol;
                objPCL019.CTRPeak = pcl019.CTRPeak;
                objPCL019.CTRMaxOffset = pcl019.CTRMaxOffset;
                objPCL019.CTROutRound = pcl019.CTROutRound;
                objPCL019.CTRTDE = pcl019.CTRTDE;
                objPCL019.CTRDump = pcl019.CTRDump;
                objPCL019.CTRMaxAllow = pcl019.CTRMaxAllow;

                if (pcl019.LineId > 0)
                {
                    objPCL019.EditedBy = objClsLoginInfo.UserName;
                    objPCL019.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL019.CreatedBy = objClsLoginInfo.UserName;
                    objPCL019.CreatedOn = DateTime.Now;
                    db.PCL019.Add(objPCL019);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL019.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCDScopeOfWork(PCL020 pcl020, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl020.HeaderId).FirstOrDefault();
                PCL020 objPCL020 = new PCL020();
                if (pcl020.LineId > 0)
                {
                    objPCL020 = db.PCL020.Where(x => x.LineId == pcl020.LineId).FirstOrDefault();
                }
                objPCL020.HeaderId = objPCL001.HeaderId;
                objPCL020.Project = objPCL001.Project;
                objPCL020.Document = objPCL001.Document;
                objPCL020.EquipmentNotReq = pcl020.EquipmentNotReq;
                objPCL020.EquipmentPC = pcl020.EquipmentPC;
                objPCL020.EquipmentMC = pcl020.EquipmentMC;
                objPCL020.EquipmentHPC3 = pcl020.EquipmentHPC3;
                objPCL020.EquipmentRNW = pcl020.EquipmentRNW;
                objPCL020.EquipmentOutside = pcl020.EquipmentOutside;
                objPCL020.DEndFormingNotReq = pcl020.DEndFormingNotReq;
                objPCL020.DEndFormingPC = pcl020.DEndFormingPC;
                objPCL020.DEndFormingMC = pcl020.DEndFormingMC;
                objPCL020.DEndFormingHPC3 = pcl020.DEndFormingHPC3;
                objPCL020.DEndFormingRNW = pcl020.DEndFormingRNW;
                objPCL020.DEndFormingOutside = pcl020.DEndFormingOutside;
                objPCL020.DEndSetupNotReq = pcl020.DEndSetupNotReq;
                objPCL020.DEndSetupPC = pcl020.DEndSetupPC;
                objPCL020.DEndSetupMC = pcl020.DEndSetupMC;
                objPCL020.DEndSetupHPC3 = pcl020.DEndSetupHPC3;
                objPCL020.DEndSetupRNW = pcl020.DEndSetupRNW;
                objPCL020.DEndSetupOutside = pcl020.DEndSetupOutside;
                objPCL020.DEndWeldingNotReq = pcl020.DEndWeldingNotReq;
                objPCL020.DEndWeldingPC = pcl020.DEndWeldingPC;
                objPCL020.DEndWeldingMC = pcl020.DEndWeldingMC;
                objPCL020.DEndWeldingHPC3 = pcl020.DEndWeldingHPC3;
                objPCL020.DEndWeldingRNW = pcl020.DEndWeldingRNW;
                objPCL020.DEndWeldingOutside = pcl020.DEndWeldingOutside;
                objPCL020.SkirtNotReq = pcl020.SkirtNotReq;
                objPCL020.SkirtPC = pcl020.SkirtPC;
                objPCL020.SkirtMC = pcl020.SkirtMC;
                objPCL020.SkirtHPC3 = pcl020.SkirtHPC3;
                objPCL020.SkirtRNW = pcl020.SkirtRNW;
                objPCL020.SkirtOutside = pcl020.SkirtOutside;
                objPCL020.YRingLSNotReq = pcl020.YRingLSNotReq;
                objPCL020.YRingLSPC = pcl020.YRingLSPC;
                objPCL020.YRingLSMC = pcl020.YRingLSMC;
                objPCL020.YRingLSHPC3 = pcl020.YRingLSHPC3;
                objPCL020.YRingLSRNW = pcl020.YRingLSRNW;
                objPCL020.YRingLSOutside = pcl020.YRingLSOutside;
                objPCL020.YRingOverlayNotReq = pcl020.YRingOverlayNotReq;
                objPCL020.YRingOverlayPC = pcl020.YRingOverlayPC;
                objPCL020.YRingOverlayMC = pcl020.YRingOverlayMC;
                objPCL020.YRingOverlayHPC3 = pcl020.YRingOverlayHPC3;
                objPCL020.YRingOverlayRNW = pcl020.YRingOverlayRNW;
                objPCL020.YRingOverlayOutside = pcl020.YRingOverlayOutside;
                objPCL020.YRingMachiningNotReq = pcl020.YRingMachiningNotReq;
                objPCL020.YRingMachiningPC = pcl020.YRingMachiningPC;
                objPCL020.YRingMachiningMC = pcl020.YRingMachiningMC;
                objPCL020.YRingMachiningHPC3 = pcl020.YRingMachiningHPC3;
                objPCL020.YRingMachiningRNW = pcl020.YRingMachiningRNW;
                objPCL020.YRingMachiningOutside = pcl020.YRingMachiningOutside;
                objPCL020.NozMachiningNotReq = pcl020.NozMachiningNotReq;
                objPCL020.NozMachiningPC = pcl020.NozMachiningPC;
                objPCL020.NozMachiningMC = pcl020.NozMachiningMC;
                objPCL020.NozMachiningHPC3 = pcl020.NozMachiningHPC3;
                objPCL020.NozMachiningRNW = pcl020.NozMachiningRNW;
                objPCL020.NozMachiningOutside = pcl020.NozMachiningOutside;
                objPCL020.NozOverlayNotReq = pcl020.NozOverlayNotReq;
                objPCL020.NozOverlayMC = pcl020.NozOverlayMC;
                objPCL020.NozOverlayPC = pcl020.NozOverlayPC;
                objPCL020.NozOverlayHPC3 = pcl020.NozOverlayHPC3;
                objPCL020.NozOverlayRNW = pcl020.NozOverlayRNW;
                objPCL020.NozOverlayOutside = pcl020.NozOverlayOutside;
                objPCL020.ManOverlayNotReq = pcl020.ManOverlayNotReq;
                objPCL020.ManOverlayPC = pcl020.ManOverlayPC;
                objPCL020.ManOverlayMC = pcl020.ManOverlayMC;
                objPCL020.ManOverlayHPC3 = pcl020.ManOverlayHPC3;
                objPCL020.ManOverlayRNW = pcl020.ManOverlayRNW;
                objPCL020.ManOverlayOutside = pcl020.ManOverlayOutside;
                objPCL020.NozFinMachiningNotReq = pcl020.NozFinMachiningNotReq;
                objPCL020.NozFinMachiningPC = pcl020.NozFinMachiningPC;
                objPCL020.NozFinMachiningMC = pcl020.NozFinMachiningMC;
                objPCL020.NozFinMachiningHPC3 = pcl020.NozFinMachiningHPC3;
                objPCL020.NozFinMachiningRNW = pcl020.NozFinMachiningRNW;
                objPCL020.NozFinMachiningOutside = pcl020.NozFinMachiningOutside;
                objPCL020.NozWEPNotReq = pcl020.NozWEPNotReq;
                objPCL020.NozWEPPC = pcl020.NozWEPPC;
                objPCL020.NozWEPMC = pcl020.NozWEPMC;
                objPCL020.NozWEPHPC3 = pcl020.NozWEPHPC3;
                objPCL020.NozWEPRNW = pcl020.NozWEPRNW;
                objPCL020.NozWEPOutside = pcl020.NozWEPOutside;
                objPCL020.LongWEPNotReq = pcl020.LongWEPNotReq;
                objPCL020.LongWEPPC = pcl020.LongWEPPC;
                objPCL020.LongWEPMC = pcl020.LongWEPMC;
                objPCL020.LongWEPHPC3 = pcl020.LongWEPHPC3;
                objPCL020.LongWEPRNW = pcl020.LongWEPRNW;
                objPCL020.LongWEPOutside = pcl020.LongWEPOutside;
                objPCL020.CircWEPNotReq = pcl020.CircWEPNotReq;
                objPCL020.CircWEPPC = pcl020.CircWEPPC;
                objPCL020.CircWEPMC = pcl020.CircWEPMC;
                objPCL020.CircWEPHPC3 = pcl020.CircWEPHPC3;
                objPCL020.CircWEPRNW = pcl020.CircWEPRNW;
                objPCL020.CircWEPOutside = pcl020.CircWEPOutside;
                objPCL020.BailingNotReq = pcl020.BailingNotReq;
                objPCL020.BailingPC = pcl020.BailingPC;
                objPCL020.BailingMC = pcl020.BailingMC;
                objPCL020.BailingHPC3 = pcl020.BailingHPC3;
                objPCL020.BailingRNW = pcl020.BailingRNW;
                objPCL020.BailingOutside = pcl020.BailingOutside;
                objPCL020.LiftFabricationNotReq = pcl020.LiftFabricationNotReq;
                objPCL020.LiftFabricationPC = pcl020.LiftFabricationPC;
                objPCL020.LiftFabricationMC = pcl020.LiftFabricationMC;
                objPCL020.LiftFabricationHPC3 = pcl020.LiftFabricationHPC3;
                objPCL020.LiftFabricationRNW = pcl020.LiftFabricationRNW;
                objPCL020.LiftFabricationOutside = pcl020.LiftFabricationOutside;
                objPCL020.WeldExFabNotReq = pcl020.WeldExFabNotReq;
                objPCL020.WeldExFabPC = pcl020.WeldExFabPC;
                objPCL020.WeldExFabMC = pcl020.WeldExFabMC;
                objPCL020.WeldExFabHPC3 = pcl020.WeldExFabHPC3;
                objPCL020.WeldExFabRNW = pcl020.WeldExFabRNW;
                objPCL020.WeldExFabOutside = pcl020.WeldExFabOutside;
                objPCL020.WeldInFabOutside = pcl020.WeldInFabOutside;
                objPCL020.WeldInFabNotReq = pcl020.WeldInFabNotReq;
                objPCL020.WeldInFabPC = pcl020.WeldInFabPC;
                objPCL020.WeldInFabMC = pcl020.WeldInFabMC;
                objPCL020.WeldInFabHPC3 = pcl020.WeldInFabHPC3;
                objPCL020.WeldInFabRNW = pcl020.WeldInFabRNW;
                objPCL020.InsFabNotReq = pcl020.InsFabNotReq;
                objPCL020.InsFabPC = pcl020.InsFabPC;
                objPCL020.InsFabMC = pcl020.InsFabMC;
                objPCL020.InsFabHPC3 = pcl020.InsFabHPC3;
                objPCL020.InsFabRNW = pcl020.InsFabRNW;
                objPCL020.InsFabOutside = pcl020.InsFabOutside;
                objPCL020.SaddlesNotReq = pcl020.SaddlesNotReq;
                objPCL020.SaddlesPC = pcl020.SaddlesPC;
                objPCL020.SaddlesMC = pcl020.SaddlesMC;
                objPCL020.SaddlesHPC3 = pcl020.SaddlesHPC3;
                objPCL020.SaddlesRNW = pcl020.SaddlesRNW;
                objPCL020.SaddlesOutside = pcl020.SaddlesOutside;
                objPCL020.SkirtTemplateNotReq = pcl020.SkirtTemplateNotReq;
                objPCL020.SkirtTemplatePC = pcl020.SkirtTemplatePC;
                objPCL020.SkirtTemplateMC = pcl020.SkirtTemplateMC;
                objPCL020.SkirtTemplateHPC3 = pcl020.SkirtTemplateHPC3;
                objPCL020.SkirtTemplateRNW = pcl020.SkirtTemplateRNW;
                objPCL020.SkirtTemplateOutside = pcl020.SkirtTemplateOutside;
                objPCL020.SkirtSlottingNotReq = pcl020.SkirtSlottingNotReq;
                objPCL020.SkirtSlottingPC = pcl020.SkirtSlottingPC;
                objPCL020.SkirtSlottingMC = pcl020.SkirtSlottingMC;
                objPCL020.SkirtSlottingHPC3 = pcl020.SkirtSlottingHPC3;
                objPCL020.SkirtSlottingRNW = pcl020.SkirtSlottingRNW;
                objPCL020.SkirtSlottingOutside = pcl020.SkirtSlottingOutside;
                objPCL020.PWHTNotReq = pcl020.PWHTNotReq;
                objPCL020.PWHTPC = pcl020.PWHTPC;
                objPCL020.PWHTMC = pcl020.PWHTMC;
                objPCL020.PWHTHPC3 = pcl020.PWHTHPC3;
                objPCL020.PWHTRNW = pcl020.PWHTRNW;
                objPCL020.PWHTOutside = pcl020.PWHTOutside;
                objPCL020.N2FillNotReq = pcl020.N2FillNotReq;
                objPCL020.N2FillPC = pcl020.N2FillPC;
                objPCL020.N2FillMC = pcl020.N2FillMC;
                objPCL020.N2FillHPC3 = pcl020.N2FillHPC3;
                objPCL020.N2FillRNW = pcl020.N2FillRNW;
                objPCL020.N2FillOutside = pcl020.N2FillOutside;
                objPCL020.SandBlastingNotReq = pcl020.SandBlastingNotReq;
                objPCL020.SandBlastingPC = pcl020.SandBlastingPC;
                objPCL020.SandBlastingMC = pcl020.SandBlastingMC;
                objPCL020.SandBlastingHPC3 = pcl020.SandBlastingHPC3;
                objPCL020.SandBlastingRNW = pcl020.SandBlastingRNW;
                objPCL020.SandBlastingOutside = pcl020.SandBlastingOutside;
                objPCL020.PaintingNotReq = pcl020.PaintingNotReq;
                objPCL020.PaintingPC = pcl020.PaintingPC;
                objPCL020.PaintingMC = pcl020.PaintingMC;
                objPCL020.PaintingHPC3 = pcl020.PaintingHPC3;
                objPCL020.PaintingRNW = pcl020.PaintingRNW;
                objPCL020.PaintingOutside = pcl020.PaintingOutside;
                objPCL020.AcidCleaningNotReq = pcl020.AcidCleaningNotReq;
                objPCL020.AcidCleaningPC = pcl020.AcidCleaningPC;
                objPCL020.AcidCleaningMC = pcl020.AcidCleaningMC;
                objPCL020.AcidCleaningHPC3 = pcl020.AcidCleaningHPC3;
                objPCL020.AcidCleaningRNW = pcl020.AcidCleaningRNW;
                objPCL020.AcidCleaningOutside = pcl020.AcidCleaningOutside;
                objPCL020.WeightMeasurementNotReq = pcl020.WeightMeasurementPC;
                objPCL020.WeightMeasurementPC = pcl020.WeightMeasurementPC;
                objPCL020.WeightMeasurementMC = pcl020.WeightMeasurementMC;
                objPCL020.WeightMeasurementHPC3 = pcl020.WeightMeasurementHPC3;
                objPCL020.WeightMeasurementRNW = pcl020.WeightMeasurementRNW;
                objPCL020.WeightMeasurementOutside = pcl020.WeightMeasurementOutside;
                objPCL020.ManwayDavitNotReq = pcl020.ManwayDavitNotReq;
                objPCL020.ManwayDavitPC = pcl020.ManwayDavitPC;
                objPCL020.ManwayDavitMC = pcl020.ManwayDavitMC;
                objPCL020.ManwayDavitHPC3 = pcl020.ManwayDavitHPC3;
                objPCL020.ManwayDavitRNW = pcl020.ManwayDavitRNW;
                objPCL020.ManwayDavitOutside = pcl020.ManwayDavitOutside;
                objPCL020.VesselDavitNotReq = pcl020.VesselDavitNotReq;
                objPCL020.VesselDavitPC = pcl020.VesselDavitPC;
                objPCL020.VesselDavitMC = pcl020.VesselDavitMC;
                objPCL020.VesselDavitHPC3 = pcl020.VesselDavitHPC3;
                objPCL020.VesselDavitRNW = pcl020.VesselDavitRNW;
                objPCL020.VesselDavitOutside = pcl020.VesselDavitOutside;
                objPCL020.LoadTestNotReq = pcl020.LoadTestNotReq;
                objPCL020.LoadTestPC = pcl020.LoadTestPC;
                objPCL020.LoadTestMC = pcl020.LoadTestMC;
                objPCL020.LoadTestHPC3 = pcl020.LoadTestHPC3;
                objPCL020.LoadTestRNW = pcl020.LoadTestRNW;
                objPCL020.LoadTestOutside = pcl020.LoadTestOutside;
                objPCL020.DumpNotReq = pcl020.DumpNotReq;
                objPCL020.DumpPC = pcl020.DumpPC;
                objPCL020.DumpMC = pcl020.DumpMC;
                objPCL020.DumpHPC3 = pcl020.DumpHPC3;
                objPCL020.DumpRNW = pcl020.DumpRNW;
                objPCL020.DumpOutside = pcl020.DumpOutside;
                objPCL020.ShippingNotReq = pcl020.ShippingNotReq;
                objPCL020.ShippingPC = pcl020.ShippingPC;
                objPCL020.ShippingMC = pcl020.ShippingMC;
                objPCL020.ShippingHPC3 = pcl020.ShippingHPC3;
                objPCL020.ShippingRNW = pcl020.ShippingRNW;
                objPCL020.ShippingOutside = pcl020.ShippingOutside;

                if (pcl020.LineId > 0)
                {
                    objPCL020.EditedBy = objClsLoginInfo.UserName;
                    objPCL020.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL020.CreatedBy = objClsLoginInfo.UserName;
                    objPCL020.CreatedOn = DateTime.Now;
                    db.PCL020.Add(objPCL020);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL020.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult SaveCDDocument(PCL021 pcl021, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl021.HeaderId).FirstOrDefault();
                PCL021 objPCL021 = new PCL021();
                if (pcl021.LineId > 0)
                {
                    objPCL021 = db.PCL021.Where(x => x.LineId == pcl021.LineId).FirstOrDefault();
                }
                objPCL021.HeaderId = objPCL001.HeaderId;
                objPCL021.Project = objPCL001.Project;
                objPCL021.Document = objPCL001.Document;
                objPCL021.JPPReq = pcl021.JPPReq;
                objPCL021.JPPReqDate = pcl021.JPPReqDate;
                string JPPReqDate = fc["JPPReqDate"] != null ? fc["JPPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReqDate))
                {
                    objPCL021.JPPReqDate = DateTime.ParseExact(JPPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL021.JPPDocNo = pcl021.JPPDocNo;
                objPCL021.JPPReleasedOn = pcl021.JPPReleasedOn;
                string JPPReleasedOn = fc["JPPReleasedOn"] != null ? fc["JPPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReleasedOn))
                {
                    objPCL021.JPPReleasedOn = DateTime.ParseExact(JPPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL021.LineSketchReq = pcl021.LineSketchReq;
                objPCL021.LineSketchReqDate = pcl021.LineSketchReqDate;

                string LineSketchReqDate = fc["LineSketchReqDate"] != null ? fc["LineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineSketchReqDate))
                {
                    objPCL021.LineSketchReqDate = DateTime.ParseExact(LineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL021.LineSketchDocNo = pcl021.LineSketchDocNo;
                objPCL021.LineSketchReleasedOn = pcl021.LineSketchReleasedOn;
                string LineSketchReleasedOn = fc["LineSketchReleasedOn"] != null ? fc["LineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineSketchReleasedOn))
                {
                    objPCL021.LineSketchReleasedOn = DateTime.ParseExact(LineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RollingCapaReq = pcl021.RollingCapaReq;
                objPCL021.RollingCapaReqDate = pcl021.RollingCapaReqDate;
                string RollingCapaReqDate = fc["RollingCapaReqDate"] != null ? fc["RollingCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReqDate))
                {
                    objPCL021.RollingCapaReqDate = DateTime.ParseExact(RollingCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RollingCapaDocNo = pcl021.RollingCapaDocNo;
                objPCL021.RollingCapaReleasedOn = pcl021.RollingCapaReleasedOn;
                string RollingCapaReleasedOn = fc["RollingCapaReleasedOn"] != null ? fc["RollingCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReleasedOn))
                {
                    objPCL021.RollingCapaReleasedOn = DateTime.ParseExact(RollingCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.ImprovBudgetReq = pcl021.ImprovBudgetReq;
                objPCL021.ImprovBudgetReqDate = pcl021.ImprovBudgetReqDate;
                string ImprovBudgetReqDate = fc["ImprovBudgetReqDate"] != null ? fc["ImprovBudgetReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReqDate))
                {
                    objPCL021.ImprovBudgetReqDate = DateTime.ParseExact(ImprovBudgetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.ImprovBudgetDocNo = pcl021.ImprovBudgetDocNo;
                objPCL021.ImprovBudgetReleasedOn = pcl021.ImprovBudgetReleasedOn;
                string ImprovBudgetReleasedOn = fc["ImprovBudgetReleasedOn"] != null ? fc["ImprovBudgetReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReleasedOn))
                {
                    objPCL021.ImprovBudgetReleasedOn = DateTime.ParseExact(ImprovBudgetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RCCPBreakUpReq = pcl021.RCCPBreakUpReq;
                objPCL021.RCCPBreakUpReqDate = pcl021.RCCPBreakUpReqDate;
                string RCCPBreakUpReqDate = fc["RCCPBreakUpReqDate"] != null ? fc["RCCPBreakUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReqDate))
                {
                    objPCL021.RCCPBreakUpReqDate = DateTime.ParseExact(RCCPBreakUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RCCPBreakUpDocNo = pcl021.RCCPBreakUpDocNo;
                objPCL021.RCCPBreakUpReleasedOn = pcl021.RCCPBreakUpReleasedOn;
                string RCCPBreakUpReleasedOn = fc["RCCPBreakUpReleasedOn"] != null ? fc["RCCPBreakUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReleasedOn))
                {
                    objPCL021.RCCPBreakUpReleasedOn = DateTime.ParseExact(RCCPBreakUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SchedConcertoReq = pcl021.SchedConcertoReq;
                objPCL021.SchedConcertoReqDate = pcl021.SchedConcertoReqDate;
                string SchedConcertoReqDate = fc["SchedConcertoReqDate"] != null ? fc["SchedConcertoReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReqDate))
                {
                    objPCL021.SchedConcertoReqDate = DateTime.ParseExact(SchedConcertoReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SchedConcertoDocNo = pcl021.SchedConcertoDocNo;
                objPCL021.SchedConcertoReleasedOn = pcl021.SchedConcertoReleasedOn;
                string SchedConcertoReleasedOn = fc["SchedConcertoReleasedOn"] != null ? fc["SchedConcertoReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReleasedOn))
                {
                    objPCL021.SchedConcertoReleasedOn = DateTime.ParseExact(SchedConcertoReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SchedExlReq = pcl021.SchedExlReq;
                objPCL021.SchedExlReqDate = pcl021.SchedExlReqDate;
                string SchedExlReqDate = fc["SchedExlReqDate"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReqDate))
                {
                    objPCL021.SchedExlReqDate = DateTime.ParseExact(SchedExlReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SchedExlDocNo = pcl021.SchedExlDocNo;
                objPCL021.SchedExlReleasedOn = pcl021.SchedExlReleasedOn;
                string SchedExlReleasedOn = fc["SchedExlReleasedOn"] != null ? fc["SchedExlReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReleasedOn))
                {
                    objPCL021.SchedExlReleasedOn = DateTime.ParseExact(SchedExlReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SubcontPlanReq = pcl021.SubcontPlanReq;
                objPCL021.SubcontPlanReqDate = pcl021.SubcontPlanReqDate;
                string SubcontPlanReqDate = fc["SubcontPlanReqDate"] != null ? fc["SubcontPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReqDate))
                {
                    objPCL021.SubcontPlanReqDate = DateTime.ParseExact(SubcontPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SubcontPlanDocNo = pcl021.SubcontPlanDocNo;
                objPCL021.SubcontPlanReleasedOn = pcl021.SubcontPlanReleasedOn;
                string SubcontPlanReleasedOn = fc["SubcontPlanReleasedOn"] != null ? fc["SubcontPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReleasedOn))
                {
                    objPCL021.SubcontPlanReleasedOn = DateTime.ParseExact(SubcontPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.LongSeamReq = pcl021.LongSeamReq;
                objPCL021.LongSeamReqDate = pcl021.LongSeamReqDate;
                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL021.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.LongSeamDocNo = pcl021.LongSeamDocNo;
                objPCL021.LongSeamReleasedOn = pcl021.LongSeamReleasedOn;
                string LongSeamReleasedOn = fc["LongSeamReleasedOn"] != null ? fc["LongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReleasedOn))
                {
                    objPCL021.LongSeamReleasedOn = DateTime.ParseExact(LongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.CircSeamReq = pcl021.CircSeamReq;
                objPCL021.CircSeamReqDate = pcl021.CircSeamReqDate;
                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL021.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.CircSeamDocNo = pcl021.CircSeamDocNo;
                objPCL021.CircSeamReleasedOn = pcl021.CircSeamReleasedOn;
                string CircSeamReleasedOn = fc["CircSeamReleasedOn"] != null ? fc["CircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReleasedOn))
                {
                    objPCL021.CircSeamReleasedOn = DateTime.ParseExact(CircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.NozzleShellReq = pcl021.NozzleShellReq;
                objPCL021.NozzleShellReqDate = pcl021.NozzleShellReqDate;
                string NozzleShellReqDate = fc["NozzleShellReqDate"] != null ? fc["NozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReqDate))
                {
                    objPCL021.NozzleShellReqDate = DateTime.ParseExact(NozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.NozzleShellDocNo = pcl021.NozzleShellDocNo;
                objPCL021.NozzleShellReleasedOn = pcl021.NozzleShellReleasedOn;
                string NozzleShellReleasedOn = fc["NozzleShellReleasedOn"] != null ? fc["NozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReleasedOn))
                {
                    objPCL021.NozzleShellReleasedOn = DateTime.ParseExact(NozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RollCapaReq = pcl021.RollCapaReq;
                objPCL021.RollCapaReqDate = pcl021.RollCapaReqDate;
                string RollCapaReqDate = fc["RollCapaReqDate"] != null ? fc["RollCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReqDate))
                {
                    objPCL021.RollCapaReqDate = DateTime.ParseExact(RollCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RollCapaDocNo = pcl021.RollCapaDocNo;
                objPCL021.RollCapaReleasedOn = pcl021.RollCapaReleasedOn;
                string RollCapaReleasedOn = fc["RollCapaReleasedOn"] != null ? fc["RollCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReleasedOn))
                {
                    objPCL021.RollCapaReleasedOn = DateTime.ParseExact(RollCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.TablesNozReq = pcl021.TablesNozReq;
                objPCL021.TablesNozReqDate = pcl021.TablesNozReqDate;
                string TablesNozReqDate = fc["TablesNozReqDate"] != null ? fc["TablesNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReqDate))
                {
                    objPCL021.TablesNozReqDate = DateTime.ParseExact(TablesNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.TablesNozDocNo = pcl021.TablesNozDocNo;
                objPCL021.TablesNozReleasedOn = pcl021.TablesNozReleasedOn;
                string TablesNozReleasedOn = fc["TablesNozReleasedOn"] != null ? fc["TablesNozReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReleasedOn))
                {
                    objPCL021.TablesNozReleasedOn = DateTime.ParseExact(TablesNozReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.TableIntReq = pcl021.TableIntReq;
                objPCL021.TableIntReqDate = pcl021.TableIntReqDate;
                string TableIntReqDate = fc["TableIntReqDate"] != null ? fc["TableIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReqDate))
                {
                    objPCL021.TableIntReqDate = DateTime.ParseExact(TableIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.TableIntDocNo = pcl021.TableIntDocNo;
                objPCL021.TableIntReleasedOn = pcl021.TableIntReleasedOn;
                string TableIntReleasedOn = fc["TableIntReleasedOn"] != null ? fc["TableIntReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReleasedOn))
                {
                    objPCL021.TableIntReleasedOn = DateTime.ParseExact(TableIntReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.TableExtReq = pcl021.TableExtReq;
                objPCL021.TableExtReqDate = pcl021.TableExtReqDate;
                string TableExtReqDate = fc["TableExtReqDate"] != null ? fc["TableExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReqDate))
                {
                    objPCL021.TableExtReqDate = DateTime.ParseExact(TableExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.TableExtDocNo = pcl021.TableExtDocNo;
                objPCL021.TableExtReleasedOn = pcl021.TableExtReleasedOn;
                string TableExtReleasedOn = fc["TableExtReleasedOn"] != null ? fc["TableExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReleasedOn))
                {
                    objPCL021.TableExtReleasedOn = DateTime.ParseExact(TableExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTREdgeReq = pcl021.HTREdgeReq;
                objPCL021.HTREdgeReqDate = pcl021.HTREdgeReqDate;
                string HTREdgeReqDate = fc["HTREdgeReqDate"] != null ? fc["HTREdgeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReqDate))
                {
                    objPCL021.HTREdgeReqDate = DateTime.ParseExact(HTREdgeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTREdgeDocNo = pcl021.HTREdgeDocNo;
                objPCL021.HTREdgeReleasedOn = pcl021.HTREdgeReleasedOn;
                string HTREdgeReleasedOn = fc["HTREdgeReleasedOn"] != null ? fc["HTREdgeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReleasedOn))
                {
                    objPCL021.HTREdgeReleasedOn = DateTime.ParseExact(HTREdgeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRRollReq = pcl021.HTRRollReq;
                objPCL021.HTRRollReqDate = pcl021.HTRRollReqDate;
                string HTRRollReqDate = fc["HTRRollReqDate"] != null ? fc["HTRRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReqDate))
                {
                    objPCL021.HTRRollReqDate = DateTime.ParseExact(HTRRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRRollDocNo = pcl021.HTRRollDocNo;
                objPCL021.HTRRollReleasedOn = pcl021.HTRRollReleasedOn;
                string HTRRollReleasedOn = fc["HTRRollReleasedOn"] != null ? fc["HTRRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReleasedOn))
                {
                    objPCL021.HTRRollReleasedOn = DateTime.ParseExact(HTRRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRReRollReq = pcl021.HTRReRollReq;
                objPCL021.HTRReRollReqDate = pcl021.HTRReRollReqDate;
                string HTRReRollReqDate = fc["HTRReRollReqDate"] != null ? fc["HTRReRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReqDate))
                {
                    objPCL021.HTRReRollReqDate = DateTime.ParseExact(HTRReRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRReRollDocNo = pcl021.HTRReRollDocNo;
                objPCL021.HTRReRollReleasedOn = pcl021.HTRReRollReleasedOn;
                string HTRReRollReleasedOn = fc["HTRReRollReleasedOn"] != null ? fc["HTRReRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReleasedOn))
                {
                    objPCL021.HTRReRollReleasedOn = DateTime.ParseExact(HTRReRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RefLineSketchReq = pcl021.RefLineSketchReq;
                objPCL021.RefLineSketchReqDate = pcl021.RefLineSketchReqDate;
                string RefLineSketchReqDate = fc["RefLineSketchReqDate"] != null ? fc["RefLineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReqDate))
                {
                    objPCL021.RefLineSketchReqDate = DateTime.ParseExact(RefLineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.RefLineSketchDocNo = pcl021.RefLineSketchDocNo;
                objPCL021.RefLineSketchReleasedOn = pcl021.RefLineSketchReleasedOn;
                string RefLineSketchReleasedOn = fc["RefLineSketchReleasedOn"] != null ? fc["RefLineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReleasedOn))
                {
                    objPCL021.RefLineSketchReleasedOn = DateTime.ParseExact(RefLineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRISRNozzleShellReq = pcl021.HTRISRNozzleShellReq;
                objPCL021.HTRISRNozzleShellReqDate = pcl021.HTRISRNozzleShellReqDate;
                string HTRISRNozzleShellReqDate = fc["HTRISRNozzleShellReqDate"] != null ? fc["HTRISRNozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReqDate))
                {
                    objPCL021.HTRISRNozzleShellReqDate = DateTime.ParseExact(HTRISRNozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRISRNozzleShellDocNo = pcl021.HTRISRNozzleShellDocNo;
                objPCL021.HTRISRNozzleShellReleasedOn = pcl021.HTRISRNozzleShellReleasedOn;
                string HTRISRNozzleShellReleasedOn = fc["HTRISRNozzleShellReleasedOn"] != null ? fc["HTRISRNozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReleasedOn))
                {
                    objPCL021.HTRISRNozzleShellReleasedOn = DateTime.ParseExact(HTRISRNozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRISRNozzleDEndReq = pcl021.HTRISRNozzleDEndReq;
                objPCL021.HTRISRNozzleDEndReqDate = pcl021.HTRISRNozzleDEndReqDate;
                string HTRISRNozzleDEndReqDate = fc["HTRISRNozzleDEndReqDate"] != null ? fc["HTRISRNozzleDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReqDate))
                {
                    objPCL021.HTRISRNozzleDEndReqDate = DateTime.ParseExact(HTRISRNozzleDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRISRNozzleDEndDocNo = pcl021.HTRISRNozzleDEndDocNo;
                objPCL021.HTRISRNozzleDEndReleasedOn = pcl021.HTRISRNozzleDEndReleasedOn;
                string HTRISRNozzleDEndReleasedOn = fc["HTRISRNozzleDEndReleasedOn"] != null ? fc["HTRISRNozzleDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReleasedOn))
                {
                    objPCL021.HTRISRNozzleDEndReleasedOn = DateTime.ParseExact(HTRISRNozzleDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRISRNubBuildUpReq = pcl021.HTRISRNubBuildUpReq;
                objPCL021.HTRISRNubBuildUpReqDate = pcl021.HTRISRNubBuildUpReqDate;
                string HTRISRNubBuildUpReqDate = fc["HTRISRNubBuildUpReqDate"] != null ? fc["HTRISRNubBuildUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReqDate))
                {
                    objPCL021.HTRISRNubBuildUpReqDate = DateTime.ParseExact(HTRISRNubBuildUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRISRNubBuildUpDocNo = pcl021.HTRISRNubBuildUpDocNo;
                objPCL021.HTRISRNubBuildUpReleasedOn = pcl021.HTRISRNubBuildUpReleasedOn;
                string HTRISRNubBuildUpReleasedOn = fc["HTRISRNubBuildUpReleasedOn"] != null ? fc["HTRISRNubBuildUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReleasedOn))
                {
                    objPCL021.HTRISRNubBuildUpReleasedOn = DateTime.ParseExact(HTRISRNubBuildUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRPWHTReq = pcl021.HTRPWHTReq;
                objPCL021.HTRPWHTReqDate = pcl021.HTRPWHTReqDate;
                string HTRPWHTReqDate = fc["HTRPWHTReqDate"] != null ? fc["HTRPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReqDate))
                {
                    objPCL021.HTRPWHTReqDate = DateTime.ParseExact(HTRPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRPWHTDocNo = pcl021.HTRPWHTDocNo;
                objPCL021.HTRPWHTReleasedOn = pcl021.HTRPWHTReleasedOn;
                string HTRPWHTReleasedOn = fc["HTRPWHTReleasedOn"] != null ? fc["HTRPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReleasedOn))
                {
                    objPCL021.HTRPWHTReleasedOn = DateTime.ParseExact(HTRPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRLSRReq = pcl021.HTRLSRReq;
                objPCL021.HTRLSRReqDate = pcl021.HTRLSRReqDate;
                string HTRLSRReqDate = fc["HTRLSRReqDate"] != null ? fc["HTRLSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReqDate))
                {
                    objPCL021.HTRLSRReqDate = DateTime.ParseExact(HTRLSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HTRLSRDocNo = pcl021.HTRLSRDocNo;
                objPCL021.HTRLSRReleasedOn = pcl021.HTRLSRReleasedOn;
                string HTRLSRReleasedOn = fc["HTRLSRReleasedOn"] != null ? fc["HTRLSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReleasedOn))
                {
                    objPCL021.HTRLSRReleasedOn = DateTime.ParseExact(HTRLSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HandlingPlanReq = pcl021.HandlingPlanReq;
                objPCL021.HandlingPlanReqDate = pcl021.HandlingPlanReqDate;
                string HandlingPlanReqDate = fc["HandlingPlanReqDate"] != null ? fc["HandlingPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReqDate))
                {
                    objPCL021.HandlingPlanReqDate = DateTime.ParseExact(HandlingPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HandlingPlanDocNo = pcl021.HandlingPlanDocNo;
                objPCL021.HandlingPlanReleasedOn = pcl021.HandlingPlanReleasedOn;
                string HandlingPlanReleasedOn = fc["HandlingPlanReleasedOn"] != null ? fc["HandlingPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReleasedOn))
                {
                    objPCL021.HandlingPlanReleasedOn = DateTime.ParseExact(HandlingPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.LocationTankRotatorReq = pcl021.LocationTankRotatorReq;
                objPCL021.LocationTankRotatorReqDate = pcl021.LocationTankRotatorReqDate;
                string LocationTankRotatorReqDate = fc["LocationTankRotatorReqDate"] != null ? fc["LocationTankRotatorReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReqDate))
                {
                    objPCL021.LocationTankRotatorReqDate = DateTime.ParseExact(LocationTankRotatorReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.LocationTankRotatorDocNo = pcl021.LocationTankRotatorDocNo;
                objPCL021.LocationTankRotatorReleasedOn = pcl021.LocationTankRotatorReleasedOn;
                string LocationTankRotatorReleasedOn = fc["LocationTankRotatorReleasedOn"] != null ? fc["LocationTankRotatorReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReleasedOn))
                {
                    objPCL021.LocationTankRotatorReleasedOn = DateTime.ParseExact(LocationTankRotatorReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HydrotestReq = pcl021.HydrotestReq;
                objPCL021.HydrotestReqDate = pcl021.HydrotestReqDate;
                string HydrotestReqDate = fc["HydrotestReqDate"] != null ? fc["HydrotestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReqDate))
                {
                    objPCL021.HydrotestReqDate = DateTime.ParseExact(HydrotestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.HydrotestDocNo = pcl021.HydrotestDocNo;
                objPCL021.HydrotestReleasedOn = pcl021.HydrotestReleasedOn;
                string HydrotestReleasedOn = fc["HydrotestReleasedOn"] != null ? fc["HydrotestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReleasedOn))
                {
                    objPCL021.HydrotestReleasedOn = DateTime.ParseExact(HydrotestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SurfaceTreatRequestReq = pcl021.SurfaceTreatRequestReq;
                objPCL021.SurfaceTreatRequestReqDate = pcl021.SurfaceTreatRequestReqDate;
                string SurfaceTreatRequestReqDate = fc["SurfaceTreatRequestReqDate"] != null ? fc["SurfaceTreatRequestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReqDate))
                {
                    objPCL021.SurfaceTreatRequestReqDate = DateTime.ParseExact(SurfaceTreatRequestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL021.SurfaceTreatRequestDocNo = pcl021.SurfaceTreatRequestDocNo;
                objPCL021.SurfaceTreatRequestReleasedOn = pcl021.SurfaceTreatRequestReleasedOn;
                string SurfaceTreatRequestReleasedOn = fc["SurfaceTreatRequestReleasedOn"] != null ? fc["SurfaceTreatRequestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReleasedOn))
                {
                    objPCL021.SurfaceTreatRequestReleasedOn = DateTime.ParseExact(SurfaceTreatRequestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                if (pcl021.LineId > 0)
                {
                    objPCL021.EditedBy = objClsLoginInfo.UserName;
                    objPCL021.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL021.CreatedBy = objClsLoginInfo.UserName;
                    objPCL021.CreatedOn = DateTime.Now;
                    db.PCL021.Add(objPCL021);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL021.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCDDim(PCL022 pcl022, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl022.HeaderId).FirstOrDefault();
                PCL022 objPCL022 = new PCL022();
                if (pcl022.LineId > 0)
                {
                    objPCL022 = db.PCL022.Where(x => x.LineId == pcl022.LineId).FirstOrDefault();
                }
                objPCL022.HeaderId = objPCL001.HeaderId;
                objPCL022.Project = objPCL001.Project;
                objPCL022.Document = objPCL001.Document;
                objPCL022.RollDataReq = pcl022.RollDataReq;
                objPCL022.RollDataReqDate = pcl022.RollDataReqDate;
                string RollDataReqDate = fc["RollDataReqDate"] != null ? fc["RollDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReqDate))
                {
                    objPCL022.RollDataReqDate = DateTime.ParseExact(RollDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.RollDataDocNo = pcl022.RollDataDocNo;
                objPCL022.RollDataReleasedOn = pcl022.RollDataReleasedOn;
                objPCL022.ShellLongReq = pcl022.ShellLongReq;
                objPCL022.ShellLongReqDate = pcl022.ShellLongReqDate;
                string ShellLongReqDate = fc["ShellLongReqDate"] != null ? fc["ShellLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReqDate))
                {
                    objPCL022.ShellLongReqDate = DateTime.ParseExact(ShellLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.ShellLongDocNo = pcl022.ShellLongDocNo;
                objPCL022.ShellLongReleasedOn = pcl022.ShellLongReleasedOn;
                string ShellLongReleasedOn = fc["ShellLongReleasedOn"] != null ? fc["ShellLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReleasedOn))
                {
                    objPCL022.ShellLongReleasedOn = DateTime.ParseExact(ShellLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.ESSCShellReq = pcl022.ESSCShellReq;
                objPCL022.ESSCShellReqDate = pcl022.ESSCShellReqDate;
                string ESSCShellReqDate = fc["ESSCShellReqDate"] != null ? fc["ESSCShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReqDate))
                {
                    objPCL022.ESSCShellReqDate = DateTime.ParseExact(ESSCShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.ESSCShellDocNo = pcl022.ESSCShellDocNo;
                objPCL022.ESSCShellReleasedOn = pcl022.ESSCShellReleasedOn;
                string ESSCShellReleasedOn = fc["ESSCShellReleasedOn"] != null ? fc["ESSCShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReleasedOn))
                {
                    objPCL022.ESSCShellReleasedOn = DateTime.ParseExact(ESSCShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.YRingReq = pcl022.YRingReq;
                objPCL022.YRingReqDate = pcl022.YRingReqDate;
                string YRingReqDate = fc["YRingReqDate"] != null ? fc["YRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YRingReqDate))
                {
                    objPCL022.YRingReqDate = DateTime.ParseExact(YRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.YRingDocNo = pcl022.YRingDocNo;
                objPCL022.YRingReleasedOn = pcl022.YRingReleasedOn;
                string YRingReleasedOn = fc["YRingReleasedOn"] != null ? fc["YRingReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YRingReleasedOn))
                {
                    objPCL022.YRingReleasedOn = DateTime.ParseExact(YRingReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.ChangeOvalityExtReq = pcl022.ChangeOvalityExtReq;
                objPCL022.ChangeOvalityExtReqDate = pcl022.ChangeOvalityExtReqDate;
                string ChangeOvalityExtReqDate = fc["ChangeOvalityExtReqDate"] != null ? fc["ChangeOvalityExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReqDate))
                {
                    objPCL022.ChangeOvalityExtReqDate = DateTime.ParseExact(ChangeOvalityExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.ChangeOvalityExtDocNo = pcl022.ChangeOvalityExtDocNo;
                objPCL022.ChangeOvalityExtReleasedOn = pcl022.ChangeOvalityExtReleasedOn;
                string ChangeOvalityExtReleasedOn = fc["ChangeOvalityExtReleasedOn"] != null ? fc["ChangeOvalityExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReleasedOn))
                {
                    objPCL022.ChangeOvalityExtReleasedOn = DateTime.ParseExact(ChangeOvalityExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL022.DendReq = pcl022.DendReq;
                objPCL022.DendReqDate = pcl022.DendReqDate;
                string DendReqDate = fc["DendReqDate"] != null ? fc["DendReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DendReqDate))
                {
                    objPCL022.DendReqDate = DateTime.ParseExact(DendReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL022.DendDocNo = pcl022.DendDocNo;
                objPCL022.DendReleasedOn = pcl022.DendReleasedOn;
                string DendReleasedOn = fc["DendReleasedOn"] != null ? fc["DendReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DendReleasedOn))
                {
                    objPCL022.DendReleasedOn = DateTime.ParseExact(DendReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL022.SkirtReq = pcl022.SkirtReq;
                objPCL022.SkirtReqDate = pcl022.SkirtReqDate;
                string SkirtReqDate = fc["SkirtReqDate"] != null ? fc["SkirtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SkirtReqDate))
                {
                    objPCL022.SkirtReqDate = DateTime.ParseExact(SkirtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL022.SkirtDocNo = pcl022.SkirtDocNo;
                objPCL022.SkirtReleasedOn = pcl022.SkirtReleasedOn;
                string SkirtReleasedOn = fc["SkirtReleasedOn"] != null ? fc["SkirtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SkirtReleasedOn))
                {
                    objPCL022.SkirtReleasedOn = DateTime.ParseExact(SkirtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL022.DumpReq = pcl022.DumpReq;
                objPCL022.DumpReqDate = pcl022.DumpReqDate;
                string DumpReqDate = fc["DumpReqDate"] != null ? fc["DumpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DumpReqDate))
                {
                    objPCL022.DumpReqDate = DateTime.ParseExact(DumpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                }
                objPCL022.DumpDocNo = pcl022.DumpDocNo;
                objPCL022.DumpReleasedOn = pcl022.DumpReleasedOn;
                string DumpReleasedOn = fc["DumpReleasedOn"] != null ? fc["DumpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DumpReleasedOn))
                {
                    objPCL022.DumpReleasedOn = DateTime.ParseExact(DumpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }


                if (pcl022.LineId > 0)
                {
                    objPCL022.EditedBy = objClsLoginInfo.UserName;
                    objPCL022.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL022.CreatedBy = objClsLoginInfo.UserName;
                    objPCL022.CreatedOn = DateTime.Now;
                    db.PCL022.Add(objPCL022);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL022.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCDFixture(PCL023 pcl023, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl023.HeaderId).FirstOrDefault();
                PCL023 objPCL023 = new PCL023();
                bool IsEdited = false;
                if (pcl023.LineId > 0)
                {
                    objPCL023 = db.PCL023.Where(x => x.LineId == pcl023.LineId).FirstOrDefault();
                    IsEdited = true;
                }

                objPCL023.HeaderId = objPCL001.HeaderId;
                objPCL023.Project = objPCL001.Project;
                objPCL023.Document = objPCL001.Document;
                objPCL023.TempRollReqDate = pcl023.TempRollReqDate;

                string TempRollReqDate = fc["TempRollReqDate"] != null ? fc["TempRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempRollReqDate))
                {
                    objPCL023.TempRollReqDate = DateTime.ParseExact(TempRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL023.TempRollNoFix = pcl023.TempRollNoFix;
                objPCL023.TempRollAvlQty = pcl023.TempRollAvlQty;
                objPCL023.TempRollDrg = pcl023.TempRollDrg;
                objPCL023.TempRollMtrl = pcl023.TempRollMtrl;
                objPCL023.TempRollFab = pcl023.TempRollFab;
                objPCL023.TempRollDel = pcl023.TempRollDel;
                objPCL023.TempWeldReqDate = pcl023.TempWeldReqDate;
                string TempWeldReqDate = fc["TempWeldReqDate"] != null ? fc["TempWeldReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempWeldReqDate))
                {
                    objPCL023.TempWeldReqDate = DateTime.ParseExact(TempWeldReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.TempWeldNoFix = pcl023.TempWeldNoFix;
                objPCL023.TempWeldAvlQty = pcl023.TempWeldAvlQty;
                objPCL023.TempWeldDrg = pcl023.TempWeldDrg;
                objPCL023.TempWeldMtrl = pcl023.TempWeldMtrl;
                objPCL023.TempWeldFab = pcl023.TempWeldFab;
                objPCL023.TempWeldDel = pcl023.TempWeldDel;
                objPCL023.GasketCoverReqDate = pcl023.GasketCoverReqDate;
                string GasketCoverReqDate = fc["GasketCoverReqDate"] != null ? fc["GasketCoverReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(GasketCoverReqDate))
                {
                    objPCL023.GasketCoverReqDate = DateTime.ParseExact(GasketCoverReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.GasketCoverNoFix = pcl023.GasketCoverNoFix;
                objPCL023.GasketCoverAvlQty = pcl023.GasketCoverAvlQty;
                objPCL023.GasketCoverDrg = pcl023.GasketCoverDrg;
                objPCL023.GasketCoverMtrl = pcl023.GasketCoverMtrl;
                objPCL023.GasketCoverFab = pcl023.GasketCoverFab;
                objPCL023.GasketCoverDel = pcl023.GasketCoverDel;
                objPCL023.LSRSpiderReqDate = pcl023.LSRSpiderReqDate;
                string LSRSpiderReqDate = fc["LSRSpiderReqDate"] != null ? fc["LSRSpiderReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRSpiderReqDate))
                {
                    objPCL023.LSRSpiderReqDate = DateTime.ParseExact(LSRSpiderReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.LSRSpiderNoFix = pcl023.LSRSpiderNoFix;
                objPCL023.LSRSpiderAvlQty = pcl023.LSRSpiderAvlQty;
                objPCL023.LSRSpiderDrg = pcl023.LSRSpiderDrg;
                objPCL023.LSRSpiderMtrl = pcl023.LSRSpiderMtrl;
                objPCL023.LSRSpiderFab = pcl023.LSRSpiderFab;
                objPCL023.LSRSpiderDel = pcl023.LSRSpiderDel;
                objPCL023.FixtureShellReqDate = pcl023.FixtureShellReqDate;
                string FixtureShellReqDate = fc["FixtureShellReqDate"] != null ? fc["FixtureShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixtureShellReqDate))
                {
                    objPCL023.FixtureShellReqDate = DateTime.ParseExact(FixtureShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.FixtureShellNoFix = pcl023.FixtureShellNoFix;
                objPCL023.FixtureShellAvlQty = pcl023.FixtureShellAvlQty;
                objPCL023.FixtureShellDrg = pcl023.FixtureShellDrg;
                objPCL023.FixtureShellMtrl = pcl023.FixtureShellMtrl;
                objPCL023.FixtureShellFab = pcl023.FixtureShellFab;
                objPCL023.FixtureShellDel = pcl023.FixtureShellDel;
                objPCL023.DiePunchReqDate = pcl023.DiePunchReqDate;
                string DiePunchReqDate = fc["DiePunchReqDate"] != null ? fc["DiePunchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DiePunchReqDate))
                {
                    objPCL023.DiePunchReqDate = DateTime.ParseExact(DiePunchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.DiePunchNoFix = pcl023.DiePunchNoFix;
                objPCL023.DiePunchAvlQty = pcl023.DiePunchAvlQty;
                objPCL023.DiePunchDrg = pcl023.DiePunchDrg;
                objPCL023.DiePunchMtrl = pcl023.DiePunchMtrl;
                objPCL023.DiePunchFab = pcl023.DiePunchFab;
                objPCL023.DiePunchDel = pcl023.DiePunchDel;
                objPCL023.SquareBarReqDate = pcl023.SquareBarReqDate;
                string SquareBarReqDate = fc["SquareBarReqDate"] != null ? fc["SquareBarReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SquareBarReqDate))
                {
                    objPCL023.SquareBarReqDate = DateTime.ParseExact(SquareBarReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.SquareBarNoFix = pcl023.SquareBarNoFix;
                objPCL023.SquareBarAvlQty = pcl023.SquareBarAvlQty;
                objPCL023.SquareBarDrg = pcl023.SquareBarDrg;
                objPCL023.SquareBarMtrl = pcl023.SquareBarMtrl;
                objPCL023.SquareBarFab = pcl023.SquareBarFab;
                objPCL023.SquareBarDel = pcl023.SquareBarDel;
                objPCL023.PreHeatReqDate = pcl023.PreHeatReqDate;
                string PreHeatReqDate = fc["PreHeatReqDate"] != null ? fc["PreHeatReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PreHeatReqDate))
                {
                    objPCL023.PreHeatReqDate = DateTime.ParseExact(PreHeatReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.PreHeatNoFix = pcl023.PreHeatNoFix;
                objPCL023.PreHeatAvlQty = pcl023.PreHeatAvlQty;
                objPCL023.PreHeatDrg = pcl023.PreHeatDrg;
                objPCL023.PreHeatMtrl = pcl023.PreHeatMtrl;
                objPCL023.PreHeatFab = pcl023.PreHeatFab;
                objPCL023.PreHeatDel = pcl023.PreHeatDel;
                objPCL023.ExtRingReqDate = pcl023.ExtRingReqDate;
                string ExtRingReqDate = fc["ExtRingReqDate"] != null ? fc["ExtRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ExtRingReqDate))
                {
                    objPCL023.ExtRingReqDate = DateTime.ParseExact(ExtRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.ExtRingNoFix = pcl023.ExtRingNoFix;
                objPCL023.ExtRingAvlQty = pcl023.ExtRingAvlQty;
                objPCL023.ExtRingDrg = pcl023.ExtRingDrg;
                objPCL023.ExtRingMtrl = pcl023.ExtRingMtrl;
                objPCL023.ExtRingFab = pcl023.ExtRingFab;
                objPCL023.ExtRingDel = pcl023.ExtRingDel;
                objPCL023.FixPWHTReqDate = pcl023.FixPWHTReqDate;
                string FixPWHTReqDate = fc["FixPWHTReqDate"] != null ? fc["FixPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixPWHTReqDate))
                {
                    objPCL023.FixPWHTReqDate = DateTime.ParseExact(FixPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.FixPWHTNoFix = pcl023.FixPWHTNoFix;
                objPCL023.FixPWHTAvlQty = pcl023.FixPWHTAvlQty;
                objPCL023.FixPWHTDrg = pcl023.FixPWHTDrg;
                objPCL023.FixPWHTMtrl = pcl023.FixPWHTMtrl;
                objPCL023.FixPWHTFab = pcl023.FixPWHTFab;
                objPCL023.FixPWHTDel = pcl023.FixPWHTDel;
                objPCL023.SaddlePWHTReqDate = pcl023.SaddlePWHTReqDate;
                string SaddlePWHTReqDate = fc["SaddlePWHTReqDate"] != null ? fc["SaddlePWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddlePWHTReqDate))
                {
                    objPCL023.SaddlePWHTReqDate = DateTime.ParseExact(SaddlePWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.SaddlePWHTNoFix = pcl023.SaddlePWHTNoFix;
                objPCL023.SaddlePWHTAvlQty = pcl023.SaddlePWHTAvlQty;
                objPCL023.SaddlePWHTDrg = pcl023.SaddlePWHTDrg;
                objPCL023.SaddlePWHTMtrl = pcl023.SaddlePWHTMtrl;
                objPCL023.SaddlePWHTFab = pcl023.SaddlePWHTFab;
                objPCL023.SaddlePWHTDel = pcl023.SaddlePWHTDel;
                objPCL023.SaddleHydroReqDate = pcl023.SaddleHydroReqDate;
                string SaddleHydroReqDate = fc["SaddleHydroReqDate"] != null ? fc["SaddleHydroReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddleHydroReqDate))
                {
                    objPCL023.SaddleHydroReqDate = DateTime.ParseExact(SaddleHydroReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.SaddleHydroNoFix = pcl023.SaddleHydroNoFix;
                objPCL023.SaddleHydroAvlQty = pcl023.SaddleHydroAvlQty;
                objPCL023.SaddleHydroDrg = pcl023.SaddleHydroDrg;
                objPCL023.SaddleHydroMtrl = pcl023.SaddleHydroMtrl;
                objPCL023.SaddleHydroFab = pcl023.SaddleHydroFab;
                objPCL023.SaddleHydroDel = pcl023.SaddleHydroDel;
                objPCL023.LSRFurnaceReqDate = pcl023.LSRFurnaceReqDate;
                string LSRFurnaceReqDate = fc["LSRFurnaceReqDate"] != null ? fc["LSRFurnaceReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRFurnaceReqDate))
                {
                    objPCL023.LSRFurnaceReqDate = DateTime.ParseExact(LSRFurnaceReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.LSRFurnaceNoFix = pcl023.LSRFurnaceNoFix;
                objPCL023.LSRFurnaceAvlQty = pcl023.LSRFurnaceAvlQty;
                objPCL023.LSRFurnaceDrg = pcl023.LSRFurnaceDrg;
                objPCL023.LSRFurnaceMtrl = pcl023.LSRFurnaceMtrl;
                objPCL023.LSRFurnaceFab = pcl023.LSRFurnaceFab;
                objPCL023.LSRFurnaceDel = pcl023.LSRFurnaceDel;
                objPCL023.LSRRingReqDate = pcl023.LSRRingReqDate;
                string LSRRingReqDate = fc["LSRRingReqDate"] != null ? fc["LSRRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRRingReqDate))
                {
                    objPCL023.LSRRingReqDate = DateTime.ParseExact(LSRRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.LSRRingNoFix = pcl023.LSRRingNoFix;
                objPCL023.LSRRingAvlQty = pcl023.LSRRingAvlQty;
                objPCL023.LSRRingDrg = pcl023.LSRRingDrg;
                objPCL023.LSRRingMtrl = pcl023.LSRRingMtrl;
                objPCL023.LSRRingFab = pcl023.LSRRingFab;
                objPCL023.LSRRingDel = pcl023.LSRRingDel;
                objPCL023.PlateTrunReqDate = pcl023.PlateTrunReqDate;
                string PlateTrunReqDate = fc["PlateTrunReqDate"] != null ? fc["PlateTrunReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateTrunReqDate))
                {
                    objPCL023.PlateTrunReqDate = DateTime.ParseExact(PlateTrunReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.PlateTrunNoFix = pcl023.PlateTrunNoFix;
                objPCL023.PlateTrunAvlQty = pcl023.PlateTrunAvlQty;
                objPCL023.PlateTrunDrg = pcl023.PlateTrunDrg;
                objPCL023.PlateTrunMtrl = pcl023.PlateTrunMtrl;
                objPCL023.PlateTrunFab = pcl023.PlateTrunFab;
                objPCL023.PlateTrunDel = pcl023.PlateTrunDel;
                objPCL023.PipeTrunReqDate = pcl023.PipeTrunReqDate;
                string PipeTrunReqDate = fc["PipeTrunReqDate"] != null ? fc["PipeTrunReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PipeTrunReqDate))
                {
                    objPCL023.PipeTrunReqDate = DateTime.ParseExact(PipeTrunReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.PipeTrunNoFix = pcl023.PipeTrunNoFix;
                objPCL023.PipeTrunAvlQty = pcl023.PipeTrunAvlQty;
                objPCL023.PipeTrunDrg = pcl023.PipeTrunDrg;
                objPCL023.PipeTrunMtrl = pcl023.PipeTrunMtrl;
                objPCL023.PipeTrunFab = pcl023.PipeTrunFab;
                objPCL023.PipeTrunDel = pcl023.PipeTrunDel;
                objPCL023.PlateLugseqDate = pcl023.PlateLugseqDate;
                string PlateLugseqDate = fc["PlateLugseqDate"] != null ? fc["PlateLugseqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateLugseqDate))
                {
                    objPCL023.PlateLugseqDate = DateTime.ParseExact(PlateLugseqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.PlateLugsoFix = pcl023.PlateLugsoFix;
                objPCL023.PlateLugsvlQty = pcl023.PlateLugsvlQty;
                objPCL023.PlateLugsrg = pcl023.PlateLugsrg;
                objPCL023.PlateLugstrl = pcl023.PlateLugstrl;
                objPCL023.PlateLugsab = pcl023.PlateLugsab;
                objPCL023.PlateLugsel = pcl023.PlateLugsel;
                objPCL023.PlateCraneReqDate = pcl023.PlateCraneReqDate;
                string PlateCraneReqDate = fc["PlateCraneReqDate"] != null ? fc["PlateCraneReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateCraneReqDate))
                {
                    objPCL023.PlateCraneReqDate = DateTime.ParseExact(PlateCraneReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.PlateCraneNoFix = pcl023.PlateCraneNoFix;
                objPCL023.PlateCraneAvlQty = pcl023.PlateCraneAvlQty;
                objPCL023.PlateCraneDrg = pcl023.PlateCraneDrg;
                objPCL023.PlateCraneMtrl = pcl023.PlateCraneMtrl;
                objPCL023.PlateCraneFab = pcl023.PlateCraneFab;
                objPCL023.PlateCraneDel = pcl023.PlateCraneDel;
                objPCL023.PlateHydroReqDate = pcl023.PlateHydroReqDate;
                string PlateHydroReqDate = fc["PlateHydroReqDate"] != null ? fc["PlateHydroReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateHydroReqDate))
                {
                    objPCL023.PlateHydroReqDate = DateTime.ParseExact(PlateHydroReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.PlateHydroNoFix = pcl023.PlateHydroNoFix;
                objPCL023.PlateHydroAvlQty = pcl023.PlateHydroAvlQty;
                objPCL023.PlateHydroDrg = pcl023.PlateHydroDrg;
                objPCL023.PlateHydroMtrl = pcl023.PlateHydroMtrl;
                objPCL023.PlateHydroFab = pcl023.PlateHydroFab;
                objPCL023.PlateHydroDel = pcl023.PlateHydroDel;
                objPCL023.CSAWReqDate = pcl023.CSAWReqDate;
                string CSAWReqDate = fc["CSAWReqDate"] != null ? fc["CSAWReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CSAWReqDate))
                {
                    objPCL023.CSAWReqDate = DateTime.ParseExact(CSAWReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.CSAWNoFix = pcl023.CSAWNoFix;
                objPCL023.CSAWAvlQty = pcl023.CSAWAvlQty;
                objPCL023.CSAWDrg = pcl023.CSAWDrg;
                objPCL023.CSAWMtrl = pcl023.CSAWMtrl;
                objPCL023.CSAWFab = pcl023.CSAWFab;
                objPCL023.CSAWDel = pcl023.CSAWDel;
                objPCL023.ConePlateReqDate = pcl023.ConePlateReqDate;
                string ConePlateReqDate = fc["ConePlateReqDate"] != null ? fc["ConePlateReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ConePlateReqDate))
                {
                    objPCL023.ConePlateReqDate = DateTime.ParseExact(ConePlateReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.ConePlateNoFix = pcl023.ConePlateNoFix;
                objPCL023.ConePlateAvlQty = pcl023.ConePlateAvlQty;
                objPCL023.ConePlateDrg = pcl023.ConePlateDrg;
                objPCL023.ConePlateMtrl = pcl023.ConePlateMtrl;
                objPCL023.ConePlateFab = pcl023.ConePlateFab;
                objPCL023.ConePlateDel = pcl023.ConePlateDel;
                objPCL023.StiffReqDate = pcl023.StiffReqDate;
                string StiffReqDate = fc["StiffReqDate"] != null ? fc["StiffReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(StiffReqDate))
                {
                    objPCL023.StiffReqDate = DateTime.ParseExact(StiffReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.StiffNoFix = pcl023.StiffNoFix;
                objPCL023.StiffAvlQty = pcl023.StiffAvlQty;
                objPCL023.StiffDrg = pcl023.StiffDrg;
                objPCL023.StiffMtrl = pcl023.StiffMtrl;
                objPCL023.StiffFab = pcl023.StiffFab;
                objPCL023.StiffDel = pcl023.StiffDel;
                objPCL023.YRingReqDate = pcl023.YRingReqDate;
                string YRingReqDate = fc["YRingReqDate"] != null ? fc["YRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YRingReqDate))
                {
                    objPCL023.YRingReqDate = DateTime.ParseExact(YRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.YRingNoFix = pcl023.YRingNoFix;
                objPCL023.YRingAvlQty = pcl023.YRingAvlQty;
                objPCL023.YRingDrg = pcl023.YRingDrg;
                objPCL023.YRingMtrl = pcl023.YRingMtrl;
                objPCL023.YRingFab = pcl023.YRingFab;
                objPCL023.YRingDel = pcl023.YRingDel;
                objPCL023.CircSeamReqDate = pcl023.CircSeamReqDate;
                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL023.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.CircSeamNoFix = pcl023.CircSeamNoFix;
                objPCL023.CircSeamAvlQty = pcl023.CircSeamAvlQty;
                objPCL023.CircSeamDrg = pcl023.CircSeamDrg;
                objPCL023.CircSeamMtrl = pcl023.CircSeamMtrl;
                objPCL023.CircSeamFab = pcl023.CircSeamFab;
                objPCL023.CircSeamDel = pcl023.CircSeamDel;
                objPCL023.LongSeamReqDate = pcl023.LongSeamReqDate;
                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL023.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.LongSeamNoFix = pcl023.LongSeamNoFix;
                objPCL023.LongSeamAvlQty = pcl023.LongSeamAvlQty;
                objPCL023.LongSeamDrg = pcl023.LongSeamDrg;
                objPCL023.LongSeamMtrl = pcl023.LongSeamMtrl;
                objPCL023.LongSeamFab = pcl023.LongSeamFab;
                objPCL023.LongSeamDel = pcl023.LongSeamDel;
                objPCL023.ShellSegReqDate = pcl023.ShellSegReqDate;
                string ShellSegReqDate = fc["ShellSegReqDate"] != null ? fc["ShellSegReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellSegReqDate))
                {
                    objPCL023.ShellSegReqDate = DateTime.ParseExact(ShellSegReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL023.ShellSegNoFix = pcl023.ShellSegNoFix;
                objPCL023.ShellSegAvlQty = pcl023.ShellSegAvlQty;
                objPCL023.ShellSegDrg = pcl023.ShellSegDrg;
                objPCL023.ShellSegMtrl = pcl023.ShellSegMtrl;
                objPCL023.ShellSegFab = pcl023.ShellSegFab;
                objPCL023.ShellSegDel = pcl023.ShellSegDel;


                if (IsEdited)
                {
                    objPCL023.EditedBy = objClsLoginInfo.UserName;
                    objPCL023.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL023.CreatedBy = objClsLoginInfo.UserName;
                    objPCL023.CreatedOn = DateTime.Now;
                    db.PCL023.Add(objPCL023);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL023.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region EO 

        [HttpPost]
        public ActionResult SaveEOConstructor(PCL030 pcl030, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl030.HeaderId).FirstOrDefault();
                PCL030 objPCL030 = new PCL030();
                if (pcl030.LineId > 0)
                {
                    objPCL030 = db.PCL030.Where(x => x.LineId == pcl030.LineId).FirstOrDefault();
                }
                objPCL030.HeaderId = objPCL001.HeaderId;
                objPCL030.Project = objPCL001.Project;
                objPCL030.Document = objPCL001.Document;

                objPCL030.ReactorID = pcl030.ReactorID;
                objPCL030.ReactorThk1 = pcl030.ReactorThk1;
                objPCL030.TSFace = pcl030.TSFace;
                objPCL030.ReactorLg = pcl030.ReactorLg;
                objPCL030.ReactorFabWt = pcl030.ReactorFabWt;
                objPCL030.ReactorHydroWt = pcl030.ReactorHydroWt;
                objPCL030.ReactorMaxShellWd = pcl030.ReactorMaxShellWd;
                objPCL030.ReactoreDesigned = pcl030.ReactoreDesigned;
                objPCL030.TSC1Pc = pcl030.TSC1Pc;
                objPCL030.TSC2PcLS = pcl030.TSC2PcLS;
                objPCL030.TSC2PCCS = pcl030.TSC2PCCS;
                objPCL030.TSC3Pc = pcl030.TSC3Pc;
                objPCL030.TSDLess1 = pcl030.TSDLess1;
                objPCL030.TSD4_1 = pcl030.TSD4_1;
                objPCL030.TSD4_6 = pcl030.TSD4_6;
                objPCL030.TSDMore6 = pcl030.TSDMore6;
                objPCL030.TSMSS = pcl030.TSMSS;
                objPCL030.TSMLAS = pcl030.TSMLAS;
                objPCL030.TSMCS = pcl030.TSMCS;
                objPCL030.TSOSS = pcl030.TSOSS;
                objPCL030.TSOCS = pcl030.TSOCS;
                objPCL030.TSOInconnel = pcl030.TSOInconnel;
                objPCL030.TSONr = pcl030.TSONr;
                objPCL030.TubeSizeOD = pcl030.TubeSizeOD;
                objPCL030.TubeSizeThik = pcl030.TubeSizeThik;
                objPCL030.TubeSizeLg = pcl030.TubeSizeLg;
                objPCL030.TubeSizeQty = pcl030.TubeSizeQty;
                objPCL030.TubeMatDSS = pcl030.TubeMatDSS;
                objPCL030.TubeMatCS = pcl030.TubeMatCS;
                objPCL030.TubeMatLAS = pcl030.TubeMatLAS;
                objPCL030.TubeMatInco = pcl030.TubeMatInco;
                objPCL030.BaffSizeOD = pcl030.BaffSizeOD;
                objPCL030.BaffSizeThik = pcl030.BaffSizeThik;
                objPCL030.BaffSizeMat = pcl030.BaffSizeMat;
                objPCL030.BaffSizeQty = pcl030.BaffSizeQty;
                objPCL030.BaffType1Pc = pcl030.BaffType1Pc;
                objPCL030.BaffTypeMore1 = pcl030.BaffTypeMore1;
                objPCL030.BaffTypeGrid = pcl030.BaffTypeGrid;
                objPCL030.BaffTypeBox = pcl030.BaffTypeBox;
                objPCL030.BACAHoriz = pcl030.BACAHoriz;
                objPCL030.BACAVert = pcl030.BACAVert;
                objPCL030.BACFab = pcl030.BACFab;
                objPCL030.FerruleReqY = pcl030.FerruleReqY;
                objPCL030.FerruleReqN = pcl030.FerruleReqN;
                objPCL030.FerruleReqMat = pcl030.FerruleReqMat;
                objPCL030.TTTWeld = pcl030.TTTWeld;
                objPCL030.TTTExp = pcl030.TTTExp;
                objPCL030.TTTOthr = pcl030.TTTOthr;
                objPCL030.TTM2WeldMock = pcl030.TTM2WeldMock;
                objPCL030.TTM2WeldProd = pcl030.TTM2WeldProd;
                objPCL030.TTM6 = pcl030.TTM6;
                objPCL030.TTM1 = pcl030.TTM1;
                objPCL030.TTWAuto = pcl030.TTWAuto;
                objPCL030.TTWManual = pcl030.TTWManual;
                objPCL030.TTJAir = pcl030.TTJAir;
                objPCL030.TTJHel = pcl030.TTJHel;
                objPCL030.TTJHydr = pcl030.TTJHydr;
                objPCL030.TEMMech = pcl030.TEMMech;
                objPCL030.TEMHydro = pcl030.TEMHydro;
                objPCL030.TEMOther = pcl030.TEMOther;
                objPCL030.OSRHel = pcl030.OSRHel;
                objPCL030.OSRFerr = pcl030.OSRFerr;
                objPCL030.TCSWeld = pcl030.TCSWeld;
                objPCL030.TCSThread = pcl030.TCSThread;
                objPCL030.SC1Seg = pcl030.SC1Seg;
                objPCL030.SC2Seg = pcl030.SC2Seg;
                objPCL030.SC3Seg = pcl030.SC3Seg;
                objPCL030.SCForged = pcl030.SCForged;
                objPCL030.TypeRollCold = pcl030.TypeRollCold;
                objPCL030.TypeRollWarm = pcl030.TypeRollWarm;
                objPCL030.TypeRollHot = pcl030.TypeRollHot;
                objPCL030.GasCuttingPreHeat = pcl030.GasCuttingPreHeat;
                objPCL030.GasCuttingPreHeatNR = pcl030.GasCuttingPreHeatNR;
                objPCL030.ShellPush = pcl030.ShellPush;
                objPCL030.CSeamWEPPlate = pcl030.CSeamWEPPlate;
                objPCL030.CSeamWEPAfter = pcl030.CSeamWEPAfter;
                objPCL030.PTCRequiredforLS = pcl030.PTCRequiredforLS;
                objPCL030.PTCRequiredforCS = pcl030.PTCRequiredforCS;
                objPCL030.PTCRequiredforDend = pcl030.PTCRequiredforDend;
                objPCL030.PTCRequiredforDendShell = pcl030.PTCRequiredforDendShell;
                objPCL030.PTCRequiredforTTS = pcl030.PTCRequiredforTTS;
                objPCL030.ISRRequiredforLS = pcl030.ISRRequiredforLS;
                objPCL030.ISRRequiredforCS = pcl030.ISRRequiredforCS;
                objPCL030.ISRRequiredforNozzle = pcl030.ISRRequiredforNozzle;
                objPCL030.ISRRequiredforNub = pcl030.ISRRequiredforNub;
                objPCL030.ISRRequiredforTSR = pcl030.ISRRequiredforTSR;
                objPCL030.SOTESSCSingle = pcl030.SOTESSCSingle;
                objPCL030.SOTESSCDouble = pcl030.SOTESSCDouble;
                objPCL030.SOTESSCClade = pcl030.SOTESSCClade;
                objPCL030.SOTESSCOlayNR = pcl030.SOTESSCOlayNR;
                objPCL030.DCPetalWCrown = pcl030.DCPetalWCrown;
                objPCL030.DC8PetalWoCrown = pcl030.DC8PetalWoCrown;
                objPCL030.DCSinglePeice = pcl030.DCSinglePeice;
                objPCL030.DCTwoHalves = pcl030.DCTwoHalves;
                objPCL030.DRCFormedPetals = pcl030.DRCFormedPetals;
                objPCL030.DRCFormedSetup = pcl030.DRCFormedSetup;
                objPCL030.DRCFormedWelded = pcl030.DRCFormedWelded;
                objPCL030.DOTESSCSingle = pcl030.DOTESSCSingle;
                objPCL030.DOTESSCDouble = pcl030.DOTESSCDouble;
                objPCL030.DOTESSCClade = pcl030.DOTESSCClade;
                objPCL030.DOTESSCOlayNR = pcl030.DOTESSCOlayNR;
                objPCL030.SVJYring = pcl030.SVJYring;
                objPCL030.SVJBuildup = pcl030.SVJBuildup;
                objPCL030.SVJLapJoint = pcl030.SVJLapJoint;
                objPCL030.SVJNA = pcl030.SVJNA;
                objPCL030.DSRWeldBuildup = pcl030.DSRWeldBuildup;
                objPCL030.DSRForgedShell = pcl030.DSRForgedShell;
                objPCL030.DSRPlateWOlay = pcl030.DSRPlateWOlay;
                objPCL030.DSRPlate = pcl030.DSRPlate;
                objPCL030.DSRSSPlate = pcl030.DSRSSPlate;
                objPCL030.DSRNoofGussets = pcl030.DSRNoofGussets;
                objPCL030.DSRNR = pcl030.DSRNR;
                objPCL030.NFFRaised = pcl030.NFFRaised;
                objPCL030.NFFGroove = pcl030.NFFGroove;
                objPCL030.NFFOther = pcl030.NFFOther;
                objPCL030.NGFBfPWHT = pcl030.NGFBfPWHT;
                objPCL030.NGFAfPWHT = pcl030.NGFAfPWHT;
                objPCL030.SpoolMatSS = pcl030.SpoolMatSS;
                objPCL030.SpoolMatLAS = pcl030.SpoolMatLAS;
                objPCL030.JSInConnel = pcl030.JSInConnel;
                objPCL030.JSBiMetalic = pcl030.JSBiMetalic;
                objPCL030.JSCladRestoration = pcl030.JSCladRestoration;
                objPCL030.SSWBfPWHT = pcl030.SSWBfPWHT;
                objPCL030.SSWAfPWHT = pcl030.SSWAfPWHT;
                objPCL030.SSWBoth = pcl030.SSWBoth;
                objPCL030.VessleSSkirt = pcl030.VessleSSkirt;
                objPCL030.VessleSSupp = pcl030.VessleSSupp;
                objPCL030.VessleSLegs = pcl030.VessleSLegs;
                objPCL030.VessleSSaddle = pcl030.VessleSSaddle;
                objPCL030.SkirtMadein1 = pcl030.SkirtMadein1;
                objPCL030.SkirtMadein2 = pcl030.SkirtMadein2;
                objPCL030.FPNWeldNuts = pcl030.FPNWeldNuts;
                objPCL030.FPNLeaveRow = pcl030.FPNLeaveRow;
                objPCL030.PWHTSinglePiece = pcl030.PWHTSinglePiece;
                objPCL030.PWHTLSRJoint = pcl030.PWHTLSRJoint;
                objPCL030.PTCMTCptc = pcl030.PTCMTCptc;
                objPCL030.PTCMTCmtc = pcl030.PTCMTCmtc;
                objPCL030.PTCMTCptcSim = pcl030.PTCMTCptcSim;
                objPCL030.PTCMTCmtcSim = pcl030.PTCMTCmtcSim;
                objPCL030.SaddlesTrans = pcl030.SaddlesTrans;
                objPCL030.SaddlesAddit = pcl030.SaddlesAddit;
                objPCL030.PWHTinFurnacePFS = pcl030.PWHTinFurnacePFS;
                objPCL030.PWHTinFurnaceHFS1 = pcl030.PWHTinFurnaceHFS1;
                objPCL030.PWHTinFurnaceLEMF = pcl030.PWHTinFurnaceLEMF;
                objPCL030.PWHTinFurnaceShop = pcl030.PWHTinFurnaceShop;
                objPCL030.SectionWtMore200 = pcl030.SectionWtMore200;
                objPCL030.SectionWt200To230 = pcl030.SectionWt200To230;
                objPCL030.SectionWt150To200 = pcl030.SectionWt150To200;
                objPCL030.SectionWtLess150 = pcl030.SectionWtLess150;
                objPCL030.ARMRequired = pcl030.ARMRequired;
                objPCL030.ARMRT = pcl030.ARMRT;
                objPCL030.ARMPWHT = pcl030.ARMPWHT;
                objPCL030.ARMSB = pcl030.ARMSB;
                objPCL030.CLRH20ppm = pcl030.CLRH20ppm;
                objPCL030.CLRHSodium = pcl030.CLRHSodium;
                objPCL030.TemplateForReq = pcl030.TemplateForReq;
                objPCL030.TemplateForNReq = pcl030.TemplateForNReq;
                objPCL030.N2FillReq = pcl030.N2FillReq;
                objPCL030.N2FillNotReq = pcl030.N2FillNotReq;
                objPCL030.N2FillOther = pcl030.N2FillOther;
                objPCL030.ESTPlate = pcl030.ESTPlate;
                objPCL030.ESTShell = pcl030.ESTShell;
                objPCL030.ESTSection = pcl030.ESTSection;
                objPCL030.ESTEquip = pcl030.ESTEquip;
                objPCL030.ISTPlate = pcl030.ISTPlate;
                objPCL030.ISTShell = pcl030.ISTShell;
                objPCL030.ISTSection = pcl030.ISTSection;
                objPCL030.ISTEquip = pcl030.ISTEquip;
                objPCL030.ISTAfBfPWHT = pcl030.ISTAfBfPWHT;
                objPCL030.ISTAfHydro = pcl030.ISTAfHydro;
                objPCL030.MaxSBLess1_38 = pcl030.MaxSBLess1_38;
                objPCL030.MaxSBLess1_78 = pcl030.MaxSBLess1_78;
                objPCL030.MaxSBLess2_14 = pcl030.MaxSBLess2_14;
                objPCL030.MaxSBLess4 = pcl030.MaxSBLess4;
                objPCL030.MaxSB372 = pcl030.MaxSB372;
                objPCL030.MaxSB250 = pcl030.MaxSB250;
                objPCL030.MaxSB216 = pcl030.MaxSB216;
                objPCL030.SCForgedVal = pcl030.SCForgedVal;
                objPCL030.PTCRequiredforTTSVal = pcl030.PTCRequiredforTTSVal;
                objPCL030.DCPetalWCrownVal = pcl030.DCPetalWCrownVal;
                objPCL030.DSRNoofGussetsVal = pcl030.DSRNoofGussetsVal;
                objPCL030.SaddlesTransVal = pcl030.SaddlesTransVal;
                objPCL030.SaddlesAdditVal = pcl030.SaddlesAdditVal;
                objPCL030.SectionWt200To230Val = pcl030.SectionWt200To230Val;
                objPCL030.SectionWt150To200Val = pcl030.SectionWt150To200Val;

                if (pcl030.LineId > 0)
                {
                    objPCL030.EditedBy = objClsLoginInfo.UserName;
                    objPCL030.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL030.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL030.CreatedBy = objClsLoginInfo.UserName;
                    objPCL030.CreatedOn = DateTime.Now;
                    db.PCL030.Add(objPCL030);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL030.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEOAllowance(PCL031 pcl031, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl031.HeaderId).FirstOrDefault();
                PCL031 objPCL031 = new PCL031();
                if (pcl031.LineId > 0)
                {
                    objPCL031 = db.PCL031.Where(x => x.LineId == pcl031.LineId).FirstOrDefault();
                }
                objPCL031.HeaderId = objPCL001.HeaderId;
                objPCL031.Project = objPCL001.Project;
                objPCL031.Document = objPCL001.Document;
                objPCL031.ShellAllwEB1 = pcl031.ShellAllwEB1;
                objPCL031.ShellAllwEB2 = pcl031.ShellAllwEB2;
                objPCL031.ShrinkageCirc = pcl031.ShrinkageCirc;
                objPCL031.ShringageLong = pcl031.ShringageLong;
                objPCL031.Mcinglong = pcl031.Mcinglong;
                objPCL031.Mcingcirc = pcl031.Mcingcirc;
                objPCL031.NegAllow = pcl031.NegAllow;
                objPCL031.TOvality = pcl031.TOvality;
                objPCL031.TCircum = pcl031.TCircum;
                objPCL031.DEndMcing = pcl031.DEndMcing;
                objPCL031.DEndSeam = pcl031.DEndSeam;
                objPCL031.DEndCirc = pcl031.DEndCirc;
                objPCL031.DEndESSC = pcl031.DEndESSC;
                objPCL031.SLTolFlat = pcl031.SLTolFlat;
                objPCL031.SLTolEleva = pcl031.SLTolEleva;
                objPCL031.SLTolHole = pcl031.SLTolHole;
                objPCL031.CTRGapTop = pcl031.CTRGapTop;
                objPCL031.CTRGapInverted = pcl031.CTRGapInverted;
                objPCL031.CTRAllSet = pcl031.CTRAllSet;

                if (pcl031.LineId > 0)
                {
                    objPCL031.EditedBy = objClsLoginInfo.UserName;
                    objPCL031.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL031.CreatedBy = objClsLoginInfo.UserName;
                    objPCL031.CreatedOn = DateTime.Now;
                    db.PCL031.Add(objPCL031);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL031.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEOScopeOfWork(PCL032 pcl032, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl032.HeaderId).FirstOrDefault();
                PCL032 objPCL032 = new PCL032();
                if (pcl032.LineId > 0)
                {
                    objPCL032 = db.PCL032.Where(x => x.LineId == pcl032.LineId).FirstOrDefault();
                }
                objPCL032.HeaderId = objPCL001.HeaderId;
                objPCL032.Project = objPCL001.Project;
                objPCL032.Document = objPCL001.Document;

                objPCL032.EquipmentNotReq = pcl032.EquipmentNotReq;
                objPCL032.EquipmentShop = pcl032.EquipmentShop;
                objPCL032.EquipmentMC = pcl032.EquipmentMC;
                objPCL032.EquipmentLEMF = pcl032.EquipmentLEMF;
                objPCL032.EquipmentRNW = pcl032.EquipmentRNW;
                objPCL032.EquipmentOutside = pcl032.EquipmentOutside;
                objPCL032.DEndFormingNotReq = pcl032.DEndFormingNotReq;
                objPCL032.DEndFormingShop = pcl032.DEndFormingShop;
                objPCL032.DEndFormingMC = pcl032.DEndFormingMC;
                objPCL032.DEndFormingLEMF = pcl032.DEndFormingLEMF;
                objPCL032.DEndFormingRNW = pcl032.DEndFormingRNW;
                objPCL032.DEndFormingOutside = pcl032.DEndFormingOutside;
                objPCL032.DEndSetupNotReq = pcl032.DEndSetupNotReq;
                objPCL032.DEndSetupShop = pcl032.DEndSetupShop;
                objPCL032.DEndSetupMC = pcl032.DEndSetupMC;
                objPCL032.DEndSetupLEMF = pcl032.DEndSetupLEMF;
                objPCL032.DEndSetupRNW = pcl032.DEndSetupRNW;
                objPCL032.DEndSetupOutside = pcl032.DEndSetupOutside;
                objPCL032.DEndWeldingNotReq = pcl032.DEndWeldingNotReq;
                objPCL032.DEndWeldingShop = pcl032.DEndWeldingShop;
                objPCL032.DEndWeldingMC = pcl032.DEndWeldingMC;
                objPCL032.DEndWeldingLEMF = pcl032.DEndWeldingLEMF;
                objPCL032.DEndWeldingRNW = pcl032.DEndWeldingRNW;
                objPCL032.DEndWeldingOutside = pcl032.DEndWeldingOutside;
                objPCL032.LSNotReq = pcl032.LSNotReq;
                objPCL032.LSShop = pcl032.LSShop;
                objPCL032.LSMC = pcl032.LSMC;
                objPCL032.LSLEMF = pcl032.LSLEMF;
                objPCL032.LSRNW = pcl032.LSRNW;
                objPCL032.LSOutside = pcl032.LSOutside;
                objPCL032.YRingMachiningNotReq = pcl032.YRingMachiningNotReq;
                objPCL032.YRingMachiningShop = pcl032.YRingMachiningShop;
                objPCL032.YRingMachiningMC = pcl032.YRingMachiningMC;
                objPCL032.YRingMachiningLEMF = pcl032.YRingMachiningLEMF;
                objPCL032.YRingMachiningRNW = pcl032.YRingMachiningRNW;
                objPCL032.YRingMachiningOutside = pcl032.YRingMachiningOutside;
                objPCL032.NozMachiningNotReq = pcl032.NozMachiningNotReq;
                objPCL032.NozMachiningShop = pcl032.NozMachiningShop;
                objPCL032.NozMachiningMC = pcl032.NozMachiningMC;
                objPCL032.NozMachiningLEMF = pcl032.NozMachiningLEMF;
                objPCL032.NozMachiningRNW = pcl032.NozMachiningRNW;
                objPCL032.NozMachiningOutside = pcl032.NozMachiningOutside;
                objPCL032.NozOverlayNotReq = pcl032.NozOverlayNotReq;
                objPCL032.NozOverlayMC = pcl032.NozOverlayMC;
                objPCL032.NozOverlayShop = pcl032.NozOverlayShop;
                objPCL032.NozOverlayLEMF = pcl032.NozOverlayLEMF;
                objPCL032.NozOverlayRNW = pcl032.NozOverlayRNW;
                objPCL032.NozOverlayOutside = pcl032.NozOverlayOutside;
                objPCL032.ManOverlayNotReq = pcl032.ManOverlayNotReq;
                objPCL032.ManOverlayShop = pcl032.ManOverlayShop;
                objPCL032.ManOverlayMC = pcl032.ManOverlayMC;
                objPCL032.ManOverlayLEMF = pcl032.ManOverlayLEMF;
                objPCL032.ManOverlayRNW = pcl032.ManOverlayRNW;
                objPCL032.ManOverlayOutside = pcl032.ManOverlayOutside;
                objPCL032.NozFinMachiningNotReq = pcl032.NozFinMachiningNotReq;
                objPCL032.NozFinMachiningShop = pcl032.NozFinMachiningShop;
                objPCL032.NozFinMachiningMC = pcl032.NozFinMachiningMC;
                objPCL032.NozFinMachiningLEMF = pcl032.NozFinMachiningLEMF;
                objPCL032.NozFinMachiningRNW = pcl032.NozFinMachiningRNW;
                objPCL032.NozFinMachiningOutside = pcl032.NozFinMachiningOutside;
                objPCL032.NozWEPNotReq = pcl032.NozWEPNotReq;
                objPCL032.NozWEPShop = pcl032.NozWEPShop;
                objPCL032.NozWEPMC = pcl032.NozWEPMC;
                objPCL032.NozWEPLEMF = pcl032.NozWEPLEMF;
                objPCL032.NozWEPRNW = pcl032.NozWEPRNW;
                objPCL032.NozWEPOutside = pcl032.NozWEPOutside;
                objPCL032.LongWEPNotReq = pcl032.LongWEPNotReq;
                objPCL032.LongWEPShop = pcl032.LongWEPShop;
                objPCL032.LongWEPMC = pcl032.LongWEPMC;
                objPCL032.LongWEPLEMF = pcl032.LongWEPLEMF;
                objPCL032.LongWEPRNW = pcl032.LongWEPRNW;
                objPCL032.LongWEPOutside = pcl032.LongWEPOutside;
                objPCL032.CircWEPNotReq = pcl032.CircWEPNotReq;
                objPCL032.CircWEPShop = pcl032.CircWEPShop;
                objPCL032.CircWEPMC = pcl032.CircWEPMC;
                objPCL032.CircWEPLEMF = pcl032.CircWEPLEMF;
                objPCL032.CircWEPRNW = pcl032.CircWEPRNW;
                objPCL032.CircWEPOutside = pcl032.CircWEPOutside;
                objPCL032.LiftMachiningNotReq = pcl032.LiftMachiningNotReq;
                objPCL032.LiftMachiningShop = pcl032.LiftMachiningShop;
                objPCL032.LiftMachiningMC = pcl032.LiftMachiningMC;
                objPCL032.LiftMachiningLEMF = pcl032.LiftMachiningLEMF;
                objPCL032.LiftMachiningRNW = pcl032.LiftMachiningRNW;
                objPCL032.LiftMachiningOutside = pcl032.LiftMachiningOutside;
                objPCL032.LiftFabricationNotReq = pcl032.LiftFabricationNotReq;
                objPCL032.LiftFabricationShop = pcl032.LiftFabricationShop;
                objPCL032.LiftFabricationMC = pcl032.LiftFabricationMC;
                objPCL032.LiftFabricationLEMF = pcl032.LiftFabricationLEMF;
                objPCL032.LiftFabricationRNW = pcl032.LiftFabricationRNW;
                objPCL032.LiftFabricationOutside = pcl032.LiftFabricationOutside;
                objPCL032.WeldExFabNotReq = pcl032.WeldExFabNotReq;
                objPCL032.WeldExFabShop = pcl032.WeldExFabShop;
                objPCL032.WeldExFabMC = pcl032.WeldExFabMC;
                objPCL032.WeldExFabLEMF = pcl032.WeldExFabLEMF;
                objPCL032.WeldExFabRNW = pcl032.WeldExFabRNW;
                objPCL032.WeldExFabOutside = pcl032.WeldExFabOutside;
                objPCL032.WeldInFabOutside = pcl032.WeldInFabOutside;
                objPCL032.WeldInFabNotReq = pcl032.WeldInFabNotReq;
                objPCL032.WeldInFabShop = pcl032.WeldInFabShop;
                objPCL032.WeldInFabMC = pcl032.WeldInFabMC;
                objPCL032.WeldInFabLEMF = pcl032.WeldInFabLEMF;
                objPCL032.WeldInFabRNW = pcl032.WeldInFabRNW;
                objPCL032.BaffFabNotReq = pcl032.BaffFabNotReq;
                objPCL032.BaffFabShop = pcl032.BaffFabShop;
                objPCL032.BaffFabMC = pcl032.BaffFabMC;
                objPCL032.BaffFabLEMF = pcl032.BaffFabLEMF;
                objPCL032.BaffFabRNW = pcl032.BaffFabRNW;
                objPCL032.BaffFabOutside = pcl032.BaffFabOutside;
                objPCL032.BaffDrilNotReq = pcl032.BaffDrilNotReq;
                objPCL032.BaffDrilShop = pcl032.BaffDrilShop;
                objPCL032.BaffDrilMC = pcl032.BaffDrilMC;
                objPCL032.BaffDrilLEMF = pcl032.BaffDrilLEMF;
                objPCL032.BaffDrilRNW = pcl032.BaffDrilRNW;
                objPCL032.BaffDrilOutside = pcl032.BaffDrilOutside;
                objPCL032.BaffAssemNotReq = pcl032.BaffAssemNotReq;
                objPCL032.BaffAssemShop = pcl032.BaffAssemShop;
                objPCL032.BaffAssemMC = pcl032.BaffAssemMC;
                objPCL032.BaffAssemLEMF = pcl032.BaffAssemLEMF;
                objPCL032.BaffAssemRNW = pcl032.BaffAssemRNW;
                objPCL032.BaffAssemOutside = pcl032.BaffAssemOutside;
                objPCL032.BraketWEPNotReq = pcl032.BraketWEPNotReq;
                objPCL032.BraketWEPShop = pcl032.BraketWEPShop;
                objPCL032.BraketWEPMC = pcl032.BraketWEPMC;
                objPCL032.BraketWEPLEMF = pcl032.BraketWEPLEMF;
                objPCL032.BraketWEPRNW = pcl032.BraketWEPRNW;
                objPCL032.BraketWEPOutside = pcl032.BraketWEPOutside;
                objPCL032.TWFabNotReq = pcl032.TWFabNotReq;
                objPCL032.TWFabShop = pcl032.TWFabShop;
                objPCL032.TWFabMC = pcl032.TWFabMC;
                objPCL032.TWFabLEMF = pcl032.TWFabLEMF;
                objPCL032.TWFabRNW = pcl032.TWFabRNW;
                objPCL032.TWFabOutside = pcl032.TWFabOutside;
                objPCL032.InsFabNotReq = pcl032.InsFabNotReq;
                objPCL032.InsFabShop = pcl032.InsFabShop;
                objPCL032.InsFabMC = pcl032.InsFabMC;
                objPCL032.InsFabLEMF = pcl032.InsFabLEMF;
                objPCL032.InsFabRNW = pcl032.InsFabRNW;
                objPCL032.InsFabOutside = pcl032.InsFabOutside;
                objPCL032.TSFabNotReq = pcl032.TSFabNotReq;
                objPCL032.TSFabShop = pcl032.TSFabShop;
                objPCL032.TSFabMC = pcl032.TSFabMC;
                objPCL032.TSFabLEMF = pcl032.TSFabLEMF;
                objPCL032.TSFabRNW = pcl032.TSFabRNW;
                objPCL032.TSFabOutside = pcl032.TSFabOutside;
                objPCL032.TSDrilNotReq = pcl032.TSDrilNotReq;
                objPCL032.TSDrilShop = pcl032.TSDrilShop;
                objPCL032.TSDrilMC = pcl032.TSDrilMC;
                objPCL032.TSDrilLEMF = pcl032.TSDrilLEMF;
                objPCL032.TSDrilRNW = pcl032.TSDrilRNW;
                objPCL032.TSDrilOutside = pcl032.TSDrilOutside;
                objPCL032.TubingNotReq = pcl032.TubingNotReq;
                objPCL032.TubingShop = pcl032.TubingShop;
                objPCL032.TubingMC = pcl032.TubingMC;
                objPCL032.TubingLEMF = pcl032.TubingLEMF;
                objPCL032.TubingRNW = pcl032.TubingRNW;
                objPCL032.TubingOutside = pcl032.TubingOutside;
                objPCL032.TTWNotReq = pcl032.TTWNotReq;
                objPCL032.TTWShop = pcl032.TTWShop;
                objPCL032.TTWMC = pcl032.TTWMC;
                objPCL032.TTWLEMF = pcl032.TTWLEMF;
                objPCL032.TTWRNW = pcl032.TTWRNW;
                objPCL032.TTWOutside = pcl032.TTWOutside;
                objPCL032.InternalTrayFabricationNotReq = pcl032.InternalTrayFabricationNotReq;
                objPCL032.InternalTrayFabricationShop = pcl032.InternalTrayFabricationShop;
                objPCL032.InternalTrayFabricationMC = pcl032.InternalTrayFabricationMC;
                objPCL032.InternalTrayFabricationLEMF = pcl032.InternalTrayFabricationLEMF;
                objPCL032.InternalTrayFabricationRNW = pcl032.InternalTrayFabricationRNW;
                objPCL032.InternalTrayFabricationOutside = pcl032.InternalTrayFabricationOutside;
                objPCL032.InletDiffuserFabricationNotReq = pcl032.InletDiffuserFabricationNotReq;
                objPCL032.InletDiffuserFabricationShop = pcl032.InletDiffuserFabricationShop;
                objPCL032.InletDiffuserFabricationMC = pcl032.InletDiffuserFabricationMC;
                objPCL032.InletDiffuserFabricationLEMF = pcl032.InletDiffuserFabricationLEMF;
                objPCL032.InletDiffuserFabricationRNW = pcl032.InletDiffuserFabricationRNW;
                objPCL032.InletDiffuserFabricationOutside = pcl032.InletDiffuserFabricationOutside;
                objPCL032.InsertNotReq = pcl032.InsertNotReq;
                objPCL032.InsertShop = pcl032.InsertShop;
                objPCL032.InsertMC = pcl032.InsertMC;
                objPCL032.InsertLEMF = pcl032.InsertLEMF;
                objPCL032.InsertRNW = pcl032.InsertRNW;
                objPCL032.InsertOutside = pcl032.InsertOutside;
                objPCL032.InternalNozzlesNotReq = pcl032.InternalNozzlesNotReq;
                objPCL032.InternalNozzlesShop = pcl032.InternalNozzlesShop;
                objPCL032.InternalNozzlesMC = pcl032.InternalNozzlesMC;
                objPCL032.InternalNozzlesLEMF = pcl032.InternalNozzlesLEMF;
                objPCL032.InternalNozzlesRNW = pcl032.InternalNozzlesRNW;
                objPCL032.InternalNozzlesOutside = pcl032.InternalNozzlesOutside;
                objPCL032.SaddlesNotReq = pcl032.SaddlesNotReq;
                objPCL032.SaddlesShop = pcl032.SaddlesShop;
                objPCL032.SaddlesMC = pcl032.SaddlesMC;
                objPCL032.SaddlesLEMF = pcl032.SaddlesLEMF;
                objPCL032.SaddlesRNW = pcl032.SaddlesRNW;
                objPCL032.SaddlesOutside = pcl032.SaddlesOutside;
                objPCL032.SkirtTemplateNotReq = pcl032.SkirtTemplateNotReq;
                objPCL032.SkirtTemplateShop = pcl032.SkirtTemplateShop;
                objPCL032.SkirtTemplateMC = pcl032.SkirtTemplateMC;
                objPCL032.SkirtTemplateLEMF = pcl032.SkirtTemplateLEMF;
                objPCL032.SkirtTemplateRNW = pcl032.SkirtTemplateRNW;
                objPCL032.SkirtTemplateOutside = pcl032.SkirtTemplateOutside;
                objPCL032.PlatformFabricationNotReq = pcl032.PlatformFabricationNotReq;
                objPCL032.PlatformFabricationShop = pcl032.PlatformFabricationShop;
                objPCL032.PlatformFabricationMC = pcl032.PlatformFabricationMC;
                objPCL032.PlatformFabricationLEMF = pcl032.PlatformFabricationLEMF;
                objPCL032.PlatformFabricationRNW = pcl032.PlatformFabricationRNW;
                objPCL032.PlatformFabricationOutside = pcl032.PlatformFabricationOutside;
                objPCL032.PlatformTrialAssemblyNotReq = pcl032.PlatformTrialAssemblyNotReq;
                objPCL032.PlatformTrialAssemblyShop = pcl032.PlatformTrialAssemblyShop;
                objPCL032.PlatformTrialAssemblyMC = pcl032.PlatformTrialAssemblyMC;
                objPCL032.PlatformTrialAssemblyLEMF = pcl032.PlatformTrialAssemblyLEMF;
                objPCL032.PlatformTrialAssemblyRNW = pcl032.PlatformTrialAssemblyRNW;
                objPCL032.PlatformTrialAssemblyOutside = pcl032.PlatformTrialAssemblyOutside;
                objPCL032.PWHTNotReq = pcl032.PWHTNotReq;
                objPCL032.PWHTShop = pcl032.PWHTShop;
                objPCL032.PWHTMC = pcl032.PWHTMC;
                objPCL032.PWHTLEMF = pcl032.PWHTLEMF;
                objPCL032.PWHTRNW = pcl032.PWHTRNW;
                objPCL032.PWHTOutside = pcl032.PWHTOutside;
                objPCL032.InternalInstallationNotReq = pcl032.InternalInstallationNotReq;
                objPCL032.InternalInstallationShop = pcl032.InternalInstallationShop;
                objPCL032.InternalInstallationMC = pcl032.InternalInstallationMC;
                objPCL032.InternalInstallationLEMF = pcl032.InternalInstallationLEMF;
                objPCL032.InternalInstallationRNW = pcl032.InternalInstallationRNW;
                objPCL032.InternalInstallationOutside = pcl032.InternalInstallationOutside;
                objPCL032.N2FillingNotReq = pcl032.N2FillingNotReq;
                objPCL032.N2FillingShop = pcl032.N2FillingShop;
                objPCL032.N2FillingMC = pcl032.N2FillingMC;
                objPCL032.N2FillingLEMF = pcl032.N2FillingLEMF;
                objPCL032.N2FillingRNW = pcl032.N2FillingRNW;
                objPCL032.N2FillingOutside = pcl032.N2FillingOutside;
                objPCL032.SandBlastingNotReq = pcl032.SandBlastingNotReq;
                objPCL032.SandBlastingShop = pcl032.SandBlastingShop;
                objPCL032.SandBlastingMC = pcl032.SandBlastingMC;
                objPCL032.SandBlastingLEMF = pcl032.SandBlastingLEMF;
                objPCL032.SandBlastingRNW = pcl032.SandBlastingRNW;
                objPCL032.SandBlastingOutside = pcl032.SandBlastingOutside;
                objPCL032.PaintingNotReq = pcl032.PaintingNotReq;
                objPCL032.PaintingShop = pcl032.PaintingShop;
                objPCL032.PaintingMC = pcl032.PaintingMC;
                objPCL032.PaintingLEMF = pcl032.PaintingLEMF;
                objPCL032.PaintingRNW = pcl032.PaintingRNW;
                objPCL032.PaintingOutside = pcl032.PaintingOutside;
                objPCL032.AcidCleaningNotReq = pcl032.AcidCleaningNotReq;
                objPCL032.AcidCleaningShop = pcl032.AcidCleaningShop;
                objPCL032.AcidCleaningMC = pcl032.AcidCleaningMC;
                objPCL032.AcidCleaningLEMF = pcl032.AcidCleaningLEMF;
                objPCL032.AcidCleaningRNW = pcl032.AcidCleaningRNW;
                objPCL032.AcidCleaningOutside = pcl032.AcidCleaningOutside;
                objPCL032.WeightMeasurementNotReq = pcl032.WeightMeasurementNotReq;
                objPCL032.WeightMeasurementShop = pcl032.WeightMeasurementShop;
                objPCL032.WeightMeasurementMC = pcl032.WeightMeasurementMC;
                objPCL032.WeightMeasurementLEMF = pcl032.WeightMeasurementLEMF;
                objPCL032.WeightMeasurementRNW = pcl032.WeightMeasurementRNW;
                objPCL032.WeightMeasurementOutside = pcl032.WeightMeasurementOutside;
                objPCL032.ManwayDavitNotReq = pcl032.ManwayDavitNotReq;
                objPCL032.ManwayDavitShop = pcl032.ManwayDavitShop;
                objPCL032.ManwayDavitMC = pcl032.ManwayDavitMC;
                objPCL032.ManwayDavitLEMF = pcl032.ManwayDavitLEMF;
                objPCL032.ManwayDavitRNW = pcl032.ManwayDavitRNW;
                objPCL032.ManwayDavitOutside = pcl032.ManwayDavitOutside;
                objPCL032.VesselDavitNotReq = pcl032.VesselDavitNotReq;
                objPCL032.VesselDavitShop = pcl032.VesselDavitShop;
                objPCL032.VesselDavitMC = pcl032.VesselDavitMC;
                objPCL032.VesselDavitLEMF = pcl032.VesselDavitLEMF;
                objPCL032.VesselDavitRNW = pcl032.VesselDavitRNW;
                objPCL032.VesselDavitOutside = pcl032.VesselDavitOutside;
                objPCL032.LappingRingNotReq = pcl032.LappingRingNotReq;
                objPCL032.LappingRingShop = pcl032.LappingRingShop;
                objPCL032.LappingRingMC = pcl032.LappingRingMC;
                objPCL032.LappingRingLEMF = pcl032.LappingRingLEMF;
                objPCL032.LappingRingRNW = pcl032.LappingRingRNW;
                objPCL032.LappingRingOutside = pcl032.LappingRingOutside;
                objPCL032.ShippingNotReq = pcl032.ShippingNotReq;
                objPCL032.ShippingShop = pcl032.ShippingShop;
                objPCL032.ShippingMC = pcl032.ShippingMC;
                objPCL032.ShippingLEMF = pcl032.ShippingLEMF;
                objPCL032.ShippingRNW = pcl032.ShippingRNW;
                objPCL032.ShippingOutside = pcl032.ShippingOutside;

                if (pcl032.LineId > 0)
                {
                    objPCL032.EditedBy = objClsLoginInfo.UserName;
                    objPCL032.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL032.CreatedBy = objClsLoginInfo.UserName;
                    objPCL032.CreatedOn = DateTime.Now;
                    db.PCL032.Add(objPCL032);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL032.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEOFixture(PCL033 pcl033, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl033.HeaderId).FirstOrDefault();
                PCL033 objPCL033 = new PCL033();
                bool IsEdited = false;
                if (pcl033.LineId > 0)
                {
                    objPCL033 = db.PCL033.Where(x => x.LineId == pcl033.LineId).FirstOrDefault();
                    IsEdited = true;
                }

                objPCL033.HeaderId = objPCL001.HeaderId;
                objPCL033.Project = objPCL001.Project;
                objPCL033.Document = objPCL001.Document;
                objPCL033.BDSReq = pcl033.BDSReq;
                objPCL033.BDSReqDate = pcl033.BDSReqDate;
                string BDSReqDate = fc["BDSReqDate"] != null ? fc["BDSReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BDSReqDate))
                {
                    objPCL033.BDSReqDate = DateTime.ParseExact(BDSReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.BDSNoFix = pcl033.BDSNoFix;
                objPCL033.BDSAvlQty = pcl033.BDSAvlQty;
                objPCL033.BDSDrg = pcl033.BDSDrg;
                objPCL033.BDSMtrl = pcl033.BDSMtrl;
                objPCL033.BDSFab = pcl033.BDSFab;
                objPCL033.BDSDel = pcl033.BDSDel;
                objPCL033.DummyShellReq = pcl033.DummyShellReq;
                objPCL033.DummyShellReqDate = pcl033.DummyShellReqDate;
                string DummyShellReqDate = fc["DummyShellReqDate"] != null ? fc["DummyShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DummyShellReqDate))
                {
                    objPCL033.DummyShellReqDate = DateTime.ParseExact(DummyShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.DummyShellNoFix = pcl033.DummyShellNoFix;
                objPCL033.DummyShellAvlQty = pcl033.DummyShellAvlQty;
                objPCL033.DummyShellDrg = pcl033.DummyShellDrg;
                objPCL033.DummyShellMtrl = pcl033.DummyShellMtrl;
                objPCL033.DummyShellFab = pcl033.DummyShellFab;
                objPCL033.DummyShellDel = pcl033.DummyShellDel;
                objPCL033.SBWReq = pcl033.SBWReq;
                objPCL033.SBWReqDate = pcl033.SBWReqDate;
                string SBWReqDate = fc["SBWReqDate"] != null ? fc["SBWReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SBWReqDate))
                {
                    objPCL033.SBWReqDate = DateTime.ParseExact(SBWReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SBWNoFix = pcl033.SBWNoFix;
                objPCL033.SBWAvlQty = pcl033.SBWAvlQty;
                objPCL033.SBWDrg = pcl033.SBWDrg;
                objPCL033.SBWMtrl = pcl033.SBWMtrl;
                objPCL033.SBWFab = pcl033.SBWFab;
                objPCL033.SBWDel = pcl033.SBWDel;
                objPCL033.SMBReq = pcl033.SMBReq;
                objPCL033.SMBReqDate = pcl033.SMBReqDate;
                string SMBReqDate = fc["SMBReqDate"] != null ? fc["SMBReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SMBReqDate))
                {
                    objPCL033.SMBReqDate = DateTime.ParseExact(SMBReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SMBNoFix = pcl033.SMBNoFix;
                objPCL033.SMBAvlQty = pcl033.SMBAvlQty;
                objPCL033.SMBDrg = pcl033.SMBDrg;
                objPCL033.SMBMtrl = pcl033.SMBMtrl;
                objPCL033.SMBFab = pcl033.SMBFab;
                objPCL033.SMBDel = pcl033.SMBDel;
                objPCL033.SMTReq = pcl033.SMTReq;
                objPCL033.SMTReqDate = pcl033.SMTReqDate;
                string SMTReqDate = fc["SMTReqDate"] != null ? fc["SMTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SMTReqDate))
                {
                    objPCL033.SMTReqDate = DateTime.ParseExact(SMTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SMTNoFix = pcl033.SMTNoFix;
                objPCL033.SMTAvlQty = pcl033.SMTAvlQty;
                objPCL033.SMTDrg = pcl033.SMTDrg;
                objPCL033.SMTMtrl = pcl033.SMTMtrl;
                objPCL033.SMTFab = pcl033.SMTFab;
                objPCL033.SMTDel = pcl033.SMTDel;
                objPCL033.BAFReq = pcl033.BAFReq;
                objPCL033.BAFReqDate = pcl033.BAFReqDate;
                objPCL033.BAFNoFix = pcl033.BAFNoFix;
                objPCL033.BAFAvlQty = pcl033.BAFAvlQty;
                objPCL033.BAFDrg = pcl033.BAFDrg;
                objPCL033.BAFMtrl = pcl033.BAFMtrl;
                objPCL033.BAFFab = pcl033.BAFFab;
                objPCL033.BAFDel = pcl033.BAFDel;
                objPCL033.BLNReq = pcl033.BLNReq;
                objPCL033.BLNReqDate = pcl033.BLNReqDate;
                string BLNReqDate = fc["BLNReqDate"] != null ? fc["BLNReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BLNReqDate))
                {
                    objPCL033.BLNReqDate = DateTime.ParseExact(BLNReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.BLNNoFix = pcl033.BLNNoFix;
                objPCL033.BLNAvlQty = pcl033.BLNAvlQty;
                objPCL033.BLNDrg = pcl033.BLNDrg;
                objPCL033.BLNMtrl = pcl033.BLNMtrl;
                objPCL033.BLNFab = pcl033.BLNFab;
                objPCL033.BLNDel = pcl033.BLNDel;
                objPCL033.SBLReq = pcl033.SBLReq;
                objPCL033.SBLReqDate = pcl033.SBLReqDate;
                string SBLReqDate = fc["SBLReqDate"] != null ? fc["SBLReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SBLReqDate))
                {
                    objPCL033.SBLReqDate = DateTime.ParseExact(SBLReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SBLNoFix = pcl033.SBLNoFix;
                objPCL033.SBLAvlQty = pcl033.SBLAvlQty;
                objPCL033.SBLDrg = pcl033.SBLDrg;
                objPCL033.SBLMtrl = pcl033.SBLMtrl;
                objPCL033.SBLFab = pcl033.SBLFab;
                objPCL033.SBLDel = pcl033.SBLDel;
                objPCL033.TRIReq = pcl033.TRIReq;
                objPCL033.TRIReqDate = pcl033.TRIReqDate;
                string TRIReqDate = fc["TRIReqDate"] != null ? fc["TRIReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TRIReqDate))
                {
                    objPCL033.TRIReqDate = DateTime.ParseExact(TRIReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.TRINoFix = pcl033.TRINoFix;
                objPCL033.TRIAvlQty = pcl033.TRIAvlQty;
                objPCL033.TRIDrg = pcl033.TRIDrg;
                objPCL033.TRIMtrl = pcl033.TRIMtrl;
                objPCL033.TRIFab = pcl033.TRIFab;
                objPCL033.TRIDel = pcl033.TRIDel;
                objPCL033.LTCReq = pcl033.LTCReq;
                objPCL033.LTCReqDate = pcl033.LTCReqDate;
                string LTCReqDate = fc["LTCReqDate"] != null ? fc["LTCReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LTCReqDate))
                {
                    objPCL033.LTCReqDate = DateTime.ParseExact(LTCReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.LTCNoFix = pcl033.LTCNoFix;
                objPCL033.LTCAvlQty = pcl033.LTCAvlQty;
                objPCL033.LTCDrg = pcl033.LTCDrg;
                objPCL033.LTCMtrl = pcl033.LTCMtrl;
                objPCL033.LTCFab = pcl033.LTCFab;
                objPCL033.LTCDel = pcl033.LTCDel;
                objPCL033.PAFReq = pcl033.PAFReq;
                objPCL033.PAFReqDate = pcl033.PAFReqDate;
                string PAFReqDate = fc["PAFReqDate"] != null ? fc["PAFReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PAFReqDate))
                {
                    objPCL033.PAFReqDate = DateTime.ParseExact(PAFReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.PAFNoFix = pcl033.PAFNoFix;
                objPCL033.PAFAvlQty = pcl033.PAFAvlQty;
                objPCL033.PAFDrg = pcl033.PAFDrg;
                objPCL033.PAFMtrl = pcl033.PAFMtrl;
                objPCL033.PAFFab = pcl033.PAFFab;
                objPCL033.PAFDel = pcl033.PAFDel;
                objPCL033.IIFReq = pcl033.IIFReq;
                objPCL033.IIFReqDate = pcl033.IIFReqDate;
                string IIFReqDate = fc["IIFReqDate"] != null ? fc["IIFReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(IIFReqDate))
                {
                    objPCL033.IIFReqDate = DateTime.ParseExact(IIFReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.IIFNoFix = pcl033.IIFNoFix;
                objPCL033.IIFAvlQty = pcl033.IIFAvlQty;
                objPCL033.IIFDrg = pcl033.IIFDrg;
                objPCL033.IIFMtrl = pcl033.IIFMtrl;
                objPCL033.IIFFab = pcl033.IIFFab;
                objPCL033.IIFDel = pcl033.IIFDel;
                objPCL033.DSFReq = pcl033.DSFReq;
                objPCL033.DSFReqDate = pcl033.DSFReqDate;
                string DSFReqDate = fc["DSFReqDate"] != null ? fc["DSFReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DSFReqDate))
                {
                    objPCL033.DSFReqDate = DateTime.ParseExact(DSFReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL033.DSFNoFix = pcl033.DSFNoFix;
                objPCL033.DSFAvlQty = pcl033.DSFAvlQty;
                objPCL033.DSFDrg = pcl033.DSFDrg;
                objPCL033.DSFMtrl = pcl033.DSFMtrl;
                objPCL033.DSFFab = pcl033.DSFFab;
                objPCL033.DSFDel = pcl033.DSFDel;
                objPCL033.ISRReq = pcl033.ISRReq;
                objPCL033.ISRReqDate = pcl033.ISRReqDate;
                string ISRReqDate = fc["ISRReqDate"] != null ? fc["ISRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ISRReqDate))
                {
                    objPCL033.ISRReqDate = DateTime.ParseExact(ISRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.ISRNoFix = pcl033.ISRNoFix;
                objPCL033.ISRAvlQty = pcl033.ISRAvlQty;
                objPCL033.ISRDrg = pcl033.ISRDrg;
                objPCL033.ISRMtrl = pcl033.ISRMtrl;
                objPCL033.ISRFab = pcl033.ISRFab;
                objPCL033.ISRDel = pcl033.ISRDel;
                objPCL033.SBDReq = pcl033.SBDReq;
                objPCL033.SBDReqDate = pcl033.SBDReqDate;
                string SBDReqDate = fc["SBDReqDate"] != null ? fc["SBDReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SBDReqDate))
                {
                    objPCL033.SBDReqDate = DateTime.ParseExact(SBDReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SBDNoFix = pcl033.SBDNoFix;
                objPCL033.SBDAvlQty = pcl033.SBDAvlQty;
                objPCL033.SBDDrg = pcl033.SBDDrg;
                objPCL033.SBDMtrl = pcl033.SBDMtrl;
                objPCL033.SBDFab = pcl033.SBDFab;
                objPCL033.SBDDel = pcl033.SBDDel;
                objPCL033.SPWHTReq = pcl033.SPWHTReq;
                objPCL033.SPWHTReqDate = pcl033.SPWHTReqDate;
                string SPWHTReqDate = fc["SPWHTReqDate"] != null ? fc["SPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SPWHTReqDate))
                {
                    objPCL033.SPWHTReqDate = DateTime.ParseExact(SPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SPWHTNoFix = pcl033.SPWHTNoFix;
                objPCL033.SPWHTAvlQty = pcl033.SPWHTAvlQty;
                objPCL033.SPWHTDrg = pcl033.SPWHTDrg;
                objPCL033.SPWHTMtrl = pcl033.SPWHTMtrl;
                objPCL033.SPWHTFab = pcl033.SPWHTFab;
                objPCL033.SPWHTDel = pcl033.SPWHTDel;
                objPCL033.SHReq = pcl033.SHReq;
                objPCL033.SHReqDate = pcl033.SHReqDate;
                string SHReqDate = fc["SHReqDate"] != null ? fc["SHReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SHReqDate))
                {
                    objPCL033.SHReqDate = DateTime.ParseExact(SHReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SHNoFix = pcl033.SHNoFix;
                objPCL033.SHAvlQty = pcl033.SHAvlQty;
                objPCL033.SHDrg = pcl033.SHDrg;
                objPCL033.SHMtrl = pcl033.SHMtrl;
                objPCL033.SHFab = pcl033.SHFab;
                objPCL033.SHDel = pcl033.SHDel;
                objPCL033.MPHReq = pcl033.MPHReq;
                objPCL033.MPHReqDate = pcl033.MPHReqDate;
                string MPHReqDate = fc["MPHReqDate"] != null ? fc["MPHReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(MPHReqDate))
                {
                    objPCL033.MPHReqDate = DateTime.ParseExact(MPHReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.MPHNoFix = pcl033.MPHNoFix;
                objPCL033.MPHAvlQty = pcl033.MPHAvlQty;
                objPCL033.MPHDrg = pcl033.MPHDrg;
                objPCL033.MPHMtrl = pcl033.MPHMtrl;
                objPCL033.MPHFab = pcl033.MPHFab;
                objPCL033.MPHDel = pcl033.MPHDel;
                objPCL033.SCLReq = pcl033.SCLReq;
                objPCL033.SCLReqDate = pcl033.SCLReqDate;
                string SCLReqDate = fc["SCLReqDate"] != null ? fc["SCLReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SCLReqDate))
                {
                    objPCL033.SCLReqDate = DateTime.ParseExact(SCLReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.SCLNoFix = pcl033.SCLNoFix;
                objPCL033.SCLAvlQty = pcl033.SCLAvlQty;
                objPCL033.SCLDrg = pcl033.SCLDrg;
                objPCL033.SCLMtrl = pcl033.SCLMtrl;
                objPCL033.SCLFab = pcl033.SCLFab;
                objPCL033.SCLDel = pcl033.SCLDel;
                objPCL033.BIPReq = pcl033.BIPReq;
                objPCL033.BIPReqDate = pcl033.BIPReqDate;
                string BIPReqDate = fc["BIPReqDate"] != null ? fc["BIPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BIPReqDate))
                {
                    objPCL033.BIPReqDate = DateTime.ParseExact(BIPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.BIPNoFix = pcl033.BIPNoFix;
                objPCL033.BIPAvlQty = pcl033.BIPAvlQty;
                objPCL033.BIPDrg = pcl033.BIPDrg;
                objPCL033.BIPMtrl = pcl033.BIPMtrl;
                objPCL033.BIPFab = pcl033.BIPFab;
                objPCL033.BIPDel = pcl033.BIPDel;
                objPCL033.BTFReq = pcl033.BTFReq;
                objPCL033.BTFReqDate = pcl033.BTFReqDate;
                string BTFReqDate = fc["BTFReqDate"] != null ? fc["BTFReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BTFReqDate))
                {
                    objPCL033.BTFReqDate = DateTime.ParseExact(BTFReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.BTFNoFix = pcl033.BTFNoFix;
                objPCL033.BTFAvlQty = pcl033.BTFAvlQty;
                objPCL033.BTFDrg = pcl033.BTFDrg;
                objPCL033.BTFMtrl = pcl033.BTFMtrl;
                objPCL033.BTFFab = pcl033.BTFFab;
                objPCL033.BTFDel = pcl033.BTFDel;
                objPCL033.WRReq = pcl033.WRReq;
                objPCL033.WRReqDate = pcl033.WRReqDate;
                string WRReqDate = fc["WRReqDate"] != null ? fc["WRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(WRReqDate))
                {
                    objPCL033.WRReqDate = DateTime.ParseExact(WRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL033.WRNoFix = pcl033.WRNoFix;
                objPCL033.WRAvlQty = pcl033.WRAvlQty;
                objPCL033.WRDrg = pcl033.WRDrg;
                objPCL033.WRMtrl = pcl033.WRMtrl;
                objPCL033.WRFab = pcl033.WRFab;
                objPCL033.WRDel = pcl033.WRDel;

                if (IsEdited)
                {
                    objPCL033.EditedBy = objClsLoginInfo.UserName;
                    objPCL033.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL033.CreatedBy = objClsLoginInfo.UserName;
                    objPCL033.CreatedOn = DateTime.Now;
                    db.PCL033.Add(objPCL033);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL033.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEODocument(PCL034 pcl034, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl034.HeaderId).FirstOrDefault();
                PCL034 objPCL034 = new PCL034();
                if (pcl034.LineId > 0)
                {
                    objPCL034 = db.PCL034.Where(x => x.LineId == pcl034.LineId).FirstOrDefault();
                }
                objPCL034.HeaderId = objPCL001.HeaderId;
                objPCL034.Project = objPCL001.Project;
                objPCL034.Document = objPCL001.Document;

                objPCL034.JPPReq = pcl034.JPPReq;
                objPCL034.JPPReqDate = pcl034.JPPReqDate;
                string JPPReqDate = fc["JPPReqDate"] != null ? fc["JPPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReqDate))
                {
                    objPCL034.JPPReqDate = DateTime.ParseExact(JPPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.JPPDocNo = pcl034.JPPDocNo;
                objPCL034.JPPReleasedOn = pcl034.JPPReleasedOn;
                string JPPReleasedOn = fc["JPPReleasedOn"] != null ? fc["JPPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReleasedOn))
                {
                    objPCL034.JPPReleasedOn = DateTime.ParseExact(JPPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RollingCapaReq = pcl034.RollingCapaReq;
                objPCL034.RollingCapaReqDate = pcl034.RollingCapaReqDate;
                string RollingCapaReqDate = fc["RollingCapaReqDate"] != null ? fc["RollingCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReqDate))
                {
                    objPCL034.RollingCapaReqDate = DateTime.ParseExact(RollingCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RollingCapaDocNo = pcl034.RollingCapaDocNo;
                objPCL034.RollingCapaReleasedOn = pcl034.RollingCapaReleasedOn;
                string RollingCapaReleasedOn = fc["RollingCapaReleasedOn"] != null ? fc["RollingCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReleasedOn))
                {
                    objPCL034.RollingCapaReleasedOn = DateTime.ParseExact(RollingCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.ImprovBudgetReq = pcl034.ImprovBudgetReq;
                objPCL034.ImprovBudgetReqDate = pcl034.ImprovBudgetReqDate;
                string ImprovBudgetReqDate = fc["ImprovBudgetReqDate"] != null ? fc["ImprovBudgetReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReqDate))
                {
                    objPCL034.ImprovBudgetReqDate = DateTime.ParseExact(ImprovBudgetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.ImprovBudgetDocNo = pcl034.ImprovBudgetDocNo;
                objPCL034.ImprovBudgetReleasedOn = pcl034.ImprovBudgetReleasedOn;
                string ImprovBudgetReleasedOn = fc["ImprovBudgetReleasedOn"] != null ? fc["ImprovBudgetReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReleasedOn))
                {
                    objPCL034.ImprovBudgetReleasedOn = DateTime.ParseExact(ImprovBudgetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RCCPBreakUpReq = pcl034.RCCPBreakUpReq;
                objPCL034.RCCPBreakUpReqDate = pcl034.RCCPBreakUpReqDate;
                string RCCPBreakUpReqDate = fc["RCCPBreakUpReqDate"] != null ? fc["RCCPBreakUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReqDate))
                {
                    objPCL034.RCCPBreakUpReqDate = DateTime.ParseExact(RCCPBreakUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RCCPBreakUpDocNo = pcl034.RCCPBreakUpDocNo;
                objPCL034.RCCPBreakUpReleasedOn = pcl034.RCCPBreakUpReleasedOn;
                string RCCPBreakUpReleasedOn = fc["RCCPBreakUpReleasedOn"] != null ? fc["RCCPBreakUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReleasedOn))
                {
                    objPCL034.RCCPBreakUpReleasedOn = DateTime.ParseExact(RCCPBreakUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.SchedConcertoReq = pcl034.SchedConcertoReq;
                objPCL034.SchedConcertoReqDate = pcl034.SchedConcertoReqDate;
                string SchedConcertoReqDate = fc["SchedConcertoReqDate"] != null ? fc["SchedConcertoReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReqDate))
                {
                    objPCL034.SchedConcertoReqDate = DateTime.ParseExact(SchedConcertoReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.SchedConcertoDocNo = pcl034.SchedConcertoDocNo;
                objPCL034.SchedConcertoReleasedOn = pcl034.SchedConcertoReleasedOn;
                string SchedConcertoReleasedOn = fc["SchedConcertoReleasedOn"] != null ? fc["SchedConcertoReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReleasedOn))
                {
                    objPCL034.SchedConcertoReleasedOn = DateTime.ParseExact(SchedConcertoReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.SchedExlReq = pcl034.SchedExlReq;
                objPCL034.SchedExlReqDate = pcl034.SchedExlReqDate;
                string SchedExlReqDate = fc["SchedExlReqDate"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReqDate))
                {
                    objPCL034.SchedExlReqDate = DateTime.ParseExact(SchedExlReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.SchedExlDocNo = pcl034.SchedExlDocNo;
                objPCL034.SchedExlReleasedOn = pcl034.SchedExlReleasedOn;
                string SchedExlReleasedOn = fc["SchedExlReleasedOn"] != null ? fc["SchedExlReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReleasedOn))
                {
                    objPCL034.SchedExlReleasedOn = DateTime.ParseExact(SchedExlReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.SubcontPlanReq = pcl034.SubcontPlanReq;
                objPCL034.SubcontPlanReqDate = pcl034.SubcontPlanReqDate;
                string SubcontPlanReqDate = fc["SubcontPlanReqDate"] != null ? fc["SubcontPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReqDate))
                {
                    objPCL034.SubcontPlanReqDate = DateTime.ParseExact(SubcontPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.SubcontPlanDocNo = pcl034.SubcontPlanDocNo;
                objPCL034.SubcontPlanReleasedOn = pcl034.SubcontPlanReleasedOn;
                string SubcontPlanReleasedOn = fc["SubcontPlanReleasedOn"] != null ? fc["SubcontPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReleasedOn))
                {
                    objPCL034.SubcontPlanReleasedOn = DateTime.ParseExact(SubcontPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.LongSeamReq = pcl034.LongSeamReq;
                objPCL034.LongSeamReqDate = pcl034.LongSeamReqDate;
                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL034.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.LongSeamDocNo = pcl034.LongSeamDocNo;
                objPCL034.LongSeamReleasedOn = pcl034.LongSeamReleasedOn;
                string LongSeamReleasedOn = fc["LongSeamReleasedOn"] != null ? fc["LongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReleasedOn))
                {
                    objPCL034.LongSeamReleasedOn = DateTime.ParseExact(LongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.CircSeamReq = pcl034.CircSeamReq;
                objPCL034.CircSeamReqDate = pcl034.CircSeamReqDate;
                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL034.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.CircSeamDocNo = pcl034.CircSeamDocNo;
                objPCL034.CircSeamReleasedOn = pcl034.CircSeamReleasedOn;
                string CircSeamReleasedOn = fc["CircSeamReleasedOn"] != null ? fc["CircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReleasedOn))
                {
                    objPCL034.CircSeamReleasedOn = DateTime.ParseExact(CircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.NozzleShellReq = pcl034.NozzleShellReq;
                objPCL034.NozzleShellReqDate = pcl034.NozzleShellReqDate;
                string NozzleShellReqDate = fc["NozzleShellReqDate"] != null ? fc["NozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReqDate))
                {
                    objPCL034.NozzleShellReqDate = DateTime.ParseExact(NozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.NozzleShellDocNo = pcl034.NozzleShellDocNo;
                objPCL034.NozzleShellReleasedOn = pcl034.NozzleShellReleasedOn;
                string NozzleShellReleasedOn = fc["NozzleShellReleasedOn"] != null ? fc["NozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReleasedOn))
                {
                    objPCL034.NozzleShellReleasedOn = DateTime.ParseExact(NozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RollCapaReq = pcl034.RollCapaReq;
                objPCL034.RollCapaReqDate = pcl034.RollCapaReqDate;
                string RollCapaReqDate = fc["RollCapaReqDate"] != null ? fc["RollCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReqDate))
                {
                    objPCL034.RollCapaReqDate = DateTime.ParseExact(RollCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RollCapaDocNo = pcl034.RollCapaDocNo;
                objPCL034.RollCapaReleasedOn = pcl034.RollCapaReleasedOn;
                string RollCapaReleasedOn = fc["RollCapaReleasedOn"] != null ? fc["RollCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReleasedOn))
                {
                    objPCL034.RollCapaReleasedOn = DateTime.ParseExact(RollCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TablesNozReq = pcl034.TablesNozReq;
                objPCL034.TablesNozReqDate = pcl034.TablesNozReqDate;
                string TablesNozReqDate = fc["TablesNozReqDate"] != null ? fc["TablesNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReqDate))
                {
                    objPCL034.TablesNozReqDate = DateTime.ParseExact(TablesNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TablesNozDocNo = pcl034.TablesNozDocNo;
                objPCL034.TablesNozReleasedOn = pcl034.TablesNozReleasedOn;
                string TablesNozReleasedOn = fc["TablesNozReleasedOn"] != null ? fc["TablesNozReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReleasedOn))
                {
                    objPCL034.TablesNozReleasedOn = DateTime.ParseExact(TablesNozReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TableIntReq = pcl034.TableIntReq;
                objPCL034.TableIntReqDate = pcl034.TableIntReqDate;
                string TableIntReqDate = fc["TableIntReqDate"] != null ? fc["TableIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReqDate))
                {
                    objPCL034.TableIntReqDate = DateTime.ParseExact(TableIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TableIntDocNo = pcl034.TableIntDocNo;
                objPCL034.TableIntReleasedOn = pcl034.TableIntReleasedOn;
                string TableIntReleasedOn = fc["TableIntReleasedOn"] != null ? fc["TableIntReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReleasedOn))
                {
                    objPCL034.TableIntReleasedOn = DateTime.ParseExact(TableIntReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TableExtReq = pcl034.TableExtReq;
                objPCL034.TableExtReqDate = pcl034.TableExtReqDate;
                string TableExtReqDate = fc["TableExtReqDate"] != null ? fc["TableExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReqDate))
                {
                    objPCL034.TableExtReqDate = DateTime.ParseExact(TableExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TableExtDocNo = pcl034.TableExtDocNo;
                objPCL034.TableExtReleasedOn = pcl034.TableExtReleasedOn;
                string TableExtReleasedOn = fc["TableExtReleasedOn"] != null ? fc["TableExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReleasedOn))
                {
                    objPCL034.TableExtReleasedOn = DateTime.ParseExact(TableExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTREdgeReq = pcl034.HTREdgeReq;
                objPCL034.HTREdgeReqDate = pcl034.HTREdgeReqDate;
                string HTREdgeReqDate = fc["HTREdgeReqDate"] != null ? fc["HTREdgeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReqDate))
                {
                    objPCL034.HTREdgeReqDate = DateTime.ParseExact(HTREdgeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTREdgeDocNo = pcl034.HTREdgeDocNo;
                objPCL034.HTREdgeReleasedOn = pcl034.HTREdgeReleasedOn;
                string HTREdgeReleasedOn = fc["HTREdgeReleasedOn"] != null ? fc["HTREdgeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReleasedOn))
                {
                    objPCL034.HTREdgeReleasedOn = DateTime.ParseExact(HTREdgeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRRollReq = pcl034.HTRRollReq;
                objPCL034.HTRRollReqDate = pcl034.HTRRollReqDate;
                string HTRRollReqDate = fc["HTRRollReqDate"] != null ? fc["HTRRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReqDate))
                {
                    objPCL034.HTRRollReqDate = DateTime.ParseExact(HTRRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRRollDocNo = pcl034.HTRRollDocNo;
                objPCL034.HTRRollReleasedOn = pcl034.HTRRollReleasedOn;
                string HTRRollReleasedOn = fc["HTRRollReleasedOn"] != null ? fc["HTRRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReleasedOn))
                {
                    objPCL034.HTRRollReleasedOn = DateTime.ParseExact(HTRRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRReRollReq = pcl034.HTRReRollReq;
                objPCL034.HTRReRollReqDate = pcl034.HTRReRollReqDate;
                string HTRReRollReqDate = fc["HTRReRollReqDate"] != null ? fc["HTRReRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReqDate))
                {
                    objPCL034.HTRReRollReqDate = DateTime.ParseExact(HTRReRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRReRollDocNo = pcl034.HTRReRollDocNo;
                objPCL034.HTRReRollReleasedOn = pcl034.HTRReRollReleasedOn;
                string HTRReRollReleasedOn = fc["HTRReRollReleasedOn"] != null ? fc["HTRReRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReleasedOn))
                {
                    objPCL034.HTRReRollReleasedOn = DateTime.ParseExact(HTRReRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RefLineSketchReq = pcl034.RefLineSketchReq;
                objPCL034.RefLineSketchReqDate = pcl034.RefLineSketchReqDate;
                string RefLineSketchReqDate = fc["RefLineSketchReqDate"] != null ? fc["RefLineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReqDate))
                {
                    objPCL034.RefLineSketchReqDate = DateTime.ParseExact(RefLineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.RefLineSketchDocNo = pcl034.RefLineSketchDocNo;
                objPCL034.RefLineSketchReleasedOn = pcl034.RefLineSketchReleasedOn;
                string RefLineSketchReleasedOn = fc["RefLineSketchReleasedOn"] != null ? fc["RefLineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReleasedOn))
                {
                    objPCL034.RefLineSketchReleasedOn = DateTime.ParseExact(RefLineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRISRNozzleShellReq = pcl034.HTRISRNozzleShellReq;
                objPCL034.HTRISRNozzleShellReqDate = pcl034.HTRISRNozzleShellReqDate;
                string HTRISRNozzleShellReqDate = fc["HTRISRNozzleShellReqDate"] != null ? fc["HTRISRNozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReqDate))
                {
                    objPCL034.HTRISRNozzleShellReqDate = DateTime.ParseExact(HTRISRNozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRISRNozzleShellDocNo = pcl034.HTRISRNozzleShellDocNo;
                objPCL034.HTRISRNozzleShellReleasedOn = pcl034.HTRISRNozzleShellReleasedOn;
                string HTRISRNozzleShellReleasedOn = fc["HTRISRNozzleShellReleasedOn"] != null ? fc["HTRISRNozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReleasedOn))
                {
                    objPCL034.HTRISRNozzleShellReleasedOn = DateTime.ParseExact(HTRISRNozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRISRNozzleDEndReq = pcl034.HTRISRNozzleDEndReq;
                objPCL034.HTRISRNozzleDEndReqDate = pcl034.HTRISRNozzleDEndReqDate;
                string HTRISRNozzleDEndReqDate = fc["HTRISRNozzleDEndReqDate"] != null ? fc["HTRISRNozzleDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReqDate))
                {
                    objPCL034.HTRISRNozzleDEndReqDate = DateTime.ParseExact(HTRISRNozzleDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRISRNozzleDEndDocNo = pcl034.HTRISRNozzleDEndDocNo;
                objPCL034.HTRISRNozzleDEndReleasedOn = pcl034.HTRISRNozzleDEndReleasedOn;
                string HTRISRNozzleDEndReleasedOn = fc["HTRISRNozzleDEndReleasedOn"] != null ? fc["HTRISRNozzleDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReleasedOn))
                {
                    objPCL034.HTRISRNozzleDEndReleasedOn = DateTime.ParseExact(HTRISRNozzleDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRLongPWHTReq = pcl034.HTRLongPWHTReq;
                objPCL034.HTRLongPWHTReqDate = pcl034.HTRLongPWHTReqDate;
                string HTRLongPWHTReqDate = fc["HTRLongPWHTReqDate"] != null ? fc["HTRLongPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLongPWHTReqDate))
                {
                    objPCL034.HTRLongPWHTReqDate = DateTime.ParseExact(HTRLongPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRLongPWHTDocNo = pcl034.HTRLongPWHTDocNo;
                objPCL034.HTRLongPWHTReleasedOn = pcl034.HTRLongPWHTReleasedOn;
                string HTRLongPWHTReleasedOn = fc["HTRLongPWHTReleasedOn"] != null ? fc["HTRLongPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLongPWHTReleasedOn))
                {
                    objPCL034.HTRLongPWHTReleasedOn = DateTime.ParseExact(HTRLongPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTROverPWHTReq = pcl034.HTROverPWHTReq;
                objPCL034.HTROverPWHTReqDate = pcl034.HTROverPWHTReqDate;
                string HTROverPWHTReqDate = fc["HTROverPWHTReqDate"] != null ? fc["HTROverPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTROverPWHTReqDate))
                {
                    objPCL034.HTROverPWHTReqDate = DateTime.ParseExact(HTROverPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTROverPWHTDocNo = pcl034.HTROverPWHTDocNo;
                objPCL034.HTROverPWHTReleasedOn = pcl034.HTROverPWHTReleasedOn;
                string HTROverPWHTReleasedOn = fc["HTROverPWHTReleasedOn"] != null ? fc["HTROverPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTROverPWHTReleasedOn))
                {
                    objPCL034.HTROverPWHTReleasedOn = DateTime.ParseExact(HTROverPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRPWHTReq = pcl034.HTRPWHTReq;
                objPCL034.HTRPWHTReqDate = pcl034.HTRPWHTReqDate;
                string HTRPWHTReqDate = fc["HTRPWHTReqDate"] != null ? fc["HTRPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReqDate))
                {
                    objPCL034.HTRPWHTReqDate = DateTime.ParseExact(HTRPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRPWHTDocNo = pcl034.HTRPWHTDocNo;
                objPCL034.HTRPWHTReleasedOn = pcl034.HTRPWHTReleasedOn;
                string HTRPWHTReleasedOn = fc["HTRPWHTReleasedOn"] != null ? fc["HTRPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReleasedOn))
                {
                    objPCL034.HTRPWHTReleasedOn = DateTime.ParseExact(HTRPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRLSRReq = pcl034.HTRLSRReq;
                objPCL034.HTRLSRReqDate = pcl034.HTRLSRReqDate;
                string HTRLSRReqDate = fc["HTRLSRReqDate"] != null ? fc["HTRLSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReqDate))
                {
                    objPCL034.HTRLSRReqDate = DateTime.ParseExact(HTRLSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HTRLSRDocNo = pcl034.HTRLSRDocNo;
                objPCL034.HTRLSRReleasedOn = pcl034.HTRLSRReleasedOn;
                string HTRLSRReleasedOn = fc["HTRLSRReleasedOn"] != null ? fc["HTRLSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReleasedOn))
                {
                    objPCL034.HTRLSRReleasedOn = DateTime.ParseExact(HTRLSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HandlingPlanReq = pcl034.HandlingPlanReq;
                objPCL034.HandlingPlanReqDate = pcl034.HandlingPlanReqDate;
                string HandlingPlanReqDate = fc["HandlingPlanReqDate"] != null ? fc["HandlingPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReqDate))
                {
                    objPCL034.HandlingPlanReqDate = DateTime.ParseExact(HandlingPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HandlingPlanDocNo = pcl034.HandlingPlanDocNo;
                objPCL034.HandlingPlanReleasedOn = pcl034.HandlingPlanReleasedOn;
                string HandlingPlanReleasedOn = fc["HandlingPlanReleasedOn"] != null ? fc["HandlingPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReleasedOn))
                {
                    objPCL034.HandlingPlanReleasedOn = DateTime.ParseExact(HandlingPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.LocationTankRotatorReq = pcl034.LocationTankRotatorReq;
                objPCL034.LocationTankRotatorReqDate = pcl034.LocationTankRotatorReqDate;
                string LocationTankRotatorReqDate = fc["LocationTankRotatorReqDate"] != null ? fc["LocationTankRotatorReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReqDate))
                {
                    objPCL034.LocationTankRotatorReqDate = DateTime.ParseExact(LocationTankRotatorReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.LocationTankRotatorDocNo = pcl034.LocationTankRotatorDocNo;
                objPCL034.LocationTankRotatorReleasedOn = pcl034.LocationTankRotatorReleasedOn;
                string LocationTankRotatorReleasedOn = fc["LocationTankRotatorReleasedOn"] != null ? fc["LocationTankRotatorReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReleasedOn))
                {
                    objPCL034.LocationTankRotatorReleasedOn = DateTime.ParseExact(LocationTankRotatorReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HydrotestReq = pcl034.HydrotestReq;
                objPCL034.HydrotestReqDate = pcl034.HydrotestReqDate;
                string HydrotestReqDate = fc["HydrotestReqDate"] != null ? fc["HydrotestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReqDate))
                {
                    objPCL034.HydrotestReqDate = DateTime.ParseExact(HydrotestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HydrotestDocNo = pcl034.HydrotestDocNo;
                objPCL034.HydrotestReleasedOn = pcl034.HydrotestReleasedOn;
                string HydrotestReleasedOn = fc["HydrotestReleasedOn"] != null ? fc["HydrotestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReleasedOn))
                {
                    objPCL034.HydrotestReleasedOn = DateTime.ParseExact(HydrotestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TPDReq = pcl034.TPDReq;
                objPCL034.TPDReqDate = pcl034.TPDReqDate;
                string TPDReqDate = fc["TPDReqDate"] != null ? fc["TPDReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TPDReqDate))
                {
                    objPCL034.TPDReqDate = DateTime.ParseExact(TPDReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TPDDocNo = pcl034.TPDDocNo;
                objPCL034.TPDReleasedOn = pcl034.TPDReleasedOn;
                string TPDReleasedOn = fc["TPDReleasedOn"] != null ? fc["TPDReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TPDReleasedOn))
                {
                    objPCL034.TPDReleasedOn = DateTime.ParseExact(TPDReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.BSPReq = pcl034.BSPReq;
                objPCL034.BSPReqDate = pcl034.BSPReqDate;
                string BSPReqDate = fc["BSPReqDate"] != null ? fc["BSPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BSPReqDate))
                {
                    objPCL034.BSPReqDate = DateTime.ParseExact(BSPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.BSPDocNo = pcl034.BSPDocNo;
                objPCL034.BSPReleasedOn = pcl034.BSPReleasedOn;
                string BSPReleasedOn = fc["BSPReleasedOn"] != null ? fc["BSPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BSPReleasedOn))
                {
                    objPCL034.BSPReleasedOn = DateTime.ParseExact(BSPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.BVIReq = pcl034.BVIReq;
                objPCL034.BVIReqDate = pcl034.BVIReqDate;
                string BVIReqDate = fc["BVIReqDate"] != null ? fc["BVIReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BVIReqDate))
                {
                    objPCL034.BVIReqDate = DateTime.ParseExact(BVIReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.BVIDocNo = pcl034.BVIDocNo;
                objPCL034.BVIReleasedOn = pcl034.BVIReleasedOn;
                string BVIReleasedOn = fc["BVIReleasedOn"] != null ? fc["BVIReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BVIReleasedOn))
                {
                    objPCL034.BVIReleasedOn = DateTime.ParseExact(BVIReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TIPReq = pcl034.TIPReq;
                objPCL034.TIPReqDate = pcl034.TIPReqDate;
                string TIPReqDate = fc["TIPReqDate"] != null ? fc["TIPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TIPReqDate))
                {
                    objPCL034.TIPReqDate = DateTime.ParseExact(TIPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TIPDocNo = pcl034.TIPDocNo;
                objPCL034.TIPReleasedOn = pcl034.TIPReleasedOn;
                string TIPReleasedOn = fc["TIPReleasedOn"] != null ? fc["TIPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TIPReleasedOn))
                {
                    objPCL034.TIPReleasedOn = DateTime.ParseExact(TIPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TEPReq = pcl034.TEPReq;
                objPCL034.TEPReqDate = pcl034.TEPReqDate;
                string TEPReqDate = fc["TEPReqDate"] != null ? fc["TEPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TEPReqDate))
                {
                    objPCL034.TEPReqDate = DateTime.ParseExact(TEPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.TEPDocNo = pcl034.TEPDocNo;
                objPCL034.TEPReleasedOn = pcl034.TEPReleasedOn;
                string TEPReleasedOn = fc["TEPReleasedOn"] != null ? fc["TEPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TEPReleasedOn))
                {
                    objPCL034.TEPReleasedOn = DateTime.ParseExact(TEPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.EDPReq = pcl034.EDPReq;
                objPCL034.EDPReqDate = pcl034.EDPReqDate;
                string EDPReqDate = fc["EDPReqDate"] != null ? fc["EDPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(EDPReqDate))
                {
                    objPCL034.EDPReqDate = DateTime.ParseExact(EDPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.EDPDocNo = pcl034.EDPDocNo;
                objPCL034.EDPReleasedOn = pcl034.EDPReleasedOn;
                string EDPReleasedOn = fc["EDPReleasedOn"] != null ? fc["EDPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(EDPReleasedOn))
                {
                    objPCL034.EDPReleasedOn = DateTime.ParseExact(EDPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HFPReq = pcl034.HFPReq;
                objPCL034.HFPReqDate = pcl034.HFPReqDate;
                string HFPReqDate = fc["HFPReqDate"] != null ? fc["HFPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HFPReqDate))
                {
                    objPCL034.HFPReqDate = DateTime.ParseExact(HFPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.HFPDocNo = pcl034.HFPDocNo;
                objPCL034.HFPReleasedOn = pcl034.HFPReleasedOn;
                string HFPReleasedOn = fc["HFPReleasedOn"] != null ? fc["HFPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HFPReleasedOn))
                {
                    objPCL034.HFPReleasedOn = DateTime.ParseExact(HFPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.STRReq = pcl034.STRReq;
                objPCL034.STRReqDate = pcl034.STRReqDate;
                string STRReqDate = fc["STRReqDate"] != null ? fc["STRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(STRReqDate))
                {
                    objPCL034.STRReqDate = DateTime.ParseExact(STRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.STRDocNo = pcl034.STRDocNo;
                objPCL034.STRReleasedOn = pcl034.STRReleasedOn;
                string STRReleasedOn = fc["STRReleasedOn"] != null ? fc["STRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(STRReleasedOn))
                {
                    objPCL034.STRReleasedOn = DateTime.ParseExact(STRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.YRingReq = pcl034.YRingReq;
                objPCL034.YRingReqDate = pcl034.YRingReqDate;
                string YRingReqDate = fc["YRingReqDate"] != null ? fc["YRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YRingReqDate))
                {
                    objPCL034.YRingReqDate = DateTime.ParseExact(YRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL034.YRingDocNo = pcl034.YRingDocNo;
                objPCL034.YRingReleasedOn = pcl034.YRingReleasedOn;
                string YRingReleasedOn = fc["YRingReleasedOn"] != null ? fc["YRingReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YRingReleasedOn))
                {
                    objPCL034.YRingReleasedOn = DateTime.ParseExact(YRingReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                if (pcl034.LineId > 0)
                {
                    objPCL034.EditedBy = objClsLoginInfo.UserName;
                    objPCL034.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL034.CreatedBy = objClsLoginInfo.UserName;
                    objPCL034.CreatedOn = DateTime.Now;
                    db.PCL034.Add(objPCL034);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL034.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEODim(PCL035 pcl035, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl035.HeaderId).FirstOrDefault();
                PCL035 objPCL035 = new PCL035();
                if (pcl035.LineId > 0)
                {
                    objPCL035 = db.PCL035.Where(x => x.LineId == pcl035.LineId).FirstOrDefault();
                }
                objPCL035.HeaderId = objPCL001.HeaderId;
                objPCL035.Project = objPCL001.Project;
                objPCL035.Document = objPCL001.Document;

                objPCL035.RollDataReq = pcl035.RollDataReq;
                objPCL035.RollDataReqDate = pcl035.RollDataReqDate;
                string RollDataReqDate = fc["RollDataReqDate"] != null ? fc["RollDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReqDate))
                {
                    objPCL035.RollDataReqDate = DateTime.ParseExact(RollDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL035.RollDataDocNo = pcl035.RollDataDocNo;
                objPCL035.RollDataReleasedOn = pcl035.RollDataReleasedOn;
                string RollDataReleasedOn = fc["RollDataReleasedOn"] != null ? fc["RollDataReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReleasedOn))
                {
                    objPCL035.RollDataReleasedOn = DateTime.ParseExact(RollDataReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.ShellLongReq = pcl035.ShellLongReq;
                objPCL035.ShellLongReqDate = pcl035.ShellLongReqDate;
                string ShellLongReqDate = fc["ShellLongReqDate"] != null ? fc["ShellLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReqDate))
                {
                    objPCL035.ShellLongReqDate = DateTime.ParseExact(ShellLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.ShellLongDocNo = pcl035.ShellLongDocNo;
                objPCL035.ShellLongReleasedOn = pcl035.ShellLongReleasedOn;
                string ShellLongReleasedOn = fc["ShellLongReleasedOn"] != null ? fc["ShellLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReleasedOn))
                {
                    objPCL035.ShellLongReleasedOn = DateTime.ParseExact(ShellLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.YDataReq = pcl035.YDataReq;
                objPCL035.YDataReqDate = pcl035.YDataReqDate;
                string YDataReqDate = fc["YDataReqDate"] != null ? fc["YDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YDataReqDate))
                {
                    objPCL035.YDataReqDate = DateTime.ParseExact(YDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.YDataDocNo = pcl035.YDataDocNo;
                objPCL035.YDataReleasedOn = pcl035.YDataReleasedOn;
                string YDataReleasedOn = fc["YDataReleasedOn"] != null ? fc["YDataReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YDataReleasedOn))
                {
                    objPCL035.YDataReleasedOn = DateTime.ParseExact(YDataReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.YCFReq = pcl035.YCFReq;
                objPCL035.YCFReqDate = pcl035.YCFReqDate;
                string YCFReqDate = fc["YCFReqDate"] != null ? fc["YCFReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YCFReqDate))
                {
                    objPCL035.YCFReqDate = DateTime.ParseExact(YCFReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.YCFDocNo = pcl035.YCFDocNo;
                objPCL035.YCFReleasedOn = pcl035.YCFReleasedOn;
                string YCFReleasedOn = fc["YCFReleasedOn"] != null ? fc["YCFReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(YCFReleasedOn))
                {
                    objPCL035.YCFReleasedOn = DateTime.ParseExact(YCFReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.ChangeOvalityExtReq = pcl035.ChangeOvalityExtReq;
                objPCL035.ChangeOvalityExtReqDate = pcl035.ChangeOvalityExtReqDate;
                string ChangeOvalityExtReqDate = fc["ChangeOvalityExtReqDate"] != null ? fc["ChangeOvalityExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReqDate))
                {
                    objPCL035.ChangeOvalityExtReqDate = DateTime.ParseExact(ChangeOvalityExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.ChangeOvalityExtDocNo = pcl035.ChangeOvalityExtDocNo;
                objPCL035.ChangeOvalityExtReleasedOn = pcl035.ChangeOvalityExtReleasedOn;
                string ChangeOvalityExtReleasedOn = fc["ChangeOvalityExtReleasedOn"] != null ? fc["ChangeOvalityExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReleasedOn))
                {
                    objPCL035.ChangeOvalityExtReleasedOn = DateTime.ParseExact(ChangeOvalityExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TTSReq = pcl035.TTSReq;
                objPCL035.TTSReqDate = pcl035.TTSReqDate;
                string TTSReqDate = fc["TTSReqDate"] != null ? fc["TTSReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TTSReqDate))
                {
                    objPCL035.TTSReqDate = DateTime.ParseExact(TTSReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TTSDocNo = pcl035.TTSDocNo;
                objPCL035.TTSReleasedOn = pcl035.TTSReleasedOn;
                string TTSReleasedOn = fc["TTSReleasedOn"] != null ? fc["TTSReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TTSReleasedOn))
                {
                    objPCL035.TTSReleasedOn = DateTime.ParseExact(TTSReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.HPNReq = pcl035.HPNReq;
                objPCL035.HPNReqDate = pcl035.HPNReqDate;
                string HPNReqDate = fc["HPNReqDate"] != null ? fc["HPNReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HPNReqDate))
                {
                    objPCL035.HPNReqDate = DateTime.ParseExact(HPNReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.HPNDocNo = pcl035.HPNDocNo;
                objPCL035.HPNReleasedOn = pcl035.HPNReleasedOn;
                string HPNReleasedOn = fc["HPNReleasedOn"] != null ? fc["HPNReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HPNReleasedOn))
                {
                    objPCL035.HPNReleasedOn = DateTime.ParseExact(HPNReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.ChangeOvalityISRReq = pcl035.ChangeOvalityISRReq;
                objPCL035.ChangeOvalityISRReqDate = pcl035.ChangeOvalityISRReqDate;
                string ChangeOvalityISRReqDate = fc["ChangeOvalityISRReqDate"] != null ? fc["ChangeOvalityISRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReqDate))
                {
                    objPCL035.ChangeOvalityISRReqDate = DateTime.ParseExact(ChangeOvalityISRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.ChangeOvalityISRDocNo = pcl035.ChangeOvalityISRDocNo;
                objPCL035.ChangeOvalityISRReleasedOn = pcl035.ChangeOvalityISRReleasedOn;
                string ChangeOvalityISRReleasedOn = fc["ChangeOvalityISRReleasedOn"] != null ? fc["ChangeOvalityISRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReleasedOn))
                {
                    objPCL035.ChangeOvalityISRReleasedOn = DateTime.ParseExact(ChangeOvalityISRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.DEendLongSeamReq = pcl035.DEendLongSeamReq;
                objPCL035.DEendLongSeamReqDate = pcl035.DEendLongSeamReqDate;
                string DEendLongSeamReqDate = fc["DEendLongSeamReqDate"] != null ? fc["DEendLongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReqDate))
                {
                    objPCL035.DEendLongSeamReqDate = DateTime.ParseExact(DEendLongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.DEendLongSeamDocNo = pcl035.DEendLongSeamDocNo;
                objPCL035.DEendLongSeamReleasedOn = pcl035.DEendLongSeamReleasedOn;
                string DEendLongSeamReleasedOn = fc["DEendLongSeamReleasedOn"] != null ? fc["DEendLongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReleasedOn))
                {
                    objPCL035.DEendLongSeamReleasedOn = DateTime.ParseExact(DEendLongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TLongSeamReq = pcl035.TLongSeamReq;
                objPCL035.TLongSeamReqDate = pcl035.TLongSeamReqDate;
                string TLongSeamReqDate = fc["TLongSeamReqDate"] != null ? fc["TLongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TLongSeamReqDate))
                {
                    objPCL035.TLongSeamReqDate = DateTime.ParseExact(TLongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TLongSeamDocNo = pcl035.TLongSeamDocNo;
                objPCL035.TLongSeamReleasedOn = pcl035.TLongSeamReleasedOn;
                string TLongSeamReleasedOn = fc["TLongSeamReleasedOn"] != null ? fc["TLongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TLongSeamReleasedOn))
                {
                    objPCL035.TLongSeamReleasedOn = DateTime.ParseExact(TLongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TCircSeamReq = pcl035.TCircSeamReq;
                objPCL035.TCircSeamReqDate = pcl035.TCircSeamReqDate;
                string TCircSeamReqDate = fc["TCircSeamReqDate"] != null ? fc["TCircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TCircSeamReqDate))
                {
                    objPCL035.TCircSeamReqDate = DateTime.ParseExact(TCircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TCircSeamDocNo = pcl035.TCircSeamDocNo;
                objPCL035.TCircSeamReleasedOn = pcl035.TCircSeamReleasedOn;
                string TCircSeamReleasedOn = fc["TCircSeamReleasedOn"] != null ? fc["TCircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TCircSeamReleasedOn))
                {
                    objPCL035.TCircSeamReleasedOn = DateTime.ParseExact(TCircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TPWHTSeamReq = pcl035.TPWHTSeamReq;
                objPCL035.TPWHTSeamReqDate = pcl035.TPWHTSeamReqDate;
                string TPWHTSeamReqDate = fc["TPWHTSeamReqDate"] != null ? fc["TPWHTSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TPWHTSeamReqDate))
                {
                    objPCL035.TPWHTSeamReqDate = DateTime.ParseExact(TPWHTSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TPWHTSeamDocNo = pcl035.TPWHTSeamDocNo;
                objPCL035.TPWHTSeamReleasedOn = pcl035.TPWHTSeamReleasedOn;
                string TPWHTSeamReleasedOn = fc["TPWHTSeamReleasedOn"] != null ? fc["TPWHTSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TPWHTSeamReleasedOn))
                {
                    objPCL035.TPWHTSeamReleasedOn = DateTime.ParseExact(TPWHTSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TempratureReq = pcl035.TempratureReq;
                objPCL035.TempratureReqDate = pcl035.TempratureReqDate;
                string TempratureReqDate = fc["TempratureReqDate"] != null ? fc["TempratureReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempratureReqDate))
                {
                    objPCL035.TempratureReqDate = DateTime.ParseExact(TempratureReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL035.TempratureDocNo = pcl035.TempratureDocNo;
                objPCL035.TempratureReleasedOn = pcl035.TempratureReleasedOn;
                string TempratureReleasedOn = fc["TempratureReleasedOn"] != null ? fc["TempratureReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempratureReleasedOn))
                {
                    objPCL035.TempratureReleasedOn = DateTime.ParseExact(TempratureReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }


                if (pcl035.LineId > 0)
                {
                    objPCL035.EditedBy = objClsLoginInfo.UserName;
                    objPCL035.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL035.CreatedBy = objClsLoginInfo.UserName;
                    objPCL035.CreatedOn = DateTime.Now;
                    db.PCL035.Add(objPCL035);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL035.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Column

        [HttpPost]
        public ActionResult SaveColumnConstructor(PCL024 pcl024, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl024.HeaderId).FirstOrDefault();
                PCL024 objPCL024 = new PCL024();
                if (pcl024.LineId > 0)
                {
                    objPCL024 = db.PCL024.Where(x => x.LineId == pcl024.LineId).FirstOrDefault();
                }
                objPCL024.HeaderId = objPCL001.HeaderId;
                objPCL024.Project = objPCL001.Project;
                objPCL024.Document = objPCL001.Document;

                objPCL024.ReactorID = pcl024.ReactorID;
                objPCL024.ReactorThk1 = pcl024.ReactorThk1;
                objPCL024.ReactorTLTL = pcl024.ReactorTLTL;
                objPCL024.ReactorLg = pcl024.ReactorLg;
                objPCL024.ReactorFabWt = pcl024.ReactorFabWt;
                objPCL024.ReactorHydroWt = pcl024.ReactorHydroWt;
                objPCL024.ReactorMaxShellWd = pcl024.ReactorMaxShellWd;
                objPCL024.ReactoreDesigned = pcl024.ReactoreDesigned;
                objPCL024.ShellConst1Seg = pcl024.ShellConst1Seg;
                objPCL024.ShellConst2Seg = pcl024.ShellConst2Seg;
                objPCL024.ShellConst3Seg = pcl024.ShellConst3Seg;
                objPCL024.ShellConst4Seg = pcl024.ShellConst4Seg;
                objPCL024.TypeRollCold = pcl024.TypeRollCold;
                objPCL024.TypeRollWarm = pcl024.TypeRollWarm;
                objPCL024.TypeRollHot = pcl024.TypeRollHot;
                objPCL024.GasCuttingPreHeat = pcl024.GasCuttingPreHeat;
                objPCL024.GasCuttingPreHeatNR = pcl024.GasCuttingPreHeatNR;
                objPCL024.CSeamWEPPlate = pcl024.CSeamWEPPlate;
                objPCL024.CSeamWEPAfter = pcl024.CSeamWEPAfter;
                objPCL024.PTCRequiredforLS = pcl024.PTCRequiredforLS;
                objPCL024.PTCRequiredforCS = pcl024.PTCRequiredforCS;
                objPCL024.PTCRequiredforDend = pcl024.PTCRequiredforDend;
                objPCL024.PTCRequiredforDendShell = pcl024.PTCRequiredforDendShell;
                objPCL024.PTCRequiredforSpool = pcl024.PTCRequiredforSpool;
                objPCL024.ISRRequiredforLS = pcl024.ISRRequiredforLS;
                objPCL024.ISRRequiredforCS = pcl024.ISRRequiredforCS;
                objPCL024.ISRRequiredforNozzle = pcl024.ISRRequiredforNozzle;
                objPCL024.ISRRequiredforCone = pcl024.ISRRequiredforCone;
                objPCL024.ISRRequiredforHead = pcl024.ISRRequiredforHead;
                objPCL024.PTMTAfterWelding = pcl024.PTMTAfterWelding;
                objPCL024.PTMTAfterOverlay = pcl024.PTMTAfterOverlay;
                objPCL024.PTMTAfterPWHT = pcl024.PTMTAfterPWHT;
                objPCL024.PTMTAfterHydro = pcl024.PTMTAfterHydro;
                objPCL024.RTAfterWelding = pcl024.RTAfterWelding;
                objPCL024.RTAfterOverlay = pcl024.RTAfterOverlay;
                objPCL024.RTAfterPWHT = pcl024.RTAfterPWHT;
                objPCL024.RTAfterHydro = pcl024.RTAfterHydro;
                objPCL024.UTAfterRecordable = pcl024.UTAfterRecordable;
                objPCL024.UTAfterConventional = pcl024.UTAfterConventional;
                objPCL024.UTAfterWelding = pcl024.UTAfterWelding;
                objPCL024.UTAfterOverlay = pcl024.UTAfterOverlay;
                objPCL024.UTAfterPWHT = pcl024.UTAfterPWHT;
                objPCL024.UTAfterHydro = pcl024.UTAfterHydro;
                objPCL024.FerAfterWelding = pcl024.FerAfterWelding;
                objPCL024.FerAfterOverlay = pcl024.FerAfterOverlay;
                objPCL024.FerAfterPWHT = pcl024.FerAfterPWHT;
                objPCL024.FerAfterHydro = pcl024.FerAfterHydro;
                objPCL024.SOTESSCSingle = pcl024.SOTESSCSingle;
                objPCL024.SOTESSCDouble = pcl024.SOTESSCDouble;
                objPCL024.SOTESSCClade = pcl024.SOTESSCClade;
                objPCL024.SOTESSCOlayNR = pcl024.SOTESSCOlayNR;
                objPCL024.DCPetalWCrown = pcl024.DCPetalWCrown;
                objPCL024.DC4PetalWoCrown = pcl024.DC4PetalWoCrown;
                objPCL024.DCSinglePeice = pcl024.DCSinglePeice;
                objPCL024.DCTwoHalves = pcl024.DCTwoHalves;
                objPCL024.DRCFormedPetals = pcl024.DRCFormedPetals;
                objPCL024.DRCFormedSetup = pcl024.DRCFormedSetup;
                objPCL024.DRCFormedWelded = pcl024.DRCFormedWelded;
                objPCL024.DRCFormedOutside = pcl024.DRCFormedOutside;
                objPCL024.DOTESSCSingle = pcl024.DOTESSCSingle;
                objPCL024.DOTESSCDouble = pcl024.DOTESSCDouble;
                objPCL024.DOTESSCClade = pcl024.DOTESSCClade;
                objPCL024.DOTESSCOlayNR = pcl024.DOTESSCOlayNR;
                objPCL024.SVJYring = pcl024.SVJYring;
                objPCL024.SVJBuildup = pcl024.SVJBuildup;
                objPCL024.SVJLapJoint = pcl024.SVJLapJoint;
                objPCL024.YRTForgedShell = pcl024.YRTForgedShell;
                objPCL024.YRTForgedSeg = pcl024.YRTForgedSeg;
                objPCL024.YRTReadimade = pcl024.YRTReadimade;
                objPCL024.YRTPlate = pcl024.YRTPlate;
                objPCL024.YRTNR = pcl024.YRTNR;
                objPCL024.TSRWeldBuildup = pcl024.TSRWeldBuildup;
                objPCL024.TSRForgedShell = pcl024.TSRForgedShell;
                objPCL024.TSRPlateWOlay = pcl024.TSRPlateWOlay;
                objPCL024.TSRPlate = pcl024.TSRPlate;
                objPCL024.TSRSSPlate = pcl024.TSRSSPlate;
                objPCL024.TSRNoofGussets = pcl024.TSRNoofGussets;
                objPCL024.TSRNR = pcl024.TSRNR;
                objPCL024.NFFRaised = pcl024.NFFRaised;
                objPCL024.NFFGroove = pcl024.NFFGroove;
                objPCL024.NFFOther = pcl024.NFFOther;
                objPCL024.NGFBfPWHT = pcl024.NGFBfPWHT;
                objPCL024.NGFAfPWHT = pcl024.NGFAfPWHT;
                objPCL024.NGFNR = pcl024.NGFNR;
                objPCL024.SpoolMatSS = pcl024.SpoolMatSS;
                objPCL024.SpoolMatLAS = pcl024.SpoolMatLAS;
                objPCL024.SpoolMatCS = pcl024.SpoolMatCS;
                objPCL024.SpoolMatOth = pcl024.SpoolMatOth;
                objPCL024.SpoolMatOthVal = pcl024.SpoolMatOthVal;
                objPCL024.JSInConnel = pcl024.JSInConnel;
                objPCL024.JSBiMetalic = pcl024.JSBiMetalic;
                objPCL024.JSCladRestoration = pcl024.JSCladRestoration;
                objPCL024.SSWBfPWHT = pcl024.SSWBfPWHT;
                objPCL024.SSWAfPWHT = pcl024.SSWAfPWHT;
                objPCL024.SSWBoth = pcl024.SSWBoth;
                objPCL024.VessleSSkirt = pcl024.VessleSSkirt;
                objPCL024.VessleSSupp = pcl024.VessleSSupp;
                objPCL024.VessleSLegs = pcl024.VessleSLegs;
                objPCL024.VessleSSaddle = pcl024.VessleSSaddle;
                objPCL024.SkirtMadein1 = pcl024.SkirtMadein1;
                objPCL024.SkirtMadein2 = pcl024.SkirtMadein2;
                objPCL024.FPNWeldNuts = pcl024.FPNWeldNuts;
                objPCL024.FPNLeaveRow = pcl024.FPNLeaveRow;
                objPCL024.PWHTSinglePiece = pcl024.PWHTSinglePiece;
                objPCL024.PWHTLSRJoint = pcl024.PWHTLSRJoint;
                objPCL024.PWHTNoSec = pcl024.PWHTNoSec;
                objPCL024.PTCMTCptc = pcl024.PTCMTCptc;
                objPCL024.PTCMTCmtc = pcl024.PTCMTCmtc;
                objPCL024.PTCMTCptcSim = pcl024.PTCMTCptcSim;
                objPCL024.PTCMTCmtcSim = pcl024.PTCMTCmtcSim;
                objPCL024.SaddlesTrans = pcl024.SaddlesTrans;
                objPCL024.SaddlesAddit = pcl024.SaddlesAddit;
                objPCL024.PWHTinFurnacePFS = pcl024.PWHTinFurnacePFS;
                objPCL024.PWHTinFurnaceHFS1 = pcl024.PWHTinFurnaceHFS1;
                objPCL024.PWHTinFurnaceLEMF = pcl024.PWHTinFurnaceLEMF;
                objPCL024.PWHTinFurnaceName = pcl024.PWHTinFurnaceName;
                objPCL024.SectionWtMore230 = pcl024.SectionWtMore230;
                objPCL024.SectionWt200To230 = pcl024.SectionWt200To230;
                objPCL024.SectionWt150To200 = pcl024.SectionWt150To200;
                objPCL024.SectionWtLess150 = pcl024.SectionWtLess150;
                objPCL024.SectionWtSingle = pcl024.SectionWtSingle;
                objPCL024.SectionWt2Sec = pcl024.SectionWt2Sec;
                objPCL024.SectionWtNoSec = pcl024.SectionWtNoSec;
                objPCL024.ARMRequired = pcl024.ARMRequired;
                objPCL024.ARMRT = pcl024.ARMRT;
                objPCL024.ARMPWHT = pcl024.ARMPWHT;
                objPCL024.ARMSB = pcl024.ARMSB;
                objPCL024.CLRHLess30ppm = pcl024.CLRHLess30ppm;
                objPCL024.CLRHMore30ppm = pcl024.CLRHMore30ppm;
                objPCL024.CLRHppm = pcl024.CLRHppm;
                objPCL024.CLRHOther = pcl024.CLRHOther;
                objPCL024.TemplateForReq = pcl024.TemplateForReq;
                objPCL024.TemplateForNReq = pcl024.TemplateForNReq;
                objPCL024.N2FillReq = pcl024.N2FillReq;
                objPCL024.N2FillNotReq = pcl024.N2FillNotReq;
                objPCL024.N2FillOther = pcl024.N2FillOther;
                objPCL024.N2FillOtherVal = pcl024.N2FillOtherVal;
                objPCL024.ESTPlate = pcl024.ESTPlate;
                objPCL024.ESTShell = pcl024.ESTShell;
                objPCL024.ESTSection = pcl024.ESTSection;
                objPCL024.ESTEquip = pcl024.ESTEquip;
                objPCL024.ISTPlate = pcl024.ISTPlate;
                objPCL024.ISTShell = pcl024.ISTShell;
                objPCL024.ISTSection = pcl024.ISTSection;
                objPCL024.ISTEquip = pcl024.ISTEquip;
                objPCL024.ISTAfBfPWHT = pcl024.ISTAfBfPWHT;
                objPCL024.ISTAfHydro = pcl024.ISTAfHydro;
                objPCL024.CDNSS = pcl024.CDNSS;
                objPCL024.CDNLAS = pcl024.CDNLAS;
                objPCL024.CDNCS = pcl024.CDNCS;
                objPCL024.CDNNotReq = pcl024.CDNNotReq;
                objPCL024.MaxSBLess1_38 = pcl024.MaxSBLess1_38;
                objPCL024.MaxSBLess1_78 = pcl024.MaxSBLess1_78;
                objPCL024.MaxSBLess2_14 = pcl024.MaxSBLess2_14;
                objPCL024.MaxSBLess4 = pcl024.MaxSBLess4;
                objPCL024.MaxSBMore4 = pcl024.MaxSBMore4;

                objPCL024.ISRNA = pcl024.ISRNA;
                objPCL024.RTAfterNA = pcl024.RTAfterNA;
                objPCL024.TypeRollWarmVal = pcl024.TypeRollWarmVal;
                objPCL024.TypeRollHotmin = pcl024.TypeRollHotmin;
                objPCL024.TypeRollHotmin = pcl024.TypeRollHotmin;
                objPCL024.TypeRollHotVal = pcl024.TypeRollHotVal;
                objPCL024.TSRNoofGussetsVal = pcl024.TSRNoofGussetsVal;
                objPCL024.SaddlesTransVal = pcl024.SaddlesTransVal;
                objPCL024.SaddlesAdditVal = pcl024.SaddlesAdditVal;
                objPCL024.TOFDAfterWelding = pcl024.TOFDAfterWelding;
                objPCL024.TOFDAfterOverlay = pcl024.TOFDAfterOverlay;
                objPCL024.TOFDAfterPWHT = pcl024.TOFDAfterPWHT;
                objPCL024.TOFDAfterHydro = pcl024.TOFDAfterHydro;
                objPCL024.TOFDNA = pcl024.TOFDNA;

                if (pcl024.LineId > 0)
                {
                    objPCL024.EditedBy = objClsLoginInfo.UserName;
                    objPCL024.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL024.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL024.CreatedBy = objClsLoginInfo.UserName;
                    objPCL024.CreatedOn = DateTime.Now;
                    db.PCL024.Add(objPCL024);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL024.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveColumnAllowance(PCL025 pcl025, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl025.HeaderId).FirstOrDefault();
                PCL025 objPCL025 = new PCL025();
                if (pcl025.LineId > 0)
                {
                    objPCL025 = db.PCL025.Where(x => x.LineId == pcl025.LineId).FirstOrDefault();
                }
                objPCL025.HeaderId = objPCL001.HeaderId;
                objPCL025.Project = objPCL001.Project;
                objPCL025.Document = objPCL001.Document;

                objPCL025.ShellAllwEB = pcl025.ShellAllwEB;
                objPCL025.ShrinkageCirc = pcl025.ShrinkageCirc;
                objPCL025.ShringageWidth = pcl025.ShringageWidth;
                objPCL025.SeamCirc = pcl025.SeamCirc;
                objPCL025.Seamlong = pcl025.Seamlong;
                objPCL025.Mcinglong = pcl025.Mcinglong;
                objPCL025.Mcingcirc = pcl025.Mcingcirc;
                objPCL025.NegAllow = pcl025.NegAllow;
                objPCL025.AllowTSR = pcl025.AllowTSR;
                objPCL025.TOvality = pcl025.TOvality;
                objPCL025.TOutRoundness = pcl025.TOutRoundness;
                objPCL025.TCircum = pcl025.TCircum;
                objPCL025.DEndMcing = pcl025.DEndMcing;
                objPCL025.DEndSeam = pcl025.DEndSeam;
                objPCL025.DEndCirc = pcl025.DEndCirc;
                objPCL025.DEndESSC = pcl025.DEndESSC;
                objPCL025.SkirtTol = pcl025.SkirtTol;
                objPCL025.CTRPeak = pcl025.CTRPeak;
                objPCL025.CTRMaxOffset = pcl025.CTRMaxOffset;
                objPCL025.CTROutRound = pcl025.CTROutRound;
                objPCL025.CTRTDE = pcl025.CTRTDE;
                objPCL025.CTRDump = pcl025.CTRDump;
                objPCL025.CTRMaxAllow = pcl025.CTRMaxAllow;
                objPCL025.ShellAllCone = pcl025.ShellAllCone;

                if (pcl025.LineId > 0)
                {
                    objPCL025.EditedBy = objClsLoginInfo.UserName;
                    objPCL025.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL025.CreatedBy = objClsLoginInfo.UserName;
                    objPCL025.CreatedOn = DateTime.Now;
                    db.PCL025.Add(objPCL025);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL025.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveColumnScopeOfWork(PCL026 pcl026, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl026.HeaderId).FirstOrDefault();
                PCL026 objPCL026 = new PCL026();
                if (pcl026.LineId > 0)
                {
                    objPCL026 = db.PCL026.Where(x => x.LineId == pcl026.LineId).FirstOrDefault();
                }
                objPCL026.HeaderId = objPCL001.HeaderId;
                objPCL026.Project = objPCL001.Project;
                objPCL026.Document = objPCL001.Document;

                objPCL026.EquipmentNotReq = pcl026.EquipmentNotReq;
                objPCL026.EquipmentShop = pcl026.EquipmentShop;
                objPCL026.EquipmentMC = pcl026.EquipmentMC;
                objPCL026.EquipmentLEMF = pcl026.EquipmentLEMF;
                objPCL026.EquipmentRNW = pcl026.EquipmentRNW;
                objPCL026.EquipmentOutside = pcl026.EquipmentOutside;
                objPCL026.DEndFormingNotReq = pcl026.DEndFormingNotReq;
                objPCL026.DEndFormingShop = pcl026.DEndFormingShop;
                objPCL026.DEndFormingMC = pcl026.DEndFormingMC;
                objPCL026.DEndFormingLEMF = pcl026.DEndFormingLEMF;
                objPCL026.DEndFormingRNW = pcl026.DEndFormingRNW;
                objPCL026.DEndFormingOutside = pcl026.DEndFormingOutside;
                objPCL026.DEndSetupNotReq = pcl026.DEndSetupNotReq;
                objPCL026.DEndSetupShop = pcl026.DEndSetupShop;
                objPCL026.DEndSetupMC = pcl026.DEndSetupMC;
                objPCL026.DEndSetupLEMF = pcl026.DEndSetupLEMF;
                objPCL026.DEndSetupRNW = pcl026.DEndSetupRNW;
                objPCL026.DEndSetupOutside = pcl026.DEndSetupOutside;
                objPCL026.DEndWeldingNotReq = pcl026.DEndWeldingNotReq;
                objPCL026.DEndWeldingShop = pcl026.DEndWeldingShop;
                objPCL026.DEndWeldingMC = pcl026.DEndWeldingMC;
                objPCL026.DEndWeldingLEMF = pcl026.DEndWeldingLEMF;
                objPCL026.DEndWeldingRNW = pcl026.DEndWeldingRNW;
                objPCL026.DEndWeldingOutside = pcl026.DEndWeldingOutside;
                objPCL026.SkirtNotReq = pcl026.SkirtNotReq;
                objPCL026.SkirtShop = pcl026.SkirtShop;
                objPCL026.SkirtMC = pcl026.SkirtMC;
                objPCL026.SkirtLEMF = pcl026.SkirtLEMF;
                objPCL026.SkirtRNW = pcl026.SkirtRNW;
                objPCL026.SkirtOutside = pcl026.SkirtOutside;
                objPCL026.YRingMachiningNotReq = pcl026.YRingMachiningNotReq;
                objPCL026.YRingMachiningShop = pcl026.YRingMachiningShop;
                objPCL026.YRingMachiningMC = pcl026.YRingMachiningMC;
                objPCL026.YRingMachiningLEMF = pcl026.YRingMachiningLEMF;
                objPCL026.YRingMachiningRNW = pcl026.YRingMachiningRNW;
                objPCL026.YRingMachiningOutside = pcl026.YRingMachiningOutside;
                objPCL026.NozMachiningNotReq = pcl026.NozMachiningNotReq;
                objPCL026.NozMachiningShop = pcl026.NozMachiningShop;
                objPCL026.NozMachiningMC = pcl026.NozMachiningMC;
                objPCL026.NozMachiningLEMF = pcl026.NozMachiningLEMF;
                objPCL026.NozMachiningRNW = pcl026.NozMachiningRNW;
                objPCL026.NozMachiningOutside = pcl026.NozMachiningOutside;
                objPCL026.NozOverlayNotReq = pcl026.NozOverlayNotReq;
                objPCL026.NozOverlayMC = pcl026.NozOverlayMC;
                objPCL026.NozOverlayShop = pcl026.NozOverlayShop;
                objPCL026.NozOverlayLEMF = pcl026.NozOverlayLEMF;
                objPCL026.NozOverlayRNW = pcl026.NozOverlayRNW;
                objPCL026.NozOverlayOutside = pcl026.NozOverlayOutside;
                objPCL026.ManOverlayNotReq = pcl026.ManOverlayNotReq;
                objPCL026.ManOverlayShop = pcl026.ManOverlayShop;
                objPCL026.ManOverlayMC = pcl026.ManOverlayMC;
                objPCL026.ManOverlayLEMF = pcl026.ManOverlayLEMF;
                objPCL026.ManOverlayRNW = pcl026.ManOverlayRNW;
                objPCL026.ManOverlayOutside = pcl026.ManOverlayOutside;
                objPCL026.NozFinMachiningNotReq = pcl026.NozFinMachiningNotReq;
                objPCL026.NozFinMachiningShop = pcl026.NozFinMachiningShop;
                objPCL026.NozFinMachiningMC = pcl026.NozFinMachiningMC;
                objPCL026.NozFinMachiningLEMF = pcl026.NozFinMachiningLEMF;
                objPCL026.NozFinMachiningRNW = pcl026.NozFinMachiningRNW;
                objPCL026.NozFinMachiningOutside = pcl026.NozFinMachiningOutside;
                objPCL026.NozWEPNotReq = pcl026.NozWEPNotReq;
                objPCL026.NozWEPShop = pcl026.NozWEPShop;
                objPCL026.NozWEPMC = pcl026.NozWEPMC;
                objPCL026.NozWEPLEMF = pcl026.NozWEPLEMF;
                objPCL026.NozWEPRNW = pcl026.NozWEPRNW;
                objPCL026.NozWEPOutside = pcl026.NozWEPOutside;
                objPCL026.LongWEPNotReq = pcl026.LongWEPNotReq;
                objPCL026.LongWEPShop = pcl026.LongWEPShop;
                objPCL026.LongWEPMC = pcl026.LongWEPMC;
                objPCL026.LongWEPLEMF = pcl026.LongWEPLEMF;
                objPCL026.LongWEPRNW = pcl026.LongWEPRNW;
                objPCL026.LongWEPOutside = pcl026.LongWEPOutside;
                objPCL026.CircWEPNotReq = pcl026.CircWEPNotReq;
                objPCL026.CircWEPShop = pcl026.CircWEPShop;
                objPCL026.CircWEPMC = pcl026.CircWEPMC;
                objPCL026.CircWEPLEMF = pcl026.CircWEPLEMF;
                objPCL026.CircWEPRNW = pcl026.CircWEPRNW;
                objPCL026.CircWEPOutside = pcl026.CircWEPOutside;

                objPCL026.LiftMachiningShop = pcl026.LiftMachiningShop;
                objPCL026.LiftMachiningMC = pcl026.LiftMachiningMC;
                objPCL026.LiftMachiningLEMF = pcl026.LiftMachiningLEMF;
                objPCL026.LiftMachiningRNW = pcl026.LiftMachiningRNW;
                objPCL026.LiftMachiningOutside = pcl026.LiftMachiningOutside;
                objPCL026.LiftFabricationNotReq = pcl026.LiftFabricationNotReq;
                objPCL026.LiftFabricationShop = pcl026.LiftFabricationShop;
                objPCL026.LiftFabricationMC = pcl026.LiftFabricationMC;
                objPCL026.LiftFabricationLEMF = pcl026.LiftFabricationLEMF;
                objPCL026.LiftFabricationRNW = pcl026.LiftFabricationRNW;
                objPCL026.LiftFabricationOutside = pcl026.LiftFabricationOutside;
                objPCL026.WeldExFabNotReq = pcl026.WeldExFabNotReq;
                objPCL026.WeldExFabShop = pcl026.WeldExFabShop;
                objPCL026.WeldExFabMC = pcl026.WeldExFabMC;
                objPCL026.WeldExFabLEMF = pcl026.WeldExFabLEMF;
                objPCL026.WeldExFabRNW = pcl026.WeldExFabRNW;
                objPCL026.WeldExFabOutside = pcl026.WeldExFabOutside;
                objPCL026.WeldInFabOutside = pcl026.WeldInFabOutside;
                objPCL026.WeldInFabNotReq = pcl026.WeldInFabNotReq;
                objPCL026.WeldInFabShop = pcl026.WeldInFabShop;
                objPCL026.WeldInFabMC = pcl026.WeldInFabMC;
                objPCL026.WeldInFabLEMF = pcl026.WeldInFabLEMF;
                objPCL026.WeldInFabRNW = pcl026.WeldInFabRNW;
                objPCL026.CTFabNotReq = pcl026.CTFabNotReq;
                objPCL026.CTFabShop = pcl026.CTFabShop;
                objPCL026.CTFabMC = pcl026.CTFabMC;
                objPCL026.CTFabLEMF = pcl026.CTFabLEMF;
                objPCL026.CTFabRNW = pcl026.CTFabRNW;
                objPCL026.CTFabOutside = pcl026.CTFabOutside;
                objPCL026.ITANotReq = pcl026.ITANotReq;
                objPCL026.ITAShop = pcl026.ITAShop;
                objPCL026.ITAMC = pcl026.ITAMC;
                objPCL026.ITALEMF = pcl026.ITALEMF;
                objPCL026.ITARNW = pcl026.ITARNW;
                objPCL026.ITAOutside = pcl026.ITAOutside;
                objPCL026.BraketWEPNotReq = pcl026.BraketWEPNotReq;
                objPCL026.BraketWEPShop = pcl026.BraketWEPShop;
                objPCL026.BraketWEPMC = pcl026.BraketWEPMC;
                objPCL026.BraketWEPLEMF = pcl026.BraketWEPLEMF;
                objPCL026.BraketWEPRNW = pcl026.BraketWEPRNW;
                objPCL026.BraketWEPOutside = pcl026.BraketWEPOutside;
                objPCL026.TSBFabNotReq = pcl026.TSBFabNotReq;
                objPCL026.TSBFabShop = pcl026.TSBFabShop;
                objPCL026.TSBFabMC = pcl026.TSBFabMC;
                objPCL026.TSBFabLEMF = pcl026.TSBFabLEMF;
                objPCL026.TSBFabRNW = pcl026.TSBFabRNW;
                objPCL026.TSBFabOutside = pcl026.TSBFabOutside;
                objPCL026.InsFabNotReq = pcl026.InsFabNotReq;
                objPCL026.InsFabShop = pcl026.InsFabShop;
                objPCL026.InsFabMC = pcl026.InsFabMC;
                objPCL026.InsFabLEMF = pcl026.InsFabLEMF;
                objPCL026.InsFabRNW = pcl026.InsFabRNW;
                objPCL026.InsFabOutside = pcl026.InsFabOutside;
                objPCL026.CatalystBeamWEPNotReq = pcl026.CatalystBeamWEPNotReq;
                objPCL026.CatalystBeamWEPShop = pcl026.CatalystBeamWEPShop;
                objPCL026.CatalystBeamWEPMC = pcl026.CatalystBeamWEPMC;
                objPCL026.CatalystBeamWEPLEMF = pcl026.CatalystBeamWEPLEMF;
                objPCL026.CatalystBeamWEPRNW = pcl026.CatalystBeamWEPRNW;
                objPCL026.CatalystBeamWEPOutside = pcl026.CatalystBeamWEPOutside;
                objPCL026.CatalystBeamFabricationNotReq = pcl026.CatalystBeamFabricationNotReq;
                objPCL026.CatalystBeamFabricationShop = pcl026.CatalystBeamFabricationShop;
                objPCL026.CatalystBeamFabricationMC = pcl026.CatalystBeamFabricationMC;
                objPCL026.CatalystBeamFabricationLEMF = pcl026.CatalystBeamFabricationLEMF;
                objPCL026.CatalystBeamFabricationRNW = pcl026.CatalystBeamFabricationRNW;
                objPCL026.CatalystBeamFabricationOutside = pcl026.CatalystBeamFabricationOutside;
                objPCL026.InternalTrayFabricationNotReq = pcl026.InternalTrayFabricationNotReq;
                objPCL026.InternalTrayFabricationShop = pcl026.InternalTrayFabricationShop;
                objPCL026.InternalTrayFabricationMC = pcl026.InternalTrayFabricationMC;
                objPCL026.InternalTrayFabricationLEMF = pcl026.InternalTrayFabricationLEMF;
                objPCL026.InternalTrayFabricationRNW = pcl026.InternalTrayFabricationRNW;
                objPCL026.InternalTrayFabricationOutside = pcl026.InternalTrayFabricationOutside;
                objPCL026.InletDiffuserFabricationNotReq = pcl026.InletDiffuserFabricationNotReq;
                objPCL026.InletDiffuserFabricationShop = pcl026.InletDiffuserFabricationShop;
                objPCL026.InletDiffuserFabricationMC = pcl026.InletDiffuserFabricationMC;
                objPCL026.InletDiffuserFabricationLEMF = pcl026.InletDiffuserFabricationLEMF;
                objPCL026.InletDiffuserFabricationRNW = pcl026.InletDiffuserFabricationRNW;
                objPCL026.InletDiffuserFabricationOutside = pcl026.InletDiffuserFabricationOutside;
                objPCL026.OutletCollectorNotReq = pcl026.OutletCollectorNotReq;
                objPCL026.OutletCollectorShop = pcl026.OutletCollectorShop;
                objPCL026.OutletCollectorMC = pcl026.OutletCollectorMC;
                objPCL026.OutletCollectorLEMF = pcl026.OutletCollectorLEMF;
                objPCL026.OutletCollectorRNW = pcl026.OutletCollectorRNW;
                objPCL026.OutletCollectorOutside = pcl026.OutletCollectorOutside;
                objPCL026.InternalNozzlesNotReq = pcl026.InternalNozzlesNotReq;
                objPCL026.InternalNozzlesShop = pcl026.InternalNozzlesShop;
                objPCL026.InternalNozzlesMC = pcl026.InternalNozzlesMC;
                objPCL026.InternalNozzlesLEMF = pcl026.InternalNozzlesLEMF;
                objPCL026.InternalNozzlesRNW = pcl026.InternalNozzlesRNW;
                objPCL026.InternalNozzlesOutside = pcl026.InternalNozzlesOutside;
                objPCL026.SaddlesNotReq = pcl026.SaddlesNotReq;
                objPCL026.SaddlesShop = pcl026.SaddlesShop;
                objPCL026.SaddlesMC = pcl026.SaddlesMC;
                objPCL026.SaddlesLEMF = pcl026.SaddlesLEMF;
                objPCL026.SaddlesRNW = pcl026.SaddlesRNW;
                objPCL026.SaddlesOutside = pcl026.SaddlesOutside;
                objPCL026.SkirtTemplateNotReq = pcl026.SkirtTemplateNotReq;
                objPCL026.SkirtTemplateShop = pcl026.SkirtTemplateShop;
                objPCL026.SkirtTemplateMC = pcl026.SkirtTemplateMC;
                objPCL026.SkirtTemplateLEMF = pcl026.SkirtTemplateLEMF;
                objPCL026.SkirtTemplateRNW = pcl026.SkirtTemplateRNW;
                objPCL026.SkirtTemplateOutside = pcl026.SkirtTemplateOutside;
                objPCL026.PlatformFabricationNotReq = pcl026.PlatformFabricationNotReq;
                objPCL026.PlatformFabricationShop = pcl026.PlatformFabricationShop;
                objPCL026.PlatformFabricationMC = pcl026.PlatformFabricationMC;
                objPCL026.PlatformFabricationLEMF = pcl026.PlatformFabricationLEMF;
                objPCL026.PlatformFabricationRNW = pcl026.PlatformFabricationRNW;
                objPCL026.PlatformFabricationOutside = pcl026.PlatformFabricationOutside;
                objPCL026.PlatformTrialAssemblyNotReq = pcl026.PlatformTrialAssemblyNotReq;
                objPCL026.PlatformTrialAssemblyShop = pcl026.PlatformTrialAssemblyShop;
                objPCL026.PlatformTrialAssemblyMC = pcl026.PlatformTrialAssemblyMC;
                objPCL026.PlatformTrialAssemblyLEMF = pcl026.PlatformTrialAssemblyLEMF;
                objPCL026.PlatformTrialAssemblyRNW = pcl026.PlatformTrialAssemblyRNW;
                objPCL026.PlatformTrialAssemblyOutside = pcl026.PlatformTrialAssemblyOutside;
                objPCL026.PWHTNotReq = pcl026.PWHTNotReq;
                objPCL026.PWHTShop = pcl026.PWHTShop;
                objPCL026.PWHTMC = pcl026.PWHTMC;
                objPCL026.PWHTLEMF = pcl026.PWHTLEMF;
                objPCL026.PWHTRNW = pcl026.PWHTRNW;
                objPCL026.PWHTOutside = pcl026.PWHTOutside;
                objPCL026.InternalInstallationNotReq = pcl026.InternalInstallationNotReq;
                objPCL026.InternalInstallationShop = pcl026.InternalInstallationShop;
                objPCL026.InternalInstallationMC = pcl026.InternalInstallationMC;
                objPCL026.InternalInstallationLEMF = pcl026.InternalInstallationLEMF;
                objPCL026.InternalInstallationRNW = pcl026.InternalInstallationRNW;
                objPCL026.InternalInstallationOutside = pcl026.InternalInstallationOutside;
                objPCL026.N2FillingNotReq = pcl026.N2FillingNotReq;
                objPCL026.N2FillingShop = pcl026.N2FillingShop;
                objPCL026.N2FillingMC = pcl026.N2FillingMC;
                objPCL026.N2FillingLEMF = pcl026.N2FillingLEMF;
                objPCL026.N2FillingRNW = pcl026.N2FillingRNW;
                objPCL026.N2FillingOutside = pcl026.N2FillingOutside;
                objPCL026.SandBlastingNotReq = pcl026.SandBlastingNotReq;
                objPCL026.SandBlastingShop = pcl026.SandBlastingShop;
                objPCL026.SandBlastingMC = pcl026.SandBlastingMC;
                objPCL026.SandBlastingLEMF = pcl026.SandBlastingLEMF;
                objPCL026.SandBlastingRNW = pcl026.SandBlastingRNW;
                objPCL026.SandBlastingOutside = pcl026.SandBlastingOutside;
                objPCL026.PaintingNotReq = pcl026.PaintingNotReq;
                objPCL026.PaintingShop = pcl026.PaintingShop;
                objPCL026.PaintingMC = pcl026.PaintingMC;
                objPCL026.PaintingLEMF = pcl026.PaintingLEMF;
                objPCL026.PaintingRNW = pcl026.PaintingRNW;
                objPCL026.PaintingOutside = pcl026.PaintingOutside;
                objPCL026.AcidCleaningNotReq = pcl026.AcidCleaningNotReq;
                objPCL026.AcidCleaningShop = pcl026.AcidCleaningShop;
                objPCL026.AcidCleaningMC = pcl026.AcidCleaningMC;
                objPCL026.AcidCleaningLEMF = pcl026.AcidCleaningLEMF;
                objPCL026.AcidCleaningRNW = pcl026.AcidCleaningRNW;
                objPCL026.AcidCleaningOutside = pcl026.AcidCleaningOutside;
                objPCL026.WeightMeasurementNotReq = pcl026.WeightMeasurementNotReq;
                objPCL026.WeightMeasurementShop = pcl026.WeightMeasurementShop;
                objPCL026.WeightMeasurementMC = pcl026.WeightMeasurementMC;
                objPCL026.WeightMeasurementLEMF = pcl026.WeightMeasurementLEMF;
                objPCL026.WeightMeasurementRNW = pcl026.WeightMeasurementRNW;
                objPCL026.WeightMeasurementOutside = pcl026.WeightMeasurementOutside;
                objPCL026.ManwayDavitNotReq = pcl026.ManwayDavitNotReq;
                objPCL026.ManwayDavitShop = pcl026.ManwayDavitShop;
                objPCL026.ManwayDavitMC = pcl026.ManwayDavitMC;
                objPCL026.ManwayDavitLEMF = pcl026.ManwayDavitLEMF;
                objPCL026.ManwayDavitRNW = pcl026.ManwayDavitRNW;
                objPCL026.ManwayDavitOutside = pcl026.ManwayDavitOutside;
                objPCL026.VesselDavitNotReq = pcl026.VesselDavitNotReq;
                objPCL026.VesselDavitShop = pcl026.VesselDavitShop;
                objPCL026.VesselDavitMC = pcl026.VesselDavitMC;
                objPCL026.VesselDavitLEMF = pcl026.VesselDavitLEMF;
                objPCL026.VesselDavitRNW = pcl026.VesselDavitRNW;
                objPCL026.VesselDavitOutside = pcl026.VesselDavitOutside;
                objPCL026.LappingRingNotReq = pcl026.LappingRingNotReq;
                objPCL026.LappingRingShop = pcl026.LappingRingShop;
                objPCL026.LappingRingMC = pcl026.LappingRingMC;
                objPCL026.LappingRingLEMF = pcl026.LappingRingLEMF;
                objPCL026.LappingRingRNW = pcl026.LappingRingRNW;
                objPCL026.LappingRingOutside = pcl026.LappingRingOutside;
                objPCL026.ShippingNotReq = pcl026.ShippingNotReq;
                objPCL026.ShippingShop = pcl026.ShippingShop;
                objPCL026.ShippingMC = pcl026.ShippingMC;
                objPCL026.ShippingLEMF = pcl026.ShippingLEMF;
                objPCL026.ShippingRNW = pcl026.ShippingRNW;
                objPCL026.ShippingOutside = pcl026.ShippingOutside;
                if (pcl026.LineId > 0)
                {
                    objPCL026.EditedBy = objClsLoginInfo.UserName;
                    objPCL026.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL026.CreatedBy = objClsLoginInfo.UserName;
                    objPCL026.CreatedOn = DateTime.Now;
                    db.PCL026.Add(objPCL026);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL026.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveColumnFixture(PCL029 pcl029, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl029.HeaderId).FirstOrDefault();
                PCL029 objPCL029 = new PCL029();
                bool IsEdited = false;
                if (pcl029.LineId > 0)
                {
                    objPCL029 = db.PCL029.Where(x => x.LineId == pcl029.LineId).FirstOrDefault();
                    IsEdited = true;
                }

                objPCL029.HeaderId = objPCL001.HeaderId;
                objPCL029.Project = objPCL001.Project;
                objPCL029.Document = objPCL001.Document;

                objPCL029.TempRollReqDate = pcl029.TempRollReqDate;
                string TempRollReqDate = fc["TempRollReqDate"] != null ? fc["TempRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempRollReqDate))
                {
                    objPCL029.TempRollReqDate = DateTime.ParseExact(TempRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.TempRollNoFix = pcl029.TempRollNoFix;
                objPCL029.TempRollAvlQty = pcl029.TempRollAvlQty;
                objPCL029.TempRollDrg = pcl029.TempRollDrg;
                objPCL029.TempRollMtrl = pcl029.TempRollMtrl;
                objPCL029.TempRollFab = pcl029.TempRollFab;
                objPCL029.TempRollDel = pcl029.TempRollDel;
                objPCL029.TempWeldReqDate = pcl029.TempWeldReqDate;
                string TempWeldReqDate = fc["TempWeldReqDate"] != null ? fc["TempWeldReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempWeldReqDate))
                {
                    objPCL029.TempWeldReqDate = DateTime.ParseExact(TempWeldReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.TempWeldNoFix = pcl029.TempWeldNoFix;
                objPCL029.TempWeldAvlQty = pcl029.TempWeldAvlQty;
                objPCL029.TempWeldDrg = pcl029.TempWeldDrg;
                objPCL029.TempWeldMtrl = pcl029.TempWeldMtrl;
                objPCL029.TempWeldFab = pcl029.TempWeldFab;
                objPCL029.TempWeldDel = pcl029.TempWeldDel;
                objPCL029.GasketCoverReqDate = pcl029.GasketCoverReqDate;
                string GasketCoverReqDate = fc["GasketCoverReqDate"] != null ? fc["GasketCoverReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(GasketCoverReqDate))
                {
                    objPCL029.GasketCoverReqDate = DateTime.ParseExact(GasketCoverReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.GasketCoverNoFix = pcl029.GasketCoverNoFix;
                objPCL029.GasketCoverAvlQty = pcl029.GasketCoverAvlQty;
                objPCL029.GasketCoverDrg = pcl029.GasketCoverDrg;
                objPCL029.GasketCoverMtrl = pcl029.GasketCoverMtrl;
                objPCL029.GasketCoverFab = pcl029.GasketCoverFab;
                objPCL029.GasketCoverDel = pcl029.GasketCoverDel;
                objPCL029.SpiderReqDate = pcl029.SpiderReqDate;
                string SpiderReqDate = fc["SpiderReqDate"] != null ? fc["SpiderReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SpiderReqDate))
                {
                    objPCL029.SpiderReqDate = DateTime.ParseExact(SpiderReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.SpiderNoFix = pcl029.SpiderNoFix;
                objPCL029.SpiderAvlQty = pcl029.SpiderAvlQty;
                objPCL029.SpiderDrg = pcl029.SpiderDrg;
                objPCL029.SpiderMtrl = pcl029.SpiderMtrl;
                objPCL029.SpiderFab = pcl029.SpiderFab;
                objPCL029.SpiderDel = pcl029.SpiderDel;
                objPCL029.LugShellReqDate = pcl029.LugShellReqDate;
                string LugShellReqDate = fc["LugShellReqDate"] != null ? fc["LugShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LugShellReqDate))
                {
                    objPCL029.LugShellReqDate = DateTime.ParseExact(LugShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.LugShellNoFix = pcl029.LugShellNoFix;
                objPCL029.LugShellAvlQty = pcl029.LugShellAvlQty;
                objPCL029.LugShellDrg = pcl029.LugShellDrg;
                objPCL029.LugShellMtrl = pcl029.LugShellMtrl;
                objPCL029.LugShellFab = pcl029.LugShellFab;
                objPCL029.LugShellDel = pcl029.LugShellDel;
                objPCL029.DiePunchReqDate = pcl029.DiePunchReqDate;
                string DiePunchReqDate = fc["DiePunchReqDate"] != null ? fc["DiePunchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DiePunchReqDate))
                {
                    objPCL029.DiePunchReqDate = DateTime.ParseExact(DiePunchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.DiePunchNoFix = pcl029.DiePunchNoFix;
                objPCL029.DiePunchAvlQty = pcl029.DiePunchAvlQty;
                objPCL029.DiePunchDrg = pcl029.DiePunchDrg;
                objPCL029.DiePunchMtrl = pcl029.DiePunchMtrl;
                objPCL029.DiePunchFab = pcl029.DiePunchFab;
                objPCL029.DiePunchDel = pcl029.DiePunchDel;
                objPCL029.SquareBarReqDate = pcl029.SquareBarReqDate;
                string SquareBarReqDate = fc["SquareBarReqDate"] != null ? fc["SquareBarReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SquareBarReqDate))
                {
                    objPCL029.SquareBarReqDate = DateTime.ParseExact(SquareBarReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.SquareBarNoFix = pcl029.SquareBarNoFix;
                objPCL029.SquareBarAvlQty = pcl029.SquareBarAvlQty;
                objPCL029.SquareBarDrg = pcl029.SquareBarDrg;
                objPCL029.SquareBarMtrl = pcl029.SquareBarMtrl;
                objPCL029.SquareBarFab = pcl029.SquareBarFab;
                objPCL029.SquareBarDel = pcl029.SquareBarDel;
                objPCL029.PreHeatReqDate = pcl029.PreHeatReqDate;
                string PreHeatReqDate = fc["PreHeatReqDate"] != null ? fc["PreHeatReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PreHeatReqDate))
                {
                    objPCL029.PreHeatReqDate = DateTime.ParseExact(PreHeatReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.PreHeatNoFix = pcl029.PreHeatNoFix;
                objPCL029.PreHeatAvlQty = pcl029.PreHeatAvlQty;
                objPCL029.PreHeatDrg = pcl029.PreHeatDrg;
                objPCL029.PreHeatMtrl = pcl029.PreHeatMtrl;
                objPCL029.PreHeatFab = pcl029.PreHeatFab;
                objPCL029.PreHeatDel = pcl029.PreHeatDel;
                objPCL029.FixIntReqDate = pcl029.FixIntReqDate;
                string FixIntReqDate = fc["FixIntReqDate"] != null ? fc["FixIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixIntReqDate))
                {
                    objPCL029.FixIntReqDate = DateTime.ParseExact(FixIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.FixIntNoFix = pcl029.FixIntNoFix;
                objPCL029.FixIntAvlQty = pcl029.FixIntAvlQty;
                objPCL029.FixIntDrg = pcl029.FixIntDrg;
                objPCL029.FixIntMtrl = pcl029.FixIntMtrl;
                objPCL029.FixIntFab = pcl029.FixIntFab;
                objPCL029.FixIntDel = pcl029.FixIntDel;
                objPCL029.EvenerBeamReqDate = pcl029.EvenerBeamReqDate;
                string EvenerBeamReqDate = fc["EvenerBeamReqDate"] != null ? fc["EvenerBeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(EvenerBeamReqDate))
                {
                    objPCL029.EvenerBeamReqDate = DateTime.ParseExact(EvenerBeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.EvenerBeamNoFix = pcl029.EvenerBeamNoFix;
                objPCL029.EvenerBeamAvlQty = pcl029.EvenerBeamAvlQty;
                objPCL029.EvenerBeamDrg = pcl029.EvenerBeamDrg;
                objPCL029.EvenerBeamMtrl = pcl029.EvenerBeamMtrl;
                objPCL029.EvenerBeamFab = pcl029.EvenerBeamFab;
                objPCL029.EvenerBeamDel = pcl029.EvenerBeamDel;
                objPCL029.ExtRingReqDate = pcl029.ExtRingReqDate;
                string ExtRingReqDate = fc["ExtRingReqDate"] != null ? fc["ExtRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ExtRingReqDate))
                {
                    objPCL029.ExtRingReqDate = DateTime.ParseExact(ExtRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.ExtRingNoFix = pcl029.ExtRingNoFix;
                objPCL029.ExtRingAvlQty = pcl029.ExtRingAvlQty;
                objPCL029.ExtRingDrg = pcl029.ExtRingDrg;
                objPCL029.ExtRingMtrl = pcl029.ExtRingMtrl;
                objPCL029.ExtRingFab = pcl029.ExtRingFab;
                objPCL029.ExtRingDel = pcl029.ExtRingDel;
                objPCL029.FixPWHTReqDate = pcl029.FixPWHTReqDate;
                string FixPWHTReqDate = fc["FixPWHTReqDate"] != null ? fc["FixPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixPWHTReqDate))
                {
                    objPCL029.FixPWHTReqDate = DateTime.ParseExact(FixPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.FixPWHTNoFix = pcl029.FixPWHTNoFix;
                objPCL029.FixPWHTAvlQty = pcl029.FixPWHTAvlQty;
                objPCL029.FixPWHTDrg = pcl029.FixPWHTDrg;
                objPCL029.FixPWHTMtrl = pcl029.FixPWHTMtrl;
                objPCL029.FixPWHTFab = pcl029.FixPWHTFab;
                objPCL029.FixPWHTDel = pcl029.FixPWHTDel;
                objPCL029.SaddlePWHTReqDate = pcl029.SaddlePWHTReqDate;
                string SaddlePWHTReqDate = fc["SaddlePWHTReqDate"] != null ? fc["SaddlePWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddlePWHTReqDate))
                {
                    objPCL029.SaddlePWHTReqDate = DateTime.ParseExact(SaddlePWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.SaddlePWHTNoFix = pcl029.SaddlePWHTNoFix;
                objPCL029.SaddlePWHTAvlQty = pcl029.SaddlePWHTAvlQty;
                objPCL029.SaddlePWHTDrg = pcl029.SaddlePWHTDrg;
                objPCL029.SaddlePWHTMtrl = pcl029.SaddlePWHTMtrl;
                objPCL029.SaddlePWHTFab = pcl029.SaddlePWHTFab;
                objPCL029.SaddlePWHTDel = pcl029.SaddlePWHTDel;
                objPCL029.SuppIntReqDate = pcl029.SuppIntReqDate;
                string SuppIntReqDate = fc["SuppIntReqDate"] != null ? fc["SuppIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SuppIntReqDate))
                {
                    objPCL029.SuppIntReqDate = DateTime.ParseExact(SuppIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.SuppIntNoFix = pcl029.SuppIntNoFix;
                objPCL029.SuppIntAvlQty = pcl029.SuppIntAvlQty;
                objPCL029.SuppIntDrg = pcl029.SuppIntDrg;
                objPCL029.SuppIntMtrl = pcl029.SuppIntMtrl;
                objPCL029.SuppIntFab = pcl029.SuppIntFab;
                objPCL029.SuppIntDel = pcl029.SuppIntDel;
                objPCL029.SaddleHydroReqDate = pcl029.SaddleHydroReqDate;
                string SaddleHydroReqDate = fc["SaddleHydroReqDate"] != null ? fc["SaddleHydroReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddleHydroReqDate))
                {
                    objPCL029.SaddleHydroReqDate = DateTime.ParseExact(SaddleHydroReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.SaddleHydroNoFix = pcl029.SaddleHydroNoFix;
                objPCL029.SaddleHydroAvlQty = pcl029.SaddleHydroAvlQty;
                objPCL029.SaddleHydroDrg = pcl029.SaddleHydroDrg;
                objPCL029.SaddleHydroMtrl = pcl029.SaddleHydroMtrl;
                objPCL029.SaddleHydroFab = pcl029.SaddleHydroFab;
                objPCL029.SaddleHydroDel = pcl029.SaddleHydroDel;
                objPCL029.LSRFurnaceReqDate = pcl029.LSRFurnaceReqDate;
                string LSRFurnaceReqDate = fc["LSRFurnaceReqDate"] != null ? fc["LSRFurnaceReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRFurnaceReqDate))
                {
                    objPCL029.LSRFurnaceReqDate = DateTime.ParseExact(LSRFurnaceReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.LSRFurnaceNoFix = pcl029.LSRFurnaceNoFix;
                objPCL029.LSRFurnaceAvlQty = pcl029.LSRFurnaceAvlQty;
                objPCL029.LSRFurnaceDrg = pcl029.LSRFurnaceDrg;
                objPCL029.LSRFurnaceMtrl = pcl029.LSRFurnaceMtrl;
                objPCL029.LSRFurnaceFab = pcl029.LSRFurnaceFab;
                objPCL029.LSRFurnaceDel = pcl029.LSRFurnaceDel;
                objPCL029.LSRRingReqDate = pcl029.LSRRingReqDate;
                string LSRRingReqDate = fc["LSRRingReqDate"] != null ? fc["LSRRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRRingReqDate))
                {
                    objPCL029.LSRRingReqDate = DateTime.ParseExact(LSRRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.LSRRingNoFix = pcl029.LSRRingNoFix;
                objPCL029.LSRRingAvlQty = pcl029.LSRRingAvlQty;
                objPCL029.LSRRingDrg = pcl029.LSRRingDrg;
                objPCL029.LSRRingMtrl = pcl029.LSRRingMtrl;
                objPCL029.LSRRingFab = pcl029.LSRRingFab;
                objPCL029.LSRRingDel = pcl029.LSRRingDel;
                objPCL029.NozzSupReqDate = pcl029.NozzSupReqDate;
                string NozzSupReqDate = fc["NozzSupReqDate"] != null ? fc["NozzSupReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzSupReqDate))
                {
                    objPCL029.NozzSupReqDate = DateTime.ParseExact(NozzSupReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.NozzSupNoFix = pcl029.NozzSupNoFix;
                objPCL029.NozzSupAvlQty = pcl029.NozzSupAvlQty;
                objPCL029.NozzSupDrg = pcl029.NozzSupDrg;
                objPCL029.NozzSupMtrl = pcl029.NozzSupMtrl;
                objPCL029.NozzSupFab = pcl029.NozzSupFab;
                objPCL029.NozzSupDel = pcl029.NozzSupDel;
                objPCL029.MoonPlateReqDate = pcl029.MoonPlateReqDate;
                string MoonPlateReqDate = fc["MoonPlateReqDate"] != null ? fc["MoonPlateReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(MoonPlateReqDate))
                {
                    objPCL029.MoonPlateReqDate = DateTime.ParseExact(MoonPlateReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.MoonPlateNoFix = pcl029.MoonPlateNoFix;
                objPCL029.MoonPlateAvlQty = pcl029.MoonPlateAvlQty;
                objPCL029.MoonPlateDrg = pcl029.MoonPlateDrg;
                objPCL029.MoonPlateMtrl = pcl029.MoonPlateMtrl;
                objPCL029.MoonPlateFab = pcl029.MoonPlateFab;
                objPCL029.MoonPlateDel = pcl029.MoonPlateDel;
                objPCL029.BoltableReqDate = pcl029.BoltableReqDate;
                string BoltableReqDate = fc["BoltableReqDate"] != null ? fc["BoltableReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BoltableReqDate))
                {
                    objPCL029.BoltableReqDate = DateTime.ParseExact(BoltableReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.BoltableNoFix = pcl029.BoltableNoFix;
                objPCL029.BoltableAvlQty = pcl029.BoltableAvlQty;
                objPCL029.BoltableDrg = pcl029.BoltableDrg;
                objPCL029.BoltableMtrl = pcl029.BoltableMtrl;
                objPCL029.BoltableFab = pcl029.BoltableFab;
                objPCL029.BoltableDel = pcl029.BoltableDel;
                objPCL029.PlateTrunReqDate = pcl029.PlateTrunReqDate;
                string PlateTrunReqDate = fc["PlateTrunReqDate"] != null ? fc["PlateTrunReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateTrunReqDate))
                {
                    objPCL029.PlateTrunReqDate = DateTime.ParseExact(PlateTrunReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.PlateTrunNoFix = pcl029.PlateTrunNoFix;
                objPCL029.PlateTrunAvlQty = pcl029.PlateTrunAvlQty;
                objPCL029.PlateTrunDrg = pcl029.PlateTrunDrg;
                objPCL029.PlateTrunMtrl = pcl029.PlateTrunMtrl;
                objPCL029.PlateTrunFab = pcl029.PlateTrunFab;
                objPCL029.PlateTrunDel = pcl029.PlateTrunDel;
                objPCL029.PipeTrunReqDate = pcl029.PipeTrunReqDate;
                string PipeTrunReqDate = fc["PipeTrunReqDate"] != null ? fc["PipeTrunReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PipeTrunReqDate))
                {
                    objPCL029.PipeTrunReqDate = DateTime.ParseExact(PipeTrunReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.PipeTrunNoFix = pcl029.PipeTrunNoFix;
                objPCL029.PipeTrunAvlQty = pcl029.PipeTrunAvlQty;
                objPCL029.PipeTrunDrg = pcl029.PipeTrunDrg;
                objPCL029.PipeTrunMtrl = pcl029.PipeTrunMtrl;
                objPCL029.PipeTrunFab = pcl029.PipeTrunFab;
                objPCL029.PipeTrunDel = pcl029.PipeTrunDel;
                objPCL029.PlateLugsReqDate = pcl029.PlateLugsReqDate;
                string PlateLugsReqDate = fc["PlateLugsReqDate"] != null ? fc["PlateLugsReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateLugsReqDate))
                {
                    objPCL029.PlateLugsReqDate = DateTime.ParseExact(PlateLugsReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.PlateLugsNoFix = pcl029.PlateLugsNoFix;
                objPCL029.PlateLugsAvlQty = pcl029.PlateLugsAvlQty;
                objPCL029.PlateLugsDrg = pcl029.PlateLugsDrg;
                objPCL029.PlateLugsMtrl = pcl029.PlateLugsMtrl;
                objPCL029.PlateLugsFab = pcl029.PlateLugsFab;
                objPCL029.PlateLugsDel = pcl029.PlateLugsDel;
                objPCL029.PlateHydroReqDate = pcl029.PlateHydroReqDate;
                string PlateHydroReqDate = fc["PlateHydroReqDate"] != null ? fc["PlateHydroReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateHydroReqDate))
                {
                    objPCL029.PlateHydroReqDate = DateTime.ParseExact(PlateHydroReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL029.PlateHydroNoFix = pcl029.PlateHydroNoFix;
                objPCL029.PlateHydroAvlQty = pcl029.PlateHydroAvlQty;
                objPCL029.PlateHydroDrg = pcl029.PlateHydroDrg;
                objPCL029.PlateHydroMtrl = pcl029.PlateHydroMtrl;
                objPCL029.PlateHydroFab = pcl029.PlateHydroFab;
                objPCL029.PlateHydroDel = pcl029.PlateHydroDel;
                if (IsEdited)
                {
                    objPCL029.EditedBy = objClsLoginInfo.UserName;
                    objPCL029.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL029.CreatedBy = objClsLoginInfo.UserName;
                    objPCL029.CreatedOn = DateTime.Now;
                    db.PCL029.Add(objPCL029);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL029.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveColumnDocument(PCL027 pcl027, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl027.HeaderId).FirstOrDefault();
                PCL027 objPCL027 = new PCL027();
                if (pcl027.LineId > 0)
                {
                    objPCL027 = db.PCL027.Where(x => x.LineId == pcl027.LineId).FirstOrDefault();
                }
                objPCL027.HeaderId = objPCL001.HeaderId;
                objPCL027.Project = objPCL001.Project;
                objPCL027.Document = objPCL001.Document;
                objPCL027.JPPReq = pcl027.JPPReq;
                objPCL027.JPPReqDate = pcl027.JPPReqDate;
                string JPPReqDate = fc["JPPReqDate"] != null ? fc["JPPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReqDate))
                {
                    objPCL027.JPPReqDate = DateTime.ParseExact(JPPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.JPPDocNo = pcl027.JPPDocNo;
                objPCL027.JPPReleasedOn = pcl027.JPPReleasedOn;
                string JPPReleasedOn = fc["JPPReleasedOn"] != null ? fc["JPPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReleasedOn))
                {
                    objPCL027.JPPReleasedOn = DateTime.ParseExact(JPPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.LineSketchReq = pcl027.LineSketchReq;
                objPCL027.LineSketchReqDate = pcl027.LineSketchReqDate;
                string LineSketchReqDate = fc["LineSketchReqDate"] != null ? fc["LineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineSketchReqDate))
                {
                    objPCL027.LineSketchReqDate = DateTime.ParseExact(LineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.LineSketchDocNo = pcl027.LineSketchDocNo;
                objPCL027.LineSketchReleasedOn = pcl027.LineSketchReleasedOn;
                string LineSketchReleasedOn = fc["LineSketchReleasedOn"] != null ? fc["LineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineSketchReleasedOn))
                {
                    objPCL027.LineSketchReleasedOn = DateTime.ParseExact(LineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RollingCapaReq = pcl027.RollingCapaReq;
                objPCL027.RollingCapaReqDate = pcl027.RollingCapaReqDate;
                string RollingCapaReqDate = fc["RollingCapaReqDate"] != null ? fc["RollingCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReqDate))
                {
                    objPCL027.RollingCapaReqDate = DateTime.ParseExact(RollingCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RollingCapaDocNo = pcl027.RollingCapaDocNo;
                objPCL027.RollingCapaReleasedOn = pcl027.RollingCapaReleasedOn;
                string RollingCapaReleasedOn = fc["RollingCapaReleasedOn"] != null ? fc["RollingCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReleasedOn))
                {
                    objPCL027.RollingCapaReleasedOn = DateTime.ParseExact(RollingCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.ImprovBudgetReq = pcl027.ImprovBudgetReq;
                objPCL027.ImprovBudgetReqDate = pcl027.ImprovBudgetReqDate;
                string ImprovBudgetReqDate = fc["ImprovBudgetReqDate"] != null ? fc["ImprovBudgetReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReqDate))
                {
                    objPCL027.ImprovBudgetReqDate = DateTime.ParseExact(ImprovBudgetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.ImprovBudgetDocNo = pcl027.ImprovBudgetDocNo;
                objPCL027.ImprovBudgetReleasedOn = pcl027.ImprovBudgetReleasedOn;
                string ImprovBudgetReleasedOn = fc["ImprovBudgetReleasedOn"] != null ? fc["ImprovBudgetReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReleasedOn))
                {
                    objPCL027.ImprovBudgetReleasedOn = DateTime.ParseExact(ImprovBudgetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RCCPBreakUpReq = pcl027.RCCPBreakUpReq;
                objPCL027.RCCPBreakUpReqDate = pcl027.RCCPBreakUpReqDate;
                string RCCPBreakUpReqDate = fc["RCCPBreakUpReqDate"] != null ? fc["RCCPBreakUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReqDate))
                {
                    objPCL027.RCCPBreakUpReqDate = DateTime.ParseExact(RCCPBreakUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RCCPBreakUpDocNo = pcl027.RCCPBreakUpDocNo;
                objPCL027.RCCPBreakUpReleasedOn = pcl027.RCCPBreakUpReleasedOn;
                string RCCPBreakUpReleasedOn = fc["RCCPBreakUpReleasedOn"] != null ? fc["RCCPBreakUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReleasedOn))
                {
                    objPCL027.RCCPBreakUpReleasedOn = DateTime.ParseExact(RCCPBreakUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SchedConcertoReq = pcl027.SchedConcertoReq;
                objPCL027.SchedConcertoReqDate = pcl027.SchedConcertoReqDate;
                string SchedConcertoReqDate = fc["SchedConcertoReqDate"] != null ? fc["SchedConcertoReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReqDate))
                {
                    objPCL027.SchedConcertoReqDate = DateTime.ParseExact(SchedConcertoReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SchedConcertoDocNo = pcl027.SchedConcertoDocNo;
                objPCL027.SchedConcertoReleasedOn = pcl027.SchedConcertoReleasedOn;
                string SchedConcertoReleasedOn = fc["SchedConcertoReleasedOn"] != null ? fc["SchedConcertoReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReleasedOn))
                {
                    objPCL027.SchedConcertoReleasedOn = DateTime.ParseExact(SchedConcertoReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SchedExlReq = pcl027.SchedExlReq;
                objPCL027.SchedExlReqDate = pcl027.SchedExlReqDate;
                string SchedExlReqDate = fc["SchedExlReqDate"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReqDate))
                {
                    objPCL027.SchedExlReqDate = DateTime.ParseExact(SchedExlReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SchedExlDocNo = pcl027.SchedExlDocNo;
                objPCL027.SchedExlReleasedOn = pcl027.SchedExlReleasedOn;
                string SchedExlReleasedOn = fc["SchedExlReleasedOn"] != null ? fc["SchedExlReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReleasedOn))
                {
                    objPCL027.SchedExlReleasedOn = DateTime.ParseExact(SchedExlReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SubcontPlanReq = pcl027.SubcontPlanReq;
                objPCL027.SubcontPlanReqDate = pcl027.SubcontPlanReqDate;
                objPCL027.SubcontPlanDocNo = pcl027.SubcontPlanDocNo;
                objPCL027.SubcontPlanReleasedOn = pcl027.SubcontPlanReleasedOn;
                string SubcontPlanReleasedOn = fc["SubcontPlanReleasedOn"] != null ? fc["SubcontPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReleasedOn))
                {
                    objPCL027.SubcontPlanReleasedOn = DateTime.ParseExact(SubcontPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.LongSeamReq = pcl027.LongSeamReq;
                objPCL027.LongSeamReqDate = pcl027.LongSeamReqDate;
                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL027.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.LongSeamDocNo = pcl027.LongSeamDocNo;
                objPCL027.LongSeamReleasedOn = pcl027.LongSeamReleasedOn;
                string LongSeamReleasedOn = fc["LongSeamReleasedOn"] != null ? fc["LongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReleasedOn))
                {
                    objPCL027.LongSeamReleasedOn = DateTime.ParseExact(LongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.CircSeamReq = pcl027.CircSeamReq;
                objPCL027.CircSeamReqDate = pcl027.CircSeamReqDate;
                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL027.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.CircSeamDocNo = pcl027.CircSeamDocNo;
                objPCL027.CircSeamReleasedOn = pcl027.CircSeamReleasedOn;
                string CircSeamReleasedOn = fc["CircSeamReleasedOn"] != null ? fc["CircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReleasedOn))
                {
                    objPCL027.CircSeamReleasedOn = DateTime.ParseExact(CircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.NubBuildReq = pcl027.NubBuildReq;
                objPCL027.NubBuildReqDate = pcl027.NubBuildReqDate;
                string NubBuildReqDate = fc["NubBuildReqDate"] != null ? fc["NubBuildReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NubBuildReqDate))
                {
                    objPCL027.NubBuildReqDate = DateTime.ParseExact(NubBuildReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.NubBuildDocNo = pcl027.NubBuildDocNo;
                objPCL027.NubBuildReleasedOn = pcl027.NubBuildReleasedOn;
                string NubBuildReleasedOn = fc["NubBuildReleasedOn"] != null ? fc["NubBuildReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NubBuildReleasedOn))
                {
                    objPCL027.NubBuildReleasedOn = DateTime.ParseExact(NubBuildReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.NozzleShellReq = pcl027.NozzleShellReq;
                objPCL027.NozzleShellReqDate = pcl027.NozzleShellReqDate;
                string NozzleShellReqDate = fc["NozzleShellReqDate"] != null ? fc["NozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReqDate))
                {
                    objPCL027.NozzleShellReqDate = DateTime.ParseExact(NozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.NozzleShellDocNo = pcl027.NozzleShellDocNo;
                objPCL027.NozzleShellReleasedOn = pcl027.NozzleShellReleasedOn;
                string NozzleShellReleasedOn = fc["NozzleShellReleasedOn"] != null ? fc["NozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReleasedOn))
                {
                    objPCL027.NozzleShellReleasedOn = DateTime.ParseExact(NozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RollCapaReReq = pcl027.RollCapaReReq;
                objPCL027.RollCapaReReqDate = pcl027.RollCapaReReqDate;
                string RollCapaReReqDate = fc["RollCapaReReqDate"] != null ? fc["RollCapaReReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReReqDate))
                {
                    objPCL027.RollCapaReReqDate = DateTime.ParseExact(RollCapaReReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RollCapaReDocNo = pcl027.RollCapaReDocNo;
                objPCL027.RollCapaReReleasedOn = pcl027.RollCapaReReleasedOn;
                string RollCapaReReleasedOn = fc["RollCapaReReleasedOn"] != null ? fc["RollCapaReReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReReleasedOn))
                {
                    objPCL027.RollCapaReReleasedOn = DateTime.ParseExact(RollCapaReReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.TablesNozReq = pcl027.TablesNozReq;
                objPCL027.TablesNozReqDate = pcl027.TablesNozReqDate;
                string TablesNozReqDate = fc["TablesNozReqDate"] != null ? fc["TablesNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReqDate))
                {
                    objPCL027.TablesNozReqDate = DateTime.ParseExact(TablesNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.TablesNozDocNo = pcl027.TablesNozDocNo;
                objPCL027.TablesNozReleasedOn = pcl027.TablesNozReleasedOn;
                string TablesNozReleasedOn = fc["TablesNozReleasedOn"] != null ? fc["TablesNozReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReleasedOn))
                {
                    objPCL027.TablesNozReleasedOn = DateTime.ParseExact(TablesNozReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.TableIntReq = pcl027.TableIntReq;
                objPCL027.TableIntReqDate = pcl027.TableIntReqDate;
                string TableIntReqDate = fc["TableIntReqDate"] != null ? fc["TableIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReqDate))
                {
                    objPCL027.TableIntReqDate = DateTime.ParseExact(TableIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.TableIntDocNo = pcl027.TableIntDocNo;
                objPCL027.TableIntReleasedOn = pcl027.TableIntReleasedOn;
                string TableIntReleasedOn = fc["TableIntReleasedOn"] != null ? fc["TableIntReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReleasedOn))
                {
                    objPCL027.TableIntReleasedOn = DateTime.ParseExact(TableIntReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.TableExtReq = pcl027.TableExtReq;
                objPCL027.TableExtReqDate = pcl027.TableExtReqDate;
                string TableExtReqDate = fc["TableExtReqDate"] != null ? fc["TableExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReqDate))
                {
                    objPCL027.TableExtReqDate = DateTime.ParseExact(TableExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.TableExtDocNo = pcl027.TableExtDocNo;
                objPCL027.TableExtReleasedOn = pcl027.TableExtReleasedOn;
                string TableExtReleasedOn = fc["TableExtReleasedOn"] != null ? fc["TableExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReleasedOn))
                {
                    objPCL027.TableExtReleasedOn = DateTime.ParseExact(TableExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTREdgeReq = pcl027.HTREdgeReq;
                objPCL027.HTREdgeReqDate = pcl027.HTREdgeReqDate;
                string HTREdgeReqDate = fc["HTREdgeReqDate"] != null ? fc["HTREdgeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReqDate))
                {
                    objPCL027.HTREdgeReqDate = DateTime.ParseExact(HTREdgeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTREdgeDocNo = pcl027.HTREdgeDocNo;
                objPCL027.HTREdgeReleasedOn = pcl027.HTREdgeReleasedOn;
                string HTREdgeReleasedOn = fc["HTREdgeReleasedOn"] != null ? fc["HTREdgeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReleasedOn))
                {
                    objPCL027.HTREdgeReleasedOn = DateTime.ParseExact(HTREdgeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRRollReq = pcl027.HTRRollReq;
                objPCL027.HTRRollReqDate = pcl027.HTRRollReqDate;
                string HTRRollReqDate = fc["HTRRollReqDate"] != null ? fc["HTRRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReqDate))
                {
                    objPCL027.HTRRollReqDate = DateTime.ParseExact(HTRRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRRollDocNo = pcl027.HTRRollDocNo;
                objPCL027.HTRRollReleasedOn = pcl027.HTRRollReleasedOn;
                string HTRRollReleasedOn = fc["HTRRollReleasedOn"] != null ? fc["HTRRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReleasedOn))
                {
                    objPCL027.HTRRollReleasedOn = DateTime.ParseExact(HTRRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRReRollReq = pcl027.HTRReRollReq;
                objPCL027.HTRReRollReqDate = pcl027.HTRReRollReqDate;
                string HTRReRollReqDate = fc["HTRReRollReqDate"] != null ? fc["HTRReRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReqDate))
                {
                    objPCL027.HTRReRollReqDate = DateTime.ParseExact(HTRReRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRReRollDocNo = pcl027.HTRReRollDocNo;
                objPCL027.HTRReRollReleasedOn = pcl027.HTRReRollReleasedOn;
                string HTRReRollReleasedOn = fc["HTRReRollReleasedOn"] != null ? fc["HTRReRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReleasedOn))
                {
                    objPCL027.HTRReRollReleasedOn = DateTime.ParseExact(HTRReRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RefLineSketchReq = pcl027.RefLineSketchReq;
                objPCL027.RefLineSketchReqDate = pcl027.RefLineSketchReqDate;
                string RefLineSketchReqDate = fc["RefLineSketchReqDate"] != null ? fc["RefLineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReqDate))
                {
                    objPCL027.RefLineSketchReqDate = DateTime.ParseExact(RefLineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.RefLineSketchDocNo = pcl027.RefLineSketchDocNo;
                objPCL027.RefLineSketchReleasedOn = pcl027.RefLineSketchReleasedOn;
                string RefLineSketchReleasedOn = fc["RefLineSketchReleasedOn"] != null ? fc["RefLineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReleasedOn))
                {
                    objPCL027.RefLineSketchReleasedOn = DateTime.ParseExact(RefLineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRISRNozzleShellReq = pcl027.HTRISRNozzleShellReq;
                objPCL027.HTRISRNozzleShellReqDate = pcl027.HTRISRNozzleShellReqDate;
                string HTRISRNozzleShellReqDate = fc["HTRISRNozzleShellReqDate"] != null ? fc["HTRISRNozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReqDate))
                {
                    objPCL027.HTRISRNozzleShellReqDate = DateTime.ParseExact(HTRISRNozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRISRNozzleShellDocNo = pcl027.HTRISRNozzleShellDocNo;
                objPCL027.HTRISRNozzleShellReleasedOn = pcl027.HTRISRNozzleShellReleasedOn;
                string HTRISRNozzleShellReleasedOn = fc["HTRISRNozzleShellReleasedOn"] != null ? fc["HTRISRNozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReleasedOn))
                {
                    objPCL027.HTRISRNozzleShellReleasedOn = DateTime.ParseExact(HTRISRNozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRISRNozzleDEndReq = pcl027.HTRISRNozzleDEndReq;
                objPCL027.HTRISRNozzleDEndReqDate = pcl027.HTRISRNozzleDEndReqDate;
                string HTRISRNozzleDEndReqDate = fc["HTRISRNozzleDEndReqDate"] != null ? fc["HTRISRNozzleDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReqDate))
                {
                    objPCL027.HTRISRNozzleDEndReqDate = DateTime.ParseExact(HTRISRNozzleDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRISRNozzleDEndDocNo = pcl027.HTRISRNozzleDEndDocNo;
                objPCL027.HTRISRNozzleDEndReleasedOn = pcl027.HTRISRNozzleDEndReleasedOn;
                string HTRISRNozzleDEndReleasedOn = fc["HTRISRNozzleDEndReleasedOn"] != null ? fc["HTRISRNozzleDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReleasedOn))
                {
                    objPCL027.HTRISRNozzleDEndReleasedOn = DateTime.ParseExact(HTRISRNozzleDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRISRNubBuildUpReq = pcl027.HTRISRNubBuildUpReq;
                objPCL027.HTRISRNubBuildUpReqDate = pcl027.HTRISRNubBuildUpReqDate;
                string HTRISRNubBuildUpReqDate = fc["HTRISRNubBuildUpReqDate"] != null ? fc["HTRISRNubBuildUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReqDate))
                {
                    objPCL027.HTRISRNubBuildUpReqDate = DateTime.ParseExact(HTRISRNubBuildUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRISRNubBuildUpDocNo = pcl027.HTRISRNubBuildUpDocNo;
                objPCL027.HTRISRNubBuildUpReleasedOn = pcl027.HTRISRNubBuildUpReleasedOn;
                string HTRISRNubBuildUpReleasedOn = fc["HTRISRNubBuildUpReleasedOn"] != null ? fc["HTRISRNubBuildUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReleasedOn))
                {
                    objPCL027.HTRISRNubBuildUpReleasedOn = DateTime.ParseExact(HTRISRNubBuildUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRPWHTReq = pcl027.HTRPWHTReq;
                objPCL027.HTRPWHTReqDate = pcl027.HTRPWHTReqDate;
                string HTRPWHTReqDate = fc["HTRPWHTReqDate"] != null ? fc["HTRPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReqDate))
                {
                    objPCL027.HTRPWHTReqDate = DateTime.ParseExact(HTRPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRPWHTDocNo = pcl027.HTRPWHTDocNo;
                objPCL027.HTRPWHTReleasedOn = pcl027.HTRPWHTReleasedOn;
                string HTRPWHTReleasedOn = fc["HTRPWHTReleasedOn"] != null ? fc["HTRPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReleasedOn))
                {
                    objPCL027.HTRPWHTReleasedOn = DateTime.ParseExact(HTRPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRLSRReq = pcl027.HTRLSRReq;
                objPCL027.HTRLSRReqDate = pcl027.HTRLSRReqDate;
                string HTRLSRReqDate = fc["HTRLSRReqDate"] != null ? fc["HTRLSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReqDate))
                {
                    objPCL027.HTRLSRReqDate = DateTime.ParseExact(HTRLSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HTRLSRDocNo = pcl027.HTRLSRDocNo;
                objPCL027.HTRLSRReleasedOn = pcl027.HTRLSRReleasedOn;
                string HTRLSRReleasedOn = fc["HTRLSRReleasedOn"] != null ? fc["HTRLSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReleasedOn))
                {
                    objPCL027.HTRLSRReleasedOn = DateTime.ParseExact(HTRLSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HandlingPlanReq = pcl027.HandlingPlanReq;
                objPCL027.HandlingPlanReqDate = pcl027.HandlingPlanReqDate;
                string HandlingPlanReqDate = fc["HandlingPlanReqDate"] != null ? fc["HandlingPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReqDate))
                {
                    objPCL027.HandlingPlanReqDate = DateTime.ParseExact(HandlingPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HandlingPlanDocNo = pcl027.HandlingPlanDocNo;
                objPCL027.HandlingPlanReleasedOn = pcl027.HandlingPlanReleasedOn;
                string HandlingPlanReleasedOn = fc["HandlingPlanReleasedOn"] != null ? fc["HandlingPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReleasedOn))
                {
                    objPCL027.HandlingPlanReleasedOn = DateTime.ParseExact(HandlingPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.LocationTankRotatorReq = pcl027.LocationTankRotatorReq;
                objPCL027.LocationTankRotatorReqDate = pcl027.LocationTankRotatorReqDate;
                string LocationTankRotatorReqDate = fc["LocationTankRotatorReqDate"] != null ? fc["LocationTankRotatorReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReqDate))
                {
                    objPCL027.LocationTankRotatorReqDate = DateTime.ParseExact(LocationTankRotatorReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.LocationTankRotatorDocNo = pcl027.LocationTankRotatorDocNo;
                objPCL027.LocationTankRotatorReleasedOn = pcl027.LocationTankRotatorReleasedOn;
                string LocationTankRotatorReleasedOn = fc["LocationTankRotatorReleasedOn"] != null ? fc["LocationTankRotatorReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReleasedOn))
                {
                    objPCL027.LocationTankRotatorReleasedOn = DateTime.ParseExact(LocationTankRotatorReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HydrotestReq = pcl027.HydrotestReq;
                objPCL027.HydrotestReqDate = pcl027.HydrotestReqDate;
                string HydrotestReqDate = fc["HydrotestReqDate"] != null ? fc["HydrotestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReqDate))
                {
                    objPCL027.HydrotestReqDate = DateTime.ParseExact(HydrotestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.HydrotestDocNo = pcl027.HydrotestDocNo;
                objPCL027.HydrotestReleasedOn = pcl027.HydrotestReleasedOn;
                string HydrotestReleasedOn = fc["HydrotestReleasedOn"] != null ? fc["HydrotestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReleasedOn))
                {
                    objPCL027.HydrotestReleasedOn = DateTime.ParseExact(HydrotestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SurfaceTreatRequestReq = pcl027.SurfaceTreatRequestReq;
                objPCL027.SurfaceTreatRequestReqDate = pcl027.SurfaceTreatRequestReqDate;
                string SurfaceTreatRequestReqDate = fc["SurfaceTreatRequestReqDate"] != null ? fc["SurfaceTreatRequestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReqDate))
                {
                    objPCL027.SurfaceTreatRequestReqDate = DateTime.ParseExact(SurfaceTreatRequestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL027.SurfaceTreatRequestDocNo = pcl027.SurfaceTreatRequestDocNo;
                objPCL027.SurfaceTreatRequestReleasedOn = pcl027.SurfaceTreatRequestReleasedOn;
                string SurfaceTreatRequestReleasedOn = fc["SurfaceTreatRequestReleasedOn"] != null ? fc["SurfaceTreatRequestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReleasedOn))
                {
                    objPCL027.SurfaceTreatRequestReleasedOn = DateTime.ParseExact(SurfaceTreatRequestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                if (pcl027.LineId > 0)
                {
                    objPCL027.EditedBy = objClsLoginInfo.UserName;
                    objPCL027.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL027.CreatedBy = objClsLoginInfo.UserName;
                    objPCL027.CreatedOn = DateTime.Now;
                    db.PCL027.Add(objPCL027);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL027.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveColumnDim(PCL028 pcl028, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl028.HeaderId).FirstOrDefault();
                PCL028 objPCL028 = new PCL028();
                if (pcl028.LineId > 0)
                {
                    objPCL028 = db.PCL028.Where(x => x.LineId == pcl028.LineId).FirstOrDefault();
                }
                objPCL028.HeaderId = objPCL001.HeaderId;
                objPCL028.Project = objPCL001.Project;
                objPCL028.Document = objPCL001.Document;

                objPCL028.RollDataReq = pcl028.RollDataReq;
                objPCL028.RollDataReqDate = pcl028.RollDataReqDate;
                string RollDataReqDate = fc["RollDataReqDate"] != null ? fc["RollDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReqDate))
                {
                    objPCL028.RollDataReqDate = DateTime.ParseExact(RollDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.RollDataDocNo = pcl028.RollDataDocNo;
                objPCL028.RollDataReleasedOn = pcl028.RollDataReleasedOn;
                string RollDataReleasedOn = fc["RollDataReleasedOn"] != null ? fc["RollDataReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReleasedOn))
                {
                    objPCL028.RollDataReleasedOn = DateTime.ParseExact(RollDataReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ShellLongReq = pcl028.ShellLongReq;
                objPCL028.ShellLongReqDate = pcl028.ShellLongReqDate;
                string ShellLongReqDate = fc["ShellLongReqDate"] != null ? fc["ShellLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReqDate))
                {
                    objPCL028.ShellLongReqDate = DateTime.ParseExact(ShellLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ShellLongDocNo = pcl028.ShellLongDocNo;
                objPCL028.ShellLongReleasedOn = pcl028.ShellLongReleasedOn;
                string ShellLongReleasedOn = fc["ShellLongReleasedOn"] != null ? fc["ShellLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReleasedOn))
                {
                    objPCL028.ShellLongReleasedOn = DateTime.ParseExact(ShellLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ESSCShellReq = pcl028.ESSCShellReq;
                objPCL028.ESSCShellReqDate = pcl028.ESSCShellReqDate;
                string ESSCShellReqDate = fc["ESSCShellReqDate"] != null ? fc["ESSCShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReqDate))
                {
                    objPCL028.ESSCShellReqDate = DateTime.ParseExact(ESSCShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ESSCShellDocNo = pcl028.ESSCShellDocNo;
                objPCL028.ESSCShellReleasedOn = pcl028.ESSCShellReleasedOn;
                string ESSCShellReleasedOn = fc["ESSCShellReleasedOn"] != null ? fc["ESSCShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReleasedOn))
                {
                    objPCL028.ESSCShellReleasedOn = DateTime.ParseExact(ESSCShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ESSCDEndReq = pcl028.ESSCDEndReq;
                objPCL028.ESSCDEndReqDate = pcl028.ESSCDEndReqDate;
                string ESSCDEndReqDate = fc["ESSCDEndReqDate"] != null ? fc["ESSCDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCDEndReqDate))
                {
                    objPCL028.ESSCDEndReqDate = DateTime.ParseExact(ESSCDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ESSCDEndDocNo = pcl028.ESSCDEndDocNo;
                objPCL028.ESSCDEndReleasedOn = pcl028.ESSCDEndReleasedOn;
                string ESSCDEndReleasedOn = fc["ESSCDEndReleasedOn"] != null ? fc["ESSCDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCDEndReleasedOn))
                {
                    objPCL028.ESSCDEndReleasedOn = DateTime.ParseExact(ESSCDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ChangeOvalityExtReq = pcl028.ChangeOvalityExtReq;
                objPCL028.ChangeOvalityExtReqDate = pcl028.ChangeOvalityExtReqDate;
                string ChangeOvalityExtReqDate = fc["ChangeOvalityExtReqDate"] != null ? fc["ChangeOvalityExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReqDate))
                {
                    objPCL028.ChangeOvalityExtReqDate = DateTime.ParseExact(ChangeOvalityExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ChangeOvalityExtDocNo = pcl028.ChangeOvalityExtDocNo;
                objPCL028.ChangeOvalityExtReleasedOn = pcl028.ChangeOvalityExtReleasedOn;
                string ChangeOvalityExtReleasedOn = fc["ChangeOvalityExtReleasedOn"] != null ? fc["ChangeOvalityExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReleasedOn))
                {
                    objPCL028.ChangeOvalityExtReleasedOn = DateTime.ParseExact(ChangeOvalityExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ChangeOvalityWeldReq = pcl028.ChangeOvalityWeldReq;
                objPCL028.ChangeOvalityWeldReqDate = pcl028.ChangeOvalityWeldReqDate;
                string ChangeOvalityWeldReqDate = fc["ChangeOvalityWeldReqDate"] != null ? fc["ChangeOvalityWeldReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityWeldReqDate))
                {
                    objPCL028.ChangeOvalityWeldReqDate = DateTime.ParseExact(ChangeOvalityWeldReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ChangeOvalityWeldDocNo = pcl028.ChangeOvalityWeldDocNo;
                objPCL028.ChangeOvalityWeldReleasedOn = pcl028.ChangeOvalityWeldReleasedOn;
                string ChangeOvalityWeldReleasedOn = fc["ChangeOvalityWeldReleasedOn"] != null ? fc["ChangeOvalityWeldReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityWeldReleasedOn))
                {
                    objPCL028.ChangeOvalityWeldReleasedOn = DateTime.ParseExact(ChangeOvalityWeldReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ShrinkTSRReq = pcl028.ShrinkTSRReq;
                objPCL028.ShrinkTSRReqDate = pcl028.ShrinkTSRReqDate;
                string ShrinkTSRReqDate = fc["ShrinkTSRReqDate"] != null ? fc["ShrinkTSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReqDate))
                {
                    objPCL028.ShrinkTSRReqDate = DateTime.ParseExact(ShrinkTSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ShrinkTSRDocNo = pcl028.ShrinkTSRDocNo;
                objPCL028.ShrinkTSRReleasedOn = pcl028.ShrinkTSRReleasedOn;
                string ShrinkTSRReleasedOn = fc["ShrinkTSRReleasedOn"] != null ? fc["ShrinkTSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReleasedOn))
                {
                    objPCL028.ShrinkTSRReleasedOn = DateTime.ParseExact(ShrinkTSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ChangeOvalityISRReq = pcl028.ChangeOvalityISRReq;
                objPCL028.ChangeOvalityISRReqDate = pcl028.ChangeOvalityISRReqDate;
                string ChangeOvalityISRReqDate = fc["ChangeOvalityISRReqDate"] != null ? fc["ChangeOvalityISRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReqDate))
                {
                    objPCL028.ChangeOvalityISRReqDate = DateTime.ParseExact(ChangeOvalityISRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.ChangeOvalityISRDocNo = pcl028.ChangeOvalityISRDocNo;
                objPCL028.ChangeOvalityISRReleasedOn = pcl028.ChangeOvalityISRReleasedOn;
                string ChangeOvalityISRReleasedOn = fc["ChangeOvalityISRReleasedOn"] != null ? fc["ChangeOvalityISRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReleasedOn))
                {
                    objPCL028.ChangeOvalityISRReleasedOn = DateTime.ParseExact(ChangeOvalityISRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.DEendLongSeamReq = pcl028.DEendLongSeamReq;
                objPCL028.DEendLongSeamReqDate = pcl028.DEendLongSeamReqDate;
                string DEendLongSeamReqDate = fc["DEendLongSeamReqDate"] != null ? fc["DEendLongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReqDate))
                {
                    objPCL028.DEendLongSeamReqDate = DateTime.ParseExact(DEendLongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.DEendLongSeamDocNo = pcl028.DEendLongSeamDocNo;
                objPCL028.DEendLongSeamReleasedOn = pcl028.DEendLongSeamReleasedOn;
                string DEendLongSeamReleasedOn = fc["DEendLongSeamReleasedOn"] != null ? fc["DEendLongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReleasedOn))
                {
                    objPCL028.DEendLongSeamReleasedOn = DateTime.ParseExact(DEendLongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.NozzleReq = pcl028.NozzleReq;
                objPCL028.NozzleReqDate = pcl028.NozzleReqDate;
                string NozzleReqDate = fc["NozzleReqDate"] != null ? fc["NozzleReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleReqDate))
                {
                    objPCL028.NozzleReqDate = DateTime.ParseExact(NozzleReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL028.NozzleDocNo = pcl028.NozzleDocNo;
                objPCL028.NozzleReleasedOn = pcl028.NozzleReleasedOn;
                string NozzleReleasedOn = fc["NozzleReleasedOn"] != null ? fc["NozzleReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleReleasedOn))
                {
                    objPCL028.NozzleReleasedOn = DateTime.ParseExact(NozzleReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                if (pcl028.LineId > 0)
                {
                    objPCL028.EditedBy = objClsLoginInfo.UserName;
                    objPCL028.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL028.CreatedBy = objClsLoginInfo.UserName;
                    objPCL028.CreatedOn = DateTime.Now;
                    db.PCL028.Add(objPCL028);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL028.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region RR

        [HttpPost]
        public ActionResult SaveRRConstructor(PCL036 pcl036, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl036.HeaderId).FirstOrDefault();
                PCL036 objPCL036 = new PCL036();
                if (pcl036.LineId > 0)
                {
                    objPCL036 = db.PCL036.Where(x => x.LineId == pcl036.LineId).FirstOrDefault();
                }
                objPCL036.HeaderId = objPCL001.HeaderId;
                objPCL036.Project = objPCL001.Project;
                objPCL036.Document = objPCL001.Document;
                objPCL036.ReactorID = pcl036.ReactorID;
                objPCL036.ReactorThk1 = pcl036.ReactorThk1;
                objPCL036.ReactorTLTL = pcl036.ReactorTLTL;
                objPCL036.ReactorFabWt = pcl036.ReactorFabWt;
                objPCL036.ReactorHydroWt = pcl036.ReactorHydroWt;
                objPCL036.ReactorMaxShellWd = pcl036.ReactorMaxShellWd;
                objPCL036.ReactoreDesigned = pcl036.ReactoreDesigned;
                objPCL036.ShellConst1Seg = pcl036.ShellConst1Seg;
                objPCL036.ShellConst2Seg = pcl036.ShellConst2Seg;
                objPCL036.ShellConstMore2Seg = pcl036.ShellConstMore2Seg;
                objPCL036.TypeIntCyc = pcl036.TypeIntCyc;
                objPCL036.TypeIntDip = pcl036.TypeIntDip;
                objPCL036.TypeIntPle = pcl036.TypeIntPle;
                objPCL036.TypeIntAir = pcl036.TypeIntAir;
                objPCL036.TypeIntInt = pcl036.TypeIntInt;
                objPCL036.TypeIntRS2 = pcl036.TypeIntRS2;
                objPCL036.NooCyc = pcl036.NooCyc;
                objPCL036.NooCycVal = pcl036.NooCycVal;
                objPCL036.RiserSec = pcl036.RiserSec;
                objPCL036.RiserSecVal = pcl036.RiserSecVal;
                objPCL036.RefraAppNoofSec = pcl036.RefraAppNoofSec;
                objPCL036.RefraAppNoofSecVal = pcl036.RefraAppNoofSecVal;
                objPCL036.RefraAppMax = pcl036.RefraAppMax;
                objPCL036.RefraAppMaxVal = pcl036.RefraAppMaxVal;
                objPCL036.TypeRefraGun = pcl036.TypeRefraGun;
                objPCL036.TypeRefraVib = pcl036.TypeRefraVib;
                objPCL036.TypeRefraHan = pcl036.TypeRefraHan;
                objPCL036.TypeRefraFree = pcl036.TypeRefraFree;
                objPCL036.RefraDrySecInFur = pcl036.RefraDrySecInFur;
                objPCL036.RefraDrySecBaff = pcl036.RefraDrySecBaff;
                objPCL036.RefraDrySecElec = pcl036.RefraDrySecElec;
                objPCL036.RefraDryCycOpe = pcl036.RefraDryCycOpe;
                objPCL036.RefraDryCycLow = pcl036.RefraDryCycLow;
                objPCL036.TypeRollCold = pcl036.TypeRollCold;
                objPCL036.TypeRollWarm = pcl036.TypeRollWarm;
                objPCL036.TypeRollHot = pcl036.TypeRollHot;
                objPCL036.GasCuttingPreHeat = pcl036.GasCuttingPreHeat;
                objPCL036.GasCuttingPreHeatNR = pcl036.GasCuttingPreHeatNR;
                objPCL036.CSeamWEPPlate = pcl036.CSeamWEPPlate;
                objPCL036.CSeamWEPAfter = pcl036.CSeamWEPAfter;
                objPCL036.PTCRequiredforLS = pcl036.PTCRequiredforLS;
                objPCL036.PTCRequiredforCS = pcl036.PTCRequiredforCS;
                objPCL036.PTCRequiredforDend = pcl036.PTCRequiredforDend;
                objPCL036.PTCRequiredforDendShell = pcl036.PTCRequiredforDendShell;
                objPCL036.ISRRequiredforLS = pcl036.ISRRequiredforLS;
                objPCL036.ISRRequiredforCS = pcl036.ISRRequiredforCS;
                objPCL036.ISRRequiredforNozzle = pcl036.ISRRequiredforNozzle;
                objPCL036.ISRRequiredforCone = pcl036.ISRRequiredforCone;
                objPCL036.PTMTAfterWelding = pcl036.PTMTAfterWelding;
                objPCL036.PTMTAfterPWHT = pcl036.PTMTAfterPWHT;
                objPCL036.PTMTAfterHydro = pcl036.PTMTAfterHydro;
                objPCL036.RTAfterWelding = pcl036.RTAfterWelding;
                objPCL036.RTAfterOverlay = pcl036.RTAfterOverlay;
                objPCL036.RTAfterPWHT = pcl036.RTAfterPWHT;
                objPCL036.RTAfterHydro = pcl036.RTAfterHydro;
                objPCL036.UTAfterRecordable = pcl036.UTAfterRecordable;
                objPCL036.UTAfterConventional = pcl036.UTAfterConventional;
                objPCL036.UTAfterWelding = pcl036.UTAfterWelding;
                objPCL036.UTAfterOverlay = pcl036.UTAfterOverlay;
                objPCL036.UTAfterPWHT = pcl036.UTAfterPWHT;
                objPCL036.UTAfterHydro = pcl036.UTAfterHydro;
                objPCL036.FerAfterWelding = pcl036.FerAfterWelding;
                objPCL036.FerAfterOverlay = pcl036.FerAfterOverlay;
                objPCL036.FerAfterPWHT = pcl036.FerAfterPWHT;
                objPCL036.FerAfterHydro = pcl036.FerAfterHydro;
                objPCL036.DCPetalWCrown = pcl036.DCPetalWCrown;
                objPCL036.DC4PetalWoCrown = pcl036.DC4PetalWoCrown;
                objPCL036.DCSinglePeice = pcl036.DCSinglePeice;
                objPCL036.DCTwoHalves = pcl036.DCTwoHalves;
                objPCL036.DRCFormedPetals = pcl036.DRCFormedPetals;
                objPCL036.DRCFormedSetup = pcl036.DRCFormedSetup;
                objPCL036.DRCFormedWelded = pcl036.DRCFormedWelded;
                objPCL036.AirRingNos = pcl036.AirRingNos;
                objPCL036.AirRingPlate = pcl036.AirRingPlate;
                objPCL036.AirRingPipe = pcl036.AirRingPipe;
                objPCL036.AirRingFIM = pcl036.AirRingFIM;
                objPCL036.NozzInsNos = pcl036.NozzInsNos;
                objPCL036.NozzInsPWHT = pcl036.NozzInsPWHT;
                objPCL036.NozzInsSep = pcl036.NozzInsSep;
                objPCL036.NozzInsNA = pcl036.NozzInsNA;
                objPCL036.NGFBfPWHT = pcl036.NGFBfPWHT;
                objPCL036.NGFAfPWHT = pcl036.NGFAfPWHT;
                objPCL036.StellNos = pcl036.StellNos;
                objPCL036.StellApp = pcl036.StellApp;
                objPCL036.StellNApp = pcl036.StellNApp;
                objPCL036.StellWeld = pcl036.StellWeld;
                objPCL036.NormAppNos = pcl036.NormAppNos;
                objPCL036.NormAppMTC = pcl036.NormAppMTC;
                objPCL036.NormAppFIx = pcl036.NormAppFIx;
                objPCL036.SSWBfPWHT = pcl036.SSWBfPWHT;
                objPCL036.SSWAfPWHT = pcl036.SSWAfPWHT;
                objPCL036.SSWBoth = pcl036.SSWBoth;
                objPCL036.VessleSSkirt = pcl036.VessleSSkirt;
                objPCL036.VessleSSupp = pcl036.VessleSSupp;
                objPCL036.VessleSSaddle = pcl036.VessleSSaddle;
                objPCL036.SkirtMadein1 = pcl036.SkirtMadein1;
                objPCL036.SkirtMadein2 = pcl036.SkirtMadein2;
                objPCL036.SVJYring = pcl036.SVJYring;
                objPCL036.SVJBuildup = pcl036.SVJBuildup;
                objPCL036.SVJLapJoint = pcl036.SVJLapJoint;
                objPCL036.FPNWeldNuts = pcl036.FPNWeldNuts;
                objPCL036.FPNLeaveRow = pcl036.FPNLeaveRow;
                objPCL036.PWHTLSRSinglePiece = pcl036.PWHTLSRSinglePiece;
                objPCL036.PWHTLSRJoint = pcl036.PWHTLSRJoint;
                objPCL036.PWHTNoSec = pcl036.PWHTNoSec;
                objPCL036.PTCMTCptc = pcl036.PTCMTCptc;
                objPCL036.PTCMTCmtc = pcl036.PTCMTCmtc;
                objPCL036.PTCMTCptcSim = pcl036.PTCMTCptcSim;
                objPCL036.PTCMTCmtcSim = pcl036.PTCMTCmtcSim;
                objPCL036.SaddlesTrans = pcl036.SaddlesTrans;
                objPCL036.SaddlesAddit = pcl036.SaddlesAddit;
                objPCL036.PWHTinFurnacePFS = pcl036.PWHTinFurnacePFS;
                objPCL036.PWHTinFurnaceHFS1 = pcl036.PWHTinFurnaceHFS1;
                objPCL036.PWHTinFurnaceLEMF = pcl036.PWHTinFurnaceLEMF;
                objPCL036.SectionWtMore230 = pcl036.SectionWtMore230;
                objPCL036.SectionWt200To230 = pcl036.SectionWt200To230;
                objPCL036.SectionWt150To200 = pcl036.SectionWt150To200;
                objPCL036.SectionWtLess150 = pcl036.SectionWtLess150;
                objPCL036.SectionWt4Sec = pcl036.SectionWt4Sec;
                objPCL036.SectionWt2Sec = pcl036.SectionWt2Sec;
                objPCL036.SectionWtNoSec = pcl036.SectionWtNoSec;
                objPCL036.SectionWtNoSecVal = pcl036.SectionWtNoSecVal;
                objPCL036.ARMRequired = pcl036.ARMRequired;
                objPCL036.ARMRT = pcl036.ARMRT;
                objPCL036.ARMPWHT = pcl036.ARMPWHT;
                objPCL036.ARMSB = pcl036.ARMSB;
                objPCL036.HWR30ppm = pcl036.HWR30ppm;
                objPCL036.HWR50ppm = pcl036.HWR50ppm;
                objPCL036.HWRNoLim = pcl036.HWRNoLim;
                objPCL036.MaxSBLess1_38 = pcl036.MaxSBLess1_38;
                objPCL036.MaxSBLess1_78 = pcl036.MaxSBLess1_78;
                objPCL036.MaxSBLess2_14 = pcl036.MaxSBLess2_14;
                objPCL036.MaxSBLess4 = pcl036.MaxSBLess4;
                objPCL036.ESTPlate = pcl036.ESTPlate;
                objPCL036.ESTShell = pcl036.ESTShell;
                objPCL036.ESTSection = pcl036.ESTSection;
                objPCL036.ESTEquip = pcl036.ESTEquip;
                objPCL036.ISTPlate = pcl036.ISTPlate;
                objPCL036.ISTShell = pcl036.ISTShell;
                objPCL036.ISTSection = pcl036.ISTSection;
                objPCL036.ISTEquip = pcl036.ISTEquip;
                objPCL036.ISTAfBfPWHT = pcl036.ISTAfBfPWHT;
                objPCL036.ISTBfRefra = pcl036.ISTBfRefra;
                objPCL036.N2FillReq = pcl036.N2FillReq;
                objPCL036.N2FillNotReq = pcl036.N2FillNotReq;
                objPCL036.N2FillOther = pcl036.N2FillOther;
                objPCL036.SaddlesTransVal = pcl036.SaddlesTransVal;
                objPCL036.SaddlesAdditVal = pcl036.SaddlesAdditVal;
                objPCL036.HWR30ppmVal = pcl036.HWR30ppmVal;

                if (pcl036.LineId > 0)
                {
                    objPCL036.EditedBy = objClsLoginInfo.UserName;
                    objPCL036.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL036.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL036.CreatedBy = objClsLoginInfo.UserName;
                    objPCL036.CreatedOn = DateTime.Now;
                    db.PCL036.Add(objPCL036);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL036.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRRAllowance(PCL037 pcl037, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl037.HeaderId).FirstOrDefault();
                PCL037 objPCL037 = new PCL037();
                if (pcl037.LineId > 0)
                {
                    objPCL037 = db.PCL037.Where(x => x.LineId == pcl037.LineId).FirstOrDefault();
                }
                objPCL037.HeaderId = objPCL001.HeaderId;
                objPCL037.Project = objPCL001.Project;
                objPCL037.Document = objPCL001.Document;
                objPCL037.ShellAllwEB1 = pcl037.ShellAllwEB1;
                objPCL037.ShellAllWidth = pcl037.ShellAllWidth;
                objPCL037.ShellAllTrim = pcl037.ShellAllTrim;
                objPCL037.ShrinkageCirc = pcl037.ShrinkageCirc;
                objPCL037.ShringageLong = pcl037.ShringageLong;
                objPCL037.TOvality = pcl037.TOvality;
                objPCL037.TOut = pcl037.TOut;
                objPCL037.TCircum = pcl037.TCircum;
                objPCL037.DEndMcing = pcl037.DEndMcing;
                objPCL037.DEndSeam = pcl037.DEndSeam;
                objPCL037.DEndCirc = pcl037.DEndCirc;
                objPCL037.DEndESSC = pcl037.DEndESSC;
                objPCL037.YRingESSC = pcl037.YRingESSC;
                objPCL037.SkirtTol = pcl037.SkirtTol;
                objPCL037.TOvalityVal = pcl037.TOvalityVal;
                if (pcl037.LineId > 0)
                {
                    objPCL037.EditedBy = objClsLoginInfo.UserName;
                    objPCL037.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL037.CreatedBy = objClsLoginInfo.UserName;
                    objPCL037.CreatedOn = DateTime.Now;
                    db.PCL037.Add(objPCL037);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL037.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRRScopeOfWork(PCL038 pcl038, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl038.HeaderId).FirstOrDefault();
                PCL038 objPCL038 = new PCL038();
                if (pcl038.LineId > 0)
                {
                    objPCL038 = db.PCL038.Where(x => x.LineId == pcl038.LineId).FirstOrDefault();
                }
                objPCL038.HeaderId = objPCL001.HeaderId;
                objPCL038.Project = objPCL001.Project;
                objPCL038.Document = objPCL001.Document;

                objPCL038.EquipmentNotReq = pcl038.EquipmentNotReq;
                objPCL038.EquipmentShop = pcl038.EquipmentShop;
                objPCL038.EquipmentMC = pcl038.EquipmentMC;
                objPCL038.EquipmentLEMF = pcl038.EquipmentLEMF;
                objPCL038.EquipmentRNW = pcl038.EquipmentRNW;
                objPCL038.EquipmentOutside = pcl038.EquipmentOutside;
                objPCL038.DEndFormingNotReq = pcl038.DEndFormingNotReq;
                objPCL038.DEndFormingShop = pcl038.DEndFormingShop;
                objPCL038.DEndFormingMC = pcl038.DEndFormingMC;
                objPCL038.DEndFormingLEMF = pcl038.DEndFormingLEMF;
                objPCL038.DEndFormingRNW = pcl038.DEndFormingRNW;
                objPCL038.DEndFormingOutside = pcl038.DEndFormingOutside;
                objPCL038.DEndSetupNotReq = pcl038.DEndSetupNotReq;
                objPCL038.DEndSetupShop = pcl038.DEndSetupShop;
                objPCL038.DEndSetupMC = pcl038.DEndSetupMC;
                objPCL038.DEndSetupLEMF = pcl038.DEndSetupLEMF;
                objPCL038.DEndSetupRNW = pcl038.DEndSetupRNW;
                objPCL038.DEndSetupOutside = pcl038.DEndSetupOutside;
                objPCL038.DEndWeldingNotReq = pcl038.DEndWeldingNotReq;
                objPCL038.DEndWeldingShop = pcl038.DEndWeldingShop;
                objPCL038.DEndWeldingMC = pcl038.DEndWeldingMC;
                objPCL038.DEndWeldingLEMF = pcl038.DEndWeldingLEMF;
                objPCL038.DEndWeldingRNW = pcl038.DEndWeldingRNW;
                objPCL038.DEndWeldingOutside = pcl038.DEndWeldingOutside;
                objPCL038.SkirtNotReq = pcl038.SkirtNotReq;
                objPCL038.SkirtShop = pcl038.SkirtShop;
                objPCL038.SkirtMC = pcl038.SkirtMC;
                objPCL038.SkirtLEMF = pcl038.SkirtLEMF;
                objPCL038.SkirtRNW = pcl038.SkirtRNW;
                objPCL038.SkirtOutside = pcl038.SkirtOutside;
                objPCL038.SkirtTemplateNotReq = pcl038.SkirtTemplateNotReq;
                objPCL038.SkirtTemplateShop = pcl038.SkirtTemplateShop;
                objPCL038.SkirtTemplateMC = pcl038.SkirtTemplateMC;
                objPCL038.SkirtTemplateLEMF = pcl038.SkirtTemplateLEMF;
                objPCL038.SkirtTemplateRNW = pcl038.SkirtTemplateRNW;
                objPCL038.SkirtTemplateOutside = pcl038.SkirtTemplateOutside;
                objPCL038.NozOverlayNotReq = pcl038.NozOverlayNotReq;
                objPCL038.NozOverlayMC = pcl038.NozOverlayMC;
                objPCL038.NozOverlayShop = pcl038.NozOverlayShop;
                objPCL038.NozOverlayLEMF = pcl038.NozOverlayLEMF;
                objPCL038.NozOverlayRNW = pcl038.NozOverlayRNW;
                objPCL038.NozOverlayOutside = pcl038.NozOverlayOutside;
                objPCL038.NozWEPNotReq = pcl038.NozWEPNotReq;
                objPCL038.NozWEPShop = pcl038.NozWEPShop;
                objPCL038.NozWEPMC = pcl038.NozWEPMC;
                objPCL038.NozWEPLEMF = pcl038.NozWEPLEMF;
                objPCL038.NozWEPRNW = pcl038.NozWEPRNW;
                objPCL038.NozWEPOutside = pcl038.NozWEPOutside;
                objPCL038.LongWEPNotReq = pcl038.LongWEPNotReq;
                objPCL038.LongWEPShop = pcl038.LongWEPShop;
                objPCL038.LongWEPMC = pcl038.LongWEPMC;
                objPCL038.LongWEPLEMF = pcl038.LongWEPLEMF;
                objPCL038.LongWEPRNW = pcl038.LongWEPRNW;
                objPCL038.LongWEPOutside = pcl038.LongWEPOutside;
                objPCL038.CircWEPNotReq = pcl038.CircWEPNotReq;
                objPCL038.CircWEPShop = pcl038.CircWEPShop;
                objPCL038.CircWEPMC = pcl038.CircWEPMC;
                objPCL038.CircWEPLEMF = pcl038.CircWEPLEMF;
                objPCL038.CircWEPRNW = pcl038.CircWEPRNW;
                objPCL038.CircWEPOutside = pcl038.CircWEPOutside;
                objPCL038.ShellRollNotReq = pcl038.ShellRollNotReq;
                objPCL038.ShellRollShop = pcl038.ShellRollShop;
                objPCL038.ShellRollMC = pcl038.ShellRollMC;
                objPCL038.ShellRollLEMF = pcl038.ShellRollLEMF;
                objPCL038.ShellRollRNW = pcl038.ShellRollRNW;
                objPCL038.ShellRollOutside = pcl038.ShellRollOutside;
                objPCL038.ConeRollNotReq = pcl038.ConeRollNotReq;
                objPCL038.ConeRollShop = pcl038.ConeRollShop;
                objPCL038.ConeRollMC = pcl038.ConeRollMC;
                objPCL038.ConeRollLEMF = pcl038.ConeRollLEMF;
                objPCL038.ConeRollRNW = pcl038.ConeRollRNW;
                objPCL038.ConeRollOutside = pcl038.ConeRollOutside;
                objPCL038.CyclFabNotReq = pcl038.CyclFabNotReq;
                objPCL038.CyclFabShop = pcl038.CyclFabShop;
                objPCL038.CyclFabMC = pcl038.CyclFabMC;
                objPCL038.CyclFabLEMF = pcl038.CyclFabLEMF;
                objPCL038.CyclFabRNW = pcl038.CyclFabRNW;
                objPCL038.CyclFabOutside = pcl038.CyclFabOutside;
                objPCL038.CyclAssmNotReq = pcl038.CyclAssmNotReq;
                objPCL038.CyclAssmShop = pcl038.CyclAssmShop;
                objPCL038.CyclAssmMC = pcl038.CyclAssmMC;
                objPCL038.CyclAssmLEMF = pcl038.CyclAssmLEMF;
                objPCL038.CyclAssmRNW = pcl038.CyclAssmRNW;
                objPCL038.CyclAssmOutside = pcl038.CyclAssmOutside;
                objPCL038.PlenumFabNotReq = pcl038.PlenumFabNotReq;
                objPCL038.PlenumFabShop = pcl038.PlenumFabShop;
                objPCL038.PlenumFabMC = pcl038.PlenumFabMC;
                objPCL038.PlenumFabLEMF = pcl038.PlenumFabLEMF;
                objPCL038.PlenumFabRNW = pcl038.PlenumFabRNW;
                objPCL038.PlenumFabOutside = pcl038.PlenumFabOutside;
                objPCL038.IntFabNotReq = pcl038.IntFabNotReq;
                objPCL038.IntFabShop = pcl038.IntFabShop;
                objPCL038.IntFabMC = pcl038.IntFabMC;
                objPCL038.IntFabLEMF = pcl038.IntFabLEMF;
                objPCL038.IntFabRNW = pcl038.IntFabRNW;
                objPCL038.IntFabOutside = pcl038.IntFabOutside;
                objPCL038.WIFNotReq = pcl038.WIFNotReq;
                objPCL038.WIFShop = pcl038.WIFShop;
                objPCL038.WIFMC = pcl038.WIFMC;
                objPCL038.WIFLEMF = pcl038.WIFLEMF;
                objPCL038.WIFRNW = pcl038.WIFRNW;
                objPCL038.WIFOutside = pcl038.WIFOutside;
                objPCL038.WEFNotReq = pcl038.WEFNotReq;
                objPCL038.WEFShop = pcl038.WEFShop;
                objPCL038.WEFMC = pcl038.WEFMC;
                objPCL038.WEFLEMF = pcl038.WEFLEMF;
                objPCL038.WEFRNW = pcl038.WEFRNW;
                objPCL038.WEFOutside = pcl038.WEFOutside;
                objPCL038.RefractNotReq = pcl038.RefractNotReq;
                objPCL038.RefractShop = pcl038.RefractShop;
                objPCL038.RefractMC = pcl038.RefractMC;
                objPCL038.RefractLEMF = pcl038.RefractLEMF;
                objPCL038.RefractRNW = pcl038.RefractRNW;
                objPCL038.RefractOutside = pcl038.RefractOutside;
                objPCL038.CBFabNotReq = pcl038.CBFabNotReq;
                objPCL038.CBFabShop = pcl038.CBFabShop;
                objPCL038.CBFabMC = pcl038.CBFabMC;
                objPCL038.CBFabLEMF = pcl038.CBFabLEMF;
                objPCL038.CBFabRNW = pcl038.CBFabRNW;
                objPCL038.CBFabOutside = pcl038.CBFabOutside;
                objPCL038.ITFabNotReq = pcl038.ITFabNotReq;
                objPCL038.ITFabShop = pcl038.ITFabShop;
                objPCL038.ITFabMC = pcl038.ITFabMC;
                objPCL038.ITFabLEMF = pcl038.ITFabLEMF;
                objPCL038.ITFabRNW = pcl038.ITFabRNW;
                objPCL038.ITFabOutside = pcl038.ITFabOutside;
                objPCL038.IINNotReq = pcl038.IINNotReq;
                objPCL038.IINShop = pcl038.IINShop;
                objPCL038.IINMC = pcl038.IINMC;
                objPCL038.IINLEMF = pcl038.IINLEMF;
                objPCL038.IINRNW = pcl038.IINRNW;
                objPCL038.IINOutside = pcl038.IINOutside;
                objPCL038.SaddlesNotReq = pcl038.SaddlesNotReq;
                objPCL038.SaddlesShop = pcl038.SaddlesShop;
                objPCL038.SaddlesMC = pcl038.SaddlesMC;
                objPCL038.SaddlesLEMF = pcl038.SaddlesLEMF;
                objPCL038.SaddlesRNW = pcl038.SaddlesRNW;
                objPCL038.SaddlesOutside = pcl038.SaddlesOutside;
                objPCL038.PFNotReq = pcl038.PFNotReq;
                objPCL038.PFShop = pcl038.PFShop;
                objPCL038.PFMC = pcl038.PFMC;
                objPCL038.PFLEMF = pcl038.PFLEMF;
                objPCL038.PFRNW = pcl038.PFRNW;
                objPCL038.PFOutside = pcl038.PFOutside;
                objPCL038.PTANotReq = pcl038.PTANotReq;
                objPCL038.PTAShop = pcl038.PTAShop;
                objPCL038.PTAMC = pcl038.PTAMC;
                objPCL038.PTALEMF = pcl038.PTALEMF;
                objPCL038.PTARNW = pcl038.PTARNW;
                objPCL038.PTAOutside = pcl038.PTAOutside;
                objPCL038.PWHTNotReq = pcl038.PWHTNotReq;
                objPCL038.PWHTShop = pcl038.PWHTShop;
                objPCL038.PWHTMC = pcl038.PWHTMC;
                objPCL038.PWHTLEMF = pcl038.PWHTLEMF;
                objPCL038.PWHTRNW = pcl038.PWHTRNW;
                objPCL038.PWHTOutside = pcl038.PWHTOutside;
                objPCL038.InternalInstallationNotReq = pcl038.InternalInstallationNotReq;
                objPCL038.InternalInstallationShop = pcl038.InternalInstallationShop;
                objPCL038.InternalInstallationMC = pcl038.InternalInstallationMC;
                objPCL038.InternalInstallationLEMF = pcl038.InternalInstallationLEMF;
                objPCL038.InternalInstallationRNW = pcl038.InternalInstallationRNW;
                objPCL038.InternalInstallationOutside = pcl038.InternalInstallationOutside;
                objPCL038.N2FillingNotReq = pcl038.N2FillingNotReq;
                objPCL038.N2FillingShop = pcl038.N2FillingShop;
                objPCL038.N2FillingMC = pcl038.N2FillingMC;
                objPCL038.N2FillingLEMF = pcl038.N2FillingLEMF;
                objPCL038.N2FillingRNW = pcl038.N2FillingRNW;
                objPCL038.N2FillingOutside = pcl038.N2FillingOutside;
                objPCL038.SandBlastingNotReq = pcl038.SandBlastingNotReq;
                objPCL038.SandBlastingShop = pcl038.SandBlastingShop;
                objPCL038.SandBlastingMC = pcl038.SandBlastingMC;
                objPCL038.SandBlastingLEMF = pcl038.SandBlastingLEMF;
                objPCL038.SandBlastingRNW = pcl038.SandBlastingRNW;
                objPCL038.SandBlastingOutside = pcl038.SandBlastingOutside;
                objPCL038.PaintingNotReq = pcl038.PaintingNotReq;
                objPCL038.PaintingShop = pcl038.PaintingShop;
                objPCL038.PaintingMC = pcl038.PaintingMC;
                objPCL038.PaintingLEMF = pcl038.PaintingLEMF;
                objPCL038.PaintingRNW = pcl038.PaintingRNW;
                objPCL038.PaintingOutside = pcl038.PaintingOutside;
                objPCL038.AcidCleaningNotReq = pcl038.AcidCleaningNotReq;
                objPCL038.AcidCleaningShop = pcl038.AcidCleaningShop;
                objPCL038.AcidCleaningMC = pcl038.AcidCleaningMC;
                objPCL038.AcidCleaningLEMF = pcl038.AcidCleaningLEMF;
                objPCL038.AcidCleaningRNW = pcl038.AcidCleaningRNW;
                objPCL038.AcidCleaningOutside = pcl038.AcidCleaningOutside;
                objPCL038.WeightMeasurementNotReq = pcl038.WeightMeasurementNotReq;
                objPCL038.WeightMeasurementShop = pcl038.WeightMeasurementShop;
                objPCL038.WeightMeasurementMC = pcl038.WeightMeasurementMC;
                objPCL038.WeightMeasurementLEMF = pcl038.WeightMeasurementLEMF;
                objPCL038.WeightMeasurementRNW = pcl038.WeightMeasurementRNW;
                objPCL038.WeightMeasurementOutside = pcl038.WeightMeasurementOutside;
                objPCL038.ManwayDavitNotReq = pcl038.ManwayDavitNotReq;
                objPCL038.ManwayDavitShop = pcl038.ManwayDavitShop;
                objPCL038.ManwayDavitMC = pcl038.ManwayDavitMC;
                objPCL038.ManwayDavitLEMF = pcl038.ManwayDavitLEMF;
                objPCL038.ManwayDavitRNW = pcl038.ManwayDavitRNW;
                objPCL038.ManwayDavitOutside = pcl038.ManwayDavitOutside;
                objPCL038.VesselDavitNotReq = pcl038.VesselDavitNotReq;
                objPCL038.VesselDavitShop = pcl038.VesselDavitShop;
                objPCL038.VesselDavitMC = pcl038.VesselDavitMC;
                objPCL038.VesselDavitLEMF = pcl038.VesselDavitLEMF;
                objPCL038.VesselDavitRNW = pcl038.VesselDavitRNW;
                objPCL038.VesselDavitOutside = pcl038.VesselDavitOutside;
                objPCL038.LappingRingNotReq = pcl038.LappingRingNotReq;
                objPCL038.LappingRingShop = pcl038.LappingRingShop;
                objPCL038.LappingRingMC = pcl038.LappingRingMC;
                objPCL038.LappingRingLEMF = pcl038.LappingRingLEMF;
                objPCL038.LappingRingRNW = pcl038.LappingRingRNW;
                objPCL038.LappingRingOutside = pcl038.LappingRingOutside;
                objPCL038.ShippingNotReq = pcl038.ShippingNotReq;
                objPCL038.ShippingShop = pcl038.ShippingShop;
                objPCL038.ShippingMC = pcl038.ShippingMC;
                objPCL038.ShippingLEMF = pcl038.ShippingLEMF;
                objPCL038.ShippingRNW = pcl038.ShippingRNW;
                objPCL038.ShippingOutside = pcl038.ShippingOutside;

                if (pcl038.LineId > 0)
                {
                    objPCL038.EditedBy = objClsLoginInfo.UserName;
                    objPCL038.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL038.CreatedBy = objClsLoginInfo.UserName;
                    objPCL038.CreatedOn = DateTime.Now;
                    db.PCL038.Add(objPCL038);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL038.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRRFixture(PCL039 pcl039, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl039.HeaderId).FirstOrDefault();
                PCL039 objPCL039 = new PCL039();
                bool IsEdited = false;
                if (pcl039.LineId > 0)
                {
                    objPCL039 = db.PCL039.Where(x => x.LineId == pcl039.LineId).FirstOrDefault();
                    IsEdited = true;
                }

                objPCL039.HeaderId = objPCL001.HeaderId;
                objPCL039.Project = objPCL001.Project;
                objPCL039.Document = objPCL001.Document;
                objPCL039.TempRollNoFix = pcl039.TempRollNoFix;
                objPCL039.TempRollAvlQty = pcl039.TempRollAvlQty;
                objPCL039.TempRollDrg = pcl039.TempRollDrg;
                objPCL039.TempRollReqDate = pcl039.TempRollReqDate;
                string TempRollReqDate = fc["TempRollReqDate"] != null ? fc["TempRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempRollReqDate))
                {
                    objPCL039.TempRollReqDate = DateTime.ParseExact(TempRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.TempRollMtrl = pcl039.TempRollMtrl;
                objPCL039.TempRollFab = pcl039.TempRollFab;
                objPCL039.TempRollDel = pcl039.TempRollDel;
                objPCL039.GasketCoverReqDate = pcl039.GasketCoverReqDate;
                string GasketCoverReqDate = fc["GasketCoverReqDate"] != null ? fc["GasketCoverReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(GasketCoverReqDate))
                {
                    objPCL039.GasketCoverReqDate = DateTime.ParseExact(GasketCoverReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.GasketCoverNoFix = pcl039.GasketCoverNoFix;
                objPCL039.GasketCoverAvlQty = pcl039.GasketCoverAvlQty;
                objPCL039.GasketCoverDrg = pcl039.GasketCoverDrg;
                objPCL039.GasketCoverMtrl = pcl039.GasketCoverMtrl;
                objPCL039.GasketCoverFab = pcl039.GasketCoverFab;
                objPCL039.GasketCoverDel = pcl039.GasketCoverDel;
                objPCL039.SpiderReqDate = pcl039.SpiderReqDate;
                string SpiderReqDate = fc["SpiderReqDate"] != null ? fc["SpiderReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SpiderReqDate))
                {
                    objPCL039.SpiderReqDate = DateTime.ParseExact(SpiderReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.SpiderNoFix = pcl039.SpiderNoFix;
                objPCL039.SpiderAvlQty = pcl039.SpiderAvlQty;
                objPCL039.SpiderDrg = pcl039.SpiderDrg;
                objPCL039.SpiderMtrl = pcl039.SpiderMtrl;
                objPCL039.SpiderFab = pcl039.SpiderFab;
                objPCL039.SpiderDel = pcl039.SpiderDel;
                objPCL039.FixtureShellReqDate = pcl039.FixtureShellReqDate;
                string FixtureShellReqDate = fc["FixtureShellReqDate"] != null ? fc["FixtureShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixtureShellReqDate))
                {
                    objPCL039.FixtureShellReqDate = DateTime.ParseExact(FixtureShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.FixtureShellNoFix = pcl039.FixtureShellNoFix;
                objPCL039.FixtureShellAvlQty = pcl039.FixtureShellAvlQty;
                objPCL039.FixtureShellDrg = pcl039.FixtureShellDrg;
                objPCL039.FixtureShellMtrl = pcl039.FixtureShellMtrl;
                objPCL039.FixtureShellFab = pcl039.FixtureShellFab;
                objPCL039.FixtureShellDel = pcl039.FixtureShellDel;
                objPCL039.DiePunchReqDate = pcl039.DiePunchReqDate;
                string DiePunchReqDate = fc["DiePunchReqDate"] != null ? fc["DiePunchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DiePunchReqDate))
                {
                    objPCL039.DiePunchReqDate = DateTime.ParseExact(DiePunchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.DiePunchNoFix = pcl039.DiePunchNoFix;
                objPCL039.DiePunchAvlQty = pcl039.DiePunchAvlQty;
                objPCL039.DiePunchDrg = pcl039.DiePunchDrg;
                objPCL039.DiePunchMtrl = pcl039.DiePunchMtrl;
                objPCL039.DiePunchFab = pcl039.DiePunchFab;
                objPCL039.DiePunchDel = pcl039.DiePunchDel;
                objPCL039.SquareBarReqDate = pcl039.SquareBarReqDate;
                string SquareBarReqDate = fc["SquareBarReqDate"] != null ? fc["SquareBarReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SquareBarReqDate))
                {
                    objPCL039.SquareBarReqDate = DateTime.ParseExact(SquareBarReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.SquareBarNoFix = pcl039.SquareBarNoFix;
                objPCL039.SquareBarAvlQty = pcl039.SquareBarAvlQty;
                objPCL039.SquareBarDrg = pcl039.SquareBarDrg;
                objPCL039.SquareBarMtrl = pcl039.SquareBarMtrl;
                objPCL039.SquareBarFab = pcl039.SquareBarFab;
                objPCL039.SquareBarDel = pcl039.SquareBarDel;
                objPCL039.PreHeatReqDate = pcl039.PreHeatReqDate;
                string PreHeatReqDate = fc["PreHeatReqDate"] != null ? fc["PreHeatReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PreHeatReqDate))
                {
                    objPCL039.PreHeatReqDate = DateTime.ParseExact(PreHeatReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PreHeatNoFix = pcl039.PreHeatNoFix;
                objPCL039.PreHeatAvlQty = pcl039.PreHeatAvlQty;
                objPCL039.PreHeatDrg = pcl039.PreHeatDrg;
                objPCL039.PreHeatMtrl = pcl039.PreHeatMtrl;
                objPCL039.PreHeatFab = pcl039.PreHeatFab;
                objPCL039.PreHeatDel = pcl039.PreHeatDel;
                objPCL039.FixCycReqDate = pcl039.FixCycReqDate;
                string FixCycReqDate = fc["FixCycReqDate"] != null ? fc["FixCycReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixCycReqDate))
                {
                    objPCL039.FixCycReqDate = DateTime.ParseExact(FixCycReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.FixCycNoFix = pcl039.FixCycNoFix;
                objPCL039.FixCycAvlQty = pcl039.FixCycAvlQty;
                objPCL039.FixCycDrg = pcl039.FixCycDrg;
                objPCL039.FixCycMtrl = pcl039.FixCycMtrl;
                objPCL039.FixCycFab = pcl039.FixCycFab;
                objPCL039.FixCycDel = pcl039.FixCycDel;
                objPCL039.FixInstReqDate = pcl039.FixInstReqDate;
                string FixInstReqDate = fc["FixInstReqDate"] != null ? fc["FixInstReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixInstReqDate))
                {
                    objPCL039.FixInstReqDate = DateTime.ParseExact(FixInstReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.FixInstNoFix = pcl039.FixInstNoFix;
                objPCL039.FixInstAvlQty = pcl039.FixInstAvlQty;
                objPCL039.FixInstDrg = pcl039.FixInstDrg;
                objPCL039.FixInstMtrl = pcl039.FixInstMtrl;
                objPCL039.FixInstFab = pcl039.FixInstFab;
                objPCL039.FixInstDel = pcl039.FixInstDel;
                objPCL039.EvenerReqDate = pcl039.EvenerReqDate;
                string EvenerReqDate = fc["EvenerReqDate"] != null ? fc["EvenerReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(EvenerReqDate))
                {
                    objPCL039.EvenerReqDate = DateTime.ParseExact(EvenerReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.EvenerNoFix = pcl039.EvenerNoFix;
                objPCL039.EvenerAvlQty = pcl039.EvenerAvlQty;
                objPCL039.EvenerDrg = pcl039.EvenerDrg;
                objPCL039.EvenerMtrl = pcl039.EvenerMtrl;
                objPCL039.EvenerFab = pcl039.EvenerFab;
                objPCL039.EvenerDel = pcl039.EvenerDel;
                objPCL039.ExtRingReqDate = pcl039.ExtRingReqDate;
                string ExtRingReqDate = fc["ExtRingReqDate"] != null ? fc["ExtRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ExtRingReqDate))
                {
                    objPCL039.ExtRingReqDate = DateTime.ParseExact(ExtRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.ExtRingNoFix = pcl039.ExtRingNoFix;
                objPCL039.ExtRingAvlQty = pcl039.ExtRingAvlQty;
                objPCL039.ExtRingDrg = pcl039.ExtRingDrg;
                objPCL039.ExtRingMtrl = pcl039.ExtRingMtrl;
                objPCL039.ExtRingFab = pcl039.ExtRingFab;
                objPCL039.ExtRingDel = pcl039.ExtRingDel;
                objPCL039.FixPWHTReqDate = pcl039.FixPWHTReqDate;
                string FixPWHTReqDate = fc["FixPWHTReqDate"] != null ? fc["FixPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(FixPWHTReqDate))
                {
                    objPCL039.FixPWHTReqDate = DateTime.ParseExact(FixPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.FixPWHTNoFix = pcl039.FixPWHTNoFix;
                objPCL039.FixPWHTAvlQty = pcl039.FixPWHTAvlQty;
                objPCL039.FixPWHTDrg = pcl039.FixPWHTDrg;
                objPCL039.FixPWHTMtrl = pcl039.FixPWHTMtrl;
                objPCL039.FixPWHTFab = pcl039.FixPWHTFab;
                objPCL039.FixPWHTDel = pcl039.FixPWHTDel;
                objPCL039.SaddlePWHTReqDate = pcl039.SaddlePWHTReqDate;
                string SaddlePWHTReqDate = fc["SaddlePWHTReqDate"] != null ? fc["SaddlePWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddlePWHTReqDate))
                {
                    objPCL039.SaddlePWHTReqDate = DateTime.ParseExact(SaddlePWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.SaddlePWHTNoFix = pcl039.SaddlePWHTNoFix;
                objPCL039.SaddlePWHTAvlQty = pcl039.SaddlePWHTAvlQty;
                objPCL039.SaddlePWHTDrg = pcl039.SaddlePWHTDrg;
                objPCL039.SaddlePWHTMtrl = pcl039.SaddlePWHTMtrl;
                objPCL039.SaddlePWHTFab = pcl039.SaddlePWHTFab;
                objPCL039.SaddlePWHTDel = pcl039.SaddlePWHTDel;
                objPCL039.SaddleHydroReqDate = pcl039.SaddleHydroReqDate;
                string SaddleHydroReqDate = fc["SaddleHydroReqDate"] != null ? fc["SaddleHydroReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SaddleHydroReqDate))
                {
                    objPCL039.SaddleHydroReqDate = DateTime.ParseExact(SaddleHydroReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.SaddleHydroNoFix = pcl039.SaddleHydroNoFix;
                objPCL039.SaddleHydroAvlQty = pcl039.SaddleHydroAvlQty;
                objPCL039.SaddleHydroDrg = pcl039.SaddleHydroDrg;
                objPCL039.SaddleHydroMtrl = pcl039.SaddleHydroMtrl;
                objPCL039.SaddleHydroFab = pcl039.SaddleHydroFab;
                objPCL039.SaddleHydroDel = pcl039.SaddleHydroDel;
                objPCL039.LSRFurnaceReqDate = pcl039.LSRFurnaceReqDate;
                string LSRFurnaceReqDate = fc["LSRFurnaceReqDate"] != null ? fc["LSRFurnaceReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRFurnaceReqDate))
                {
                    objPCL039.LSRFurnaceReqDate = DateTime.ParseExact(LSRFurnaceReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.LSRFurnaceNoFix = pcl039.LSRFurnaceNoFix;
                objPCL039.LSRFurnaceAvlQty = pcl039.LSRFurnaceAvlQty;
                objPCL039.LSRFurnaceDrg = pcl039.LSRFurnaceDrg;
                objPCL039.LSRFurnaceMtrl = pcl039.LSRFurnaceMtrl;
                objPCL039.LSRFurnaceFab = pcl039.LSRFurnaceFab;
                objPCL039.LSRFurnaceDel = pcl039.LSRFurnaceDel;
                objPCL039.LSRRingReqDate = pcl039.LSRRingReqDate;
                string LSRRingReqDate = fc["LSRRingReqDate"] != null ? fc["LSRRingReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LSRRingReqDate))
                {
                    objPCL039.LSRRingReqDate = DateTime.ParseExact(LSRRingReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.LSRRingNoFix = pcl039.LSRRingNoFix;
                objPCL039.LSRRingAvlQty = pcl039.LSRRingAvlQty;
                objPCL039.LSRRingDrg = pcl039.LSRRingDrg;
                objPCL039.LSRRingMtrl = pcl039.LSRRingMtrl;
                objPCL039.LSRRingFab = pcl039.LSRRingFab;
                objPCL039.LSRRingDel = pcl039.LSRRingDel;
                objPCL039.BaffleReqDate = pcl039.BaffleReqDate;
                string BaffleReqDate = fc["BaffleReqDate"] != null ? fc["BaffleReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BaffleReqDate))
                {
                    objPCL039.BaffleReqDate = DateTime.ParseExact(BaffleReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.BaffleNoFix = pcl039.BaffleNoFix;
                objPCL039.BaffleAvlQty = pcl039.BaffleAvlQty;
                objPCL039.BaffleDrg = pcl039.BaffleDrg;
                objPCL039.BaffleMtrl = pcl039.BaffleMtrl;
                objPCL039.BaffleFab = pcl039.BaffleFab;
                objPCL039.BaffleDel = pcl039.BaffleDel;
                objPCL039.PlateTrunReqDate = pcl039.PlateTrunReqDate;
                string PlateTrunReqDate = fc["PlateTrunReqDate"] != null ? fc["PlateTrunReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateTrunReqDate))
                {
                    objPCL039.PlateTrunReqDate = DateTime.ParseExact(PlateTrunReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PlateTrunNoFix = pcl039.PlateTrunNoFix;
                objPCL039.PlateTrunAvlQty = pcl039.PlateTrunAvlQty;
                objPCL039.PlateTrunDrg = pcl039.PlateTrunDrg;
                objPCL039.PlateTrunMtrl = pcl039.PlateTrunMtrl;
                objPCL039.PlateTrunFab = pcl039.PlateTrunFab;
                objPCL039.PlateTrunDel = pcl039.PlateTrunDel;
                objPCL039.PipeTrunReqDate = pcl039.PipeTrunReqDate;
                string PipeTrunReqDate = fc["PipeTrunReqDate"] != null ? fc["PipeTrunReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PipeTrunReqDate))
                {
                    objPCL039.PipeTrunReqDate = DateTime.ParseExact(PipeTrunReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PipeTrunNoFix = pcl039.PipeTrunNoFix;
                objPCL039.PipeTrunAvlQty = pcl039.PipeTrunAvlQty;
                objPCL039.PipeTrunDrg = pcl039.PipeTrunDrg;
                objPCL039.PipeTrunMtrl = pcl039.PipeTrunMtrl;
                objPCL039.PipeTrunFab = pcl039.PipeTrunFab;
                objPCL039.PipeTrunDel = pcl039.PipeTrunDel;
                objPCL039.PlateLugsReqDate = pcl039.PlateLugsReqDate;
                string PlateLugsReqDate = fc["PlateLugsReqDate"] != null ? fc["PlateLugsReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateLugsReqDate))
                {
                    objPCL039.PlateLugsReqDate = DateTime.ParseExact(PlateLugsReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PlateLugsNoFix = pcl039.PlateLugsNoFix;
                objPCL039.PlateLugsAvlQty = pcl039.PlateLugsAvlQty;
                objPCL039.PlateLugsDrg = pcl039.PlateLugsDrg;
                objPCL039.PlateLugsMtrl = pcl039.PlateLugsMtrl;
                objPCL039.PlateLugsFab = pcl039.PlateLugsFab;
                objPCL039.PlateLugsDel = pcl039.PlateLugsDel;
                objPCL039.PinGaugesReqDate = pcl039.PinGaugesReqDate;
                string PinGaugesReqDate = fc["PinGaugesReqDate"] != null ? fc["PinGaugesReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PinGaugesReqDate))
                {
                    objPCL039.PinGaugesReqDate = DateTime.ParseExact(PinGaugesReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PinGaugesNoFix = pcl039.PinGaugesNoFix;
                objPCL039.PinGaugesAvlQty = pcl039.PinGaugesAvlQty;
                objPCL039.PinGaugesDrg = pcl039.PinGaugesDrg;
                objPCL039.PinGaugesMtrl = pcl039.PinGaugesMtrl;
                objPCL039.PinGaugesFab = pcl039.PinGaugesFab;
                objPCL039.PinGaugesDel = pcl039.PinGaugesDel;
                objPCL039.PlateCraneReqDate = pcl039.PlateCraneReqDate;
                string PlateCraneReqDate = fc["PlateCraneReqDate"] != null ? fc["PlateCraneReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateCraneReqDate))
                {
                    objPCL039.PlateCraneReqDate = DateTime.ParseExact(PlateCraneReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PlateCraneNoFix = pcl039.PlateCraneNoFix;
                objPCL039.PlateCraneAvlQty = pcl039.PlateCraneAvlQty;
                objPCL039.PlateCraneDrg = pcl039.PlateCraneDrg;
                objPCL039.PlateCraneMtrl = pcl039.PlateCraneMtrl;
                objPCL039.PlateCraneFab = pcl039.PlateCraneFab;
                objPCL039.PlateCraneDel = pcl039.PlateCraneDel;
                objPCL039.PlateHydroReqDate = pcl039.PlateHydroReqDate;
                string PlateHydroReqDate = fc["PlateHydroReqDate"] != null ? fc["PlateHydroReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlateHydroReqDate))
                {
                    objPCL039.PlateHydroReqDate = DateTime.ParseExact(PlateHydroReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PlateHydroNoFix = pcl039.PlateHydroNoFix;
                objPCL039.PlateHydroAvlQty = pcl039.PlateHydroAvlQty;
                objPCL039.PlateHydroDrg = pcl039.PlateHydroDrg;
                objPCL039.PlateHydroMtrl = pcl039.PlateHydroMtrl;
                objPCL039.PlateHydroFab = pcl039.PlateHydroFab;
                objPCL039.PlateHydroDel = pcl039.PlateHydroDel;
                objPCL039.SupFixReqDate = pcl039.SupFixReqDate;
                string SupFixReqDate = fc["SupFixReqDate"] != null ? fc["SupFixReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SupFixReqDate))
                {
                    objPCL039.SupFixReqDate = DateTime.ParseExact(SupFixReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.SupFixNoFix = pcl039.SupFixNoFix;
                objPCL039.SupFixAvlQty = pcl039.SupFixAvlQty;
                objPCL039.SupFixDrg = pcl039.SupFixDrg;
                objPCL039.SupFixMtrl = pcl039.SupFixMtrl;
                objPCL039.SupFixFab = pcl039.SupFixFab;
                objPCL039.SupFixDel = pcl039.SupFixDel;
                objPCL039.PlatformVibReqDate = pcl039.PlatformVibReqDate;
                string PlatformVibReqDate = fc["PlatformVibReqDate"] != null ? fc["PlatformVibReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(PlatformVibReqDate))
                {
                    objPCL039.PlatformVibReqDate = DateTime.ParseExact(PlatformVibReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL039.PlatformVibNoFix = pcl039.PlatformVibNoFix;
                objPCL039.PlatformVibAvlQty = pcl039.PlatformVibAvlQty;
                objPCL039.PlatformVibDrg = pcl039.PlatformVibDrg;
                objPCL039.PlatformVibMtrl = pcl039.PlatformVibMtrl;
                objPCL039.PlatformVibFab = pcl039.PlatformVibFab;
                objPCL039.PlatformVibDel = pcl039.PlatformVibDel;
                if (IsEdited)
                {
                    objPCL039.EditedBy = objClsLoginInfo.UserName;
                    objPCL039.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL039.CreatedBy = objClsLoginInfo.UserName;
                    objPCL039.CreatedOn = DateTime.Now;
                    db.PCL039.Add(objPCL039);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL039.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRRDocument(PCL040 pcl040, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl040.HeaderId).FirstOrDefault();
                PCL040 objPCL040 = new PCL040();
                if (pcl040.LineId > 0)
                {
                    objPCL040 = db.PCL040.Where(x => x.LineId == pcl040.LineId).FirstOrDefault();
                }
                objPCL040.HeaderId = objPCL001.HeaderId;
                objPCL040.Project = objPCL001.Project;
                objPCL040.Document = objPCL001.Document;

                objPCL040.JPPReq = pcl040.JPPReq;
                objPCL040.JPPReqDate = pcl040.JPPReqDate;
                string JPPReqDate = fc["JPPReqDate"] != null ? fc["JPPReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReqDate))
                {
                    objPCL040.JPPReqDate = DateTime.ParseExact(JPPReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL040.JPPDocNo = pcl040.JPPDocNo;
                objPCL040.JPPReleasedOn = pcl040.JPPReleasedOn;
                string JPPReleasedOn = fc["JPPReleasedOn"] != null ? fc["JPPReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(JPPReleasedOn))
                {
                    objPCL040.JPPReleasedOn = DateTime.ParseExact(JPPReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.LineSketchReq = pcl040.LineSketchReq;
                objPCL040.LineSketchReqDate = pcl040.LineSketchReqDate;
                string LineSketchReqDate = fc["LineSketchReqDate"] != null ? fc["LineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineSketchReqDate))
                {
                    objPCL040.LineSketchReqDate = DateTime.ParseExact(LineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.LineSketchDocNo = pcl040.LineSketchDocNo;
                objPCL040.LineSketchReleasedOn = pcl040.LineSketchReleasedOn;
                string LineSketchReleasedOn = fc["LineSketchReleasedOn"] != null ? fc["LineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LineSketchReleasedOn))
                {
                    objPCL040.LineSketchReleasedOn = DateTime.ParseExact(LineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RollingCapaReq = pcl040.RollingCapaReq;
                objPCL040.RollingCapaReqDate = pcl040.RollingCapaReqDate;
                string RollingCapaReqDate = fc["RollingCapaReqDate"] != null ? fc["RollingCapaReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReqDate))
                {
                    objPCL040.RollingCapaReqDate = DateTime.ParseExact(RollingCapaReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RollingCapaDocNo = pcl040.RollingCapaDocNo;
                objPCL040.RollingCapaReleasedOn = pcl040.RollingCapaReleasedOn;
                string RollingCapaReleasedOn = fc["RollingCapaReleasedOn"] != null ? fc["RollingCapaReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollingCapaReleasedOn))
                {
                    objPCL040.RollingCapaReleasedOn = DateTime.ParseExact(RollingCapaReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.ImprovBudgetReq = pcl040.ImprovBudgetReq;
                objPCL040.ImprovBudgetReqDate = pcl040.ImprovBudgetReqDate;
                string ImprovBudgetReqDate = fc["ImprovBudgetReqDate"] != null ? fc["ImprovBudgetReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReqDate))
                {
                    objPCL040.ImprovBudgetReqDate = DateTime.ParseExact(ImprovBudgetReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.ImprovBudgetDocNo = pcl040.ImprovBudgetDocNo;
                objPCL040.ImprovBudgetReleasedOn = pcl040.ImprovBudgetReleasedOn;
                string ImprovBudgetReleasedOn = fc["ImprovBudgetReleasedOn"] != null ? fc["ImprovBudgetReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ImprovBudgetReleasedOn))
                {
                    objPCL040.ImprovBudgetReleasedOn = DateTime.ParseExact(ImprovBudgetReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RCCPBreakUpReq = pcl040.RCCPBreakUpReq;
                objPCL040.RCCPBreakUpReqDate = pcl040.RCCPBreakUpReqDate;
                string RCCPBreakUpReqDate = fc["RCCPBreakUpReqDate"] != null ? fc["RCCPBreakUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReqDate))
                {
                    objPCL040.RCCPBreakUpReqDate = DateTime.ParseExact(RCCPBreakUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RCCPBreakUpDocNo = pcl040.RCCPBreakUpDocNo;
                objPCL040.RCCPBreakUpReleasedOn = pcl040.RCCPBreakUpReleasedOn;
                string RCCPBreakUpReleasedOn = fc["RCCPBreakUpReleasedOn"] != null ? fc["RCCPBreakUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RCCPBreakUpReleasedOn))
                {
                    objPCL040.RCCPBreakUpReleasedOn = DateTime.ParseExact(RCCPBreakUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SchedConcertoReq = pcl040.SchedConcertoReq;
                objPCL040.SchedConcertoReqDate = pcl040.SchedConcertoReqDate;
                string SchedConcertoReqDate = fc["SchedConcertoReqDate"] != null ? fc["SchedConcertoReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReqDate))
                {
                    objPCL040.SchedConcertoReqDate = DateTime.ParseExact(SchedConcertoReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SchedConcertoDocNo = pcl040.SchedConcertoDocNo;
                objPCL040.SchedConcertoReleasedOn = pcl040.SchedConcertoReleasedOn;
                string SchedConcertoReleasedOn = fc["SchedConcertoReleasedOn"] != null ? fc["SchedConcertoReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedConcertoReleasedOn))
                {
                    objPCL040.SchedConcertoReleasedOn = DateTime.ParseExact(SchedConcertoReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SchedExlReq = pcl040.SchedExlReq;
                objPCL040.SchedExlReqDate = pcl040.SchedExlReqDate;
                string SchedExlReqDate = fc["SchedExlReqDate"] != null ? fc["SchedExlReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReqDate))
                {
                    objPCL040.SchedExlReqDate = DateTime.ParseExact(SchedExlReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SchedExlDocNo = pcl040.SchedExlDocNo;
                objPCL040.SchedExlReleasedOn = pcl040.SchedExlReleasedOn;
                string SchedExlReleasedOn = fc["SchedExlReleasedOn"] != null ? fc["SchedExlReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SchedExlReleasedOn))
                {
                    objPCL040.SchedExlReleasedOn = DateTime.ParseExact(SchedExlReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SubcontPlanReq = pcl040.SubcontPlanReq;
                objPCL040.SubcontPlanReqDate = pcl040.SubcontPlanReqDate;
                string SubcontPlanReqDate = fc["SubcontPlanReqDate"] != null ? fc["SubcontPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReqDate))
                {
                    objPCL040.SubcontPlanReqDate = DateTime.ParseExact(SubcontPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SubcontPlanDocNo = pcl040.SubcontPlanDocNo;
                objPCL040.SubcontPlanReleasedOn = pcl040.SubcontPlanReleasedOn;
                string SubcontPlanReleasedOn = fc["SubcontPlanReleasedOn"] != null ? fc["SubcontPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SubcontPlanReleasedOn))
                {
                    objPCL040.SubcontPlanReleasedOn = DateTime.ParseExact(SubcontPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.LongSeamReq = pcl040.LongSeamReq;
                objPCL040.LongSeamReqDate = pcl040.LongSeamReqDate;
                string LongSeamReqDate = fc["LongSeamReqDate"] != null ? fc["LongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReqDate))
                {
                    objPCL040.LongSeamReqDate = DateTime.ParseExact(LongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.LongSeamDocNo = pcl040.LongSeamDocNo;
                objPCL040.LongSeamReleasedOn = pcl040.LongSeamReleasedOn;
                string LongSeamReleasedOn = fc["LongSeamReleasedOn"] != null ? fc["LongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LongSeamReleasedOn))
                {
                    objPCL040.LongSeamReleasedOn = DateTime.ParseExact(LongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.CircSeamReq = pcl040.CircSeamReq;
                objPCL040.CircSeamReqDate = pcl040.CircSeamReqDate;
                string CircSeamReqDate = fc["CircSeamReqDate"] != null ? fc["CircSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReqDate))
                {
                    objPCL040.CircSeamReqDate = DateTime.ParseExact(CircSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.CircSeamDocNo = pcl040.CircSeamDocNo;
                objPCL040.CircSeamReleasedOn = pcl040.CircSeamReleasedOn;
                string CircSeamReleasedOn = fc["CircSeamReleasedOn"] != null ? fc["CircSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CircSeamReleasedOn))
                {
                    objPCL040.CircSeamReleasedOn = DateTime.ParseExact(CircSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.NubBuildReq = pcl040.NubBuildReq;
                objPCL040.NubBuildReqDate = pcl040.NubBuildReqDate;
                string NubBuildReqDate = fc["NubBuildReqDate"] != null ? fc["NubBuildReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NubBuildReqDate))
                {
                    objPCL040.NubBuildReqDate = DateTime.ParseExact(NubBuildReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.NubBuildDocNo = pcl040.NubBuildDocNo;
                objPCL040.NubBuildReleasedOn = pcl040.NubBuildReleasedOn;
                string NubBuildReleasedOn = fc["NubBuildReleasedOn"] != null ? fc["NubBuildReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NubBuildReleasedOn))
                {
                    objPCL040.NubBuildReleasedOn = DateTime.ParseExact(NubBuildReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.NozzleShellReq = pcl040.NozzleShellReq;
                objPCL040.NozzleShellReqDate = pcl040.NozzleShellReqDate;
                string NozzleShellReqDate = fc["NozzleShellReqDate"] != null ? fc["NozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReqDate))
                {
                    objPCL040.NozzleShellReqDate = DateTime.ParseExact(NozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.NozzleShellDocNo = pcl040.NozzleShellDocNo;
                objPCL040.NozzleShellReleasedOn = pcl040.NozzleShellReleasedOn;
                string NozzleShellReleasedOn = fc["NozzleShellReleasedOn"] != null ? fc["NozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(NozzleShellReleasedOn))
                {
                    objPCL040.NozzleShellReleasedOn = DateTime.ParseExact(NozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RollCapaReReq = pcl040.RollCapaReReq;
                objPCL040.RollCapaReReqDate = pcl040.RollCapaReReqDate;
                string RollCapaReReqDate = fc["RollCapaReReqDate"] != null ? fc["RollCapaReReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReReqDate))
                {
                    objPCL040.RollCapaReReqDate = DateTime.ParseExact(RollCapaReReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RollCapaReDocNo = pcl040.RollCapaReDocNo;
                objPCL040.RollCapaReReleasedOn = pcl040.RollCapaReReleasedOn;
                string RollCapaReReleasedOn = fc["RollCapaReReleasedOn"] != null ? fc["RollCapaReReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollCapaReReleasedOn))
                {
                    objPCL040.RollCapaReReleasedOn = DateTime.ParseExact(RollCapaReReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TablesNozReq = pcl040.TablesNozReq;
                objPCL040.TablesNozReqDate = pcl040.TablesNozReqDate;
                string TablesNozReqDate = fc["TablesNozReqDate"] != null ? fc["TablesNozReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReqDate))
                {
                    objPCL040.TablesNozReqDate = DateTime.ParseExact(TablesNozReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TablesNozDocNo = pcl040.TablesNozDocNo;
                objPCL040.TablesNozReleasedOn = pcl040.TablesNozReleasedOn;
                string TablesNozReleasedOn = fc["TablesNozReleasedOn"] != null ? fc["TablesNozReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TablesNozReleasedOn))
                {
                    objPCL040.TablesNozReleasedOn = DateTime.ParseExact(TablesNozReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TableIntReq = pcl040.TableIntReq;
                objPCL040.TableIntReqDate = pcl040.TableIntReqDate;
                string TableIntReqDate = fc["TableIntReqDate"] != null ? fc["TableIntReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReqDate))
                {
                    objPCL040.TableIntReqDate = DateTime.ParseExact(TableIntReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TableIntDocNo = pcl040.TableIntDocNo;
                objPCL040.TableIntReleasedOn = pcl040.TableIntReleasedOn;
                string TableIntReleasedOn = fc["TableIntReleasedOn"] != null ? fc["TableIntReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableIntReleasedOn))
                {
                    objPCL040.TableIntReleasedOn = DateTime.ParseExact(TableIntReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TableExtReq = pcl040.TableExtReq;
                objPCL040.TableExtReqDate = pcl040.TableExtReqDate;
                string TableExtReqDate = fc["TableExtReqDate"] != null ? fc["TableExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReqDate))
                {
                    objPCL040.TableExtReqDate = DateTime.ParseExact(TableExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TableExtDocNo = pcl040.TableExtDocNo;
                objPCL040.TableExtReleasedOn = pcl040.TableExtReleasedOn;
                string TableExtReleasedOn = fc["TableExtReleasedOn"] != null ? fc["TableExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TableExtReleasedOn))
                {
                    objPCL040.TableExtReleasedOn = DateTime.ParseExact(TableExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTREdgeReq = pcl040.HTREdgeReq;
                objPCL040.HTREdgeReqDate = pcl040.HTREdgeReqDate;
                string HTREdgeReqDate = fc["HTREdgeReqDate"] != null ? fc["HTREdgeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReqDate))
                {
                    objPCL040.HTREdgeReqDate = DateTime.ParseExact(HTREdgeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTREdgeDocNo = pcl040.HTREdgeDocNo;
                objPCL040.HTREdgeReleasedOn = pcl040.HTREdgeReleasedOn;
                string HTREdgeReleasedOn = fc["HTREdgeReleasedOn"] != null ? fc["HTREdgeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTREdgeReleasedOn))
                {
                    objPCL040.HTREdgeReleasedOn = DateTime.ParseExact(HTREdgeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRRollReq = pcl040.HTRRollReq;
                objPCL040.HTRRollReqDate = pcl040.HTRRollReqDate;
                string HTRRollReqDate = fc["HTRRollReqDate"] != null ? fc["HTRRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReqDate))
                {
                    objPCL040.HTRRollReqDate = DateTime.ParseExact(HTRRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRRollDocNo = pcl040.HTRRollDocNo;
                objPCL040.HTRRollReleasedOn = pcl040.HTRRollReleasedOn;
                string HTRRollReleasedOn = fc["HTRRollReleasedOn"] != null ? fc["HTRRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRRollReleasedOn))
                {
                    objPCL040.HTRRollReleasedOn = DateTime.ParseExact(HTRRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRReRollReq = pcl040.HTRReRollReq;
                objPCL040.HTRReRollReqDate = pcl040.HTRReRollReqDate;
                string HTRReRollReqDate = fc["HTRReRollReqDate"] != null ? fc["HTRReRollReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReqDate))
                {
                    objPCL040.HTRReRollReqDate = DateTime.ParseExact(HTRReRollReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRReRollDocNo = pcl040.HTRReRollDocNo;
                objPCL040.HTRReRollReleasedOn = pcl040.HTRReRollReleasedOn;
                string HTRReRollReleasedOn = fc["HTRReRollReleasedOn"] != null ? fc["HTRReRollReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRReRollReleasedOn))
                {
                    objPCL040.HTRReRollReleasedOn = DateTime.ParseExact(HTRReRollReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RefLineSketchReq = pcl040.RefLineSketchReq;
                objPCL040.RefLineSketchReqDate = pcl040.RefLineSketchReqDate;
                string RefLineSketchReqDate = fc["RefLineSketchReqDate"] != null ? fc["RefLineSketchReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReqDate))
                {
                    objPCL040.RefLineSketchReqDate = DateTime.ParseExact(RefLineSketchReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.RefLineSketchDocNo = pcl040.RefLineSketchDocNo;
                objPCL040.RefLineSketchReleasedOn = pcl040.RefLineSketchReleasedOn;
                string RefLineSketchReleasedOn = fc["RefLineSketchReleasedOn"] != null ? fc["RefLineSketchReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RefLineSketchReleasedOn))
                {
                    objPCL040.RefLineSketchReleasedOn = DateTime.ParseExact(RefLineSketchReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRISRNozzleShellReq = pcl040.HTRISRNozzleShellReq;
                objPCL040.HTRISRNozzleShellReqDate = pcl040.HTRISRNozzleShellReqDate;
                string HTRISRNozzleShellReqDate = fc["HTRISRNozzleShellReqDate"] != null ? fc["HTRISRNozzleShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReqDate))
                {
                    objPCL040.HTRISRNozzleShellReqDate = DateTime.ParseExact(HTRISRNozzleShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRISRNozzleShellDocNo = pcl040.HTRISRNozzleShellDocNo;
                objPCL040.HTRISRNozzleShellReleasedOn = pcl040.HTRISRNozzleShellReleasedOn;
                string HTRISRNozzleShellReleasedOn = fc["HTRISRNozzleShellReleasedOn"] != null ? fc["HTRISRNozzleShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleShellReleasedOn))
                {
                    objPCL040.HTRISRNozzleShellReleasedOn = DateTime.ParseExact(HTRISRNozzleShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRISRNozzleDEndReq = pcl040.HTRISRNozzleDEndReq;
                objPCL040.HTRISRNozzleDEndReqDate = pcl040.HTRISRNozzleDEndReqDate;
                string HTRISRNozzleDEndReqDate = fc["HTRISRNozzleDEndReqDate"] != null ? fc["HTRISRNozzleDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReqDate))
                {
                    objPCL040.HTRISRNozzleDEndReqDate = DateTime.ParseExact(HTRISRNozzleDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRISRNozzleDEndDocNo = pcl040.HTRISRNozzleDEndDocNo;
                objPCL040.HTRISRNozzleDEndReleasedOn = pcl040.HTRISRNozzleDEndReleasedOn;
                string HTRISRNozzleDEndReleasedOn = fc["HTRISRNozzleDEndReleasedOn"] != null ? fc["HTRISRNozzleDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNozzleDEndReleasedOn))
                {
                    objPCL040.HTRISRNozzleDEndReleasedOn = DateTime.ParseExact(HTRISRNozzleDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRISRNubBuildUpReq = pcl040.HTRISRNubBuildUpReq;
                objPCL040.HTRISRNubBuildUpReqDate = pcl040.HTRISRNubBuildUpReqDate;
                string HTRISRNubBuildUpReqDate = fc["HTRISRNubBuildUpReqDate"] != null ? fc["HTRISRNubBuildUpReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReqDate))
                {
                    objPCL040.HTRISRNubBuildUpReqDate = DateTime.ParseExact(HTRISRNubBuildUpReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRISRNubBuildUpDocNo = pcl040.HTRISRNubBuildUpDocNo;
                objPCL040.HTRISRNubBuildUpReleasedOn = pcl040.HTRISRNubBuildUpReleasedOn;
                string HTRISRNubBuildUpReleasedOn = fc["HTRISRNubBuildUpReleasedOn"] != null ? fc["HTRISRNubBuildUpReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRISRNubBuildUpReleasedOn))
                {
                    objPCL040.HTRISRNubBuildUpReleasedOn = DateTime.ParseExact(HTRISRNubBuildUpReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRPWHTReq = pcl040.HTRPWHTReq;
                objPCL040.HTRPWHTReqDate = pcl040.HTRPWHTReqDate;
                string HTRPWHTReqDate = fc["HTRPWHTReqDate"] != null ? fc["HTRPWHTReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReqDate))
                {
                    objPCL040.HTRPWHTReqDate = DateTime.ParseExact(HTRPWHTReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRPWHTDocNo = pcl040.HTRPWHTDocNo;
                objPCL040.HTRPWHTReleasedOn = pcl040.HTRPWHTReleasedOn;
                string HTRPWHTReleasedOn = fc["HTRPWHTReleasedOn"] != null ? fc["HTRPWHTReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRPWHTReleasedOn))
                {
                    objPCL040.HTRPWHTReleasedOn = DateTime.ParseExact(HTRPWHTReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRLSRReq = pcl040.HTRLSRReq;
                objPCL040.HTRLSRReqDate = pcl040.HTRLSRReqDate;
                string HTRLSRReqDate = fc["HTRLSRReqDate"] != null ? fc["HTRLSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReqDate))
                {
                    objPCL040.HTRLSRReqDate = DateTime.ParseExact(HTRLSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HTRLSRDocNo = pcl040.HTRLSRDocNo;
                objPCL040.HTRLSRReleasedOn = pcl040.HTRLSRReleasedOn;
                string HTRLSRReleasedOn = fc["HTRLSRReleasedOn"] != null ? fc["HTRLSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HTRLSRReleasedOn))
                {
                    objPCL040.HTRLSRReleasedOn = DateTime.ParseExact(HTRLSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HandlingPlanReq = pcl040.HandlingPlanReq;
                objPCL040.HandlingPlanReqDate = pcl040.HandlingPlanReqDate;
                string HandlingPlanReqDate = fc["HandlingPlanReqDate"] != null ? fc["HandlingPlanReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReqDate))
                {
                    objPCL040.HandlingPlanReqDate = DateTime.ParseExact(HandlingPlanReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HandlingPlanDocNo = pcl040.HandlingPlanDocNo;
                objPCL040.HandlingPlanReleasedOn = pcl040.HandlingPlanReleasedOn;
                string HandlingPlanReleasedOn = fc["HandlingPlanReleasedOn"] != null ? fc["HandlingPlanReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HandlingPlanReleasedOn))
                {
                    objPCL040.HandlingPlanReleasedOn = DateTime.ParseExact(HandlingPlanReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.LocationTankRotatorReq = pcl040.LocationTankRotatorReq;
                objPCL040.LocationTankRotatorReqDate = pcl040.LocationTankRotatorReqDate;
                string LocationTankRotatorReqDate = fc["LocationTankRotatorReqDate"] != null ? fc["LocationTankRotatorReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReqDate))
                {
                    objPCL040.LocationTankRotatorReqDate = DateTime.ParseExact(LocationTankRotatorReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.LocationTankRotatorDocNo = pcl040.LocationTankRotatorDocNo;
                objPCL040.LocationTankRotatorReleasedOn = pcl040.LocationTankRotatorReleasedOn;
                string LocationTankRotatorReleasedOn = fc["LocationTankRotatorReleasedOn"] != null ? fc["LocationTankRotatorReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(LocationTankRotatorReleasedOn))
                {
                    objPCL040.LocationTankRotatorReleasedOn = DateTime.ParseExact(LocationTankRotatorReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HydrotestReq = pcl040.HydrotestReq;
                objPCL040.HydrotestReqDate = pcl040.HydrotestReqDate;
                string HydrotestReqDate = fc["HydrotestReqDate"] != null ? fc["HydrotestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReqDate))
                {
                    objPCL040.HydrotestReqDate = DateTime.ParseExact(HydrotestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.HydrotestDocNo = pcl040.HydrotestDocNo;
                objPCL040.HydrotestReleasedOn = pcl040.HydrotestReleasedOn;
                string HydrotestReleasedOn = fc["HydrotestReleasedOn"] != null ? fc["HydrotestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(HydrotestReleasedOn))
                {
                    objPCL040.HydrotestReleasedOn = DateTime.ParseExact(HydrotestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TubeReq = pcl040.TubeReq;
                objPCL040.TubeReqDate = pcl040.TubeReqDate;
                string TubeReqDate = fc["TubeReqDate"] != null ? fc["TubeReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeReqDate))
                {
                    objPCL040.TubeReqDate = DateTime.ParseExact(TubeReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.TubeDocNo = pcl040.TubeDocNo;
                objPCL040.TubeReleasedOn = pcl040.TubeReleasedOn;
                string TubeReleasedOn = fc["TubeReleasedOn"] != null ? fc["TubeReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeReleasedOn))
                {
                    objPCL040.TubeReleasedOn = DateTime.ParseExact(TubeReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.BaffleReq = pcl040.BaffleReq;
                objPCL040.BaffleReqDate = pcl040.BaffleReqDate;
                string BaffleReqDate = fc["BaffleReqDate"] != null ? fc["BaffleReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BaffleReqDate))
                {
                    objPCL040.BaffleReqDate = DateTime.ParseExact(BaffleReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.BaffleDocNo = pcl040.BaffleDocNo;
                objPCL040.BaffleReleasedOn = pcl040.BaffleReleasedOn;
                string BaffleReleasedOn = fc["BaffleReleasedOn"] != null ? fc["BaffleReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(BaffleReleasedOn))
                {
                    objPCL040.BaffleReleasedOn = DateTime.ParseExact(BaffleReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SurfaceTreatRequestReq = pcl040.SurfaceTreatRequestReq;
                objPCL040.SurfaceTreatRequestReqDate = pcl040.SurfaceTreatRequestReqDate;
                string SurfaceTreatRequestReqDate = fc["SurfaceTreatRequestReqDate"] != null ? fc["SurfaceTreatRequestReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReqDate))
                {
                    objPCL040.SurfaceTreatRequestReqDate = DateTime.ParseExact(SurfaceTreatRequestReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL040.SurfaceTreatRequestDocNo = pcl040.SurfaceTreatRequestDocNo;
                objPCL040.SurfaceTreatRequestReleasedOn = pcl040.SurfaceTreatRequestReleasedOn;
                string SurfaceTreatRequestReleasedOn = fc["SurfaceTreatRequestReleasedOn"] != null ? fc["SurfaceTreatRequestReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(SurfaceTreatRequestReleasedOn))
                {
                    objPCL040.SurfaceTreatRequestReleasedOn = DateTime.ParseExact(SurfaceTreatRequestReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                if (pcl040.LineId > 0)
                {
                    objPCL040.EditedBy = objClsLoginInfo.UserName;
                    objPCL040.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL040.CreatedBy = objClsLoginInfo.UserName;
                    objPCL040.CreatedOn = DateTime.Now;
                    db.PCL040.Add(objPCL040);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL040.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRRDim(PCL041 pcl041, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl041.HeaderId).FirstOrDefault();
                PCL041 objPCL041 = new PCL041();
                if (pcl041.LineId > 0)
                {
                    objPCL041 = db.PCL041.Where(x => x.LineId == pcl041.LineId).FirstOrDefault();
                }
                objPCL041.HeaderId = objPCL001.HeaderId;
                objPCL041.Project = objPCL001.Project;
                objPCL041.Document = objPCL001.Document;

                objPCL041.RollDataReq = pcl041.RollDataReq;
                objPCL041.RollDataReqDate = pcl041.RollDataReqDate;
                string RollDataReqDate = fc["RollDataReqDate"] != null ? fc["RollDataReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReqDate))
                {
                    objPCL041.RollDataReqDate = DateTime.ParseExact(RollDataReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.RollDataDocNo = pcl041.RollDataDocNo;
                objPCL041.RollDataReleasedOn = pcl041.RollDataReleasedOn;
                string RollDataReleasedOn = fc["RollDataReleasedOn"] != null ? fc["RollDataReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(RollDataReleasedOn))
                {
                    objPCL041.RollDataReleasedOn = DateTime.ParseExact(RollDataReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ShellLongReq = pcl041.ShellLongReq;
                objPCL041.ShellLongReqDate = pcl041.ShellLongReqDate;
                string ShellLongReqDate = fc["ShellLongReqDate"] != null ? fc["ShellLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReqDate))
                {
                    objPCL041.ShellLongReqDate = DateTime.ParseExact(ShellLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ShellLongDocNo = pcl041.ShellLongDocNo;
                objPCL041.ShellLongReleasedOn = pcl041.ShellLongReleasedOn;
                string ShellLongReleasedOn = fc["ShellLongReleasedOn"] != null ? fc["ShellLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShellLongReleasedOn))
                {
                    objPCL041.ShellLongReleasedOn = DateTime.ParseExact(ShellLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ESSCShellReq = pcl041.ESSCShellReq;
                objPCL041.ESSCShellReqDate = pcl041.ESSCShellReqDate;
                string ESSCShellReqDate = fc["ESSCShellReqDate"] != null ? fc["ESSCShellReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReqDate))
                {
                    objPCL041.ESSCShellReqDate = DateTime.ParseExact(ESSCShellReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ESSCShellDocNo = pcl041.ESSCShellDocNo;
                objPCL041.ESSCShellReleasedOn = pcl041.ESSCShellReleasedOn;
                string ESSCShellReleasedOn = fc["ESSCShellReleasedOn"] != null ? fc["ESSCShellReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCShellReleasedOn))
                {
                    objPCL041.ESSCShellReleasedOn = DateTime.ParseExact(ESSCShellReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ESSCDEndReq = pcl041.ESSCDEndReq;
                objPCL041.ESSCDEndReqDate = pcl041.ESSCDEndReqDate;
                string ESSCDEndReqDate = fc["ESSCDEndReqDate"] != null ? fc["ESSCDEndReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCDEndReqDate))
                {
                    objPCL041.ESSCDEndReqDate = DateTime.ParseExact(ESSCDEndReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ESSCDEndDocNo = pcl041.ESSCDEndDocNo;
                objPCL041.ESSCDEndReleasedOn = pcl041.ESSCDEndReleasedOn;
                string ESSCDEndReleasedOn = fc["ESSCDEndReleasedOn"] != null ? fc["ESSCDEndReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ESSCDEndReleasedOn))
                {
                    objPCL041.ESSCDEndReleasedOn = DateTime.ParseExact(ESSCDEndReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ChangeOvalityExtReq = pcl041.ChangeOvalityExtReq;
                objPCL041.ChangeOvalityExtReqDate = pcl041.ChangeOvalityExtReqDate;
                string ChangeOvalityExtReqDate = fc["ChangeOvalityExtReqDate"] != null ? fc["ChangeOvalityExtReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReqDate))
                {
                    objPCL041.ChangeOvalityExtReqDate = DateTime.ParseExact(ChangeOvalityExtReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ChangeOvalityExtDocNo = pcl041.ChangeOvalityExtDocNo;
                objPCL041.ChangeOvalityExtReleasedOn = pcl041.ChangeOvalityExtReleasedOn;
                string ChangeOvalityExtReleasedOn = fc["ChangeOvalityExtReleasedOn"] != null ? fc["ChangeOvalityExtReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityExtReleasedOn))
                {
                    objPCL041.ChangeOvalityExtReleasedOn = DateTime.ParseExact(ChangeOvalityExtReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ChangeOvalityNubReq = pcl041.ChangeOvalityNubReq;
                objPCL041.ChangeOvalityNubReqDate = pcl041.ChangeOvalityNubReqDate;
                string ChangeOvalityNubReqDate = fc["ChangeOvalityNubReqDate"] != null ? fc["ChangeOvalityNubReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityNubReqDate))
                {
                    objPCL041.ChangeOvalityNubReqDate = DateTime.ParseExact(ChangeOvalityNubReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ChangeOvalityNubDocNo = pcl041.ChangeOvalityNubDocNo;
                objPCL041.ChangeOvalityNubReleasedOn = pcl041.ChangeOvalityNubReleasedOn;
                string ChangeOvalityNubReleasedOn = fc["ChangeOvalityNubReleasedOn"] != null ? fc["ChangeOvalityNubReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityNubReleasedOn))
                {
                    objPCL041.ChangeOvalityNubReleasedOn = DateTime.ParseExact(ChangeOvalityNubReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ShrinkTSRReq = pcl041.ShrinkTSRReq;
                objPCL041.ShrinkTSRReqDate = pcl041.ShrinkTSRReqDate;
                string ShrinkTSRReqDate = fc["ShrinkTSRReqDate"] != null ? fc["ShrinkTSRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReqDate))
                {
                    objPCL041.ShrinkTSRReqDate = DateTime.ParseExact(ShrinkTSRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ShrinkTSRDocNo = pcl041.ShrinkTSRDocNo;
                objPCL041.ShrinkTSRReleasedOn = pcl041.ShrinkTSRReleasedOn;
                string ShrinkTSRReleasedOn = fc["ShrinkTSRReleasedOn"] != null ? fc["ShrinkTSRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ShrinkTSRReleasedOn))
                {
                    objPCL041.ShrinkTSRReleasedOn = DateTime.ParseExact(ShrinkTSRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ChangeOvalityISRReq = pcl041.ChangeOvalityISRReq;
                objPCL041.ChangeOvalityISRReqDate = pcl041.ChangeOvalityISRReqDate;
                string ChangeOvalityISRReqDate = fc["ChangeOvalityISRReqDate"] != null ? fc["ChangeOvalityISRReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReqDate))
                {
                    objPCL041.ChangeOvalityISRReqDate = DateTime.ParseExact(ChangeOvalityISRReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.ChangeOvalityISRDocNo = pcl041.ChangeOvalityISRDocNo;
                objPCL041.ChangeOvalityISRReleasedOn = pcl041.ChangeOvalityISRReleasedOn;
                string ChangeOvalityISRReleasedOn = fc["ChangeOvalityISRReleasedOn"] != null ? fc["ChangeOvalityISRReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ChangeOvalityISRReleasedOn))
                {
                    objPCL041.ChangeOvalityISRReleasedOn = DateTime.ParseExact(ChangeOvalityISRReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.DEendLongSeamReq = pcl041.DEendLongSeamReq;
                objPCL041.DEendLongSeamReqDate = pcl041.DEendLongSeamReqDate;
                string DEendLongSeamReqDate = fc["DEendLongSeamReqDate"] != null ? fc["DEendLongSeamReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReqDate))
                {
                    objPCL041.DEendLongSeamReqDate = DateTime.ParseExact(DEendLongSeamReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.DEendLongSeamDocNo = pcl041.DEendLongSeamDocNo;
                objPCL041.DEendLongSeamReleasedOn = pcl041.DEendLongSeamReleasedOn;
                string DEendLongSeamReleasedOn = fc["DEendLongSeamReleasedOn"] != null ? fc["DEendLongSeamReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(DEendLongSeamReleasedOn))
                {
                    objPCL041.DEendLongSeamReleasedOn = DateTime.ParseExact(DEendLongSeamReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TubeLongReq = pcl041.TubeLongReq;
                objPCL041.TubeLongReqDate = pcl041.TubeLongReqDate;
                string TubeLongReqDate = fc["TubeLongReqDate"] != null ? fc["TubeLongReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeLongReqDate))
                {
                    objPCL041.TubeLongReqDate = DateTime.ParseExact(TubeLongReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TubeLongDocNo = pcl041.TubeLongDocNo;
                objPCL041.TubeLongReleasedOn = pcl041.TubeLongReleasedOn;
                string TubeLongReleasedOn = fc["TubeLongReleasedOn"] != null ? fc["TubeLongReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeLongReleasedOn))
                {
                    objPCL041.TubeLongReleasedOn = DateTime.ParseExact(TubeLongReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TubeESSCReq = pcl041.TubeESSCReq;
                objPCL041.TubeESSCReqDate = pcl041.TubeESSCReqDate;
                string TubeESSCReqDate = fc["TubeESSCReqDate"] != null ? fc["TubeESSCReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeESSCReqDate))
                {
                    objPCL041.TubeESSCReqDate = DateTime.ParseExact(TubeESSCReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TubeESSCDocNo = pcl041.TubeESSCDocNo;
                objPCL041.TubeESSCReleasedOn = pcl041.TubeESSCReleasedOn;
                string TubeESSCReleasedOn = fc["TubeESSCReleasedOn"] != null ? fc["TubeESSCReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeESSCReleasedOn))
                {
                    objPCL041.TubeESSCReleasedOn = DateTime.ParseExact(TubeESSCReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TubeFlexReq = pcl041.TubeFlexReq;
                objPCL041.TubeFlexReqDate = pcl041.TubeFlexReqDate;
                string TubeFlexReqDate = fc["TubeFlexReqDate"] != null ? fc["TubeFlexReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeFlexReqDate))
                {
                    objPCL041.TubeFlexReqDate = DateTime.ParseExact(TubeFlexReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TubeFlexDocNo = pcl041.TubeFlexDocNo;
                objPCL041.TubeFlexReleasedOn = pcl041.TubeFlexReleasedOn;
                string TubeFlexReleasedOn = fc["TubeFlexReleasedOn"] != null ? fc["TubeFlexReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TubeFlexReleasedOn))
                {
                    objPCL041.TubeFlexReleasedOn = DateTime.ParseExact(TubeFlexReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TempratureReleasedOn = pcl041.TempratureReleasedOn;
                string TempratureReleasedOn = fc["TempratureReleasedOn"] != null ? fc["TempratureReleasedOn"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempratureReleasedOn))
                {
                    objPCL041.TempratureReleasedOn = DateTime.ParseExact(TempratureReleasedOn, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TempratureReq = pcl041.TempratureReq;

                objPCL041.TempratureReqDate = pcl041.TempratureReqDate;
                string TempratureReqDate = fc["TempratureReqDate"] != null ? fc["TempratureReqDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(TempratureReqDate))
                {
                    objPCL041.TempratureReqDate = DateTime.ParseExact(TempratureReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                objPCL041.TempratureDocNo = pcl041.TempratureDocNo;

                if (pcl041.LineId > 0)
                {
                    objPCL041.EditedBy = objClsLoginInfo.UserName;
                    objPCL041.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL041.CreatedBy = objClsLoginInfo.UserName;
                    objPCL041.CreatedOn = DateTime.Now;
                    db.PCL041.Add(objPCL041);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
                objResponseMsg.HeaderID = objPCL001.HeaderId;
                objResponseMsg.LineID = objPCL041.LineId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Heater
        [HttpPost]
        public ActionResult SaveHSCCategory(PCL017 pcl017, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl017.HeaderId).FirstOrDefault();
                PCL017 objPCL017 = new PCL017();
                if (pcl017.LineId > 0)
                {
                    objPCL017 = db.PCL017.Where(x => x.LineId == pcl017.LineId).FirstOrDefault();
                }
                objPCL017.HeaderId = objPCL001.HeaderId;
                objPCL017.Project = objPCL001.Project;
                objPCL017.Document = objPCL001.Document;
                objPCL017.CSRDate = pcl017.CSRDate;
                string CSRDate = fc["CSRDate"] != null ? fc["CSRDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(CSRDate))
                {
                    objPCL017.CSRDate = DateTime.ParseExact(CSRDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                string KOMDate = fc["KOMDate"] != null ? fc["KOMDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(KOMDate))
                {
                    objPCL017.KOMDate = DateTime.ParseExact(KOMDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL017.MKOMDate = pcl017.MKOMDate;
                string MKOMDate = fc["MKOMDate"] != null ? fc["MKOMDate"].ToString() : "";
                if (!string.IsNullOrWhiteSpace(MKOMDate))
                {
                    objPCL017.MKOMDate = DateTime.ParseExact(MKOMDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }

                objPCL017.chk1 = pcl017.chk1;
                objPCL017.remark1 = pcl017.remark1;
                objPCL017.chk2 = pcl017.chk2;
                objPCL017.remark2 = pcl017.remark2;
                objPCL017.chk3 = pcl017.chk3;
                objPCL017.remark3 = pcl017.remark3;
                objPCL017.chk4 = pcl017.chk4;
                objPCL017.remark4 = pcl017.remark4;
                objPCL017.chk5 = pcl017.chk5;
                objPCL017.remark5 = pcl017.remark5;
                objPCL017.chk6 = pcl017.chk6;
                objPCL017.remark6 = pcl017.remark6;
                objPCL017.chk7 = pcl017.chk7;
                objPCL017.remark7 = pcl017.remark7;
                objPCL017.chk8 = pcl017.chk8;
                objPCL017.remark8 = pcl017.remark8;
                objPCL017.chk9 = pcl017.chk9;
                objPCL017.remark9 = pcl017.remark9;
                objPCL017.chk10 = pcl017.chk10;
                objPCL017.remark10 = pcl017.remark10;
                objPCL017.chk11 = pcl017.chk11;
                objPCL017.remark11 = pcl017.remark11;
                objPCL017.chk12 = pcl017.chk12;
                objPCL017.remark12 = pcl017.remark12;
                objPCL017.chk13 = pcl017.chk13;
                objPCL017.remark13 = pcl017.remark13;
                objPCL017.chk14 = pcl017.chk14;
                objPCL017.remark14 = pcl017.remark14;
                objPCL017.chk15 = pcl017.chk15;
                objPCL017.remark15 = pcl017.remark15;
                objPCL017.chk16 = pcl017.chk16;
                objPCL017.remark16 = pcl017.remark16;
                objPCL017.chk17 = pcl017.chk17;
                objPCL017.remark17 = pcl017.remark17;
                objPCL017.chk18 = pcl017.chk18;
                objPCL017.remark18 = pcl017.remark18;
                objPCL017.chk19 = pcl017.chk19;
                objPCL017.remark19 = pcl017.remark19;
                objPCL017.chk20 = pcl017.chk20;
                objPCL017.remark20 = pcl017.remark20;
                objPCL017.chk21 = pcl017.chk21;
                objPCL017.remark21 = pcl017.remark21;
                objPCL017.chk22 = pcl017.chk22;
                objPCL017.remark22 = pcl017.remark22;
                objPCL017.chk23 = pcl017.chk23;
                objPCL017.remark23 = pcl017.remark23;
                objPCL017.chk24 = pcl017.chk24;
                objPCL017.remark24 = pcl017.remark24;
                objPCL017.chk25 = pcl017.chk25;
                objPCL017.remark25 = pcl017.remark25;
                objPCL017.chk26 = pcl017.chk26;
                objPCL017.remark26 = pcl017.remark26;
                objPCL017.chk27 = pcl017.chk27;
                objPCL017.remark27 = pcl017.remark27;
                objPCL017.chk28 = pcl017.chk28;
                objPCL017.remark28 = pcl017.remark28;
                objPCL017.chk29 = pcl017.chk29;
                objPCL017.remark29 = pcl017.remark29;
                objPCL017.chk30 = pcl017.chk30;
                objPCL017.remark30 = pcl017.remark30;
                objPCL017.chk31 = pcl017.chk31;
                objPCL017.remark31 = pcl017.remark31;
                objPCL017.chk32 = pcl017.chk32;
                objPCL017.remark32 = pcl017.remark32;
                objPCL017.chk33 = pcl017.chk33;
                objPCL017.remark33 = pcl017.remark33;
                objPCL017.chk34 = pcl017.chk34;
                objPCL017.remark34 = pcl017.remark34;
                objPCL017.chk35 = pcl017.chk35;
                objPCL017.remark35 = pcl017.remark35;
                objPCL017.chk36 = pcl017.chk36;
                objPCL017.remark36 = pcl017.remark36;
                objPCL017.chk37 = pcl017.chk37;
                objPCL017.remark37 = pcl017.remark37;
                objPCL017.chk38 = pcl017.chk38;
                objPCL017.remark38 = pcl017.remark38;
                objPCL017.chk39 = pcl017.chk39;
                objPCL017.remark39 = pcl017.remark39;
                objPCL017.chk40 = pcl017.chk40;
                objPCL017.remark40 = pcl017.remark40;
                objPCL017.chk41 = pcl017.chk41;
                objPCL017.remark41 = pcl017.remark41;
                objPCL017.chk42 = pcl017.chk42;
                objPCL017.remark42 = pcl017.remark42;
                objPCL017.chk43 = pcl017.chk43;
                objPCL017.remark43 = pcl017.remark43;
                objPCL017.chk44 = pcl017.chk44;
                objPCL017.remark44 = pcl017.remark44;
                objPCL017.chk45 = pcl017.chk45;
                objPCL017.remark45 = pcl017.remark45;
                objPCL017.chk46 = pcl017.chk46;
                objPCL017.remark46 = pcl017.remark46;
                objPCL017.chk47 = pcl017.chk47;
                objPCL017.remark47 = pcl017.remark47;
                objPCL017.chk48 = pcl017.chk48;
                objPCL017.remark48 = pcl017.remark48;
                objPCL017.chk49 = pcl017.chk49;
                objPCL017.remark49 = pcl017.remark49;
                objPCL017.chk50 = pcl017.chk50;
                objPCL017.remark50 = pcl017.remark50;
                objPCL017.chk51 = pcl017.chk51;
                objPCL017.remark51 = pcl017.remark51;
                objPCL017.chk52 = pcl017.chk52;
                objPCL017.remark52 = pcl017.remark52;
                objPCL017.chk53 = pcl017.chk53;
                objPCL017.remark53 = pcl017.remark53;
                objPCL017.chk54 = pcl017.chk54;
                objPCL017.remark54 = pcl017.remark54;
                objPCL017.chk55 = pcl017.chk55;
                objPCL017.remark55 = pcl017.remark55;
                objPCL017.chk56 = pcl017.chk56;
                objPCL017.remark56 = pcl017.remark56;
                objPCL017.chk57 = pcl017.chk57;
                objPCL017.remark57 = pcl017.remark57;
                objPCL017.chk58 = pcl017.chk58;
                objPCL017.remark58 = pcl017.remark58;
                objPCL017.chk59 = pcl017.chk59;
                objPCL017.remark59 = pcl017.remark59;
                objPCL017.chk60 = pcl017.chk60;
                objPCL017.remark60 = pcl017.remark60;
                objPCL017.chk61 = pcl017.chk61;
                objPCL017.remark61 = pcl017.remark61;
                objPCL017.chk62 = pcl017.chk62;
                objPCL017.remark62 = pcl017.remark62;

                if (pcl017.LineId > 0)
                {
                    objPCL017.EditedBy = objClsLoginInfo.UserName;
                    objPCL017.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL017.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL017.CreatedBy = objClsLoginInfo.UserName;
                    objPCL017.CreatedOn = DateTime.Now;
                    db.PCL017.Add(objPCL017);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL017.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHSCDocument(PCL016 pcl016, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                PCL001 objPCL001 = db.PCL001.Where(x => x.HeaderId == pcl016.HeaderId).FirstOrDefault();
                PCL016 objPCL016 = new PCL016();
                if (pcl016.Id > 0)
                {
                    objPCL016 = db.PCL016.Where(x => x.Id == pcl016.Id).FirstOrDefault();
                }
                objPCL016.HeaderId = objPCL001.HeaderId;
                objPCL016.Project = objPCL001.Project;
                objPCL016.Document = objPCL001.Document;
                objPCL016.CSRDate = pcl016.CSRDate;
                objPCL016.KOMDate = pcl016.KOMDate;
                objPCL016.MKOMDate = pcl016.MKOMDate;

                if (pcl016.Id > 0)
                {
                    objPCL016.EditedBy = objClsLoginInfo.UserName;
                    objPCL016.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL016.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Update.ToString();
                }
                else
                {
                    objPCL016.CreatedBy = objClsLoginInfo.UserName;
                    objPCL016.CreatedOn = DateTime.Now;
                    db.PCL016.Add(objPCL016);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPCL001.HeaderId;
                    objResponseMsg.LineID = objPCL016.Id;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

    }
}