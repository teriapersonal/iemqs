﻿using System.Web.Mvc;

namespace IEMQS.Areas.GTM
{
    public class GTMAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "GTM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "GTM_default",
                "GTM/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}