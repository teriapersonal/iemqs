﻿using IEMQSImplementation;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MOM.Models
{
    public class HeaderModel
    {
        public string Project { get; set;}
    }
    public class MOMProjectDetail
    {
        public MOM001 objMOM { get; set; }
        public string Condition { get; set; }
        //public string webCDDString { get { return (objMOM.CDD.HasValue ? "" : objMOM.CDD.Value.ToString("yyyy-MM-dd")); } }
    }

}