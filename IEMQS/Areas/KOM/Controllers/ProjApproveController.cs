﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.MSW.Controllers;

namespace IEMQS.Areas.KOM.Controllers
{
    public class ProjApproveController : clsBase
    {

        // GET: KOM/ProjMaintain

        [SessionExpireFilter]
        public ActionResult KOMProjHeaderDetails()
        {

            return View();
        }

        [SessionExpireFilter]
        public ActionResult MKOMProjHeaderDetails()
        {

            return View();
        }




        [SessionExpireFilter]
        public ActionResult ProjLineDetails(int Id = 0, string Stage = "")
        {

            KOM001 objKOM001 = new KOM001();
            KOM002 objKOM002 = new KOM002();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            var project = (from a in db.KOM001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            var lstResult = db.SP_KOM_GETApproverName
                        (
                         "", "1=1"
                        ).ToList();

            List<string> lst = lstResult.Select(x => x.Name).ToList();

            //List<string> lstFuncationLeader = db.COM003.Select(i => i.t_psno + " - " + i.t_name).ToList();
            ViewBag.lstFuncationLeader = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            //List<string> lstBuddy = db.COM003.Where(i => i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).ToList();
            ViewBag.lstBuddy = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objKOM001 = db.KOM001.Where(x => x.HeaderId == Id).FirstOrDefault();

                DateTime temp = Convert.ToDateTime(objKOM001.ZeroDate);
                objKOM001.KOMPlannedDate = temp.AddDays(21);

                ViewBag.LineStatus = objKOM001.Status;
                ViewBag.Heading = "Project Level Full Kit For KOM";
                ViewBag.MainHeading = "Kick of Meeting Detail";

                if (ViewBag.LineStatus.Contains("MKOM") && Stage == "KOM")
                {
                    ViewBag.LineStatus = "KOM Completed";

                }

                if (ViewBag.LineStatus.Contains("MKOM"))
                {
                    ViewBag.Heading = "Project Level Full Kit For MKOM";
                    ViewBag.MainHeading = "Manufacturing Kick of Meeting";
                    objKOM001.KOMPlannedDate = temp.AddDays(135);
                }
                ViewBag.ZeroDate = Convert.ToDateTime(objKOM001.ZeroDate).ToString("dd/MM/yyyy");

            }

            return View(objKOM001);
        }



        [HttpPost]
        public ActionResult GetKOMProjHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetKOMProjHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetMKOMProjHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetMKOMProjHeaderGridDataPartial");
        }



        [HttpPost]
        public JsonResult LoadKOMProjHeaderData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.KOMApprovePMG.GetStringValue() + "')";
                }
                else
                {
                    //   strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.KOMSentPMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.KOMApprovePMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.KOMApproveTOC.GetStringValue() + "')";
                    strWhere += "1=1  and kom1.Status not in ('" + clsImplementationEnum.KOMStatus.Draft.GetStringValue() + "' , '" + clsImplementationEnum.KOMStatus.KOM.GetStringValue() + "')";
                }
                strWhere += " and ( [KOMTOCManager] = '" + user + "' ) ";
                // strWhere += "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%' or kom1.Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                // strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_KOM_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                Convert.ToString( uc.Status.Replace("MKOM Approved by TOC","KOM Completed").Replace("MKOM Approved by PMG","KOM Completed").Replace("MKOM Sent for Approval to PMG","KOM Completed").Replace("MKOM Completed","KOM Completed").Replace("MKOM","KOM Completed")),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadMKOMProjHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMApproveTOC.GetStringValue() + "','" + clsImplementationEnum.KOMStatus.MKOMCompleted.GetStringValue() + "')";
                }
                strWhere += " and (  [MKOMTOCManager] = '" + user + "' ) ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_KOM_GETHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                uc.Status,
                                uc.HeaderId.ToString(),
                                uc.HeaderId.ToString()
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public ActionResult LoadProjLineData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                string headerstatus = param.MTStatus;
                string strWhere = string.Empty;


                if (headerstatus.Contains("MKOM"))
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere = " (Project like '%" + param.sSearch
                            + "%' or Functions like '%" + param.sSearch
                            + "%' or MKOMApprovedBy like '%" + param.sSearch
                            + "%' or MKOMPMGComment like '%" + param.sSearch
                             + "%' or FunctionalLead like '%" + param.sSearch
                            + "%')";
                    }
                    else
                    {
                        strWhere = "1=1";
                    }

                    var lstResult = db.SP_MKOM_GET_ProjLINEDETAILS
                                (
                                StartIndex, EndIndex, "", headerid, strWhere
                                ).ToList();
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                     
                                     "",
                                        GenerateTextboxFor(newRecordId, "Functions"),
                                       GenerateTextboxFor(newRecordId, "Responsible"),
                                        GenerateTextboxFor(newRecordId, "FunctionalLead"),
                                         GenerateTextboxFor(newRecordId, "MKOMApprovedBy"),
                                           GenerateTextboxFor(newRecordId, "MKOMApprovedOn"),
                                               GenerateTextboxFor(newRecordId, "MKOMPMGComment"),
                                        Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                        "",
                                        "",
                                        };

                    var data = (from uc in lstResult
                                select new[]
                                {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Functions),
                                 Convert.ToString(uc.Responsible),
                                 Convert.ToString(uc.FunctionalLead),
                                  Convert.ToString(uc.MKOMApprovedBy),
                                   Convert.ToString(uc.MKOMApprovedOn),
                                    Convert.ToString(uc.MKOMPMGComment),
                          //   GenerateTextboxFor(headerid, "PMGComments", Convert.ToString(uc.PMGComments),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),

                            Convert.ToString(uc.HeaderId),
                             Convert.ToString(uc.LineId) ,
                             HTMLActionStringForView(headerid,uc.Functions,"MKOM",  Convert.ToString(uc.MKOMApprovedBy) ),

                           }).ToList();

                    data.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere = " (Project like '%" + param.sSearch
                            + "%' or Functions like '%" + param.sSearch
                            + "%' or ApprovedBy like '%" + param.sSearch
                            + "%' or PMGComments like '%" + param.sSearch
                            + "%' or FunctionalLead like '%" + param.sSearch
                            + "%')";
                    }
                    else
                    {
                        strWhere = "1=1";
                    }
                    var lstResult = db.SP_KOM_GET_ProjLINEDETAILS
                                (
                                StartIndex, EndIndex, "", headerid, strWhere
                                ).ToList();
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                     
                                     "",
                                        GenerateTextboxFor(newRecordId, "Functions"),
                                       GenerateTextboxFor(newRecordId, "Responsible"),
                                        GenerateTextboxFor(newRecordId, "FunctionalLead"),
                                         GenerateTextboxFor(newRecordId, "ApprovedBy"),
                                           GenerateTextboxFor(newRecordId, "ApprovedOn"),
                                               GenerateTextboxFor(newRecordId, "PMGComments"),
                                        Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                        "",
                                        "",
                                        };

                    var data = (from uc in lstResult
                                select new[]
                                {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Functions),
                                 Convert.ToString(uc.Responsible),
                                 Convert.ToString(uc.FunctionalLead),
                                  Convert.ToString(uc.ApprovedBy),
                                   Convert.ToString(uc.ApprovedOn),
                                    Convert.ToString(uc.PMGComments),
                          //   GenerateTextboxFor(headerid, "PMGComments", Convert.ToString(uc.PMGComments),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),

                            Convert.ToString(uc.HeaderId),
                             Convert.ToString(uc.LineId) ,
                             HTMLActionStringForView(headerid,uc.Functions,"KOM",  Convert.ToString(uc.ApprovedBy) ),

                           }).ToList();

                    data.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "KOM002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateProjDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "KOM002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult getCodeValue(string project, string TOCapprover, string PMGapprover, string contract)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == TOCapprover && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.appdesc = objResponseMsg.appdesc + '|' + db.COM003.Where(i => i.t_psno == PMGapprover && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.contractDesc = db.COM004.Where(x => x.t_cono == contract).Select(x => x.t_cono + " - " + x.t_desc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult UploadPPTTOC(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                objResponseMsg.status = "Uploaded";
                var folderPath = "KOM001/" + headerid + "_KOM_FINAL";
                objResponseMsg.appdesc = folderPath;
                //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult MKOMUploadPPTTOC(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                objResponseMsg.status = "Uploaded";
                var folderPath = "KOM001/" + headerid + "_MKOM_FINAL";
                objResponseMsg.appdesc = folderPath;
                //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult ApproveTOC(int headerid, string remark)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                //var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.PMGComments == "" || x.PMGComments == null)).Any(x => x.Project == objKOM001.Project);  Remarks should not be mandatory in all sessions. 06/12/2018

                //if (isvalid == false)
                //{
                //    isvalid = db.KOM002.Where(x => x.HeaderId == headerid && x.Status != "KOM Sent for Approval to PMG").Any(x => x.Project == objKOM001.Project);
                //}

                //if (isvalid == false)
                //{
                if (headerid > 0)
                {

                    int HeaderId = headerid;

                    objKOM001.Status = "KOM Approved by TOC";

                    //DateTime KOMPlannedDate = Convert.ToDateTime(objKOM001.ZeroDate);
                    //objKOM001.KOMPlannedDate = KOMPlannedDate.AddDays(21);
                    objKOM001.KOMTOCDate = DateTime.Now;
                    objKOM001.KOMTOCComments = @remark;
                    objKOM001.ReturnRemark = "";

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                    objResponseMsg.status = objKOM001.Status;

                }
                //}
                //else
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = clsImplementationMessage.KOMMessage.SentForApprovalValid.ToString();
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ReturnTOC(int headerid, string remark)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                //  var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.PMGComments == "" || x.PMGComments == null)).Any(x => x.Project == objKOM001.Project);
                //if (isvalid == false)
                //{
                //    isvalid = db.KOM002.Where(x => x.HeaderId == headerid && x.Status != "KOM Sent for Approval to PMG").Any(x => x.Project == objKOM001.Project);
                //}
                var isvalid = false;
                if (isvalid == false)
                {
                    if (headerid > 0)
                    {

                        int HeaderId = headerid;

                        objKOM001.Status = "KOM Sent for Approval to PMG";

                        //DateTime KOMPlannedDate = Convert.ToDateTime(objKOM001.ZeroDate);
                        //objKOM001.KOMPlannedDate = KOMPlannedDate.AddDays(21);
                        // objKOM001.KOMPMGDate = DateTime.Now;
                        objKOM001.ReturnRemark = @remark;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                        objResponseMsg.status = objKOM001.Status;

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.SentForApprovalValid.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MKOMReturnTOC(int headerid, string remark)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                //  var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.PMGComments == "" || x.PMGComments == null)).Any(x => x.Project == objKOM001.Project);
                //if (isvalid == false)
                //{
                //    isvalid = db.KOM002.Where(x => x.HeaderId == headerid && x.Status != "KOM Sent for Approval to PMG").Any(x => x.Project == objKOM001.Project);
                //}
                var isvalid = false;
                if (isvalid == false)
                {
                    if (headerid > 0)
                    {

                        int HeaderId = headerid;

                        objKOM001.Status = "MKOM Sent for Approval to PMG";

                        //DateTime KOMPlannedDate = Convert.ToDateTime(objKOM001.ZeroDate);
                        //objKOM001.KOMPlannedDate = KOMPlannedDate.AddDays(21);
                        //  objKOM001.KOMPMGDate = DateTime.Now;
                        objKOM001.ReturnRemark = @remark;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                        objResponseMsg.status = objKOM001.Status;

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.SentForApprovalValid.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MKOMApproveTOC(int headerid, string remark)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();

                //Remarks should not be mandatory in all sessions. Obs Id#18424
                //var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.PMGComments == "" || x.PMGComments == null)).Any(x => x.Project == objKOM001.Project);

                //if (isvalid == false)
                //{
                //    isvalid = db.KOM002.Where(x => x.HeaderId == headerid && x.Status != "MKOM Sent for Approval to PMG").Any(x => x.Project == objKOM001.Project);
                //}

                //if (isvalid == false)
                //{
                if (headerid > 0)
                {

                    int HeaderId = headerid;
                    objKOM001.Status = "MKOM Approved by TOC";

                    //DateTime KOMPlannedDate = Convert.ToDateTime(objKOM001.ZeroDate);
                    //objKOM001.KOMPlannedDate = KOMPlannedDate.AddDays(21);
                    objKOM001.MKOMTOCDate = DateTime.Now;
                    objKOM001.MKOMTOCComments = @remark;
                    objKOM001.ReturnRemark = "";

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                    objResponseMsg.status = objKOM001.Status;

                }
                //}
                //else
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = clsImplementationMessage.KOMMessage.SentForApprovalValid.ToString();
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }





        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

        public string HTMLActionStringForView(int rowId, string Funcation, string Stage, string isapprove)
        {
            string htmlControl = "";
            string inputID = WebsiteURL + "/KOM/Maintain/KOMLineDetails?Id=" + rowId + "&Funcation=" + Funcation + "&Stage=" + Stage + "&ReadOnly=1";
            string Icon = "<i style = 'margin-left:5px;' class='fa fa-eye'></i>";

            if (isapprove == null)
            {
                htmlControl = "<a title='View' style = 'pointer-events:none; opacity:0.3'  href= '" + inputID + "' target='_blank'>" + Icon + "</a>";
            }
            else
            {
                htmlControl = "<a  title='View'  href= '" + inputID + "' target='_blank'>" + Icon + "</a>";
            }


            return htmlControl;
        }


    }
}