﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Areas.CSR.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MIP.Controllers
{
    public class ReportController : clsBase
    {
        // GET: MIP/Report
        public ActionResult Index()
        {
            List<SelectListItem> ReportType = new List<SelectListItem>()
            { new SelectListItem { Text = "VE after machining", Value = "VE" },
              new SelectListItem { Text = "LPE", Value = "LPE" },
              //new SelectListItem { Text = "PICKLING", Value = "PP" },
              new SelectListItem { Text = "VE REPORT AFTER PICKLING", Value = "VE/PP" },
              new SelectListItem { Text = "WIPE", Value = "WT" },
              new SelectListItem { Text = "LPE after Root Pass", Value = "LPE/RP" },
              new SelectListItem { Text = "Visual inspection of completed weld ", Value = "WV" },
              new SelectListItem { Text = "UE of machined brackets", Value = "UE" }
            };
            ViewBag.ReportType = ReportType;
            var lst = GetSubCatagory("MIP_Type", objClsLoginInfo.Location, "");
            ViewBag.DocNo = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Code }).ToList();
            ViewBag.QualityProject = db.MIP050.Select(x => new { CatDesc = x.QualityProject, CatID = x.QualityProject + "#" + x.BU + "#" + x.Location }).Distinct().ToList();
            return View();
        }

        public List<CategoryData> GetSubCatagory(string Key, string strLoc, string BU)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            List<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct().ToList();


                //lstGLB002 = (from glb002 in db.GLB002
                //             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                //             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location) &&
                //                   glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase)
                //             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();
            }
            else
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct().ToList();

                //lstGLB002 = (from glb002 in db.GLB002
                //             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                //             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                //             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();
            }
            return lstGLB002;
        }

        [HttpPost]
        public ActionResult getDataEdit(string bu, string loc)
        {
            List<CategoryData> lst = GetSubCatagory("MIP_Type", loc, bu);
            var listCategoryData = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();
            return Json(listCategoryData, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter,UserPermissions]
        public ActionResult WIPReport()
        {
            var lst = GetSubCatagory("MIP_Type", objClsLoginInfo.Location, "");
            ViewBag.MIP_Type = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();
            ViewBag.QualityProjects = db.MIP005.Select(x => new { CatID = x.QualityProject.Trim(), CatDesc = x.QualityProject.Trim() }).Distinct().ToList();
            return View();
        }

        [SessionExpireFilter]
        public JsonResult GetCategoryList(string QualityProject)
        {
            var lstCategory= db.MIP005.Where(i=>i.QualityProject==QualityProject).Select(x => new { CatID = x.Category.Trim(), CatDesc = x.Category.Trim() }).Distinct().ToList();
            return Json(lstCategory);
        }
    }
}