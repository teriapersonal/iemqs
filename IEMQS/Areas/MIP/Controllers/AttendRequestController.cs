﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.Utility.Models;
using System.Data;
using OfficeOpenXml;
using System.IO;

namespace IEMQS.Areas.MIP.Controllers
{
    public class AttendRequestController : clsBase
    {
        // GET: MIP/AttendRequest
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Attend Operation";
            return View();
        }

        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadMIPIndexDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m50.BU", "m50.Location");
                string _urlform = string.Empty;
                string indextype = param.Department;

                if (param.Status.ToUpper() == "PENDING")
                {
                    whereCondition += " and m50.TestResult IS NULL and m50.OfferedBy IS NOT NULL ";
                }

                string[] columnName = { "QualityProject", "(m50.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(m50.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(m50.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                string sortColumnName = Convert.ToString(param.sColumns.Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_GET_ATTEND_INSPECTION_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        !string.IsNullOrWhiteSpace(a.ComponentNo) ?a.ComponentNo.ToUpper() : "",
                        Manager.GetCategoryOnlyDescription(a.DocNo,a.BU.Split('-')[0],a.Location.Split('-')[0]  ,"MIP_DocNo" ),
                        Manager.GetCategoryOnlyDescription(a.MIPType,a.BU.Split('-')[0],a.Location.Split('-')[0]  ,"MIP_Type" ),
                        HTMLActionString(Convert.ToInt32(a.RequestId), "View", "View Details", "fa fa-eye", "", WebsiteURL + "/MIP/AttendRequest/AttendOperationDetails/"+a.RequestId )
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadMIPHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m3.BU", "m3.Location");
                string _urlform = string.Empty;
                string reqId = param.Headerid;
                if (param.Status.ToUpper() == "PENDING")
                {
                    whereCondition += " and m4.InspectionStatus= '" + clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue() + "'";
                }
                if (param.Status.ToUpper() == "REOFFER")
                {
                    whereCondition += " and m4.InspectionStatus in ('" + clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue() + "')";
                }

                whereCondition += " and m4.QualityProject='" + param.Project + "' and m4.BU='" + param.BU + "'"
                                 + " and m4.Location='" + param.Location + "' and M4.ComponentNo='" + param.Customer + "' "
                                 + " and m3.DocNo='" + param.FilterDocNo + "' and m3.MIPType='" + param.UserTypeId + "'";


                string[] columnName = { "m3.QualityProject", "(m3.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(m3.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(m3.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))", "m4.OperationNo", "m4.ActivityDescription", "m4.RefDocument", "m4.RefDocRevNo", "m4.PIA", "m4.IAgencyLNT", "m4.IAgencyVVPT", "m4.IAgencySRO", "m4.RecordsDocuments", "m4.Observations", "m4.InspectionStatus" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                //else
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_HEADER_GET_LINES_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from uc in lstHeader
                          select new[] {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               uc.OperationNo.ToString("G29"),
                               uc.ActivityDescription,
                               uc.RefDocument,
                               uc.RefDocRevNo,
                               Manager.GetCategoryOnlyDescription(uc.PIA,uc.BU.Split('-')[0],uc.Location.Split('-')[0] ,"PIA" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,uc.BU.Split('-')[0], uc.Location.Split('-')[0],"MIP Inspection Agency L&T" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,uc.BU.Split('-')[0],uc.Location.Split('-')[0],"MIP Inspection Agency VVPT"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencySRO,uc.BU.Split('-')[0], uc.Location.Split('-')[0]  ,"MIP Inspection Agency SRO"),
                               uc.RecordsDocuments,
                               uc.Observations,
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               uc.InspectionStatus,
                               HTMLActionString(Convert.ToInt32(uc.HeaderId), "View", "View Details", "fa fa-eye", "", WebsiteURL + "/MIP/AttendRequest/Details/"+uc.LineId )
                               + Manager.generateMIPGridButtons(uc.LineId,"MIP Details",50,"MIP Details","Request Details","MIP Request Details")
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }


        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AttendOperationDetails(int id = 0)
        {
            MIP050 objMIP050 = new MIP050();

            if (id > 0)
            {
                objMIP050 = db.MIP050.Where(i => i.RequestId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objMIP050.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP050.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP050.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.DocNo = Manager.GetCategoryOnlyDescription(objMIP050.DocNo, objMIP050.BU, objMIP050.Location, "MIP_DocNo");
                ViewBag.MIPType = Manager.GetCategoryOnlyDescription(objMIP050.MIPType, objMIP050.BU, objMIP050.Location, "MIP_Type");
            }
            else
            {
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objMIP050.Location = objClsLoginInfo.Location;
            }

            return View(objMIP050);
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            MIP050 objMIP050 = new MIP050();
            if (id > 0)
            {
                var objMIP004 = db.MIP004.FirstOrDefault(f => f.LineId == id);
                objMIP050 = db.MIP050.Where(i => i.QualityProject == objMIP004.QualityProject && i.BU == objMIP004.MIP003.BU && i.Location == objMIP004.MIP003.Location && i.DocNo == objMIP004.MIP003.DocNo && i.MIPType == objMIP004.MIP003.MIPType && i.OperationNo == objMIP004.OperationNo && i.RequestNo == objMIP004.RequestNo && i.RequestNoSequence == objMIP004.RequestSequenceNo).FirstOrDefault();
                ViewBag.ProjectCode = objMIP004.MIP003.Project;
                ViewBag.QualityProject = objMIP004.MIP003.QualityProject;
                ViewBag.LocationCode = objMIP004.MIP003.Location;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objMIP004.MIP003.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP004.MIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP004.MIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.DocNo = Manager.GetCategoryOnlyDescription(objMIP004.MIP003.DocNo, objMIP004.MIP003.BU, objMIP004.MIP003.Location, "MIP_DocNo");
                ViewBag.MIPType = Manager.GetCategoryOnlyDescription(objMIP004.MIP003.MIPType, objMIP004.MIP003.BU, objMIP004.MIP003.Location, "MIP_Type");
                if (objMIP050 == null)
                {
                    objMIP050 = new MIP050();
                    objMIP050.QualityProject = objMIP004.QualityProject;
                    objMIP050.BU = objMIP004.BU;
                    objMIP050.Location = objMIP004.Location;
                    objMIP050.RevNo = objMIP004.MIPRevNo;
                    objMIP050.TestResult = objMIP004.InspectionStatus;
                }
                ViewBag.IDM = objMIP004.IDM;
                if (db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId).OrderByDescending(x => x.OperationNo).Select(x => x.OperationNo).FirstOrDefault() == objMIP004.OperationNo)
                {
                    ViewBag.IsRequiredIDM = true;
                }
                else { ViewBag.IsRequiredIDM = false; }

            }
            return View(objMIP050);
        }

        public ActionResult GetMIPHeaderDataPartial(string Status, string QualityProject, string Location, string BU, int requestid)
        {
            ViewBag.status = Status;
            ViewBag.lineid = requestid;
            MIP050 objMIP050 = new MIP050();
            if (requestid > 0)
            {
                objMIP050 = db.MIP050.Where(i => i.RequestId == requestid).FirstOrDefault();
                var objMIP004 = db.MIP004.FirstOrDefault(w => w.QualityProject == objMIP050.QualityProject && w.BU == objMIP050.BU && w.Location == objMIP050.Location && w.MIP003.DocNo == objMIP050.DocNo && w.MIP003.MIPType == objMIP050.MIPType && w.MIP003.ComponentNo == objMIP050.ComponentNo);
                string clearStatus = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string naStatus = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                if (objMIP004 != null && !db.MIP004.Any(w => w.HeaderId == objMIP004.HeaderId && (w.InspectionStatus != clearStatus && w.InspectionStatus != naStatus)))
                {
                    var folderPaths = db.MIP004.Where(w => w.HeaderId == objMIP004.HeaderId && w.InspectionStatus == clearStatus).Select(s => "MIP050/" + s.RequestNo);
                    ViewBag.FolderPaths = String.Join(",", folderPaths);
                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            return PartialView("_GetHeaderDataPartial", objMIP050);
        }

        [HttpPost]
        public ActionResult RequestClear(int RequestId, string QCRemark, string idm)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string readytooffer = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
                string clear = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string NA = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                string underInspection = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                MIP050 objMIP050 = db.MIP050.Where(x => x.RequestId == RequestId).FirstOrDefault();
                MIP004 objMIP004 = db.MIP004.Where(x => x.RequestNo == objMIP050.RequestNo && x.RequestSequenceNo == objMIP050.RequestNoSequence).FirstOrDefault();
                if (objMIP004 != null)
                {
                    if (objMIP004.InspectionStatus == underInspection)
                    {
                        if (objMIP004.MIP003.Status == approved)
                        {
                            #region Clear
                            objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                            objMIP004.QCRemark = QCRemark;
                            objMIP004.IDM = idm;
                            objMIP050.TestResult = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                            objMIP050.InspectedBy = objClsLoginInfo.UserName;
                            objMIP050.InspectedOn = DateTime.Now;
                            objMIP050.QCRemark = QCRemark;
                            foreach (var item in db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId && (x.InspectionStatus == readytooffer)))
                            {
                                item.InspectionStatus = null;
                            }
                            db.SaveChanges();

                            MIP004 nextobjMIP004 = db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId && (((x.InspectionStatus != clear && x.InspectionStatus != NA) && x.InspectionStatus == null) || x.InspectionStatus == readytooffer)).OrderBy(x => x.OperationNo).FirstOrDefault();
                            if (nextobjMIP004 != null)
                            {
                                nextobjMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                                nextobjMIP004.EditedBy = objClsLoginInfo.UserName;
                                nextobjMIP004.EditedOn = DateTime.Now;
                            }
                            //objMIP004.EditedBy = objClsLoginInfo.UserName;
                            //objMIP004.EditedOn = DateTime.Now;
                            objMIP004.InspectedBy = objClsLoginInfo.UserName;
                            objMIP004.InspectedOn = DateTime.Now;
                            db.SaveChanges();


                            //#region Send Notification
                            //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                            //    "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " & Project No: " + objTVL050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objTVL050.RequestId);
                            //#endregion

                            #endregion

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Data Cleared Successfully";
                            objResponseMsg.Status = objMIP004.Status;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Component: " + objMIP004.MIP003.ComponentNo + " is in '" + objMIP004.MIP003.Status + "' in MIP Header. Please approve component first.";
                        }
                    }
                    else { objResponseMsg.Value = "Request is not in '" + underInspection + "' Stage."; }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult RequestReturn(int RequestId, string status, string QCRemark, string idm)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
                MIP050 objMIP050 = db.MIP050.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objMIP050 != null)
                {
                    MIP004 objMIP004 = db.MIP004.Where(x => x.RequestNo == objMIP050.RequestNo && x.RequestSequenceNo == objMIP050.RequestNoSequence).FirstOrDefault();

                    if (string.IsNullOrWhiteSpace(objMIP050.TestResult))
                    {
                        if (objMIP004.MIP003.Status == approved)
                        {
                            #region return
                            objMIP050.TestResult = status;
                            objMIP050.EditedBy = objClsLoginInfo.UserName;
                            objMIP050.EditedOn = DateTime.Now;
                            objMIP050.InspectedBy = objClsLoginInfo.UserName;
                            objMIP050.InspectedOn = DateTime.Now;
                            objMIP050.QCRemark = QCRemark;
                            //MIP004 objMIP004 = db.MIP004.Where(x => x.QualityProject == objMIP050.QualityProject
                            //                                        && x.OperationNo == objMIP050.OperationNo
                            //                                        ).FirstOrDefault();
                            objMIP004.InspectionStatus = status;
                            objMIP004.InspectedBy = objClsLoginInfo.UserName;
                            objMIP004.InspectedOn = DateTime.Now;
                            objMIP004.QCRemark = QCRemark;
                            objMIP004.IDM = idm;
                            db.SaveChanges();


                            //#region Send Notification
                            //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                            //    "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " & Project No: " + objTVL050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objTVL050.RequestId);
                            //#endregion

                            #endregion

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Data " + objMIP050.TestResult + " Successfully";
                            objResponseMsg.Status = objMIP050.TestResult;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Component: " + objMIP004.MIP003.ComponentNo + " is in '" + objMIP004.MIP003.Status + "' in MIP Header. Please approve component first.";
                        }
                    }
                    else { objResponseMsg.Value = "Request is '" + objMIP050.TestResult + "'."; }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }
        #endregion
        #region Group Clear
        public ActionResult GroupClearPartial()
        {
            var whereCondition = " m50.TestResult IS NULL and m50.OfferedBy IS NOT NULL ";
            whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m50.BU", "m50.Location");
            ViewBag.lstPandingQualityProject = db.SP_MIP_GET_ATTEND_INSPECTION_DATA(0, int.MaxValue, "", whereCondition).Select(s => s.QualityProject).Distinct().ToList();
            return PartialView("_GroupClearPartial");
        }

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetailWithComponent(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var ComponentNo = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU == objProjectsDetails.BUCode && lstLoc.Contains(w.Location) && w.InspectionStatus == lineStatus).Select(s => s.ComponentNo).OrderBy(o => o).Distinct().ToList();
            return Json(new { ProjectsDetails = objProjectsDetails, ComponentNo = ComponentNo }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetOperationNoWiseProjectBUDetail(string qualityProject, string bu, string componentNoFrom, string componentNoTo, string annexure)
        {
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
            var objMIP004 = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU == bu && lstLoc.Contains(w.Location) && w.InspectionStatus == lineStatus && w.MIP003.DocNo == annexure).OrderBy(o => o.ComponentNo).ToList();
            bool started = false;
            var OperationNo = new List<decimal>();
            foreach (var item in objMIP004)
            {
                if (item.ComponentNo == componentNoFrom || item.ComponentNo == componentNoTo)
                    started = true;
                if (started == false)
                    continue;
                OperationNo.Add(item.OperationNo);

                if (item.ComponentNo == componentNoTo)
                    break;
            }
            OperationNo = OperationNo.Distinct().OrderBy(o => o).ToList();
            return Json(OperationNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAnnexureDetail(string qualityProject, string bu, string loc, string componentNoFrom, string componentNoTo)
        {
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
            var lstComponentNo = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU.Trim() == bu.Trim() && lstLoc.Contains(w.Location) && w.InspectionStatus == lineStatus).OrderBy(o => o.ComponentNo).ToList();
            bool started = false;

            var lstAnnexure = new List<DropdownModel>();
            foreach (var item in lstComponentNo)
            {
                if (item.ComponentNo == componentNoFrom)
                    started = true;
                if (started == false)
                    continue;
                lstAnnexure.Add(new DropdownModel { Code = item.MIP003.DocNo });
                if (item.ComponentNo == componentNoTo)
                    break;
            }
            var AnnexureList = lstAnnexure.Select(c => c.Code).Distinct().Select(x => new DropdownModel { Code = x.Trim(), Description = Manager.GetCategoryOnlyDescription(x, bu, loc, "MIP_DocNo") }).Distinct().ToList();
            var Annexure = AnnexureList.OrderBy(o => o.Code).ToList();
            return Json(Annexure, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GroupClear(string lineIds) //, bool hasAttachments, Dictionary<string, string> Attach, string qualityProject, string BU, string location, string componentNoFrom, string componentNoTo, decimal operationNo,
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string UnderInspection = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                string rejected = clsImplementationEnum.MIPInspectionStatus.Rejected.GetStringValue();
                string returned = clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue();
                string clear = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string rto = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string NA = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                var LineIds = lineIds.Split(',').Select(s => int.Parse(s));// new List<int>();
                var NotApprovedComponentNos = new List<string>();

                /*if (hasAttachments)
                {
                    var folderPaths = db.MIP004.Where(x => LineIds.Contains(x.LineId)).Select(s => "MIP050/" + s.RequestNo).ToArray();
                    try
                    {
                        Manager.ManageDocumentsOnMultipleDestination(folderPaths, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString() + " While Uploading Files";
                        return Json(objResponseMsg);
                    }
                }*/

                foreach (var lineid in LineIds)
                {
                    var objMIP004 = db.MIP004.FirstOrDefault(x => x.LineId == lineid);
                    if (objMIP004.MIP003.Status == Approved)
                    {
                        #region Clear
                        MIP050 objMIP050 = db.MIP050.FirstOrDefault(x => x.RequestNo == objMIP004.RequestNo && x.RequestNoSequence == objMIP004.RequestSequenceNo);
                        objMIP004.InspectionStatus = clear;
                        objMIP050.TestResult = clear;
                        objMIP050.InspectedBy = objClsLoginInfo.UserName;
                        objMIP050.InspectedOn = DateTime.Now;
                        foreach (var item in db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId && (x.InspectionStatus == rto)))
                        {
                            item.InspectionStatus = null;
                        }
                        db.SaveChanges();


                        MIP004 nextobjMIP004 = db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId && ((x.InspectionStatus != clear && x.InspectionStatus != NA && x.InspectionStatus == null) || x.InspectionStatus == rto)).OrderBy(x => x.OperationNo).FirstOrDefault();
                        if (nextobjMIP004 != null)
                        {
                            nextobjMIP004.InspectionStatus = rto;
                            nextobjMIP004.EditedBy = objClsLoginInfo.UserName;
                            nextobjMIP004.EditedOn = DateTime.Now;
                        }
                        objMIP004.InspectedBy = objClsLoginInfo.UserName;
                        objMIP004.InspectedOn = DateTime.Now;
                        db.SaveChanges();

                        //#region Send Notification
                        //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                        //    "Traveler:  Request " + objTVL050.RequestNo + " of Activity: " + objTVL050.Activity + " & Project No: " + objTVL050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objTVL050.RequestId);
                        //#endregion
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data Cleared Successfully";
                        objResponseMsg.Status = objMIP004.Status;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        NotApprovedComponentNos.Add(objMIP004.MIP003.ComponentNo);
                    }
                }
                if (NotApprovedComponentNos.Count() > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Components: " + String.Join(",", NotApprovedComponentNos) + " is not 'Approved' in MIP Header. Please approve component first.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }
        public ActionResult loadOperationListGroupClearDataTable(JQueryDataTableParamModel param, string componentNoFrom, string componentNoTo, decimal operationNo)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m3.BU", "m3.Location");
                string _urlform = string.Empty;
                string reqId = param.Headerid;
                whereCondition += " and m4.InspectionStatus= '" + clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue() + "'";
                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lineStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                var objMIP004 = db.MIP004.Where(w => w.QualityProject == param.Project && w.BU == param.BU && lstLoc.Contains(w.Location) && w.OperationNo == operationNo && w.InspectionStatus == lineStatus).OrderBy(o => o.ComponentNo).ToList();
                bool started = false;
                var ComponentNo = new List<string>();
                foreach (var item in objMIP004)
                {
                    if (item.ComponentNo == componentNoFrom)
                        started = true;
                    if (started == false)
                        continue;
                    ComponentNo.Add(item.ComponentNo);
                    if (item.ComponentNo == componentNoTo)
                        break;
                }

                whereCondition += " and m4.QualityProject='" + param.Project + "' and m4.BU='" + param.BU + "'"
                                 + " and M4.ComponentNo in ('" + String.Join("','", ComponentNo) + "') "
                                 + " and m4.OperationNo= " + operationNo;

                string[] columnName = { "m4.ComponentNo", "m4.ActivityDescription", "m4.RefDocument", "m4.RefDocRevNo", "m4.PIA", "m4.IAgencyLNT", "m4.IAgencyVVPT", "m4.IAgencySRO", "m4.RecordsDocuments", "m4.Observations" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_HEADER_GET_LINES_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                string CheckedIds = "", FolderPaths = "";
                if (param.bCheckedAll && lstHeader.Any())
                {
                    var data = db.SP_MIP_HEADER_GET_LINES_DATA(1, 0, strSortOrder, whereCondition).Select(s => new SelectItemList() { id = s.LineId + "", text = "MIP050/" + s.RequestNo }).ToList();
                    CheckedIds = String.Join(",", data.Select(s => s.id));
                    FolderPaths = String.Join(",", data.Select(s => s.text));
                }

                var res = from uc in lstHeader
                          select new[] {
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.HeaderId),
                               uc.ComponentNo,
                               uc.ActivityDescription,
                               uc.RefDocument,
                               uc.RefDocRevNo,
                               Manager.GetCategoryOnlyDescription(uc.PIA,uc.BU.Split('-')[0],uc.Location.Split('-')[0] ,"PIA" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,uc.BU.Split('-')[0], uc.Location.Split('-')[0],"MIP Inspection Agency L&T" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,uc.BU.Split('-')[0],uc.Location.Split('-')[0],"MIP Inspection Agency VVPT"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencySRO,uc.BU.Split('-')[0], uc.Location.Split('-')[0]  ,"MIP Inspection Agency SRO"),
                               uc.RecordsDocuments,
                               uc.Observations,
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               HTMLActionString(Convert.ToInt32(uc.HeaderId), "View", "View Details", "fa fa-eye", "", WebsiteURL + "/MIP/AttendRequest/Details/"+uc.LineId ),
                               uc.RequestNo+""
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    checkedIds = CheckedIds,
                    folderPaths = FolderPaths
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Common Methods

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = "";
                    objUserRoleAccessDetails.UserDesignation = "";
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }

        public string GenerateRemark(int headerId, string columnName, string userAccessRole, string userRole, bool isEditable = false)
        {
            MIP001 objMIP001 = db.MIP001.FirstOrDefault(c => c.HeaderId == headerId);
            //bool isEditable = false;
            //if (objMIP001 != null)
            //{
            //    //if (objMIP001.Status == clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue() && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue())
            //    //    isEditable = true;
            //    //else if (objMIP001.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue() && objMIP001.WEApprovedBy != null && objMIP001.QCApprovedBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
            //    //    isEditable = true;
            //    //else 
            //    if (objMIP001.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue() && objMIP001.WEApprovedBy != null && objMIP001.QCApprovedBy != null && objMIP001.CustomerApprovedBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue())
            //        isEditable = true;
            //    else if (objMIP001.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue() && objMIP001.WEApprovedBy != null && objMIP001.QCApprovedBy != null && objMIP001.CustomerApprovedBy != null && objMIP001.PMGReleaseBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.PMG2.GetStringValue())
            //        isEditable = true;

            //    if(userRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue() && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())



            //    else
            //        isEditable = false;
            //}

            string remarkValue = (!string.IsNullOrWhiteSpace(objMIP001.PMGReturnedBy) ? objMIP001.PMGReturnRemarks : "");

            if (isEditable)
            {
                return Helper.GenerateTextbox(objMIP001.HeaderId, columnName, remarkValue, "", false, "", "100");
            }
            else
            {
                return remarkValue;
            }

        }
        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }
        public class ResponceMsgWithMultiValue : clsHelper.ResponseMsg
        {
            public string pendingMIP { get; set; }
            public string submittedMIP { get; set; }
            public bool isIdenticalProjectEditable { get; set; }
        }
        #endregion

    }
}