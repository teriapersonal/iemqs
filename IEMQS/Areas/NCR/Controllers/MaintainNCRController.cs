﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using IEMQS.DrawingReference;
using IEMQSImplementation.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.Areas.FDMS.Controllers;
using System.Collections;
using IEMQS.HEDirectoryService;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.DESCore.Data;

namespace IEMQS.Areas.NCR.Controllers
{
    public class MaintainNCRController : clsBase
    {
        // GET: NCR/MaintainNCR
        //IEMQSImplementation.Drawingdocumentreference.hedProjectDocListWebServiceClient sc = new IEMQSImplementation.Drawingdocumentreference.hedProjectDocListWebServiceClient();


        #region Utility
        public void SentEmailNotificationStageWise(string emailTo, string NCRNo, int headerid, bool close = false)
        {
            if (!string.IsNullOrWhiteSpace(emailTo))
            {
                Hashtable _ht = new Hashtable();
                EmailSend _objEmail = new EmailSend();

                _ht["[NCRNo]"] = NCRNo;
                _ht["[NCRLink]"] = WebsiteURL + "/NCR/MaintainNCR/AddHeader?headerID=" + headerid;
                MAIL001 objTemplateMaster;
                if (close == true)
                {
                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.NCR.NCRClosed).SingleOrDefault();
                }
                else
                {
                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.NCR.NCRStageWiseEmail).SingleOrDefault();

                }
                _objEmail.MailToAdd = emailTo;
                _ht["[Subject]"] = "NCR : " + NCRNo;
                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
            }
        }
        #endregion

        [SessionExpireFilter]
        public ActionResult Index(string Project)
        {
            ViewBag.PageTitle = "Maintain Non Conformity Report";
            ViewBag.stage = "Maintain";
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View();
        }

        [SessionExpireFilter]
        public ActionResult ValidateStage()
        {
            ViewBag.PageTitle = "Validate Non Conformity Report";
            ViewBag.stage = "Validate";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult CapaStage(string Project)
        {
            ViewBag.PageTitle = "Capa Submission";
            ViewBag.stage = "Capa";
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View("Index");
        }
        public ActionResult QAStage()
        {
            ViewBag.PageTitle = "QA Approval";
            ViewBag.stage = "QA";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        public ActionResult ReviewCommentStage(string Project)
        {
            ViewBag.PageTitle = "Review Comments";
            ViewBag.stage = "ReviewComment";
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View("Index");
        }
        public ActionResult TaskStage(string Project)
        {
            ViewBag.PageTitle = "Assigned Task";
            ViewBag.stage = "Task";
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View("TaskIndex");
        }
        public ActionResult QCApproval()
        {
            ViewBag.PageTitle = "QC Approval";
            ViewBag.stage = "QC";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        public ActionResult ExternalStage()
        {
            ViewBag.PageTitle = "External Approval";
            ViewBag.stage = "External";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        public ActionResult PlanImplStage(string Project)
        {
            ViewBag.PageTitle = "Plan Implementation";
            ViewBag.stage = "PlanImplementation";
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View("Index");
        }
        public ActionResult ImplStage()
        {
            ViewBag.PageTitle = "Implement";
            ViewBag.stage = "Implement";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        public ActionResult QCClosureStage()
        {
            ViewBag.PageTitle = "QC Closure";
            ViewBag.stage = "QCClosure";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        public ActionResult ExternalClosureStage()
        {
            ViewBag.PageTitle = "External Closure";
            ViewBag.stage = "ExternalClosure";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }
        public ActionResult QAClosureStage()
        {
            ViewBag.PageTitle = "QA Closure";
            ViewBag.stage = "QAClosure";
            ViewBag.IsDisplayOnly = false;
            return View("Index");
        }

        public ActionResult DisplayNCR()
        {
            ViewBag.PageTitle = "Display NCR";
            ViewBag.stage = string.Empty;
            return View();
        }

        public ActionResult AddRemAssStage()
        {
            ViewBag.stage = "AddRemoveAssignee";
            return View("AddRemoveAssignee");
        }

        public FileResult DownloadSample()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/NCR/Views/MaintainNCR/RCAFormat.doc"));
            string fileName = " UploadTemplate.doc";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [SessionExpireFilter]
        public ActionResult AddHeader(int? headerId, int d = 0, string Project = "")
        {
            ViewBag.IsDisplayOnly = false;
            //ViewBag.disableAllInputs = true;

            var roleQcManager = (from a in db.ATH001
                                 join b in db.ATH004 on a.Role equals b.Id
                                 where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && b.Role.ToLower() == clsImplementationEnum.UserRoleName.QC2.ToString().ToLower()

                                 select new { RoleId = a.Role, RoleDesc = b.Role }).FirstOrDefault();

            NCR001 objNCR001 = new NCR001();
            try
            {
                string user = objClsLoginInfo.UserName;

                string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
                ModelWQRequest objModelWQRequest = new ModelWQRequest();
                List<Projects> project = Manager.getProjectsByUser(user);
                List<CategoryData> ncrCategories = objModelWQRequest.GetSubCatagorywithoutBULocationCode("NCR Category").ToList();
                //List<string> ncrCategories1 = db.NCR006.Select(q => q.NCRCategory).Distinct().ToList();
                List<string> locs = clsImplementationEnum.getNCRLocation().ToList();
                List<string> ncrCat = clsImplementationEnum.getNCRLocation().ToList();
                List<string> toa = clsImplementationEnum.getTypeofAssembly().ToList();
                List<string> disposition = clsImplementationEnum.getDisposition().ToList();
                List<string> pdt = clsImplementationEnum.getProblemDueTo().ToList();
                List<string> rcc = clsImplementationEnum.getRootCauseCategory().ToList();
                List<string> minmaj = clsImplementationEnum.getMinMaj().ToList();
                List<GLB002> lstLocation = GetSubCatagories("NCR Location").ToList();
                //Dropdown
                ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x, CatID = x }).ToList();
                ViewBag.Disposition = disposition.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x, CatID = x }).ToList();
                ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x, CatID = x }).ToList();
                ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x, CatID = x }).ToList();
                ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.CategoryDescription, CatID = x.Value }).ToList();

                if (!string.IsNullOrEmpty(Project))
                {
                    ViewBag.getPROJECT = Manager.GetProjectAndDescription(Project);
                }

                if (headerId > 0)
                {
                    objNCR001 = db.NCR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    //List<string> lstdrawing = drawref.getDocumentMasterList(objNCR001.Project).ToList();

                    //ViewBag.refgrawing = lstdrawing.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();


                    if (objNCR001.ExternalApprover != null)
                    {
                        string externalapprover = objNCR001.ExternalApprover + '-' + Manager.GetUserNameFromPsNo(objNCR001.ExternalApprover);
                        ViewBag.externalapprover = externalapprover;
                    }
                    else
                    {
                        ViewBag.externalapprover = "";
                    }
                    var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, "").ToList();

                    if (roleQcManager != null && (objAccessProjects.Count > 0 ? objAccessProjects.Contains(objNCR001.Project) : false))
                    {
                        ViewBag.QcManager = "true";
                    }
                    else
                    {
                        ViewBag.QcManager = "false";
                    }
                    var obj = Manager.getBUandContractProjectWise(objNCR001.Project);
                    ViewBag.BUDesc = obj.BU;
                    ViewBag.ContractNo = obj.contract;

                    ViewBag.NCRNo = objNCR001.NCRNo;
                    //ViewBag.Project = project.Where(i => i.projectCode.Equals(objNCR001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.projectDescription).FirstOrDefault();
                    ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNCR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.Locations = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new SelectListItem() { Text = (x.Description).ToString().ToUpperInvariant(), Value = x.Code.ToString() }).Distinct().ToList();

                    //ViewBag.Locations = locs.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = clsImplementationEnum.convertNCRLocation(x), Selected = (clsImplementationEnum.convertNCRLocation(x).Equals(objNCR001.NCRLocation)) }).ToList();
                    //ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.TypeofItem_Assembly)) }).ToList();
                    //ViewBag.Disposition = disposition.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.Disposition)) }).ToList();
                    //if (!string.IsNullOrEmpty(objNCR001.ProblemDueTo))
                    //    ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.ProblemDueTo)) }).ToList();
                    //else ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();

                    //if (!string.IsNullOrEmpty(objNCR001.RootCauseCategory))
                    //    ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.RootCauseCategory)) }).ToList();
                    //else ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    var lstRole = (from a in db.ATH001
                                   join b in db.ATH004 on a.Role equals b.Id
                                   where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                   select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                    if (objNCR001.Status == clsImplementationEnum.NCRStatus.Implement.GetStringValue() && lstRole.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                    {
                        ViewBag.allowed = true;
                    }
                    else if (objNCR001.Owner.Trim() == objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.allowed = true;
                    }
                    else
                    {
                        ViewBag.allowed = false;
                    }
                    if (!string.IsNullOrEmpty(objNCR001.QCManager))
                    {
                        if (objNCR001.QCManager.Trim() == objClsLoginInfo.UserName.Trim())
                        {
                            ViewBag.Responsibleqcmanager = true;
                        }
                        else
                        {
                            ViewBag.Responsibleqcmanager = false;
                        }
                    }
                    else
                    {
                        ViewBag.Responsibleqcmanager = false;
                    }
                    ViewBag.qcengineer = objNCR001.QCEngineer;
                    // ViewBag.allowed = objNCR001.Owner.Equals(objClsLoginInfo.UserName);


                    //if (!string.IsNullOrEmpty(objNCR001.CategoryofNCR))
                    //    ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.CategoryDescription, CatID = x.Value }).ToList();
                    //else
                    if (!string.IsNullOrEmpty(objNCR001.CategoryofNCR))
                    {
                        ViewBag.CategoryOfNcrSelected = ncrCategories.Where(v => v.Value == objNCR001.CategoryofNCR).Select(x => x.CategoryDescription).FirstOrDefault();
                    }
                    if (objNCR001.SDE != null)
                    {
                        ViewBag.SDE = objNCR001.SDE;
                        objNCR001.SDE = GetCodeAndNameEmployee(objNCR001.SDE);


                    }
                    if (objNCR001.Owner != null)
                    {
                        if (objNCR001.Status == clsImplementationEnum.NCRStatus.Implement.GetStringValue())
                        {
                            objNCR001.Owner = GetCodeAndNameEmployee(objClsLoginInfo.UserName);
                        }
                        else
                        {
                            objNCR001.Owner = GetCodeAndNameEmployee(objNCR001.Owner);
                        }
                    }
                    ViewBag.Owner = objNCR001.Owner;
                    if (objNCR001.QCEngineer != null)

                        objNCR001.QCEngineer = GetCodeAndNameEmployee(objNCR001.QCEngineer);
                    if (objNCR001.QAEngineer != null)
                    {
                        ViewBag.QAEngineer = objNCR001.QAEngineer;
                        objNCR001.QAEngineer = GetCodeAndNameEmployee(objNCR001.QAEngineer);

                    }
                    if (objNCR001.RespDeptMgr != null)
                    {
                        ViewBag.RespDeptMgr = objNCR001.RespDeptMgr;
                        objNCR001.RespDeptMgr = GetCodeAndNameEmployee(objNCR001.RespDeptMgr);

                    }
                    if (objNCR001.QCManager != null)
                    {
                        ViewBag.QCManagerdept = objNCR001.QCManager;
                        objNCR001.QCManager = GetCodeAndNameEmployee(objNCR001.QCManager);
                    }
                    var assignee = objNCR001.NCR002.Select(x => x.Assignee).FirstOrDefault();
                    if (assignee != null)
                    {
                        ViewBag.Assignee = GetCodeAndNameEmployee(assignee);
                        if (db.NCR004.Where(x => x.NCRNo == objNCR001.NCRNo).Any())
                        {
                            ViewBag.AssigneeComment = db.NCR004.Where(x => (x.NCRNo == objNCR001.NCRNo || x.NCRNo == objNCR001.OldNCRNumber) && x.Assignee == assignee && x.TaskOrder == 1).OrderByDescending(x => x.Id).Select(x => x.Comments).FirstOrDefault();
                        }
                    }
                    if (objNCR001.RespDept != null)
                        objNCR001.RespDept = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx.Equals(objNCR001.RespDept)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    //ViewBag.TypeOfAssembly = toa.Where(i => i.Equals(objNCR001.TypeofItem_Assembly)).Select(x => new SelectListItem() { Text = x, Value = x, Selected = true }).ToList();
                    ViewBag.MinMaj = minmaj.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.NCRClass)) }).ToList();
                    ViewBag.Status = objNCR001.Status;
                    ViewBag.IsDisplayOnly = false;
                    bool Linkagebutton = true;
                    string RCAUrl = string.Empty;
                    string RCAstatus = string.Empty;
                    string RCANo = string.Empty;
                    string isRCAApplicable = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsRCAApplicable.GetStringValue());

                    if (objNCR001.Status == clsImplementationEnum.NCRStatus.Draft.GetStringValue() || objNCR001.Status == clsImplementationEnum.NCRStatus.Create.GetStringValue() || objNCR001.Status == clsImplementationEnum.NCRStatus.Validate.GetStringValue())
                    {
                        Linkagebutton = false;
                    }

                    clsHelper.ResponseMsgWithStatus objResponseMsgRCA = new clsHelper.ResponseMsgWithStatus();
                    if (Linkagebutton && isRCAApplicable == "true")
                    {
                        DirectoryService ds = new DirectoryService();
                        var upsno = ds.Encrypt(Convert.ToInt64(objClsLoginInfo.UserName).ToString("X"));

                        objResponseMsgRCA = (new GeneralController()).RCAStatus("V", "NCR", objNCR001.HeaderId);
                        var host = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.RCA_URL.GetStringValue()));
                        RCAUrl = host + "?upsno=" + upsno + "&ReferenceNo=" + objNCR001.HeaderId + "&IncidentType=NCR";
                        RCANo = objResponseMsgRCA.Value;
                        RCAstatus = !string.IsNullOrWhiteSpace(objResponseMsgRCA.dataValue) ? (objResponseMsgRCA.dataValue.TrimStart().TrimEnd()) : string.Empty;
                    }
                    ViewBag.RCAUrl = RCAUrl;
                    ViewBag.RCAstatus = RCAstatus;
                    ViewBag.RCANo = RCANo;
                    ViewBag.Action = "Edit";
                    ViewBag.isRCAApplicable = isRCAApplicable;
                }
                else
                {
                    ViewBag.Locations = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new SelectListItem() { Text = (x.Description).ToString().ToUpperInvariant(), Value = x.Code.ToString(), Selected = (x.Code.Equals(objClsLoginInfo.Location)) }).Distinct().ToList();

                    //ViewBag.Locations = locs.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = clsImplementationEnum.convertNCRLocation(x), Selected = (clsImplementationEnum.convertNCRLocation(x).Equals(objClsLoginInfo.Location)) }).ToList();
                    //ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    //ViewBag.Disposition = disposition.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    //ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    //ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.allowed = true;
                    ViewBag.MinMaj = minmaj.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
                    //ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();
                    objNCR001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    ViewBag.Status = objNCR001.Status;
                    ViewBag.Action = "AddNew";
                    //List<string> lstdrawing = drawref.getDocumentMasterList(projectdrawuing).ToList();
                    //ViewBag.refgrawing = lstdrawing.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    // ViewBag.refgrawing = null;
                    ViewBag.externalapprover = ""; ViewBag.QcManager = "false";
                }
                if (d == 1)
                {
                    ViewBag.disableAllInputs = true;
                    ViewBag.IsDisplayOnly = true;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            ViewBag.chkProject = Project;
            return View(objNCR001);
        }

        [SessionExpireFilter]
        public ActionResult ViewHeader(int? headerId)
        {
            ViewBag.disableAllInputs = true;
            NCR001 objNCR001 = new NCR001();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            try
            {
                string user = objClsLoginInfo.UserName;

                string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
                List<Projects> project = Manager.getProjectsByUser(user);
                List<CategoryData> ncrCategories = objModelWQRequest.GetSubCatagorywithoutBULocationCode("NCR Category").ToList();
                //List<string> ncrCategories = db.NCR006.Select(q => q.NCRCategory).Distinct().ToList();
                List<string> locs = clsImplementationEnum.getNCRLocation().ToList();
                List<string> ncrCat = clsImplementationEnum.getNCRLocation().ToList();
                List<string> toa = clsImplementationEnum.getTypeofAssembly().ToList();
                List<string> disposition = clsImplementationEnum.getDisposition().ToList();
                List<string> pdt = clsImplementationEnum.getProblemDueTo().ToList();
                List<string> rcc = clsImplementationEnum.getRootCauseCategory().ToList();
                List<string> minmaj = clsImplementationEnum.getMinMaj().ToList();
                List<GLB002> lstLocation = GetSubCatagories("NCR Location").ToList();
                if (headerId > 0)
                {
                    objNCR001 = db.NCR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    ViewBag.isQCManager = objClsLoginInfo.UserName == objNCR001.QCManager;
                    ViewBag.NCRNo = objNCR001.NCRNo;
                    //ViewBag.Project = project.Where(i => i.projectCode.Equals(objNCR001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.projectDescription).FirstOrDefault();
                    ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNCR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                    ViewBag.Locations = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new SelectListItem() { Text = (x.Description).ToString().ToUpperInvariant(), Value = x.Code.ToString() }).Distinct().ToList();

                    //ViewBag.Locations = locs.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = clsImplementationEnum.convertNCRLocation(x), Selected = (clsImplementationEnum.convertNCRLocation(x).Equals(objNCR001.NCRLocation)) }).ToList();
                    ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.TypeofItem_Assembly)) }).ToList();
                    ViewBag.Disposition = disposition.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.Disposition)) }).ToList();
                    if (!string.IsNullOrEmpty(objNCR001.ProblemDueTo))
                        ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.ProblemDueTo)) }).ToList();
                    else ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();

                    if (!string.IsNullOrEmpty(objNCR001.RootCauseCategory))
                        ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.RootCauseCategory)) }).ToList();
                    else ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();

                    if (!string.IsNullOrEmpty(objNCR001.CategoryofNCR))
                        ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value, Selected = (x.Equals(objNCR001.CategoryofNCR)) }).ToList();
                    else ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();

                    if (objNCR001.Owner != null)
                        objNCR001.Owner = GetCodeAndNameEmployee(objNCR001.Owner);
                    if (objNCR001.SDE != null)
                        objNCR001.SDE = GetCodeAndNameEmployee(objNCR001.SDE);
                    if (objNCR001.QCEngineer != null)
                        objNCR001.QCEngineer = GetCodeAndNameEmployee(objNCR001.QCEngineer);
                    if (objNCR001.QAEngineer != null)
                        objNCR001.QAEngineer = GetCodeAndNameEmployee(objNCR001.QAEngineer);
                    if (objNCR001.RespDeptMgr != null)
                        objNCR001.RespDeptMgr = GetCodeAndNameEmployee(objNCR001.RespDeptMgr);
                    if (objNCR001.QCManager != null)
                        objNCR001.QCManager = GetCodeAndNameEmployee(objNCR001.QCManager);
                    if (objNCR001.RespDept != null)
                        objNCR001.RespDept = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx.Equals(objNCR001.RespDept)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    //ViewBag.TypeOfAssembly = toa.Where(i => i.Equals(objNCR001.TypeofItem_Assembly)).Select(x => new SelectListItem() { Text = x, Value = x, Selected = true }).ToList();
                    ViewBag.MinMaj = minmaj.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001.NCRClass)) }).ToList();
                    ViewBag.Status = objNCR001.Status;
                    ViewBag.Action = "Edit";
                }
                else
                {
                    ViewBag.Locations = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new SelectListItem() { Text = (x.Description).ToString().ToUpperInvariant(), Value = x.Code.ToString(), Selected = (x.Code.Equals(objClsLoginInfo.Location)) }).Distinct().ToList();

                    //ViewBag.Locations = locs.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = clsImplementationEnum.convertNCRLocation(x), Selected = (clsImplementationEnum.convertNCRLocation(x).Equals(objClsLoginInfo.Location)) }).ToList();
                    ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.Disposition = disposition.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.MinMaj = minmaj.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
                    ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();
                    objNCR001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    ViewBag.Status = objNCR001.Status;
                    ViewBag.Action = "AddNew";
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return View("AddHeader", objNCR001);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult getDetails(string projectCd)
        {
            ProjectDetail objNCRCommon = new ProjectDetail();
            var SkipPLM = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SkipPLM.GetStringValue()));
            try
            {
                if (!string.IsNullOrEmpty(projectCd))
                {
                    objNCRCommon = Manager.getBUandContractProjectWise(projectCd);
                    string contract = objNCRCommon.contract;
                    objNCRCommon.NCRNo = getNextID();
                    List<string> lst = new List<string>();
                    bool IsPLMProject = Manager.IsPLMProject(projectCd);
                    List<string> lstdrawing = new List<string>();
                    if (IsPLMProject)
                    {
                        if (!SkipPLM)
                        {
                            IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();
                            lst = drawref.getDocumentMasterList(projectCd).ToList();
                        }
                        else
                        {
                            lst.Add("Dummy Drawing Number");
                        }
                    }
                    else
                    {
                        using (IEMQSDESEntities db = new IEMQSDESEntities())
                        {
                            try
                            {
                                List<string> lstProject = db.DES051.Where(x=>x.Contract == contract).Select(x=>x.Project).ToList();
                                lst = db.DES059.Where(x => x.DocumentTypeId == 1 && lstProject.Contains(x.Project) && x.DocumentNo != null && x.Status == "Completed" && x.IsLatestRevision == true).Select(x => x.DocumentNo).ToList();
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }
                    }

                    var lstDrawingNo = lst.AsEnumerable().Select(x => new DropdownModel { Code = x, Description = x }).Distinct().ToList();
                    objNCRCommon.DrawingNo = lstDrawingNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                List<string> lstdrawing = new List<string>();
                if (SkipPLM)
                    lstdrawing.Add("Dummy Drawing No");

                objNCRCommon.DrawingNo = lstdrawing.AsEnumerable().Select(x => new DropdownModel { Code = x, Description = x }).ToList();
            }
            return Json(objNCRCommon);
        }
        public string getNextID()
        {
            string id = "";
            int? i = (from n in db.NCR001
                      orderby n.HeaderId descending
                      select n.HeaderId).FirstOrDefault();
            if (i != null)
            {
                i++;
                id = "NCR-" + i.ToString().PadLeft(5, '0');
            }
            return id;
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult TemplateBodyEditPartial(int headerid)
        {
            MailConfiguration mailConfiguration = new MailConfiguration();
            ViewBag.HeaderId = headerid;
            return PartialView("_NCRMailTemplate", mailConfiguration);
        }
        public string getNextRouteID()
        {
            string id = "";
            int? i = (from n in db.NCR003
                      orderby n.Id descending
                      select n.Id).FirstOrDefault();
            if (i != null)
            {
                i++;
                id = "Route-" + i.ToString().PadLeft(5, '0');
            }
            return id;
        }
        public string getNextTaskID()
        {
            string id = "";
            int? i = (from n in db.NCR004
                      orderby n.Id descending
                      select n.Id).FirstOrDefault();
            if (i != null)
            {
                i++;
                id = "Task-" + i.ToString().PadLeft(5, '0');
            }
            return id;
        }

        [HttpPost]
        public ActionResult SaveAttachments(bool hasAttachments, Dictionary<string, string> Attach, int HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!db.NCR001.Any(q => q.HeaderId == HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not found.";
                }
                else
                {
                    Manager.ManageDocuments("NCR001/" + HeaderId, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Attachments Updated.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAttachments1(bool hasAttachments, Dictionary<string, string> Attach, int HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!db.NCR001.Any(q => q.HeaderId == HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not found.";
                }
                else
                {
                    Manager.ManageDocuments("NCR001/" + HeaderId + "/Drawing No", hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Attachments Updated.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult SaveHeader(NCR001 objNCR001, string[] ddlPart = null, string ddlAssignee = null, string[] ddlECR = null, string zProject = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (objNCR001 != null)
                {
                    if (!alreadyExist(objNCR001.NCRNo))
                    {
                        if (!string.IsNullOrEmpty(objNCR001.BU.ToString()))
                            objNCR001.BU = objNCR001.BU.Split('-')[0].Trim();
                        objNCR001.CreatedBy = objClsLoginInfo.UserName;
                        objNCR001.Originator = objClsLoginInfo.UserName;
                        objNCR001.Owner = objClsLoginInfo.UserName;
                        objNCR001.CreatedOn = DateTime.Now;
                        objNCR001.Createon = DateTime.Now;
                        objNCR001.FreezeNCR = "Defreeze";
                        objNCR001.Status = clsImplementationEnum.NCRStatus.Create.GetStringValue();
                        objNCR001.DrwgNo = Convert.ToString(objNCR001.DrwgNo);
                        if (ddlPart != null)
                            objNCR001.Part_Assembly = string.Join(",", ddlPart);
                        db.NCR001.Add(objNCR001);
                        db.SaveChanges();
                        NCR001 ncrNew = db.NCR001.Where(x => x.NCRNo == objNCR001.NCRNo).FirstOrDefault();
                        ViewBag.Status = ncrNew.Status;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString() + "#" + ncrNew.HeaderId + "#" + clsImplementationEnum.NCRStatus.Create.GetStringValue();
                    }
                    else
                    {
                        #region remove old assignee and add latest
                        if (objNCR001.Status == clsImplementationEnum.NCRStatus.Validate.GetStringValue())
                        {
                            List<NCR002> lstAssignee = db.NCR002.Where(x => x.NCRNo == objNCR001.NCRNo).ToList();
                            foreach (var row in lstAssignee)
                            {
                                db.NCR002.Remove(row);
                            }
                            db.SaveChanges();
                            if (ddlAssignee != null)
                            {
                                addAssignee(ddlAssignee, objNCR001.HeaderId, objNCR001.NCRNo);
                            }
                        }
                        #endregion
                        if (!string.IsNullOrEmpty(objNCR001.BU.ToString()))
                            objNCR001.BU = objNCR001.BU.Split('-')[0].Trim();
                        if (objNCR001.Owner != null)
                            objNCR001.Owner = objNCR001.Owner.Split('-')[0].Trim();
                        if (ddlPart != null)
                            objNCR001.Part_Assembly = string.Join(",", ddlPart);
                        if (ddlECR != null)
                            objNCR001.ECR = string.Join(",", ddlECR);
                        objNCR001.DrwgNo = Convert.ToString(objNCR001.DrwgNo);
                        //db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                        db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            ViewBag.chkProject = zProject;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult PromoteHeader(NCR001 objNCR001, string lastStage, string ddlAssignee = null, string[] ddlPart = null, string[] ddlECR = null)
        {
            var create = clsImplementationEnum.NCRStatus.Create.GetStringValue();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            if (headerAlreadyExist(objNCR001.HeaderId))
            {
                try
                {
                    if (objNCR001 != null)
                    {

                        if (lastStage.Equals(clsImplementationEnum.NCRStatus.Validate.GetStringValue()))
                        {
                            clsHelper.ResponseMsgWithStatus objResponseMsgRCA = new clsHelper.ResponseMsgWithStatus();
                            var deptname = Manager.getDepartmentDescription(objNCR001.RespDept);
                            var bu = db.COM002.Where(x => x.t_dimx == objNCR001.BU).Select(a => a.t_desc).FirstOrDefault();
                            string isRCAApplicable = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsRCAApplicable.GetStringValue());
                            if (isRCAApplicable == "true")
                            {
                                objResponseMsgRCA = (new GeneralController()).RCAStatus("I", "NCR", objNCR001.HeaderId, objNCR001.NCRLocation, bu, objNCR001.Project, DateTime.Now.ToString("yyyy-MM-dd"), objNCR001.Description, objNCR001.RespDeptMgr, deptname, objNCR001.QAEngineer);
                            }
                            else
                            {
                                objResponseMsgRCA.Key = true;
                            }
                            if (objResponseMsgRCA.Key)
                            {
                                List<NCR002> lstAssignee = db.NCR002.Where(x => x.NCRNo == objNCR001.NCRNo).ToList();
                                foreach (var row in lstAssignee)
                                {
                                    db.NCR002.Remove(row);
                                }
                                db.SaveChanges();
                                if (ddlAssignee != null)
                                {
                                    addAssignee(ddlAssignee, objNCR001.HeaderId, objNCR001.NCRNo);
                                }

                                //if (ddlAssignee != null)
                                //    addAssignee(ddlAssignee, objNCR001.HeaderId, objNCR001.NCRNo);
                                if (ddlPart != null)
                                    objNCR001.Part_Assembly = string.Join(",", ddlPart);
                                if (ddlECR != null)
                                    objNCR001.ECR = string.Join(",", ddlECR);
                                var nextState = GetNextStage(objNCR001);
                                var owner = GetOwnerFromStage(objNCR001, nextState);
                                db.NCR007.Add(new NCR007()
                                {
                                    NCRId = objNCR001.HeaderId,
                                    StageFrom = objNCR001.Status,
                                    StageTo = nextState.GetStringValue()
                                });
                                objNCR001.Status = nextState.GetStringValue();
                                objNCR001.Owner = owner;
                                db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                //stampStatusChangeAndSave(objNCR001, nextState.GetStringValue(), owner);
                                /*objNCR001.Status = nextState.GetStringValue();
                                objNCR001.Owner = owner;
                                db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();*/
                                db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = nextState.GetStringValue();
                                objResponseMsg.HeaderStatus = objResponseMsgRCA.dataValue;
                                objResponseMsg.ItemNumber1 = objResponseMsgRCA.Value;
                                #region SentEmail Notification
                                string emailTo = string.Empty;
                                emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                                SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId);
                                #endregion
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "RCA not generated.Please try again";
                            }
                        }
                        else if (lastStage.Equals(clsImplementationEnum.NCRStatus.QAApprove.GetStringValue()))
                        {
                            var nextState = GetNextStage(objNCR001);
                            var owner = GetOwnerFromStage(objNCR001, nextState);
                            if (ddlPart != null)
                                objNCR001.Part_Assembly = string.Join(",", ddlPart);
                            if (ddlECR != null)
                                objNCR001.ECR = string.Join(",", ddlECR);
                            db.NCR007.Add(new NCR007()
                            {
                                NCRId = objNCR001.HeaderId,
                                StageFrom = objNCR001.Status,
                                StageTo = nextState.GetStringValue()
                            });
                            objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //stampStatusChangeAndSave(objNCR001, nextState.GetStringValue(), owner);
                            /*objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();*/
                            db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                            bool routeCreated = db.NCR003.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == "Started"));
                            if (!routeCreated)
                            {
                                string routeId = createRoute(objNCR001.NCRNo, objNCR001.QCManager);
                                if (!string.IsNullOrEmpty(routeId))
                                    createTasks(objNCR001.NCRNo, objNCR001.SDE, routeId, objNCR001.HeaderId);
                            }

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = nextState.GetStringValue();

                            #region SentEmail Notification
                            string emailTo = string.Empty;
                            emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                            SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId);
                            #endregion
                        }
                        else if (lastStage.Equals(clsImplementationEnum.NCRStatus.Approve.GetStringValue()))// objNCR001.ExtrApprovalReq.ToLower().Equals("yes"))
                        {
                            var nextState = GetNextStage(objNCR001);
                            var owner = GetOwnerFromStage(objNCR001, nextState);
                            if (ddlPart != null)
                                objNCR001.Part_Assembly = string.Join(",", ddlPart);
                            if (ddlECR != null)
                                objNCR001.ECR = string.Join(",", ddlECR);
                            db.NCR007.Add(new NCR007()
                            {
                                NCRId = objNCR001.HeaderId,
                                StageFrom = objNCR001.Status,
                                StageTo = nextState.GetStringValue()
                            });
                            objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            // stampStatusChangeAndSave(objNCR001, nextState.GetStringValue(), owner);
                            /*objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();*/
                            db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                            //as per the observation 15558,15701 on 21-07-2018
                            //bool routeCreated = db.NCR003.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == "Started"));
                            //if (!routeCreated && nextState.GetStringValue().ToString() == clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue().ToString())
                            //{
                            //    string routeId = createRoute(objNCR001.NCRNo, objNCR001.QCManager);
                            //    if (!string.IsNullOrEmpty(routeId))
                            //        createTasks(objNCR001.NCRNo, objNCR001.SDE, routeId);
                            //}

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = nextState.GetStringValue();
                            #region SentEmail Notification
                            string emailTo = string.Empty;
                            emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                            SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId);
                            #endregion
                        }
                        else if (lastStage.Equals(clsImplementationEnum.NCRStatus.QCClosure.GetStringValue())) //Coment for testing && objNCR001.ExternClosureReq.ToLower().Equals("yes"))
                        {
                            var nextState = GetNextStage(objNCR001);
                            var owner = GetOwnerFromStage(objNCR001, nextState);
                            if (ddlPart != null)
                                objNCR001.Part_Assembly = string.Join(",", ddlPart);
                            if (ddlECR != null)
                                objNCR001.ECR = string.Join(",", ddlECR);
                            db.NCR007.Add(new NCR007()
                            {
                                NCRId = objNCR001.HeaderId,
                                StageFrom = objNCR001.Status,
                                StageTo = nextState.GetStringValue()
                            });
                            objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //  stampStatusChangeAndSave(objNCR001, nextState.GetStringValue(), owner);
                            /*objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();*/
                            db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                            //as per the observation 15558,15701 on 21-07-2018
                            //bool routeCreated = db.NCR003.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == "Started"));
                            //if (!routeCreated && nextState.GetStringValue() == clsImplementationEnum.NCRStatus.ExternalClosure.GetStringValue())
                            //{
                            //    string routeId = createRoute(objNCR001.NCRNo, objNCR001.QCManager);
                            //    if (!string.IsNullOrEmpty(routeId))
                            //        //createTasks(objNCR001.NCRNo, objNCR001.SDE, routeId); //as per the observation 15558 on 21-07-2018
                            //}
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = nextState.GetStringValue();
                            #region SentEmail Notification
                            string emailTo = string.Empty;
                            emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                            SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId);
                            #endregion
                        }
                        else if (lastStage.Equals(clsImplementationEnum.NCRStatus.Closed.GetStringValue()))
                        {
                            #region SentEmail Notification
                            string emailTo = string.Empty;
                            //emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                            emailTo += "S.Sivakumar@larsentoubro.com,NEHAL.GANDHI@larsentoubro.com";
                            SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId, true);
                            #endregion
                        }
                        else
                        {
                            var nextState = GetNextStage(objNCR001);
                            var owner = GetOwnerFromStage(objNCR001, nextState);
                            if (ddlPart != null)
                                objNCR001.Part_Assembly = string.Join(",", ddlPart);
                            if (ddlECR != null)
                                objNCR001.ECR = string.Join(",", ddlECR);
                            db.NCR007.Add(new NCR007()
                            {
                                NCRId = objNCR001.HeaderId,
                                StageFrom = objNCR001.Status,
                                StageTo = nextState.GetStringValue()
                            });
                            objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //  stampStatusChangeAndSave(objNCR001, nextState.GetStringValue(), owner);
                            /*objNCR001.Status = nextState.GetStringValue();
                            objNCR001.Owner = owner;
                            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();*/
                            db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = nextState.GetStringValue();

                            //Developed by Ajay Chauhan ObservationID 15558 on 18-07-2018
                            #region SentEmail Notification
                            string emailTo = string.Empty;
                            emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                            SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId);
                            #endregion
                        }
                    }
                }

                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
                }
            }
            else if (db.NCR001.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == create)))
            {
                NCR001 objNewNCR001 = new NCR001();
                objNewNCR001 = db.NCR001.FirstOrDefault(x => (x.NCRNo == objNCR001.NCRNo));
                var nextState = GetNextStage(objNewNCR001);
                var owner = GetOwnerFromStage(objNewNCR001, nextState);
                db.NCR007.Add(new NCR007()
                {
                    NCRId = objNewNCR001.HeaderId,
                    StageFrom = objNewNCR001.Status,
                    StageTo = nextState.GetStringValue()
                });
                objNewNCR001.Status = nextState.GetStringValue();
                objNewNCR001.Owner = owner;
                db.Entry(objNewNCR001).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = objResponseMsg.Value = nextState.GetStringValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult PromoteHeader_NB(NCR001 objNCR001, string lastStage, string ddlAssignee = null, string[] ddlPart = null, string[] ddlECR = null)
        {
            bool IsRouteCreateStage = false; string emailTo = string.Empty;
            var create = clsImplementationEnum.NCRStatus.Create.GetStringValue();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (headerAlreadyExist(objNCR001.HeaderId))
            {
                try
                {
                    if (objNCR001 != null)
                    {
                        var nextState = GetNextStage(objNCR001);
                        var owner = GetOwnerFromStage(objNCR001, nextState);
                        if (lastStage.Equals(clsImplementationEnum.NCRStatus.Validate.GetStringValue()))
                        {
                            if (ddlAssignee != null)
                                addAssignee(ddlAssignee, objNCR001.HeaderId, objNCR001.NCRNo);
                            //if (ddlPart != null)
                            //    objNCR001.Part_Assembly = string.Join(",", ddlPart);
                            //if (ddlECR != null)
                            //    objNCR001.ECR = string.Join(",", ddlECR);

                            //emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                        }
                        else if (lastStage.Equals(clsImplementationEnum.NCRStatus.QAApprove.GetStringValue()))
                        {
                            //var nextState = GetNextStage(objNCR001);
                            //var owner = GetOwnerFromStage(objNCR001, nextState);
                            //if (ddlPart != null)
                            //    objNCR001.Part_Assembly = string.Join(",", ddlPart);
                            //if (ddlECR != null)
                            //    objNCR001.ECR = string.Join(",", ddlECR);

                            //db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                            IsRouteCreateStage = true;
                            //bool routeCreated = db.NCR003.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == "Started"));
                            //if (!routeCreated)
                            //{
                            //    string routeId = createRoute(objNCR001.NCRNo, objNCR001.QCManager);
                            //    if (!string.IsNullOrEmpty(routeId))
                            //        createTasks(objNCR001.NCRNo, objNCR001.SDE, routeId);
                            //}

                            //emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                        }
                        //else if (lastStage.Equals(clsImplementationEnum.NCRStatus.Approve.GetStringValue()))// objNCR001.ExtrApprovalReq.ToLower().Equals("yes"))
                        //{
                        //    //var nextState = GetNextStage(objNCR001);
                        //    //var owner = GetOwnerFromStage(objNCR001, nextState);
                        //    if (ddlPart != null)
                        //        objNCR001.Part_Assembly = string.Join(",", ddlPart);
                        //    if (ddlECR != null)
                        //        objNCR001.ECR = string.Join(",", ddlECR);

                        //    //emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                        //}
                        //else if (lastStage.Equals(clsImplementationEnum.NCRStatus.QCClosure.GetStringValue())) //Coment for testing && objNCR001.ExternClosureReq.ToLower().Equals("yes"))
                        //{
                        //    if (ddlPart != null)
                        //        objNCR001.Part_Assembly = string.Join(",", ddlPart);
                        //    if (ddlECR != null)
                        //        objNCR001.ECR = string.Join(",", ddlECR);

                        //    //emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                        //}
                        //else
                        //{
                        //    if (ddlPart != null)
                        //        objNCR001.Part_Assembly = string.Join(",", ddlPart);
                        //    if (ddlECR != null)
                        //        objNCR001.ECR = string.Join(",", ddlECR);

                        //    //emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                        //}
                        if (ddlPart != null)
                            objNCR001.Part_Assembly = string.Join(",", ddlPart);
                        if (ddlECR != null)
                            objNCR001.ECR = string.Join(",", ddlECR);

                        db.NCR007.Add(new NCR007()
                        {
                            NCRId = objNCR001.HeaderId,
                            StageFrom = objNCR001.Status,
                            StageTo = nextState.GetStringValue()
                        });

                        objNCR001.Status = nextState.GetStringValue();
                        objNCR001.Owner = owner;
                        db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                        if (IsRouteCreateStage)
                        {
                            bool routeCreated = db.NCR003.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == "Started"));
                            if (!routeCreated)
                            {
                                string routeId = createRoute(objNCR001.NCRNo, objNCR001.QCManager);
                                if (!string.IsNullOrEmpty(routeId))
                                    createTasks(objNCR001.NCRNo, objNCR001.SDE, routeId, objNCR001.HeaderId);
                            }
                        }
                        #region SentEmail Notification
                        emailTo += Manager.GetMailIdFromPsNo(owner) + ";";
                        SentEmailNotificationStageWise(emailTo, objNCR001.NCRNo, objNCR001.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = nextState.GetStringValue();
                    }
                }

                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
                }
            }
            else if (db.NCR001.Any(x => (x.NCRNo == objNCR001.NCRNo) && (x.Status == create)))
            {
                NCR001 objNewNCR001 = new NCR001();
                objNewNCR001 = db.NCR001.FirstOrDefault(x => (x.NCRNo == objNCR001.NCRNo));
                var nextState = GetNextStage(objNewNCR001);
                var owner = GetOwnerFromStage(objNewNCR001, nextState);
                db.NCR007.Add(new NCR007()
                {
                    NCRId = objNewNCR001.HeaderId,
                    StageFrom = objNewNCR001.Status,
                    StageTo = nextState.GetStringValue()
                });
                objNewNCR001.Status = nextState.GetStringValue();
                objNewNCR001.Owner = owner;
                db.Entry(objNewNCR001).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = objResponseMsg.Value = nextState.GetStringValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [SessionExpireFilter]
        public ActionResult DemoteHeader(NCR001 objNCR001)
        {
            var comments = objNCR001.demotionComments;
            var ncr = objNCR001.NCRNo;
            objNCR001 = db.NCR001.FirstOrDefault(q => (q.NCRNo.Equals(ncr)));
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //if (objNCR001.Status.Equals(clsImplementationEnum.NCRStatus.QAApprove.GetStringValue()))
            //{
            var listOfRoute = db.NCR003.Where(x => (x.NCRNo == ncr) && (x.Status == "Started")).ToList();
            if (listOfRoute != null)
            {
                foreach (var route in listOfRoute)
                {
                    route.Status = "Finished";
                    route.RouteCompletionDate = DateTime.Now;
                    db.Entry(route).State = System.Data.Entity.EntityState.Modified;
                }
                var listOfTask = db.NCR004.Where(x => (x.NCRNo == ncr) && (x.Status == "Assigned" || x.Status == "Pending")).ToList();
                foreach (var task in listOfTask)
                {
                    task.Status = "Completed";
                    task.CompletionDate = DateTime.Now;
                    db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();
            }

            // }
            try
            {
                if (objNCR001 != null)
                {
                    var lastEntry = objNCR001.NCR007.OrderByDescending(q => q.Id).FirstOrDefault();
                    var newState = GetStatusFromString(lastEntry.StageFrom, objNCR001);
                    var owner = GetOwnerFromStage(objNCR001, newState);
                    objNCR001.Status = newState.GetStringValue();
                    objNCR001.demotionComments = comments;
                    objNCR001.Owner = owner;
                    db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                    db.NCR007.Remove(lastEntry);
                    db.SaveChanges();
                    db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = newState.GetStringValue();
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult ChangeNCRNo(string ncrNo, string project, int headerid)
        {
            string[] splitter = ncrNo.Split('-');
            string digits = splitter[1].Trim();
            string dig3 = digits.Substring(digits.Length - 3);
            int highest = 0;
            NCR001 objCurrentNCR = db.NCR001.Where(x => x.HeaderId == headerid).FirstOrDefault();


            NCR001 objDigitNCR = db.NCR001.Where(x => (x.Project == project)).OrderByDescending(x => x.NewNcrIdDigit).FirstOrDefault();

            //NCR001 objDigitNCR = db.NCR001.Where(x => (x.NCRNo == ncrNo) && (x.OldNCRNumber != null)).OrderByDescending(x => x.NewNcrIdDigit).FirstOrDefault();
            if (objDigitNCR != null)
            {
                highest = objDigitNCR.NewNcrIdDigit;
            }
            dig3 = (highest + 1).ToString().PadLeft(3, '0');
            string newNcr = project + "-" + "NCR-" + dig3;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                NCR001 objNCR001 = db.NCR001.FirstOrDefault(x => (x.NCRNo.Equals(ncrNo) && x.HeaderId == headerid));
                //add condition by zaid khandwani on 17/07/2018 at a time of KT
                if (objCurrentNCR.OldNCRNumber == null)
                {
                    if (objNCR001 != null)
                    {

                        objNCR001.OldNCRNumber = objNCR001.NCRNo;
                        objNCR001.NCRNo = newNcr;
                        objNCR001.NewNcrIdDigit = highest + 1;
                        objNCR001.ReferredtoCustomer = "Yes";
                        db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;

                        var listOfAssignee = db.NCR002.Where(x => x.NCRNo == ncrNo).ToList();
                        foreach (var assignee in listOfAssignee)
                        {
                            assignee.NCRNo = newNcr;
                            db.Entry(assignee).State = System.Data.Entity.EntityState.Modified;
                        }

                        var listOfRoute = db.NCR003.Where(x => x.NCRNo == ncrNo).ToList();
                        foreach (var route in listOfRoute)
                        {
                            route.NCRNo = newNcr;
                            db.Entry(route).State = System.Data.Entity.EntityState.Modified;
                        }
                        var listOfTask = db.NCR004.Where(x => x.NCRNo == ncrNo).ToList();
                        foreach (var task in listOfTask)
                        {
                            task.NCRNo = newNcr;
                            db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                        }
                        db.SaveChanges();
                    }
                }

                objResponseMsg.NewNcrIdDigit = objNCR001.NewNcrIdDigit;
                objResponseMsg.OldNCRNumber = objNCR001.OldNCRNumber;
                objResponseMsg.Key = true;
                objResponseMsg.Value = objNCR001.NCRNo;
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ncrNo"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult checkRouteComplete(string ncrNo)
        {
            bool result = false;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                result = db.NCR004.Any(x => (x.NCRNo == ncrNo) && (x.Status == "Pending" || x.Status == "Assigned"));
                objResponseMsg.Key = result;
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = result;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsImplementationEnum.NCRStatus GetStatusFromString(string stat, NCR001 obj)
        {
            if (stat.Equals(clsImplementationEnum.NCRStatus.Draft.GetStringValue()))
                return clsImplementationEnum.NCRStatus.Draft;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Create.GetStringValue()))
            {
                obj.ValidateOn = null;
                return clsImplementationEnum.NCRStatus.Create;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Validate.GetStringValue()))
            {
                obj.RespDeptOn = null;
                return clsImplementationEnum.NCRStatus.Validate;
            }
            //else if (stat.Equals(clsImplementationEnum.NCRStatus.Validate.GetStringValue()))
            //    return clsImplementationEnum.NCRStatus.Validate;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue()))
            {
                obj.QAApproveOn = null;
                return clsImplementationEnum.NCRStatus.ResponsibleDepartment;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QAApprove.GetStringValue()))
            {
                obj.ReviewOn = null;
                return clsImplementationEnum.NCRStatus.QAApprove;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ReviewComment.GetStringValue()))
            {
                obj.ApproveOn = null;
                return clsImplementationEnum.NCRStatus.ReviewComment;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Approve.GetStringValue()))
            {

                bool ea = obj.ExtrApprovalReq.ToLower().Equals("yes");
                bool ecr = obj.ECRRequired.ToLower().Equals("yes");
                bool rr = obj.RectificationRequired.ToLower().Equals("yes");
                if (ea)
                    obj.ExtApproveOn = null;
                else
                {
                    if (ecr)
                        obj.PlanImptOn = null;
                    else
                    {
                        if (rr)
                            obj.ImplementOn = null;
                        else
                            obj.QCClosureOn = null;
                    }
                }

                return clsImplementationEnum.NCRStatus.Approve;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue()))
            {

                bool ecr = obj.ECRRequired.ToLower().Equals("yes");
                bool rr = obj.RectificationRequired.ToLower().Equals("yes");
                if (ecr)
                    obj.PlanImptOn = null;
                else
                {
                    if (rr)
                        obj.ImplementOn = null;
                    else
                        obj.QCClosureOn = null;
                }

                return clsImplementationEnum.NCRStatus.ExternalApprove;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.PlanImplementation.GetStringValue()))
            {
                bool rr = obj.RectificationRequired.ToLower().Equals("yes");
                if (rr)
                    obj.ImplementOn = null;
                else
                    obj.QCClosureOn = null;
                return clsImplementationEnum.NCRStatus.PlanImplementation;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Implement.GetStringValue()))
            {
                obj.QCClosureOn = null;
                return clsImplementationEnum.NCRStatus.Implement;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QCClosure.GetStringValue()))
            {
                obj.ExtClosureOn = null;
                return clsImplementationEnum.NCRStatus.QCClosure;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ExternalClosure.GetStringValue()))
            {
                obj.QAClosureOn = null;
                return clsImplementationEnum.NCRStatus.ExternalClosure;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QAClosure.GetStringValue()))
            {
                obj.QAClosureOn = null;
                return clsImplementationEnum.NCRStatus.QAClosure;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Closed.GetStringValue()))
                return clsImplementationEnum.NCRStatus.Closed;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.MarkedForCancel.GetStringValue()))
                return clsImplementationEnum.NCRStatus.MarkedForCancel;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Cancelled.GetStringValue()))
                return clsImplementationEnum.NCRStatus.Cancelled;
            else
                return clsImplementationEnum.NCRStatus.Draft;
        }
        public clsImplementationEnum.NCRStatus GetNextStage(NCR001 objNCR001)
        {
            var stat = objNCR001.Status;
            if (stat.Equals(clsImplementationEnum.NCRStatus.Draft.GetStringValue()))
            {
                objNCR001.CreatedOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.Create;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Create.GetStringValue()))
            {
                objNCR001.SendForValidateOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.Validate;
            }
            //else if (stat.Equals(clsImplementationEnum.NCRStatus.SendForValidation.GetStringValue()))
            //    return clsImplementationEnum.NCRStatus.Validate;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Validate.GetStringValue()))
            {
                objNCR001.ValidateOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.ResponsibleDepartment;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue()))
            {
                objNCR001.RespDeptOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.QAApprove;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QAApprove.GetStringValue()))
            {
                objNCR001.QAApproveOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.ReviewComment;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ReviewComment.GetStringValue()))
            {
                objNCR001.ReviewOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.Approve;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Approve.GetStringValue()))
            {
                objNCR001.ApproveOn = DateTime.Now;
                bool ea = objNCR001.ExtrApprovalReq.ToLower().Equals("yes");
                bool ecr = objNCR001.ECRRequired.ToLower().Equals("yes");
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (ea)
                    return clsImplementationEnum.NCRStatus.ExternalApprove;
                else
                {
                    if (ecr)
                        return clsImplementationEnum.NCRStatus.PlanImplementation;
                    else
                    {
                        if (rr)
                            return clsImplementationEnum.NCRStatus.Implement;
                        else
                            return clsImplementationEnum.NCRStatus.QCClosure;
                    }
                }
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue()))
            {
                objNCR001.ExtApproveOn = DateTime.Now;
                bool ecr = objNCR001.ECRRequired.ToLower().Equals("yes");
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (ecr)
                    return clsImplementationEnum.NCRStatus.PlanImplementation;
                else
                {
                    if (rr)
                        return clsImplementationEnum.NCRStatus.Implement;
                    else
                        return clsImplementationEnum.NCRStatus.QCClosure;
                }
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.PlanImplementation.GetStringValue()))
            {
                objNCR001.PlanImptOn = DateTime.Now;
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (rr)
                    return clsImplementationEnum.NCRStatus.Implement;
                else
                    return clsImplementationEnum.NCRStatus.QCClosure;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Implement.GetStringValue()))
            {
                objNCR001.ImplementOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.QCClosure;
                //bool excr = objNCR001.ExternClosureReq.ToLower().Equals("yes");
                //bool minorMajor = objNCR001.NCRClass.ToLower().Equals("minor");
                //if (excr)
                //    return clsImplementationEnum.NCRStatus.QCClosure;
                //else
                //{
                //    if (minorMajor)
                //    {
                //        return clsImplementationEnum.NCRStatus.Closed;
                //    }
                //    else
                //    {
                //        return clsImplementationEnum.NCRStatus.QCClosure;
                //    }
                //}
            }

            else if (stat.Equals(clsImplementationEnum.NCRStatus.QCClosure.GetStringValue()))
            {
                objNCR001.QCClosureOn = DateTime.Now;
                bool excr = objNCR001.ExternClosureReq.ToLower().Equals("yes");

                if (excr)
                    return clsImplementationEnum.NCRStatus.ExternalClosure;
                else
                {
                    #region Added by Anil Hadiyal on 04-01-2017 (observation : 14264 )
                    bool isMinor = objNCR001.NCRClass.ToLower().Equals("minor");
                    if (isMinor)
                    {
                        objNCR001.QCClosureOn = DateTime.Now;
                        return clsImplementationEnum.NCRStatus.Closed;
                    }
                    else
                    {
                        return clsImplementationEnum.NCRStatus.QAClosure;
                    }
                    #endregion
                    //return clsImplementationEnum.NCRStatus.QAClosure;
                }

            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ExternalClosure.GetStringValue()))
            {
                objNCR001.ExtClosureOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.QAClosure;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QAClosure.GetStringValue()))
            {
                objNCR001.QAClosureOn = DateTime.Now;
                return clsImplementationEnum.NCRStatus.Closed;
            }
            else
                return clsImplementationEnum.NCRStatus.Draft;
        }
        public string GetOwnerFromStage(NCR001 objNCR001, clsImplementationEnum.NCRStatus stat)
        {
            if (stat.Equals(clsImplementationEnum.NCRStatus.Draft))
                return objNCR001.CreatedBy;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Create))
                return objNCR001.Originator;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Validate))
                return objNCR001.QCEngineer;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ResponsibleDepartment))
                return objNCR001.RespDeptMgr;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QAApprove))
                return objNCR001.QAEngineer;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ReviewComment))
                return objNCR001.SDE;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Approve))
                return objNCR001.QCManager;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ExternalApprove))
            {
                bool ea = objNCR001.ExtrApprovalReq.ToLower().Equals("yes");
                bool ecr = objNCR001.ECRRequired.ToLower().Equals("yes");
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (ea)
                    return objNCR001.ExternalApprover;
                else
                {
                    if (ecr)
                        return objNCR001.SDE;
                    else
                    {
                        if (rr)
                            return objNCR001.QCEngineer;
                        else
                            return objNCR001.QCManager;
                    }
                }
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.PlanImplementation))
            {
                bool ecr = objNCR001.ECRRequired.ToLower().Equals("yes");
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (ecr)
                    return objNCR001.SDE;
                else
                {
                    if (rr)
                        return objNCR001.QCEngineer;
                    else
                        return objNCR001.QCManager;
                }
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.Implement))
            {
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (rr)
                    return objNCR001.QCEngineer;
                else
                    return objNCR001.QCManager;
            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QCClosure))
                return objNCR001.QCManager;
            else if (stat.Equals(clsImplementationEnum.NCRStatus.ExternalClosure))
            {
                bool excr = objNCR001.ExternClosureReq.ToLower().Equals("yes");
                bool minorMajor = objNCR001.NCRClass.ToLower().Equals("minor");
                if (excr)
                    return objNCR001.ExternClosureAgent;
                else
                {
                    if (minorMajor)
                    {
                        return objNCR001.QCManager;
                    }
                    else
                    {
                        return objNCR001.QAEngineer;
                    }
                }

            }
            else if (stat.Equals(clsImplementationEnum.NCRStatus.QAClosure))
            {
                return objNCR001.QAEngineer;
            }
            else
                return objNCR001.QAEngineer; // set QAEngineer as per the requirements Observation 15701

            //else if (stat.Equals(clsImplementationEnum.NCRStatus.QAClosure))
            //    return objNCR001.QCManager;
            //else
            //    return "";
        }

        public ActionResult GetNCRData(string ECR, string Project)
        {
            var data = Manager.GetNCRData(ECR, Project);
            var toSend = data.Select(q => new { text = q.ECRName + " - " + q.ECRDescription, id = q.ECRName }).ToArray();
            return Json(toSend, JsonRequestBehavior.AllowGet);
        }

        public void addAssignee(string assignee, int HeaderId, string NCRNo)
        {
            //for (int i = 0; i < assignee.Length; i++) // Observation 15701
            //{
            NCR002 objNCR002 = new NCR002();
            objNCR002.HeaderId = HeaderId;
            objNCR002.NCRNo = NCRNo;
            objNCR002.Assignee = assignee;
            objNCR002.CreatedBy = objClsLoginInfo.UserName;
            objNCR002.CreatedOn = DateTime.Now;
            db.NCR002.Add(objNCR002);
            db.SaveChanges();
            //}

        }
        public bool alreadyExist(string ncrNo)
        {
            bool result = db.NCR001.Any(x => x.NCRNo == ncrNo);
            return result;
        }
        public bool headerAlreadyExist(int headerId)
        {
            bool result = db.NCR001.Any(x => x.HeaderId == headerId);
            return result;
        }
        public string createRoute(string NCRNo, string QCmanager)
        {
            NCR003 objNCR003 = new NCR003();
            string rId = getNextRouteID();
            objNCR003.RouteName = rId;
            objNCR003.NCRNo = NCRNo;
            objNCR003.Status = "Started";
            objNCR003.RouteCompletionAction = "Promote Connected Object";
            objNCR003.RouteBasePurpose = "Standard";
            objNCR003.CreatedBy = QCmanager;
            objNCR003.CreatedOn = DateTime.Now;
            db.NCR003.Add(objNCR003);
            db.SaveChanges();
            return rId;
        }

        public void createTasks(string NCRNo, string SDE, string routeName, int headerid)
        {
            string emailTo = string.Empty;
            //List<string> assignees = db.NCR002.Where(z => z.NCRNo.Equals(NCRNo)).Select(z => z.Assignee).Distinct().ToList(); // Observation 15701
            string assignees = db.NCR002.Where(z => z.NCRNo.Equals(NCRNo)).Select(z => z.Assignee).Distinct().FirstOrDefault();
            if (assignees != null)
            {
                NCR004 objNCR004 = new NCR004();
                //for (int i = 0; i < assignees.Count(); i++)
                //{
                objNCR004.Task = getNextTaskID();
                objNCR004.NCRNo = NCRNo;
                objNCR004.RouteName = routeName;
                objNCR004.TaskOrder = 1;
                objNCR004.Title = "Comments";
                objNCR004.Action = "Approve";
                objNCR004.Assignee = assignees;
                objNCR004.Status = "Assigned";
                db.NCR004.Add(objNCR004);
                db.SaveChanges();

                emailTo += Manager.GetMailIdFromPsNo(objNCR004.Assignee) + ";";
                //}
                objNCR004.Task = getNextTaskID();
                objNCR004.NCRNo = NCRNo;
                objNCR004.RouteName = routeName;
                objNCR004.TaskOrder = 2;
                objNCR004.Title = "Comments";
                objNCR004.Assignee = SDE;
                objNCR004.Status = "Pending";
                db.NCR004.Add(objNCR004);
                db.SaveChanges();

                #region SentEmail Notification
                emailTo += Manager.GetMailIdFromPsNo(objNCR004.Assignee) + ";";
                SentEmailNotificationStageWise(emailTo, NCRNo, headerid);
                #endregion
            }
        }

        public ActionResult GetNCRHeaderGridDataPartial(string stage, string status)
        {
            ViewBag.status = status;
            ViewBag.stage = stage;
            return PartialView("_GetNCRHeaderGridDataPartial");
        }

        public ActionResult cancelNCR(string status, string ncrNo, string comments)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string newStatus = "";
            if (status.Equals(clsImplementationEnum.NCRStatus.MarkedForCancel.GetStringValue()))
            {
                newStatus = clsImplementationEnum.NCRStatus.Cancelled.GetStringValue();
            }
            else
            {
                newStatus = clsImplementationEnum.NCRStatus.MarkedForCancel.GetStringValue();
            }
            try
            {

                NCR001 objNCR001 = db.NCR001.FirstOrDefault(x => (x.NCRNo.Equals(ncrNo)));
                if (objNCR001 != null)
                {
                    objNCR001.cancelledcomments = comments;
                    objNCR001.Status = newStatus;
                    objNCR001.Owner = objNCR001.CreatedBy;
                    db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    db.SP_NCR_PROMOTE_DEMOTE(objNCR001.HeaderId);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = newStatus;
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNCRTaskGridDataPartial(string route = null, string tab = null)
        {
            ViewBag.tab = tab;
            return PartialView("_GetNCRTaskGridDataPartial");
        }
        [HttpPost]
        public ActionResult GetHistoryView(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_NCRHistoryGrid");
        }
        [HttpPost]
        public JsonResult LoadHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string strWhere = "1=1 and HeaderId=" + HeaderId;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or NCRNo like '%" + param.sSearch
                         + "%' or Description like '%" + param.sSearch
                         + "%' or NCRLocation like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                var lstResult = db.SP_NCR_GET_HEADERHISTORYDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                //Convert.ToString(uc.HeaderId),
                                // Convert.ToString(uc.Project),
                                //Convert.ToString(uc.NCRNo),
                                // Convert.ToString(uc.Description),
                                // Convert.ToString(uc.NCRLocation),
                                // Convert.ToString(uc.Status),
                                //Convert.ToString(uc.CreatedBy),
                                // Convert.ToString(db.NCR001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId)).Id)
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.NCRNo),
                                Convert.ToString(uc.Project),
                               Convert.ToString(uc.Description),
                               Convert.ToString(uc.NCRLocation),
                               Convert.ToString(uc.Status),
                              Convert.ToString(uc.CreatedBy),
                               Convert.ToString(db.NCR001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status, string stage, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    bool RefToCust = Convert.ToBoolean(param.IsVisible);

                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string whereCondition = "1=1";
                    if (RefToCust)
                    {
                        whereCondition += " and ReferredtoCustomer='Yes'";
                    }
                    if (status.ToLower().Equals("pending"))
                    {
                        switch (stage)
                        {
                            case "Maintain":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Create.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.MarkedForCancel.GetStringValue() + "') and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "Validate":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.Validate.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "Capa":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "QA":
                                whereCondition += " and (Status='" + clsImplementationEnum.NCRStatus.QAApprove.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "') or ( QAEngineer='" + objClsLoginInfo.UserName.ToString() + "' and Status='" + clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue() + "' and RCAStatus ='" + clsImplementationEnum.RCAStatus.SubmittoInitiator.GetStringValue() + "')";
                                break;
                            case "ReviewComment":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ReviewComment.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "QC":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "External":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "PlanImplementation":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.PlanImplementation.GetStringValue() + "') and ECRRequired ='Yes' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "Implement":
                                var lstRole = (from a in db.ATH001
                                               join b in db.ATH004 on a.Role equals b.Id
                                               where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                               select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                                if (lstRole.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                                {
                                    whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Implement.GetStringValue() + "') and RectificationRequired='Yes' ";
                                    //whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.Implement.GetStringValue() + "'  or Status='" + clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue() + "') and RectificationRequired='Yes' ";// and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                }
                                break;
                            case "QCClosure":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.QCClosure.GetStringValue() + "') and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "ExternalClosure":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ExternalClosure.GetStringValue() + "'and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "QAClosure":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.QCClosure.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.QAClosure.GetStringValue() + "') and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;

                        }

                    }
                    whereCondition += " and Project='" + Project.ToString() + "'";
                    string strSortOrder = string.Empty;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    else if (status.ToLower() == "All" && param.IsDisplayOnly == false)
                    {
                        whereCondition += " and CreatedBy = '" + objClsLoginInfo.UserName.ToString() + "' ";
                    }
                    string[] columnName = { "NCRNo", "OldNCRNumber", "Description", "Project", "NCRLocation", "BU", "RespDept", "QCEngineer", "RespDeptMgr", "Status" };
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    var lstHeader = db.SP_NCR_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstHeader = lstHeader.Where(x => x.Project.Contains(Project)).ToList();
                    int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstHeader
                              select new[] {
                              Convert.ToString(h.NCRNo),
                              Convert.ToString(h.OldNCRNumber),
                              Convert.ToString(h.Project),
                              Convert.ToString(h.HeaderId),
                              Convert.ToString(h.Description),
                              Convert.ToString(h.BU),
                              Convert.ToString(h.NCRLocation),
                              Convert.ToString(h.Status),
                              Convert.ToString(h.CreatedBy),
                              //Convert.ToString(status.ToUpper() == "ALL"),
                              //Convert.ToString(h.QCEngineer),
                              Convert.ToString(h.Description),
                              Convert.ToString(h.RCAStatus),
                              Convert.ToString(h.RCANumber),
                              //Convert.ToString(h.RespDeptMgr),
                              GridButtons(h.HeaderId,Project)
                    };

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    bool RefToCust = Convert.ToBoolean(param.IsVisible);

                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string whereCondition = "1=1";
                    if (RefToCust)
                    {
                        whereCondition += " and ReferredtoCustomer='Yes'";
                    }
                    if (status.ToLower().Equals("pending"))
                    {
                        switch (stage)
                        {
                            case "Maintain":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Create.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.MarkedForCancel.GetStringValue() + "') and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "Validate":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.Validate.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "Capa":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "QA":
                                whereCondition += " and (Status='" + clsImplementationEnum.NCRStatus.QAApprove.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "') or ( QAEngineer='" + objClsLoginInfo.UserName.ToString() + "' and Status='" + clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue() + "' and RCAStatus ='" + clsImplementationEnum.RCAStatus.SubmittoInitiator.GetStringValue() + "')";
                                break;
                            case "ReviewComment":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ReviewComment.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "QC":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "External":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue() + "' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "PlanImplementation":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.PlanImplementation.GetStringValue() + "') and ECRRequired ='Yes' and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "Implement":
                                var lstRole = (from a in db.ATH001
                                               join b in db.ATH004 on a.Role equals b.Id
                                               where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                               select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                                if (lstRole.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                                {
                                    whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Implement.GetStringValue() + "') and RectificationRequired='Yes' ";
                                    //whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.Implement.GetStringValue() + "'  or Status='" + clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue() + "') and RectificationRequired='Yes' ";// and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                }
                                break;
                            case "QCClosure":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.Approve.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.QCClosure.GetStringValue() + "') and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "ExternalClosure":
                                whereCondition += "and Status='" + clsImplementationEnum.NCRStatus.ExternalClosure.GetStringValue() + "'and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;
                            case "QAClosure":
                                whereCondition += "and (Status='" + clsImplementationEnum.NCRStatus.QCClosure.GetStringValue() + "' or Status='" + clsImplementationEnum.NCRStatus.QAClosure.GetStringValue() + "') and Owner = '" + objClsLoginInfo.UserName.ToString() + "'";
                                break;

                        }

                    }

                    string strSortOrder = string.Empty;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    else if (status.ToLower() == "All" && param.IsDisplayOnly == false)
                    {
                        whereCondition += " and CreatedBy = '" + objClsLoginInfo.UserName.ToString() + "' ";
                    }
                    string[] columnName = { "NCRNo", "OldNCRNumber", "ncr1.Description", "Project", "NCRLocation", "BU", "RespDept", "QCEngineer", "RespDeptMgr", "Status" };
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    var lstHeader = db.SP_NCR_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstHeader
                              select new[] {
                              Convert.ToString(h.NCRNo),
                              Convert.ToString(h.OldNCRNumber),
                              Convert.ToString(h.Project),
                              Convert.ToString(h.HeaderId),
                              Convert.ToString(h.Description),
                              Convert.ToString(h.BU),
                              Convert.ToString(h.NCRLocation),
                              Convert.ToString(h.Status),
                              Convert.ToString(h.CreatedBy),
                              //Convert.ToString(status.ToUpper() == "ALL"),
                              //Convert.ToString(h.QCEngineer),
                              Convert.ToString(h.Description),
                              Convert.ToString(h.RCAStatus),
                              Convert.ToString(h.RCANumber),
                              //Convert.ToString(h.RespDeptMgr),
                              GridButtons(h.HeaderId,Project="")
                    };

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GridButtons(int headerid, string Project)
        {
            if (Project != null && Project != "")
            {
                // NCR Search Keyword start               
                var objncr = db.NCR001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                // End

                var buttons = "";
                buttons = "<a href='" + WebsiteURL + "/NCR/MaintainNCR/AddHeader?headerID=" + headerid + "&Project=" + Project + "' title='View' style='cursor:pointer;' name='btnAction' id='btnEdit'><i class='iconspace fa fa-eye'></i></a>" + " "
                        + "<i class='iconspace fa fa-clock-o' href='javascript:void(0)' title='Show Timeline' onclick=ShowTimeline('/NCR/MaintainNCR/ShowTimeline?HeaderID=" + headerid + "');></i>" + " "
                        + "<i class='iconspace fa fa-history' href='javascript:void(0)' title='History Details' onclick=ViewHistoryData(" + headerid + ");></i>" + " "
                        + "<a class='iconspace fa fa-files-o'  title='Copy Details' href='" + WebsiteURL + "/NCR/MaintainNCR/CopyHeader?headerId=" + headerid + "';></a>" + " "
                        + "<i class='iconspace fa fa-print' title='Print' onclick=PrintReport(" + headerid + ");></i>" + " "
                        + "<i class='fa fa-users' href='javascript:void(0)'  title='Responsible person' onclick=Responsibleperson(" + headerid + ");></i>" + " ";
                buttons += Helper.GenerateActionIcon(headerid, "", "View Attachments", "fa fa-paperclip " + ((new clsFileUpload()).CheckAnyFileUploadInFolder("NCR001/" + headerid.ToString()) ? "hasattachment" : ""), "showAttachments(this, " + headerid + ",'false')");
                if (objncr.Status == clsImplementationEnum.NCRStatus.Closed.GetStringValue())
                {
                    buttons += "<a class='iconspace fa fa-search' target='_blank'  title='Search Keyword' href='" + WebsiteURL + "/NCR/MaintainNCR/NCRSearch?ncrNo=" + objncr.NCRNo + "';></a>" + " ";
                }
                return buttons;
            }
            else
            {
                // NCR Search Keyword start               
                var objncr = db.NCR001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                // End

                var buttons = "";
                buttons = "<a href='" + WebsiteURL + "/NCR/MaintainNCR/AddHeader?headerID=" + headerid + "' title='View' style='cursor:pointer;' name='btnAction' id='btnEdit'><i class='iconspace fa fa-eye'></i></a>" + " "
                        + "<i class='iconspace fa fa-clock-o' href='javascript:void(0)' title='Show Timeline' onclick=ShowTimeline('/NCR/MaintainNCR/ShowTimeline?HeaderID=" + headerid + "');></i>" + " "
                        + "<i class='iconspace fa fa-history' href='javascript:void(0)' title='History Details' onclick=ViewHistoryData(" + headerid + ");></i>" + " "
                        + "<a class='iconspace fa fa-files-o'  title='Copy Details' href='" + WebsiteURL + "/NCR/MaintainNCR/CopyHeader?headerId=" + headerid + "';></a>" + " "
                        + "<i class='iconspace fa fa-print' title='Print' onclick=PrintReport(" + headerid + ");></i>" + " "
                        + "<i class='fa fa-users' href='javascript:void(0)'  title='Responsible person' onclick=Responsibleperson(" + headerid + ");></i>" + " ";
                buttons += Helper.GenerateActionIcon(headerid, "", "View Attachments", "fa fa-paperclip " + ((new clsFileUpload()).CheckAnyFileUploadInFolder("NCR001/" + headerid.ToString()) ? "hasattachment" : ""), "showAttachments(this, " + headerid + ",'false')");
                if (objncr.Status == clsImplementationEnum.NCRStatus.Closed.GetStringValue())
                {
                    buttons += "<a class='iconspace fa fa-search' target='_blank'  title='Search Keyword' href='" + WebsiteURL + "/NCR/MaintainNCR/NCRSearch?ncrNo=" + objncr.NCRNo + "';></a>" + " ";
                }
                return buttons;
            }

        }
        public ActionResult NCRHistoryDetail(int Id = 0)
        {
            NCR001_Log objNCR001_log = new NCR001_Log();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            try
            {
                string user = objClsLoginInfo.UserName;

                string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
                List<Projects> project = Manager.getProjectsByUser(user);
                List<CategoryData> ncrCategories = objModelWQRequest.GetSubCatagorywithoutBULocationCode("NCR Category").ToList();
                //List<string> ncrCategories = db.NCR006.Select(q => q.NCRCategory).Distinct().ToList();
                List<string> locs = clsImplementationEnum.getNCRLocation().ToList();
                List<string> ncrCat = clsImplementationEnum.getNCRLocation().ToList();
                List<string> toa = clsImplementationEnum.getTypeofAssembly().ToList();
                List<string> disposition = clsImplementationEnum.getDisposition().ToList();
                List<string> pdt = clsImplementationEnum.getProblemDueTo().ToList();
                List<string> rcc = clsImplementationEnum.getRootCauseCategory().ToList();
                List<string> minmaj = clsImplementationEnum.getMinMaj().ToList();
                List<GLB002> lstLocation = GetSubCatagories("NCR Location").ToList();

                if (Id > 0)
                {
                    objNCR001_log = db.NCR001_Log.Where(i => i.HeaderId == Id).FirstOrDefault();
                    ViewBag.NCRNo = objNCR001_log.NCRNo;
                    //ViewBag.Project = project.Where(i => i.projectCode.Equals(objNCR001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.projectDescription).FirstOrDefault();
                    ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNCR001_log.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                    ViewBag.Locations = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new SelectListItem() { Text = (x.Description).ToString().ToUpperInvariant(), Value = x.Code.ToString() }).Distinct().ToList();

                    //ViewBag.Locations = locs.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = clsImplementationEnum.convertNCRLocation(x), Selected = (clsImplementationEnum.convertNCRLocation(x).Equals(objNCR001_log.NCRLocation)) }).ToList();
                    ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001_log.TypeofItem_Assembly)) }).ToList();
                    ViewBag.Disposition = disposition.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001_log.Disposition)) }).ToList();
                    if (!string.IsNullOrEmpty(objNCR001_log.ProblemDueTo))
                        ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001_log.ProblemDueTo)) }).ToList();
                    else ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();

                    if (!string.IsNullOrEmpty(objNCR001_log.RootCauseCategory))
                        ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001_log.RootCauseCategory)) }).ToList();
                    else ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();

                    ViewBag.allowed = objNCR001_log.Owner.Equals(objClsLoginInfo.UserName);


                    if (!string.IsNullOrEmpty(objNCR001_log.CategoryofNCR))
                        ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value, Selected = (x.Equals(objNCR001_log.CategoryofNCR)) }).ToList();
                    else ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();
                    if (objNCR001_log.SDE != null)
                        objNCR001_log.SDE = GetCodeAndNameEmployee(objNCR001_log.SDE);
                    if (objNCR001_log.Owner != null)
                        objNCR001_log.Owner = GetCodeAndNameEmployee(objNCR001_log.Owner);
                    if (objNCR001_log.QCEngineer != null)
                        objNCR001_log.QCEngineer = GetCodeAndNameEmployee(objNCR001_log.QCEngineer);
                    if (objNCR001_log.QAEngineer != null)
                        objNCR001_log.QAEngineer = GetCodeAndNameEmployee(objNCR001_log.QAEngineer);
                    if (objNCR001_log.RespDeptMgr != null)
                        objNCR001_log.RespDeptMgr = GetCodeAndNameEmployee(objNCR001_log.RespDeptMgr);
                    if (objNCR001_log.QCManager != null)
                        objNCR001_log.QCManager = GetCodeAndNameEmployee(objNCR001_log.QCManager);
                    if (objNCR001_log.RespDept != null)
                        objNCR001_log.RespDept = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx.Equals(objNCR001_log.RespDept)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    //ViewBag.TypeOfAssembly = toa.Where(i => i.Equals(objNCR001.TypeofItem_Assembly)).Select(x => new SelectListItem() { Text = x, Value = x, Selected = true }).ToList();
                    ViewBag.MinMaj = minmaj.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x, Selected = (x.Equals(objNCR001_log.NCRClass)) }).ToList();
                    ViewBag.Status = objNCR001_log.Status;
                    ViewBag.Action = "Edit";
                }
                else
                {

                    ViewBag.Locations = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new SelectListItem() { Text = (x.Description).ToString().ToUpperInvariant(), Value = x.Code.ToString(), Selected = (x.Code.Equals(objClsLoginInfo.Location)) }).Distinct().ToList();

                    //ViewBag.Locations = locs.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = clsImplementationEnum.convertNCRLocation(x), Selected = (clsImplementationEnum.convertNCRLocation(x).Equals(objClsLoginInfo.Location)) }).ToList();
                    ViewBag.TypeOfAssembly = toa.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.Disposition = disposition.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.ProblemDueTo = pdt.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.RootCauseCategory = rcc.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.allowed = true;
                    ViewBag.MinMaj = minmaj.AsEnumerable().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                    ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
                    ViewBag.CategoryOfNcr = ncrCategories.AsEnumerable().Select(x => new SelectListItem() { Text = x.CategoryDescription, Value = x.Value }).ToList();
                    objNCR001_log.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    ViewBag.Status = objNCR001_log.Status;
                    ViewBag.Action = "AddNew";
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return View(objNCR001_log);
        }
        public ActionResult loadRouteDataTable(JQueryDataTableParamModel param, string ncrNo)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                if (!string.IsNullOrEmpty(ncrNo))
                {
                    whereCondition += "and NCRNo='" + ncrNo + "'";
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "NCRNo", "RouteName", "Status", "RouteBasePurpose", "RouteCompletionAction", "RouteCompletionDate" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstRoute = db.SP_NCR_GETROUTE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstRoute.Select(i => i.TotalCount).FirstOrDefault();
                var res = from r in lstRoute
                          select new[] {
                              Convert.ToString(r.NCRNo),
                              Convert.ToString(r.RouteName),
                              Convert.ToString(r.Status),
                              Convert.ToString(r.RouteBasePurpose),
                              Convert.ToString(r.RouteCompletionAction),
                              Convert.ToString(r.RouteCompletionDate),
                              Convert.ToString(r.Id)
                            //  Convert.ToString(""),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadTaskDataTable(JQueryDataTableParamModel param, string tab, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string whereCondition = "1=1";
                    if (!string.IsNullOrEmpty(tab) && tab.ToLower().Equals("pending"))
                        whereCondition += " and Assignee = '" + objClsLoginInfo.UserName.ToString() + "' and (ncr4.Status='Assigned' or ncr4.Status='Pending')";
                    if (!string.IsNullOrEmpty(tab) && tab.ToLower().Equals("all"))
                        whereCondition += " and Assignee = '" + objClsLoginInfo.UserName.ToString() + "' ";

                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    string[] columnName = { "ncr4.NCRNo", "ncr4.RouteName", "ncr4.Task", "ncr4.Title", "ncr4.TaskOrder", "ncr4.Status", "ncr4.CompletionDate", "ncr4.Comments" };
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    whereCondition += " and abc.Project='" + Project.ToString() + "'";

                    var lstTask = db.SP_NCR_GETTASK(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstTask = lstTask.Where(x => x.Project.Contains(Project)).ToList();

                    int? totalRecords = lstTask.Select(i => i.TotalCount).FirstOrDefault();
                    var res = from r in lstTask
                              select new[] {
                              Convert.ToString(r.NCRNo),
                              Convert.ToString(r.RouteName),
                              Convert.ToString(r.Task),
                              Convert.ToString(r.Title),
                              Convert.ToString(r.TaskOrder),
                              Convert.ToString(r.Status),
                              Convert.ToString(r.CompletionDate),
                              Convert.ToString(r.Comments),
                              Convert.ToString(r.Id)
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string whereCondition = "1=1";
                    if (!string.IsNullOrEmpty(tab) && tab.ToLower().Equals("pending"))
                        whereCondition += " and Assignee = '" + objClsLoginInfo.UserName.ToString() + "' and (Status='Assigned' or Status='Pending')";
                    if (!string.IsNullOrEmpty(tab) && tab.ToLower().Equals("all"))
                        whereCondition += " and Assignee = '" + objClsLoginInfo.UserName.ToString() + "' ";

                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    string[] columnName = { "NCRNo", "RouteName", "Task", "Title", "TaskOrder", "Status", "CompletionDate", "Comments" };
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }
                    var lstTask = db.SP_NCR_GETTASK(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    int? totalRecords = lstTask.Select(i => i.TotalCount).FirstOrDefault();
                    var res = from r in lstTask
                              select new[] {
                              Convert.ToString(r.NCRNo),
                              Convert.ToString(r.RouteName),
                              Convert.ToString(r.Task),
                              Convert.ToString(r.Title),
                              Convert.ToString(r.TaskOrder),
                              Convert.ToString(r.Status),
                              Convert.ToString(r.CompletionDate),
                              Convert.ToString(r.Comments),
                              Convert.ToString(r.Id)
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ViewTasks(string route, string Project = "")
        {
            NCR004 objNCR004 = db.NCR004.Where(i => i.RouteName == route).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(Project))
            {
                List<Employee> lstEmployee = new List<Employee>();
                lstEmployee = (from ath1 in db.ATH001
                               join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                               join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                               where (ath1.Project.Equals(Project) || ath1.Project.ToLower().Equals("all"))
                               select
                               new Employee { isActive = com3.t_actv, psnum = com3.t_psno, name = com3.t_psno + " - " + com3.t_name }).Distinct().ToList();
                ViewBag.lstEmployee = lstEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.psnum, CatDesc = i.name }).ToList();
            }
            return PartialView("_GetTasksList", objNCR004);
        }

        public ActionResult GetTaskDetails(int Id = 0)
        {
            var result = db.NCR004.FirstOrDefault(x => (x.Id == Id));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHeaderId(string NCRNo = "")
        {
            int HeaderId = 0;
            NCR001 objNCR001 = db.NCR001.Where(x => x.NCRNo == NCRNo.Trim()).FirstOrDefault();
            if (objNCR001 != null)
            {
                HeaderId = objNCR001.HeaderId;
            }
            return Json(HeaderId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadTaskList(JQueryDataTableParamModel param, string routeName)
        {
            try
            {

                var roleQcManager = (from a in db.ATH001
                                     join b in db.ATH004 on a.Role equals b.Id
                                     where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && b.Role.ToLower() == clsImplementationEnum.UserRoleName.QC2.ToString().ToLower()
                                     select new { RoleId = a.Role, RoleDesc = b.Role }).FirstOrDefault();

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += "and RouteName = '" + routeName + "'";
                var lstTask = db.SP_NCR_GETTASK(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstTask.Select(i => i.TotalCount).FirstOrDefault();
                var res = from r in lstTask
                          select new[] {
                              Convert.ToString(r.NCRNo),
                              Convert.ToString(r.RouteName),
                              Convert.ToString(r.Task),
                              (roleQcManager != null && r.Status.ToLower() == "pending"?GenerateAutoComplete(r.Id, "Assignee",r.Assignee,"", "", false,"","Assignee",false,"autocomplete"):r.Assignee),//Convert.ToString(r.Assignee),
                              Convert.ToString(r.Title),
                              Convert.ToString(r.TaskOrder),
                              Convert.ToString(r.Status),
                              Convert.ToString(r.CompletionDate),
                              Convert.ToString(r.Comments),
                              Convert.ToString(r.Id)
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public string GetCodeAndNameEmployee(string psnum)
        {
            var result = db.COM003.Where(i => i.t_psno == psnum).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            return result;
        }

        public JsonResult GetAssigneeFromNCR(string NCRno, string term)
        {
            string project = db.NCR001.Where(i => i.NCRNo == NCRno).Select(i => i.Project).FirstOrDefault();
            List<Employee> lstEmployee = new List<Employee>();
            try
            {
                lstEmployee = (from ath1 in db.ATH001
                               join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                               join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                               where (ath1.Project.Equals(project) || ath1.Project.ToLower().Equals("all"))
                               select
                               new Employee { isActive = com3.t_actv, psnum = com3.t_psno, name = com3.t_psno + " - " + com3.t_name }).Distinct().ToList();

                lstEmployee = lstEmployee.Where(q => q.name.ToLower().Contains(term.ToLower())).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAssignee(string project)
        {
            var result = new object();
            try
            {
                project = project.Split('-')[0].Trim();
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                List<BULocWiseCategoryModel> lstEmployee = new List<BULocWiseCategoryModel>();
                string we2 = clsImplementationEnum.UserRoleName.WE2.GetStringValue();
                //List<Employee> lstEmployee = new List<Employee>();
                lstEmployee = (from ath1 in db.ATH001
                               join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                               join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                               where (ath1.Project.Equals(project) || ath1.Project.ToLower().Equals("all"))
                                     && (ath4.Role.Equals(we2))
                               select
                               new BULocWiseCategoryModel { CatID = com3.t_psno, CatDesc = com3.t_psno + " - " + com3.t_name }).Distinct().ToList();

                result = new { Key = true, Value = lstEmployee };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = new { Key = false };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getECR(string Project)
        {
            var result = new object();
            try
            {
                Project = Project.Split('-')[0].Trim();
                var lstECR = db.ECR001.Where(x => x.Project == Project).Select(x => x.ECRName).Distinct().ToList();
                //List<Employee> lstEmployee = new List<Employee>();
                //lstEmployee = (from ath1 in db.ATH001
                //               join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                //               join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                //               where (ath1.Project.Equals(project) || ath1.Project.ToLower().Equals("all"))
                //               select
                //               new Employee { isActive = com3.t_actv, psnum = com3.t_psno, name = com3.t_psno + " - " + com3.t_name }).Distinct().ToList();

                result = new { Key = true, Value = lstECR };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = new { Key = false, Value = new List<string>() };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPart(string project)
        {
            var result = new object();
            try
            {
                List<string> partList = Manager.GetHBOMData(project).Select(x => x.Part).Distinct().ToList();
                //List<string> partList = db.VW_IPI_GETHBOMLIST.Where(x => x.Project.Contains(project)).Select(x => x.Part).Distinct().ToList();
                result = new { Key = true, Value = partList };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = new { Key = false, Value = new List<string>() };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult approveTask(int Id, string comments)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NCR004 objNCR004 = db.NCR004.FirstOrDefault(x => x.Id == Id);
                if (objNCR004.Status.Equals("Approved"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Approved";
                }
                else if (objNCR004.Status.Equals("Completed"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Completed";
                }
                else if (objNCR004.Status.Equals("Rejected"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Rejected";

                }
                else if (objNCR004.Status.Equals("Assigned"))
                {
                    objNCR004.Status = "Approved";
                    objNCR004.CompletionDate = DateTime.Now;
                    objNCR004.Comments = comments.Trim();
                    db.Entry(objNCR004).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Task Approved Successfully !";
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult rejectTask(int Id, string comments)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                NCR004 objNCR004 = db.NCR004.FirstOrDefault(x => x.Id == Id);
                if (objNCR004.Status.Equals("Approved"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Approved";
                }
                else if (objNCR004.Status.Equals("Completed"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Completed";
                }
                else if (objNCR004.Status.Equals("Rejected"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Rejected";

                }
                else if (objNCR004.Status.Equals("Assigned"))
                {
                    objNCR004.Status = "Rejected";
                    objNCR004.Comments = comments.Trim();
                    objNCR004.CompletionDate = DateTime.Now;
                    db.Entry(objNCR004).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    NCR003 objNCR003 = db.NCR003.FirstOrDefault(x => x.RouteName.Equals(objNCR004.RouteName));
                    List<NCR004> lstOfPendingTask = db.NCR004.Where(x => (x.Assignee != objClsLoginInfo.UserName) && (x.NCRNo == objNCR004.NCRNo) && (x.RouteName == objNCR003.RouteName)).ToList();
                    foreach (var row in lstOfPendingTask)
                    {
                        row.Status = "Completed";
                        db.Entry(row).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    objNCR003.Status = "Finished";
                    objNCR003.RouteCompletionDate = DateTime.Now;
                    db.Entry(objNCR003).State = System.Data.Entity.EntityState.Modified;                             //Route Finished
                    NCR001 objNCR001 = db.NCR001.FirstOrDefault(x => x.NCRNo.Equals(objNCR004.NCRNo));
                    DemoteHeader(objNCR001);                                                                         //Demote NCr
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Task Rejected Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult completeTask(int Id, string comments)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NCR004 objNCR004 = db.NCR004.FirstOrDefault(x => x.Id == Id);

                if (objNCR004.Status.Equals("Completed"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Completed";
                }
                else if (objNCR004.Status.Equals("Pending"))
                {
                    var taskOn = db.NCR004.Any(x => (x.Status == "Assigned") && (x.RouteName == objNCR004.RouteName) && (x.NCRNo == objNCR004.NCRNo));
                    if (taskOn)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Assignee Task not yet Completed";
                    }
                    else
                    {
                        objNCR004.Status = "Completed";
                        objNCR004.CompletionDate = DateTime.Now;
                        objNCR004.Comments = comments.Trim();
                        db.Entry(objNCR004).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        NCR003 objNCR003 = db.NCR003.FirstOrDefault(x => x.RouteName == objNCR004.RouteName);
                        objNCR003.Status = "Finished";
                        objNCR003.RouteCompletionDate = DateTime.Now;
                        db.Entry(objNCR003).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Task Completed Successfully";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult transferTask(int Id, string psno)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {


                NCR004 objNCR004 = db.NCR004.FirstOrDefault(x => x.Id == Id);
                if (objNCR004.Status.Equals("Completed"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Completed";
                }
                else if (objNCR004.Status.Equals("Approved"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Approved";
                }
                else if (objNCR004.Status.Equals("Rejected"))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Task is Already been Rejected";
                }
                else
                {
                    objNCR004.Assignee = psno;
                    db.Entry(objNCR004).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Task Transferred Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void stampStatusChangeAndSave(NCR001 objNCR001, string nextStage, string owner)
        {
            //NCR001 objNCR001 = db.NCR001.Where(x => (x.HeaderId == headerId)).FirstOrDefault();

            if (nextStage.Equals(clsImplementationEnum.NCRStatus.Create.GetStringValue()))
                objNCR001.CreatedOn = DateTime.Now;
            //else if (nextStage.Equals(clsImplementationEnum.NCRStatus.SendForValidation.GetStringValue()))
            //    objNCR001.ValidateOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.Validate.GetStringValue()))
                objNCR001.EditedOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.ResponsibleDepartment.GetStringValue()))
                objNCR001.ValidateOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.QAApprove.GetStringValue()))
                objNCR001.RespDeptOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.ReviewComment.GetStringValue()))
                objNCR001.QAApproveOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.Approve.GetStringValue()))
                objNCR001.ReviewOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.ExternalApprove.GetStringValue()))
            {
                bool ea = objNCR001.ExtrApprovalReq.ToLower().Equals("yes");
                bool ecr = objNCR001.ECRRequired.ToLower().Equals("yes");
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (ea)
                    objNCR001.ApproveOn = DateTime.Now;
            }

            //nikita
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.PlanImplementation.GetStringValue()))
            {
                bool ea = objNCR001.ExtrApprovalReq.ToLower().Equals("yes");
                bool ecr = objNCR001.ECRRequired.ToLower().Equals("yes");
                bool rr = objNCR001.RectificationRequired.ToLower().Equals("yes");
                if (ecr)
                    objNCR001.ApproveOn = DateTime.Now;
            }
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.Implement.GetStringValue()))
                objNCR001.PlanImptOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.QCClosure.GetStringValue()))
                objNCR001.ImplementOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.ExternalClosure.GetStringValue()))
                objNCR001.QCClosureOn = DateTime.Now;
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.QAClosure.GetStringValue()))
            {
                bool excr = objNCR001.ExternClosureReq.ToLower().Equals("yes");
                if (excr)
                {
                    objNCR001.ExtClosureOn = DateTime.Now;//.QAClosureOn = DateTime.Now;
                }
                bool isMinor = objNCR001.NCRClass.ToLower().Equals("minor");
                if (!isMinor)
                {
                    objNCR001.QAClosureOn = DateTime.Now;
                }
            }
            else if (nextStage.Equals(clsImplementationEnum.NCRStatus.MarkedForCancel.GetStringValue()))
                objNCR001.MarkedForCancelOn = DateTime.Now;
            else
            { }
            objNCR001.Status = nextStage;
            objNCR001.Owner = owner;
            db.Entry(objNCR001).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult PoSearch(string term, string supplier)
        {
            var lstPo = db.SP_NCR_GET_PO(supplier).ToList();
            return Json(lstPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "NCR History";
            model.Title = "NCR";
            NCR001 objNCR001 = db.NCR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            var objWENCR004 = db.NCR004.Where(x => x.NCRNo == objNCR001.NCRNo && x.TaskOrder == 1).Select(x => new { assignee = x.Assignee, completeOn = x.CompletionDate, id = x.Id }).OrderByDescending(x => x.id).FirstOrDefault();
            var objDENCR004 = db.NCR004.Where(x => x.NCRNo == objNCR001.NCRNo && x.TaskOrder == 2).Select(x => new { assignee = x.Assignee, completeOn = x.CompletionDate, id = x.Id }).OrderByDescending(x => x.id).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objNCR001.CreatedBy);
            model.CreatedOn = objNCR001.CreatedOn;
            model.QCEngineer = Manager.GetUserNameFromPsNo(objNCR001.QCEngineer);
            model.RespDeptMgr = Manager.GetUserNameFromPsNo(objNCR001.RespDeptMgr);
            model.WEEngineer = objWENCR004 != null ? Manager.GetUserNameFromPsNo(objWENCR004.assignee) : null;
            model.DEEngineer = objDENCR004 != null ? Manager.GetUserNameFromPsNo(objDENCR004.assignee) : null;
            model.QAEngineer = Manager.GetUserNameFromPsNo(objNCR001.QAEngineer);
            model.SDE = Manager.GetUserNameFromPsNo(objNCR001.SDE);
            model.QCManager = Manager.GetUserNameFromPsNo(objNCR001.QCManager);
            model.ExternalApprover = Manager.GetUserNameFromPsNo(objNCR001.ExternalApprover);
            model.ExternClosureAgent = Manager.GetUserNameFromPsNo(objNCR001.ExternClosureAgent);

            model.ValidateOn = objNCR001.ValidateOn;
            model.RespDeptOn = objNCR001.RespDeptOn;
            model.QAApproveOn = objNCR001.QAApproveOn;
            model.ReviewOn = objNCR001.ReviewOn;
            model.ApproveOn = objNCR001.ApproveOn;
            model.WEApproveOn = objWENCR004 != null ? objWENCR004.completeOn : null;
            model.DEApproveOn = objDENCR004 != null ? objDENCR004.completeOn : null;
            model.ExtApproveOn = objNCR001.ExtApproveOn;
            model.PlanImptOn = objNCR001.PlanImptOn;
            model.ImplementOn = objNCR001.ImplementOn;
            model.QCClosureOn = objNCR001.QCClosureOn;
            model.ExtClosureOn = objNCR001.ExtClosureOn;
            model.QAClosureOn = objNCR001.QAClosureOn;

            model.ExtrApprovalReq = objNCR001.ExtrApprovalReq;
            model.ECRRequired = objNCR001.ECRRequired;
            model.RectificationRequired = objNCR001.RectificationRequired;
            model.ExternClosureReq = objNCR001.ExternClosureReq;
            model.NCRClass = objNCR001.NCRClass;
            model.Status = objNCR001.Status;
            bool ea = objNCR001.ExtrApprovalReq != null ? objNCR001.ExtrApprovalReq.ToLower().Equals("yes") : false;

            if (objNCR001.Status == clsImplementationEnum.NCRStatus.Closed.GetStringValue())
            {
                if (objNCR001.NCRClass == "Major" || ea)
                {
                    model.ClosedBy = Manager.GetUserNameFromPsNo(objNCR001.QAEngineer);
                    model.ClosedOn = objNCR001.QAClosureOn;
                }
                else
                {
                    model.ClosedBy = Manager.GetUserNameFromPsNo(objNCR001.QCManager);
                    model.ClosedOn = objNCR001.QCClosureOn;
                }
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [SessionExpireFilter]
        public ActionResult CopyHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NCR001 objNCR001 = new NCR001();
                objNCR001 = db.NCR001.FirstOrDefault(q => (q.HeaderId == headerId));
                NCR001 objNCR002 = new NCR001()
                {
                    Project = objNCR001.Project,
                    BU = objNCR001.BU,
                    ContractNo = objNCR001.ContractNo,
                    ReferredtoCustomer = objNCR001.ReferredtoCustomer,
                    PSQCP_QAPlan = objNCR001.PSQCP_QAPlan,
                    Material = objNCR001.Material,
                    RoutingCardNo = objNCR001.RoutingCardNo,
                    SuprName = objNCR001.SuprName,
                    QMSProjectNumber = objNCR001.QMSProjectNumber,
                    SeamNumber = objNCR001.SeamNumber,
                    PositionNumberFindNumber = objNCR001.PositionNumberFindNumber,
                    IMRNo = objNCR001.IMRNo,
                    SerialNo = objNCR001.SerialNo,
                    TypeofItem_Assembly = objNCR001.TypeofItem_Assembly,
                    Quantity = objNCR001.Quantity,
                    IR_Reference = objNCR001.IR_Reference,
                    StageOfInspection = objNCR001.StageOfInspection,
                    QCEngineer = objNCR001.QCEngineer,
                    RespDept = objNCR001.RespDept,
                    RespDeptMgr = objNCR001.RespDeptMgr,
                    SDE = objNCR001.SDE,
                    ExternClosureAgent = objNCR001.ExternClosureAgent,
                    ExternalApprover = objNCR001.ExternalApprover,
                    QAEngineer = objNCR001.QAEngineer,
                    CreatedBy = objClsLoginInfo.UserName,
                    Owner = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                    Status = clsImplementationEnum.NCRStatus.Create.GetStringValue(),
                    NCRLocation = objNCR001.NCRLocation,
                    NCRNo = getNextID()
                };
                db.NCR001.Add(objNCR002);
                db.SaveChanges();
                return RedirectToAction("AddHeader", new { headerId = objNCR002.HeaderId });
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
                return new HttpStatusCodeResult(500);
            }


        }

        [HttpPost]
        public ActionResult GetReposiblePersonsHtml(int HeaderId)
        {
            NCR001 objNCR001 = db.NCR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_ResponsiblePersonPartial", objNCR001);
        }

        #region Print
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult PrintReport()
        {
            return View();
        }

        public JsonResult GetBUs(string term, string selectedVal)
        {
            List<CategoryData> lstBUs = new List<CategoryData>();
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            try
            {
                string[] arrselectedVal = selectedVal.Split(',');

                if (!arrselectedVal.Contains("ALL"))
                {
                    if (!string.IsNullOrEmpty(term))
                    {
                        lstBUs = (from a in Manager.getBUsByID(BU).ToList()
                                  where a.t_dtyp == 2 && a.t_dimx != "" && a.t_desc.ToLower().Contains(term.ToLower())
                                  select new CategoryData
                                  {
                                      id = a.t_dimx,
                                      text = a.t_desc
                                  }).Distinct().ToList();
                    }
                    else
                    {
                        lstBUs = (from a in Manager.getBUsByID(BU).ToList()
                                  where a.t_dtyp == 2 && a.t_dimx != ""
                                  select new CategoryData
                                  {
                                      id = a.t_dimx,
                                      text = a.t_desc
                                  }).Distinct().Take(10).ToList();
                    }


                    if (lstBUs.Count > 0)
                    {
                        lstBUs.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                    }
                }

                return Json(lstBUs, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetLocations(string term, string selectedVal)
        {
            List<CategoryData> items = new List<CategoryData>();
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocations = db.COM002.Where(x => x.t_dtyp == LOC).Distinct().ToList();
            List<GLB002> lstLocation = GetSubCatagories("NCR Location").ToList();

            try
            {
                string[] arrselectedVal = selectedVal.Split(',');

                if (!arrselectedVal.Contains("ALL"))
                {
                    if (!string.IsNullOrEmpty(term))
                    {
                        items = lstLocation.AsEnumerable().Where(x => x.Description.ToLower().Contains(term)).Select(i => new { i.Code, i.Description }).Distinct().Select(x => new CategoryData { text = (x.Description).ToString().ToUpperInvariant(), id = x.Code.ToString() }).Distinct().ToList();

                    }
                    else
                    {
                        items = lstLocation.AsEnumerable().Select(i => new { i.Code, i.Description }).Distinct().Select(x => new CategoryData { text = (x.Description).ToString().ToUpperInvariant(), id = x.Code.ToString() }).Distinct().ToList();
                        //(from li in lstLocations
                        //         select new CategoryData
                        //         {
                        //             id = li.t_dimx,
                        //             text = li.t_desc
                        //         }).Take(10).ToList();
                    }
                }

                if (items.Count > 0)
                {
                    items.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetProjects(string term, string selectedVal)
        {
            List<CategoryData> lstProjects = new List<CategoryData>();
            try
            {
                string[] arrselectedVal = selectedVal.Split(',');

                if (!arrselectedVal.Contains("ALL"))
                {
                    if (!string.IsNullOrEmpty(term))
                    {
                        lstProjects = (from a in db.QMS010
                                       where a.Project.Contains(term)
                                       orderby a.Project
                                       select new CategoryData
                                       {
                                           id = a.Project,
                                           text = a.Project
                                       }).Distinct().ToList();
                    }
                    else
                    {
                        lstProjects = (from a in db.QMS010
                                       orderby a.Project
                                       select new CategoryData
                                       {
                                           id = a.Project,
                                           text = a.Project
                                       }).Distinct().Take(10).ToList();
                    }
                }

                if (lstProjects.Count > 0)
                {
                    lstProjects.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstProjects, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCategory(string term, string selectedVal)
        {
            List<CategoryData> lstCategory = new List<CategoryData>();
            try
            {
                string[] arrselectedVal = selectedVal.Split(',');

                if (!arrselectedVal.Contains("ALL"))
                {
                    if (!string.IsNullOrEmpty(term))
                    {
                        lstCategory = (from a in db.NCR006
                                       where a.NCRCategory.Contains(term)
                                       orderby a.NCRCategory
                                       select new CategoryData
                                       {
                                           id = a.NCRCategory,
                                           text = a.NCRCategory
                                       }).Distinct().ToList();
                    }
                    else
                    {
                        lstCategory = (from a in db.NCR006
                                       orderby a.NCRCategory
                                       select new CategoryData
                                       {
                                           id = a.NCRCategory,
                                           text = a.NCRCategory
                                       }).Distinct().Take(10).ToList();
                    }

                }
                if (lstCategory.Count > 0)
                {
                    lstCategory.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstCategory, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDepartments(string term, string selectedVal)
        {
            List<CategoryData> lstDepartments = new List<CategoryData>();
            try
            {
                string[] arrselectedVal = selectedVal.Split(',');

                if (!arrselectedVal.Contains("ALL"))
                {
                    if (!string.IsNullOrEmpty(term))
                    {
                        lstDepartments = (from a in Manager.getDepartmentAutocomplete("").ToList()
                                          where a.ddesc.Contains(term)
                                          orderby a.dcode
                                          select new CategoryData
                                          {
                                              id = a.dcode,
                                              text = a.ddesc
                                          }).Distinct().ToList();
                    }
                    else
                    {
                        lstDepartments = (from a in Manager.getDepartmentAutocomplete("").ToList()
                                          orderby a.dcode
                                          select new CategoryData
                                          {
                                              id = a.dcode,
                                              text = a.ddesc
                                          }).Distinct().Take(10).ToList();
                    }

                }
                if (lstDepartments.Count > 0)
                {
                    lstDepartments.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstDepartments, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetStatus(string term, string selectedVal)
        {
            List<CategoryData> lstStatus = new List<CategoryData>();
            try
            {
                string[] arrselectedVal = selectedVal.Split(',');

                if (!arrselectedVal.Contains("ALL"))
                {
                    if (!string.IsNullOrEmpty(term))
                    {
                        lstStatus = (from a in clsImplementationEnum.GetNCRStatus().ToList()
                                     where a.Contains(term)
                                     select new CategoryData
                                     {
                                         id = a,
                                         text = a
                                     }).Distinct().ToList();
                    }
                    else
                    {
                        lstStatus = (from a in clsImplementationEnum.GetNCRStatus().ToList()
                                     select new CategoryData
                                     {
                                         id = a,
                                         text = a
                                     }).Distinct().Take(10).ToList();
                    }
                }

                if (lstStatus.Count > 0)
                {
                    lstStatus.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetInitiator(string term, string selectedVal)
        {
            List<CategoryData> lstInitiator = new List<CategoryData>();

            string[] arrselectedVal = selectedVal.Split(',');
            try
            {
                if (!arrselectedVal.Contains("ALL"))
                {

                    if (!string.IsNullOrEmpty(term))
                    {
                        lstInitiator = (from a in Manager.getEmployeeAutocomplete("").ToList()
                                        where a.name.Contains(term)
                                        select new CategoryData
                                        {
                                            id = a.psnum,
                                            text = a.psnum + "-" + a.name
                                        }).Distinct().ToList();
                    }
                    else
                    {
                        lstInitiator = (from a in Manager.getEmployeeAutocomplete("").ToList()
                                        select new CategoryData
                                        {
                                            id = a.psnum,
                                            text = a.psnum + "-" + a.name
                                        }).Distinct().Take(10).ToList();
                    }

                }
                if (lstInitiator.Count > 0)
                {
                    lstInitiator.Insert(0, new CategoryData { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstInitiator, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllValue(string selectedValue, string fieldType)
        {
            ResponceMsg objResponceMsg = new ResponceMsg();
            string[] arrayValue = selectedValue.Split(',').ToArray();

            if (fieldType.ToLower() == "bu")
            {
                if (arrayValue.Contains("ALL"))
                {
                    //var items = db.COM002.Where(x => x.t_dtyp == 2 && x.t_dimx != "").Select(x => x.t_dimx).Distinct().ToList();
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            else if (fieldType.ToLower() == "location")
            {
                if (arrayValue.Contains("ALL"))
                {
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
                    //var lstLocations = db.COM002.Where(x => x.t_dtyp == LOC).Distinct().ToList();
                    //var items = lstLocations.Select(x => x.t_dimx).Distinct().ToList();
                    //objResponceMsg.Key = true;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            else if (fieldType.ToLower() == "project")
            {
                if (arrayValue.Contains("ALL"))
                {
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //var items = db.QMS010.Select(x => x.Project).Distinct().ToList();
                    //objResponceMsg.Key = true;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            else if (fieldType.ToLower() == "category")
            {
                if (arrayValue.Contains("ALL"))
                {
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //var items = db.NCR006.Select(x => x.NCRCategory).Distinct().ToList();
                    //objResponceMsg.Key = true;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            else if (fieldType.ToLower() == "department")
            {
                if (arrayValue.Contains("ALL"))
                {
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //var items = Manager.getDepartmentAutocomplete("").Select(x => x.dcode).ToList();
                    //objResponceMsg.Key = true;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            else if (fieldType.ToLower() == "status")
            {
                if (arrayValue.Contains("ALL"))
                {
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //var items = clsImplementationEnum.GetNCRStatus();
                    //objResponceMsg.Key = true;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            else if (fieldType.ToLower() == "initiator")
            {
                if (arrayValue.Contains("ALL"))
                {
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = string.Empty;
                    //var items = Manager.getEmployeeAutocomplete("").Select(x => x.psnum).ToList();
                    //objResponceMsg.Key = true;
                    //objResponceMsg.Value = "'" + string.Join("','", items) + "'";
                }
                else
                {
                    objResponceMsg.Key = false;
                    objResponceMsg.Value = selectedValue;
                }
            }
            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetQualityProjectResult()
        {
            var lstQualityProject = (from a in db.QMS010
                                     orderby a.QualityProject
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReportFormate()
        {
            List<string> lstFormat = clsImplementationEnum.GetNCRReportFormat().ToList();
            ViewBag.lstReportFormat = lstFormat.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            return PartialView("_ReportFormatSelectionPartial");
        }

        [HttpPost]
        public ActionResult GetNcrReportPartial(string status)
        {
            ViewBag.Status = status;
            //var lstBUs = (from a in db.COM002
            //              where a.t_dtyp == 2 && a.t_dimx != ""
            //              select new { a.t_dimx, Desc = a.t_desc }).ToList();
            //ViewBag.Bus = lstBUs.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatDesc = x.Desc, CatID = x.t_dimx }).ToList();

            //int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            //var lstLocations = db.COM002.Where(x => x.t_dtyp == LOC).Distinct().ToList();
            //var items = (from li in lstLocations
            //             select new
            //             {
            //                 id = li.t_dimx,
            //                 text = li.t_desc,
            //             }).ToList();

            //ViewBag.location = items.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatID = x.id, CatDesc = x.text }).ToList();

            //var lstProject = (from a in db.QMS010
            //                  orderby a.Project
            //                  select new { a.Project }).Distinct().ToList();

            //ViewBag.Projects = lstProject.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatDesc = x.Project, CatID = x.Project }).ToList();

            //ViewBag.lstNCRCategories = db.NCR006.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatID = x.NCRCategory, CatDesc = x.NCRCategory }).Distinct().ToList();

            //List<Department> lstDepartment = Manager.getDepartmentAutocomplete("").ToList();
            //ViewBag.lstRespDept = lstDepartment.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatID = x.dcode, CatDesc = x.ddesc }).ToList();

            //List<Employee> lstEmployee = Manager.getEmployeeAutocomplete("");
            //ViewBag.lstInitiator = lstEmployee.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatID = x.psnum, CatDesc = x.name }).ToList();

            //List<string> lstStatus = clsImplementationEnum.GetNCRStatus();
            //ViewBag.lstNCRStatus = lstStatus.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatID = x.ToString(), CatDesc = x.ToString() }).ToList();
            return PartialView("_GetNcrReportPartial");
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_NCR_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      NCRNo = li.NCRNo,
                                      OldNCR = Convert.ToString(li.OldNCRNumber),
                                      HeaderId = li.HeaderId,
                                      Project = li.Project,
                                      BU = li.BU,
                                      NCRLocation = li.NCRLocation,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      QCEngineer = li.QCEngineer,
                                      RespDept = li.RespDept,
                                      RespDeptMgr = li.RespDeptMgr,
                                      Description = li.Description
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_NCR_GETROUTE(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      NCRNo = li.NCRNo,
                                      RouteName = li.RouteName,
                                      Status = li.Status,
                                      RouteBasePurpose = li.RouteBasePurpose,
                                      RouteCompletionAction = li.RouteCompletionAction,
                                      RouteCompletionDate = li.RouteCompletionDate,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
                {
                    var lst = db.SP_NCR_GETTASK(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      NCRNo = li.NCRNo,
                                      RouteName = li.RouteName,
                                      Task = li.Task,
                                      Assignee = li.Assignee,
                                      Title = li.Title,
                                      TaskOrder = li.TaskOrder,
                                      Status = li.Status,
                                      CompletionDate = li.CompletionDate,
                                      Comments = li.Comments,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_NCR_GET_HEADERHISTORYDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      NCRNo = li.NCRNo,
                                      Project = li.Project,
                                      Description = li.Description,
                                      NCRLocation = li.NCRLocation,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region
        public IQueryable<CategoryData> GetSubCatagoryForNCRLocation(string Key)
        {
            IQueryable<CategoryData> lstGLB002 = null;
            lstGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
                         select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct();
            return lstGLB002;


        }
        public List<GLB002> GetSubCatagories(string Key)
        {
            List<GLB002> lstGLB002 = (from glb002 in db.GLB002
                                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
                                      select glb002).ToList();
            return lstGLB002;
        }
        #endregion

        [HttpPost]
        public ActionResult updateAssignee(int TaskId, string assigneeId)
        {
            ResponceMsgWithStatus objRes = new ResponceMsgWithStatus();
            NCR004 objNCR004 = db.NCR004.Where(x => x.Id == TaskId).FirstOrDefault();
            if (objNCR004 != null)
            {
                objNCR004.Assignee = assigneeId;
                objNCR004.EditedBy = objClsLoginInfo.UserName;
                objNCR004.EditedOn = DateTime.Now;
                db.SaveChanges();
                objRes.Key = true;
                objRes.Value = "Assignee successfully updated.";
            }
            else
            {
                objRes.Key = true;
                objRes.Value = "Task details not available.";
            }
            return Json(objRes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSuplier(string term)
        {
            List<BULocWiseCategoryModel> lstSupplier = new List<BULocWiseCategoryModel>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstSupplier = (from a in db.COM006
                               where (a.t_bpid.Contains(term)) || (a.t_nama.Contains(term))
                               select new BULocWiseCategoryModel { CatID = a.t_bpid, CatDesc = a.t_bpid + " - " + a.t_nama }).Distinct().ToList();

            }
            else
            {
                lstSupplier = (from a in db.COM006
                               select new BULocWiseCategoryModel { CatID = a.t_bpid, CatDesc = a.t_bpid + " - " + a.t_nama }).Distinct().Take(10).ToList();
            }

            return Json(lstSupplier, JsonRequestBehavior.AllowGet);

        }

        public ActionResult NCRSearch(string ncrNo)
        {
            NCR009 objnCR009 = new NCR009();
            objnCR009 = db.NCR009.Where(x => x.NCRNo == ncrNo).FirstOrDefault();
            return View(objnCR009);
        }
        [HttpPost]
        public ActionResult NCRSearch(NCR009 ncr009)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                NCR009 objnCR009 = new NCR009();
                var q = db.NCR009.Where(x => x.NCRNo == ncr009.NCRNo).FirstOrDefault();
                if (q == null)
                {
                    objnCR009.NCRNo = ncr009.NCRNo;
                    objnCR009.ProjectNumber = ncr009.ProjectNumber;
                    objnCR009.EquipmentNumber = ncr009.EquipmentNumber;
                    objnCR009.DeviationFailure = ncr009.DeviationFailure;
                    objnCR009.Customer = ncr009.Customer;
                    objnCR009.CustomerLocation = ncr009.CustomerLocation;
                    objnCR009.EndCustomer = ncr009.EndCustomer;
                    objnCR009.PMC = ncr009.PMC;
                    objnCR009.Supplier = ncr009.Supplier;
                    objnCR009.SupplierLocation = ncr009.SupplierLocation;
                    objnCR009.ProductName = ncr009.ProductName;
                    objnCR009.MaterialSpec = ncr009.MaterialSpec;
                    objnCR009.MaterialGrade = ncr009.MaterialGrade;
                    objnCR009.MaterialThickness = ncr009.MaterialThickness;
                    objnCR009.ItemName = ncr009.ItemName;
                    objnCR009.SubAssembly = ncr009.SubAssembly;
                    objnCR009.NDEMethod = ncr009.NDEMethod;
                    objnCR009.OperatorName = ncr009.OperatorName;
                    objnCR009.WelderName = ncr009.WelderName;
                    objnCR009.WeldingType = ncr009.WeldingType;
                    objnCR009.WeldingConsumable = ncr009.WeldingConsumable;
                    objnCR009.WeldingConsumableSupplier = ncr009.WeldingConsumableSupplier;
                    objnCR009.ShopName = ncr009.ShopName;
                    objnCR009.YardName = ncr009.YardName;
                    objnCR009.DepartmentName = ncr009.DepartmentName;
                    objnCR009.NatureOfWeldingDefect = ncr009.NatureOfWeldingDefect;
                    objnCR009.NatureOfMaterialDefec = ncr009.NatureOfMaterialDefec;
                    objnCR009.TPIMaterialInspection = ncr009.TPIMaterialInspection;
                    objnCR009.TPIProductInspection = ncr009.TPIProductInspection;
                    objnCR009.PTCFailures = ncr009.PTCFailures;
                    objnCR009.SubContractorName = ncr009.SubContractorName;
                    objnCR009.HeatTreatment = ncr009.HeatTreatment;
                    objnCR009.Machining = ncr009.Machining;
                    objnCR009.Forming = ncr009.Forming;
                    objnCR009.Fastener = ncr009.Fastener;
                    objnCR009.YearOfDispatch = ncr009.YearOfDispatch;
                    objnCR009.CreatedBy = objClsLoginInfo.UserName;
                    objnCR009.CreatedOn = DateTime.Now;
                    db.NCR009.Add(objnCR009);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}