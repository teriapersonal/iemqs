﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Data.Entity;

using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Globalization;

namespace IEMQS.Areas.IMB.Controllers
{
    public class ApproveController : clsBase
    {
        #region Header
        [SessionExpireFilter, AllowAnonymous, UserPermissions]

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        public ActionResult LoadHeaderData(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;

                //Func<IMB001, string> orderingFunction = (tsk =>
                //                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");                

                string whereCondition = "1=1";
              //  whereCondition += " and ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                whereCondition += " and Location = '" + objClsLoginInfo.Location + "'";

                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue() + "') and ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and ( lcom2.t_desc like '%" + param.sSearch + "%' or (imb1.Project +'-'+ com1.t_dsca) like '%" + param.sSearch + "%' or Document like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or ProcessLicensor like '%" + param.sSearch + "%' or Product like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHEADER = db.SP_IMB_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, whereCondition
                                ).ToList();

                int totalRecord = Convert.ToInt32(lstHEADER.Select(x => x.TotalCount).FirstOrDefault());

                var data = (from uc in lstHEADER
                            select new[]
                           {
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.Project),
                           
                           Convert.ToString(uc.Document),
                           Convert.ToString("R"+uc.RevNo),
                           uc.Customer,
                           uc.Product,
                           uc.ProcessLicensor,
                           Convert.ToString(uc.LocationDesc),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.CreatedBy+'-'+ Manager.GetUserNameFromPsNo(uc.CreatedBy)),
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                        Convert.ToString(uc.SubmittedBy),
                        uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Convert.ToString(uc.ApprovedBy),
                        uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                          
                           //"<center><a href='javascript:void(O);' title='View' onclick='Viewlines(" + uc.HeaderId + ")'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"//Convert.ToString(uc.HeaderId),
                           //+"  "+(uc.RevNo>0?"<i title='History' style='margin-left:5px;cursor:pointer;' class='fa fa-history' onClick='GetHistoryDetailsPartial("+uc.HeaderId+")'></i>"+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/IMB/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i class='fa fa-clock-o'></i></a>":"</center>")//Convert.ToString(h.HeaderId)
                            "<nobr><center>"+
                           "<a class='iconspace' href='javascript:void(O);' title='View' onclick='Viewlines(" + uc.HeaderId + ")'><i class='fa fa-eye'></i></a>"+
                             "<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/IMB/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                           (uc.RevNo>0?"<i title='History' class='iconspace fa fa-history' onClick='GetHistoryDetailsPartial("+uc.HeaderId+")'></i> ":"<i title='History' class='disabledicon fa fa-history' ></i>" )+

                           "</center></nobr>",
                            Convert.ToString(uc.HeaderId),

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Improvement_Budget_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //public ActionResult SaveHeader(int headerId, bool hasAttachments, Dictionary<string, string> Attach)
      public ActionResult SaveHeader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            IMB001 objIMB001 = new IMB001();
            try
            {
                string LoginUser = objClsLoginInfo.UserName;
                if (headerId > 0)
                {
                    objIMB001 = db.IMB001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    #region Header Update

                    //var folderPath = "IMB001/" + objIMB001.HeaderId + "/R" + objIMB001.RevNo;
                    // Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsgWithStatus.HeaderId = objIMB001.HeaderId;
                    objResponseMsgWithStatus.Revision = Convert.ToInt32(objIMB001.RevNo);
                    #endregion

                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = objIMB001.HeaderId.ToString();
                    var folderLoadingSketchPath = "IMB001/" + objIMB001.HeaderId + "/R" + objIMB001.RevNo;
                    Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }

        #endregion

        #region Lines
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Viewlines(int id)
        {
            ViewBag.id = id;
            IMB001 objIMB001 = db.IMB001.FirstOrDefault(x => x.HeaderId == id);// && x.ApprovedBy == objClsLoginInfo.UserName);
            //if (objIMB001 == null)
            //{
            //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            //}
            ViewBag.DocNo = objIMB001.Document;
            ViewBag.EquipmentName = db.COM001.Where(x => x.t_cprj == objIMB001.Project).Select(x => x.t_dsca).FirstOrDefault();

            var PlanningDinID = db.PDN002.Where(x => x.RefId == id && x.DocumentNo == objIMB001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
            ViewBag.PlanningDinID = PlanningDinID;
            ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, id, clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue());

            ViewBag.btnApprAccess = false;
            if (objIMB001.Status == clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
            {
                ViewBag.btnApprAccess = true;
            }
            var lstResult = (from a in db.ATH001
                             join b in db.ATH004 on a.Role equals b.Id
                             where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                             select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();
            if (lstResult.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                             || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                             || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).Distinct().ToList().Any() || string.IsNullOrWhiteSpace(objIMB001.ApprovedBy) || objIMB001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
            {
                ViewBag.display = "false";
            }
            else
            {
                ViewBag.display = "true";
            }
            if (objIMB001.ApprovedBy != null)
            {
                if (objIMB001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.IsDisplayMode = true;
                }
                else
                {
                    ViewBag.IsDisplayMode = false;
                }
            }
            else
            {
                ViewBag.IsDisplayMode = true;
            }
            return View(objIMB001);
        }

        [HttpPost]
        public ActionResult Approve(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                db.SP_IMB_APPROVE(id, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                IMB001 objIMB001 = db.IMB001.Where(x => x.HeaderId == id).FirstOrDefault();
                int newId = db.IMB001_Log.Where(q => q.HeaderId == id).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document, newId);

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objIMB001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objIMB001.Project, clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(), objIMB001.RevNo.Value.ToString(), objIMB001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(), objIMB001.HeaderId.ToString(), false), objIMB001.CreatedBy);
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveSelectedHeader(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);

                    db.SP_IMB_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    IMB001 objIMB001 = db.IMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.IMB001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document, newId);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Return(int id, string remark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<IMB002> lstline = await (from li in db.IMB002
                                              where li.HeaderId == id
                                              select li).ToListAsync();

                if (lstline.Any())
                {
                    IMB001 objIMB001 = await db.IMB001.FirstOrDefaultAsync(x => x.HeaderId == id);
                    objIMB001.Status = clsImplementationEnum.IMBStatus.Returned.GetStringValue();
                    objIMB001.Remarks = remark;
                    await db.SaveChangesAsync();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
                    Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objIMB001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objIMB001.Project, clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(), objIMB001.RevNo.Value.ToString(), objIMB001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(), objIMB001.HeaderId.ToString(), false), objIMB001.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.ReturnValidation.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadLinesData(JQueryDataTableParamModel param, int id)
        {
            try
            {

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string condition = string.Empty;
                condition = "HeaderId = " + id;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( ImprovementDescription like '%" + param.sSearch + "%' or TeamMember like '%" + param.sSearch + "%' or Comments like '%" + param.sSearch + "%' or TeamFormation like '%" + param.sSearch + "%'  or TeamLead like '%" + param.sSearch + "%')";
                }

                var lstLines = db.SP_IMB_GETLINES(StartIndex, EndIndex, "", condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.LineId),
                                        GetAttachment( c.LineId),
                                        Convert.ToString(c.ImprovementDescription),
                                        Convert.ToString(c.TeamLeadName),
                                        Convert.ToString(c.TeamMemberName),
                                        Convert.ToDateTime( c.TargetDate).ToString("dd/MM/yyyy"),
                                        Convert.ToString(c.TeamFormation),
                                        Convert.ToString(c.Comments) ,
                                        Convert.ToString(c.Complete)
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords,
                    iTotalRecords = totalRecords,
                    aaData = res,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetAttachment(int id)
        {
            string attachment;
            //var folderPath = "IMB002/" + id;
            //var existing =(new clsFileUpload()).GetDocuments(folderPath);
            IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new Utility.Controllers.FileUploadController();
            if (!_obj.CheckAnyDocumentsExits("IMB002", id))
            {
                attachment = "False";
            }
            else
            {
                attachment = "True";
            }
            return attachment;
        }
        [HttpPost]
        public ActionResult AddAttachment(int headerId, int? lineId)
        {
            IMB002 objIMB002 = new IMB002();

            if (lineId != null && lineId > 0)
            {
                objIMB002 = db.IMB002.FirstOrDefault(x => x.HeaderId == headerId && x.LineId == lineId);
                ViewBag.Visibility = "true";
            }
            else
            {
                ViewBag.Visibility = "true";
                objIMB002.HeaderId = headerId;
            }
            return PartialView("~/Areas/IMB/Views/Shared/_LineAttachment.cshtml", objIMB002);
        }
        #endregion

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {//header grid data
                    var lst = db.SP_IMB_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Document = Convert.ToString(h.Document),
                                      RevNo = Convert.ToString("R" + h.RevNo),
                                      Customer = Convert.ToString(h.Customer),
                                      Product = Convert.ToString(h.Product),
                                      ProcessLicensor = Convert.ToString(h.ProcessLicensor),
                                      Location = Convert.ToString(h.LocationDesc),
                                      Status = Convert.ToString(h.Status),
                                      SubmittedBy = h.SubmittedBy,
                                      SubmittedOn = h.SubmittedOn == null || h.SubmittedOn.Value == DateTime.MinValue ? "NA" : h.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = h.ApprovedBy,
                                      ApprovedOn = h.ApprovedOn == null || h.ApprovedOn.Value == DateTime.MinValue ? "NA" : h.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_IMB_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      ImprovementDescription = Convert.ToString(c.ImprovementDescription),
                                      TeamLead = Convert.ToString(c.TeamLead),
                                      TeamLeadName = Convert.ToString(c.TeamLeadName),
                                      TeamMember = Convert.ToString(c.TeamMember),
                                      TeamMemberName = Convert.ToString(c.TeamMemberName),
                                      TargetDate = (c.TargetDate == null || c.TargetDate.Value == DateTime.MinValue ? "NA" : c.TargetDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      TeamFormation = Convert.ToString(c.TeamFormation),
                                      Comments = Convert.ToString(c.Comments),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {//header grid data
                    var lst = db.SP_IMB_GETHEADERHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Document = Convert.ToString(h.Document),
                                      RevNo = Convert.ToString("R" + h.RevNo),
                                      Customer = Convert.ToString(h.Customer),
                                      Product = Convert.ToString(h.Product),
                                      ProcessLicensor = Convert.ToString(h.ProcessLicensor),
                                      Location = Convert.ToString(h.LocationDesc),
                                      Status = Convert.ToString(h.Status),
                                      SubmittedBy = h.SubmittedBy,
                                      SubmittedOn = h.SubmittedOn == null || h.SubmittedOn.Value == DateTime.MinValue ? "NA" : h.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = h.ApprovedBy,
                                      ApprovedOn = h.ApprovedOn == null || h.ApprovedOn.Value == DateTime.MinValue ? "NA" : h.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_IMB_GETLINESHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      ImprovementDescription = Convert.ToString(c.ImprovementDescription),
                                      TeamLead = Convert.ToString(c.TeamLead),
                                      TeamLeadName = Convert.ToString(c.TeamLeadName),
                                      TeamMember = Convert.ToString(c.TeamMember),
                                      TeamMemberName = Convert.ToString(c.TeamMemberName),
                                      TargetDate = (c.TargetDate == null || c.TargetDate.Value == DateTime.MinValue ? "NA" : c.TargetDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      TeamFormation = Convert.ToString(c.TeamFormation),
                                      Comments = Convert.ToString(c.Comments),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}