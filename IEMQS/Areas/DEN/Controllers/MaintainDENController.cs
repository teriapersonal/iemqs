﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.DEN.Controllers
{
    public class MaintainDENController : clsBase
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LNConcerto"].ToString());
        // GET: DEN/MaintainDEN
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult IndexFSP()
        {
            ViewBag.Title = clsImplementationEnum.DENIndexTitle.MaintainFP.GetStringValue();
            ViewBag.Role = "FSP";
            return View("Index");
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult IndexJP()
        {
            ViewBag.Title = clsImplementationEnum.DENIndexTitle.MaintainFPRD.GetStringValue();
            ViewBag.Role = "JP";
            return View("Index");
        }

        [UserPermissions]
        public ActionResult DisplayDEN()
        {
            ViewBag.Title = clsImplementationEnum.DENIndexTitle.DisplayFP.GetStringValue();
            ViewBag.IsDisplay = true;
            return View("Index", new DEN001());
        }

        public ActionResult GetIndexGridDataPartial(string status, string role, bool isDisplay, string press)
        {
            ViewBag.Status = status;
            ViewBag.Role = role;
            ViewBag.Press = press;
            ViewBag.IsDisplayy = (status.ToLower() == clsImplementationEnum.Overlay.Yes.GetStringValue().ToLower()) ? true : isDisplay;

            ViewBag.lstyesno = new string[] { clsImplementationEnum.yesno.Yes.GetStringValue(), clsImplementationEnum.yesno.No.GetStringValue() };
            ViewBag.lstyesnona = new string[] { clsImplementationEnum.YesNoNA.Yes.GetStringValue(), clsImplementationEnum.YesNoNA.No.GetStringValue(), clsImplementationEnum.YesNoNA.NA.GetStringValue() };
            ViewBag.lstyesnoip = new string[] { clsImplementationEnum.YesNoIP.Yes.GetStringValue(), clsImplementationEnum.YesNoIP.No.GetStringValue(), clsImplementationEnum.YesNoIP.IP.GetStringValue() };

            return PartialView("_GetIndexGridDataPartial");
        }

        public JsonResult LoadDataGridData(JQueryDataTableParamModel param, string role, string press)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1 ";

                if (!string.IsNullOrEmpty(press))
                {
                    strWhere += "and press='" + press + "' ";
                }

                if (param.Status.ToLower() == clsImplementationEnum.Overlay.No.GetStringValue().ToLower())
                {
                    strWhere += "and [DISP]!='Yes'";
                }
                else if (param.Status.ToLower() == clsImplementationEnum.Overlay.Yes.GetStringValue().ToLower())
                {
                    strWhere += "and [DISP]='Yes'";
                }

                string[] columnName = { "Project", "Customer", "Priority", "TASKUNIQUEID", "ActivityDescription", "Dia", "Thk", "NoofPetal", "WeightKG", "Construction", "Press", "MaterialAvailability"
                ,"DiePunchAvailability", "TemplateAvailability", "HTRAvailability", "LockingCleatAvailability", "FixtureCleatAvailability", "TotalDurationInDays", "PressingDurationInDays"
                , "CONVERT(nvarchar(20),ConcertoStartDate,103)", "CONVERT(nvarchar(20),PlannedStartDate,103)", "CONVERT(nvarchar(20),PlannedEndDate,103)", "CONVERT(nvarchar(20),RequiredOn,103)", "CONVERT(nvarchar(20),ActualStart,103)"
                ,"PlateCutting","Pressing","HT","RePressing","Setup","Inspection","DISP" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_DEN_GET_MAINTAIN_DATA(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                //var statusApproved = clsImplementationEnum.DENStatus.Approved.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               uc.Project,
                               Generatetooltipdesc(uc.Customer,"Customer"),
                               Generatetooltipdesc(uc.ActivityDescription,"ActivityDescription"),
                               Convert.ToString(uc.Press),
                               Convert.ToString(uc.Priority),
                               
                               Convert.ToString(uc.TASKUNIQUEID),

                               Convert.ToString(uc.Dia),
                               Convert.ToString(uc.Thk),
                               Convert.ToString(uc.NoofPetal),
                               Convert.ToString(uc.WeightKG),
                               uc.Construction,

                               uc.MaterialAvailability,
                               uc.DiePunchAvailability,
                               uc.TemplateAvailability,
                               uc.HTRAvailability,
                               uc.LockingCleatAvailability,
                               uc.FixtureCleatAvailability,

                               Convert.ToString(uc.TotalDurationInDays),
                               Convert.ToString(uc.PressingDurationInDays),
                               Convert.ToString(uc.ConcertoStartDate.HasValue ? uc.ConcertoStartDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                               Convert.ToString(uc.PlannedStartDate.HasValue ? uc.PlannedStartDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                               Convert.ToString(uc.PlannedEndDate.HasValue ? uc.PlannedEndDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),

                               Convert.ToString(uc.RequiredOn.HasValue ? uc.RequiredOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                               Convert.ToString(uc.ActualStart.HasValue ? uc.ActualStart.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),

                               uc.PlateCutting,
                               uc.Pressing,
                               uc.HT,
                               uc.RePressing,
                               uc.Setup,
                               uc.Inspection,
                               uc.DISP,

                               ((role == "FSP" && param.Status.ToLower() == clsImplementationEnum.Overlay.No.GetStringValue().ToLower()) ? "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.HeaderId+")'><i class='fa fa-trash'></i></a></span>" : "")
                               //param.Status == "Pending" && indextype == "m"? Helper.HTMLActionString(uc.HeaderId, "Edit", "Edit Record", "fa fa-edit")+Helper.HTMLActionString(uc.HeaderId, "Delete", "Delete Record", "fa fa-trash","DeleteLine("+uc.HeaderId+")"): ""
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string role, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.HTMLAutoComplete(newRecordId,"Project","","",false,"","",false,"","","Select","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Customer",  "", "", true, "", "","Customer"),
                                        Helper.HTMLAutoComplete(newRecordId,"ActivityDescription","","",false,"","",false,"","","Select","form-control ActivityDescription"),
                                        Helper.GenerateTextbox(newRecordId, "Press",  "", "", false, "", "","Press",true),
                                        Helper.GenerateTextbox(newRecordId, "Priority",  "", "", true, "", "3","numeric33 Priority"),
                                        
                                        GenerateHidden(newRecordId, "TASKUNIQUEID", "", "TASKUNIQUEID"),

                                        Helper.GenerateTextbox(newRecordId,"Dia",  "", "", false, "", "6","numeric6 Dia",false,"","Dia"),
                                        Helper.GenerateTextbox(newRecordId,"Thk",  "", "", false, "", "4","numeric4 Thk",false,"","Thk"),
                                        Helper.GenerateTextbox(newRecordId,"NoofPetal",  "", "", false, "", "3","numeric3 NoofPetal",false,"","NoofPetal"),
                                        Helper.GenerateTextbox(newRecordId,"WeightKG",  "", "", false, "", "6","numeric6 WeightKG",false,"","WeightKG"),
                                        Helper.GenerateTextbox(newRecordId, "Construction",  "", "", false, "", "100","form-control Construction"),

                                        Helper.HTMLAutoComplete(newRecordId,"MaterialAvailability","No","",false,"","",false,"","","Select","form-control MaterialAvailability"),
                                        Helper.HTMLAutoComplete(newRecordId,"DiePunchAvailability","No","",false,"","",false,"","","Select","form-control DiePunchAvailability"),
                                        Helper.HTMLAutoComplete(newRecordId,"TemplateAvailability","No","",false,"","",false,"","","Select","form-control TemplateAvailability"),
                                        Helper.HTMLAutoComplete(newRecordId,"HTRAvailability","No","",false,"","",false,"","","Select","form-control HTRAvailability"),
                                        Helper.HTMLAutoComplete(newRecordId,"LockingCleatAvailability","No","",false,"","",false,"","","Select","form-control LockingCleatAvailability"),
                                        Helper.HTMLAutoComplete(newRecordId,"FixtureCleatAvailability","No","",false,"","",false,"","","Select","form-control FixtureCleatAvailability"),

                                        Helper.GenerateTextbox(newRecordId, "TotalDurationInDays",  "", "", false, "", "","TotalDurationInDays",true),
                                        Helper.GenerateTextbox(newRecordId, "PressingDurationInDays",  "", "", false, "", "","PressingDurationInDays",true),
                                        Helper.GenerateTextbox(newRecordId, "ConcertoStartDate",  "", "", false, "", "","ConcertoStartDate",true),
                                        Helper.GenerateTextbox(newRecordId, "PlannedStartDate",  "", "", false, "", "","form-control datePicker PlannedStartDate",false),
                                        Helper.GenerateTextbox(newRecordId, "PlannedEndDate",  "", "", false, "", "","PlannedEndDate",true),

                                        Helper.GenerateTextbox(newRecordId, "RequiredOn",  "", "",false,"","","form-control datePicker RequiredOn",true, "", ""),
                                        Helper.GenerateTextbox(newRecordId, "ActualStart",  "", "",false,"","","form-control datePicker ActualStart",false, "", ""),

                                        Helper.HTMLAutoComplete(newRecordId,"PlateCutting","","",false,"","",false,"","","Select","form-control PlateCutting"),
                                        Helper.HTMLAutoComplete(newRecordId,"Pressing","","",false,"","",false,"","","Select","form-control Pressing"),
                                        Helper.HTMLAutoComplete(newRecordId,"HT","","",false,"","",false,"","","Select","form-control HT"),
                                        Helper.HTMLAutoComplete(newRecordId,"RePressing","","",false,"","",false,"","","Select","form-control RePressing"),
                                        Helper.HTMLAutoComplete(newRecordId,"Setup","","",false,"","",false,"","","Select","form-control Setup"),
                                        Helper.HTMLAutoComplete(newRecordId,"Inspection","","",false,"","",false,"","","Select","form-control Inspection"),
                                        Helper.HTMLAutoComplete(newRecordId,"DISP","","",false,"","",false,"","","Select","form-control DISP"),

                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "Savedata()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_DEN_GET_MAINTAIN_DATA(1, 0, "", "HeaderId = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                Convert.ToString(uc.HeaderId),
                                uc.Project,
                                Generatetooltipdesc(uc.Customer,"Customer"),
                                Generatetooltipdesc(uc.ActivityDescription,"ActivityDescription"),
                                uc.Press,
                                Convert.ToString(uc.Priority),
                                
                                Convert.ToString(uc.TASKUNIQUEID),

                                Convert.ToString(uc.Dia),
                                Convert.ToString(uc.Thk),
                                Convert.ToString(uc.NoofPetal),
                                Convert.ToString(uc.WeightKG),
                                uc.Construction,

                                uc.MaterialAvailability,
                                uc.DiePunchAvailability,
                                uc.TemplateAvailability,
                                uc.HTRAvailability,
                                uc.LockingCleatAvailability,
                                uc.FixtureCleatAvailability,

                                Convert.ToString(uc.TotalDurationInDays),
                                Convert.ToString(uc.PressingDurationInDays),

                                Convert.ToString(uc.ConcertoStartDate.HasValue ? uc.ConcertoStartDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                                Convert.ToString(uc.PlannedStartDate.HasValue ? uc.PlannedStartDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                                Convert.ToString(uc.PlannedEndDate.HasValue ? uc.PlannedEndDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                                Convert.ToString(uc.RequiredOn.HasValue ? uc.RequiredOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                                Convert.ToString(uc.ActualStart.HasValue ? uc.ActualStart.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),

                                uc.PlateCutting,
                                uc.Pressing,
                                uc.HT,
                                uc.RePressing,
                                uc.Setup,
                                uc.Inspection,
                                uc.DISP,
                          }).ToList();
                    }
                    else
                    {
                        data = (from uc in lstResult
                                select new[] {
                                        uc.HeaderId.ToString(),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"Project",uc.Project,"",false,"","",true,"","","Select","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Customer",uc.Customer, "", true, "", "","Customer"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"ActivityDescription",uc.ActivityDescription,"",false,"","",true,"","","Select","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Press",uc.Press, "", true, "", "","Press"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Priority",  Convert.ToString(uc.Priority), "UpdateData(this, "+ uc.HeaderId+",true)", (role == "JP"?true:false), "", "3","Priority numeric33"),
                                        
                                        GenerateHidden(uc.HeaderId, "TASKUNIQUEID", Convert.ToString(uc.TASKUNIQUEID), "TASKUNIQUEID"),

                                        Helper.GenerateTextbox(uc.HeaderId, "Dia",  Convert.ToString(uc.Dia), "UpdateData(this, "+ uc.HeaderId+",true)", (role == "JP"?true:false), "", "6","Dia numeric6"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Thk",  Convert.ToString(uc.Thk), "UpdateData(this, "+ uc.HeaderId+",true)", (role == "JP"?true:false), "", "4","Thk numeric4"),
                                        Helper.GenerateTextbox(uc.HeaderId, "NoofPetal",  Convert.ToString(uc.NoofPetal), "UpdateData(this, "+ uc.HeaderId+",true)", (role == "JP"?true:false), "", "3","NoofPetal numeric3"),
                                        Helper.GenerateTextbox(uc.HeaderId, "WeightKG",  Convert.ToString(uc.WeightKG), "UpdateData(this, "+ uc.HeaderId+",true)", (role == "JP"?true:false), "", "6","WeightKG numeric6"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Construction",  Convert.ToString(uc.Construction), "UpdateData(this, "+ uc.HeaderId+",true)", (role == "JP"?true:false), "", "100","form-control Construction"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "MaterialAvailability",uc.MaterialAvailability,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable MaterialAvailability"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "DiePunchAvailability",uc.DiePunchAvailability,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable DiePunchAvailability"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "TemplateAvailability",uc.TemplateAvailability,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable TemplateAvailability"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "HTRAvailability",uc.HTRAvailability,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable HTRAvailability"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "LockingCleatAvailability",uc.LockingCleatAvailability,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable LockingCleatAvailability"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "FixtureCleatAvailability",uc.FixtureCleatAvailability,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable FixtureCleatAvailability"),

                                        Helper.GenerateTextbox(uc.HeaderId, "TotalDurationInDays",Convert.ToString(uc.TotalDurationInDays), "", true, "", "","TotalDurationInDays"),
                                        Helper.GenerateTextbox(uc.HeaderId, "PressingDurationInDays",Convert.ToString(uc.PressingDurationInDays), "", true, "", "","PressingDurationInDays"),
                                        Helper.GenerateTextbox(uc.HeaderId, "ConcertoStartDate",uc.ConcertoStartDate.HasValue?uc.ConcertoStartDate.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker ConcertoStartDate",true,"UpdateData(this, "+ uc.HeaderId+",true)","ConcertoStartDate"),
                                        Helper.GenerateTextbox(uc.HeaderId, "PlannedStartDate",uc.PlannedStartDate.HasValue?uc.PlannedStartDate.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker PlannedStartDate",(role == "JP"?true:false),"UpdateData(this, "+ uc.HeaderId+",true)","PlannedStartDate"),
                                        Helper.GenerateTextbox(uc.HeaderId, "PlannedEndDate",uc.PlannedEndDate.HasValue?uc.PlannedEndDate.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker PlannedEndDate",true,"UpdateData(this, "+ uc.HeaderId+",true)","PlannedEndDate"),

                                        Helper.GenerateTextbox(uc.HeaderId, "RequiredOn",uc.RequiredOn.HasValue?uc.RequiredOn.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker RequiredOn",(role == "FSP"?true:false),"","RequiredOn"),
                                        Helper.GenerateTextbox(uc.HeaderId, "ActualStart",uc.ActualStart.HasValue?uc.ActualStart.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker ActualStart",(role == "JP"?true:false),"UpdateData(this, "+ uc.HeaderId+",true)","ActualStart"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "PlateCutting",uc.PlateCutting,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable PlateCutting"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Pressing",uc.Pressing,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable Pressing"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "HT",uc.HT,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable HT"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "RePressing",uc.RePressing,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable RePressing"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Setup",uc.Setup,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable Setup"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Inspection",uc.Inspection,"", false, "", "",(role == "JP"?true:false),"","","Select","form-control editable Inspection"),
                                        //Helper.HTMLAutoComplete(uc.HeaderId, "DISP",uc.DISP,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","DISP","form-control editable DISP"),
                                        Helper.GenerateTextbox(uc.HeaderId, "DISP",  Convert.ToString(uc.DISP), "UpdateData(this, "+ uc.HeaderId+",true)", false, "", "","form-control DISP",(role == "JP"?true:false),"","Select"),

                                        (role == "FSP" ? "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.HeaderId+")'><i class='fa fa-trash'></i></a></span> &nbsp" : " " ) +
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Save' class='blue action' onclick='ReloadGrid()'><i class='fa fa-save'></i></a></span>",

                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                double pressingdurationday = Convert.ToDouble(!string.IsNullOrEmpty(fc["PressingDurationInDays" + 0]) ? Convert.ToDouble(fc["PressingDurationInDays" + 0]) : 0);
                int pressingdurationdayd = (int)Math.Round(pressingdurationday) ;
                var den001 = new DEN001();

                den001.Project = fc["Project" + 0];
                den001.TASKUNIQUEID = Convert.ToInt32(!string.IsNullOrEmpty(fc["TASKUNIQUEID" + 0]) ? Convert.ToInt32(fc["TASKUNIQUEID" + 0]) : 0);

                if (db.DEN001.Any(x => x.Project == den001.Project && x.TASKUNIQUEID == den001.TASKUNIQUEID))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project and Activity Description Already Exist";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                den001.Customer = fc["Customer" + 0];
                den001.ActivityDescription = fc["ActivityDescription" + 0];
                den001.Dia = Convert.ToDouble(fc["Dia" + 0]);
                den001.Thk = Convert.ToDouble(fc["Thk" + 0]);
                den001.NoofPetal = Convert.ToDouble(fc["NoofPetal" + 0]);
                den001.WeightKG = Convert.ToDouble(fc["WeightKG" + 0]);
                den001.Construction = fc["Construction" + 0];
                den001.Press = fc["Press" + 0];
                den001.MaterialAvailability = fc["MaterialAvailability" + 0];
                den001.DiePunchAvailability = fc["DiePunchAvailability" + 0];
                den001.TemplateAvailability = fc["TemplateAvailability" + 0];
                den001.HTRAvailability = fc["HTRAvailability" + 0];
                den001.LockingCleatAvailability = fc["LockingCleatAvailability" + 0];
                den001.FixtureCleatAvailability = fc["FixtureCleatAvailability" + 0];
                den001.TotalDurationInDays = Convert.ToInt32(!string.IsNullOrEmpty(fc["TotalDurationInDays" + 0]) ? Convert.ToInt32(fc["TotalDurationInDays" + 0]) : 0);
                den001.PressingDurationInDays = pressingdurationdayd;
                den001.ConcertoStartDate = (!string.IsNullOrEmpty(fc["ConcertoStartDate" + 0]) ? Convert.ToDateTime(fc["ConcertoStartDate" + 0]) : (DateTime?)null);
                den001.PlannedStartDate = (!string.IsNullOrEmpty(fc["PlannedStartDate" + 0]) ? Convert.ToDateTime(fc["PlannedStartDate" + 0]) : (DateTime?)null);
                den001.PlannedEndDate = (!string.IsNullOrEmpty(fc["PlannedEndDate" + 0]) ? Convert.ToDateTime(fc["PlannedEndDate" + 0]) : (DateTime?)null);
                //den001.RequiredOn = (!string.IsNullOrEmpty(fc["RequiredOn" + 0]) ? Convert.ToDateTime(fc["RequiredOn" + 0]) : (DateTime?)null);
                den001.ActualStart = (!string.IsNullOrEmpty(fc["ActualStart" + 0]) ? Convert.ToDateTime(fc["ActualStart" + 0]) : (DateTime?)null);
                den001.PlateCutting = fc["PlateCutting" + 0];
                den001.Pressing = fc["Pressing" + 0];
                den001.HT = fc["HT" + 0];
                den001.RePressing = fc["RePressing" + 0];
                den001.Setup = fc["Setup" + 0];
                den001.Inspection = fc["Inspection" + 0];
                den001.DISP = fc["DISP" + 0];
                den001.CreatedOn = DateTime.Now;
                den001.CreatedBy = objClsLoginInfo.UserName;

                int maxpriority = 0;
                if (db.DEN001.Where(x => x.Press.ToLower() == den001.Press.ToLower() && x.DISP.ToLower() != "yes").OrderByDescending(x => x.Priority).Any())
                {
                    maxpriority = db.DEN001.Where(x => x.Press.ToLower() == den001.Press.ToLower() && x.DISP.ToLower() != "yes").OrderByDescending(x => x.Priority).FirstOrDefault().Priority;
                }
                den001.Priority = maxpriority + 1;

                db.DEN001.Add(den001);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data save successfully.";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private int SetDENPriority(int oldPriority, int newPriority)
        {
            if (oldPriority != newPriority)
            {
                var lowP = newPriority > oldPriority ? oldPriority : newPriority;
                var highP = newPriority < oldPriority ? oldPriority : newPriority;
                var lstDen1 = db.DEN001.Where(w => w.Priority >= lowP && w.Priority <= highP).OrderBy(o => o.Priority);
                foreach (var item in lstDen1)
                {
                    item.Priority = highP--;
                }
                //var newDen = db.DEN001.Where(w => w.Priority == newPriority);
                //var oldDen = db.DEN001.Where(w => w.Priority == oldPriority);
                db.SaveChanges();
            }
            return newPriority;
        }

        [HttpPost]
        public JsonResult DeleteLines(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var objDEN001 = db.DEN001.FirstOrDefault(x => x.HeaderId == HeaderId);
            try
            {
                if (objDEN001 != null)
                {
                    db.DEN001.Remove(objDEN001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record deleted successfully";
                }
                else
                {
                    objResponseMsg.Value = "Some thing Went to Wrong, Please try Again Later";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult GetProjectByCustomer(string term, string Project, string Press)
        {
            try
            {
                List<GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION> lstgetAllbom = getDataConcerto("", "").ToList();

                if (lstgetAllbom != null)
                {
                    dynamic item;

                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        item = lstgetAllbom.Where(x => x.Press.ToLower() == Press.ToLower() && x.Project.Trim().ToLower().Contains(term.Trim().ToLower())).Select(x => new
                        {
                            Value = x.Project,
                            Customer = x.Customer
                        }).Distinct().ToList();
                    }
                    else {
                        item = lstgetAllbom.Where(x => x.Press.ToLower() == Press.ToLower()).Select(x => new
                        {
                            Value = x.Project,
                            Customer = x.Customer
                        }).Distinct().Take(10).ToList();
                    }

                    return Json(item, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetprojectByTaskDescription(string term, string Project, string TaskDescription, string Press)
        {
            try
            {
                List<GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION> lstgetAllbom = getDataConcerto(Project, TaskDescription).ToList();
                string planstartdate = string.Empty;
                if (!string.IsNullOrEmpty(Project))
                {
                    dynamic item;

                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        item = lstgetAllbom.Where(x => x.Press.ToLower() == Press.ToLower() && x.Project.Trim().ToLower().Contains(term.Trim().ToLower())).Select(x => new
                        {
                            Value = x.TaskDescription,
                            Press = x.Press,
                            TASKUNIQUEID = x.TASKUNIQUEID,
                            totalduration = x.TotalDuration,
                            PressingDurationInDays = x.PressDuration,
                            ConcertoStartDate = x.ConcertoStartDate
                        }).Distinct().ToList();
                    }
                    else
                    {
                        item = lstgetAllbom.Where(x => x.Press.ToLower() == Press.ToLower()).Select(x => new
                        {
                            Value = x.TaskDescription,
                            Press = x.Press,
                            TASKUNIQUEID = x.TASKUNIQUEID,
                            totalduration = x.TotalDuration,
                            PressingDurationInDays = x.PressDuration,
                            ConcertoStartDate = x.ConcertoStartDate
                        }).Distinct().Take(10).ToList();
                    }
                    return Json(item , JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public List<GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION> getDataConcerto(string project, string taskDescription)
        {
            List<GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION> data = new List<GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION>();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                connection.Open();

                string query = $@"  
                                    select 
			                                Project
			                                ,Customer
			                                ,ProjectDescription
			                                ,TASKUNIQUEID
			                                ,TaskDescription
			                                ,TotalDuration
			                                ,Press
			                                ,ResourceFactor
			                                ,PressDuration
			                                ,ConcertoStartDate
	                                from	VW_Concerto_DEN_Data 
                                    where	(Project = @ProjectNo or @ProjectNo = '' )
			                        and 
			                        (TaskDescription = @TaskDescription or @TaskDescription = '')";

                using (var command = new SqlCommand(query, connection))
                {
                    command.CommandTimeout = 0;
                    command.Parameters.AddWithValue("@ProjectNo", project);
                    command.Parameters.AddWithValue("@TaskDescription", taskDescription);

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION f = new GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION();
                        f.Project = Convert.ToString(reader["Project"]); 
                        f.Customer = Convert.ToString(reader["Customer"]);
                        f.ProjectDescription = Convert.ToString(reader["ProjectDescription"]);
                        f.TASKUNIQUEID = Convert.ToDouble(reader["TASKUNIQUEID"]);
                        f.TaskDescription = Convert.ToString(reader["TaskDescription"]);
                        f.TotalDuration = Convert.ToDouble(reader["TotalDuration"]);
                        f.Press = Convert.ToString(reader["Press"]);
                        f.ResourceFactor = Convert.ToDecimal(reader["ResourceFactor"]);
                        f.PressDuration = Convert.ToDouble(reader["PressDuration"]);
                        f.ConcertoStartDate = (!string.IsNullOrEmpty(Convert.ToString(reader["ConcertoStartDate"])) ? Convert.ToDateTime(Convert.ToString(reader["ConcertoStartDate"])).ToString("dd/MM/yyyy") : "");

                        data.Add(f);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            finally
            {
                connection.Close();
            }
            return data;
        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                columnValue = columnValue.Replace("'", "");

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName.ToLower() == "priority")
                    {
                        SetPrioritybasechange(id, columnValue);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else if (columnName.ToLower() == "disp")
                    {
                        if(columnValue == "Yes")
                            SetPriorityondispatch(id);
                    }
                    else if (columnName.ToLower() == "plannedstartdate")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                            SetStartDatePrioritybasechange(id, columnValue);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (columnName.ToLower() == "plannedenddate")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                        }
                    }
                    else if (columnName.ToLower() == "requiredon")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                        }
                    }
                    else if (columnName.ToLower() == "actualstart")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                        }
                    }
                    db.SP_COMMON_TABLE_UPDATE("den001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("den001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getmaxpriority(int hid)
        {
            var objResponseMsg = new ResponseMsgWithStatus();
            objResponseMsg.Maxdays = 1;
            try
            {
                if (hid > 0)
                {
                    var prioritydata = db.DEN001.Where(x=>x.HeaderId == hid).FirstOrDefault();
                    if (prioritydata != null)
                    {
                        var pdays = db.DEN001.Where(x=>x.Press.ToLower() == prioritydata.Press.ToLower() && x.DISP.ToLower() != "yes").OrderByDescending(x => x.Priority).FirstOrDefault();

                        if (pdays != null)
                        {
                            objResponseMsg.Maxdays = pdays.Priority;
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPressingdays(double pressingdays, string press, int durationdays)
        {
            var objResponseMsg = new ResponseMsgWithStatus();
            objResponseMsg.Key = true;

            try
            {
                if (!string.IsNullOrEmpty(press))
                {
                    var prioritydata = db.DEN001.Where(x=>x.Press.ToLower() == press.ToLower() && x.DISP.ToLower() != "yes").OrderByDescending(x => x.Priority).FirstOrDefault();

                    if (prioritydata != null)
                    {
                        int pressdays = (int)Math.Round(pressingdays);
                        DateTime? newplanneddate = prioritydata.PlannedStartDate;
                        DateTime ndate = newplanneddate.HasValue ? newplanneddate.Value.AddDays(pressdays) : DateTime.Now;
                        TimeSpan difference = ndate - DateTime.Now;

                        objResponseMsg.Days = difference.Days;
                        objResponseMsg.Startdate = Convert.ToDateTime(ndate).ToString("dd/MM/yyyy");
                        objResponseMsg.Enddate = Convert.ToDateTime(ndate.AddDays(durationdays)).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        objResponseMsg.Days = 0;
                        objResponseMsg.Startdate = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture); 
                        objResponseMsg.Enddate = Convert.ToDateTime(DateTime.Now.AddDays(durationdays)).ToString("dd/MM/yyyy");
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_DEN_GET_MAINTAIN_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var data = (from uc in lst
                            select new
                            {
                                Project = uc.Project,
                                Customer = uc.Customer,
                                QualityProject = uc.ActivityDescription,
                                Priority = uc.Priority,
                                Dia = uc.Dia,
                                Thk = uc.Thk,
                                NoofPetal = uc.NoofPetal,
                                WeightKG = uc.WeightKG,
                                Construction = uc.Construction,
                                MaterialAvailability = uc.MaterialAvailability,
                                DiePunchAvailability = uc.DiePunchAvailability,
                                TemplateAvailability = uc.TemplateAvailability,
                                HTRAvailability = uc.HTRAvailability,
                                LockingCleatAvailability = uc.LockingCleatAvailability,
                                FixtureCleatAvailability = uc.FixtureCleatAvailability,
                                TotalDurationInDays = uc.TotalDurationInDays,
                                PressingDurationInDays = uc.PressingDurationInDays,

                                ConcertoStartDate = Convert.ToString(uc.ConcertoStartDate.HasValue ? uc.ConcertoStartDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                PlannedStartDate = Convert.ToString(uc.PlannedStartDate.HasValue ? uc.ConcertoStartDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                PlannedEndDate = Convert.ToString(uc.PlannedEndDate.HasValue ? uc.ConcertoStartDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                RequiredOn = Convert.ToString(uc.RequiredOn.HasValue ? uc.RequiredOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                ActualStart = Convert.ToString(uc.ActualStart.HasValue ? uc.ActualStart.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),

                                PlateCutting = Convert.ToString(uc.PlateCutting),
                                Pressing = Convert.ToString(uc.Pressing),
                                HT = Convert.ToString(uc.HT),
                                RePressing = Convert.ToString(uc.RePressing),
                                Setup = Convert.ToString(uc.Setup),
                                Inspection = Convert.ToString(uc.Inspection),
                                DISP = Convert.ToString(uc.DISP),
                                
                            }).ToList();

                strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }


        public static string GenerateDropdown(int rowId, string columnName, List<string> itemList, string SelectedValue, string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", string onClick = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClick) ? "onclick='" + onClick + "'" : "";

            foreach (var item in itemList)
            {
                if (item == SelectedValue)
                {
                    selectOptions += "<option selected>" + item + "</option>";
                }
                else
                {
                    selectOptions += "<option>" + item + "</option>";
                }
            }

            return "<select id='" + inputID + "' name='" + inputID + "' " + (isReadOnly ? "disabled='disabled'" : "") + " " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' " + onClickEvent + " >" + selectOptions + "</select>";

        }

        [NonAction]
        public static string GenerateHidden(int rowId, string columnName, string columnValue = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue;

            htmlControl = "<input type='hidden' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "' class='" + className + "'/>";

            return htmlControl;
        }

        [NonAction]
        public static string Generatetooltipdesc(string columnValue = "", string columnName = "")
        {
            string htmlControl = string.Empty;
            string inputValue = string.Empty;

            if (!string.IsNullOrEmpty(columnValue))
            {
                if (columnName == "Customer")
                    inputValue = Ellipsis(columnValue, columnName, 9);
                else if (columnName == "ActivityDescription")
                    inputValue = Ellipsis(columnValue, columnName, 14);

                htmlControl = "<span title='" + columnValue + "'>" + inputValue + "</span>";
            }
            return htmlControl;
        }

        [NonAction]
        public static string Ellipsis(string text, string name, int length)
        {
            string value = string.Empty;
            value = text;
            if (value.Length > length)
                return value.Substring(0, length) + "...";

            return value;
        }

        public void SetPriorityondispatch(int id)
        {
            DEN001[] listden001 = null;
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            DateTime newPlannedstartdate = DateTime.Now;
            var oldrecord = db.DEN001.Where(x => x.HeaderId == id).FirstOrDefault();

            if (oldrecord != null)
            {
                listden001 = db.DEN001.Where(x => x.DISP.ToLower() != yes.ToLower() && x.Press.ToLower() == oldrecord.Press.ToLower() && x.Priority > oldrecord.Priority).OrderBy(p => p.Priority).ToArray();
                for (int i = 0; i < listden001.Length ; i++)
                {
                    listden001[i].Priority = (listden001[i].Priority - 1);
                    listden001[i].EditedBy = objClsLoginInfo.UserName;
                    listden001[i].EditedOn = DateTime.Now;
                }
                db.SaveChanges();
            }
        }

        public void SetPrioritybasechange(int id, string columnValue)
        {
            DEN001[] listden001 = null;
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            int neworderno = 0;
            neworderno = Convert.ToInt32(columnValue);

            var oldrecord = db.DEN001.Where(x => x.HeaderId == id).FirstOrDefault();

            if (neworderno > 0)
            {
                if (oldrecord.Priority < neworderno)
                    listden001 = db.DEN001.Where(x => x.DISP.ToLower() != yes.ToLower() && x.Press.ToLower() == oldrecord.Press.ToLower() && x.Priority >= oldrecord.Priority && x.Priority <= neworderno).OrderByDescending(p => p.Priority).ToArray();
                else
                    listden001 = db.DEN001.Where(x => x.DISP.ToLower() != yes.ToLower() && x.Press.ToLower() == oldrecord.Press.ToLower() && x.Priority <= oldrecord.Priority && x.Priority >= neworderno).OrderBy(p => p.Priority).ToArray();

                //for (int i = listden001.Length - 1; i > 0; i--)
                //{
                //    int temp = listden001[i].Priority;
                //    listden001[i].Priority = listden001[i - 1].Priority;
                //    listden001[i - 1].Priority = temp;

                //    listden001[i].EditedBy = objClsLoginInfo.UserName;
                //    listden001[i].EditedOn = DateTime.Now;
                //}

                for (int ii = 0; ii < listden001.Length - 1; ii++)
                {
                    int temp = listden001[ii].Priority;
                    listden001[ii].Priority = listden001[ii + 1].Priority;
                    listden001[ii + 1].Priority = temp;

                    listden001[ii].EditedBy = objClsLoginInfo.UserName;
                    listden001[ii].EditedOn = DateTime.Now;
                }

                listden001 = listden001.OrderBy(x => x.Priority).ToArray();

                for (int i = 0 ; i < listden001.Length ; i++)
                {
                    if (listden001[i].Priority == 1)
                    {
                        listden001[i].PlannedStartDate = DateTime.Now;
                    }
                    else
                    {
                        if (i == 0)
                        {
                            string press = listden001[i].Press;
                            int pri = listden001[i].Priority;
                            var getlastdatetime = db.DEN001.Where(x => x.DISP.ToLower() != "yes" && x.Press.ToLower() == press.ToLower() && x.Priority == (pri - 1)).FirstOrDefault().PlannedStartDate;
                            listden001[i].PlannedStartDate = getlastdatetime.Value.AddDays((int)listden001[i].PressingDurationInDays + 1);
                        }
                        else
                        {
                            listden001[i].PlannedStartDate = listden001[i - 1].PlannedStartDate.Value.AddDays((int)listden001[i - 1].PressingDurationInDays + 1);
                        }
                    }
                    listden001[i].PlannedEndDate = listden001[i].PlannedStartDate.Value.AddDays((int)listden001[i].TotalDurationInDays);
                }
                db.SaveChanges();
            }
        }

        public void SetStartDatePrioritybasechange(int id, string columnValue)
        {
            DEN001[] listden001 = null;
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            DateTime newPlannedstartdate = Convert.ToDateTime(columnValue);

            var oldrecord = db.DEN001.Where(x => x.HeaderId == id).FirstOrDefault();

            if (oldrecord != null)
            {
                if (!string.IsNullOrEmpty(columnValue))
                {
                    listden001 = db.DEN001.Where(x => x.DISP.ToLower() != yes.ToLower() && x.Press.ToLower() == oldrecord.Press.ToLower() && x.Priority >= oldrecord.Priority).OrderBy(p => p.Priority).ToArray();

                    for (int i = 0; i < listden001.Length; i++)
                    {
                        if (listden001[i].Priority == 1)
                        {
                            listden001[i].PlannedStartDate = newPlannedstartdate.AddDays((int)listden001[i].PressingDurationInDays + 1);
                        }
                        else
                        {
                            if (i == 0)
                                listden001[i].PlannedStartDate = newPlannedstartdate.AddDays((int)listden001[i].PressingDurationInDays + 1);
                            else
                                listden001[i].PlannedStartDate = listden001[i - 1].PlannedStartDate.Value.AddDays((int)listden001[i - 1].PressingDurationInDays + 1);
                        }
                        listden001[i].PlannedEndDate = listden001[i].PlannedStartDate.Value.AddDays((int)listden001[i].TotalDurationInDays);

                        listden001[i].EditedBy = objClsLoginInfo.UserName;
                        listden001[i].EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                }
            }
        }
    }

    public class ResponseMsgWithStatus : ResponseMsg
    {
        public int Maxdays { get; set; }
        public int Days { get; set; }
        public string Startdate { get; set; }
        public string Enddate { get; set; }
    }

    public partial class GET_PROJECT_BY_CUSTOMER_TASKDESCRIPTION
    {
        public string Project { get; set; }
        public string Customer { get; set; }
        public string ProjectDescription { get; set; }
        public double TASKUNIQUEID { get; set; }
        public string TaskDescription { get; set; }
        public Nullable<double> TotalDuration { get; set; }
        public string Press { get; set; }
        public Nullable<decimal> ResourceFactor { get; set; }
        public Nullable<double> PressDuration { get; set; }
        public string ConcertoStartDate { get; set; }
    }
}