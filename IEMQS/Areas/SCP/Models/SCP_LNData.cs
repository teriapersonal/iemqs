﻿using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace IEMQS.Areas.SCP.Models
{
    public class SCP_LNData
    {
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public string RevNo { get; set; }
        public string ReferenceDrawing { get; set; }
        public string ScopeOfWork { get; set; }
        public string EstFor { get; set; }
        public string Weight { get; set; }
        public string RCCHours { get; set; }
        public string TaskManager { get; set; }


        public int TASKUNIQUEID { get; set; }
        public DateTime ExpMatDelDate { get; set; }
        public DateTime ReqComDate { get; set; }
        public string SubConTo { get; set; }
        public string DeliverTo { get; set; }
        public bool IsLNLine { get; set; }
        public string CreatedBy { get; set; }
        public string TaskDecription { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}