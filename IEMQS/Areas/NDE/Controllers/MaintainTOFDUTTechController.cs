﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainTOFDUTTechController : clsBase
    {
        #region Main Header grid
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        // GET: NDE/MaintainTOFDUTTech
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadTOFDUTDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_TOFDUTHtmlPartial");
        }

        [HttpPost]
        public JsonResult LoadTOFDUTHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                "%' or QualityProject like '%" + param.sSearch +
                                "%' or lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde12.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or UTTechNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or UTProcedureNo like '%" + param.sSearch +
                                "%' or CouplantUsed like '%" + param.sSearch +
                                "%' or ReferenceReflectorSize like '%" + param.sSearch +
                                "%' or PlannerRemarks like '%" + param.sSearch +
                                "%' or ReturnRemarks like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch +
                                "%' or TechniqueSheetNo like '%" + param.sSearch +
                                "%' or (nde12.CreatedBy+'-'+c32.t_namb) like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde12.BU", "nde12.Location");

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_NDE_TOFD_UT_TECH_GETDETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.TechniqueSheetNo),
                            Convert.ToString(uc.UTTechNo),
                            Convert.ToString("R" + uc.RevNo),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.UTProcedureNo),
                            Convert.ToString(uc.ReturnRemarks),
                            //Convert.ToString(uc.HeaderId)
                            MakeDisplayButton(uc.HeaderId,uc.Status,Convert.ToInt32(uc.RevNo),true,true,false)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Header/detail form
        [SessionExpireFilter]
        public ActionResult AddHeader(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE012");
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadTOFDUTFormPartial(int HeaderID)
        {
            NDE012 objNDE012 = new NDE012();

            string user = objClsLoginInfo.UserName;
            string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
            List<Projects> project = Manager.getProjectsByUser(user);

            //List<string> lstEncoderType = clsImplementationEnum.getNDEEncoderType().ToList();
            //ViewBag.EncoderType = lstEncoderType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            List<string> lstEncoderType = clsImplementationEnum.getyesno().ToList();
            ViewBag.EncoderType = lstEncoderType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            List<string> lstScannerType = clsImplementationEnum.getNDEScannerType().ToList();
            ViewBag.ScannerType = lstScannerType.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            NDEModels objNDEModels = new NDEModels();
            if (HeaderID > 0)
            {
                objNDE012 = db.NDE012.Where(x => x.HeaderId == HeaderID).FirstOrDefault();

                ViewBag.CouplantUsed = objNDEModels.GetCategory("Couplant", objNDE012.CouplantUsed, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.CableType = objNDEModels.GetCategory("Cable Type", objNDE012.CableType, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.CableLength = objNDEModels.GetCategory("Cable Length", objNDE012.CableLength, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objNDE012.ScanningDetail, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.ReferenceReflectorSize = objNDEModels.GetCategory("Reference Reflector Size", objNDE012.ReferenceReflectorSize, objNDE012.BU, objNDE012.Location, true).CategoryDescription;

                objNDE012.Project = db.COM001.Where(i => i.t_cprj == objNDE012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objNDE012.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objNDE012.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.QualityProject = db.QMS010.Where(i => i.QualityProject == objNDE012.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                objNDE012.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objNDE012.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                ViewBag.Action = "edit";
            }
            else
            {
                objNDE012.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objNDE012.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objNDE012.RevNo = 0;
                ViewBag.DefaultEncoderType = clsImplementationEnum.NDEEncoderType.SemiAutomatic.GetStringValue();
                ViewBag.DefaultScannerType = clsImplementationEnum.NDEScannerType.SemiAutomatic.GetStringValue();
            }

            return PartialView("_TOFDUTTechFormPartial", objNDE012);
        }
        //Insert / update header
        [HttpPost]
        public ActionResult SaveTOFDHeader(NDE012 nde012)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (nde012.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde012.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    bool isRevised = false;
                    NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == nde012.HeaderId).FirstOrDefault();
                    //objNDE010.QualityProject = nde010.QualityProject;
                    //objNDE010.Project = nde010.Project;
                    //objNDE010.BU = nde010.BU;
                    //objNDE010.Location = nde010.Location;
                    if (objNDE012.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                    {
                        objNDE012.RevNo = Convert.ToInt32(objNDE012.RevNo) + 1;
                        objNDE012.ReturnRemarks = null;
                        objNDE012.ReturnedBy = null;
                        objNDE012.ReturnedOn = null;
                        objNDE012.SubmittedBy = null;
                        objNDE012.SubmittedOn = null;
                        objNDE012.ApprovedOn = null;
                        objNDE012.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        isRevised = true;
                    }
                    objNDE012.TechniqueSheetNo = nde012.TechniqueSheetNo;
                    objNDE012.CouplantUsed = nde012.CouplantUsed;
                    objNDE012.ReferenceReflectorSize = nde012.ReferenceReflectorSize;
                    objNDE012.ReferenceReflectorDistance = nde012.ReferenceReflectorDistance;
                    objNDE012.CalibrationBlockID = nde012.CalibrationBlockID;
                    objNDE012.CalibrationBlockIDThickness = nde012.CalibrationBlockIDThickness;
                    objNDE012.ScanningDetail = nde012.ScanningDetail;
                    objNDE012.EncoderType = nde012.EncoderType;
                    objNDE012.ScannerMake = nde012.ScannerMake;
                    objNDE012.ScannerType = nde012.ScannerType;
                    objNDE012.CableType = nde012.CableType;
                    objNDE012.CableLength = nde012.CableLength;
                    objNDE012.SimulationBlockID = nde012.SimulationBlockID;
                    objNDE012.SimulationBlockIDwiththickness = nde012.SimulationBlockIDwiththickness;
                    objNDE012.PlannerRemarks = nde012.PlannerRemarks;
                    objNDE012.UTProcedureNo = nde012.UTProcedureNo;
                    objNDE012.EditedBy = objClsLoginInfo.UserName;
                    objNDE012.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    var folderPath = "NDE012/" + objNDE012.HeaderId + "/R" + objNDE012.RevNo;
                    if (isRevised)
                    {
                        var oldFolderPath = "NDE012/" + objNDE012.HeaderId + "/R" + (objNDE012.RevNo - 1);
                        ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                        (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
                        //clsUploadCopy.CopyFolderContents(oldFolderPath, folderPath, objATH008, true);
                    }
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    objResponseMsg.HeaderID = objNDE012.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderUpdate.ToString();
                    objResponseMsg.Remarks = objNDE012.ReturnRemarks;
                    objResponseMsg.RevNo = objNDE012.RevNo;
                }
                else
                {
                    NDE012 objNDE012 = new NDE012();
                    string doc = nde012.UTTechNo.Split('/')[2];
                    int docNo = Convert.ToInt32(doc);
                    objNDE012.QualityProject = nde012.QualityProject;
                    objNDE012.Project = nde012.Project.Split('-')[0].ToString();
                    objNDE012.BU = nde012.BU.Split('-')[0].ToString();
                    objNDE012.Location = nde012.Location.Split('-')[0].ToString();
                    objNDE012.RevNo = 0;
                    objNDE012.UTTechNo = nde012.UTTechNo;
                    objNDE012.DocNo = docNo;
                    objNDE012.Status = nde012.Status;
                    objNDE012.UTProcedureNo = nde012.UTProcedureNo;
                    objNDE012.TechniqueSheetNo = nde012.TechniqueSheetNo;
                    objNDE012.CouplantUsed = nde012.CouplantUsed;
                    objNDE012.ReferenceReflectorSize = nde012.ReferenceReflectorSize;
                    objNDE012.ReferenceReflectorDistance = nde012.ReferenceReflectorDistance;
                    objNDE012.CalibrationBlockID = nde012.CalibrationBlockID;
                    objNDE012.CalibrationBlockIDThickness = nde012.CalibrationBlockIDThickness;
                    objNDE012.SimulationBlockID = nde012.SimulationBlockID;
                    objNDE012.SimulationBlockIDwiththickness = nde012.SimulationBlockIDwiththickness;
                    objNDE012.ScanningDetail = nde012.ScanningDetail;
                    objNDE012.ScannerMake = nde012.ScannerMake;
                    objNDE012.EncoderType = nde012.EncoderType;
                    objNDE012.ScannerType = nde012.ScannerType;
                    objNDE012.CableType = nde012.CableType;
                    objNDE012.CableLength = nde012.CableLength;
                    objNDE012.PlannerRemarks = nde012.PlannerRemarks;
                    objNDE012.CreatedBy = objClsLoginInfo.UserName;
                    objNDE012.CreatedOn = DateTime.Now;
                    db.NDE012.Add(objNDE012);
                    db.SaveChanges();
                    //var folderPath = "NDE012/" + objNDE012.HeaderId + "/R" + objNDE012.RevNo;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    objResponseMsg.HeaderID = objNDE012.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderInsert.ToString();
                    objResponseMsg.Remarks = objNDE012.ReturnRemarks;
                    objResponseMsg.RevNo = objNDE012.RevNo;

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //delete header
        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == headerId).FirstOrDefault();
                List<NDE013> objNDE013 = db.NDE013.Where(x => x.HeaderId == headerId).ToList();
                if (objNDE012 != null)
                {
                    if (objNDE013.Count > 0)
                    {
                        db.NDE013.RemoveRange(objNDE013);
                        db.SaveChanges();
                    }
                    db.NDE012.Remove(objNDE012);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                NDE012 objNDE012 = db.NDE012.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objNDE012 != null)
                {
                    objNDE012.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                    objNDE012.SubmittedBy = objClsLoginInfo.UserName;
                    objNDE012.SubmittedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE012.Project, objNDE012.BU, objNDE012.Location, "Technique No: " + objNDE012.UTTechNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveTOFDUTTech/ViewDocument?HeaderID=" + objNDE012.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details has been successfully sent for approval.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE012 != null)
            {
                if (objNDE012.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE012.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

       
        private bool IsapplicableForSave(int headerId)
        {
            NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE012 != null)
            {
                if (objNDE012.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }
        #endregion

        #region Lines
        //Lines form
        [HttpPost]
        public ActionResult GetTOFDUTLinesForm(int HeaderId, string projCode, string BU, string location, string utTechNo, int LineId)
        {
            NDE013 objNDE013 = new NDE013();
            string project = projCode.Split('-')[0];

            string Qmslocation = location.Split('-')[0];

            NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            var lstSearchUnit = db.NDE017.ToList();
            ViewBag.SearchUnit = lstSearchUnit.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.SearchUnit.ToString(), CatID = x.Id.ToString() }).ToList();

            //List<string> lstScannerType = clsImplementationEnum.getNDEScanType().ToList();
            List<GLB002> lstScannerType = Manager.GetSubCatagories("Scan Type", objNDE012.BU, objNDE012.Location).ToList();
            ViewBag.ScanType = lstScannerType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.Description, CatID = x.Code }).ToList();

            List<GLB002> lstScanningSurface = Manager.GetSubCatagories("Scanning Surface", objNDE012.BU, objNDE012.Location).ToList();
            ViewBag.ScanningSurface = lstScanningSurface.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.Description, CatID = x.Code }).ToList();

            if (LineId > 0)
            {
                objNDE013 = db.NDE013.Where(x => x.LineId == LineId).FirstOrDefault();
                var Project = (from a in db.COM001
                               where a.t_cprj == objNDE013.Project
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                ViewBag.Project = Project.pDesc;
                NDEModels objNDEModels = new NDEModels();
                ViewBag.SelectedScanType = objNDEModels.GetCategory("Scan Type", objNDE013.ScanType, objNDE013.BU, objNDE013.Location, true).CategoryDescription;
                ViewBag.SelectedScanningSurface = objNDEModels.GetCategory("Scanning Surface", objNDE013.ScanningSurface, objNDE013.BU, objNDE013.Location, true).CategoryDescription;
                objNDE013.NDE012.RevNo = objNDE013.RevNo;
                ViewBag.SearchUnit1 = db.NDE017.Where(x => x.Id == objNDE013.SearchUnit).Select(x => x.SearchUnit).FirstOrDefault();
                int searchunit = Convert.ToInt32(objNDE013.SearchUnit);
                NDE017 objNDE017 = db.NDE017.FirstOrDefault(x => x.Id == searchunit);
                ViewBag.Frequency = objNDE017.Frequency;
                ViewBag.Element = objNDE017.ElementSize;
                ViewBag.Beamangle = objNDE017.BeamAngle;
                ViewBag.make = objNDE017.SearchUnitMake;
                ViewBag.Action = "line edit";
            }
            else
            {
                objNDE013.RevNo = 0;

                int? _newScanno = 0;
                var maxScanNo = db.NDE013.Where(x => x.HeaderId == HeaderId).OrderByDescending(x => x.LineId).Select(x => x.ScanNo).FirstOrDefault();
                if (maxScanNo != null)
                {
                    _newScanno = maxScanNo + 1;
                }
                else
                { _newScanno = 1; }
                objNDE013.ScanNo = _newScanno;

                var Project = (from a in db.COM001
                               where a.t_cprj == project
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                ViewBag.Project = Project.pDesc;
            }

            objNDE013.HeaderId = objNDE012.HeaderId;

            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            objNDE013.QualityProject = objNDE012.QualityProject;
            ViewBag.BU = BU;
            ViewBag.Location = location;
            ViewBag.UTTechNo = utTechNo;
            return PartialView("_GetTOFDUTLinesForm", objNDE013);
        }

        [SessionExpireFilter]
        [HttpPost]
        public JsonResult LoadTOFDUTLineData(JQueryDataTableParamModel param)
        {
            try
            {

                string strWhere = string.Empty;
                strWhere += "1=1 and HeaderId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( RevNo like '%" + param.sSearch +
                               "%' or nde017.SearchUnit like '%" + param.sSearch +
                               "%' or ScanNo like '%" + param.sSearch +
                               "%' or ScanType like '%" + param.sSearch +
                               "%' or PCS like '%" + param.sSearch +
                               "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_NDE_TOFD_UT_LINE_GETDETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                             Convert.ToString(uc.LineId),
                             Convert.ToString(uc.ROW_NO),
                             Convert.ToString(uc.ScanNo),
                             Convert.ToString(uc.SearchUnit),
                             Convert.ToString(uc.ScanType),
                             Convert.ToString(uc.PCS),
                             Convert.ToString(uc.OFFSET),
                             Convert.ToString(uc.ScanningSurface),
                             Convert.ToString("R"+uc.RevNo),
                             Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveTOFDUTLines(NDE013 nde013, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isRevised = false;
                string message = "";
                bool successKey = false;
                NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == nde013.HeaderId).FirstOrDefault();
                var lstNDE013 = db.NDE013.Where(x => x.HeaderId == nde013.HeaderId && x.ScanNo == nde013.ScanNo).ToList();
                if (nde013.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde013.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                
                if (nde013.LineId > 0)
                {
                    NDE013 objNDE013 = db.NDE013.Where(x => x.LineId == nde013.LineId).FirstOrDefault();
                    if (objNDE012.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                    {
                        objNDE012.RevNo = Convert.ToInt32(objNDE012.RevNo) + 1;
                        objNDE013.RevNo = Convert.ToInt32(objNDE012.RevNo);
                        objNDE012.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        isRevised = true;
                        objNDE012.ReturnRemarks = null;
                        objNDE012.ReturnedBy = null;
                        objNDE012.ReturnedOn = null;
                        objNDE012.SubmittedBy = null;
                        objNDE012.SubmittedOn = null;
                        objNDE012.ApprovedOn = null;
                    }
                    objNDE013.UTTechNo = nde013.UTTechNo;
                    objNDE013.QualityProject = nde013.QualityProject;
                    objNDE013.SearchUnit = nde013.SearchUnit;
                    objNDE013.ScanNo = nde013.ScanNo;
                    objNDE013.ScanType = nde013.ScanType;
                    objNDE013.PCS = nde013.PCS;
                    objNDE013.OFFSET = nde013.OFFSET;
                    objNDE013.ScanningSurface = nde013.ScanningSurface;
                    objNDE013.EditedBy = objClsLoginInfo.UserName;
                    objNDE013.EditedOn = DateTime.Now;
                    successKey = true;
                    message = clsImplementationMessage.NDETechniquesMessage.LineUpdate;
                }
                else
                {
                    if (lstNDE013.Count == 0)
                    {
                        db.NDE013.Add(new NDE013
                        {
                            HeaderId = nde013.HeaderId,
                            Project = objNDE012.Project,
                            BU = objNDE012.BU,
                            Location = objNDE012.Location,
                            UTTechNo = objNDE012.UTTechNo,
                            QualityProject = objNDE012.QualityProject,
                            RevNo = 0,
                            SearchUnit = nde013.SearchUnit,
                            ScanNo = nde013.ScanNo,
                            ScanType = nde013.ScanType,
                            PCS = nde013.PCS,
                            OFFSET = nde013.OFFSET,
                            ScanningSurface = nde013.ScanningSurface,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                        if (objNDE012.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                        {
                            objNDE012.RevNo = Convert.ToInt32(objNDE012.RevNo) + 1;
                            objNDE012.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                            objNDE012.EditedBy = objClsLoginInfo.UserName;
                            objNDE012.EditedOn = DateTime.Now;
                            objNDE012.ReturnRemarks = null;
                            objNDE012.ReturnedBy = null;
                            objNDE012.ReturnedOn = null;
                            objNDE012.SubmittedBy = null;
                            objNDE012.SubmittedOn = null;
                            objNDE012.ApprovedOn = null;
                            isRevised = true;
                        }
                        successKey = true;
                        message = clsImplementationMessage.NDETechniquesMessage.LineInsert;
                    }
                    else
                    { successKey = false; message = clsImplementationMessage.CommonMessages.Duplicate; }
                }
                db.SaveChanges();
                if (isRevised)
                {
                    var oldFolderPath = "NDE012/" + objNDE012.HeaderId + "/R" + (objNDE012.RevNo - 1);
                    var newFolderPath = "NDE012/" + objNDE012.HeaderId + "/R" + (objNDE012.RevNo);
                    //ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                    //clsUploadCopy.CopyFolderContents(oldFolderPath, newFolderPath, objATH008, true);
                    (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, newFolderPath);
                }
                if (successKey)
                {
                    objResponseMsg.Key = true;
                }
                objResponseMsg.Value = message;
                objResponseMsg.Remarks = objNDE012.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                NDE013 objNDE013 = db.NDE013.Where(x => x.LineId == LineID).FirstOrDefault();
                
                if (objNDE013 != null)
                {
                    if (objNDE013.HeaderId > 0)
                    {
                        if (!IsapplicableForSubmit(objNDE013.HeaderId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    db.NDE013.Remove(objNDE013);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common Functiality
        [HttpPost]
        public ActionResult GetUTNumber(string qms, string loc)
        { //generate UT number
            if (!string.IsNullOrWhiteSpace(qms))
            {
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();
                NDE012 objNDE012 = new NDE012();
                //string project = new NDEModels().GetQMSProject(qms).Project;
                string strproject = db.QMS010.Where(x => x.QualityProject == qms).Select(x => x.Project).FirstOrDefault();

                string location = loc.Split('-')[0];
                var lstobjNDE012 = db.NDE012.Where(x => x.QualityProject == qms && x.Location == location).ToList();
                var project = (from a in db.COM001
                               where a.t_cprj == strproject
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                objProjectDataModel.Project = project.pDesc;

                string BU = db.COM001.Where(i => i.t_cprj == strproject).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                List<GLB002> lstCouplantGLB002 = Manager.GetSubCatagories("Couplant", BU, location).ToList();
                if (lstCouplantGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseCouplantUsed = lstCouplantGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstCouplantUsed = lstBULocWiseCouplantUsed;
                }

                List<GLB002> lstCLGLB002 = Manager.GetSubCatagories("Cable Length", BU, location).ToList();
                if (lstCLGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseCableLength = lstCLGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstCableLength = lstBULocWiseCableLength;
                }

                List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Cable Type", BU, location).ToList();
                if (lstCTGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseCableType = lstCTGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstCableType = lstBULocWiseCableType;
                }

                List<GLB002> lstSDGLB002 = Manager.GetSubCatagories("Scanning Detail", BU, location).ToList();
                //List<BULocWiseCategoryModel> lstBULocWiseScanDetail = null;
                if (lstSDGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseScanDetail = lstSDGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstScanningDetail = lstBULocWiseScanDetail;
                }

                List<GLB002> lstRRGLB002 = Manager.GetSubCatagories("Reference Reflector Size", BU, location).ToList();
                if (lstRRGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseRRS = lstRRGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstReferenceReflectorSize = lstBULocWiseRRS;
                }

                if (lstRRGLB002.Any() && lstSDGLB002.Any() && lstCTGLB002.Any() && lstCLGLB002.Any() && lstCouplantGLB002.Any())
                    objProjectDataModel.Key = true;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstCouplantGLB002.Any())
                        lstCategory.Add("Couplant");
                    if (!lstCLGLB002.Any())
                        lstCategory.Add("Cable Length");
                    if (!lstCTGLB002.Any())
                        lstCategory.Add("Cable Type");
                    if (!lstSDGLB002.Any())
                        lstCategory.Add("Scanning Detail");
                    if (!lstRRGLB002.Any())
                        lstCategory.Add("Reference Reflector Size");

                    objProjectDataModel.Key = false;
                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
                if (objProjectDataModel.Key)
                {
                    List<string> lstCategoryCode = new List<string>();
                    string cellulos = clsImplementationEnum.TOFDUTR1Changes.Cellulose_Water.GetStringValue();
                    if (!lstCouplantGLB002.Any(x => x.Description.Trim() == cellulos.Trim()))
                    {
                        lstCategoryCode.Add(cellulos + " in Couplant");
                    }
                    else
                    {
                        var category = lstCouplantGLB002.Where(x => x.Description.Trim() == cellulos.Trim()).FirstOrDefault();
                        objProjectDataModel.celluloscatCode = category.Code;
                        objProjectDataModel.celluloscatDesc = category.Description;
                    }
                    string Co_Axial = clsImplementationEnum.TOFDUTR1Changes.Co_Axial.GetStringValue();
                    if (!lstCTGLB002.Any(x => x.Description.Trim() == Co_Axial.Trim()))
                    {
                        lstCategoryCode.Add(Co_Axial + " in Cable Type");

                    }
                    else
                    {
                        var category = lstCTGLB002.Where(x => x.Description.Trim() == Co_Axial.Trim()).FirstOrDefault();
                        objProjectDataModel.Co_AxialcatCode = category.Code;
                        objProjectDataModel.Co_AxialcatDesc = category.Description;
                        //    objProjectDataModel.IsCodeExists = true;
                    }

                    string Semi_Automated = clsImplementationEnum.TOFDUTR1Changes.Semi_Automated.GetStringValue();
                    if (!lstSDGLB002.Any(x => x.Description.Trim() == Semi_Automated.Trim()))
                    {
                        lstCategoryCode.Add(Semi_Automated + " in Scanning Detail");
                    }
                    else
                    {
                        var category = lstSDGLB002.Where(x => x.Description.Trim() == Semi_Automated.Trim()).FirstOrDefault();
                        objProjectDataModel.Semi_AutomatedcatCode = category.Code;
                        objProjectDataModel.Semi_AutomatedcatDesc = category.Description;
                    }

                    string cableLength = clsImplementationEnum.TOFDUTR1Changes.CL5.GetStringValue();
                    if (!lstCLGLB002.Any(x => x.Description.Trim() == cableLength.Trim()))
                    {
                        lstCategoryCode.Add(cableLength + " in Cable Length");
                    }
                    else
                    {
                        var category = lstCLGLB002.Where(x => x.Description.Trim() == cableLength.Trim()).FirstOrDefault();
                        objProjectDataModel.cabelLengthcatCode = category.Code;
                        objProjectDataModel.cabelLengthcatDesc = category.Description;
                    }

                    if (lstCategoryCode.Count > 0)
                    {
                        objProjectDataModel.IsCodeExists = true;
                        objProjectDataModel.CodeValue = string.Format(string.Join(",", lstCategoryCode.ToList()) + "not exist for selected project/ Quality Project, Please contact Admin");
                    }
                }
                var UTNum = MakeTOFDUT(qms, location);
                objProjectDataModel.TechNo = UTNum;

                objProjectDataModel.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == qms).Select(x => x.ManufacturingCode).FirstOrDefault();

                return Json(objProjectDataModel);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveHeader(NDE012 nde012, FormCollection fc, HttpPostedFileBase fileScanPlan)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (nde012.HeaderId > 0)
                {
                    NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == nde012.HeaderId).FirstOrDefault();
                    //objNDE010.QualityProject = nde010.QualityProject;
                    //objNDE010.Project = nde010.Project;
                    //objNDE010.BU = nde010.BU;
                    //objNDE010.Location = nde010.Location;
                    if (objNDE012.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                    {
                        objNDE012.RevNo = Convert.ToInt32(objNDE012.RevNo) + 1;
                        objNDE012.ReturnRemarks = null;
                        objNDE012.ReturnedBy = null;
                        objNDE012.ReturnedOn = null;
                        objNDE012.SubmittedBy = null;
                        objNDE012.SubmittedOn = null;
                        objNDE012.ApprovedOn = null;
                        objNDE012.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    }
                    objNDE012.TechniqueSheetNo = nde012.TechniqueSheetNo;
                    objNDE012.CouplantUsed = nde012.CouplantUsed;
                    objNDE012.ReferenceReflectorSize = nde012.ReferenceReflectorSize;
                    objNDE012.ReferenceReflectorDistance = nde012.ReferenceReflectorDistance;
                    objNDE012.CalibrationBlockID = nde012.CalibrationBlockID;
                    objNDE012.CalibrationBlockIDThickness = nde012.CalibrationBlockIDThickness;
                    objNDE012.ScanningDetail = nde012.ScanningDetail;
                    objNDE012.EncoderType = nde012.EncoderType;
                    objNDE012.ScannerMake = nde012.ScannerMake;
                    objNDE012.ScannerType = nde012.ScannerType;
                    objNDE012.CableType = nde012.CableType;
                    objNDE012.CableLength = nde012.CableLength;
                    objNDE012.SimulationBlockID = nde012.SimulationBlockID;
                    objNDE012.SimulationBlockIDwiththickness = nde012.SimulationBlockIDwiththickness;
                    if (fileScanPlan != null && fileScanPlan.ContentLength > 0)
                    {
                        string fname = fileScanPlan.FileName;
                        objNDE012.ScanPlan = fname;
                    }
                    objNDE012.UTProcedureNo = nde012.UTProcedureNo;
                    objNDE012.PlannerRemarks = nde012.PlannerRemarks;
                    objNDE012.EditedBy = objClsLoginInfo.UserName;
                    objNDE012.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE012.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderUpdate.ToString();
                    objResponseMsg.Remarks = objNDE012.ReturnRemarks;
                }
                else
                {
                    NDE012 objNDE012 = new NDE012();
                    string doc = nde012.UTTechNo.Split('/')[2];
                    int docNo = Convert.ToInt32(doc);
                    objNDE012.QualityProject = nde012.QualityProject;
                    objNDE012.Project = nde012.Project.Split('-')[0].ToString();
                    objNDE012.BU = nde012.BU.Split('-')[0].ToString();
                    objNDE012.Location = nde012.Location.Split('-')[0].ToString();
                    objNDE012.RevNo = 0;
                    objNDE012.UTTechNo = nde012.UTTechNo;
                    objNDE012.DocNo = docNo;
                    objNDE012.Status = nde012.Status;
                    objNDE012.UTProcedureNo = nde012.UTProcedureNo;
                    objNDE012.TechniqueSheetNo = nde012.TechniqueSheetNo;
                    objNDE012.CouplantUsed = nde012.CouplantUsed;
                    objNDE012.ReferenceReflectorSize = nde012.ReferenceReflectorSize;
                    objNDE012.ReferenceReflectorDistance = nde012.ReferenceReflectorDistance;
                    objNDE012.CalibrationBlockID = nde012.CalibrationBlockID;
                    objNDE012.CalibrationBlockIDThickness = nde012.CalibrationBlockIDThickness;
                    objNDE012.ScanningDetail = nde012.ScanningDetail;
                    objNDE012.ScannerMake = nde012.ScannerMake;
                    objNDE012.EncoderType = nde012.EncoderType;
                    objNDE012.ScannerType = nde012.ScannerType;
                    objNDE012.CableType = nde012.CableType;
                    objNDE012.CableLength = nde012.CableLength;
                    objNDE012.SimulationBlockID = nde012.SimulationBlockID;
                    objNDE012.SimulationBlockIDwiththickness = nde012.SimulationBlockIDwiththickness;
                    if (fileScanPlan != null && fileScanPlan.ContentLength > 0)
                    {
                        string fname = fileScanPlan.FileName;
                        objNDE012.ScanPlan = fname;
                    }
                    objNDE012.PlannerRemarks = nde012.PlannerRemarks;
                    objNDE012.CreatedBy = objClsLoginInfo.UserName;
                    objNDE012.CreatedOn = DateTime.Now;
                    db.NDE012.Add(objNDE012);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE012.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderInsert.ToString();
                    objResponseMsg.Remarks = objNDE012.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string MakeTOFDUT(string QualityProject, string currentLoc)
        {
            var lstobjNDE012 = db.NDE012.Where(x => x.QualityProject == QualityProject && x.Location == currentLoc).ToList();
            string UTNumber = "";
            string TOFDUTNo = string.Empty;

            if (lstobjNDE012 != null && lstobjNDE012.Count() != 0)
            {
                var UTNum = (from a in db.NDE012
                             where a.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase) && a.QualityProject.Equals(QualityProject, StringComparison.OrdinalIgnoreCase)
                             select a).Max(a => a.DocNo);
                UTNumber = (UTNum + 1).ToString();
            }
            else
            {
                UTNumber = "1";
            }
            TOFDUTNo = "UT/TOFD/" + UTNumber.PadLeft(3, '0');
            return TOFDUTNo;
        }
        public class ResponceDocNumber : QualityProjectResponse
        {
            public string UTTechNum;
        }
        public class ResponceHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }


        public string MakeDisplayButton(int HeaderID, string status, int revision, bool viewTimeLine = false, bool viewHistory = false, bool isHistory = false)
        {
            string strButton = string.Empty;
            strButton = "<center style='white-space: nowrap;cursor:pointer;margin-left:5px;' class='clsDisplayInlineEle'><a title='View' href='"+ WebsiteURL+"/NDE/MaintainTOFDUTTech/AddHeader?HeaderID=" + HeaderID + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a>";
            if (status == IEMQSImplementation.clsImplementationEnum.NDETechniqueStatus.Draft.ToString() && revision == 0)
            {
                strButton += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader('" + HeaderID + "')\"></i>";
            }
            else
            {
                strButton += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;opacity:0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" ></i>";
            }
            if (viewTimeLine)
            {
                strButton += "<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainTOFDUTTech/ShowTimeline?HeaderID=" + Convert.ToInt32(HeaderID) + "&isHistory=" + isHistory + "');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>";
            }
            if (viewHistory)
            {
                if (revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButton += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderID) + "'); style = \"margin-left:5px;cursor:pointer;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButton += "<i title=\"History\" style = \"margin-left:5px;opacity:0.5;cursor:pointer;\" class=\"fa fa-history\"></i>";
                }
            }
            strButton += "</center>";
            return strButton;
        }


        public class BUWiseDropdown : ProjectDataModel
        {
            public string Project { get; set; }
            public string TechNo { get; set; }
            public new string ManufacturingCode { get; set; }
            public bool Key { get; set; }
            public bool IsCodeExists { get; set; }
            public string CodeValue { get; set; }
            public string Value { get; set; }
            public string celluloscatCode { get; set; }
            public string celluloscatDesc { get; set; }
            public string Co_AxialcatCode { get; set; }
            public string Co_AxialcatDesc { get; set; }

            public string cabelLengthcatCode { get; set; }
            public string cabelLengthcatDesc { get; set; }
            public string Semi_AutomatedcatCode { get; set; }
            public string Semi_AutomatedcatDesc { get; set; }
            public List<BULocWiseCategoryModel> lstCableLength { get; set; }
            public List<BULocWiseCategoryModel> lstCouplantUsed { get; set; }
            public List<BULocWiseCategoryModel> lstCableType { get; set; }
            public List<BULocWiseCategoryModel> lstScanningDetail { get; set; }
            public List<BULocWiseCategoryModel> lstReferenceReflectorSize { get; set; }
        }
        #endregion

        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for copy Details
        {
            return PartialView("~/Areas/NDE/Views/Shared/_CopyQualityProjectPartial.cshtml");
        }
        [HttpPost]
        public ActionResult copyDetails(string QualityProject, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx.Trim()).FirstOrDefault();

                string project = new NDEModels().GetQMSProject(QualityProject).Project;

                // NDE012 objNDE012 = db.NDE012.Where(x => x.QualityProject == QualityProject && x.Location == currentLoc).FirstOrDefault();
                NDE012 objExistingNDE012 = db.NDE012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var UTNum = MakeTOFDUT(QualityProject, currentLoc);
                #region old code
                //if (objNDE012 != null)
                //{
                //    if (objNDE012.Location.Trim() == currentLoc)
                //    {
                //        if (objNDE012.Status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue())
                //        {
                //            objResponseMsg = InsertNewDoc(objExistingNDE012, objNDE012, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "TOFD UT Technique already exists.";
                //        }
                //    }
                //    else
                //    {
                //        NDE012 objDesNDE012 = new NDE012();
                //        string BU = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
                //        objDesNDE012.QualityProject = QualityProject;
                //        objDesNDE012.Project = Project;
                //        objDesNDE012.BU = BU;
                //        objDesNDE012.Location = currentLoc;
                //        objDesNDE012.UTTechNo = UTNum;
                //        objResponseMsg = InsertNewDoc(objExistingNDE012, objDesNDE012, false);
                //    }
                //}
                //else
                //{
                #endregion
                NDE012 objDesNDE012 = new NDE012();

                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;
                objDesNDE012.QualityProject = QualityProject;
                objDesNDE012.Project = project;
                objDesNDE012.BU = BU;
                objDesNDE012.Location = currentLoc;
                objDesNDE012.UTTechNo = UTNum;
                objResponseMsg = InsertNewDoc(objExistingNDE012, objDesNDE012, false);
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus InsertNewDoc(NDE012 objSrcNDE012, NDE012 objDesNDE012, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string doc = objDesNDE012.UTTechNo.Split('/')[2];
                int docNo = Convert.ToInt32(doc);
                objDesNDE012.RevNo = 0;
                objDesNDE012.DocNo = docNo;
                objDesNDE012.UTProcedureNo = objSrcNDE012.UTProcedureNo;
                objDesNDE012.TechniqueSheetNo = objSrcNDE012.TechniqueSheetNo;
                objDesNDE012.CouplantUsed = objSrcNDE012.CouplantUsed;
                objDesNDE012.ReferenceReflectorSize = objSrcNDE012.ReferenceReflectorSize;
                objDesNDE012.ReferenceReflectorDistance = objSrcNDE012.ReferenceReflectorDistance;
                objDesNDE012.CalibrationBlockID = objSrcNDE012.CalibrationBlockID;
                objDesNDE012.CalibrationBlockIDThickness = objSrcNDE012.CalibrationBlockIDThickness;
                objDesNDE012.SimulationBlockID = objSrcNDE012.SimulationBlockID;
                objDesNDE012.SimulationBlockIDwiththickness = objSrcNDE012.SimulationBlockIDwiththickness;
                objDesNDE012.ScanningDetail = objSrcNDE012.ScanningDetail;
                objDesNDE012.ScannerMake = objSrcNDE012.ScannerMake;
                objDesNDE012.EncoderType = objSrcNDE012.EncoderType;
                objDesNDE012.ScannerType = objSrcNDE012.ScannerType;
                objDesNDE012.CableType = objSrcNDE012.CableType;
                objDesNDE012.CableLength = objSrcNDE012.CableLength;
                objDesNDE012.ScanPlan = objSrcNDE012.ScanPlan;
                objDesNDE012.PlannerRemarks = objSrcNDE012.PlannerRemarks;
                objDesNDE012.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                #region old code
                //if (IsEdited)
                //{
                //    objDesNDE012.EditedBy = objClsLoginInfo.UserName;
                //    objDesNDE012.EditedOn = DateTime.Now;
                //    List<NDE013> lstDesNDE013 = db.NDE013.Where(x => x.HeaderId == objDesNDE012.HeaderId).ToList();

                //    if (lstDesNDE013 != null && lstDesNDE013.Count > 0)
                //    {
                //        db.NDE013.RemoveRange(lstDesNDE013);
                //    }
                //    db.SaveChanges();
                //    List<NDE013> lstSrcNDE013 = db.NDE013.Where(x => x.HeaderId == objSrcNDE012.HeaderId).ToList();
                //    db.NDE013.AddRange(
                //                        lstSrcNDE013.Select(x =>
                //                        new NDE013
                //                        {
                //                            HeaderId = objDesNDE012.HeaderId,
                //                            Project = objDesNDE012.Project,
                //                            BU = objDesNDE012.BU,
                //                            Location = objDesNDE012.Location,
                //                            RevNo = Convert.ToInt32(objDesNDE012.RevNo),
                //                            ScanNo = x.ScanNo,
                //                            ScanType = x.ScanType,
                //                            SearchUnit = x.SearchUnit,
                //                            PCS = x.PCS,
                //                            CreatedBy = objClsLoginInfo.UserName,
                //                            CreatedOn = DateTime.Now,
                //                        })
                //                      );
                //    db.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderUpdate.ToString();
                //    objResponseMsg.HeaderId = objDesNDE012.HeaderId;
                //}
                //else
                //{
                #endregion

                objDesNDE012.CreatedBy = objClsLoginInfo.UserName;
                objDesNDE012.CreatedOn = DateTime.Now;
                db.NDE012.Add(objDesNDE012);
                db.SaveChanges();
                List<NDE013> lstSrcNDE013 = db.NDE013.Where(x => x.HeaderId == objSrcNDE012.HeaderId).ToList();
                if (lstSrcNDE013 != null && lstSrcNDE013.Count > 0)
                {
                    db.NDE013.AddRange(
                                    lstSrcNDE013.Select(x =>
                                    new NDE013
                                    {
                                        HeaderId = objDesNDE012.HeaderId,
                                        Project = objDesNDE012.Project,
                                        BU = objDesNDE012.BU,
                                        Location = objDesNDE012.Location,
                                        RevNo = Convert.ToInt32(objDesNDE012.RevNo),
                                        ScanNo = x.ScanNo,
                                        ScanType = x.ScanType,
                                        SearchUnit = x.SearchUnit,
                                        PCS = x.PCS,
                                        OFFSET = x.OFFSET,
                                        ScanningSurface = x.ScanningSurface,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                    })
                                  );
                    //}
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objDesNDE012.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE012.UTTechNo;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.Copy.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE012.Project + " Project.";

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region History
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetTOFDUTHistoryDetails(int Id) //partial for CTQ History Lines Details
        {
            NDE012_Log objNDE010Log = new NDE012_Log();
            objNDE010Log = db.NDE012_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_TOFDUTHistoryPartial", objNDE010Log);
        }
        [SessionExpireFilter]
        public ActionResult ViewLogDetails(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            NDE012_Log objLogNDE012 = new NDE012_Log();
            if (Id > 0)
            {
                objLogNDE012 = db.NDE012_Log.Where(i => i.Id == Id).FirstOrDefault();
                ViewBag.QualityProject = db.QMS010.Where(i => i.QualityProject == objLogNDE012.QualityProject).Select(i => i.QualityProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.CouplantUsed = objNDEModels.GetCategory("Couplant", objLogNDE012.CouplantUsed, objLogNDE012.BU, objLogNDE012.Location, true).CategoryDescription;
                ViewBag.CableType = objNDEModels.GetCategory("Cable Type", objLogNDE012.CableType, objLogNDE012.BU, objLogNDE012.Location, true).CategoryDescription;
                ViewBag.CableLength = objNDEModels.GetCategory("Cable Length", objLogNDE012.CableLength, objLogNDE012.BU, objLogNDE012.Location, true).CategoryDescription;
                ViewBag.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objLogNDE012.ScanningDetail, objLogNDE012.BU, objLogNDE012.Location, true).CategoryDescription;
                ViewBag.ReferenceReflectorSize = objNDEModels.GetCategory("Reference Reflector Size", objLogNDE012.ReferenceReflectorSize, objLogNDE012.BU, objLogNDE012.Location, true).CategoryDescription;
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objLogNDE012.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                //List<string> lstEncoderType = clsImplementationEnum.getNDEEncoderType().ToList();
                //ViewBag.EncoderType = lstEncoderType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                objLogNDE012.Project = db.COM001.Where(i => i.t_cprj == objLogNDE012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objLogNDE012.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogNDE012.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objLogNDE012.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogNDE012.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //List<string> lstScannerType = clsImplementationEnum.getNDEScannerType().ToList();
                //ViewBag.ScannerType = lstScannerType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            }
            return View(objLogNDE012);
        }

        [HttpPost]
        public JsonResult LoadTOFDUTHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                strWhere += "1=1 and HeaderId=" + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                "%' or lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde12.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or UTTechNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or UTProcedureNo like '%" + param.sSearch +
                                "%' or CouplantUsed like '%" + param.sSearch +
                                "%' or ReferenceReflectorSize like '%" + param.sSearch +
                                "%' or PlannerRemarks like '%" + param.sSearch +
                                "%' or ReturnRemarks like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch +
                                "%' or TechniqueSheetNo like '%" + param.sSearch +
                                "%' or (nde12.CreatedBy+'-'+c32.t_namb) like '%" + param.sSearch + "%')";
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strSortOrder = string.Empty;


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_NDE_TOFD_UT_TECH_HISTORY_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.TechniqueSheetNo),
                            Convert.ToString(uc.UTTechNo),
                            Convert.ToString("R" + uc.RevNo),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.UTProcedureNo),
                            Convert.ToString(uc.ReturnRemarks),
                            "<center class='clsDisplayInlineEle'><a title='View' href='"+WebsiteURL+"/NDE/MaintainTOFDUTTech/ViewLogDetails?ID="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainTOFDUTTech/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTOFDUTLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                strWhere += "1=1 and RefId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( RevNo like '%" + param.sSearch +
                                "%' or nde017.SearchUnit like '%" + param.sSearch +
                               "%' or ScanNo like '%" + param.sSearch +
                               "%' or ScanType like '%" + param.sSearch +
                               "%' or PCS like '%" + param.sSearch +
                               "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_NDE_TOFD_UT_LINE_HISTORY_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.ScanNo),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ScanType),
                                Convert.ToString(uc.PCS),
                                Convert.ToString(uc.OFFSET),
                                Convert.ToString(uc.ScanningSurface),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region timeline
        public ActionResult ShowTimeline(int HeaderId, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE012_Log objNDE012 = db.NDE012_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "SCANUTTECH";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE012.ApprovedBy);
                model.ApprovedOn = objNDE012.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE012.SubmittedBy);
                model.SubmittedOn = objNDE012.SubmittedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE012.ReturnedBy);
                model.ReturnedOn = objNDE012.ReturnedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE012.CreatedBy);
                model.CreatedOn = objNDE012.CreatedOn;
            }
            else
            {
                NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "SCANUTTECH";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE012.ApprovedBy);
                model.ApprovedOn = objNDE012.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE012.SubmittedBy);
                model.SubmittedOn = objNDE012.SubmittedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE012.ReturnedBy);
                model.ReturnedOn = objNDE012.ReturnedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE012.CreatedBy);
                model.CreatedOn = objNDE012.CreatedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Export to excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                //main header
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {

                    var lst = db.SP_NDE_TOFD_UT_TECH_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      QualityProject = uc.QualityProject,
                                      Project = uc.Project,
                                      BU = uc.BU,
                                      Location = uc.Location,
                                      UTTechNo = Convert.ToString(uc.TechniqueSheetNo),
                                      DocumentNo = uc.UTTechNo,
                                      RevNo = "R" + uc.RevNo,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn != null ? (Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy") : ""),
                                      Status = Convert.ToString(uc.Status),
                                      UTProcedureNo = Convert.ToString(uc.UTProcedureNo),
                                      ReturnRemarks = Convert.ToString(uc.ReturnRemarks),
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //history header
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {

                    var lst = db.SP_NDE_TOFD_UT_TECH_HISTORY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      QualityProject = uc.QualityProject,
                                      Project = uc.Project,
                                      BU = uc.BU,
                                      Location = uc.Location,
                                      UTTechNo = Convert.ToString(uc.TechniqueSheetNo),
                                      DocumentNo = uc.UTTechNo,
                                      RevNo = "R" + uc.RevNo,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn != null ? (Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy") : ""),
                                      Status = Convert.ToString(uc.Status),
                                      UTProcedureNo = Convert.ToString(uc.UTProcedureNo),
                                      ReturnRemarks = Convert.ToString(uc.ReturnRemarks),
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //main lines
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {

                    var lst = db.SP_NDE_TOFD_UT_LINE_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      // ROW_NO = Convert.ToString(uc.ROW_NO),
                                      ScanNo = Convert.ToString(uc.ScanNo),
                                      SearchUnit = Convert.ToString(uc.SearchUnit),
                                      ScanType = Convert.ToString(uc.ScanType),
                                      PCS = Convert.ToString(uc.PCS),
                                      //   RevNo = Convert.ToString("R" + uc.RevNo),
                                      OFFSET = Convert.ToString(uc.OFFSET),
                                      ScanningSurface = Convert.ToString(uc.ScanningSurface),
                }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
            }
                //history lines
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
            {
                var lst = db.SP_NDE_TOFD_UT_LINE_HISTORY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from uc in lst
                              select new
                              {
                                  ScanNo = Convert.ToString(uc.ScanNo),
                                  SearchUnit = Convert.ToString(uc.SearchUnit),
                                  ScanType = Convert.ToString(uc.ScanType),
                                  PCS = Convert.ToString(uc.PCS),
                                  //   RevNo = Convert.ToString("R" + uc.RevNo),
                                  OFFSET = Convert.ToString(uc.OFFSET),
                                  ScanningSurface = Convert.ToString(uc.ScanningSurface),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
            }
            objResponseMsg.Key = true;
            objResponseMsg.Value = strFileName;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
    }
}
        #endregion
    }
}