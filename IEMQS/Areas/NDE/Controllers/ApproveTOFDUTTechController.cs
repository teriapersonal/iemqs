﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveTOFDUTTechController : clsBase
    {
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        // GET: NDE/ApproveTOFDUTTech
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetApproveTOFDUTPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApproveTOFDUTGridDataPartial");
        }


        [HttpPost]
        public JsonResult LoadTOFDUTHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                "%' or lcom2.t_desc like '%" + param.sSearch +
                                    "%' or QualityProject like '%" + param.sSearch +
                                "%' or (nde12.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or UTTechNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or UTProcedureNo like '%" + param.sSearch +
                                "%' or CouplantUsed like '%" + param.sSearch +
                                "%' or ReferenceReflectorSize like '%" + param.sSearch +
                                "%' or PlannerRemarks like '%" + param.sSearch +
                                "%' or ReturnRemarks like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch +
                                "%' or TechniqueSheetNo like '%" + param.sSearch +
                                "%' or (nde12.CreatedBy+'-'+c32.t_namb) like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde12.BU", "nde12.Location");

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_NDE_TOFD_UT_TECH_GETDETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),                           
                            Convert.ToString(uc.TechniqueSheetNo),
                             Convert.ToString(uc.UTTechNo),
                            Convert.ToString("R" + uc.RevNo),
                              Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                          
                            Convert.ToString(uc.UTProcedureNo),
                           "<center style='white-space:nowrap;' class='clsDisplayInlineEle'><a title='View' href='"+WebsiteURL +"/NDE/ApproveTOFDUTTech/ViewDocument?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainTOFDUTTech/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a><a title='Print' href='javascript:void(0)' onclick='PrintReport("+Convert.ToInt32(uc.HeaderId)+",\""+uc.Status+"\");'><i style='margin-left:10px;' " +
                                "class='fa fa-print'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public JsonResult LoadTOFDUTLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                strWhere += "1=1 and HeaderId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( RevNo like '%" + param.sSearch +
                               "%' or nde017.SearchUnit like '%" + param.sSearch +
                               "%' or ScanNo like '%" + param.sSearch +
                               "%' or ScanType like '%" + param.sSearch +
                               "%' or PCS like '%" + param.sSearch +
                               "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_NDE_TOFD_UT_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.ScanNo),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ScanType),
                                Convert.ToString(uc.PCS),
                                Convert.ToString(uc.OFFSET),
                                Convert.ToString(uc.ScanningSurface),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [SessionExpireFilter]
        public ActionResult ViewDocument(int headerID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE012");
            NDE012 objNDE012 = new NDE012();
            NDEModels objNDEModels = new NDEModels();
            if (headerID > 0)
            {
                objNDE012 = db.NDE012.Where(i => i.HeaderId == headerID).FirstOrDefault();
                ViewBag.QualityProject = db.QMS010.Where(i => i.QualityProject == objNDE012.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                ViewBag.CouplantUsed = objNDEModels.GetCategory("Couplant", objNDE012.CouplantUsed, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.CableType = objNDEModels.GetCategory("Cable Type", objNDE012.CableType, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.CableLength = objNDEModels.GetCategory("Cable Length", objNDE012.CableLength, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objNDE012.ScanningDetail, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.ReferenceReflectorSize = objNDEModels.GetCategory("Reference Reflector Size", objNDE012.ReferenceReflectorSize, objNDE012.BU, objNDE012.Location, true).CategoryDescription;
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE012.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                objNDE012.Project = db.COM001.Where(i => i.t_cprj == objNDE012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                string strproject = objNDE012.Project.Split('-')[0];
                string strBU = db.COM001.Where(i => i.t_cprj == strproject).FirstOrDefault().t_entu;
                objNDE012.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == strBU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();


                objNDE012.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objNDE012.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                List<string> lstScannerType = clsImplementationEnum.getNDEScannerType().ToList();
                ViewBag.ScannerType = lstScannerType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                ViewBag.Action = "edit";
            }
            return View(objNDE012);
        }

        [HttpPost]
        public ActionResult ReturnHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE012 objNDE012 = new NDE012();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (headerId > 0)
                {
                    objNDE012 = db.NDE012.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objNDE012.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                    objNDE012.ReturnRemarks = returnRemark;
                    objNDE012.ReturnedBy = objClsLoginInfo.UserName;
                    objNDE012.ReturnedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE012.Project, objNDE012.BU, objNDE012.Location, "Technique No: " + objNDE012.UTTechNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainTOFDUTTech/AddHeader?HeaderID=" + objNDE012.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveHeader(int headerId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                db.SP_NDE_TOFD_UT_APPROVE(headerId, Remarks, objClsLoginInfo.UserName);

                #region Send Notification
                NDE012 objNDE012 = db.NDE012.Where(c => c.HeaderId == headerId).FirstOrDefault();
                if (objNDE012 != null)
                {
                    objNDE012.ReturnRemarks = Remarks;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE012.Project, objNDE012.BU, objNDE012.Location, "Technique No: " + objNDE012.UTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE012 objNDE012 = db.NDE012.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE012 != null)
            {
                if (objNDE012.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_NDE_TOFD_UT_APPROVE(HeaderId, null, objClsLoginInfo.UserName);

                    #region Send Notification
                    NDE012 objNDE012 = db.NDE012.Where(c => c.HeaderId == HeaderId).FirstOrDefault();
                    if (objNDE012 != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE012.Project, objNDE012.BU, objNDE012.Location, "Technique No: " + objNDE012.UTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}