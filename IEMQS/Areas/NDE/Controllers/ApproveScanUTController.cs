﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveScanUTController : clsBase
    {
        // GET: NDE/ApproveScanUT
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetApproveScanUTPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApproveUTTechniqueGridDataPartial");
        }


        [HttpPost]
        public JsonResult LoadHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "where 1=1 and status in('" + clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "where 1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                "%' or lcom2.t_desc like '%" + param.sSearch +
                                    "%' or QualityProject like '%" + param.sSearch +
                                "%' or (nde10.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or UTTechNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or DocNo like '%" + param.sSearch +
                                "%' or UTProcedureNo like '%" + param.sSearch +
                                "%' or RecordableIndication like '%" + param.sSearch +
                                "%' or ScanTechnique like '%" + param.sSearch +
                                "%' or DGSDiagram like '%" + param.sSearch +
                                "%' or BasicCalibrationBlock like '%" + param.sSearch +
                                "%' or SimulationBlock1 like '%" + param.sSearch +
                                "%' or SimulationBlock2 like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch +
                                "%' or TechniqueSheetNo like '%" + param.sSearch +
                                "%' or (nde10.CreatedBy+'-'+c32.t_namb) like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde10.BU", "nde10.Location");
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCAN_UT_TECH_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.TechniqueSheetNo),
                            Convert.ToString(uc.UTTechNo),
                            Convert.ToString("R" + uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),                          
                            Convert.ToString(uc.UTProcedureNo),
                            "<center style='white-space: nowrap;' class='clsDisplayInlineEle' ><a title='View' href='"+WebsiteURL +"/NDE/ApproveScanUT/ViewAScanUTTechnique?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainScanUTTech/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a><a title='Print' href='javascript:void(0)' onclick='PrintReport("+Convert.ToInt32(uc.HeaderId)+",\""+uc.Status+"\");'><i style='margin-left:10px;' " +
                                "class='fa fa-print'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadScanUTLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "where ";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( RevNo like '%" + param.sSearch +
                         "%' or nde017.SearchUnit like '%" + param.sSearch +
                                "%' or ReferenceReflector like '%" + param.sSearch +
                                "%' or CableType like '%" + param.sSearch +
                                "%' or CableLength like '%" + param.sSearch +
                                "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCAN_UT_LINES_DETAILS
                                (
                                 StartIndex,
                                 EndIndex,
                                 strSortOrder,
                                 strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ReferenceReflector),
                                Convert.ToString(uc.CableType),
                                Convert.ToString(uc.CableLength),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = "",
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [SessionExpireFilter]
        public ActionResult ViewAScanUTTechnique(int headerID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE010");
            NDEModels objNDEModels = new NDEModels();
            NDE010 objNDE010 = new NDE010();
            if (headerID > 0)
            {
                objNDE010 = db.NDE010.Where(i => i.HeaderId == headerID).FirstOrDefault();
                ViewBag.QMS = new SelectList(db.QMS010.ToList(), "QualityProject", "QualityProject", objNDE010.QualityProject);

                string BU = db.COM001.Where(i => i.t_cprj == objNDE010.Project).FirstOrDefault().t_entu;

                ViewBag.ScanTechnique = objNDEModels.GetCategory("Scan Technique", objNDE010.ScanTechnique, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.CouplantUsed = objNDEModels.GetCategory("Couplant", objNDE010.CouplantUsed, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.SimulationBlock1 = objNDEModels.GetCategory("Simulation Block", objNDE010.SimulationBlock1, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.ScanningSensitivity = objNDEModels.GetCategory("Scanning Sensitivity", objNDE010.ScanningSensitivity, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.BasicCalibrationBlock = objNDEModels.GetCategory("Basic Calibration Block", objNDE010.BasicCalibrationBlock, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.RecordableIndication = objNDEModels.GetCategory("Recordable Indication At Reference Gain", objNDE010.RecordableIndication, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE010.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                //List<GLB002> lstScanGLB002 = Manager.GetSubCatagories("Scan Technique", BU,objNDE010.Location).ToList();
                //ViewBag.ScanTech = lstScanGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();

                //List<GLB002> lstCouplantGLB002 = Manager.GetSubCatagories("Couplant", BU, objNDE010.Location).ToList();
                //ViewBag.CouplantUsed = lstCouplantGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();

                //List<GLB002> lstSBGLB002 = Manager.GetSubCatagories("Simulation Block", BU, objNDE010.Location).ToList();
                //ViewBag.SimulationBlock1 = lstSBGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();

                //List<GLB002> lstSSGLB002 = Manager.GetSubCatagories("Scanning Sensitivity", BU, objNDE010.Location).ToList();
                //ViewBag.ScanningSensitivity = lstSSGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();

                //List<string> lstyesno = clsImplementationEnum.getDGSyesno().ToList();
                //ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                //List<string> lstBasicCalib = clsImplementationEnum.getBasicCalibrationBlock().ToList();
                //ViewBag.lstBasicCalib = lstBasicCalib.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                //ViewBag.ManufacturingCode = db.TMP001.Where(x => x.QualityProject == objNDE010.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                objNDE010.Project = db.COM001.Where(i => i.t_cprj == objNDE010.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                string strproject = objNDE010.Project.Split('-')[0];
                string strBU = db.COM001.Where(i => i.t_cprj == strproject).FirstOrDefault().t_entu;
                objNDE010.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == strBU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                objNDE010.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objNDE010.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            return View(objNDE010);
        }

        [HttpPost]
        public ActionResult ReturnHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE010 objNDE010 = new NDE010();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (headerId > 0)
                {
                    objNDE010 = db.NDE010.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    objNDE010.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                    objNDE010.ReturnRemarks = returnRemark;
                    objNDE010.ReturnedBy = objClsLoginInfo.UserName;
                    objNDE010.ReturnedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE010.Project, objNDE010.BU, objNDE010.Location, "Technique No: " + objNDE010.UTTechNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainScanUTTech/AddHeader?HeaderID=" + objNDE010.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveHeader(int headerId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                db.SP_ASCAN_UT_APPROVE(headerId, Remarks, objClsLoginInfo.UserName);

                #region Send Notification
                NDE010 objNDE010 = db.NDE010.Where(c => c.HeaderId == headerId).FirstOrDefault();
                if (objNDE010 != null)
                {
                    objNDE010.ReturnRemarks = Remarks;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE010.Project, objNDE010.BU, objNDE010.Location, "Technique No: " + objNDE010.UTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE010 != null)
            {
                if (objNDE010.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_ASCAN_UT_APPROVE(HeaderId, null, objClsLoginInfo.UserName);

                    #region Send Notification
                    NDE010 objNDE010 = db.NDE010.Where(c => c.HeaderId == HeaderId).FirstOrDefault();
                    if (objNDE010 != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE010.Project, objNDE010.BU, objNDE010.Location, "Technique No: " + objNDE010.UTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
}