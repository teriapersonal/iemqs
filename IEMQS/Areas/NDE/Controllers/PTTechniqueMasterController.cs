﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using System.Data;
using System.Globalization;

namespace IEMQS.Areas.NDE.Controllers
{

    public class PTTechniqueMasterController : clsBase
    {
        // GET: NDE/PTTechniqueMaster
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetPTTechniqueGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetPTTechniqueGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde7.BU", "nde7.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde7.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(NDE7.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(NDE7.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or NDE7.QualityProject like '%" + param.sSearch + "%' or (NDE7.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or PTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%'" +
                                            " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%' )";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_GETPTTECHNIQUEHEADER(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        
                        //Convert.ToString(a.HeaderId),
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.PTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center style='display:inline;'><a href='"+WebsiteURL+"/NDE/PTTechniqueMaster/AddPTTechnique?headerId=" + a.HeaderId + "' title='View Record' style='cursor:pointer;' name='btnAction' id='btnEdit'><i style='' class='fa fa-eye'></i></a>"+HTMLActionString(a.HeaderId,"","btnDelete","Delete Record","fa fa-trash-o","DeleteHeader(" + a.HeaderId + ");",!(a.RevNo ==0 && a.Status.Equals(clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue())))+"<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/PTTechniqueMaster/ShowTimeline?HeaderID="+Convert.ToInt32(a.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"+HTMLActionString(a.HeaderId,"","btnHistory","History","fa fa-history","History(" + a.HeaderId + ");",!(a.RevNo > 0 || a.Status.Equals(clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())))+"</center>",
                //Convert.ToString(a.HeaderId)
                //a.EditedBy
                //a.Status,
            };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #region PT Technique Header
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult AddPTTechnique(int? headerId)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE007");
            NDE007 objNDE007 = new NDE007();
            try
            {
                string user = objClsLoginInfo.UserName;

                var lstQMSProject = (from a in db.QMS010
                                     select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();

                var location = db.COM003.Where(i => i.t_psno.Equals(objClsLoginInfo.UserName) && i.t_actv == 1).Select(i => i.t_loca).FirstOrDefault();
                var Location = (from a in db.COM002
                                where a.t_dtyp == 1 && a.t_dimx.Equals(location, StringComparison.OrdinalIgnoreCase)
                                select new { DescLocation = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                NDEModels objNDEModels = new NDEModels();
                ViewBag.PTConsume = "";
                if (headerId > 0)
                {
                    objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    ViewBag.PTConsume = objNDE007.PTConsuNo.ToString();
                    var QualityProj = objNDEModels.GetQMSProject(objNDE007.QualityProject);
                    ViewBag.QMSProject = QualityProj.QualityProject;// +"-"+QualityProj.ManufacturingCode;//new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc", objNDE007.QualityProject);
                    ViewBag.Location = Location.DescLocation;
                    ViewBag.PTMethod = objNDEModels.GetCategory(objNDE007.Method).CategoryDescription;
                    ViewBag.CleaningSolvent = objNDEModels.GetCategory("Cleaning Solvent", objNDE007.CleaningSolvent, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.ApplicationMethod = objNDEModels.GetCategory("PT Application Method", objNDE007.ApplicationMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.RemovalMethod = objNDEModels.GetCategory("Removal Method", objNDE007.RemovalMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.LightIntensity = objNDEModels.GetCategory("Light Intensity", objNDE007.LightIntensity, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.LightSource = objNDEModels.GetCategory("Light Source", objNDE007.LightSource, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.MethodOfExamination = objNDEModels.GetCategory("Method Of Examination", objNDE007.MethodOfExamination, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.PenetrantProcessType = objNDEModels.GetCategory("Penetrant Process Type", objNDE007.PenetrantProcessType, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    ViewBag.DevAppMethod = objNDEModels.GetCategory("Developer Application Method", objNDE007.DevAppMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                    //  ViewBag.VisibleUVLightIntensity = objNDEModels.GetCategory("Visible/UV Light Intensity", objNDE007.VisibleUVLightIntensity, objNDE007.BU, objNDE007.Location, true).CategoryDescription;

                }
                else
                {
                    objNDE007.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objNDE007.RevNo = 0;
                    ViewBag.Location = Location.DescLocation;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }

            return View(objNDE007);
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            NDE007 objNDE007 = new NDE007();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    if (headerId > 0)
                    {
                        if (!IsapplicableForSave(headerId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        #region PT Technique Header Update

                        objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();

                        if (string.Equals(objNDE007.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objNDE007.RevNo = objNDE007.RevNo + 1;
                            objNDE007.ReturnRemarks = null;
                            objNDE007.ReturnedBy = null;
                            objNDE007.ReturnedOn = null;
                            objNDE007.SubmittedBy = null;
                            objNDE007.SubmittedOn = null;
                            objNDE007.ApprovedOn = null;
                            objNDE007.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        }
                        objNDE007.TechniqueSheetNo = fc["TechniqueSheetNo"];
                        objNDE007.PTTechNo = fc["PTTechNo"];
                        objNDE007.DocNo = !string.IsNullOrEmpty(fc["DocNo"]) ? Convert.ToInt32(fc["DocNo"]) : 0;
                        objNDE007.Method = fc["Method"];// Convert.ToInt32(fc["Method"]);
                        objNDE007.DryingTime = fc["DryingTime"];
                        objNDE007.SurfaceTemperature = fc["SurfaceTemperature"];
                        objNDE007.ProcedureNo = fc["ProcedureNo"];
                        objNDE007.CleaningSolvent = fc["CleaningSolvent"];//Convert.ToInt32(fc["ddlCleaningSolvent"]);
                        objNDE007.ApplicationMethod = fc["ApplicationMethod"];//Convert.ToInt32(fc["ddlApplicationMethod"]);
                        objNDE007.DwellTime = fc["DwellTime"];
                        objNDE007.RemovalMethod = fc["RemovalMethod"];// Convert.ToInt32(fc["ddlRemovalMethod"]);
                        objNDE007.DevAppMethod = fc["DevAppMethod"];
                        objNDE007.DevDwellTime = fc["DevDwellTime"];
                        objNDE007.LightIntensity = fc["LightIntensity"];//Convert.ToInt32(fc["ddlLightIntensity"]);
                        objNDE007.LightSource = fc["LightSource"];//Convert.ToInt32(fc["ddlLightSource"]);
                        objNDE007.OtherLightSource = fc["OtherLightSource"];
                        objNDE007.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE007.EditedBy = objClsLoginInfo.UserName;
                        objNDE007.EditedOn = DateTime.Now;
                        objNDE007.MethodOfExamination = fc["MethodOfExamination"].ToString();
                        objNDE007.PenetrantProcessType = fc["PenetrantProcessType"].ToString();
                        //  objNDE007.VisibleUVLightIntensity = fc["VisibleUVLightIntensity"].ToString();
                        objNDE007.PostCleaning = fc["PostCleaning"].ToString();
                       
                        //objNDE007.PTConsuNo= fc["PTConsuNo"].ToString();
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = Convert.ToString(objNDE007.HeaderId);
                        objResponseMsg.RevNo = Convert.ToString(objNDE007.RevNo);
                        objResponseMsg.HeaderStatus = objNDE007.Status;
                        objResponseMsg.Remarks = objNDE007.ReturnRemarks;
                        #endregion
                    }
                    else
                    {
                        #region PT Technique New Header 
                        objNDE007.TechniqueSheetNo = fc["TechniqueSheetNo"];
                        objNDE007.QualityProject = fc["QualityProject"];
                        objNDE007.Project = fc["Project"].Split('-')[0];
                        objNDE007.BU = fc["BU"].Split('-')[0];
                        objNDE007.Location = fc["Location"].Split('-')[0];
                        objNDE007.RevNo = 0;
                        objNDE007.PTTechNo = fc["PTTechNo"];
                        objNDE007.DocNo = !string.IsNullOrEmpty(fc["DocNo"]) ? Convert.ToInt32(fc["DocNo"]) : 0;
                        objNDE007.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objNDE007.Method = fc["Method"];// Convert.ToInt32(fc["Method"]);
                        objNDE007.DryingTime = fc["DryingTime"];
                        objNDE007.SurfaceTemperature = fc["SurfaceTemperature"];
                        objNDE007.ProcedureNo = fc["ProcedureNo"];
                        objNDE007.CleaningSolvent = fc["CleaningSolvent"];//Convert.ToInt32(fc["ddlCleaningSolvent"]);
                        objNDE007.ApplicationMethod = fc["ApplicationMethod"];//Convert.ToInt32(fc["ddlApplicationMethod"]);
                        objNDE007.DwellTime = fc["DwellTime"];
                        objNDE007.RemovalMethod = fc["RemovalMethod"];// Convert.ToInt32(fc["ddlRemovalMethod"]);
                        objNDE007.DevAppMethod = fc["DevAppMethod"];
                        objNDE007.DevDwellTime = fc["DevDwellTime"];
                        objNDE007.LightIntensity = fc["LightIntensity"];//Convert.ToInt32(fc["ddlLightIntensity"]);
                        objNDE007.LightSource = fc["LightSource"];//Convert.ToInt32(fc["ddlLightSource"]);
                        objNDE007.OtherLightSource = fc["OtherLightSource"];
                        objNDE007.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE007.MethodOfExamination = fc["MethodOfExamination"].ToString();
                        objNDE007.PenetrantProcessType = fc["PenetrantProcessType"].ToString();
                        //  objNDE007.VisibleUVLightIntensity = fc["VisibleUVLightIntensity"].ToString();
                        objNDE007.CreatedBy = objClsLoginInfo.UserName;
                        objNDE007.CreatedOn = DateTime.Now;
                        objNDE007.PostCleaning = fc["PostCleaning"].ToString();
                        objNDE007.PTConsuNo = fc["txtPTConsuNo"].ToString();
                        db.NDE007.Add(objNDE007);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = Convert.ToString(objNDE007.HeaderId);
                        objResponseMsg.RevNo = Convert.ToString(objNDE007.RevNo);
                        objResponseMsg.HeaderStatus = objNDE007.Status;
                        objResponseMsg.Remarks = objNDE007.ReturnRemarks;
                        #endregion
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }

            return Json(objResponseMsg);
        }
        #endregion

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE007 objNDE007 = new NDE007();
            try
            {
                if (headerId > 0)
                {
                    objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    db.NDE007.Remove(objNDE007);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Deleted SuccessFully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        #region PT Technique Send For Approval
        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE007 objNDE007 = new NDE007();
            try
            {
                if (headerId > 0)
                {
                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE007 != null)
                    {
                        objNDE007.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                        objNDE007.SubmittedBy = objClsLoginInfo.UserName;
                        objNDE007.SubmittedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE007.Project, objNDE007.BU, objNDE007.Location, "Technique No: " + objNDE007.PTTechNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApprovePTTechnique/ViewPTTechnique?headerId=" + objNDE007.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE007 objNDE007 = db.NDE007.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE007 != null)
            {
                if (objNDE007.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE007.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }


        private bool IsapplicableForSave(int headerId)
        {
            NDE007 objNDE007 = db.NDE007.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE007 != null)
            {
                if (objNDE007.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }
        #endregion

        #region PT Technique Copy
        [HttpPost]
        public ActionResult CopyPTTechniquePartial()
        {
            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();
            ViewBag.QMSProject = new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyNDETechniquePartial.cshtml");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destQMSProject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            NDE007 objNDE007src = new NDE007();
            NDE007 objNDE007Dest = new NDE007();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                if (headerId > 0)
                {
                    objNDE007src = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
                }

                #region OldCode
                //if (!string.IsNullOrEmpty(destQMSProject))
                //{
                //    objNDE007Dest = db.NDE007.Where(i => i.QualityProject.Equals(destQMSProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                //    if (objNDE007Dest != null)
                //    {
                //        if (string.Equals(objNDE007Dest.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue()))
                //        {
                //            objResponseMsg = InsertOrUpdatePTTechnique(objNDE007src, objNDE007Dest, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "You cannot copy to " + destQMSProject + " as it is in " + objNDE007Dest.Status + " Staus";
                //        }
                //    }
                //    else
                //    {
                //        string project = new NDEModels().GetQMSProject(destQMSProject).Project;
                //        objNDE007Dest = new NDE007();
                //        objNDE007Dest.QualityProject = destQMSProject;
                //        objNDE007Dest.Project = project;
                //        objNDE007Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                //        objNDE007Dest.Location = currentLoc;

                //        var maxPTTechno = MaxTechniqueNo(objNDE007src.Method, currentLoc, destQMSProject);

                //        objNDE007Dest.PTTechNo = "PT/" + maxPTTechno.Method + "/" + maxPTTechno.DocumentNo.ToString("D3");
                //        objNDE007Dest.DocNo = maxPTTechno.DocumentNo;
                //        objResponseMsg = InsertOrUpdatePTTechnique(objNDE007src, objNDE007Dest, false);
                //    }
                //}
                #endregion

                string project = new NDEModels().GetQMSProject(destQMSProject).Project;
                var BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;

                var Category = new NDEModels().GetCategoryId("PT Method");
                if (Manager.isMethodExistForBULocation(BU, currentLoc, objNDE007src.Method, Category))
                {
                    objNDE007Dest = new NDE007();
                    objNDE007Dest.QualityProject = destQMSProject;
                    objNDE007Dest.Project = project;
                    objNDE007Dest.BU = BU;
                    objNDE007Dest.Location = currentLoc;

                    var maxPTTechno = MaxTechniqueNo(objNDE007src.Method, currentLoc, destQMSProject);

                    objNDE007Dest.PTTechNo = "PT/" + maxPTTechno.Method + "/" + maxPTTechno.DocumentNo.ToString("D3");
                    objNDE007Dest.DocNo = maxPTTechno.DocumentNo;
                    objResponseMsg = InsertOrUpdatePTTechnique(objNDE007src, objNDE007Dest, false);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Technique Is Not Available For Destination Project!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PT Technique History
        [HttpPost]
        public ActionResult GetHistoryPTTechniquePartial(int headerId)
        {
            NDE007 objNDE007 = new NDE007();
            if (headerId > 0)
                objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryPTTechniquePartial", objNDE007);
        }

        public ActionResult LoadPTTechniqueHistory(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde7.BU", "nde7.Location");
                whereCondition += " AND NDE7.HeaderId=" + headerId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (NDE7.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or PTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                    whereCondition += " and (ltrim(rtrim(NDE7.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(NDE7.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or NDE7.QualityProject like '%" + param.sSearch + "%' or (NDE7.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or PTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%' or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%' )";
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_NDE_PTTechnique_GetHistory(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.PTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center><a href=\""+WebsiteURL+"/NDE/PTTechniqueMaster/ViewHistory?id=" + a.Id + "\" target=\"_blank\" title=\"View History\" style=\"cursor:pointer;\" name=\"btnAction\" id=\"btnViewHistory\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/PTTechniqueMaster/ShowTimeline?HeaderID="+Convert.ToInt32(a.Id)+"&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"

                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult ViewHistory(int id)
        {
            NDE007_Log objNDE007 = new NDE007_Log();
            if (id > 0)
            {
                objNDE007 = db.NDE007_Log.Where(i => i.Id == id).FirstOrDefault();
                NDEModels objNDEModels = new NDEModels();
                var QMSProject = new NDEModels().GetQMSProject(objNDE007.QualityProject);
                ViewBag.QMSProject = QMSProject.QualityProject;// + " - " + QMSProject.ManufacturingCode;// db.TMP001.Where(i => i.QMSProject == objNDE007.QMSProject).Select(i => i.QMSProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.MfgCode = QMSProject.ManufacturingCode;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE007.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE007.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE007.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.PTMethod = objNDEModels.GetCategory(objNDE007.Method).CategoryDescription;
                ViewBag.CleaningSolvent = objNDEModels.GetCategory("Cleaning Solvent", objNDE007.CleaningSolvent, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.ApplicationMethod = objNDEModels.GetCategory("PT Application Method", objNDE007.ApplicationMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.RemovalMethod = objNDEModels.GetCategory("Removal Method", objNDE007.RemovalMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.LightIntensity = objNDEModels.GetCategory("Light Intensity", objNDE007.LightIntensity, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.LightSource = objNDEModels.GetCategory("Light Source", objNDE007.LightSource, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                objNDE007.MethodOfExamination = objNDEModels.GetCategory("Method Of Examination", objNDE007.MethodOfExamination, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                objNDE007.PenetrantProcessType = objNDEModels.GetCategory("Penetrant Process Type", objNDE007.PenetrantProcessType, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                objNDE007.DevAppMethod = objNDEModels.GetCategory("Developer Application Method", objNDE007.DevAppMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                //  objNDE007.VisibleUVLightIntensity = objNDEModels.GetCategory("Visible/UV Light Intensity", objNDE007.VisibleUVLightIntensity, objNDE007.BU, objNDE007.Location, true).CategoryDescription;

            }
            return View(objNDE007);
        }
        #endregion

        #region PT Technique Common Functions
        public clsHelper.ResponseMsgWithStatus InsertOrUpdatePTTechnique(NDE007 objNDE007Src, NDE007 objNDE007Dest, bool isUpdate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (isUpdate)
                {
                    objNDE007Dest.TechniqueSheetNo = objNDE007Src.TechniqueSheetNo;
                    objNDE007Dest.ProcedureNo = objNDE007Src.ProcedureNo;
                    objNDE007Dest.DryingTime = objNDE007Src.DryingTime;
                    objNDE007Dest.SurfaceTemperature = objNDE007Src.SurfaceTemperature;
                    objNDE007Dest.CleaningSolvent = objNDE007Src.CleaningSolvent;
                    objNDE007Dest.ApplicationMethod = objNDE007Src.ApplicationMethod;
                    objNDE007Dest.DwellTime = objNDE007Src.DwellTime;
                    objNDE007Dest.RemovalMethod = objNDE007Src.RemovalMethod;
                    objNDE007Dest.DevAppMethod = objNDE007Src.DevAppMethod;
                    objNDE007Dest.DevDwellTime = objNDE007Src.DevDwellTime;
                    objNDE007Dest.LightIntensity = objNDE007Src.LightIntensity;
                    objNDE007Dest.LightSource = objNDE007Src.LightSource;
                    objNDE007Dest.OtherLightSource = objNDE007Src.OtherLightSource;
                    objNDE007Dest.PlannerRemarks = objNDE007Src.PlannerRemarks;
                    objNDE007Dest.MethodOfExamination = objNDE007Src.MethodOfExamination;
                    objNDE007Dest.PenetrantProcessType = objNDE007Src.PenetrantProcessType;
                    objNDE007Dest.VisibleUVLightIntensity = objNDE007Src.VisibleUVLightIntensity;
                    objNDE007Dest.PostCleaning = objNDE007Src.PostCleaning;
                    objNDE007Dest.EditedBy = objClsLoginInfo.UserName;
                    objNDE007Dest.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE007Dest.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objNDE007Dest.PTTechNo;
                    objResponseMsg.Value = "Data Copied Successfully To Document No : " + objResponseMsg.HeaderStatus + " of " + objNDE007Dest.Project + " Project.";
                }
                else
                {
                    objNDE007Dest.TechniqueSheetNo = objNDE007Src.TechniqueSheetNo;
                    objNDE007Dest.ProcedureNo = objNDE007Src.ProcedureNo;
                    objNDE007Dest.DryingTime = objNDE007Src.DryingTime;
                    objNDE007Dest.SurfaceTemperature = objNDE007Src.SurfaceTemperature;
                    objNDE007Dest.CleaningSolvent = objNDE007Src.CleaningSolvent;
                    objNDE007Dest.ApplicationMethod = objNDE007Src.ApplicationMethod;
                    objNDE007Dest.RevNo = 0;
                    objNDE007Dest.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objNDE007Dest.DwellTime = objNDE007Src.DwellTime;
                    objNDE007Dest.RemovalMethod = objNDE007Src.RemovalMethod;
                    objNDE007Dest.DevAppMethod = objNDE007Src.DevAppMethod;
                    objNDE007Dest.DevDwellTime = objNDE007Src.DevDwellTime;
                    objNDE007Dest.LightIntensity = objNDE007Src.LightIntensity;
                    objNDE007Dest.LightSource = objNDE007Src.LightSource;
                    objNDE007Dest.OtherLightSource = objNDE007Src.OtherLightSource;
                    objNDE007Dest.Method = objNDE007Src.Method;
                    objNDE007Dest.PlannerRemarks = objNDE007Src.PlannerRemarks;
                    objNDE007Dest.CreatedBy = objClsLoginInfo.UserName;
                    objNDE007Dest.CreatedOn = DateTime.Now;
                    objNDE007Dest.MethodOfExamination = objNDE007Src.MethodOfExamination;
                    objNDE007Dest.PenetrantProcessType = objNDE007Src.PenetrantProcessType;
                    objNDE007Dest.VisibleUVLightIntensity = objNDE007Src.VisibleUVLightIntensity;
                    objNDE007Dest.PostCleaning = objNDE007Src.PostCleaning;
                    db.NDE007.Add(objNDE007Dest);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE007Dest.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objNDE007Dest.PTTechNo;
                    objResponseMsg.Value = "Data Copied Successfully To Document No : " + objResponseMsg.HeaderStatus + " of " + objNDE007Dest.Project + " Project.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult GetQMSProjectDetail_Old(string QMSProject)
        {
            ModelNDETechnique objModelTechniqueNo = new ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                objModelTechniqueNo = new NDEModels().GetQMSProjectDetail(QMSProject);
            }

            return Json(objModelTechniqueNo);
        }

        [HttpPost]
        public ActionResult GetQMSProjectDetail(string QMSProject, string location)
        {
            ModelCategory objModelCategory = new ModelCategory();

            ModelNDETechnique objModelTechniqueNo = new ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                objModelTechniqueNo = new NDEModels().GetQMSProjectDetail(QMSProject);
            }
            string[] arrCategory = { "pt method", "Cleaning Solvent", "Application Method", "Removal Method", "Light Intensity", "Light Source" };

            //var lstGLB002 = (from glb002 in db.GLB002
            //                 join glb001 in db.GLB001 on glb002.Category equals glb001.Id
            //                 //where arrCategory.Contains(glb001.Category)
            //                 group glb001 by glb001.Id into tmp
            //                 select new CategoryData {Id=tmp. }).ToList();

            string strBu = string.Empty;
            objModelCategory.project = objModelTechniqueNo.project;
            objModelCategory.BU = objModelTechniqueNo.BU;
            strBu = objModelTechniqueNo.BU.Split('-')[0];
            objModelCategory.ManufacturingCode = objModelTechniqueNo.ManufacturingCode;
            var lstPTMethod = Manager.GetSubCatagories("pt method", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            var lstCleaningSolvent = Manager.GetSubCatagories("Cleaning Solvent", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstApplicationMethod = Manager.GetSubCatagories("PT Application Method", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstRemovalMethod = Manager.GetSubCatagories("Removal Method", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstLightIntensity = Manager.GetSubCatagories("Light Intensity", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstLightSource = Manager.GetSubCatagories("Light Source", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstMethodOfExamination = Manager.GetSubCatagories("Method Of Examination", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            //   var lstVisibleUVLightIntensity = Manager.GetSubCatagories("Visible/UV Light Intensity", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstPenetrantProcessType = Manager.GetSubCatagories("Penetrant Process Type", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstDeveloperApplicationMethod = Manager.GetSubCatagories("Developer Application Method", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();


            objModelCategory.PTMethod = lstPTMethod.Any() ? lstPTMethod : null;
            objModelCategory.CleaningSolvent = lstCleaningSolvent.Any() ? lstCleaningSolvent : null;
            objModelCategory.ApplicationMethod = lstApplicationMethod.Any() ? lstApplicationMethod.ToList() : null;
            objModelCategory.RemovalMethod = lstRemovalMethod.Any() ? lstRemovalMethod : null;
            objModelCategory.LightIntensity = lstLightIntensity.Any() ? lstLightIntensity : null;
            objModelCategory.LightSource = lstLightSource.Any() ? lstLightSource : null;
            objModelCategory.MethodOfExamination = lstMethodOfExamination.Any() ? lstMethodOfExamination : null;
            //objModelCategory.VisibleUVLightIntensity = lstVisibleUVLightIntensity.Any() ? lstVisibleUVLightIntensity : null;
            objModelCategory.PenetrantProcessType = lstPenetrantProcessType.Any() ? lstPenetrantProcessType : null;
            objModelCategory.DeveloperApplicationMethod = lstDeveloperApplicationMethod.Any() ? lstDeveloperApplicationMethod : null;


            if (lstPTMethod.Any() && lstCleaningSolvent.Any() && lstApplicationMethod.Any() && lstRemovalMethod.Any() && lstLightIntensity.Any() && lstLightSource.Any())
                objModelCategory.Key = true;
            else
            {
                List<string> lstCategory = new List<string>();
                if (!lstPTMethod.Any())
                    lstCategory.Add("PT Method");
                if (!lstCleaningSolvent.Any())
                    lstCategory.Add("Cleaning Solvent");
                if (!lstApplicationMethod.Any())
                    lstCategory.Add("PT Application Method");
                if (!lstRemovalMethod.Any())
                    lstCategory.Add("Removal Method");
                if (!lstLightIntensity.Any())
                    lstCategory.Add("Light Intensity");
                if (!lstLightSource.Any())
                    lstCategory.Add("Light Source");
                if (!lstMethodOfExamination.Any())
                    lstCategory.Add("Method Of Examination");
                //if (!lstVisibleUVLightIntensity.Any())
                //    lstCategory.Add("Visible/UV Light Intensity");
                if (!lstPenetrantProcessType.Any())
                    lstCategory.Add("Penetrant Process Type");
                if (!lstDeveloperApplicationMethod.Any())
                    lstCategory.Add("Developer Application Method");

                objModelCategory.Key = false;
                objModelCategory.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
            }

            return Json(objModelCategory);
        }

        [HttpPost]
        public ActionResult GetMaxPTTechNo(string method, string location, string QMSproject)
        {
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                objMethodResponse = MaxTechniqueNo(method, location, QMSproject);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message.ToString();
            }
            return Json(objMethodResponse, JsonRequestBehavior.AllowGet);
        }

        public MethodResponse MaxTechniqueNo(string method, string location, string QMSproject)
        {
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                string methodNm = db.GLB002.Where(i => i.Code == method).Select(i => i.Code).FirstOrDefault();
                if (isPTTechniqueNoExist(method, location, QMSproject))
                {
                    var PTTechniqueDocNo = (from a in db.NDE007
                                            where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.Method == method && a.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase)
                                            select a).Max(a => a.DocNo);
                    objMethodResponse.Key = true;
                    objMethodResponse.Method = methodNm;
                    objMethodResponse.DocumentNo = Convert.ToInt32(PTTechniqueDocNo + 1);
                }
                else
                {
                    objMethodResponse.Key = true;
                    objMethodResponse.Method = methodNm;
                    objMethodResponse.DocumentNo = 1;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message.ToString();
            }

            return objMethodResponse;
        }

        public bool isPTTechniqueNoExist(string method, string location, string QMSproject)
        {
            bool flag = false;
            var lstPTTechnique = (from a in db.NDE007
                                  where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) //&& a.Method == method//a.QMSProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase) && 
                                  select a).ToList();

            if (lstPTTechnique.Any())
            {
                if (!string.IsNullOrEmpty(QMSproject) && !string.IsNullOrEmpty(method))
                {
                    if (lstPTTechnique.Where(i => i.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase) && i.Method == method).ToList().Any())
                        flag = true;
                }
            }

            return flag;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;opacity:0.5;' Title='" + buttonTooltip + "' class='" + className + "'" + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }
        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_GETPTTECHNIQUEHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.PTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_NDE_PTTechnique_GetHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.PTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE007_Log objNDE007 = db.NDE007_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "NDETECHNIQUE";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE007.ApprovedBy);
                model.ApprovedOn = objNDE007.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE007.SubmittedBy);
                model.SubmittedOn = objNDE007.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE007.CreatedBy);
                model.CreatedOn = objNDE007.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE007.ReturnedBy);
                model.ReturnedOn = objNDE007.ReturnedOn;

            }
            else
            {
                NDE007 objNDE007 = db.NDE007.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "NDETECHNIQUE";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE007.ApprovedBy);
                model.ApprovedOn = objNDE007.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE007.SubmittedBy);
                model.SubmittedOn = objNDE007.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE007.CreatedBy);
                model.CreatedOn = objNDE007.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE007.ReturnedBy);
                model.ReturnedOn = objNDE007.ReturnedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetPTConsumNumber(string methodtext)
        {
            string method = methodtext.Substring(0, methodtext.IndexOf("-"));
            method = method.Trim();
            NDE021 objNDE021 = new NDE021();
            var data = (from o in db.NDE021
                        where o.Method== method
                        select new
                        {
                            PTConsuNo = o.PTConsuNo
                        }).Distinct().ToList();

            //DataTable dt = ToDataTable(data);
            List<SelectListItem> ptcon = new List<SelectListItem>();
            if (data.Count > 0)
            {
                foreach (var item in data)
                {

                    ptcon.Add(new SelectListItem { Text = item.PTConsuNo.ToString(), Value = item.PTConsuNo.ToString() });
                }
            }
           
             
            return Json(new SelectList(ptcon,"Value","Text"));
           
        }

        


    }
}