﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ConsumePTMasterController : clsBase
    {
        #region first Page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        // load datatable
        [HttpPost]
        public JsonResult LoadPTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.MTStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('Draft','Returned')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                        "%' or lcom2.t_desc like '%" + param.sSearch +
                                        "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                        "%' or  QualityProject like '%" + param.sSearch +
                                        "%' or PTConsuNo like '%" + param.sSearch +
                                        "%' or BU like '%" + param.sSearch +
                                        "%' or Location like '%" + param.sSearch +
                                        "%' or Method like '%" + param.sSearch +
                                        "%' or PenetrantManufacturer like '%" + param.sSearch +
                                        "%' or PenetrantBrandType like '%" + param.sSearch +
                                        "%' or ReturnRemarks like '%" + param.sSearch +
                                        "%' or RevNo like '%" + param.sSearch +
                                        "%' or CreatedBy like '%" + param.sSearch +
                                        "%'  or Status like '%" + param.sSearch + "%')";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "NDE.BU", "NDE.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                NDEModels objNDEModels = new NDEModels();
                var lstResult = db.SP_PT_CONSUMABLE_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Method),
                           Convert.ToString(uc.PTConsuNo),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                           Convert.ToString(objNDEModels.GetCategory(uc.PenetrantManufacturer).CategoryDescription),
                           Convert.ToString(uc.PenetrantBrandType),
                            Convert.ToString(uc.ReturnRemarks),
                           Convert.ToString("R" +uc.RevNo),
                           Convert.ToString(uc.Status),
                           generateActionButtons(uc.HeaderId,uc.Status,"/NDE/ConsumePTMaster/AddConsumable?Id="+uc.HeaderId,"/NDE/ConsumePTMaster/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId),true,true,true,true,Convert.ToInt32(uc.RevNo))
                           //Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE021 objNDE021 = db.NDE021.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objNDE021 != null)
                {
                    db.NDE021.Remove(objNDE021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Tab
        [HttpPost]
        public ActionResult GetPTPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetPTHtmlPartial");
        }
        #endregion

        #region Create Consumable

        //detail page
        [SessionExpireFilter]
        public ActionResult AddConsumable(int? id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE021");
            NDE021 objNDE021 = new NDE021();
            NDEModels objNDEModels = new NDEModels();

            try
            {
                if (id != null)
                {
                    var objId = Convert.ToInt32(id);
                    objNDE021 = db.NDE021.Where(x => x.HeaderId == objId).FirstOrDefault();
                    ViewBag.QualityProject = objNDE021.QualityProject;
                    objNDE021.Project = db.COM001.Where(i => i.t_cprj == objNDE021.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    objNDE021.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE021.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    objNDE021.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE021.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    ViewBag.PTMethod = objNDEModels.GetCategory(objNDE021.Method).CategoryDescription;
                    ViewBag.BrandType = objNDEModels.GetCategory(objNDE021.PenetrantBrandType).CategoryDescription;
                    ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE021.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                    ViewBag.PenetrantManufacturer = objNDEModels.GetCategory(objNDE021.PenetrantManufacturer).CategoryDescription;
                    ViewBag.CleanerBrandType = objNDEModels.GetCategory(objNDE021.CleanerBrandType).CategoryDescription;
                    ViewBag.CleanerManufacturer = objNDEModels.GetCategory(objNDE021.CleanerManufacturer).CategoryDescription;
                    ViewBag.DeveloperBrandType = objNDEModels.GetCategory(objNDE021.DeveloperBrandType).CategoryDescription;
                    ViewBag.DeveloperManufacturer = objNDEModels.GetCategory(objNDE021.DeveloperManufacturer).CategoryDescription;


                    ViewBag.Action = "Edit";
                }
                else
                {
                    objNDE021.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objNDE021.RevNo = 0;
                    objNDE021.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objClsLoginInfo.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    ViewBag.Action = "AddNew";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View(objNDE021);
        }
        #endregion

        #region CRUD operations on Header and send to approver
        //insert / update 
        [HttpPost]
        public ActionResult SavePT(FormCollection fc, NDE021 nde021)
        {
            NDE021 objNDE021 = new NDE021();
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string location = fc["Location"].Split('-')[0];
                string Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                if (nde021.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde021.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE021 = db.NDE021.Where(x => x.HeaderId == nde021.HeaderId).FirstOrDefault();
                    if (objNDE021.Status == Approved)
                    {
                        objNDE021.RevNo = objNDE021.RevNo + 1;
                        objNDE021.ReturnRemarks = null;
                        objNDE021.ReturnedBy = null;
                        objNDE021.ReturnedOn = null;
                        objNDE021.SubmittedBy = null;
                        objNDE021.SubmittedOn = null;
                        objNDE021.ApprovedOn = null;
                    }
                    string PenetrantBatchDate1 = fc["PenetrantBatchDate1"]!=null ? fc["PenetrantBatchDate1"].ToString():"";
                    string CleanerBatchDate1 = fc["CleanerBatchDate1"] != null ? fc["CleanerBatchDate1"].ToString() : "";
                    string DeveloperBatchDate1 = fc["DeveloperBatchDate1"] != null ? fc["DeveloperBatchDate1"].ToString() : "";

                    objNDE021.Status = Status;
                    objNDE021.PenetrantManufacturer = nde021.PenetrantManufacturer;
                    objNDE021.PenetrantBrandType = nde021.PenetrantBrandType;
                    objNDE021.PenetrantBatchNo = nde021.PenetrantBatchNo;
                    objNDE021.PenetrantBatchDate = nde021.PenetrantBatchDate;// DateTime.ParseExact(PenetrantBatchDate1, @"d/M/yyyy", CultureInfo.InvariantCulture); //DateTime.ParseExact(PenetrantBatchDate1, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                    objNDE021.CleanerManufacturer = nde021.CleanerManufacturer;
                    objNDE021.CleanerBrandType = nde021.CleanerBrandType;
                    objNDE021.CleanerBatchNo = nde021.CleanerBatchNo;
                    objNDE021.CleanerBatchDate = nde021.CleanerBatchDate;// DateTime.ParseExact(CleanerBatchDate1, @"d/M/yyyy", CultureInfo.InvariantCulture); //DateTime.ParseExact(CleanerBatchDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    objNDE021.DeveloperManufacturer = nde021.DeveloperManufacturer;
                    objNDE021.DeveloperBrandType = nde021.DeveloperBrandType;
                    objNDE021.DeveloperBatchNo = nde021.DeveloperBatchNo;
                    objNDE021.DeveloperBatchDate = nde021.DeveloperBatchDate;// DateTime.ParseExact(DeveloperBatchDate1, @"d/M/yyyy", CultureInfo.InvariantCulture); //DateTime.ParseExact(DeveloperBatchDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    objNDE021.PlannerRemarks = nde021.PlannerRemarks;
                    objNDE021.EditedBy = objClsLoginInfo.UserName;
                    objNDE021.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    int Id = objNDE021.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Id.ToString();
                    objResponseMsg.Status = objNDE021.Status;
                    objResponseMsg.RevNo = objNDE021.RevNo;
                    objResponseMsg.Remarks = objNDE021.ReturnRemarks;
                }
                else
                {
                    string doc = nde021.PTConsuNo.Split('/')[2];
                    string loc = fc["Location"].Split('-')[0];
                    int docNo = Convert.ToInt32(doc);
                    objNDE021.QualityProject = nde021.QualityProject;
                    objNDE021.Project = fc["Project"].Split('-')[0];
                    objNDE021.BU = fc["BU"].Split('-')[0];
                    objNDE021.Location = location;
                    objNDE021.RevNo = 0;
                    objNDE021.PTConsuNo = nde021.PTConsuNo;
                    objNDE021.DocNo = docNo;
                    objNDE021.Status = Status;
                    objNDE021.Method = nde021.Method;
                    string date1 = nde021.PenetrantBatchDate.ToString();
                    objNDE021.PenetrantManufacturer = nde021.PenetrantManufacturer;
                    objNDE021.PenetrantBrandType = nde021.PenetrantBrandType;
                    objNDE021.PenetrantBatchNo = nde021.PenetrantBatchNo;
                    objNDE021.PenetrantBatchDate = nde021.PenetrantBatchDate;// Convert.ToDateTime(DateTime.ParseExact(date1, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                    objNDE021.CleanerManufacturer = nde021.CleanerManufacturer;
                    objNDE021.CleanerBrandType = nde021.CleanerBrandType;
                    objNDE021.CleanerBatchNo = nde021.CleanerBatchNo;
                    objNDE021.CleanerBatchDate = nde021.CleanerBatchDate;// Convert.ToDateTime(DateTime.ParseExact(nde021.CleanerBatchDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));

                    objNDE021.DeveloperManufacturer = nde021.DeveloperManufacturer;
                    objNDE021.DeveloperBrandType = nde021.DeveloperBrandType;
                    objNDE021.DeveloperBatchNo = nde021.DeveloperBatchNo;
                    objNDE021.DeveloperBatchDate = nde021.DeveloperBatchDate;// Convert.ToDateTime(DateTime.ParseExact(nde021.DeveloperBatchDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));

                    objNDE021.PlannerRemarks = nde021.PlannerRemarks;

                    objNDE021.CreatedBy = objClsLoginInfo.UserName;
                    objNDE021.CreatedOn = DateTime.Now;
                    db.NDE021.Add(objNDE021);
                    db.SaveChanges();
                    int Id = objNDE021.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Id.ToString();
                    objResponseMsg.Status = objNDE021.Status;
                    objResponseMsg.RevNo = objNDE021.RevNo;
                    objResponseMsg.Remarks = objNDE021.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //delete
        [HttpPost]
        public ActionResult DeletePT(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE021 objNDE021 = db.NDE021.Where(x => x.HeaderId == Id).FirstOrDefault();
                db.NDE021.Remove(objNDE021);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //send for approval
        [HttpPost]
        public ActionResult SendtoApprover(string project, string PTNumber, string Location)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string dStatus = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
            try
            {
                string location = Location.Split('-')[0];
                NDE021 objNDE021 = db.NDE021.Where(u => u.QualityProject == project && u.PTConsuNo == PTNumber && u.Location == location).SingleOrDefault();
                if (objNDE021 != null)
                {
                    if (objNDE021.HeaderId > 0)
                    {
                        if (!IsapplicableForSubmit(objNDE021.HeaderId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    objNDE021.Status = dStatus;
                    objNDE021.SubmittedBy = objClsLoginInfo.UserName;
                    objNDE021.SubmittedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE021.Project, objNDE021.BU, objNDE021.Location, "Consumable: " + objNDE021.PTConsuNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/approvePTConsum/ApproveConsumable?Id=" + objNDE021.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please save the details";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE021 objNDE021 = db.NDE021.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE021 != null)
            {
                if (objNDE021.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE021.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
            
        }


        private bool IsapplicableForSave(int headerId)
        {
            NDE021 objNDE021 = db.NDE021.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE021 != null)
            {
                if (objNDE021.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        #endregion

        #region Common Functions

        [HttpPost]
        public ActionResult getProjectByID(string Id)
        {
            //check duplicate record
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string project = (from pro in db.NDE021
                                  where pro.Project == Id
                                  select pro.Project).FirstOrDefault();
                if (project != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Duplicate;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //get project and BU by Qms project
        [HttpPost]
        public ActionResult GetProject(string qms)
        {
            if (!string.IsNullOrWhiteSpace(qms))
            {
                NDE021 objNDE021 = new NDE021();
                ConsumeMTMasterController.BUWiseDropdown objProjectDataModel = new ConsumeMTMasterController.BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;
                var lstProject = (from a in db.COM001
                                  where a.t_cprj == project
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;
                List<BULocWiseCategoryModel> lstBULocWiseMethodModel;
                List<BULocWiseCategoryModel> lstBULocWiseBrandModel;
                List<BULocWiseCategoryModel> listP_manufacturer;
                //List<BULocWiseCategoryModel> listP_BrandModel;
                List<BULocWiseCategoryModel> listC_BrandModel;
                List<BULocWiseCategoryModel> listD_BrandModel;
                List<BULocWiseCategoryModel> listC_manufacturer;
                List<BULocWiseCategoryModel> listD_manufacturer;

                objProjectDataModel.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == qms).Select(x => x.ManufacturingCode).FirstOrDefault();


                List<GLB002> lstMethodGLB002 = Manager.GetSubCatagories("PT Method", BU, objClsLoginInfo.Location).ToList();
                if (lstMethodGLB002.Count > 0)
                {
                    lstBULocWiseMethodModel = lstMethodGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseMethodModel = lstBULocWiseMethodModel;
                }

                List<GLB002> lstBrandGLB002 = Manager.GetSubCatagories("PT Brand Type", BU, objClsLoginInfo.Location).ToList();
                if (lstBrandGLB002.Count > 0)
                {
                    lstBULocWiseBrandModel = lstBrandGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseBrandModel = lstBULocWiseBrandModel;
                }

                List<GLB002> lstPenetrantmanufacturer = Manager.GetSubCatagories("Penetrant manufacturer", BU, objClsLoginInfo.Location).ToList();
                if (lstPenetrantmanufacturer.Count > 0)
                {
                    listP_manufacturer = lstPenetrantmanufacturer.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseP_manufacturer = listP_manufacturer;
                }

                //List<GLB002> lstPenetrantBrandtype = Manager.GetSubCatagories("Penetrant Brand type", BU, objClsLoginInfo.Location).ToList();
                //if (lstPenetrantBrandtype.Count > 0)
                //{
                //    listP_BrandModel = lstPenetrantBrandtype.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                //    objProjectDataModel.lstBULocWisePBrandModel = listP_BrandModel;
                //}

                List<GLB002> lstCleanermanufacturer = Manager.GetSubCatagories("Cleaner manufacturer", BU, objClsLoginInfo.Location).ToList();
                if (lstCleanermanufacturer.Count > 0)
                {
                    listC_manufacturer = lstCleanermanufacturer.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseC_manufacturer = listC_manufacturer;
                }

                List<GLB002> lstCleanerBrandtype = Manager.GetSubCatagories("Cleaner Brand type", BU, objClsLoginInfo.Location).ToList();
                if (lstCleanerBrandtype.Count > 0)
                {
                    listC_BrandModel = lstCleanerBrandtype.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseCBrandModel = listC_BrandModel;
                }

                List<GLB002> lstDeveloperBrandtype = Manager.GetSubCatagories("Developer Brand type", BU, objClsLoginInfo.Location).ToList();
                if (lstDeveloperBrandtype.Count > 0)
                {
                    listD_BrandModel = lstDeveloperBrandtype.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseDBrandModel = listD_BrandModel;
                }

                List<GLB002> lstDevelopermanufacturer = Manager.GetSubCatagories("Developer manufacturer", BU, objClsLoginInfo.Location).ToList();
                if (lstDevelopermanufacturer.Count > 0)
                {
                    listD_manufacturer = lstDevelopermanufacturer.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseD_manufacturer = listD_manufacturer;
                }


                if (lstBrandGLB002.Any() && lstMethodGLB002.Any())
                    objProjectDataModel.Key = true;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstMethodGLB002.Any())
                        lstCategory.Add("PT Method");
                    if (!lstBrandGLB002.Any())
                        lstCategory.Add("PT Brand Type");

                    if (!lstPenetrantmanufacturer.Any())
                        lstCategory.Add("Penetrant manufacturer");
                    //if (!lstPenetrantBrandtype.Any())
                    //    lstCategory.Add("Penetrant Brand Type");
                    if (!lstCleanermanufacturer.Any())
                        lstCategory.Add("Cleaner manufacturer");
                    if (!lstCleanerBrandtype.Any())
                        lstCategory.Add("Cleaner Brand Type");
                    if (!lstDeveloperBrandtype.Any())
                        lstCategory.Add("Developer Brand type");
                    if (!lstDevelopermanufacturer.Any())
                        lstCategory.Add("Developer manufacturer");


                    objProjectDataModel.Key = false;
                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //Generate PT Number
        public string MakeDocNumber(string qms, string method, string loc)
        {
            string location = loc.Split('-')[0];
            var lstobjNDE021 = db.NDE021.Where(x => x.QualityProject == qms && x.Method == method && x.Location == location).ToList();
            string strMethod = db.GLB002.Where(i => i.Code == method).Select(i => i.Code).FirstOrDefault();
            string ptNumber = "";
            string ptConsuNo = string.Empty;

            if (lstobjNDE021 != null && lstobjNDE021.Count() != 0)
            {
                var ptDocNum = (from a in db.NDE021
                                where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.Method == method && a.QualityProject.Equals(qms, StringComparison.OrdinalIgnoreCase)
                                select a).Max(a => a.DocNo);

                ptNumber = (ptDocNum + 1).ToString();
            }
            else
            {
                ptNumber = "1";
            }
            ptConsuNo = "PT/" + strMethod + "/" + ptNumber.PadLeft(3, '0');
            return ptConsuNo;
        }
        //Fetch PT number
        [HttpPost]
        public ActionResult GetPTNumber(string qms, string method, string loc)
        {
            NDE021 objNDE021 = new NDE021();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                string ptConsuNo = MakeDocNumber(qms, method, loc);
                objMethodResponse.Method = ptConsuNo;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message.ToString();
            }
            return Json(objMethodResponse);
        }
        public string generateActionButtons(int HeaderId, string status, string viewUrl = "", string timelineUrl = "", bool isViewButton = false, bool isDeleteButton = false, bool isTimelinebutton = false, bool isHistoryButton = false, int Revision = 0)
        {
            string strButtons = "<center style=\"white-space: nowrap;\">";
            if (isViewButton)
            {
                strButtons += "<a title=\"View\" href=\"" + WebsiteURL + viewUrl + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a>";
            }
            if (isDeleteButton)
            {
                if (status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue() && Revision == 0)
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader(" + HeaderId + ")\"></i>";
                }
                else
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer; opacity: 0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\"></i>";
                }
            }
            if (isTimelinebutton)
            {
                strButtons += "<a title=\"View Timeline\" href=\"javascript:void(0)\" onclick=\"ShowTimeline('" + timelineUrl + "');\"><i style=\"margin-left:5px;\" class=\"fa fa-clock-o\"></i></a>";
            }
            if (isHistoryButton)
            {
                if (Revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButtons += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderId) + "'); style = \"cursor: pointer;margin-left:5px;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButtons += "<i title=\"History\" style = \"cursor: pointer;margin-left:5px;opacity:0.5\" class=\"fa fa-history\"></i>";
                }
            }
            strButtons += "</center>";
            return strButtons;
        }

        #endregion

        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for Copy Details
        {
            return PartialView("~/Areas/NDE/Views/Shared/_CopyQualityProjectPartial.cshtml");
        }
        [HttpPost]
        public ActionResult copyDetails(string QualityProject, string method, int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;
                string strProject = new NDEModels().GetQMSProject(QualityProject).Project;
                string BU = db.COM001.Where(i => i.t_cprj == strProject).FirstOrDefault().t_entu;

                // NDE021 objNDE021 = db.NDE021.Where(x => x.QualityProject == QualityProject && x.Method == method && x.Location == currentLoc).FirstOrDefault();
                NDE021 objExistingNDE021 = db.NDE021.Where(x => x.HeaderId == headerId).FirstOrDefault();
                string ptConsuNo = MakeDocNumber(QualityProject, method, currentLoc);
                var Category = new NDEModels().GetCategoryId("PT Method");
                if (Manager.isMethodExistForBULocation(BU, currentLoc, objExistingNDE021.Method, Category))
                {
                    NDE021 objDesNDE021 = new NDE021();
                    objDesNDE021.QualityProject = QualityProject;
                    objDesNDE021.Project = strProject;
                    objDesNDE021.BU = BU;
                    objDesNDE021.Location = currentLoc;
                    objDesNDE021.Method = method;
                    objDesNDE021.PTConsuNo = ptConsuNo;
                    objResponseMsg = InsertNewDoc(objExistingNDE021, objDesNDE021, false);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Technique Is Not Available For Destination Project!";
                }
                #region old code
                //if (objNDE021 != null)
                //{
                //    if (objNDE021.Location.Trim() == currentLoc)
                //    {
                //        if (objNDE021.Status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue())
                //        {
                //            objResponseMsg = InsertNewDoc(objExistingNDE021, objNDE021, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "PT Consumable already exists.";
                //        }
                //    }
                //    else
                //    {
                //        NDE021 objDesNDE021 = new NDE021();
                //        string BU = db.COM001.Where(i => i.t_cprj == strProject).FirstOrDefault().t_entu;
                //        objDesNDE021.QualityProject = QualityProject;
                //        objDesNDE021.Project = strProject;
                //        objDesNDE021.BU = BU;
                //        objDesNDE021.Location = currentLoc;
                //        objDesNDE021.Method = method;
                //        objDesNDE021.PTConsuNo = ptConsuNo;
                //        objResponseMsg = InsertNewDoc(objExistingNDE021, objDesNDE021, false);
                //    }
                //}
                //else
                //{
                //    NDE021 objDesNDE021 = new NDE021();

                //    string BU = db.COM001.Where(i => i.t_cprj == strProject).FirstOrDefault().t_entu;
                //    objDesNDE021.QualityProject = QualityProject;
                //    objDesNDE021.Project = strProject;
                //    objDesNDE021.BU = BU;
                //    objDesNDE021.Location = currentLoc;
                //    objDesNDE021.Method = method;
                //    objDesNDE021.PTConsuNo = ptConsuNo;
                //    objResponseMsg = InsertNewDoc(objExistingNDE021, objDesNDE021, false);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg InsertNewDoc(NDE021 objSrcNDE021, NDE021 objDesNDE021, bool IsEdited)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string doc = objDesNDE021.PTConsuNo.Split('/')[2];
                int docNo = Convert.ToInt32(doc);
                objDesNDE021.DocNo = docNo;
                objDesNDE021.Status = clsImplementationEnum.NDETechniqueStatus.Draft.ToString();
                objDesNDE021.RevNo = 0;
                objDesNDE021.PenetrantManufacturer = objSrcNDE021.PenetrantManufacturer;
                objDesNDE021.PenetrantBrandType = objSrcNDE021.PenetrantBrandType;
                objDesNDE021.PenetrantBatchNo = objSrcNDE021.PenetrantBatchNo;
                objDesNDE021.PenetrantBatchDate = objSrcNDE021.PenetrantBatchDate;
                objDesNDE021.CleanerManufacturer = objSrcNDE021.CleanerManufacturer;
                objDesNDE021.CleanerBrandType = objSrcNDE021.CleanerBrandType;
                objDesNDE021.CleanerBatchNo = objSrcNDE021.CleanerBatchNo;
                objDesNDE021.CleanerBatchDate = objSrcNDE021.CleanerBatchDate;
                objDesNDE021.DeveloperManufacturer = objSrcNDE021.DeveloperManufacturer;
                objDesNDE021.DeveloperBrandType = objSrcNDE021.DeveloperBrandType;
                objDesNDE021.DeveloperBatchNo = objSrcNDE021.DeveloperBatchNo;
                objDesNDE021.DeveloperBatchDate = objSrcNDE021.DeveloperBatchDate;
                objDesNDE021.PlannerRemarks = objSrcNDE021.PlannerRemarks;
                #region old code
                //if (IsEdited)
                //{
                //    objDesNDE021.EditedBy = objClsLoginInfo.UserName;
                //    objDesNDE021.EditedOn = DateTime.Now;
                //    db.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderUpdate.ToString();
                //    objResponseMsg.HeaderID = objDesNDE021.HeaderId;
                //}
                //else
                //{
                #endregion
                objDesNDE021.CreatedBy = objClsLoginInfo.UserName;
                objDesNDE021.CreatedOn = DateTime.Now;
                db.NDE021.Add(objDesNDE021);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data Copied Successfully To Document No : " + objDesNDE021.PTConsuNo + " of " + objDesNDE021.Project + " Project.";
                //objResponseMsg.HeaderID = objDesNDE021.HeaderId;
                //}

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region History
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetPTHistoryDetails(int Id) //partial for  History Lines Details
        {
            NDE021_Log objNDE021Log = new NDE021_Log();
            objNDE021Log = db.NDE021_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_PTConsumableHistoryPartial", objNDE021Log);
        }

        [SessionExpireFilter]
        public ActionResult ViewLogDetails(int? Id)
        {
            NDE021_Log objLogNDE021 = new NDE021_Log();
            var user = objClsLoginInfo.UserName;
            NDEModels objNDEModels = new NDEModels();
            if (Id != null)
            {
                objLogNDE021 = db.NDE021_Log.Where(x => x.Id == Id).FirstOrDefault();
                ViewBag.BrandType = db.GLB002.Where(i => i.Code == objLogNDE021.PenetrantBrandType).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                ViewBag.Method = db.GLB002.Where(i => i.Code == objLogNDE021.Method).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                objLogNDE021.QualityProject = db.QMS010.Where(i => i.QualityProject == objLogNDE021.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                objLogNDE021.Project = db.COM001.Where(i => i.t_cprj == objLogNDE021.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objLogNDE021.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objLogNDE021.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objLogNDE021.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objLogNDE021.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objLogNDE021.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                ViewBag.PenetrantManufacturer = objNDEModels.GetCategory(objLogNDE021.PenetrantManufacturer).CategoryDescription;
                ViewBag.CleanerBrandType = objNDEModels.GetCategory(objLogNDE021.CleanerBrandType).CategoryDescription;
                ViewBag.CleanerManufacturer = objNDEModels.GetCategory(objLogNDE021.CleanerManufacturer).CategoryDescription;
                ViewBag.DeveloperBrandType = objNDEModels.GetCategory(objLogNDE021.DeveloperBrandType).CategoryDescription;
                ViewBag.DeveloperManufacturer = objNDEModels.GetCategory(objLogNDE021.DeveloperManufacturer).CategoryDescription;

            }
            return View(objLogNDE021);
        }

        [HttpPost]
        public JsonResult LoadPTHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "1=1 and HeaderId=" + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                      "%' or lcom2.t_desc like '%" + param.sSearch +
                                      "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                      "%' or  QualityProject like '%" + param.sSearch +
                                      "%' or PTConsuNo like '%" + param.sSearch +
                                      "%' or BU like '%" + param.sSearch +
                                      "%' or Location like '%" + param.sSearch +
                                      "%' or Method like '%" + param.sSearch +
                                      "%' or PenetrantManufacturer like '%" + param.sSearch +
                                      "%' or PenetrantBrandType like '%" + param.sSearch +
                                      "%' or ReturnRemarks like '%" + param.sSearch +
                                      "%' or RevNo like '%" + param.sSearch +
                                      "%' or CreatedBy like '%" + param.sSearch +
                                      "%'  or Status like '%" + param.sSearch + "%')";
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_NDE_PT_CONSUMABLE_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                NDEModels objNDEModels = new NDEModels();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.BU),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.Method),
                                Convert.ToString(uc.PTConsuNo),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                                Convert.ToString(objNDEModels.GetCategory(uc.PenetrantManufacturer).CategoryDescription),
                                Convert.ToString(uc.PenetrantBrandType),
                                Convert.ToString(uc.ReturnRemarks),
                                Convert.ToString("R" +uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.Project),
                                generateActionButtons(uc.Id,uc.Status,"/NDE/ConsumePTMaster/ViewLogDetails?Id="+uc.Id,"/NDE/ConsumePTMaster/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "&isHistory=true",true,false,true,false,Convert.ToInt32(uc.RevNo))
                                //Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region timeline
        public ActionResult ShowTimeline(int HeaderId, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE021_Log objNDE021 = db.NDE021_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "NDECONSUMABLE HEADER";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE021.ApprovedBy);
                model.ApprovedOn = objNDE021.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE021.SubmittedBy);
                model.SubmittedOn = objNDE021.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE021.CreatedBy);
                model.CreatedOn = objNDE021.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE021.ReturnedBy);
                model.ReturnedBy = objNDE021.ReturnedBy;
            }
            else
            {
                NDE021 objNDE021 = db.NDE021.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "NDECONSUMABLE HEADER";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE021.ApprovedBy);
                model.ApprovedOn = objNDE021.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE021.SubmittedBy);
                model.SubmittedOn = objNDE021.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE021.CreatedBy);
                model.CreatedOn = objNDE021.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE021.ReturnedBy);
                model.ReturnedBy = objNDE021.ReturnedBy;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                var lstResult = db.SP_PT_CONSUMABLE_GETDETAILS
                                (
                                1, int.MaxValue, strSortOrder, whereCondition
                                ).Select(x => new { x.QualityProject, x.Project, x.BU, x.Location, x.Method, x.PTConsuNo, x.PenetrantManufacturer, x.PenetrantBrandType, x.ReturnRemarks, x.RevNo, x.Status, x.CreatedBy }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
            {
                var lstResult = db.SP_NDE_PT_CONSUMABLE_HISTORY_DETAILS
                           (
                           1, int.MaxValue, strSortOrder, whereCondition
                           ).Select(x => new { x.QualityProject, x.Project, x.BU, x.Location, x.Method, x.PTConsuNo, x.PenetrantManufacturer, x.PenetrantBrandType, x.ReturnRemarks, x.RevNo, x.Status, x.CreatedBy }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region common class
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
            public string Status;
            public int? RevNo;
            public string Remarks;
        }
        #endregion

    }
}