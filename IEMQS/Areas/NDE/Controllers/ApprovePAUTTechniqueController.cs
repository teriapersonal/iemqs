﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using System.Data.Entity.Core.Objects;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApprovePAUTTechniqueController : clsBase
    {
        // GET: NDE/ApprovePAUTTechnique
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetApprovePAUTHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApprovePAUTHeaderGridDataPartial");
        }

        [SessionExpireFilter]
        public ActionResult ViewPAUTTechnique(int headerId)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE014");
            NDE014 objNDE014 = new NDE014();
            if (headerId > 0)
                objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();

            var QMSProject = new NDEModels().GetQMSProject(objNDE014.QualityProject);
            NDEModels objNDEModels = new NDEModels();
            ViewBag.QMSProject = QMSProject.QualityProject;// + " - " + QMSProject.ManufacturingCode; //db.TMP001.Where(i => i.QualityProject == objNDE014.QualityProject).Select(i => i.QualityProject + " - " + i.ManufacturingCode).FirstOrDefault();
            ViewBag.MfgCode = QMSProject.ManufacturingCode;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE014.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE014.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE014.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault(); ;

            ViewBag.Couplant = objNDEModels.GetCategory("Couplant", objNDE014.CouplantUsed, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
            ViewBag.ReflectorSize = objNDEModels.GetCategory("Reflector Size", objNDE014.ReferenceReflectorSize, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
            ViewBag.CableType = objNDEModels.GetCategory("Cable Type", objNDE014.CableType, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
            ViewBag.CableLength = objNDEModels.GetCategory("Cable Length", objNDE014.CableLength, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
            ViewBag.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objNDE014.ScanningTechnique, objNDE014.BU, objNDE014.Location, true).CategoryDescription;

            return View(objNDE014);
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde1.BU", "nde1.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde1.Status in('" + clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue() + "')";
                }

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or (nde1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or UTTechNo like '%" + param.sSearch + "%' " +
                                            " or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            "%' or TechniqueSheetNo like '%" + param.sSearch + "%'" + " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_PAUT_GetHeader(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                              Convert.ToString(a.HeaderId),
                              Convert.ToString(a.QualityProject),
                              Convert.ToString(a.Project),
                              Convert.ToString(a.BU),
                              Convert.ToString(a.Location),
                              Convert.ToString(a.TechniqueSheetNo),
                              Convert.ToString(a.UTTechNo),
                              Convert.ToString("R" + a.RevNo),
                               a.Status,
                              a.CreatedBy,
                              Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                             
                              "<center><a href=\""+WebsiteURL +"/NDE/ApprovePAUTTechnique/ViewPAUTTechnique?headerId=" + a.HeaderId +"\" title=\"View\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPAUTTechnique/ShowTimeline?HeaderID=" + Convert.ToInt32(a.HeaderId) + "&isHistory=false');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a><a title='Print' href='javascript:void(0)' onclick='PrintReport("+Convert.ToInt32(a.HeaderId)+",\""+a.Status+"\");'><i style='margin-left:10px;' " +
                                "class='fa fa-print'></i></a></center>"
                              //Convert.ToString(a.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult LoadSublinesData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;
                //readonly= "readonly"
                QCP001 objQCP001 = db.QCP001.FirstOrDefault(x => x.HeaderId == headerId);

                Func<QCP001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                var sortDirection = Request["sSortDir_0"]; // asc or desc
                string whereCondition = "nde16.LineId =" + param.CTQLineHeaderId;
                //string whereCondition = "nde16.HeaderId in (" + headerId + ") ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and nde16.ZoneNo in ('" + param.sSearch + "') or nde16.GroupNo in ('" + param.sSearch + "') or nde16.Angle in ('" + param.sSearch + "') or nde16.DepthCoverage in ('" + param.sSearch + "') or nde16.TCGIndicationAmplitude in ('" + param.sSearch + "') or nde16.FocalDepth in ('" + param.sSearch + "') or nde16.FocalPlane in ('" + param.sSearch + "')";
                    //whereCondition += " and ( l.MainCategory like '%" + param.sSearch + "%' or Activity like '%" + param.sSearch + "%' or ReferenceDoc like '%" + param.sSearch + "%' or Characteristics like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or VerifyingDoc like '%" + param.sSearch + "%'   or HeaderNotes like '%" + param.sSearch + "%' or FooterNotes like '%" + param.sSearch + "%' or RevisionDesc like '%" + param.sSearch + "%' or Status like '%" + param.sSearch + "%' or sl.CreatedBy like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstSublines = db.SP_NDE_PAUT_GetSubCategory(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int totalRecord = Convert.ToInt32(lstSublines.Select(x => x.TotalCount).FirstOrDefault());

                var data = (from uc in lstSublines
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           //GetRemarkStyle(objQCP001.Status,uc.ReturnRemark),
                             Convert.ToString(uc.ZoneNo),
                           //Convert.ToString(uc.LineSrNo),
                            Convert.ToString(uc.GroupNo),
                            Convert.ToString(uc.Angle),
                            Convert.ToString(uc.DepthCoverage),
                            Convert.ToString(uc.FocalDepth),
                            Convert.ToString(uc.TCGIndicationAmplitude),
                            Convert.ToString(uc.StartElement),
                            Convert.ToString(uc.NoofElements),
                            Convert.ToString(uc.AngularIncrementalChange),
                            Convert.ToString(uc.FocalPlane),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(uc.SubLineId),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),

                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReturnHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE014 objNDE014 = new NDE014();

            try
            {
                if (headerId > 0)
                {
                    if (!IsapplicableForAttend(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objNDE014.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                    objNDE014.ReturnRemarks = returnRemark;
                    objNDE014.ReturnedBy = objClsLoginInfo.UserName;
                    objNDE014.ReturnedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE014.Project, objNDE014.BU, objNDE014.Location, "Technique No: " + objNDE014.UTTechNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainPAUTTechnique/AddPAUTHeader?headerId=" + objNDE014.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document do not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApprovePAUTHeader(int headerID, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerID > 0)
                {
                    if (!IsapplicableForAttend(headerID))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                db.SP_NDE_PAUT_ApproveHeader(headerID, objClsLoginInfo.UserName);

                #region Send Notification
                NDE014 objNDE014 = db.NDE014.Where(c => c.HeaderId == headerID).FirstOrDefault();
                if (objNDE014 != null)
                {
                    objNDE014.ReturnRemarks = returnRemarks;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE014.Project, objNDE014.BU, objNDE014.Location, "Technique No: " + objNDE014.UTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE014 objNDE014 = db.NDE014.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE014 != null)
            {
                if (objNDE014.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult ApproveSelectedTechnique(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    string[] arrstrHeaderIds = strHeaderIds.Split(',');
                    for (int i = 0; i < arrstrHeaderIds.Length; i++)
                    {
                        int headerId = Convert.ToInt32(arrstrHeaderIds[i]);
                        db.SP_NDE_PAUT_ApproveHeader(headerId, objClsLoginInfo.UserName);

                        #region Send Notification
                        NDE014 objNDE014 = db.NDE014.Where(c => c.HeaderId == headerId).FirstOrDefault();
                        if (objNDE014 != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE014.Project, objNDE014.BU, objNDE014.Location, "Technique No: " + objNDE014.UTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetRemarkStyle(string status, string returnRemarks)
        {

            if (status == clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
            {
                return "<input type='text' name='txtRemark'  class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";

            }
            else
            {
                return "<input type='text' name='txtRemark' readonly= 'readonly'  class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";
            }
        }

        #region Export to Excel 
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //generate for header
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      UTTechNo = li.TechniqueSheetNo,
                                      TechniqueSheetNo = Convert.ToString(li.UTTechNo),
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn != null ? (Convert.ToDateTime(li.CreatedOn)).ToString("dd/MM/yyyy") : "",
                                      Status = li.Status,
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //for Lines
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetSubCategory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from sl in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(sl.ROW_NO),
                                      ZoneNo = Convert.ToString(sl.ZoneNo),
                                      GroupNo = Convert.ToString(sl.GroupNo),
                                      Angle = sl.Angle,
                                      DepthCoverage = sl.DepthCoverage,
                                      FocalDepth = Convert.ToString(sl.FocalDepth),
                                      TCGIndicationAmplitude = Convert.ToString(sl.TCGIndicationAmplitude),
                                      StartElement = Convert.ToString(sl.StartElement),
                                      NoofElements = Convert.ToString(sl.NoofElements),
                                      AngularIncrementalChange = Convert.ToString(sl.AngularIncrementalChange),
                                      FocalPlane = sl.FocalPlane,
                                      CreatedBy = Convert.ToString(sl.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(sl.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //for Sub Lines
                else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetSubCategory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from sl in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(sl.ROW_NO),
                                      GroupNo = Convert.ToString(sl.GroupNo),
                                      Angle = sl.Angle,
                                      DepthCoverage = sl.DepthCoverage,
                                      FocalDepth = Convert.ToString(sl.FocalDepth),
                                      TCGIndicationAmplitude = Convert.ToString(sl.TCGIndicationAmplitude),
                                      StartElement = Convert.ToString(sl.StartElement),
                                      NoofElements = Convert.ToString(sl.NoofElements),
                                      AngularIncrementalChange = Convert.ToString(sl.AngularIncrementalChange),
                                      FocalPlane = sl.FocalPlane,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetHeaderHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      UTTechNo = li.UTTechNo,
                                      TechniqueSheetNo = Convert.ToString(li.TechniqueSheetNo),
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn != null ? (Convert.ToDateTime(li.CreatedOn)).ToString("dd/MM/yyyy") : "",
                                      Status = li.Status,
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                if (gridType == clsImplementationEnum.GridType.HISTORY_SUB_LINES.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetSubCategoryHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from sl in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(sl.ROW_NO),
                                      //GetRemarkStyle(objQCP001.Status,uc.ReturnRemark),
                                      ZoneNo = Convert.ToString(sl.ZoneNo),
                                      //Convert.ToString(uc.LineSrNo),
                                      GroupNo = Convert.ToString(sl.GroupNo),
                                      Angle = sl.Angle,
                                      DepthCoverage = sl.DepthCoverage,
                                      FocalDepth = Convert.ToString(sl.FocalDepth),
                                      TCGIndicationAmplitude = Convert.ToString(sl.TCGIndicationAmplitude),
                                      StartElement = Convert.ToString(sl.StartElement),
                                      NoofElements = Convert.ToString(sl.NoofElements),
                                      AngularIncrementalChange = Convert.ToString(sl.AngularIncrementalChange),
                                      FocalPlane = sl.FocalPlane,
                                      CreatedBy = Convert.ToString(sl.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(sl.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}