﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApprovePTMTCTQController : clsBase
    {
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        //check for duplicate tab on submit and save
        private bool IsapplicableForAttend(int headerId)
        {
            NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE001 != null)
            {
                if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult PTMTCTQLineDetails(string HeaderID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE001");
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(HeaderID))
            {
                HeaderId = Convert.ToInt32(HeaderID);
            }
            NDEModels objNDEModels = new NDEModels();
            NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objNDE001 != null)
            {
                ViewBag.HeaderID = objNDE001.HeaderId;
                ViewBag.Status = objNDE001.Status;
                ViewBag.CTQNo = objNDE001.CTQNo;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objNDE001.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var BUDescription = db.COM002.Where(x => x.t_dimx == objNDE001.BU).Select(x => x.t_desc).FirstOrDefault();
                objNDE001.BU = Convert.ToString(objNDE001.BU + " - " + BUDescription);
                var Location = db.COM002.Where(x => x.t_dimx == objNDE001.Location).Select(x => x.t_desc).FirstOrDefault();
                objNDE001.Location = Convert.ToString(objNDE001.Location + " - " + Location);
            }
            return View(objNDE001);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetCTQHeaderLinesHtml(int HeaderId, string strCTQNo)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.NDE001.Where(x => x.HeaderId == HeaderId).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetHeaderLinesHtml");
        }

        [HttpPost]
        public JsonResult LoadPTMTCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde1.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde1.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde1.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or nde1.CreatedBy + ' - ' + com003c.t_name like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde1.BU", "nde1.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTMTCTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                //Convert.ToString(uc.Project),
                                //Convert.ToString(uc.BU),
                                //Convert.ToString(uc.Location),
                                //Convert.ToString(uc.CTQNo),
                                //Convert.ToString(uc.ManufacturingCode),
                                //Convert.ToString(uc.ApplicableSpecification),
                                //Convert.ToString(uc.AcceptanceStandard),
                                //(uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                                //Convert.ToString(uc.TPIName),
                                //Convert.ToString(uc.NotificationRequired==true ? "Yes":uc.NotificationRequired == false ? "No" : ""),
                                ////(uc.NotificationRequired == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),
                                //Convert.ToString(uc.ThicknessOfJob),
                                //Convert.ToString(uc.SizeDimension),
                                //Convert.ToString("R"+uc.RevNo),
                                //Convert.ToString(uc.CreatedBy),
                                //Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                                //Convert.ToString(uc.Status),
                                //Convert.ToString(uc.NDERemarks),
                                 Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                               Convert.ToString(uc.NotificationRequired==true ? "Yes":uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<center><a title='View' href='"+WebsiteURL +"/NDE/ApprovePTMTCTQ/PTMTCTQLineDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPTMTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"+""+((uc.RevNo > 0 || uc.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()) ?"<i title='History' onclick=History('"+Convert.ToInt32(uc.HeaderId)+"'); style = 'margin-left:5px;' class='fa fa-history'></i>" : "<i title='History' style = 'margin-left:5px;opacity:0.5' class='fa fa-history'></i>" )+""+"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadPTMTCTQHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                if (param.sSearch == "yes" || param.sSearch == "Yes" || param.sSearch == "YES")
                {
                    param.sSearch = "1";
                }
                if (param.sSearch == "no" || param.sSearch == "NO" || param.sSearch == "No")
                {
                    param.sSearch = "0";
                }
                string[] arrayCTQStatus = { clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() };
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                int ndeHeaderId = Convert.ToInt32(param.CTQHeaderId);
                NDE001 objNDE001 = db.NDE001.FirstOrDefault(x => x.HeaderId == ndeHeaderId);
                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_PT_MT_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.ApplicableForPT),
                                Convert.ToString(uc.ApplicableForMT),
                                Convert.ToString(uc.SpecificationNo),
                                Convert.ToString(uc.SpecificationWithClauseNo),
                                Convert.ToString(uc.ToBeDiscussed==true ? "Yes":uc.ToBeDiscussed==false?"No":""),
                                Convert.ToString(uc.SpecificationRequirement),
                                Convert.ToString(uc.CriticalToQuality==true ? "Yes":uc.CriticalToQuality == false ? "No" : ""),
                                Convert.ToString(uc.CTQRequirement),
                                GetRemarkStyle(objNDE001.Status,uc.Status, uc.ReturnRemark),//(arrayCTQStatus.Contains(Convert.ToString(uc.Status))? "<input type='text' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='"+Convert.ToString(uc.ReturnRemark)+"' />":Convert.ToString(uc.ReturnRemark)),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetRemarkStyle(string status,string linestatus, string returnRemarks)
        {

            if (linestatus != clsImplementationEnum.CommonStatus.Deleted.GetStringValue()&& status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return "<input type='text' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' maxlength=\"100\" />";

            }
            else
            {
                return "<input type='text' readonly= 'readonly' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' maxlength=\"100\" />";
            }
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId, null, false);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                
                objResponseMsg = ApproveHeaderByApprover(HeaderId, returnRemarks, true);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId, string returnRemarks, bool IsReturnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue().ToUpper();
                string deleted = clsImplementationEnum.CommonStatus.Deleted.GetStringValue().ToUpper();
                List<NDE002> lstdeletedLines = db.NDE002.Where(x => x.HeaderId == HeaderId && x.Status.Trim() == deleted.Trim()).ToList();
                if (lstdeletedLines != null && lstdeletedLines.Count > 0)
                {
                    db.NDE002.RemoveRange(lstdeletedLines);
                    db.SaveChanges();
                }

                List<NDE002> lstNDE002 = db.NDE002.Where(x => x.HeaderId == HeaderId).ToList();
                List<NDE002_Log> lstNDE002_Log = new List<NDE002_Log>();
                if (lstNDE002 != null && lstNDE002.Count > 0)
                {
                    foreach (var objData in lstNDE002)
                    {
                        NDE002 objNDE002 = lstNDE002.Where(x => x.LineId == objData.LineId).FirstOrDefault();

                        objNDE002.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                        objNDE002.ApprovedBy = objClsLoginInfo.UserName;
                        objNDE002.ApprovedOn = DateTime.Now;
                        objNDE002.EditedBy = objClsLoginInfo.UserName;
                        objNDE002.EditedOn = DateTime.Now;

                        #region Line Log

                        NDE002_Log objNDE002_Log = new NDE002_Log();

                        objNDE002_Log.LineId = objNDE002.LineId;
                        objNDE002_Log.HeaderId = objNDE002.HeaderId;
                        objNDE002_Log.Project = objNDE002.Project;
                        objNDE002_Log.BU = objNDE002.BU;
                        objNDE002_Log.Location = objNDE002.Location;
                        objNDE002_Log.RevNo = objNDE002.RevNo;
                        objNDE002_Log.LineRevNo = objNDE002.LineRevNo;
                        objNDE002_Log.Status = objNDE002.Status;
                        objNDE002_Log.ApplicableForMT = objNDE002.ApplicableForMT;
                        objNDE002_Log.ApplicableForPT = objNDE002.ApplicableForPT;
                        objNDE002_Log.Description = objNDE002.Description;
                        objNDE002_Log.SpecificationNo = objNDE002.SpecificationNo;
                        objNDE002_Log.SpecificationWithClauseNo = objNDE002.SpecificationWithClauseNo;
                        objNDE002_Log.ToBeDiscussed = objNDE002.ToBeDiscussed;
                        objNDE002_Log.SpecificationRequirement = objNDE002.SpecificationRequirement;
                        objNDE002_Log.CriticalToQuality = objNDE002.CriticalToQuality;
                        objNDE002_Log.CTQRequirement = objNDE002.CTQRequirement;
                        objNDE002_Log.ReturnRemark = objNDE002.ReturnRemark;
                        objNDE002_Log.CreatedBy = objNDE002.CreatedBy;
                        objNDE002_Log.CreatedOn = objNDE002.CreatedOn;
                        objNDE002_Log.EditedBy = objNDE002.EditedBy;
                        objNDE002_Log.EditedOn = objNDE002.EditedOn;
                        objNDE002_Log.SubmittedBy = objNDE002.SubmittedBy;
                        objNDE002_Log.SubmittedOn = objNDE002.SubmittedOn;
                        objNDE002_Log.ApprovedBy = objNDE002.ApprovedBy;
                        objNDE002_Log.ApprovedOn = objNDE002.ApprovedOn;
                        objNDE002_Log.ReturnedBy = objNDE002.ReturnedBy;
                        objNDE002_Log.ReturnedOn = objNDE002.ReturnedOn;
                        objNDE002_Log.ReturnRemark = objNDE002.ReturnRemark;
                        objNDE002_Log.ApproverRemark = objNDE002.ApproverRemark;
                        lstNDE002_Log.Add(objNDE002_Log);
                        #endregion

                    }


                }
                var lstNDE001_Log = db.NDE001_Log.Where(x => x.HeaderId == HeaderId).ToList();
                NDE001_Log objLog = new NDE001_Log();
                if (objLog != null)
                {
                    objLog = lstNDE001_Log.FirstOrDefault();
                    lstNDE001_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                }

                NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objNDE001 != null)
                {
                    objNDE001.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                    if (IsReturnRemark)
                    {
                        objNDE001.ReturnRemarks = returnRemarks;
                    }
                    objNDE001.ApprovedBy = objClsLoginInfo.UserName;
                    objNDE001.ApprovedOn = DateTime.Now;
                    objNDE001.EditedBy = objClsLoginInfo.UserName;
                    objNDE001.EditedOn = DateTime.Now;

                    #region Header Log
                    NDE001_Log objNDE001_Log = db.NDE001_Log.Where(x => x.HeaderId == objNDE001.HeaderId).FirstOrDefault();

                    if (objNDE001_Log == null)
                    {
                        objNDE001_Log = db.NDE001_Log.Add(new NDE001_Log
                        {
                            HeaderId = objNDE001.HeaderId,
                            Project = objNDE001.Project,
                            BU = objNDE001.BU,
                            Location = objNDE001.Location,
                            RevNo = objNDE001.RevNo,
                            CTQNo = objNDE001.CTQNo,
                            Status = objNDE001.Status,
                            ManufacturingCode = objNDE001.ManufacturingCode,
                            ApplicableSpecification = objNDE001.ApplicableSpecification,
                            AcceptanceStandard = objNDE001.AcceptanceStandard,
                            ASMECodeStamp = objNDE001.ASMECodeStamp,
                            TPIName = objNDE001.TPIName,
                            NotificationRequired = objNDE001.NotificationRequired,
                            ThicknessOfJob = objNDE001.ThicknessOfJob,
                            SizeDimension = objNDE001.SizeDimension,
                            NDERemarks = objNDE001.NDERemarks,
                            CreatedBy = objNDE001.CreatedBy,
                            CreatedOn = objNDE001.CreatedOn,
                            EditedBy = objNDE001.EditedBy,
                            EditedOn = objNDE001.EditedOn,
                            ApprovedBy = objNDE001.ApprovedBy,
                            ApprovedOn = objNDE001.ApprovedOn,
                            SubmittedBy = objNDE001.SubmittedBy,
                            SubmittedOn = objNDE001.SubmittedOn
                        });
                    }
                    else
                    {
                        objNDE001_Log.Status = clsImplementationEnum.PTMTCTQStatus.Superseded.GetStringValue();
                        objNDE001_Log = db.NDE001_Log.Add(new NDE001_Log
                        {
                            HeaderId = objNDE001.HeaderId,
                            Project = objNDE001.Project,
                            BU = objNDE001.BU,
                            Location = objNDE001.Location,
                            RevNo = objNDE001.RevNo,
                            CTQNo = objNDE001.CTQNo,
                            Status = objNDE001.Status,
                            ManufacturingCode = objNDE001.ManufacturingCode,
                            ApplicableSpecification = objNDE001.ApplicableSpecification,
                            AcceptanceStandard = objNDE001.AcceptanceStandard,
                            ASMECodeStamp = objNDE001.ASMECodeStamp,
                            TPIName = objNDE001.TPIName,
                            NotificationRequired = objNDE001.NotificationRequired,
                            ThicknessOfJob = objNDE001.ThicknessOfJob,
                            SizeDimension = objNDE001.SizeDimension,
                            NDERemarks = objNDE001.NDERemarks,
                            CreatedBy = objNDE001.CreatedBy,
                            CreatedOn = objNDE001.CreatedOn,
                            EditedBy = objNDE001.EditedBy,
                            EditedOn = objNDE001.EditedOn,
                            ApprovedBy = objNDE001.ApprovedBy,
                            ApprovedOn = objNDE001.ApprovedOn,
                            SubmittedBy = objNDE001.SubmittedBy,
                            SubmittedOn = objNDE001.SubmittedOn
                        });
                    }
                    #endregion
                    db.SaveChanges();
                    if (lstNDE002_Log != null && lstNDE002_Log.Count > 0)
                    {
                        lstNDE002_Log.ForEach(x => x.RefId = objNDE001_Log.Id);
                        db.NDE002_Log.AddRange(lstNDE002_Log);
                    }
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE001.Project, objNDE001.BU, objNDE001.Location, "CTQ: " + objNDE001.CTQNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Lines successfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Lines not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult UpdateReturnRemarks(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE002 objNDE002 = db.NDE002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objNDE002 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objNDE002.ReturnRemark = changeText;
                    }
                    else
                    {
                        objNDE002.ReturnRemark = null;
                    }
                    //objNDE002.ReturnedBy = objClsLoginInfo.UserName;
                    // objNDE002.ReturnedOn = DateTime.Now;
                    objNDE002.EditedBy = objClsLoginInfo.UserName;
                    objNDE002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnLines(int HeaderId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                string Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToUpper();
                //List<NDE002> lstNDE002 = db.NDE002.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper().Trim() == Status.ToUpper().Trim() && x.ReturnRemark != null).ToList();
                List<NDE002> lstNDE002 = db.NDE002.Where(x => x.HeaderId == HeaderId && x.ReturnRemark != null).ToList();
                if (lstNDE002 != null && lstNDE002.Count > 0 || !string.IsNullOrWhiteSpace(returnRemarks))
                {
                    foreach (var objData in lstNDE002)
                    {
                        NDE002 objNDE002 = lstNDE002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                    //    objNDE002.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                        objNDE002.ReturnedBy = objClsLoginInfo.UserName;
                        objNDE002.ReturnedOn = DateTime.Now;
                        objNDE002.EditedBy = objClsLoginInfo.UserName;
                        objNDE002.EditedOn = DateTime.Now;
                    }
                    NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                    objNDE001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objNDE001.EditedBy = objClsLoginInfo.UserName;
                    objNDE001.EditedOn = DateTime.Now;
                    objNDE001.ReturnRemarks = returnRemarks;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE001.Project, objNDE001.BU, objNDE001.Location, "CTQ: " + objNDE001.CTQNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainPTMTCTQ/AddUpdatePTMTCTQ?HeaderID=" + objNDE001.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully returned.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add return remarks for header/lines.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PTMTCTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = li.NotificationRequired,
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      ManufacturingCode = li.ManufacturingCode,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_GET_PT_MT_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      LineRevNo = li.LineRevNo,
                                      Status = li.Status,
                                      SpecificationWithClauseNo = li.SpecificationWithClauseNo,
                                      SpecificationRequirement = li.SpecificationRequirement,
                                      CriticalToQuality = li.CriticalToQuality,
                                      CTQRequirement = li.CTQRequirement,
                                      ToBeDiscussed = li.ToBeDiscussed,
                                      QualificationForExam = li.QualificationForExam,
                                      ApprovalRequired = li.ApprovalRequired,
                                      ConsumableSelection = li.ConsumableSelection,
                                      MethodofPT = li.MethodofPT,
                                      MethodofMT = li.MethodofMT,
                                      MTParticleTypes = li.MTParticleTypes,
                                      ExtentofCustomer = li.ExtentofCustomer,
                                      PTMTAcceptanceStandard = li.PTMTAcceptanceStandard,
                                      DwellTimeforPT = li.DwellTimeforPT,
                                      MTTechniques = li.MTTechniques,
                                      ColourContrast = li.ColourContrast,
                                      Others = li.Others,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      ReturnedBy = li.ReturnedBy,
                                      ReturnedOn = li.ReturnedOn,
                                      ReturnRemark = li.ReturnRemark,
                                      ApproverRemark = li.ApproverRemark,
                                      MTInaccessible = li.MTInaccessible,
                                      Description = li.Description,
                                      SpecificationNo = li.SpecificationNo
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}