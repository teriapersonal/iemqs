﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApprovePTTechniqueController : clsBase
    {
        // GET: NDE/ApprovePTTechnique
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetApprovePTTechniqueGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApprovePTTechniqueGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde7.BU", "nde7.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde7.Status in('" + clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue() + "')";
                }

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(NDE7.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(NDE7.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or NDE7.QualityProject like '%" + param.sSearch + "%' or (NDE7.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or PTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%'" +
                                            "or TechniqueSheetNo like '%" + param.sSearch + "%' or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_GETPTTECHNIQUEHEADER(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                                Convert.ToString(a.HeaderId),
                                //Convert.ToString(a.HeaderId),
                                Convert.ToString(a.QualityProject),
                                Convert.ToString(a.Project),
                                Convert.ToString(a.BU),
                                Convert.ToString(a.Location),
                                Convert.ToString(a.TechniqueSheetNo),
                                Convert.ToString(a.PTTechNo),
                                Convert.ToString("R" + a.RevNo),
                                a.CreatedBy,
                                Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                                a.Status,
                                "<center style=\"display:inline;\"><a href=\""+WebsiteURL +"/NDE/ApprovePTTechnique/ViewPTTechnique?headerId=" + a.HeaderId + "\" title=\"View\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i> </a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/PTTechniqueMaster/ShowTimeline?HeaderID="+Convert.ToInt32(a.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>" +
                                "<a title='Print' href='javascript:void(0)' onclick='PrintReport("+Convert.ToInt32(a.HeaderId)+",\""+a.Status+"\");'><i style='margin-left:10px;' " +
                                "class='fa fa-print'></i></a></center>"

                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]

        public ActionResult ViewPTTechnique(int headerId)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE007");
            NDE007 objNDE007 = new NDE007();
            NDEModels objNDEModels = new NDEModels();
            if (headerId > 0)
            {
                objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
                var QMSProject = new NDEModels().GetQMSProject(objNDE007.QualityProject);
                ViewBag.QMSProject = QMSProject.QualityProject;//+" - "+QMSProject.ManufacturingCode;//db.TMP001.Where(i => i.QualityProject == objNDE007.QualityProject).Select(i => i.QualityProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.MfgCode = QMSProject.ManufacturingCode;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE007.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE007.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE007.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.PTMethod = objNDEModels.GetCategory(objNDE007.Method).CategoryDescription;
                ViewBag.CleaningSolvent = objNDEModels.GetCategory("Cleaning Solvent", objNDE007.CleaningSolvent, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.ApplicationMethod = objNDEModels.GetCategory("PT Application Method", objNDE007.ApplicationMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.RemovalMethod = objNDEModels.GetCategory("Removal Method", objNDE007.RemovalMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.LightIntensity = objNDEModels.GetCategory("Light Intensity", objNDE007.LightIntensity, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                ViewBag.LightSource = objNDEModels.GetCategory("Light Source", objNDE007.LightSource, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                objNDE007.MethodOfExamination = objNDEModels.GetCategory("Method Of Examination", objNDE007.MethodOfExamination, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                objNDE007.PenetrantProcessType = objNDEModels.GetCategory("Penetrant Process Type", objNDE007.PenetrantProcessType, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                objNDE007.DevAppMethod = objNDEModels.GetCategory("Developer Application Method", objNDE007.DevAppMethod, objNDE007.BU, objNDE007.Location, true).CategoryDescription;
                //      objNDE007.VisibleUVLightIntensity = objNDEModels.GetCategory("Visible/UV Light Intensity", objNDE007.VisibleUVLightIntensity, objNDE007.BU, objNDE007.Location, true).CategoryDescription;

            }
            return View(objNDE007);
        }

        [HttpPost]
        public ActionResult ReturnHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE007 objNDE007 = new NDE007();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (headerId > 0)
                {
                    objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    objNDE007.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                    objNDE007.ReturnRemarks = returnRemark;
                    objNDE007.ReturnedBy = objClsLoginInfo.UserName;
                    objNDE007.ReturnedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE007.Project, objNDE007.BU, objNDE007.Location, "Technique No: " + objNDE007.PTTechNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/PTTechniqueMaster/AddPTTechnique?headerId=" + objNDE007.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                db.SP_NDE_ApprovePTTechnique(headerId, objClsLoginInfo.UserName);

                #region Send Notification
                NDE007 objNDE007 = db.NDE007.Where(i => i.HeaderId == headerId).FirstOrDefault();
                if (objNDE007 != null)
                {
                    objNDE007.ReturnRemarks = returnRemark;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE007.Project, objNDE007.BU, objNDE007.Location, "Technique No: " + objNDE007.PTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE007 objNDE007 = db.NDE007.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE007 != null)
            {
                if (objNDE007.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;

        }

        [HttpPost]
        public ActionResult ApproveSelectedTechnique(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    string[] arrstrHeaderIds = strHeaderIds.Split(',');
                    for (int i = 0; i < arrstrHeaderIds.Length; i++)
                    {
                        int headerId = Convert.ToInt32(arrstrHeaderIds[i]);
                        db.SP_NDE_ApprovePTTechnique(headerId, objClsLoginInfo.UserName);

                        #region Send Notification
                        NDE007 objNDE007 = db.NDE007.Where(c => c.HeaderId == headerId).FirstOrDefault();
                        if (objNDE007 != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE007.Project, objNDE007.BU, objNDE007.Location, "Technique No: " + objNDE007.PTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion

                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_NDE_GETPTTECHNIQUEHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  QualityProject = li.QualityProject,
                                  Project = li.Project,
                                  BU = li.BU,
                                  Location = li.Location,
                                  PTTechNo = li.TechniqueSheetNo,
                                  TechniqueSheetNo = Convert.ToString(li.PTTechNo),
                                  RevNo = "R" + li.RevNo,
                                  CreatedBy = li.CreatedBy,
                                  EditedBy = li.EditedBy,
                                  Status = li.Status,
                                  CreatedOn = li.CreatedOn,
                                  EditedOn = li.EditedOn
                              }).ToList();
                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}