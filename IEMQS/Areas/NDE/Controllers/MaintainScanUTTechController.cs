﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainScanUTTechController : clsBase
    {
        // GET: NDE/MaintainScanUTTech

        #region Header
        //Main Header grid Page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        //partial for Grid (Index)
        [HttpPost]
        public ActionResult LoadScanUTDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_ScanUTHtmlPartial");
        }

        //datatable for header
        [HttpPost]
        public JsonResult LoadScanUTHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "where 1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "where 1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                "%' or QualityProject like '%" + param.sSearch +
                                "%' or lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde10.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or UTTechNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or DocNo like '%" + param.sSearch +
                                "%' or UTProcedureNo like '%" + param.sSearch +
                                "%' or RecordableIndication like '%" + param.sSearch +
                                "%' or ScanTechnique like '%" + param.sSearch +
                                "%' or DGSDiagram like '%" + param.sSearch +
                                "%' or BasicCalibrationBlock like '%" + param.sSearch +
                                "%' or SimulationBlock1 like '%" + param.sSearch +
                                "%' or SimulationBlock2 like '%" + param.sSearch +
                                "%' or CreatedBy like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch +
                                "%' or TechniqueSheetNo like '%" + param.sSearch +
                                "%' or (nde10.CreatedBy+'-'+c32.t_namb) like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde10.BU", "nde10.Location");

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCAN_UT_TECH_GETDETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.TechniqueSheetNo),
                            Convert.ToString(uc.UTTechNo),
                            Convert.ToString("R" + uc.RevNo),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.UTProcedureNo),
                            Convert.ToString(uc.ReturnRemarks),
                            MakeDisplayButton(uc.HeaderId,uc.Status,Convert.ToInt32(uc.RevNo),true,true,false)
                           // Convert.ToString(uc.HeaderId),

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //detail page
        [SessionExpireFilter]
        public ActionResult AddHeader(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE010");
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        //partial for Header detail form
        [HttpPost]
        public ActionResult LoadScanUTFormPartial(int HeaderID)
        {
            NDE010 objNDE010 = new NDE010();
            NDEModels objNDEModels = new NDEModels();
            string user = objClsLoginInfo.UserName;
            List<Projects> project = Manager.getProjectsByUser(user);

            var location = objClsLoginInfo.Location;

            List<string> lstyesno = clsImplementationEnum.getDGSyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objNDE010 = db.NDE010.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                ViewBag.QualityProject = objNDE010.QualityProject;

                ViewBag.ScanTechnique = objNDEModels.GetCategory("Scan Technique", objNDE010.ScanTechnique, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.CouplantUsed = objNDEModels.GetCategory("Couplant", objNDE010.CouplantUsed, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.SimulationBlock1 = objNDEModels.GetCategory("Simulation Block", objNDE010.SimulationBlock1, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.ScanningSensitivity = objNDEModels.GetCategory("Scanning Sensitivity", objNDE010.ScanningSensitivity, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.BasicCalibrationBlock = objNDEModels.GetCategory("Basic Calibration Block", objNDE010.BasicCalibrationBlock, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.RecordableIndication = objNDEModels.GetCategory("Recordable Indication At Reference Gain", objNDE010.RecordableIndication, objNDE010.BU, objNDE010.Location, true).CategoryDescription;

                objNDE010.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objNDE010.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objNDE010.Project = db.COM001.Where(i => i.t_cprj == objNDE010.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objNDE010.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objNDE010.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE010.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                ViewBag.Action = "edit";
            }
            else
            {
                objNDE010.Location = db.COM002.Where(a => a.t_dtyp == 1 && location.Contains(a.t_dimx)).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objNDE010.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objNDE010.DGSDiagram = false;
                objNDE010.RevNo = 0;
            }

            return PartialView("_ScanUTTechFormPartial", objNDE010);
        }


        //Insert/Update header 
        [HttpPost]
        public ActionResult SaveUTTechHeader(NDE010 nde010, string dsgDiagram)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                bool d1;
                if (dsgDiagram.ToString() == "Yes")
                {
                    d1 = true;
                }
                else
                {
                    d1 = false;
                }

                if (nde010.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde010.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    bool isRevised = false;
                    NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == nde010.HeaderId).FirstOrDefault();
                    if (objNDE010.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE010.RevNo = Convert.ToInt32(objNDE010.RevNo) + 1;
                        objNDE010.ReturnRemarks = null;
                        objNDE010.ReturnedBy = null;
                        objNDE010.ReturnedOn = null;
                        objNDE010.SubmittedBy = null;
                        objNDE010.SubmittedOn = null;
                        objNDE010.ApprovedOn = null;
                        objNDE010.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        isRevised = true;
                    }
                    //objNDE010.UTTechNo = nde010.UTTechNo;
                    objNDE010.UTProcedureNo = nde010.UTProcedureNo;
                    objNDE010.RecordableIndication = nde010.RecordableIndication;
                    objNDE010.ScanTechnique = nde010.ScanTechnique;
                    objNDE010.DGSDiagram = d1;
                    objNDE010.BasicCalibrationBlock = nde010.BasicCalibrationBlock;
                    objNDE010.SimulationBlock1 = nde010.SimulationBlock1;
                    objNDE010.SimulationBlock2 = nde010.SimulationBlock2;
                    objNDE010.CouplantUsed = nde010.CouplantUsed;
                    objNDE010.TechniqueSheetNo= nde010.TechniqueSheetNo;
                    objNDE010.ScanningSensitivity = nde010.ScanningSensitivity;
                    objNDE010.PlannerRemarks = nde010.PlannerRemarks;
                    objNDE010.EditedBy = objClsLoginInfo.UserName;
                    objNDE010.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    var folderPath = "NDE010/" + objNDE010.HeaderId + "/R" + objNDE010.RevNo;
                    if (isRevised)
                    {
                        var oldFolderPath = "NDE010/" + objNDE010.HeaderId + "/R" + (objNDE010.RevNo - 1);
                        ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                        (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
                        //clsUploadCopy.CopyFolderContents(oldFolderPath, folderPath, objATH008, true);
                    }
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.HeaderID = objNDE010.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                    objResponseMsg.Remarks = objNDE010.ReturnRemarks;
                    objResponseMsg.Status = objNDE010.Status;
                    objResponseMsg.RevNo = objNDE010.RevNo;
                }
                else
                {
                    NDE010 objNDE010 = new NDE010();
                    string doc = nde010.UTTechNo.Split('/')[2];
                    int docNo = Convert.ToInt32(doc);
                    objNDE010.QualityProject = nde010.QualityProject;
                    objNDE010.Project = nde010.Project.Split('-')[0].ToString();
                    objNDE010.BU = nde010.BU.Split('-')[0].ToString();
                    objNDE010.Location = nde010.Location.Split('-')[0].ToString();
                    objNDE010.RevNo = 0;
                    objNDE010.UTTechNo = nde010.UTTechNo;
                    objNDE010.DocNo = docNo;
                    objNDE010.Status = nde010.Status;
                    objNDE010.UTProcedureNo = nde010.UTProcedureNo;
                    objNDE010.RecordableIndication = nde010.RecordableIndication;
                    objNDE010.ScanTechnique = nde010.ScanTechnique;
                    objNDE010.DGSDiagram = d1;
                    objNDE010.BasicCalibrationBlock = nde010.BasicCalibrationBlock;
                    objNDE010.SimulationBlock1 = nde010.SimulationBlock1;
                    objNDE010.SimulationBlock2 = nde010.SimulationBlock2;
                    objNDE010.CouplantUsed = nde010.CouplantUsed;
                    objNDE010.ScanningSensitivity = nde010.ScanningSensitivity;
                    objNDE010.TechniqueSheetNo = nde010.TechniqueSheetNo;
                    objNDE010.PlannerRemarks = nde010.PlannerRemarks;
                    objNDE010.CreatedBy = objClsLoginInfo.UserName;
                    objNDE010.CreatedOn = DateTime.Now;
                    db.NDE010.Add(objNDE010);
                    db.SaveChanges();

                    //var folderPath = "NDE010/" + objNDE010.HeaderId + "/R" + objNDE010.RevNo;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.HeaderID = objNDE010.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString();
                    objResponseMsg.Remarks = objNDE010.ReturnRemarks;
                    objResponseMsg.Status = objNDE010.Status;
                    objResponseMsg.RevNo = objNDE010.RevNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //delete header (in draft and R0)
        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == headerId).FirstOrDefault();
                List<NDE011> objNDE011 = db.NDE011.Where(x => x.HeaderId == headerId).ToList();
                if (objNDE010 != null)
                {
                    if (objNDE011.Count > 0)
                    {
                        db.NDE011.RemoveRange(objNDE011);
                        db.SaveChanges();
                    }
                    db.NDE010.Remove(objNDE010);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetScanUTLinesForm(int HeaderId, string projCode, string BU, string location, int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            //if (HeaderId > 0)
            //{
            //    if (!IsapplicableForSave(HeaderId))
            //    {
            //        objResponseMsg.Key = false;
            //        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
            //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            //    }
            //}

            NDE011 objNDE011 = new NDE011();
            BUWiseDropdown objProjectDataModel = new BUWiseDropdown();
            string project = projCode.Split('-')[0];
            string buCode = BU.Split('-')[0];
            string Qmslocation = location.Split('-')[0];

            NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            var lstSearchUnit = db.NDE017.ToList();
            ViewBag.SearchUnit = lstSearchUnit.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.SearchUnit.ToString(), CatID = x.Id.ToString() }).ToList();

            List<GLB002> lstRRGLB002 = Manager.GetSubCatagories("Reference Reflector", buCode, Qmslocation).ToList();
            ViewBag.ReferenceReflector = lstRRGLB002.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = (x.Description).ToString(), CatID = x.Code.ToString() }).ToList();

            List<GLB002> lstCLGLB002 = Manager.GetSubCatagories("Cable Length", buCode, Qmslocation).ToList();
            ViewBag.CableLength = lstCLGLB002.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = (x.Description).ToString(), CatID = x.Code.ToString() }).ToList();

            List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Cable Type", buCode, Qmslocation).ToList();
            ViewBag.CableType = lstCTGLB002.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = (x.Description).ToString(), CatID = x.Code.ToString() }).ToList();

            if (lstRRGLB002.Any() && lstCLGLB002.Any() && lstCTGLB002.Any())
                ViewBag.Key = true;
            else
            {
                List<string> lstCategory = new List<string>();
                if (!lstRRGLB002.Any())
                    lstCategory.Add("Reference Reflector");
                if (!lstCLGLB002.Any())
                    lstCategory.Add("Cable Length");
                if (!lstCTGLB002.Any())
                    lstCategory.Add("Cable Type");

                ViewBag.Key = false;
                ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
            }

            if (LineId > 0)
            {
                objNDE011 = db.NDE011.Where(x => x.LineId == LineId).FirstOrDefault();
                NDEModels objNDEModels = new NDEModels();
                var Project = (from a in db.COM001
                               where a.t_cprj == objNDE011.Project
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                ViewBag.Project = Project.pDesc;
                objNDE011.NDE010.RevNo = objNDE011.RevNo;
                ViewBag.Action = "line edit";
                ViewBag.ReferenceReflector1 = objNDEModels.GetCategory("Reference Reflector", objNDE011.ReferenceReflector, objNDE011.BU, objNDE011.Location, true).CategoryDescription;
                ViewBag.CableLength1 = objNDEModels.GetCategory("Cable Length", objNDE011.CableLength, objNDE011.BU, objNDE011.Location, true).CategoryDescription;
                ViewBag.CableType1 = objNDEModels.GetCategory("Cable Type", objNDE011.CableType, objNDE011.BU, objNDE011.Location, true).CategoryDescription;

                //ViewBag.ReferenceReflector1 = db.GLB002.Where(i => i.Code == objNDE011.ReferenceReflector).Select(i =>  i.Description).FirstOrDefault();
                //ViewBag.CableLength1 = db.GLB002.Where(i => i.Code == objNDE011.CableLength).Select(i =>  i.Description).FirstOrDefault();
                //ViewBag.CableType1 = db.GLB002.Where(i => i.Code == objNDE011.CableType).Select(i =>  i.Description).FirstOrDefault();
                ViewBag.SearchUnit1 = db.NDE017.Where(x => x.Id == objNDE011.SearchUnit).Select(x => x.SearchUnit).FirstOrDefault();

                int searchunit = Convert.ToInt32(objNDE011.SearchUnit);
                NDE017 objNDE017 = db.NDE017.FirstOrDefault(x => x.Id == searchunit);
                ViewBag.Frequency = objNDE017.Frequency;
                ViewBag.Element = objNDE017.ElementSize;
                ViewBag.Beamangle = objNDE017.BeamAngle;
                ViewBag.make = objNDE017.SearchUnitMake;
            }
            else
            {
                objNDE011.RevNo = 0;

                var Project = (from a in db.COM001
                               where a.t_cprj == project
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                ViewBag.Project = Project.pDesc;


            }
            objNDE011.HeaderId = objNDE010.HeaderId;
            objNDE011.QualityProject = objNDE010.QualityProject;
            objNDE011.UTTechNo = objNDE010.UTTechNo;
            ViewBag.BU = BU;
            ViewBag.Location = location;
            return PartialView("_GetScanUTLinesForm", objNDE011);
        }

        //datatable
        [HttpPost]
        public JsonResult LoadScanUTLineData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = "where HeaderId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( RevNo like '%" + param.sSearch +
                               "%' or nde017.SearchUnit like '%" + param.sSearch +
                               "%' or ReferenceReflector like '%" + param.sSearch +
                               "%' or CableType like '%" + param.sSearch +
                               "%' or CableLength like '%" + param.sSearch +
                               "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCAN_UT_LINES_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ReferenceReflector),
                                Convert.ToString(uc.CableType),
                                Convert.ToString(uc.CableLength),
                                Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //Insert/Update for lines
        [HttpPost]
        public ActionResult SaveScanUTLines(NDE011 nde011, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isRevised = false;
                NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == nde011.HeaderId).FirstOrDefault();

                if (objNDE010.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde011.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }                

                if (nde011.LineId > 0)
                {
                    NDE011 objNDE011 = db.NDE011.Where(x => x.LineId == nde011.LineId).FirstOrDefault();
                    if (objNDE010.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                    {

                        objNDE010.RevNo = Convert.ToInt32(objNDE010.RevNo) + 1;
                        //objNDE011.RevNo = Convert.ToInt32(objNDE011.RevNo) + 1;
                        objNDE010.ReturnRemarks = null;
                        objNDE010.ReturnedBy = null;
                        objNDE010.ReturnedOn = null;
                        objNDE010.SubmittedBy = null;
                        objNDE010.SubmittedOn = null;
                        objNDE010.ApprovedOn = null;
                        objNDE011.RevNo = Convert.ToInt32(objNDE010.RevNo);
                        objNDE010.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        isRevised = true;
                    }
                    objNDE011.UTTechNo = objNDE010.UTTechNo;
                    objNDE011.QualityProject = objNDE010.QualityProject;
                    objNDE011.SearchUnit = nde011.SearchUnit;
                    objNDE011.ReferenceReflector = nde011.ReferenceReflector;
                    objNDE011.CableLength = nde011.CableLength;
                    objNDE011.CableType = nde011.CableType;
                    objNDE011.EditedBy = objClsLoginInfo.UserName;
                    objNDE011.EditedOn = DateTime.Now;
                }
                else
                {
                    db.NDE011.Add(new NDE011
                    {
                        HeaderId = nde011.HeaderId,
                        Project = objNDE010.Project,
                        BU = objNDE010.BU,
                        Location = objNDE010.Location,
                        RevNo = 0,
                        UTTechNo = objNDE010.UTTechNo,
                        QualityProject = objNDE010.QualityProject,
                        SearchUnit = nde011.SearchUnit,
                        ReferenceReflector = nde011.ReferenceReflector,
                        CableType = nde011.CableType,
                        CableLength = nde011.CableLength,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    if (objNDE010.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                    {
                        objNDE010.RevNo = Convert.ToInt32(objNDE010.RevNo) + 1;
                        objNDE010.ReturnRemarks = null;
                        objNDE010.ReturnedBy = null;
                        objNDE010.ReturnedOn = null;
                        objNDE010.SubmittedBy = null;
                        objNDE010.SubmittedOn = null;
                        objNDE010.ApprovedOn = null;
                        objNDE010.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objNDE010.EditedBy = objClsLoginInfo.UserName;
                        objNDE010.EditedOn = DateTime.Now;
                        isRevised = true;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (isRevised)
                {
                    var oldFolderPath = "NDE010/" + objNDE010.HeaderId + "/R" + (objNDE010.RevNo - 1);
                    var newFolderPath = "NDE010/" + objNDE010.HeaderId + "/R" + (objNDE010.RevNo);
                    //ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                    //clsUploadCopy.CopyFolderContents(oldFolderPath, newFolderPath, objATH008, true);
                    (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, newFolderPath);
                }
                if (nde011.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
                }
                objResponseMsg.Status = objNDE010.Status;
                objResponseMsg.Remarks = objNDE010.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //send header for approval
        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                NDE010 objNDE010 = db.NDE010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objNDE010 != null)
                {
                    objNDE010.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                    objNDE010.SubmittedBy = objClsLoginInfo.UserName;
                    objNDE010.SubmittedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE010.Project, objNDE010.BU, objNDE010.Location, "Technique No: " + objNDE010.UTTechNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveScanUT/ViewAScanUTTechnique?HeaderID=" + objNDE010.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details has been successfully sent for approval.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE010.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE010.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsapplicableForSave(int headerId)
        {
            NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == headerId).FirstOrDefault();

            if (objNDE010!=null && objNDE010.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //delete individual line
        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                NDE011 objNDE011 = db.NDE011.Where(x => x.LineId == LineID).FirstOrDefault();
                if(objNDE011.HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(objNDE011.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                if (objNDE011 != null)
                {
                    db.NDE011.Remove(objNDE011);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for Copy Details
        {
            return PartialView("~/Areas/NDE/Views/Shared/_CopyQualityProjectPartial.cshtml");
        }
        [HttpPost]
        public ActionResult copyDetails(string QualityProject, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = objClsLoginInfo.Location;

                string project = new NDEModels().GetQMSProject(QualityProject).Project;

                //NDE010 objNDE010 = db.NDE010.Where(x => x.QualityProject == QualityProject && x.Location == currentLoc).FirstOrDefault();
                NDE010 objExistingNDE010 = db.NDE010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var UTNum = MakeAScanUT(QualityProject.Trim(), currentLoc.Trim());

                NDE010 objDesNDE010 = new NDE010();
                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;
                objDesNDE010.QualityProject = QualityProject;
                objDesNDE010.Project = project;
                objDesNDE010.BU = BU;
                objDesNDE010.Location = currentLoc;
                objDesNDE010.UTTechNo = UTNum;
                objResponseMsg = InsertNewDoc(objExistingNDE010, objDesNDE010, false);
                #region old code
                //if (objNDE010 != null)
                //{
                //    if (objNDE010.Location.Trim() == currentLoc)
                //    {
                //        if (objNDE010.Status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue())
                //        {
                //            objResponseMsg = InsertNewDoc(objExistingNDE010, objNDE010, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "A Scan UT Technique already exists.";
                //        }
                //    }
                //    else
                //    {
                //        NDE010 objDesNDE010 = new NDE010();
                //        string BU = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
                //        objDesNDE010.QualityProject = QualityProject;
                //        objDesNDE010.Project = Project;
                //        objDesNDE010.BU = BU;
                //        objDesNDE010.Location = currentLoc;
                //        objDesNDE010.UTTechNo = UTNum;
                //        objResponseMsg = InsertNewDoc(objExistingNDE010, objDesNDE010, false);
                //    }
                //}
                //else
                //{
                //    NDE010 objDesNDE010 = new NDE010();
                //    string BU = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
                //    objDesNDE010.QualityProject = QualityProject;
                //    objDesNDE010.Project = Project;
                //    objDesNDE010.BU = BU;
                //    objDesNDE010.Location = currentLoc;
                //    objDesNDE010.UTTechNo = UTNum;
                //    objResponseMsg = InsertNewDoc(objExistingNDE010, objDesNDE010, false);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus InsertNewDoc(NDE010 objSrcNDE010, NDE010 objDesNDE010, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string doc = objDesNDE010.UTTechNo.Split('/')[2];
                int docNo = Convert.ToInt32(doc);
                objDesNDE010.RevNo = 0;
                objDesNDE010.DocNo = docNo;
                objDesNDE010.TechniqueSheetNo = objSrcNDE010.TechniqueSheetNo;
                objDesNDE010.UTProcedureNo = objSrcNDE010.UTProcedureNo;
                objDesNDE010.RecordableIndication = objSrcNDE010.RecordableIndication;
                objDesNDE010.ScanTechnique = objSrcNDE010.ScanTechnique;
                objDesNDE010.DGSDiagram = objSrcNDE010.DGSDiagram;
                objDesNDE010.BasicCalibrationBlock = objSrcNDE010.BasicCalibrationBlock;
                objDesNDE010.SimulationBlock1 = objSrcNDE010.SimulationBlock1;
                objDesNDE010.SimulationBlock2 = objSrcNDE010.SimulationBlock2;
                objDesNDE010.CouplantUsed = objSrcNDE010.CouplantUsed;
                objDesNDE010.ScanPlan = objSrcNDE010.ScanPlan;
                objDesNDE010.ScanningSensitivity = objSrcNDE010.ScanningSensitivity;
                objDesNDE010.PlannerRemarks = objSrcNDE010.PlannerRemarks;
                objDesNDE010.ScanPlan = objSrcNDE010.ScanPlan;
                objDesNDE010.PlannerRemarks = objSrcNDE010.PlannerRemarks;
                objDesNDE010.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                #region old code
                //if (IsEdited)
                //{
                //    objDesNDE010.EditedBy = objClsLoginInfo.UserName;
                //    objDesNDE010.EditedOn = DateTime.Now;
                //    List<NDE011> lstDesNDE011 = db.NDE011.Where(x => x.HeaderId == objDesNDE010.HeaderId).ToList();

                //    if (lstDesNDE011 != null && lstDesNDE011.Count > 0)
                //    {
                //        db.NDE011.RemoveRange(lstDesNDE011);
                //    }
                //    db.SaveChanges();
                //    List<NDE011> lstSrcNDE011 = db.NDE011.Where(x => x.HeaderId == objSrcNDE010.HeaderId).ToList();
                //    db.NDE011.AddRange(
                //                        lstSrcNDE011.Select(x =>
                //                        new NDE011
                //                        {
                //                            HeaderId = objDesNDE010.HeaderId,
                //                            Project = objDesNDE010.Project,
                //                            BU = objDesNDE010.BU,
                //                            Location = objDesNDE010.Location,
                //                            RevNo = 0,
                //                            SearchUnit = x.SearchUnit,
                //                            ReferenceReflector = x.ReferenceReflector,
                //                            CableType = x.CableType,
                //                            CableLength = x.CableLength,
                //                            CreatedBy = objClsLoginInfo.UserName,
                //                            CreatedOn = DateTime.Now,
                //                        })
                //                      );
                //    db.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderUpdate.ToString();
                //    objResponseMsg.HeaderId = objDesNDE010.HeaderId;
                //}
                //else
                //{
                #endregion
                objDesNDE010.CreatedBy = objClsLoginInfo.UserName;
                objDesNDE010.CreatedOn = DateTime.Now;
                db.NDE010.Add(objDesNDE010);
                db.SaveChanges();
                List<NDE011> lstSrcNDE011 = db.NDE011.Where(x => x.HeaderId == objSrcNDE010.HeaderId).ToList();
                if (lstSrcNDE011 != null && lstSrcNDE011.Count > 0)
                {
                    db.NDE011.AddRange(
                                    lstSrcNDE011.Select(x =>
                                    new NDE011
                                    {
                                        HeaderId = objDesNDE010.HeaderId,
                                        Project = objDesNDE010.Project,
                                        BU = objDesNDE010.BU,
                                        Location = objDesNDE010.Location,
                                        RevNo = 0,
                                        SearchUnit = x.SearchUnit,
                                        ReferenceReflector = x.ReferenceReflector,
                                        CableType = x.CableType,
                                        CableLength = x.CableLength,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                    })
                                  );
                    //}
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objDesNDE010.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE010.UTTechNo;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.Copy.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE010.Project + " Project.";

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        public string MakeAScanUT(string QualityProject, string currentLoc)
        {
            var lstobjNDE010 = db.NDE010.Where(x => x.QualityProject == QualityProject.Trim() && x.Location == currentLoc.Trim()).ToList();
            string UTNumber = "";
            string AScanUTNo = string.Empty;

            if (lstobjNDE010 != null && lstobjNDE010.Count() != 0)
            {
                var UTNum = (from a in db.NDE010
                             where a.Location.Equals(currentLoc.Trim(), StringComparison.OrdinalIgnoreCase) && a.QualityProject.Equals(QualityProject.Trim(), StringComparison.OrdinalIgnoreCase)
                             select a).Max(a => a.DocNo);
                UTNumber = (UTNum + 1).ToString();
            }
            else
            {
                UTNumber = "1";
            }
            AScanUTNo = "UT/ASCAN/" + UTNumber.PadLeft(3, '0');
            return AScanUTNo;
        }
        #endregion

        #region History
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetScantUTHistoryDetails(int Id) //partial for CTQ History Lines Details
        {
            NDE010_Log objNDE010Log = new NDE010_Log();
            objNDE010Log = db.NDE010_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_ASCANUTHistoryPartial", objNDE010Log);
        }
        [SessionExpireFilter]
        public ActionResult ViewLogDetails(int? Id)
        {
            NDEModels objNDEModels = new NDEModels();
            NDE010_Log objNDE010 = new NDE010_Log();
            if (Id > 0)
            {
                objNDE010 = db.NDE010_Log.Where(i => i.Id == Id).FirstOrDefault();
                ViewBag.QualityProject = db.QMS010.Where(i => i.QualityProject == objNDE010.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                ViewBag.ScanTechnique = objNDEModels.GetCategory("Scan Technique", objNDE010.ScanTechnique, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.CouplantUsed = objNDEModels.GetCategory("Couplant", objNDE010.CouplantUsed, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.SimulationBlock1 = objNDEModels.GetCategory("Simulation Block", objNDE010.SimulationBlock1, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.ScanningSensitivity = objNDEModels.GetCategory("Scanning Sensitivity", objNDE010.ScanningSensitivity, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.BasicCalibrationBlock = objNDEModels.GetCategory("Basic Calibration Block", objNDE010.BasicCalibrationBlock, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.RecordableIndication = objNDEModels.GetCategory("Recordable Indication At Reference Gain", objNDE010.RecordableIndication, objNDE010.BU, objNDE010.Location, true).CategoryDescription;
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE010.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();


                //List<string> lstyesno = clsImplementationEnum.getDGSyesno().ToList();
                //ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                //List<string> lstBasicCalib = clsImplementationEnum.getBasicCalibrationBlock().ToList();
                //ViewBag.lstBasicCalib = lstBasicCalib.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                objNDE010.Project = db.COM001.Where(i => i.t_cprj == objNDE010.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objNDE010.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objNDE010.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objNDE010.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objNDE010.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            return View(objNDE010);
        }

        [HttpPost]
        public JsonResult LoadScanUTHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "where 1=1 and HeaderId=" + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                                   "%' or lcom2.t_desc like '%" + param.sSearch +
                                                   "%' or (nde10.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                                   "%' or UTTechNo like '%" + param.sSearch +
                                                   "%' or RevNo like '%" + param.sSearch +
                                                   "%' or DocNo like '%" + param.sSearch +
                                                   "%' or UTProcedureNo like '%" + param.sSearch +
                                                   "%' or RecordableIndication like '%" + param.sSearch +
                                                   "%' or ScanTechnique like '%" + param.sSearch +
                                                   "%' or DGSDiagram like '%" + param.sSearch +
                                                   "%' or BasicCalibrationBlock like '%" + param.sSearch +
                                                   "%' or SimulationBlock1 like '%" + param.sSearch +
                                                   "%' or SimulationBlock2 like '%" + param.sSearch +
                                                   "%' or CreatedBy like '%" + param.sSearch +
                                                   "%' or Status like '%" + param.sSearch +
                                                   "%' or TechniqueSheetNo like '%" + param.sSearch +
                                                   "%' or (nde10.CreatedBy+'-'+c32.t_namb) like '%" + param.sSearch + "%')";
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_NDE_ASCAN_UT_TECH_HISTROY_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.TechniqueSheetNo),
                            Convert.ToString(uc.UTTechNo),
                            Convert.ToString("R" + uc.RevNo),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.UTProcedureNo),
                            Convert.ToString(uc.ReturnRemarks),
                            "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a title=\"View\" href=\""+WebsiteURL+"/NDE/MaintainScanUTTech/ViewLogDetails?Id=" + uc.Id + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainScanUTTech/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                           //Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadScanUTLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += "where RefId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( RevNo like '%" + param.sSearch +
                               "%' or nde017.SearchUnit like '%" + param.sSearch +
                               "%' or ReferenceReflector like '%" + param.sSearch +
                               "%' or CableType like '%" + param.sSearch +
                               "%' or CableLength like '%" + param.sSearch +
                               "%')";
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_NDE_SCAN_UT_LINES_HISTORY_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ReferenceReflector),
                                Convert.ToString(uc.CableType),
                                Convert.ToString(uc.CableLength),
                                Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSearchUnitDetails(int searchUnit)
        {
            NDE017 lstSearchUnit = db.NDE017.Where(x => x.Id == searchUnit).FirstOrDefault();
            var results = new
            {
                Frequency = lstSearchUnit.Frequency,
                Element = lstSearchUnit.ElementSize,
                Beamangle = lstSearchUnit.BeamAngle,
                make = lstSearchUnit.SearchUnitMake
            };
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Timeline
        public ActionResult ShowTimeline(int HeaderId, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE010_Log objNDE010 = db.NDE010_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "SCANUTTECH";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE010.ApprovedBy);
                model.ApprovedOn = objNDE010.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE010.SubmittedBy);
                model.SubmittedOn = objNDE010.SubmittedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE010.ReturnedBy);
                model.ReturnedOn = objNDE010.ReturnedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE010.CreatedBy);
                model.CreatedOn = objNDE010.CreatedOn;
            }
            else
            {
                NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "SCANUTTECH";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE010.ApprovedBy);
                model.ApprovedOn = objNDE010.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE010.SubmittedBy);
                model.SubmittedOn = objNDE010.SubmittedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE010.ReturnedBy);
                model.ReturnedOn = objNDE010.ReturnedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE010.CreatedBy);
                model.CreatedOn = objNDE010.CreatedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region export to excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {

                    var lst = db.SP_SCAN_UT_TECH_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      UTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.UTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn != null ? (Convert.ToDateTime(li.CreatedOn)).ToString("dd/MM/yyyy") : "",
                                      UTProcedureNo = li.UTProcedureNo,
                                      ReturnRemarks = li.ReturnRemarks
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {

                    var lst = db.SP_SCAN_UT_LINES_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ScanDirection = li.ROW_NO,
                                      SearchUnit = Convert.ToString(li.SearchUnit),
                                      ReferenceReflector = Convert.ToString(li.ReferenceReflector),
                                      CableType = Convert.ToString(li.CableType),
                                      CableLength = Convert.ToString(li.CableLength),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_ASCAN_UT_TECH_HISTROY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      UTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.UTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn != null ? (Convert.ToDateTime(li.CreatedOn)).ToString("dd/MM/yyyy") : "",
                                      UTProcedureNo = li.UTProcedureNo,
                                      ReturnRemarks = li.ReturnRemarks
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_NDE_SCAN_UT_LINES_HISTORY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ScanDirection = li.ROW_NO,
                                      SearchUnit = Convert.ToString(li.SearchUnit),
                                      ReferenceReflector = Convert.ToString(li.ReferenceReflector),
                                      CableType = Convert.ToString(li.CableType),
                                      CableLength = Convert.ToString(li.CableLength),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region common functions
        //generate Number and bind Dropdowns accoding to BU & LOC
        [HttpPost]
        public ActionResult GetUTNumber(string qms, string loc)
        {   //generate UT number
            if (!string.IsNullOrWhiteSpace(qms))
            {
                NDE010 objNDE010 = new NDE010();
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string strproject = db.QMS010.Where(x => x.QualityProject == qms).Select(x => x.Project).FirstOrDefault();
                //string project = new NDEModels().GetQMSProject(qms).Project;
                string location = loc.Split('-')[0].Trim();

                var lstobjNDE010 = db.NDE010.Where(x => x.QualityProject == qms && x.Location == location).ToList();

                var lstProject = (from a in db.COM001
                                  where a.t_cprj == strproject
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                string BU = db.COM001.Where(i => i.t_cprj == strproject).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.Project = lstProject.ProjectDesc.Trim();
                objProjectDataModel.BUDescription = BUDescription.BUDesc.Trim();

                List<GLB002> lstScanGLB002 = Manager.GetSubCatagories("Scan Technique", BU.Trim(), objClsLoginInfo.Location.Trim()).ToList();
                if (lstScanGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseScanTech = lstScanGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstScanTech = lstBULocWiseScanTech;
                }

                List<GLB002> lstCouplantGLB002 = Manager.GetSubCatagories("Couplant", BU.Trim(), objClsLoginInfo.Location.Trim()).ToList();
                if (lstCouplantGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseCouplant = lstCouplantGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstCouplantUsed = lstBULocWiseCouplant;
                }
                List<GLB002> lstSBGLB002 = Manager.GetSubCatagories("Simulation Block", BU.Trim(), objClsLoginInfo.Location.Trim()).ToList();
                if (lstSBGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseSimulation = lstSBGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstSimulationBlock1 = lstBULocWiseSimulation;
                }
                List<GLB002> lstSSGLB002 = Manager.GetSubCatagories("Scanning Sensitivity", BU.Trim(), objClsLoginInfo.Location.Trim()).ToList();
                if (lstSSGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseScanningSensitivity = lstSSGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstScanningSensitivity = lstBULocWiseScanningSensitivity;
                }
                List<GLB002> lstBasicCalBlockGLB002 = Manager.GetSubCatagories("Basic Calibration Block", BU.Trim(), objClsLoginInfo.Location.Trim()).ToList();
                if (lstBasicCalBlockGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseBasicCalBlock = lstBasicCalBlockGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBasicCalibrationBlock = lstBULocWiseBasicCalBlock;
                }
                List<GLB002> lstRecordableIndicationGLB002 = Manager.GetSubCatagories("Recordable Indication At Reference Gain", BU.Trim(), objClsLoginInfo.Location.Trim()).ToList();
                if (lstBasicCalBlockGLB002.Count > 0)
                {
                    List<BULocWiseCategoryModel> lstBULocWiseRecordableIndication = lstRecordableIndicationGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstRecordableIndication = lstBULocWiseRecordableIndication;
                }

                if (lstSSGLB002.Any() && lstSBGLB002.Any() && lstCouplantGLB002.Any() && lstScanGLB002.Any() && lstBasicCalBlockGLB002.Any() && lstRecordableIndicationGLB002.Any())
                    objProjectDataModel.Key = true;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstCouplantGLB002.Any())
                        lstCategory.Add("Couplant");
                    if (!lstSSGLB002.Any())
                        lstCategory.Add("Scanning Sensitivity");
                    if (!lstScanGLB002.Any())
                        lstCategory.Add("Scan Technique");
                    if (!lstSBGLB002.Any())
                        lstCategory.Add("Simulation Block");
                    if (!lstBasicCalBlockGLB002.Any())
                        lstCategory.Add("Basic Calibration Block");
                    if (!lstRecordableIndicationGLB002.Any())
                        lstCategory.Add("Recordable Indication At Reference Gain");
                    objProjectDataModel.Key = false;
                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
                if (objProjectDataModel.Key)
                {
                    List<string> lstCategoryCode = new List<string>();
                    string cellulos = clsImplementationEnum.ScanUTR1Changes.Cellulose_Water.GetStringValue();
                    if (!lstCouplantGLB002.Any(x => x.Description.Trim() == cellulos.Trim()))
                    {
                        lstCategoryCode.Add(cellulos);
                        //objProjectDataModel.CodeValue = cellulos + " in Couplant not exist for selected project/ Quality Project, Please contact Admin"; 
                    }
                    else
                    {
                        var category1 = lstCouplantGLB002.Where(x => x.Description.Trim() == cellulos.Trim()).FirstOrDefault();
                        objProjectDataModel.catCode = category1.Code;
                        objProjectDataModel.catDesc = category1.Description;
                    }
                    string Ref_06_dB = clsImplementationEnum.ScanUTR1Changes.Ref_06_dB.GetStringValue();
                    if (!lstSSGLB002.Any(x => x.Description.Trim() == Ref_06_dB.Trim()))
                    {
                        lstCategoryCode.Add(Ref_06_dB);
                        //objProjectDataModel.CodeValue = Ref_06_dB + " in Scanning sensitivity not exist for selected project/ Quality Project, Please contact Admin";
                    }
                    else
                    {
                        var category = lstSSGLB002.Where(x => x.Description.Trim() == Ref_06_dB.Trim()).FirstOrDefault();
                        objProjectDataModel.SScatCode = category.Code;
                        objProjectDataModel.SScatDesc = category.Description;
                    }

                    if (lstCategoryCode.Count > 0)
                    {
                        objProjectDataModel.IsCodeExists = true;
                    }
                }

                var UTNum = MakeAScanUT(qms.Trim(), location.Trim());
                objProjectDataModel.TechNo = UTNum;
                objProjectDataModel.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == qms).Select(x => x.ManufacturingCode).FirstOrDefault();

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public string MakeDisplayButton(int HeaderID, string status, int revision, bool viewTimeLine = false, bool viewHistory = false, bool isHistory = false)
        {
            string strButton = string.Empty;
            strButton = "<center style='white-space: nowrap;cursor:pointer;margin-left:5px;' class='clsDisplayInlineEle'><a title='View' href='"+ WebsiteURL + "/NDE/MaintainScanUTTech/AddHeader?HeaderID=" + HeaderID + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a>";
            if (status == IEMQSImplementation.clsImplementationEnum.NDETechniqueStatus.Draft.ToString() && revision == 0)
            {
                strButton += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader('" + HeaderID + "')\"></i>";
            }
            else
            {
                strButton += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;opacity:0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" ></i>";
            }
            if (viewTimeLine)
            {
                strButton += "<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainScanUTTech/ShowTimeline?HeaderID=" + Convert.ToInt32(HeaderID) + "&isHistory=" + isHistory + "');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>";
            }
            if (viewHistory)
            {
                if (revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButton += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderID) + "'); style = \"margin-left:5px;cursor:pointer;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButton += "<i title=\"History\" style = \"margin-left:5px;opacity:0.5;cursor:pointer;\" class=\"fa fa-history\"></i>";
                }
            }
            strButton += "</center>";
            return strButton;
        }

        public class BUWiseDropdown : ProjectDataModel
        {
            public string Project { get; set; }
            public string TechNo { get; set; }
            public new string ManufacturingCode { get; set; }
            public bool Key { get; set; }
            public bool IsCodeExists { get; set; }
            public string CodeValue { get; set; }
            public string Value { get; set; }
            public string catCode { get; set; }
            public string catDesc { get; set; }
            public string SScatCode { get; set; }
            public string SScatDesc { get; set; }
            public List<BULocWiseCategoryModel> lstScanTech { get; set; }
            public List<BULocWiseCategoryModel> lstCouplantUsed { get; set; }
            public List<BULocWiseCategoryModel> lstSimulationBlock1 { get; set; }
            public List<BULocWiseCategoryModel> lstScanningSensitivity { get; set; }
            public List<BULocWiseCategoryModel> lstBasicCalibrationBlock { get; set; }
            public List<BULocWiseCategoryModel> lstRecordableIndication { get; set; }
        }
        #endregion

        #region  Not in used/Old code
        //save header
        [HttpPost]
        public ActionResult SaveHeader(NDE010 nde010, FormCollection fc, HttpPostedFileBase fileScanPlan)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                bool d1;
                if (fc["DGSDiagram"].ToString() == "Yes")
                {
                    d1 = true;
                }
                else
                {
                    d1 = false;
                }

                if (nde010.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde010.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    NDE010 objNDE010 = db.NDE010.Where(x => x.HeaderId == nde010.HeaderId).FirstOrDefault();

                    if (objNDE010.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE010.RevNo = Convert.ToInt32(objNDE010.RevNo) + 1;
                        objNDE010.ReturnRemarks = null;
                        objNDE010.ReturnedBy = null;
                        objNDE010.ReturnedOn = null;
                        objNDE010.SubmittedBy = null;
                        objNDE010.SubmittedOn = null;
                        objNDE010.ApprovedOn = null;
                        objNDE010.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    objNDE010.UTTechNo = nde010.UTTechNo;
                    objNDE010.UTProcedureNo = nde010.UTProcedureNo;
                    objNDE010.RecordableIndication = nde010.RecordableIndication;
                    objNDE010.ScanTechnique = nde010.ScanTechnique;
                    objNDE010.DGSDiagram = d1;
                    objNDE010.BasicCalibrationBlock = nde010.BasicCalibrationBlock;
                    objNDE010.SimulationBlock1 = nde010.SimulationBlock1;
                    objNDE010.SimulationBlock2 = nde010.SimulationBlock2;
                    objNDE010.CouplantUsed = nde010.CouplantUsed;
                    if (fileScanPlan != null && fileScanPlan.ContentLength > 0)
                    {
                        string fname = fileScanPlan.FileName;
                        objNDE010.ScanPlan = fname;
                    }
                    objNDE010.ScanningSensitivity = nde010.ScanningSensitivity;
                    objNDE010.PlannerRemarks = nde010.PlannerRemarks;
                    objNDE010.EditedBy = objClsLoginInfo.UserName;
                    objNDE010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE010.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                    objResponseMsg.Remarks = objNDE010.ReturnRemarks;
                }
                else
                {
                    NDE010 objNDE010 = new NDE010();
                    string doc = nde010.UTTechNo.Split('/')[2];
                    int docNo = Convert.ToInt32(doc);
                    objNDE010.QualityProject = nde010.QualityProject;
                    objNDE010.Project = nde010.Project.Split('-')[0].ToString();
                    objNDE010.BU = nde010.BU.Split('-')[0].ToString();
                    objNDE010.Location = nde010.Location.Split('-')[0].ToString();
                    objNDE010.RevNo = 0;
                    objNDE010.UTTechNo = nde010.UTTechNo;
                    objNDE010.DocNo = docNo;
                    objNDE010.Status = nde010.Status;
                    objNDE010.UTProcedureNo = nde010.UTProcedureNo;
                    objNDE010.RecordableIndication = nde010.RecordableIndication;
                    objNDE010.ScanTechnique = nde010.ScanTechnique;
                    objNDE010.DGSDiagram = d1;
                    objNDE010.BasicCalibrationBlock = nde010.BasicCalibrationBlock;
                    objNDE010.SimulationBlock1 = nde010.SimulationBlock1;
                    objNDE010.SimulationBlock2 = nde010.SimulationBlock2;
                    objNDE010.CouplantUsed = nde010.CouplantUsed;
                    if (fileScanPlan != null && fileScanPlan.ContentLength > 0)
                    {
                        string fname = fileScanPlan.FileName;
                        objNDE010.ScanPlan = fname;
                    }
                    objNDE010.ScanningSensitivity = nde010.ScanningSensitivity;
                    objNDE010.PlannerRemarks = nde010.PlannerRemarks;
                    objNDE010.CreatedBy = objClsLoginInfo.UserName;
                    objNDE010.CreatedOn = DateTime.Now;
                    db.NDE010.Add(objNDE010);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE010.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString();
                    objResponseMsg.Remarks = objNDE010.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}