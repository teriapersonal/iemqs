﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainRTTechniqueController : clsBase
    {
        // GET: NDE/MaintainRTTechnique
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRTHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetRTHeaderGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde9.BU", "nde9.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde9.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(nde9.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(nde9.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' or nde9.QualityProject like '%" + param.sSearch + "%' or (nde9.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or TechniqueSheetNo like '%" + param.sSearch + "%'  or RTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or ecom3.t_psno+'-'+ecom3.t_name like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_GetRTTechniqueHeader(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.RTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center style='display:inline;'><a href='"+WebsiteURL+"/NDE/MaintainRTTechnique/AddRTTechnique?headerId=" + a.HeaderId + "' title='View' style='cursor:pointer;' name='btnAction' id='btnEdit'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+HTMLActionString(a.HeaderId,"","btnDelete","Delete Record","fa fa-trash-o","DeleteHeader(" + a.HeaderId + ");",!(a.RevNo ==0 && a.Status.Equals(clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue())))+"<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTTechnique/ShowTimeline?HeaderID="+Convert.ToInt32(a.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"+HTMLActionString(a.HeaderId,"","btnHistory","History","fa fa-history","History(" + a.HeaderId + ");",!(a.RevNo > 0 || a.Status.Equals(clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())))+"</center>",
                //Convert.ToString(a.HeaderId),
                //a.Status,
            };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region RT Technique Header

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult AddRTTechnique(int? headerId)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE009");
            string user = objClsLoginInfo.UserName;

            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();

            var location = db.COM003.Where(i => i.t_psno.Equals(objClsLoginInfo.UserName) && i.t_actv == 1).Select(i => i.t_loca).FirstOrDefault();
            var Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();

            NDE009 objNDE009 = new NDE009();
            NDEModels objNDEModels = new NDEModels();

            if (headerId > 0)
            {
                objNDE009 = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();

                ViewBag.QMSProject = objNDEModels.GetQMSProject(objNDE009.QualityProject).QualityProject;
                ViewBag.Location = Location;
                ViewBag.MfgCode = objNDEModels.GetQMSProject(objNDE009.QualityProject).ManufacturingCode;
                ViewBag.Technique = objNDEModels.GetCategory("RT Technique", objNDE009.Technique, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.SourceOfRadiation = objNDEModels.GetCategory("Source of Radiation", objNDE009.SourceofRadiation, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.Film = objNDEModels.GetCategory("Film", objNDE009.Film, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.IQI = objNDEModels.GetCategory("IQI", objNDE009.IQI, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                //ViewBag.ExposureDetail = objNDEModels.GetCategory("Exposure Detail", objNDE009.ExposureDetail, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.Weld = objNDEModels.GetCategory("Weld", objNDE009.Weld, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                //ViewBag.GammaRaySource = objNDEModels.GetCategory("Gamma Ray Source", objNDE009.GammaRaySource, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.IntensifyingScreens = objNDEModels.GetCategory("Intensifying Screens", objNDE009.IntensifyingScreen, objNDE009.BU, objNDE009.Location, true).CategoryDescription;

                //ViewBag.IQIHoleType = objNDEModels.GetCategory("IQI Hole Type", objNDE009.IQIHoleType, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                //ViewBag.IQIWireType = objNDEModels.GetCategory("IQI Wire Type", objNDE009.IQIWireType, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.FilmBrand = objNDEModels.GetCategory("Film Branding", objNDE009.FilmBrand, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.FilmViewing = objNDEModels.GetCategory("Film Viewing", objNDE009.FilmViewing, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
            }
            else
            {
                objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                objNDE009.RevNo = 0;

                ViewBag.QMSProject = new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc");
                ViewBag.Location = Location;
                //ViewBag.SourceOfRadiation = new SelectList(objNDEModels.GetCategoryData(db.GLB001.Where(i => i.Category.Equals("Source of Radiation", StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault()), "Id", "CategoryDescription");
            }

            return View(objNDE009);
        }

        [HttpPost]
        public ActionResult SaveHeader(NDE009 obj)
        {
            bool isRevised = false;
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE009 objNDE009 = new NDE009();
            try
            {
                if (obj != null)
                {
                    int headerId = Convert.ToInt32(obj.HeaderId);
                    if (headerId > 0)
                    {
                        if (!IsapplicableForSave(obj.HeaderId))
                        {
                            objResponseMsgWithStatus.Key = false;
                            objResponseMsgWithStatus.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
                        }
                        #region RT Technique Header Update

                        objNDE009 = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();


                        if (string.Equals(objNDE009.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                        {
                            objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                            objNDE009.ReturnRemarks = null;
                            objNDE009.ReturnedBy = null;
                            objNDE009.ReturnedOn = null;
                            objNDE009.SubmittedBy = null;
                            objNDE009.SubmittedOn = null;
                            objNDE009.ApprovedOn = null;
                            isRevised = true;
                            objNDE009.RevNo = objNDE009.RevNo + 1;
                        }
                        objNDE009.RTProcedureNo = obj.RTProcedureNo;
                        objNDE009.TechniqueSheetNo = obj.TechniqueSheetNo;
                        objNDE009.RTTechNo = obj.RTTechNo;
                        objNDE009.DocNo = obj.DocNo;
                        objNDE009.Technique = obj.Technique;
                        objNDE009.SourceofRadiation = obj.SourceofRadiation;
                        objNDE009.Film = obj.Film;
                        objNDE009.IQI = obj.IQI;
                       // objNDE009.ExposureDetail = obj.ExposureDetail;
                        objNDE009.Weld = obj.Weld;
                        objNDE009.WeldReinforcement = obj.WeldReinforcement;
                      //  objNDE009.GammaRaySource = obj.GammaRaySource;
                        objNDE009.Activity = obj.Activity;
                        objNDE009.DensityRange = obj.DensityRange;
                        objNDE009.mA = obj.mA;
                        objNDE009.Kvp = obj.Kvp;
                        objNDE009.FocalSpotSize = obj.FocalSpotSize;
                        objNDE009.SFD = obj.SFD;
                        objNDE009.ExposureTime = obj.ExposureTime;
                        objNDE009.BeamAngle = obj.BeamAngle;
                        objNDE009.IntensifyingScreen = obj.IntensifyingScreen;
                        objNDE009.IQIHoleType = obj.IQIHoleType;
                        objNDE009.IQIWireType = obj.IQIWireType;
                        objNDE009.ShimThickness = obj.ShimThickness;
                        objNDE009.FilmBrand = obj.FilmBrand;
                        objNDE009.NoofFilmPerCassettes = obj.NoofFilmPerCassettes;
                        objNDE009.NoofExposure = obj.NoofExposure;
                        objNDE009.FilmViewing = obj.FilmViewing;
                        objNDE009.LeadLetter = obj.LeadLetter;
                        objNDE009.PlannerRemarks = obj.PlannerRemarks;
                        objNDE009.WireDia = obj.WireDia;
                        objNDE009.EditedBy = objClsLoginInfo.UserName;
                        objNDE009.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        //var folderPath = "NDE009/" + objNDE009.HeaderId + "/R" + objNDE009.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE009.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE009.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE009.Status;
                        objResponseMsgWithStatus.Remarks = objNDE009.ReturnRemarks;
                        #endregion
                    }
                    else
                    {
                        #region RT Technique New Header

                        objNDE009.QualityProject = obj.QualityProject;
                        objNDE009.Project = obj.Project.Split('-')[0];
                        objNDE009.BU = obj.BU.Split('-')[0];
                        objNDE009.Location = obj.Location.Split('-')[0];
                        objNDE009.RevNo = 0;
                        objNDE009.RTTechNo = obj.RTTechNo;
                        objNDE009.DocNo = obj.DocNo;
                        objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objNDE009.RTProcedureNo = obj.RTProcedureNo;
                        objNDE009.TechniqueSheetNo = obj.TechniqueSheetNo;
                        objNDE009.Technique = obj.Technique;
                        objNDE009.SourceofRadiation = obj.SourceofRadiation;
                        objNDE009.Film = obj.Film;
                        objNDE009.IQI = obj.IQI;
                     //   objNDE009.ExposureDetail = obj.ExposureDetail;
                        objNDE009.Weld = obj.Weld;
                        objNDE009.WeldReinforcement = obj.WeldReinforcement;
                     //   objNDE009.GammaRaySource = obj.GammaRaySource;
                        objNDE009.Activity = obj.Activity;
                        objNDE009.DensityRange = obj.DensityRange;
                        objNDE009.mA = obj.mA;
                        objNDE009.Kvp = obj.Kvp;
                        objNDE009.FocalSpotSize = obj.FocalSpotSize;
                        objNDE009.SFD = obj.SFD;
                        objNDE009.ExposureTime = obj.ExposureTime;
                        objNDE009.BeamAngle = obj.BeamAngle;
                        objNDE009.IntensifyingScreen = obj.IntensifyingScreen;
                        objNDE009.IQIHoleType = obj.IQIHoleType;
                        objNDE009.IQIWireType = obj.IQIWireType;
                        objNDE009.ShimThickness = obj.ShimThickness;
                        objNDE009.FilmBrand = obj.FilmBrand;
                        objNDE009.NoofFilmPerCassettes = obj.NoofFilmPerCassettes;
                        objNDE009.NoofExposure = obj.NoofExposure;
                        objNDE009.FilmViewing = obj.FilmViewing;
                        objNDE009.LeadLetter = obj.LeadLetter;
                        objNDE009.PlannerRemarks = obj.PlannerRemarks;
                        objNDE009.WireDia = obj.WireDia;
                        objNDE009.CreatedBy = objClsLoginInfo.UserName;
                        objNDE009.CreatedOn = DateTime.Now;

                        db.NDE009.Add(objNDE009);
                        db.SaveChanges();
                        //var folderPath = "NDE009/" + objNDE009.HeaderId + "/R" + objNDE009.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE009.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE009.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE009.Status;

                        #endregion
                    }
                }
                if (isRevised)
                {
                    var oldFolderPath = "NDE009/" + objNDE009.HeaderId + "/R" + (objNDE009.RevNo - 1);
                    var newFolderPath = "NDE009/" + objNDE009.HeaderId + "/R" + (objNDE009.RevNo);
                    ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                    (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, newFolderPath);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }


            return Json(objResponseMsgWithStatus);
        }

        [HttpPost]
        public ActionResult SaveHeader_old(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE009 objNDE009 = new NDE009();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderID"]);
                    if (headerId > 0)
                    {
                        #region RT Technique Header Update

                        objNDE009 = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();


                        if (string.Equals(objNDE009.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                        {
                            objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                            objNDE009.ReturnRemarks = null;
                            objNDE009.ReturnedBy = null;
                            objNDE009.ReturnedOn = null;
                            objNDE009.SubmittedBy = null;
                            objNDE009.SubmittedOn = null;
                            objNDE009.ApprovedOn = null;

                            objNDE009.RevNo = objNDE009.RevNo + 1;
                        }
                        objNDE009.RTProcedureNo = fc["RTProcedureNo"];
                        objNDE009.TechniqueSheetNo = fc["TechniqueSheetNo"];
                        objNDE009.RTTechNo = fc["RTTechNo"];
                        objNDE009.DocNo = Convert.ToInt32(fc["DocNo"]);
                        objNDE009.Technique = fc["Technique"];
                        objNDE009.SourceofRadiation = fc["SourceofRadiation"];
                        objNDE009.Film = fc["Film"];
                        objNDE009.IQI = fc["IQI"];
                      //  objNDE009.ExposureDetail = fc["ExposureDetail"];
                        objNDE009.Weld = fc["Weld"];
                        objNDE009.WeldReinforcement = Convert.ToInt32(fc["WeldReinforcement"]);
                      //  objNDE009.GammaRaySource = fc["GammaRaySource"];
                        objNDE009.Activity = Convert.ToInt32(fc["Activity"]);
                        objNDE009.DensityRange = fc["DensityRange"];
                        objNDE009.mA = Convert.ToInt32(fc["mA"]);
                        objNDE009.Kvp = Convert.ToInt32(fc["Kvp"]);
                        objNDE009.FocalSpotSize = fc["FocalSpotSize"];
                        objNDE009.SFD = Convert.ToInt32(fc["SFD"]);
                        objNDE009.ExposureTime = fc["ExposureTime"];
                        objNDE009.BeamAngle = fc["BeamAngle"];
                        objNDE009.IntensifyingScreen = fc["IntensifyingScreen"];
                        objNDE009.IQIHoleType = fc["IQIHoleType"];
                        objNDE009.IQIWireType = fc["IQIWireType"];
                        objNDE009.ShimThickness = fc["ShimThickness"];
                        objNDE009.FilmBrand = fc["FilmBrand"];
                        objNDE009.NoofFilmPerCassettes = Convert.ToInt32(fc["NoofFilmPerCassettes"]);
                        objNDE009.NoofExposure = Convert.ToInt32(fc["NoofExposure"]);
                        objNDE009.FilmViewing = fc["FilmViewing"];
                        objNDE009.LeadLetter = fc["LeadLetter"];
                        objNDE009.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE009.WireDia = Convert.ToDecimal(fc["WireDia"]);
                        objNDE009.EditedBy = objClsLoginInfo.UserName;
                        objNDE009.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE009.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE009.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE009.Status;
                        objResponseMsgWithStatus.Remarks = objNDE009.ReturnRemarks;
                        #endregion
                    }
                    else
                    {
                        #region RT Technique New Header

                        objNDE009.QualityProject = fc["QualityProject"];
                        objNDE009.Project = fc["Project"].Split('-')[0];
                        objNDE009.BU = fc["BU"].Split('-')[0];
                        objNDE009.Location = fc["Location"].Split('-')[0];
                        objNDE009.RevNo = 0;
                        objNDE009.RTTechNo = fc["RTTechNo"];
                        objNDE009.DocNo = Convert.ToInt32(fc["DocNo"]);
                        objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objNDE009.RTProcedureNo = fc["RTProcedureNo"];
                        objNDE009.TechniqueSheetNo = fc["TechniqueSheetNo"];
                        objNDE009.Technique = fc["Technique"];
                        objNDE009.SourceofRadiation = fc["SourceofRadiation"];
                        objNDE009.Film = fc["Film"];
                        objNDE009.IQI = fc["IQI"];
                       // objNDE009.ExposureDetail = fc["ExposureDetail"];
                        objNDE009.Weld = fc["Weld"];
                        objNDE009.WeldReinforcement = Convert.ToInt32(fc["WeldReinforcement"]);
                       // objNDE009.GammaRaySource = fc["GammaRaySource"];
                        objNDE009.Activity = Convert.ToInt32(fc["Activity"]);
                        objNDE009.DensityRange = fc["DensityRange"];
                        objNDE009.mA = Convert.ToInt32(fc["mA"]);
                        objNDE009.Kvp = Convert.ToInt32(fc["Kvp"]);
                        objNDE009.FocalSpotSize = fc["FocalSpotSize"];
                        objNDE009.SFD = Convert.ToInt32(fc["SFD"]);
                        objNDE009.ExposureTime = fc["ExposureTime"];
                        objNDE009.BeamAngle = fc["BeamAngle"];
                        objNDE009.IntensifyingScreen = fc["IntensifyingScreen"];
                        objNDE009.IQIHoleType = fc["IQIHoleType"];
                        objNDE009.IQIWireType = fc["IQIWireType"];
                        objNDE009.ShimThickness = fc["ShimThickness"];
                        objNDE009.FilmBrand = fc["FilmBrand"];
                        objNDE009.NoofFilmPerCassettes = Convert.ToInt32(fc["NoofFilmPerCassettes"]);
                        objNDE009.NoofExposure = Convert.ToInt32(fc["NoofExposure"]);
                        objNDE009.FilmViewing = fc["FilmViewing"];
                        objNDE009.LeadLetter = fc["LeadLetter"];
                        objNDE009.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE009.WireDia = Convert.ToDecimal(fc["WireDia"]);
                        objNDE009.CreatedBy = objClsLoginInfo.UserName;
                        objNDE009.CreatedOn = DateTime.Now;

                        db.NDE009.Add(objNDE009);
                        db.SaveChanges();

                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE009.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE009.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE009.Status;

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }


            return Json(objResponseMsgWithStatus);
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE009 objNDE009 = new NDE009();
            try
            {
                if (headerId > 0)
                {
                    objNDE009 = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE009 != null)
                    {
                        if (string.Equals(objNDE009.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) && objNDE009.RevNo == 0)
                        {
                            db.NDE009.Remove(objNDE009);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Technique Deleted Successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Technique Cannot Deleted";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Technique Do Not Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region RT Technique Send For Approval
        [HttpPost]
        public ActionResult SendForApproval(int headerID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
           
            NDE009 objNDE009 = new NDE009();

            if (headerID > 0)
            {
                if (!IsapplicableForSubmit(headerID))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                objNDE009 = db.NDE009.Where(i => i.HeaderId == headerID).FirstOrDefault();
                if (objNDE009 != null)
                {
                    objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                    objNDE009.SubmittedBy = objClsLoginInfo.UserName;
                    objNDE009.SubmittedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE009.Project, objNDE009.BU, objNDE009.Location, "Technique No: " + objNDE009.RTTechNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveRTTechnique/ViewRTTechnique?headerID=" + objNDE009.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                }
            }



            return Json(objResponseMsg);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE009 objNDE009 = db.NDE009.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE009.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE009.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private bool IsapplicableForSave(int headerId)
        {
            NDE009 objNDE009 = db.NDE009.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE009.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region RT Technique Copy
        [HttpPost]
        public ActionResult CopyRTTechniquePartial()
        {
            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();
            ViewBag.QMSProject = new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyNDETechniquePartial.cshtml");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destQMSProject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            NDE009 objNDE009Src = new NDE009();
            NDE009 objNDE009Dest = new NDE009();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                if (headerId > 0)
                {
                    objNDE009Src = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();
                }
                #region OldCode
                //if (!string.IsNullOrEmpty(destQMSProject))
                //{
                //    objNDE009Dest = db.NDE009.Where(i => i.QualityProject.Equals(destQMSProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                //    if (objNDE009Dest != null)
                //    {
                //        if (string.Equals(objNDE009Dest.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue()))
                //        {
                //            objResponseMsg = InsertOrUpdateRTTechnique(objNDE009Src, objNDE009Dest, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "You cannot copy to " + destQMSProject + " as it is in " + objNDE009Dest.Status + " Staus";
                //        }
                //    }
                //    else
                //    {
                //        string project = new NDEModels().GetQMSProject(destQMSProject).Project;

                //        objNDE009Dest = new NDE009();
                //        objNDE009Dest.QualityProject = destQMSProject;
                //        objNDE009Dest.Project = project;
                //        objNDE009Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                //        objNDE009Dest.Location = currentLoc;

                //        var maxRTTechno = MaxRTTechNo(objNDE009Src.Technique, objNDE009Src.SourceofRadiation, objNDE009Src.Film, objNDE009Src.IQI, currentLoc, destQMSProject);

                //        objNDE009Dest.RTTechNo = maxRTTechno.Method + "/" + maxRTTechno.DocumentNo.ToString("D3");
                //        objNDE009Dest.DocNo = maxRTTechno.DocumentNo;

                //        objResponseMsg = InsertOrUpdateRTTechnique(objNDE009Src, objNDE009Dest, false);
                //    }
                //}
                #endregion

                #region NewCode
                string project = new NDEModels().GetQMSProject(destQMSProject).Project;

                objNDE009Dest = new NDE009();
                objNDE009Dest.QualityProject = destQMSProject;
                objNDE009Dest.Project = project;
                objNDE009Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                objNDE009Dest.Location = currentLoc;

                var maxRTTechno = MaxRTTechNo(objNDE009Src.Technique, objNDE009Src.SourceofRadiation, objNDE009Src.Film, objNDE009Src.IQI, currentLoc, destQMSProject);

                objNDE009Dest.RTTechNo = maxRTTechno.Method + maxRTTechno.DocumentNo.ToString("D3");
                objNDE009Dest.DocNo = maxRTTechno.DocumentNo;

                objResponseMsg = InsertOrUpdateRTTechnique(objNDE009Src, objNDE009Dest, false);
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region RT Technique History
        [HttpPost]
        public ActionResult GetHistoryRTTechniquePartial(int headerId)
        {
            NDE009 objNDE009 = new NDE009();
            if (headerId > 0)
                objNDE009 = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryRTTechniquePartial", objNDE009);
        }

        public ActionResult LoadRTTechniqueHistory(JQueryDataTableParamModel param, int headerId)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde9.BU", "nde9.Location");
                whereCondition += " AND nde9.HeaderId=" + headerId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(nde9.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(nde9.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or nde9.QualityProject like '%" + param.sSearch + "%' or (nde9.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or RTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%'" +
                                            " or ecom3.t_psno+'-'+ecom3.t_name like '%" + param.sSearch + "%')";
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_RTTechnique_GetHistory(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        //Convert.ToString(a.Id),
                        //Convert.ToString(a.HeaderId),
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.RTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center><a href=\""+WebsiteURL+"/NDE/MaintainRTTechnique/ViewHistory?id="+a.Id+"\" target=\"_blank\" title=\"View History\" style=\"cursor:pointer;\" name=\"btnAction\" id=\"btnViewHistory\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTTechnique/ShowTimeline?HeaderID="+Convert.ToInt32(a.Id)+"&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                        //a.EditedBy
                        //a.Status,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult ViewHistory(int id)
        {
            NDE009_Log objNDE009 = new NDE009_Log();
            if (id > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objNDE009 = db.NDE009_Log.Where(i => i.Id == id).FirstOrDefault();
                var QMSProject = new NDEModels().GetQMSProject(objNDE009.QualityProject);
                ViewBag.QMSProject = QMSProject.QualityProject + " - " + QMSProject.ManufacturingCode;//db.TMP001.Where(i => i.QMSProject == objNDE009.QMSProject).Select(i => i.QMSProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.MfgCode = objNDEModels.GetQMSProject(objNDE009.QualityProject).ManufacturingCode;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE009.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE009.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE009.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault(); ;

                ViewBag.Technique = objNDEModels.GetCategory("RT Technique", objNDE009.Technique, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.SourceOfRadiation = objNDEModels.GetCategory("Source of Radiation", objNDE009.SourceofRadiation, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.Film = objNDEModels.GetCategory("Film", objNDE009.Film, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.IQI = objNDEModels.GetCategory("IQI", objNDE009.IQI, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
             //   ViewBag.ExposureDetail = objNDEModels.GetCategory("Exposure Detail", objNDE009.ExposureDetail, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.Weld = objNDEModels.GetCategory("Weld", objNDE009.Weld, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
            //    ViewBag.GammaRaySource = objNDEModels.GetCategory("Gamma Ray Source", objNDE009.GammaRaySource, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.IntensifyingScreens = objNDEModels.GetCategory("Intensifying Screens", objNDE009.IntensifyingScreen, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                //ViewBag.IQIHoleType = objNDEModels.GetCategory("IQI Hole Type", objNDE009.IQIHoleType, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                //ViewBag.IQIWireType = objNDEModels.GetCategory("IQI Wire Type", objNDE009.IQIWireType, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.FilmBrand = objNDEModels.GetCategory("Film Branding", objNDE009.FilmBrand, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.FilmViewing = objNDEModels.GetCategory("Film Viewing", objNDE009.FilmViewing, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
            }
            return View(objNDE009);
        }
        #endregion

        #region RTTechnique Other Functions

        public ActionResult GetMaxRTTechNo(string technique, string sourceOfRadiation, string film, string iqi, string location, string QMSproject)
        {
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                objMethodResponse = MaxRTTechNo(technique, sourceOfRadiation, film, iqi, location, QMSproject);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message;
            };


            return Json(objMethodResponse);
        }

        [HttpPost]
        public ActionResult GetQMSProjectDetail(string QMSProject, string location)
        {
            ModelCategory objModelCategory = new ModelCategory();
            ModelNDETechnique objModelTechniqueNo = new ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                objModelTechniqueNo = new NDEModels().GetQMSProjectDetail(QMSProject);
            }

            string strBu = string.Empty;
            objModelCategory.project = objModelTechniqueNo.project;
            objModelCategory.BU = objModelTechniqueNo.BU;
            objModelCategory.ManufacturingCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.ManufacturingCode).FirstOrDefault();
            strBu = objModelTechniqueNo.BU.Split('-')[0];

            var lstRTTechnique = Manager.GetSubCatagories("RT Technique", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstSourceOfRadiation = Manager.GetSubCatagories("Source of Radiation", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstFilm = Manager.GetSubCatagories("Film", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstIQI = Manager.GetSubCatagories("IQI", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
          //  var lstExposureDetail = Manager.GetSubCatagories("Exposure Detail", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstWeld = Manager.GetSubCatagories("Weld", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
        //    var lstGammaRaySource = Manager.GetSubCatagories("Gamma Ray Source", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
         //   var lstIQIHoleType = Manager.GetSubCatagories("IQI Hole Type", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
         //   var lstIQIWireType = Manager.GetSubCatagories("IQI Wire Type", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstFilmBrand = Manager.GetSubCatagories("Film Branding", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstFilmViewing = Manager.GetSubCatagories("Film Viewing", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstIntensifyingScreen = Manager.GetSubCatagories("Intensifying Screens", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();


            objModelCategory.Technique = lstRTTechnique.Any() ? lstRTTechnique : null;
            objModelCategory.SourceOfRadiation = lstSourceOfRadiation.Any() ? lstSourceOfRadiation : null;
            objModelCategory.Film = lstFilm.Any() ? lstFilm : null;
            objModelCategory.IQI = lstIQI.Any() ? lstIQI.ToList() : null;
        //    objModelCategory.ExposureDetail = lstExposureDetail.Any() ? lstExposureDetail : null;
            objModelCategory.Weld = lstWeld.Any() ? lstWeld : null;
         //   objModelCategory.GammaRaySource = lstGammaRaySource.Any() ? lstGammaRaySource : null;
            objModelCategory.IntensifyingScreen = lstIntensifyingScreen.Any() ? lstIntensifyingScreen : null;
            //objModelCategory.IQIHoleType = lstIQIHoleType.Any() ? lstIQIHoleType : null;
            //objModelCategory.IQIWireType = lstIQIWireType.Any() ? lstIQIWireType : null;
            objModelCategory.FilmBrand = lstFilmBrand.Any() ? lstFilmBrand : null;
            objModelCategory.FilmViewing = lstFilmViewing.Any() ? lstFilmViewing : null;

            if (lstRTTechnique.Any() && lstSourceOfRadiation.Any() && lstFilm.Any() && lstIQI.Any()  && lstWeld.Any() &&   lstFilmBrand.Any() && lstFilmViewing.Any())
                objModelCategory.Key = true;
            else
            {
                List<string> lstCategory = new List<string>();
                if (!lstRTTechnique.Any())
                    lstCategory.Add("RT Technique");
                if (!lstSourceOfRadiation.Any())
                    lstCategory.Add("Source of Radiation");
                if (!lstFilm.Any())
                    lstCategory.Add("Film");
                if (!lstIQI.Any())
                    lstCategory.Add("IQI");
                //if (!lstExposureDetail.Any())
                //    lstCategory.Add("Exposure Detail");
                if (!lstWeld.Any())
                    lstCategory.Add("Weld");
                //if (!lstGammaRaySource.Any())
                //    lstCategory.Add("Gamma Ray Source");
                //if (!lstIQIHoleType.Any())
                //    lstCategory.Add("IQI Hole Type");
                if (!lstIntensifyingScreen.Any())
                    lstCategory.Add("Intensifying Screens");
                //if (!lstIQIWireType.Any())
                //    lstCategory.Add("IQI Wire Type");
                if (!lstFilmBrand.Any())
                    lstCategory.Add("Film Branding");
                if (!lstFilmViewing.Any())
                    lstCategory.Add("Film Viewing");


                objModelCategory.Key = false;
                objModelCategory.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
            }


            return Json(objModelCategory);
        }

        public clsHelper.ResponseMsgWithStatus InsertOrUpdateRTTechnique(NDE009 objNDE009Src, NDE009 objNDE009Dest, bool isUpdate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (isUpdate)
                {
                    objNDE009Dest.RTProcedureNo = objNDE009Src.RTProcedureNo;
                    objNDE009Dest.TechniqueSheetNo = objNDE009Src.TechniqueSheetNo;
                    objNDE009Dest.Technique = objNDE009Src.Technique;
                    objNDE009Dest.SourceofRadiation = objNDE009Src.SourceofRadiation;
                    objNDE009Dest.Film = objNDE009Src.Film;
                    objNDE009Dest.IQI = objNDE009Src.IQI;
                    objNDE009Dest.ExposureDetail = objNDE009Src.ExposureDetail;
                    objNDE009Dest.Weld = objNDE009Src.Weld;
                    objNDE009Dest.WeldReinforcement = objNDE009Src.WeldReinforcement;
                    objNDE009Dest.GammaRaySource = objNDE009Src.GammaRaySource;
                    objNDE009Dest.Activity = objNDE009Src.Activity;
                    objNDE009Dest.DensityRange = objNDE009Src.DensityRange;
                    objNDE009Dest.mA = objNDE009Src.mA;
                    objNDE009Dest.Kvp = objNDE009Src.Kvp;
                    objNDE009Dest.FocalSpotSize = objNDE009Src.FocalSpotSize;
                    objNDE009Dest.SFD = objNDE009Src.SFD;
                    objNDE009Dest.ExposureTime = objNDE009Src.ExposureTime;
                    objNDE009Dest.BeamAngle = objNDE009Src.BeamAngle;
                    objNDE009Dest.IntensifyingScreen = objNDE009Src.IntensifyingScreen;
                    objNDE009Dest.IQIHoleType = objNDE009Src.IQIHoleType;
                    objNDE009Dest.IQIWireType = objNDE009Src.IQIWireType;
                    objNDE009Dest.ShimThickness = objNDE009Src.ShimThickness;
                    objNDE009Dest.FilmBrand = objNDE009Src.FilmBrand;
                    objNDE009Dest.NoofFilmPerCassettes = objNDE009Src.NoofFilmPerCassettes;
                    objNDE009Dest.NoofExposure = objNDE009Src.NoofExposure;
                    objNDE009Dest.FilmViewing = objNDE009Src.FilmViewing;
                    objNDE009Dest.LeadLetter = objNDE009Src.LeadLetter;
                    objNDE009Dest.PlannerRemarks = objNDE009Src.PlannerRemarks;
                    objNDE009Dest.WireDia = objNDE009Src.WireDia;
                    objNDE009Dest.EditedBy = objClsLoginInfo.UserName;
                    objNDE009Dest.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE009Dest.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objNDE009Dest.RTTechNo;
                    objResponseMsg.Value = "Data Copied Successfully To Document No : " + objResponseMsg.HeaderStatus + ".";
                }
                else
                {
                    objNDE009Dest.TechniqueSheetNo = objNDE009Src.TechniqueSheetNo;
                    objNDE009Dest.RTProcedureNo = objNDE009Src.RTProcedureNo;
                    objNDE009Dest.Technique = objNDE009Src.Technique;
                    objNDE009Dest.SourceofRadiation = objNDE009Src.SourceofRadiation;
                    objNDE009Dest.Film = objNDE009Src.Film;
                    objNDE009Dest.IQI = objNDE009Src.IQI;
                    objNDE009Dest.ExposureDetail = objNDE009Src.ExposureDetail;
                    objNDE009Dest.Weld = objNDE009Src.Weld;
                    objNDE009Dest.WeldReinforcement = objNDE009Src.WeldReinforcement;
                    objNDE009Dest.GammaRaySource = objNDE009Src.GammaRaySource;
                    objNDE009Dest.Activity = objNDE009Src.Activity;
                    objNDE009Dest.DensityRange = objNDE009Src.DensityRange;
                    objNDE009Dest.mA = objNDE009Src.mA;
                    objNDE009Dest.Kvp = objNDE009Src.Kvp;
                    objNDE009Dest.FocalSpotSize = objNDE009Src.FocalSpotSize;
                    objNDE009Dest.SFD = objNDE009Src.SFD;
                    objNDE009Dest.ExposureTime = objNDE009Src.ExposureTime;
                    objNDE009Dest.BeamAngle = objNDE009Src.BeamAngle;
                    objNDE009Dest.IntensifyingScreen = objNDE009Src.IntensifyingScreen;
                    objNDE009Dest.IQIHoleType = objNDE009Src.IQIHoleType;
                    objNDE009Dest.IQIWireType = objNDE009Src.IQIWireType;
                    objNDE009Dest.ShimThickness = objNDE009Src.ShimThickness;
                    objNDE009Dest.FilmBrand = objNDE009Src.FilmBrand;
                    objNDE009Dest.NoofFilmPerCassettes = objNDE009Src.NoofFilmPerCassettes;
                    objNDE009Dest.NoofExposure = objNDE009Src.NoofExposure;
                    objNDE009Dest.FilmViewing = objNDE009Src.FilmViewing;
                    objNDE009Dest.LeadLetter = objNDE009Src.LeadLetter;
                    objNDE009Dest.PlannerRemarks = objNDE009Src.PlannerRemarks;
                    objNDE009Dest.RevNo = 0;
                    objNDE009Dest.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objNDE009Dest.CreatedBy = objClsLoginInfo.UserName;
                    objNDE009Dest.CreatedOn = DateTime.Now;

                    db.NDE009.Add(objNDE009Dest);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE009Dest.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objNDE009Dest.RTTechNo;
                    objResponseMsg.Value = "Data Copied Successfully To Document No : " + objResponseMsg.HeaderStatus + " of " + objNDE009Dest.Project + " Project.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }

        public MethodResponse MaxRTTechNo(string technique, string sourceOfRadiation, string film, string iqi, string location, string QMSproject)
        {
            MethodResponse objMethodResponse = new MethodResponse();

            string techniqueCat, radiationCat, filmCat, iqiCat = string.Empty;//GetSubCategoryName(technique);
            try
            {
                NDEModels objNDEModels = new NDEModels();
                techniqueCat = !string.IsNullOrEmpty(technique) ? objNDEModels.GetCategory(technique).Code : string.Empty;
                radiationCat = !string.IsNullOrEmpty(sourceOfRadiation) ? objNDEModels.GetCategory(sourceOfRadiation).Code : string.Empty;
                filmCat = !string.IsNullOrEmpty(film) ? objNDEModels.GetCategory(film).Code : string.Empty;
                iqiCat = !string.IsNullOrEmpty(iqi) ? objNDEModels.GetCategory(iqi).Code : string.Empty;

                if (isRTTechniqueNoExist(technique, sourceOfRadiation, film, iqi, location, QMSproject))
                {
                    var PTTechniqueDocNo = (from a in db.NDE009
                                            where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase) && a.Technique.Equals(technique, StringComparison.OrdinalIgnoreCase) && a.SourceofRadiation.Equals(sourceOfRadiation, StringComparison.OrdinalIgnoreCase) && a.Film.Equals(film, StringComparison.OrdinalIgnoreCase) && a.IQI.Equals(iqi, StringComparison.OrdinalIgnoreCase)
                                            select a).Max(a => a.DocNo);

                    objMethodResponse.DocumentNo = Convert.ToInt32(PTTechniqueDocNo + 1);
                }
                else
                {
                    objMethodResponse.DocumentNo = 1;
                }
                objMethodResponse.Key = true;
                objMethodResponse.Method = "RT/" + techniqueCat + "/" + radiationCat + "/" + filmCat + "/" + iqiCat + "/";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message;
            };


            return objMethodResponse;
        }

        public string GetSubCategoryName(int id)
        {
            string strsubCategory = string.Empty;
            strsubCategory = db.GLB002.Where(i => i.Id == id).Select(i => i.Code).FirstOrDefault();

            return strsubCategory;
        }

        public bool isRTTechniqueNoExist(string technique, string sourceOfRadiation, string film, string iqi, string location, string QMSproject)
        {
            bool flag = false;
            var lstRTTechnique = (from a in db.NDE009
                                  where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.QualityProject == QMSproject
                                  select a).ToList();
            try
            {
                if (lstRTTechnique.Any())
                {
                    if (!string.IsNullOrEmpty(QMSproject) && !string.IsNullOrEmpty(technique) && !string.IsNullOrEmpty(sourceOfRadiation) && !string.IsNullOrEmpty(film) && !string.IsNullOrEmpty(iqi))
                    {
                        if (lstRTTechnique.Where(i => i.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase) && i.Technique.Equals(technique, StringComparison.OrdinalIgnoreCase) && i.SourceofRadiation.Equals(sourceOfRadiation, StringComparison.OrdinalIgnoreCase) && i.Film.Equals(film, StringComparison.OrdinalIgnoreCase) && i.IQI.Equals(iqi, StringComparison.OrdinalIgnoreCase)).ToList().Any())
                            flag = true;
                        else
                            flag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return flag;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;opacity:0.5;' Title='" + buttonTooltip + "' class='" + className + "'" + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }

        #endregion

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE009_Log objNDE009 = db.NDE009_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "NDETECHNIQUE";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE009.ApprovedBy);
                model.ApprovedOn = objNDE009.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE009.SubmittedBy);
                model.SubmittedOn = objNDE009.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE009.CreatedBy);
                model.CreatedOn = objNDE009.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE009.ReturnedBy);
                model.ReturnedOn = objNDE009.ReturnedOn;

            }
            else
            {
                NDE009 objNDE009 = db.NDE009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "NDETECHNIQUE";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE009.ApprovedBy);
                model.ApprovedOn = objNDE009.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE009.SubmittedBy);
                model.SubmittedOn = objNDE009.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE009.CreatedBy);
                model.CreatedOn = objNDE009.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE009.ReturnedBy);
                model.ReturnedOn = objNDE009.ReturnedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_GetRTTechniqueHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.RTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_NDE_RTTechnique_GetHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.RTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}