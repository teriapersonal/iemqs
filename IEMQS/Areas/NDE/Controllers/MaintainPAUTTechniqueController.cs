﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity.Core.Objects;
using Newtonsoft.Json;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainPAUTTechniqueController : clsBase
    {
        // GET: NDE/MaintainPAUTTechnique
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPAUTHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetPAUTHeaderGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde1.BU", "nde1.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde1.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or nde1.QualityProject like '%" + param.sSearch + "%' or (nde1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or UTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%'" +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%'" +
                                            " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_PAUT_GetHeader(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                            Convert.ToString(a.QualityProject),
                            Convert.ToString(a.Project),
                            Convert.ToString(a.BU),
                            Convert.ToString(a.Location),
                            Convert.ToString(a.TechniqueSheetNo),
                            Convert.ToString(a.UTTechNo),
                            Convert.ToString("R" + a.RevNo),
                            a.CreatedBy,
                            Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            a.Status,
                            MakeDisplayButton(a.HeaderId,a.Status,Convert.ToInt32(a.RevNo),true,true,false)
                        //Convert.ToString(a.HeaderId)                        
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region PAUT Technique Header
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult AddPAUTHeader(int? headerId)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE014");
            NDE014 objNDE014 = new NDE014();
            string user = objClsLoginInfo.UserName;
            NDEModels objNDEModels = new NDEModels();

            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();

            var location = db.COM003.Where(i => i.t_psno.Equals(objClsLoginInfo.UserName) && i.t_actv == 1).Select(i => i.t_loca).FirstOrDefault();
            var Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            List<string> lstEncoderType = clsImplementationEnum.getyesno().ToList();
            List<string> lstScannerType = clsPAUTEnum.getPAUTEnum().ToList();


            if (headerId > 0)
            {
                objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.EncoderType = lstEncoderType.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
                ViewBag.ScannerType = lstScannerType.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
                ViewBag.QMSProject = objNDEModels.GetQMSProject(objNDE014.QualityProject).QualityProject;
                ViewBag.MfgCode = objNDEModels.GetQMSProject(objNDE014.QualityProject).ManufacturingCode;
                ViewBag.Location = Location;
                ViewBag.Couplant = objNDEModels.GetCategory("Couplant", objNDE014.CouplantUsed, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.ReflectorSize = objNDEModels.GetCategory("Reflector Size", objNDE014.ReferenceReflectorSize, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.CableType = objNDEModels.GetCategory("Cable Type", objNDE014.CableType, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.CableLength = objNDEModels.GetCategory("Cable Length", objNDE014.CableLength, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objNDE014.ScanningTechnique, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
            }
            else
            {
                objNDE014.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                objNDE014.RevNo = 0;
                var DefaultEncoderType = lstEncoderType.Where(i => i.Equals(clsPAUTEnum.PAUTEnum.SemiAutomatic.GetStringValue())).FirstOrDefault();//Contains(clsPAUTEnum.PAUTEnum.SemiAutomatic.GetStringValue()); //.Last().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() });
                var DefaultScannerType = lstScannerType.Where(i => i.Equals(clsPAUTEnum.PAUTEnum.SemiAutomatic.GetStringValue())).FirstOrDefault();//Contains(clsPAUTEnum.PAUTEnum.SemiAutomatic.GetStringValue()); //.Last().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() });
                //ViewBag.EncoderType = new SelectList(lstEncoderType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList(), "Text", "Value", DefaultEncoderType);
                //ViewBag.ScannerType = new SelectList(lstScannerType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList(), "Text", "Value", DefaultScannerType);

                ViewBag.EncoderType = lstEncoderType.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();//JsonConvert.SerializeObject(lstEncoderType.AsEnumerable().Select(x=>new CategoryData { Code=x.ToString(),CategoryDescription=x.ToString()})).ToList();
                ViewBag.ScannerType = lstScannerType.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
                objNDE014.EncoderType = DefaultEncoderType;
                objNDE014.ScannerType = DefaultScannerType;
                ViewBag.QMSProject = new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc");
                ViewBag.Location = Location;
            }
            return View(objNDE014);
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc, HttpPostedFileBase fileScanPlan)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE014 objNDE014 = new NDE014();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    
                    if (headerId > 0)
                    {
                        if (!IsapplicableForSave(headerId))
                        {
                            objResponseMsgWithStatus.Key = false;
                            objResponseMsgWithStatus.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
                        }
                        #region PAUTHeader Update
                        objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();

                        objNDE014.UTProcedureNo = fc["UTProcedureNo"];
                        objNDE014.CouplantUsed = fc["CouplantUsed"];
                        objNDE014.ReferenceReflectorSize = fc["ReferenceReflectorSize"];
                        objNDE014.ReferenceReflectorDistance = fc["ReferenceReflectorDistance"];
                        objNDE014.CalibrationBlockID = fc["CalibrationBlockID"];
                        objNDE014.CalibrationBlockIDThickness = Convert.ToDecimal(fc["CalibrationBlockIDThickness"]);
                        objNDE014.SimulationBlockID = fc["SimulationBlockID"];
                        objNDE014.SimulationBlockIDThickness = Convert.ToDecimal(fc["SimulationBlockIDThickness"]);
                        objNDE014.EncoderType = fc["EncoderType"];
                        objNDE014.ScannerType = fc["ScannerType"];
                        objNDE014.ScannerMake = fc["ScannerMake"];
                        objNDE014.CableType = fc["CableType"];
                        objNDE014.CableLength = fc["CableLength"];
                        if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                        {
                            objResponseMsgWithStatus = UpdateHeaderOnceApproved(headerId);

                        }
                        if (fileScanPlan != null && fileScanPlan.ContentLength > 0)
                        {
                            string fname = fileScanPlan.FileName;
                            objNDE014.ScanPlan = fname;
                        }

                        objNDE014.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE014.EditedBy = objClsLoginInfo.UserName;
                        objNDE014.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE014.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                        objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                        #endregion

                    }
                    else
                    {
                        #region PAUT New Header Added
                        objNDE014.QualityProject = fc["QualityProject"];
                        objNDE014.Project = fc["Project"].Split('-')[0];
                        objNDE014.BU = fc["BU"].Split('-')[0];
                        objNDE014.Location = fc["Location"].Split('-')[0];
                        objNDE014.RevNo = 0;
                        objNDE014.UTTechNo = fc["UTTechNo"];
                        objNDE014.DocNo = Convert.ToInt32(fc["DocNo"]);
                        objNDE014.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objNDE014.UTProcedureNo = fc["UTProcedureNo"];
                        objNDE014.CouplantUsed = fc["CouplantUsed"];
                        objNDE014.ReferenceReflectorSize = fc["ReferenceReflectorSize"];
                        objNDE014.ReferenceReflectorDistance = fc["ReferenceReflectorDistance"];
                        objNDE014.CalibrationBlockID = fc["CalibrationBlockID"];
                        objNDE014.CalibrationBlockIDThickness = Convert.ToDecimal(fc["CalibrationBlockIDThickness"]);
                        objNDE014.SimulationBlockID = fc["SimulationBlockID"];
                        objNDE014.SimulationBlockIDThickness = Convert.ToDecimal(fc["SimulationBlockIDThickness"]);
                        objNDE014.EncoderType = fc["EncoderType"];
                        objNDE014.ScannerType = fc["ScannerType"];
                        objNDE014.ScannerMake = fc["ScannerMake"];
                        objNDE014.CableType = fc["CableType"];
                        objNDE014.CableLength = fc["CableLength"];
                        if (fileScanPlan != null && fileScanPlan.ContentLength > 0)
                        {
                            string fname = fileScanPlan.FileName;
                            objNDE014.ScanPlan = fname;
                        }

                        objNDE014.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE014.CreatedBy = objClsLoginInfo.UserName;
                        objNDE014.CreatedOn = DateTime.Now;

                        db.NDE014.Add(objNDE014);
                        db.SaveChanges();

                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE014.HeaderId);
                        objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                        objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }


        [HttpPost]
        public ActionResult SavePAUTHeader(NDE014 nde014)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE014 objNDE014 = new NDE014();
            try
            {
                if (nde014.HeaderId > 0)
                {
                    #region PAUTHeader Update
                    objNDE014 = db.NDE014.Where(i => i.HeaderId == nde014.HeaderId).FirstOrDefault();

                    objNDE014.UTProcedureNo = nde014.UTProcedureNo;
                    objNDE014.CouplantUsed = nde014.CouplantUsed;
                    objNDE014.ReferenceReflectorSize = nde014.ReferenceReflectorSize;
                    objNDE014.ReferenceReflectorDistance = nde014.ReferenceReflectorDistance;
                    objNDE014.CalibrationBlockID = nde014.CalibrationBlockID;
                    objNDE014.CalibrationBlockIDThickness = Convert.ToDecimal(nde014.CalibrationBlockIDThickness);
                    objNDE014.SimulationBlockID = nde014.SimulationBlockID;
                    objNDE014.SimulationBlockIDThickness = Convert.ToDecimal(nde014.SimulationBlockIDThickness);
                    objNDE014.EncoderType = nde014.EncoderType;
                    objNDE014.ScannerType = nde014.ScannerType;
                    objNDE014.ScannerMake = nde014.ScannerMake;
                    objNDE014.CableType = nde014.CableType;
                    objNDE014.CableLength = nde014.CableLength;
                    objNDE014.ScanningTechnique = nde014.ScanningTechnique;
                    objNDE014.TechniqueSheetNo = nde014.TechniqueSheetNo;
                    if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                    {
                        objResponseMsgWithStatus = UpdateHeaderOnceApproved(nde014.HeaderId);
                    }

                    objNDE014.PlannerRemarks = nde014.PlannerRemarks;
                    objNDE014.EditedBy = objClsLoginInfo.UserName;
                    objNDE014.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = Convert.ToString(objNDE014.HeaderId);
                    objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                    objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                    objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                    #endregion

                }
                else
                {
                    #region PAUT New Header Added
                    objNDE014.QualityProject = nde014.QualityProject;
                    objNDE014.Project = nde014.Project.Split('-')[0];
                    objNDE014.BU = nde014.BU.Split('-')[0];
                    objNDE014.Location = nde014.Location.Split('-')[0];
                    objNDE014.RevNo = 0;
                    objNDE014.UTTechNo = nde014.UTTechNo;
                    objNDE014.DocNo = Convert.ToInt32(nde014.DocNo);
                    objNDE014.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objNDE014.UTProcedureNo = nde014.UTProcedureNo;
                    objNDE014.CouplantUsed = nde014.CouplantUsed;
                    objNDE014.ReferenceReflectorSize = nde014.ReferenceReflectorSize;
                    objNDE014.ReferenceReflectorDistance = nde014.ReferenceReflectorDistance;
                    objNDE014.CalibrationBlockID = nde014.CalibrationBlockID;
                    objNDE014.CalibrationBlockIDThickness = Convert.ToDecimal(nde014.CalibrationBlockIDThickness);
                    objNDE014.SimulationBlockID = nde014.SimulationBlockID;
                    objNDE014.SimulationBlockIDThickness = Convert.ToDecimal(nde014.SimulationBlockIDThickness);
                    objNDE014.EncoderType = nde014.EncoderType;
                    objNDE014.ScannerType = nde014.ScannerType;
                    objNDE014.ScannerMake = nde014.ScannerMake;
                    objNDE014.CableType = nde014.CableType;
                    objNDE014.CableLength = nde014.CableLength;
                    objNDE014.TechniqueSheetNo = nde014.TechniqueSheetNo;
                    objNDE014.ScanningTechnique = nde014.ScanningTechnique;
                    objNDE014.PlannerRemarks = nde014.PlannerRemarks;
                    objNDE014.CreatedBy = objClsLoginInfo.UserName;
                    objNDE014.CreatedOn = DateTime.Now;

                    db.NDE014.Add(objNDE014);
                    db.SaveChanges();
                    //var folderPath = "NDE014/" + objNDE014.HeaderId + "/R" + objNDE014.RevNo;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = Convert.ToString(objNDE014.HeaderId);
                    objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                    objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                    objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }

        public JsonResult SaveAttachment(int HeaderId, bool hasAttachments, KeyValuePair<string, string> Attach)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            //bool hasAttach = false;
            int hid = 0;
            try
            {
                //hasAttach = hasAttachments != null ? Convert.ToBoolean(hasAttachments) : false;
                //hid = HeaderId != null ? Convert.ToInt32(HeaderId) : 0;
                if (HeaderId > 0)
                {
                    var folderPath = "NDE014/" + hid.ToString();
                    //Manager.ManageDocuments(folderPath, hasAttach, Attach, objClsLoginInfo.UserName);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE014 objNDE014 = new NDE014();
            try
            {
                if (headerId > 0)
                {
                    objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE014 != null)
                    {
                        if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) && objNDE014.RevNo == 0)
                        {
                            if (db.NDE015.Where(i => i.HeaderId == headerId).Any())
                            {
                                var lineIds = db.NDE015.Where(i => i.HeaderId == headerId).ToList();
                                var subLines = db.NDE016.Where(i => i.HeaderId == headerId).ToList();
                                if (subLines.Any())
                                {
                                    db.NDE016.RemoveRange(subLines);
                                }
                                db.NDE015.RemoveRange(lineIds);
                            }

                            db.NDE014.Remove(objNDE014);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Technique Deleted Successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Technique Cannot Deleted";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Technique Do Not Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PAUT Technique Category
        [HttpPost]
        public ActionResult PAUTTechniqueCategory(int headerId)
        {
            NDE015 objNDE015 = new NDE015();
            NDEModels objNDEModels = new NDEModels();

            objNDE015.ZoneNo = nextZoneNo(headerId);
            objNDE015.HeaderId = headerId;
            var lstSearchUnit = db.NDE017.ToList();
            List<string> lstScanType = clsPAUTEnum.getScanTypeEnum().ToList();
            var DefaultScanType = lstScanType.Where(i => i.Equals(clsPAUTEnum.ScanType.NonParallel.GetStringValue())).FirstOrDefault();
            ViewBag.ScanType = lstScanType.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();//new SelectList(lstScanType.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList(), "Text", "Value", DefaultScanType);

            ViewBag.Probe = new SelectList(db.NDE018.ToList(), "Id", "Probe");
            ViewBag.Wedge = lstSearchUnit.AsEnumerable().Select(x => new CategoryData() { CategoryDescription = x.SearchUnit.ToString(), Value = x.Id.ToString() }).ToList();
            //new SelectList(objNDEModels.GetCategoryData(db.GLB001.Where(i => i.Category.Equals("Wedge", StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault()), "Id", "CategoryDescription");
            // ViewBag.NearRefractedArea = new SelectList(objNDEModels.GetCategoryData(db.GLB001.Where(i => i.Category.Equals("Near Refracted Area", StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault()), "Id", "CategoryDescription");
            ViewBag.DefaultScanType = DefaultScanType;


            return PartialView("_PAUTTechniqueCategory", objNDE015);
        }
        public int nextZoneNo(int headerId)
        {

            int _newZoneNo = 0;
            var maxZoneNo = db.NDE015.Where(x => x.HeaderId == headerId).OrderByDescending(x => x.LineId).Select(x => x.ZoneNo).FirstOrDefault();
            if (maxZoneNo != 0)
            {
                _newZoneNo = maxZoneNo + 1;
            }
            else
            { _newZoneNo = 1; }
            return _newZoneNo;
        }
        public ActionResult LoadCategoryDataTable(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and HeaderId=" + headerId;
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde15.BU", "nde9.Location");
                //if (status.ToUpper() == "PENDING")
                //{
                //    whereCondition += " and nde9.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                //}
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (Probe like '%" + param.sSearch + "%' or Wedge like '%" + param.sSearch + "%' or NearRefractedArea like '%" + param.sSearch + "%' or Skip like '%" + param.sSearch + "%'  or ScanType like '%" + param.sSearch + "%')";
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                // var lstCategory = db.NDE015.Where(i => i.HeaderId == headerId).ToList();//db.SP_NDE_GetRTTechniqueHeader(startIndex, endIndex, "", whereCondition).ToList();

                var lstCategory = db.SP_NDE_PAUT_GetCategories(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstCategory.Count;//lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstCategory
                          select new[] {
                        Convert.ToString(a.LineId),
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.ZoneNo),
                        Convert.ToString(a.Probe),
                        Convert.ToString(db.NDE017.Where(x=> x.Id.ToString() == a.Wedge).Select(x=>x.SearchUnit).FirstOrDefault()),
                        Convert.ToString(db.NDE017.Where(x=> x.Id.ToString() == a.Wedge).Select(x=>x.BeamAngle).FirstOrDefault()),
                        a.Skip,
                        a.ScanType
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveCategory(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE014 objNDE014 = new NDE014();
            NDE015 objNDE015 = new NDE015();

            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    if (headerId > 0)
                    {
                        if (!IsapplicableForSave(headerId))
                        {
                            objResponseMsgWithStatus.Key = false;
                            objResponseMsgWithStatus.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
                        }
                        //if (!IsapplicableForSubmit(headerId))
                        //{
                        //    objResponseMsgWithStatus.Key = false;
                        //    objResponseMsgWithStatus.Value = "This record has been already submitted, Please refresh page.";
                        //    return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
                        //}
                    }
                    
                   
                    if (headerId > 0)
                        objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    int lineId = Convert.ToInt32(fc["LineId"]);
                    int zoneNo = Convert.ToInt32(fc["ZoneNo"]);
                    if (isCategoryExist(objNDE014.Project, objNDE014.BU, objNDE014.Location, objNDE014.RevNo, zoneNo, lineId, headerId))
                    {
                        objResponseMsgWithStatus.Key = false;
                        objResponseMsgWithStatus.Value = "Category Already Exist";
                    }
                    else
                    {
                        if (lineId > 0)
                        {
                            //Category Edit
                            #region Update Existing Category
                            objNDE015 = db.NDE015.Where(i => i.LineId == lineId).FirstOrDefault();

                            objNDE015.Probe = Convert.ToInt32(fc["Probe"]);
                            objNDE015.Wedge = fc["Wedge"];
                            objNDE015.NearRefractedArea = fc["NearRefractedArea"];
                            objNDE015.Skip = fc["Skip"];
                            objNDE015.ScanType = fc["ScanType"];
                            objNDE015.EditedBy = objClsLoginInfo.UserName;
                            objNDE015.EditedOn = DateTime.Now;
                            db.SaveChanges();

                            if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                            {
                                objResponseMsgWithStatus = UpdateHeaderOnceApproved(headerId);
                            }

                            objResponseMsgWithStatus.Key = true;
                            objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                            objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                            objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                            objResponseMsgWithStatus.Value = "Category Updated successfully";
                            #endregion
                        }
                        else
                        {
                            //New Category Add
                            #region Add New Category
                            objNDE015.HeaderId = objNDE014.HeaderId;
                            objNDE015.Project = objNDE014.Project;
                            objNDE015.BU = objNDE014.BU;
                            objNDE015.Location = objNDE014.Location;
                            objNDE015.RevNo = Convert.ToInt32(objNDE014.RevNo);

                            objNDE015.ZoneNo = zoneNo;
                            objNDE015.Probe = Convert.ToInt32(fc["Probe"]);
                            objNDE015.Wedge = fc["Wedge"];
                            objNDE015.NearRefractedArea = fc["NearRefractedArea"];
                            objNDE015.Skip = fc["Skip"];
                            objNDE015.ScanType = fc["ScanType"];
                            objNDE015.CreatedBy = objClsLoginInfo.UserName;
                            objNDE015.CreatedOn = DateTime.Now;

                            db.NDE015.Add(objNDE015);
                            db.SaveChanges();

                            if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                            {
                                objResponseMsgWithStatus = UpdateHeaderOnceApproved(headerId);
                            }

                            objResponseMsgWithStatus.Key = true;
                            objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                            objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                            objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                            objResponseMsgWithStatus.Value = "Category Added successfully";
                            #endregion
                        }
                        objResponseMsgWithStatus.ZoneNo = nextZoneNo(headerId);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }

        public ActionResult EditCategory(int categoryId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            

            NDEModels objNDEModels = new NDEModels();

            var objNDE15res = db.NDE015.Where(i => i.LineId == categoryId && i.HeaderId == headerId).FirstOrDefault();
            var Probe = db.NDE018.Where(i => i.Id == objNDE15res.Probe).Select(i => i.Probe).FirstOrDefault();
            var Wedge = db.NDE017.Where(x => x.Id.ToString() == objNDE15res.Wedge).AsEnumerable().Select(x => new CategoryData() { CategoryDescription = x.SearchUnit.ToString(), Value = x.Id.ToString() }).FirstOrDefault();
            //var Wedge = objNDEModels.GetCategory(objNDE15res.Wedge, true).CategoryDescription;//a.Probe,
            var NearRefractedArea = db.NDE017.Where(x => x.Id.ToString() == objNDE15res.Wedge).Select(x => x.BeamAngle).FirstOrDefault();
            var ScanType = objNDEModels.GetCategory(objNDE15res.ScanType).CategoryDescription;

            if (categoryId > 0 && headerId > 0)
            {
                var objNDE15 = (new ModelNDEPAUTCategory
                {
                    HeaderId = objNDE15res.HeaderId,
                    LineId = objNDE15res.LineId,
                    ZoneNo = objNDE15res.ZoneNo,
                    Probe = Probe,
                    ProbeId = objNDE15res.Probe,
                    Wedge = Wedge.CategoryDescription,
                    WedgeCode = objNDE15res.Wedge,
                    NearRefractedArea = NearRefractedArea,
                    NearRefractedAreaCode = objNDE15res.NearRefractedArea,
                    ScanType = ScanType,
                    Skip = objNDE15res.Skip
                });

                return Json(objNDE15, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteCategory(int lineId, int headerID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            NDE015 objNDE015 = new NDE015();

            try
            {
                if (headerID > 0)
                {
                    if (!IsapplicableForSave(headerID))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    NDE014 objNDE014 = db.NDE014.Where(i => i.HeaderId == headerID).FirstOrDefault();
                    objNDE015 = db.NDE015.Where(i => i.LineId == lineId).FirstOrDefault();

                    if (isHavingSubCategory(lineId))
                    {
                        var lstSubCategory = db.NDE016.Where(i => i.HeaderId == headerID && i.LineId == lineId).ToList();
                        if (lstSubCategory.Any())
                        {
                            db.NDE016.RemoveRange(lstSubCategory);
                            db.SaveChanges();
                        }
                    }
                    db.NDE015.Remove(objNDE015);
                    db.SaveChanges();
                    if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objResponseMsg = UpdateHeaderOnceApproved(headerID);
                        objResponseMsg.Value = "Zone Deleted Successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Zone Deleted Successfully";
                        objResponseMsg.RevNo = Convert.ToString(objNDE014.RevNo);
                        objResponseMsg.ZoneNo = nextZoneNo(headerID);
                        objResponseMsg.HeaderStatus = objNDE014.Status;
                        objResponseMsg.Remarks = objNDE014.ReturnRemarks;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetNearRefractedAngle(string wedgeDescription)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var searchUnit = db.NDE017.Where(x => x.SearchUnit == wedgeDescription).FirstOrDefault();
                if (searchUnit != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = searchUnit.BeamAngle;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString(); ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PAUT Technique Sub-Category
        [HttpPost]
        public ActionResult GetPAUTSubCategroyPartial(int lineId, int headerId, string user)
        {
            NDE016 objNDE016 = new NDE016();
            objNDE016.HeaderId = headerId;
            objNDE016.LineId = lineId;
            objNDE016.TCGIndicationAmplitude = 80;
            objNDE016.AngularIncrementalChange = 1;
            objNDE016.FocalPlane = "NA";
            ViewBag.User = user;
            return PartialView("_PAUTSubCategroyPartial", objNDE016);
        }

        public ActionResult LoadSubCategoryDataTable(JQueryDataTableParamModel param, int refLineId, int refHeaderId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string condition = string.Empty;
                condition = "nde16.LineId = " + refLineId + " AND nde16.HeaderId = " + refHeaderId;
                if (refHeaderId > 0)
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        //condition += " and ( l.MainCategory like '%" + param.sSearch + "%' or Activity like '%" + param.sSearch + "%' or ReferenceDoc like '%" + param.sSearch + "%' or Characteristics like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or VerifyingDoc like '%" + param.sSearch + "%'   or HeaderNotes like '%" + param.sSearch + "%' or FooterNotes like '%" + param.sSearch + "%' or RevisionDesc like '%" + param.sSearch + "%' or Status like '%" + param.sSearch + "%' or sl.CreatedBy like '%" + param.sSearch + "%')";
                    }

                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstsubLines = db.SP_NDE_PAUT_GetSubCategory(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstsubLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from sl in lstsubLines
                          select new[] {

                                Convert.ToString(sl.ROW_NO),
                                //Convert.ToString(sl.ZoneNo),
                                Convert.ToString(sl.GroupNo),
                                sl.Angle,
                                sl.DepthCoverage,
                                Convert.ToString(sl.FocalDepth),
                                Convert.ToString(sl.TCGIndicationAmplitude),
                                Convert.ToString(sl.StartElement),
                                Convert.ToString(sl.NoofElements),
                                Convert.ToString(sl.AngularIncrementalChange),
                                sl.FocalPlane,
                                Convert.ToString(sl.HeaderId),
                                Convert.ToString(sl.LineId),
                                Convert.ToString(sl.SubLineId)
                      };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = 0,
                    iTotalRecords = 0,
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveSubCategory(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE014 objNDE014 = new NDE014();
            NDE015 objNDE015 = new NDE015();
            NDE016 objNDE016 = new NDE016();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);

                    if (headerId > 0)
                    {
                        if (!IsapplicableForSave(headerId))
                        {
                            objResponseMsgWithStatus.Key = false;
                            objResponseMsgWithStatus.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
                        }
                    }
                        
                    int lineId = Convert.ToInt32(fc["LineId"]);
                    int subLineId = Convert.ToInt32(fc["SubLineId"]);

                    if (headerId > 0)
                        objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    if (lineId > 0)
                        objNDE015 = db.NDE015.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();

                    if (isSubCategoryExist(objNDE015.Project, objNDE015.BU, objNDE015.Location, objNDE015.RevNo, objNDE015.ZoneNo, Convert.ToInt32(fc["GroupNo"]), objNDE015.LineId, subLineId))
                    {
                        objResponseMsgWithStatus.Key = false;
                        objResponseMsgWithStatus.Value = "Sub Categroy already exist";

                    }
                    else
                    {
                        if (subLineId > 0)
                        {
                            #region Update PAUT Sub-Category 

                            objNDE016 = db.NDE016.Where(i => i.SubLineId == subLineId).FirstOrDefault();
                            objNDE016.Angle = fc["Angle"];
                            objNDE016.DepthCoverage = fc["DepthCoverage"];
                            objNDE016.FocalDepth = Convert.ToInt32(fc["FocalDepth"]);
                            objNDE016.TCGIndicationAmplitude = Convert.ToInt32(fc["TCGIndicationAmplitude"]);
                            objNDE016.StartElement = Convert.ToInt32(fc["StartElement"]);
                            objNDE016.NoofElements = Convert.ToInt32(fc["NoofElements"]);
                            objNDE016.AngularIncrementalChange = Convert.ToDecimal(fc["AngularIncrementalChange"]);
                            objNDE016.FocalPlane = fc["FocalPlane"];
                            objNDE016.EditedBy = objClsLoginInfo.UserName;
                            objNDE016.EditedOn = DateTime.Now;

                            db.SaveChanges();

                            if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                            {
                                objResponseMsgWithStatus = UpdateHeaderOnceApproved(headerId);
                            }
                            else
                            {
                                objResponseMsgWithStatus.Key = true;
                                objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                                objResponseMsgWithStatus.Value = "Sub Categroy Updated Successfully";
                                objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                            }
                            objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                            #endregion
                        }
                        else
                        {
                            #region  Add New PAUT Sub-Category
                            objNDE016.LineId = objNDE015.LineId;
                            objNDE016.HeaderId = objNDE015.HeaderId;
                            objNDE016.Project = objNDE015.Project;
                            objNDE016.BU = objNDE015.BU;
                            objNDE016.Location = objNDE015.Location;
                            //if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                            //{
                            //    //objNDE014.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                            //    //objNDE014.RevNo = objNDE014.RevNo + 1;
                            //    //objNDE016.RevNo = Convert.ToInt32(objNDE014.RevNo);
                            //    objResponseMsgWithStatus = UpdateHeaderOnceApproved(headerId);
                            //}

                            objNDE016.RevNo = objNDE015.RevNo;
                            objNDE016.ZoneNo = objNDE015.ZoneNo;

                            objNDE016.GroupNo = Convert.ToInt32(fc["GroupNo"]);
                            objNDE016.Angle = fc["Angle"];
                            objNDE016.DepthCoverage = fc["DepthCoverage"];
                            objNDE016.FocalDepth = Convert.ToInt32(fc["FocalDepth"]);
                            objNDE016.TCGIndicationAmplitude = Convert.ToInt32(fc["TCGIndicationAmplitude"]);
                            objNDE016.StartElement = Convert.ToInt32(fc["StartElement"]);
                            objNDE016.NoofElements = Convert.ToInt32(fc["NoofElements"]);
                            objNDE016.AngularIncrementalChange = Convert.ToDecimal(fc["AngularIncrementalChange"]);
                            objNDE016.FocalPlane = fc["FocalPlane"];
                            objNDE016.CreatedBy = objClsLoginInfo.UserName;
                            objNDE016.CreatedOn = DateTime.Now;

                            db.NDE016.Add(objNDE016);
                            db.SaveChanges();

                            if (string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                            {
                                objResponseMsgWithStatus = UpdateHeaderOnceApproved(headerId);
                            }
                            else
                            {
                                objResponseMsgWithStatus.Key = true;
                                objResponseMsgWithStatus.Value = "Sub Categroy saved successfully";
                                objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE014.RevNo);
                                objResponseMsgWithStatus.HeaderStatus = objNDE014.Status;
                            }
                            objResponseMsgWithStatus.Remarks = objNDE014.ReturnRemarks;
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }

        public ActionResult EditSubCategory(int subcategoryId)
        {
            if (subcategoryId > 0)
            {
                var objNDE16 = (from a in db.NDE016
                                where a.SubLineId == subcategoryId
                                select new
                                {
                                    a.HeaderId,
                                    a.LineId,
                                    a.SubLineId,
                                    a.GroupNo,
                                    a.Angle,
                                    a.DepthCoverage,
                                    a.TCGIndicationAmplitude,
                                    a.StartElement,
                                    a.NoofElements,
                                    a.AngularIncrementalChange,
                                    a.FocalDepth,
                                    a.FocalPlane
                                }).FirstOrDefault();

                return Json(objNDE16, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteSubCategory(int subLineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE016 objNDE016 = new NDE016();
            try
            {
                if (!IsapplicableForSubmit(objNDE016.HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (subLineId > 0)
                {
                    objNDE016 = db.NDE016.Where(i => i.SubLineId == subLineId).FirstOrDefault();
                    db.NDE016.Remove(objNDE016);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PAUT Technique Send For Approval
        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE014 objNDE014 = new NDE014();
            List<string> lstCategoryName = new List<string>();
            List<NDE015> lstCategoryNotHavingSubCategory = new List<NDE015>();
            List<NDE016> lstSubCategories = new List<NDE016>();

            try
            {
                if (!IsapplicableForSubmit(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (headerId > 0)
                {
                    objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    if (!string.Equals(objNDE014.Status, clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        var lstCategory = db.NDE015.Where(i => i.HeaderId == objNDE014.HeaderId).ToList();
                        if (lstCategory.Any())
                        {
                            foreach (var item in lstCategory)
                            {
                                var lstSubCategory = db.NDE016.Where(i => i.HeaderId == headerId && i.LineId == item.LineId).ToList();
                                if (lstSubCategory != null && lstSubCategory.Count > 0)
                                {
                                    lstSubCategories.AddRange(lstSubCategory);
                                }
                                else
                                {
                                    //lstCategoryName.Add(item.MainCategory);
                                    lstCategoryNotHavingSubCategory.Add(item);
                                }

                            }

                            if (lstCategoryNotHavingSubCategory.Any())
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Please Add Sub Category ";//For " + string.Join(",", lstCategoryName);
                            }
                            else
                            {
                                objNDE014.Status = clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue();
                                objNDE014.SubmittedBy = objClsLoginInfo.UserName;
                                objNDE014.SubmittedOn = DateTime.Now;
                                db.SaveChanges();

                                #region Send Notification
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE014.Project, objNDE014.BU, objNDE014.Location, "Technique No: " + objNDE014.UTTechNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApprovePAUTTechnique/ViewPAUTTechnique?headerId=" + objNDE014.HeaderId);
                                #endregion

                                //var lstsubcat = lstSubCategories.Where(i => i.Status.Equals(clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.Status.Equals(clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList();
                                //lstSubCategories = lstSubCategories.Except(lstsubcat).ToList();

                                //lstSubCategories.ForEach(x => x.Status = objNDE014.Status);
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "PAUT Document sent successfully  for approval";

                            }

                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Add Main Category For Header";
                        }

                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "PAUT Document has already been sent for approval";
                    }

                }
                else
                { }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE014 objNDE014 = db.NDE014.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE014 != null)
            {
                if (objNDE014.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE014.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }


        private bool IsapplicableForSave(int headerId)
        {
            NDE014 objNDE014 = db.NDE014.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE014 != null)
            {
                if (objNDE014.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
            
        }

        #endregion

        #region PAUT Technique Copy
        [HttpPost]
        public ActionResult CopyPAUTTechniquePartial()
        {
            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();
            ViewBag.QMSProject = new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyNDETechniquePartial.cshtml");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destQMSProject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            NDE014 objNDE0014Src = new NDE014();
            NDE014 objNDE0014Dest = new NDE014();
            SourcePAUTTechnique objSourcePAUTTechnique = new SourcePAUTTechnique();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                if (headerId > 0)
                {
                    objNDE0014Src = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objSourcePAUTTechnique = GetSourcePAUTDetail(headerId);
                }

                #region OldCode
                //if (!string.IsNullOrEmpty(destQMSProject))
                //{
                //    objNDE0014Dest = db.NDE014.Where(i => i.QualityProject.Equals(destQMSProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                //    if (objNDE0014Dest != null)
                //    {
                //        if (string.Equals(objNDE0014Dest.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue()))
                //        {
                //            objResponseMsg = InsertOrUpdatePAUT(objSourcePAUTTechnique, objNDE0014Dest, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "You cannot copy to " + destQMSProject + " as it is in " + objNDE0014Dest.Status + " Staus";
                //        }
                //    }
                //    else
                //    {
                //        string project = new NDEModels().GetQMSProject(destQMSProject).Project;

                //        objNDE0014Dest = new NDE014();
                //        objNDE0014Dest.QualityProject = destQMSProject;
                //        objNDE0014Dest.Project = project;
                //        objNDE0014Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                //        objNDE0014Dest.Location = currentLoc;

                //        var maxUTTechno = MaxUTTechNo(currentLoc, destQMSProject);

                //        objNDE0014Dest.UTTechNo = "UT/PAUT/" + maxUTTechno.DocumentNo.ToString("D3");
                //        objNDE0014Dest.DocNo = maxUTTechno.DocumentNo;

                //        objResponseMsg = InsertOrUpdatePAUT(objSourcePAUTTechnique, objNDE0014Dest, false);
                //    }
                //}
                #endregion

                #region NewCode
                string project = new NDEModels().GetQMSProject(destQMSProject).Project;

                objNDE0014Dest = new NDE014();
                objNDE0014Dest.QualityProject = destQMSProject;
                objNDE0014Dest.Project = project;
                objNDE0014Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                objNDE0014Dest.Location = currentLoc;

                var maxUTTechno = MaxUTTechNo(currentLoc, destQMSProject);

                objNDE0014Dest.UTTechNo = "UT/PAUT/" + maxUTTechno.DocumentNo.ToString("D3");
                objNDE0014Dest.DocNo = maxUTTechno.DocumentNo;

                objResponseMsg = InsertOrUpdatePAUT(objSourcePAUTTechnique, objNDE0014Dest, false);
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PAUT Technique History
        [HttpPost]
        public ActionResult GetHistoryPAUTTechniquePartial(int headerId)
        {
            NDE014 objNDE014 = new NDE014();
            if (headerId > 0)
                objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryPAUTTechniquePartial", objNDE014);
        }

        public ActionResult LoadPAUTTechniqueHistory(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde1.BU", "nde1.Location");
                whereCondition += " AND nde1.HeaderId=" + headerId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or nde1.QualityProject like '%" + param.sSearch + "%' or (nde1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or UTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%'" + " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_PAUT_GetHeaderHistory(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        //
                        //Convert.ToString(a.HeaderId),
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.UTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        a.Status,
                        //a.EditedBy
                        //Convert.ToString(a.Id)
                        "<center><a href=\""+WebsiteURL + "/NDE/MaintainPAUTTechnique/ViewHistory?id=" + a.Id + "\" target=\"_blank\" title=\"View History\" style=\"cursor:pointer;\" name=\"btnAction\" id=\"btnViewHistory\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPAUTTechnique/ShowTimeline?HeaderID=" + Convert.ToInt32(a.Id) + "&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                        //a.Status,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult ViewHistory(int id)
        {
            NDE014_Log objNDE014 = new NDE014_Log();
            if (id > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objNDE014 = db.NDE014_Log.Where(i => i.Id == id).FirstOrDefault();
                var QMSProject = new NDEModels().GetQMSProject(objNDE014.QualityProject);
                ViewBag.QMSProject = QMSProject.QualityProject;// + " - " + QMSProject.ManufacturingCode;
                ViewBag.MfgCode = QMSProject.ManufacturingCode;//db.TMP001.Where(i => i.QMSProject == objNDE014.QMSProject).Select(i => i.QMSProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE014.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE014.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE014.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault(); ;
                ViewBag.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objNDE014.ScanningTechnique, objNDE014.BU, objNDE014.Location, true).CategoryDescription;

                ViewBag.Couplant = objNDEModels.GetCategory("Couplant", objNDE014.CouplantUsed, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.ReflectorSize = objNDEModels.GetCategory("Reflector Size", objNDE014.ReferenceReflectorSize, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.CableType = objNDEModels.GetCategory("Cable Type", objNDE014.CableType, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
                ViewBag.CableLength = objNDEModels.GetCategory("Cable Length", objNDE014.CableLength, objNDE014.BU, objNDE014.Location, true).CategoryDescription;
            }
            return View(objNDE014);
        }

        [HttpPost]
        public ActionResult GetPAUTSubCategroyHistoryPartial(int reflineId, int refheaderId)
        {
            NDE016_Log objNDE016 = new NDE016_Log();
            NDE014_Log objNDE014 = db.NDE014_Log.Where(i => i.Id == refheaderId).FirstOrDefault();
            objNDE016.Id = objNDE014.Id;
            objNDE016.RefId = refheaderId;
            objNDE016.RefLineLogId = reflineId;
            return PartialView("_PAUTSubCategroyHistoryPartial", objNDE016);
        }

        public ActionResult LoadCategoryHistoryDataTable(JQueryDataTableParamModel param, int Id)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and RefId=" + Id;
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde15.BU", "nde9.Location");
                //if (status.ToUpper() == "PENDING")
                //{
                //    whereCondition += " and nde9.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                //}
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (Probe like '%" + param.sSearch + "%' or Wedge like '%" + param.sSearch + "%' or NearRefractedArea like '%" + param.sSearch + "%' or Skip like '%" + param.sSearch + "%'  or ScanType like '%" + param.sSearch + "%')";
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                // var lstCategory = db.NDE015.Where(i => i.HeaderId == headerId).ToList();//db.SP_NDE_GetRTTechniqueHeader(startIndex, endIndex, "", whereCondition).ToList();

                var lstCategory = db.SP_NDE_PAUT_GetCategories_History(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstCategory.Count;//lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstCategory
                          select new[] {
                        Convert.ToString(a.Id),
                        Convert.ToString(a.LineId),
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.ZoneNo),
                        Convert.ToString(a.Probe),
                        Convert.ToString(a.Wedge),
                        Convert.ToString(a.NearRefractedArea),
                        a.Skip,
                        a.ScanType
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadSublinesHistoryData(JQueryDataTableParamModel param, int refLineId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                Func<QCP001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                var sortDirection = Request["sSortDir_0"]; // asc or desc
                string whereCondition = "1=1";
                whereCondition += " and nde16.RefLineLogId in (" + refLineId + ") ";
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSublines = db.SP_NDE_PAUT_GetSubCategoryHistory(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int totalRecord = Convert.ToInt32(lstSublines.Select(x => x.TotalCount).FirstOrDefault());

                var data = (from sl in lstSublines
                            select new[]
                            {
                                Convert.ToString(sl.ROW_NO),
                                Convert.ToString(sl.GroupNo),
                                sl.Angle,
                                sl.DepthCoverage,
                                Convert.ToString(sl.FocalDepth),
                                Convert.ToString(sl.TCGIndicationAmplitude),
                                Convert.ToString(sl.StartElement),
                                Convert.ToString(sl.NoofElements),
                                Convert.ToString(sl.AngularIncrementalChange),
                                sl.FocalPlane,
                                Convert.ToString(sl.HeaderId),
                                Convert.ToString(sl.LineId),
                                Convert.ToString(sl.SubLineId)

                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region PAUT Technique Common Function

        [HttpPost]
        public ActionResult GetMaxPAUTTechNo(string location, string qmsProject)
        {
            MethodResponse objMethodResponse = new MethodResponse();

            try
            {
                objMethodResponse = MaxUTTechNo(location, qmsProject);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message;
            }

            return Json(objMethodResponse);
        }

        [HttpPost]
        public ActionResult GetQMSProjectDetail(string QMSProject, string location)
        {
            ModelCategory objModelCategory = new ModelCategory();
            ModelNDETechnique objModelTechniqueNo = new ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                objModelTechniqueNo = new NDEModels().GetQMSProjectDetail(QMSProject);
            }

            string strBu = string.Empty;
            objModelCategory.project = objModelTechniqueNo.project;
            objModelCategory.BU = objModelTechniqueNo.BU;
            strBu = objModelTechniqueNo.BU.Split('-')[0];

            var lstCouplant = Manager.GetSubCatagories("Couplant", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstReflectorSize = Manager.GetSubCatagories("Reflector Size", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstCableType = Manager.GetSubCatagories("Cable Type", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstCableLength = Manager.GetSubCatagories("Cable Length", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstEncoderType = clsPAUTEnum.getPAUTEnum().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            //    var lstWedge = Manager.GetSubCatagories("Wedge", strBu, location).ToList();
            //  var lstNearRefractedArea = Manager.GetSubCatagories("Near Refracted Area", strBu, location).ToList();
            var lstScanningTechnique = Manager.GetSubCatagories("Scanning Detail", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

            objModelCategory.ManufacturingCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.ManufacturingCode).FirstOrDefault();
            objModelCategory.Couplant = lstCouplant.Any() ? lstCouplant : null;
            objModelCategory.ReflectorSize = lstReflectorSize.Any() ? lstReflectorSize : null;
            objModelCategory.CableType = lstCableType.Any() ? lstCableType : null;
            objModelCategory.CableLength = lstCableLength.Any() ? lstCableLength.ToList() : null;
            objModelCategory.EncoderType = lstEncoderType.Any() ? lstEncoderType.ToList() : null;
            objModelCategory.ScanningTechnique = lstScanningTechnique.Any() ? lstScanningTechnique : null;

            if (lstCouplant.Any() && lstReflectorSize.Any() && lstCableType.Any() && lstCableLength.Any())
            {
                objModelCategory.Key = true;
            }
            else
            {
                List<string> lstCategory = new List<string>();
                if (!lstCouplant.Any())
                    lstCategory.Add("Couplant");
                if (!lstReflectorSize.Any())
                    lstCategory.Add("Reflector Size");
                if (!lstCableType.Any())
                    lstCategory.Add("Cable Type");
                if (!lstCableLength.Any())
                    lstCategory.Add("Cable Length");
                //if (!lstWedge.Any())
                //    lstCategory.Add("Wedge");
                //if (!lstNearRefractedArea.Any())
                //    lstCategory.Add("Near Refracted Area");
                if (!lstScanningTechnique.Any())
                    lstCategory.Add("Scanning Detail");
                objModelCategory.Key = false;
                objModelCategory.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
            }
            if (objModelCategory.Key)
            {
                List<string> lstCategoryCode = new List<string>();
                string cellulos = clsImplementationEnum.TOFDUTR1Changes.Cellulose_Water.GetStringValue();
                if (!lstCouplant.Any(x => x.CategoryDescription.Trim() == cellulos.Trim()))
                {
                    lstCategoryCode.Add(cellulos + " in Couplant");
                }
                else
                {
                    var category = lstCouplant.Where(x => x.CategoryDescription.Trim() == cellulos.Trim()).FirstOrDefault();
                    objModelCategory.celluloscatCode = category.Code;
                    objModelCategory.celluloscatDesc = category.CategoryDescription;
                }
                string Co_Axial = clsImplementationEnum.TOFDUTR1Changes.Co_Axial.GetStringValue();
                if (!lstCableType.Any(x => x.CategoryDescription.Trim() == Co_Axial.Trim()))
                {
                    lstCategoryCode.Add(Co_Axial + " in Cable Type");

                }
                else
                {
                    var category = lstCableType.Where(x => x.CategoryDescription.Trim() == Co_Axial.Trim()).FirstOrDefault();
                    objModelCategory.Co_AxialcatCode = category.Code;
                    objModelCategory.Co_AxialcatDesc = category.CategoryDescription;
                    //    objProjectDataModel.IsCodeExists = true;
                }

                string Semi_Automated = clsImplementationEnum.TOFDUTR1Changes.Semi_Automated.GetStringValue();
                if (!lstScanningTechnique.Any(x => x.CategoryDescription.Trim() == Semi_Automated.Trim()))
                {
                    lstCategoryCode.Add(Semi_Automated + " in Scanning Detail");
                }
                else
                {
                    var category = lstScanningTechnique.Where(x => x.CategoryDescription.Trim() == Semi_Automated.Trim()).FirstOrDefault();
                    objModelCategory.Semi_AutomatedcatCode = category.Code;
                    objModelCategory.Semi_AutomatedcatDesc = category.CategoryDescription;
                }

                string cableLength = clsImplementationEnum.TOFDUTR1Changes.CableLength2.GetStringValue();
                if (!lstCableLength.Any(x => x.CategoryDescription.Trim() == cableLength.Trim()))
                {
                    lstCategoryCode.Add(cableLength + " in Cable Length");
                }
                else
                {
                    var category = lstCableLength.Where(x => x.CategoryDescription.Trim() == cableLength.Trim()).FirstOrDefault();
                    objModelCategory.cabelLengthcatCode = category.Code;
                    objModelCategory.cabelLengthcatDesc = category.CategoryDescription;
                }

                if (lstCategoryCode.Count > 0)
                {
                    objModelCategory.IsCodeExists = true;
                    objModelCategory.CodeValue = string.Format(string.Join(",", lstCategoryCode.ToList()) + "not exist for selected project/ Quality Project, Please contact Admin");
                }
            }


            return Json(objModelCategory);
        }

        [HttpPost]
        public ActionResult GetQMSProjectDetailForCategory(string strBU, string location)
        {
            ModelCategory objModelCategory = new ModelCategory();
            NDEModels objNDEModels = new NDEModels();

            var lstScanType = clsPAUTEnum.getScanTypeEnum().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            var DefaultScanType = lstScanType.Where(i => i.Equals(clsPAUTEnum.ScanType.NonParallel.GetStringValue())).FirstOrDefault();
            ViewBag.ScanType = lstScanType.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            var lstProbe = db.NDE018.Select(x => new CategoryData { Value = x.Id.ToString(), Code = x.Probe, CategoryDescription = x.Probe }).ToList();
            //var lstWedge = Manager.GetSubCatagories("Wedge", strBU, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList(); //new SelectList(objNDEModels.GetCategoryData(db.GLB001.Where(i => i.Category.Equals("Wedge", StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault()), "Id", "CategoryDescription");
            // var lstNearRefractedArea = Manager.GetSubCatagories("Near Refracted Area", strBU, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList(); //new SelectList(objNDEModels.GetCategoryData(db.GLB001.Where(i => i.Category.Equals("Near Refracted Area", StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault()), "Id", "CategoryDescription");
            objModelCategory.Probe = lstProbe.Any() ? lstProbe : null;
            // objModelCategory.Wedge = lstWedge.Any() ? lstWedge : null;
            //  objModelCategory.NearRefractedArea = lstNearRefractedArea.Any() ? lstNearRefractedArea : null;
            objModelCategory.ScanType = lstScanType.Any() ? lstScanType : null;
            return Json(objModelCategory, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProbeDetails(int probeId)
        {
            NDE018 objNDE018 = new NDE018();
            ModelProbeDetail objModelProbeDetail = new ModelProbeDetail();
            if (probeId > 0)
            {
                objNDE018 = db.NDE018.Where(i => i.Id == probeId).FirstOrDefault();
                if (objNDE018 != null)
                {
                    objModelProbeDetail.ProbeId = objNDE018.Id;
                    objModelProbeDetail.ProbeMake = objNDE018.ProbeMake;
                    objModelProbeDetail.Frequency = Convert.ToInt32(objNDE018.Frequency);
                    objModelProbeDetail.ElementSize = Convert.ToDecimal(objNDE018.ElementSize);
                    objModelProbeDetail.ElementPitch = Convert.ToDecimal(objNDE018.ElementPitch);
                    objModelProbeDetail.ElementGapDistance = Convert.ToDecimal(objNDE018.ElementGapDistance);
                    objModelProbeDetail.NoOfElement = Convert.ToInt32(objNDE018.NoOfElement);
                }
            }
            return Json(objModelProbeDetail, JsonRequestBehavior.AllowGet);
        }

        public MethodResponse MaxUTTechNo(string location, string qmsProject)
        {
            MethodResponse objMethodResponse = new MethodResponse();

            try
            {
                if (isPAUTTechniqueNoExist(location, qmsProject))
                {
                    var PAUTTechniqueDocNo = (from a in db.NDE014
                                              where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.QualityProject.Equals(qmsProject, StringComparison.OrdinalIgnoreCase)
                                              select a).Max(a => a.DocNo);

                    objMethodResponse.DocumentNo = Convert.ToInt32(PAUTTechniqueDocNo + 1);
                }
                else
                {
                    objMethodResponse.DocumentNo = 1;
                }
                objMethodResponse.Key = true;
                //objMethodResponse.Method = "RT/" + techniqueCat + "/" + radiationCat + "/" + filmCat + "/" + iqiCat + "/";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message;
            }

            return objMethodResponse;
        }

        public clsHelper.ResponseMsgWithStatus InsertOrUpdatePAUT(SourcePAUTTechnique sourcePAUTTechnique, NDE014 objNDE014Dest, bool isUpdate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var souceHeader = sourcePAUTTechnique.SourceHeader;
            try
            {
                if (isUpdate)
                {
                    var lstExistingSubCategory = db.NDE016.Where(i => i.HeaderId == objNDE014Dest.HeaderId).ToList();
                    if (lstExistingSubCategory.Any())
                    {
                        db.NDE016.RemoveRange(lstExistingSubCategory);
                        db.SaveChanges();
                    }

                    var lstExistingCategory = db.NDE015.Where(i => i.HeaderId == objNDE014Dest.HeaderId).ToList();
                    if (lstExistingCategory.Any())
                    {
                        db.NDE015.RemoveRange(lstExistingCategory);
                        db.SaveChanges();
                    }

                    #region Update Existing Header
                    //objNDE014Dest.UTProcedureNo = souceHeader.UTProcedureNo;
                    objNDE014Dest.CouplantUsed = souceHeader.CouplantUsed;
                    objNDE014Dest.ReferenceReflectorSize = souceHeader.ReferenceReflectorSize;
                    objNDE014Dest.ReferenceReflectorDistance = souceHeader.ReferenceReflectorDistance;
                    objNDE014Dest.CalibrationBlockID = souceHeader.CalibrationBlockID;
                    objNDE014Dest.CalibrationBlockIDThickness = souceHeader.CalibrationBlockIDThickness;
                    objNDE014Dest.TechniqueSheetNo = souceHeader.TechniqueSheetNo;
                    objNDE014Dest.SimulationBlockID = souceHeader.SimulationBlockID;
                    objNDE014Dest.SimulationBlockIDThickness = souceHeader.SimulationBlockIDThickness;
                    objNDE014Dest.EncoderType = souceHeader.EncoderType;
                    objNDE014Dest.ScannerType = souceHeader.ScannerType;
                    objNDE014Dest.ScannerMake = souceHeader.ScannerMake;
                    objNDE014Dest.CableType = souceHeader.CableType;
                    objNDE014Dest.CableLength = souceHeader.CableLength;
                    objNDE014Dest.ScanPlan = souceHeader.ScanPlan;
                    objNDE014Dest.PlannerRemarks = souceHeader.PlannerRemarks;
                    objNDE014Dest.EditedBy = objClsLoginInfo.UserName;
                    objNDE014Dest.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE014Dest.HeaderId;
                    #endregion

                }
                else
                {
                    #region Add New Header
                    objNDE014Dest.UTProcedureNo = souceHeader.UTProcedureNo;
                    objNDE014Dest.CouplantUsed = souceHeader.CouplantUsed;
                    objNDE014Dest.ReferenceReflectorSize = souceHeader.ReferenceReflectorSize;
                    objNDE014Dest.ReferenceReflectorDistance = souceHeader.ReferenceReflectorDistance;
                    objNDE014Dest.CalibrationBlockID = souceHeader.CalibrationBlockID;
                    objNDE014Dest.CalibrationBlockIDThickness = souceHeader.CalibrationBlockIDThickness;
                    objNDE014Dest.TechniqueSheetNo = souceHeader.TechniqueSheetNo;
                    objNDE014Dest.SimulationBlockID = souceHeader.SimulationBlockID;
                    objNDE014Dest.SimulationBlockIDThickness = souceHeader.SimulationBlockIDThickness;
                    objNDE014Dest.EncoderType = souceHeader.EncoderType;
                    objNDE014Dest.ScannerType = souceHeader.ScannerType;
                    objNDE014Dest.ScannerMake = souceHeader.ScannerMake;
                    objNDE014Dest.CableType = souceHeader.CableType;
                    objNDE014Dest.CableLength = souceHeader.CableLength;
                    objNDE014Dest.ScanPlan = souceHeader.ScanPlan;
                    objNDE014Dest.PlannerRemarks = souceHeader.PlannerRemarks;
                    objNDE014Dest.RevNo = 0;
                    objNDE014Dest.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objNDE014Dest.CreatedBy = objClsLoginInfo.UserName;
                    objNDE014Dest.CreatedOn = DateTime.Now;

                    db.NDE014.Add(objNDE014Dest);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE014Dest.HeaderId;
                    #endregion
                }

                #region Copy Category And Sub Category 

                var lstSourceCategory = sourcePAUTTechnique.SourceCategory.Where(i => i.HeaderId == souceHeader.HeaderId).ToList();

                foreach (var item in lstSourceCategory)
                {
                    NDE015 objNDE015 = new NDE015();
                    objNDE015.HeaderId = objNDE014Dest.HeaderId;
                    objNDE015.Project = objNDE014Dest.Project;
                    objNDE015.BU = objNDE014Dest.BU;
                    objNDE015.Location = objNDE014Dest.Location;
                    objNDE015.RevNo = Convert.ToInt32(objNDE014Dest.RevNo);

                    objNDE015.ZoneNo = item.ZoneNo;
                    objNDE015.Probe = item.Probe;
                    objNDE015.Wedge = item.Wedge;
                    objNDE015.NearRefractedArea = item.NearRefractedArea;
                    objNDE015.Skip = item.Skip;
                    objNDE015.ScanType = item.ScanType;
                    objNDE015.CreatedBy = objClsLoginInfo.UserName;
                    objNDE015.CreatedOn = DateTime.Now;

                    db.NDE015.Add(objNDE015);
                    db.SaveChanges();

                    var sourceSubCategory = sourcePAUTTechnique.SourceSubCategory.Where(i => i.HeaderId == souceHeader.HeaderId && i.LineId == item.LineId).ToList();

                    db.NDE016.AddRange(sourceSubCategory.Select(i => new NDE016
                    {
                        LineId = objNDE015.LineId,
                        HeaderId = objNDE014Dest.HeaderId,
                        Project = objNDE014Dest.Project,
                        BU = objNDE014Dest.BU,
                        Location = objNDE014Dest.Location,
                        RevNo = Convert.ToInt32(objNDE014Dest.RevNo),
                        ZoneNo = objNDE015.ZoneNo,
                        GroupNo = i.GroupNo,
                        Angle = i.Angle,
                        DepthCoverage = i.DepthCoverage,
                        FocalDepth = i.FocalDepth,
                        TCGIndicationAmplitude = i.TCGIndicationAmplitude,
                        StartElement = i.StartElement,
                        NoofElements = i.NoofElements,
                        AngularIncrementalChange = i.AngularIncrementalChange,
                        FocalPlane = i.FocalPlane,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }));

                    db.SaveChanges();
                }
                #endregion
                objResponseMsg.HeaderStatus = objNDE014Dest.UTTechNo;
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data Copied Successfully To Document No : " + objResponseMsg.HeaderStatus + " of " + objNDE014Dest.Project + " Project.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                objResponseMsg.HeaderId = 0;
            }
            return objResponseMsg;
        }

        public SourcePAUTTechnique GetSourcePAUTDetail(int headerId)
        {
            SourcePAUTTechnique objSourcePAUTTechnique = new SourcePAUTTechnique();
            if (headerId > 0)
            {
                objSourcePAUTTechnique.SourceHeader = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
                objSourcePAUTTechnique.SourceCategory = db.NDE015.Where(i => i.HeaderId == headerId).ToList();
                objSourcePAUTTechnique.SourceSubCategory = db.NDE016.Where(i => i.HeaderId == headerId).ToList();
            }

            return objSourcePAUTTechnique;
        }

        public clsHelper.ResponseMsgWithStatus UpdateHeaderOnceApproved(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            NDE014 objNDE014 = new NDE014();
            NDE015 objNDE015 = new NDE015();
            NDE016 objNDE016 = new NDE016();
            List<NDE016> lstNDE016 = new List<NDE016>();
            try
            {
                if (headerId > 0)
                {
                    objNDE014 = db.NDE014.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE014 != null)
                    {
                        var lstNDE015 = db.NDE015.Where(i => i.HeaderId == headerId).ToList();
                        if (lstNDE015.Any())
                        {
                            var arrLineId = lstNDE015.Select(i => i.LineId).ToArray();
                            lstNDE016.AddRange(db.NDE016.Where(i => i.HeaderId == headerId && arrLineId.Contains(i.LineId)).ToList());

                            if (lstNDE016.Any())
                            {
                                lstNDE016.ForEach(i => i.RevNo = Convert.ToInt32(objNDE014.RevNo + 1));
                            }
                            lstNDE015.ForEach(i => i.RevNo = Convert.ToInt32(objNDE014.RevNo + 1));
                            objNDE014.RevNo = objNDE014.RevNo + 1;
                            objNDE014.ReturnRemarks = null;
                            objNDE014.ReturnedBy = null;
                            objNDE014.ReturnedOn = null;
                            objNDE014.SubmittedBy = null;
                            objNDE014.SubmittedOn = null;
                            objNDE014.ApprovedOn = null;
                            objNDE014.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        }
                        db.SaveChanges();
                        var oldFolderPath = "NDE014/" + objNDE014.HeaderId + "/R" + (objNDE014.RevNo - 1);
                        var newFolderPath = "NDE014/" + objNDE014.HeaderId + "/R" + (objNDE014.RevNo);
                        ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                        (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, newFolderPath);
                        //clsUploadCopy.CopyFolderContents(oldFolderPath, newFolderPath, objATH008, true);
                        objResponseMsg.Key = true;
                        //objResponseMsg.Value = Convert.ToString(objNDE014.HeaderId);
                        objResponseMsg.HeaderStatus = objNDE014.Status;
                        objResponseMsg.RevNo = Convert.ToString(objNDE014.RevNo);
                        objResponseMsg.Remarks = objNDE014.ReturnRemarks;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        //objResponseMsg.Value = Convert.ToString(objNDE014.HeaderId);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    //objResponseMsg.Value = Convert.ToString(objNDE014.HeaderId);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }

            return objResponseMsg;
        }

        public bool isHavingSubCategory(int lineId)
        {
            bool flag = false;
            if (lineId > 0)
            {
                var lstsubCategory = db.NDE016.Where(i => i.LineId == lineId).ToList();

                if (lstsubCategory.Any())
                    flag = true;
            }

            return flag;
        }

        public bool isCategoryExist(string project, string BU, string location, int? revNo, int zoneNo, int lineId = 0, int headerId = 0)
        {
            bool flag = false;
            List<NDE015> lstNDE015 = db.NDE015.Where(i => i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.RevNo == revNo && i.ZoneNo == zoneNo && i.LineId != lineId && i.HeaderId == headerId).ToList();

            if (lstNDE015.Any())
                flag = true;

            return flag;
        }

        public bool isSubCategoryExist(string project, string BU, string location, int? revNo, int zoneNo, int groupNo, int lineId, int subLineId = 0)
        {
            bool flag = false;

            List<NDE016> lstNDE016 = db.NDE016.Where(i => i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.RevNo == revNo && i.ZoneNo == zoneNo && i.GroupNo == groupNo && i.LineId == lineId && i.SubLineId != subLineId).ToList();

            if (lstNDE016.Any())
                flag = true;

            return flag;
        }

        public bool isPAUTTechniqueNoExist(string location, string QMSproject)
        {
            bool flag = false;
            var lstPAUtTechnique = (from a in db.NDE014
                                    where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                    select a).ToList();

            if (lstPAUtTechnique.Any())
            {
                if (!string.IsNullOrEmpty(QMSproject))
                {
                    if (lstPAUtTechnique.Where(i => i.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase)).ToList().Any())
                        flag = true;
                }
            }

            return flag;
        }
        #endregion

        public string MakeDisplayButton(int HeaderID, string status, int revision, bool viewTimeLine = false, bool viewHistory = false, bool isHistory = false)
        {
            string strButton = string.Empty;
            strButton = "<center style='white-space: nowrap;cursor:pointer;margin-left:5px;' class='clsDisplayInlineEle'><a title='View' href='" + WebsiteURL + "/NDE/MaintainPAUTTechnique/AddPAUTHeader?headerId=" + HeaderID + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a>";
            if (status == IEMQSImplementation.clsImplementationEnum.NDETechniqueStatus.Draft.ToString() && revision == 0)
            {
                strButton += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader('" + HeaderID + "')\"></i>";
            }
            else
            {
                strButton += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;opacity:0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" ></i>";
            }
            if (viewTimeLine)
            {
                strButton += "<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPAUTTechnique/ShowTimeline?HeaderID=" + Convert.ToInt32(HeaderID) + "&isHistory=" + isHistory + "');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>";
            }
            if (viewHistory)
            {
                if (revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButton += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderID) + "'); style = \"margin-left:5px;cursor:pointer;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButton += "<i title=\"History\" style = \"margin-left:5px;opacity:0.5;cursor:pointer;\" class=\"fa fa-history\"></i>";
                }
            }
            strButton += "</center>";
            return strButton;
        }

        public ActionResult ShowTimeline(int HeaderId, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE014_Log objNDE014 = db.NDE014_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "SCANUTTECH";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE014.ApprovedBy);
                model.ApprovedOn = objNDE014.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE014.SubmittedBy);
                model.SubmittedOn = objNDE014.SubmittedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE014.ReturnedBy);
                model.ReturnedOn = objNDE014.ReturnedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE014.CreatedBy);
                model.CreatedOn = objNDE014.CreatedOn;
            }
            else
            {
                NDE014 objNDE014 = db.NDE014.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "SCANUTTECH";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE014.ApprovedBy);
                model.ApprovedOn = objNDE014.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE014.SubmittedBy);
                model.SubmittedOn = objNDE014.SubmittedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE014.ReturnedBy);
                model.ReturnedOn = objNDE014.ReturnedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE014.CreatedBy);
                model.CreatedOn = objNDE014.CreatedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        #region Export to Excel 
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //generate for header
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      UTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.UTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn != null ? (Convert.ToDateTime(li.CreatedOn)).ToString("dd/MM/yyyy") : "",
                                      Status = li.Status,
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //for Lines
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetCategories(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from a in lst
                                  select new
                                  {
                                      ZoneNo = Convert.ToString(a.ZoneNo),
                                      Probe = Convert.ToString(a.Probe),
                                      Wedge = Convert.ToString(a.Wedge),
                                      NearRefractedArea = Convert.ToString(a.NearRefractedArea),
                                      Skip = a.Skip,
                                      ScanType = a.ScanType
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //for Sub Lines
                else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetSubCategory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from sl in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(sl.ROW_NO),
                                      GroupNo = Convert.ToString(sl.GroupNo),
                                      Angle = sl.Angle,
                                      DepthCoverage = sl.DepthCoverage,
                                      FocalDepth = Convert.ToString(sl.FocalDepth),
                                      TCGIndicationAmplitude = Convert.ToString(sl.TCGIndicationAmplitude),
                                      StartElement = Convert.ToString(sl.StartElement),
                                      NoofElements = Convert.ToString(sl.NoofElements),
                                      AngularIncrementalChange = Convert.ToString(sl.AngularIncrementalChange),
                                      FocalPlane = sl.FocalPlane,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetHeaderHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      UTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumentNo = li.UTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn != null ? (Convert.ToDateTime(li.CreatedOn)).ToString("dd/MM/yyyy") : "",
                                      Status = li.Status,
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                if (gridType == clsImplementationEnum.GridType.HISTORY_SUB_LINES.GetStringValue())
                {
                    var lst = db.SP_NDE_PAUT_GetSubCategoryHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from sl in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(sl.ROW_NO),
                                      //GetRemarkStyle(objQCP001.Status,uc.ReturnRemark),
                                      ZoneNo = Convert.ToString(sl.ZoneNo),
                                      //Convert.ToString(uc.LineSrNo),
                                      GroupNo = Convert.ToString(sl.GroupNo),
                                      Angle = sl.Angle,
                                      DepthCoverage = sl.DepthCoverage,
                                      FocalDepth = Convert.ToString(sl.FocalDepth),
                                      TCGIndicationAmplitude = Convert.ToString(sl.TCGIndicationAmplitude),
                                      StartElement = Convert.ToString(sl.StartElement),
                                      NoofElements = Convert.ToString(sl.NoofElements),
                                      AngularIncrementalChange = Convert.ToString(sl.AngularIncrementalChange),
                                      FocalPlane = sl.FocalPlane,
                                      CreatedBy = Convert.ToString(sl.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(sl.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}