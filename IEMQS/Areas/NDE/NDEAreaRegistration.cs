﻿using System.Web.Mvc;

namespace IEMQS.Areas.NDE
{
    public class NDEAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "NDE";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "NDE_default",
                "NDE/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}