﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.Dashboard.Controllers
{
    public class QuickSearchController : clsBase
    {
        public ActionResult Index()
        {
            return PartialView("Index");
        }

        public PartialViewResult GetProjectDetails(string ProjectNo, string QualityProject = "")
        {
            List<ProjectUsedDtl> listProjectUsedDtl = new List<ProjectUsedDtl>();
            List<SP_ATH_INS_DEL_GET_USER_WISE_ACCESS_MENU_Result> list = new List<SP_ATH_INS_DEL_GET_USER_WISE_ACCESS_MENU_Result>();
            ProjectUsedDtl objProjectUsedDtl = null;
            ProjectUsedActionType objProjectUsedActionType = null;
            int Key = 1;
            try
            {
                var objProjectDtl = db.COM001.Where(i => i.t_cprj.ToLower() == ProjectNo.ToLower()).FirstOrDefault();
                ViewBag.ProjectDesription = objProjectDtl.t_dsca;
                var listMenu = objClsLoginInfo.listMenuResult;

                #region FKMS
                list = listMenu.Where(i => i.Area == "FKMS").ToList();
                FKM101 objFKM101 = null;
                FKM114 objFKM114 = null;
                if (db.FKM101.Any(i => i.Project == ProjectNo) || db.FKM114.Any(i => i.Project == ProjectNo))
                {
                    objFKM101 = db.FKM101.Where(i => i.Project == ProjectNo).FirstOrDefault();
                    objFKM114 = db.FKM114.Where(i => i.Project == ProjectNo).FirstOrDefault();
                    if ((objFKM101 != null && objFKM101.HeaderId > 0) || (objFKM114 != null && objFKM114.HeaderId > 0))
                    {
                        objProjectUsedDtl = new ProjectUsedDtl();
                        objProjectUsedDtl.Module = "FKMS";
                        objProjectUsedDtl.Key = Key++;
                    }
                    if (objFKM101 != null && objFKM101.HeaderId > 0)
                    {
                        var item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "index" &&
                                     !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "maintainfkms").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Maintain"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Maintain";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/FKMSDetails/" + objFKM101.HeaderId.ToString() + "?urlForm=m"
                            });
                        }

                        item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "approverindex" &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "maintainfkms").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Approve"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Approve";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Approve").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/FKMSDetails/" + objFKM101.HeaderId.ToString() + "?urlForm=a"
                            });
                        }

                        item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "displayfkms" &&
                                                                 !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "maintainfkms").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Display"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Display";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Display").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/FKMSDisplayDetails/" + objFKM101.HeaderId.ToString() + "?urlForm=m"
                            });
                        }

                    }
                    if (objFKM114 != null && objFKM114.HeaderId > 0)
                    {
                        var item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "index" &&
                                     !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "maintainfr").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Maintain"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Maintain";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/FRDetails/" + objFKM114.HeaderId.ToString() + "?urlForm=m"
                            });
                        }

                        item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "approverindex" &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "maintainfr").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Approve"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Approve";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Approve").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/FRDetails/" + objFKM114.HeaderId.ToString() + "?urlForm=a"
                            });
                        }
                    }
                    listProjectUsedDtl.Add(objProjectUsedDtl);
                }
                #endregion

                #region PDIN
                list = listMenu.Where(i => i.Area == "PDIN").ToList();
                PDN001 objPDN001 = null;
                objProjectUsedDtl = null;
                if (db.PDN001.Any(i => i.Project.ToLower() == ProjectNo.ToLower()))
                {
                    objPDN001 = db.PDN001.Where(i => i.Project.ToLower() == ProjectNo.ToLower()).OrderByDescending(i => i.HeaderId).FirstOrDefault();
                    if ((objPDN001 != null && objPDN001.HeaderId > 0))
                    {
                        objProjectUsedDtl = new ProjectUsedDtl();
                        objProjectUsedDtl.Module = "Planning DIN";

                        var item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "index" &&
                                     !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "maintain").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Maintain"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Maintain";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                            }


                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/Create?HeaderID=" + objPDN001.HeaderId.ToString()
                            });
                        }

                        item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "index" &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "approve").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Approve"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Approve";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Approve").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/ViewHeader?HeaderID=" + objPDN001.HeaderId.ToString()
                            });
                        }

                        item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "index" &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "shopsummary").FirstOrDefault();
                        if (item != null)
                        {
                            if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Display"))
                            {
                                objProjectUsedActionType = new ProjectUsedActionType();
                                objProjectUsedActionType.ActionType = "Display";
                                objProjectUsedActionType.Key = Key++;
                                objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                            }
                            else
                            {
                                objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                            }

                            objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                            {
                                MenuName = item.Process,
                                URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/ViewHeader?HeaderID=" + objPDN001.HeaderId.ToString()
                            });
                        }
                    }
                    listProjectUsedDtl.Add(objProjectUsedDtl);
                }
                #endregion

                #region In-Process Inspection
                if (!string.IsNullOrEmpty(QualityProject))
                {
                    list = listMenu.Where(i => i.Area == "IPI").ToList();
                    #region Quality Id
                    QMS015 objQMS015 = null;
                    objProjectUsedDtl = null;
                    if (db.QMS015.Any(i => i.QualityProject.ToLower() == QualityProject.ToLower() && i.Location == objClsLoginInfo.Location))
                    {
                        objQMS015 = db.QMS015.Where(i => i.QualityProject.ToLower() == QualityProject.ToLower() && i.Location == objClsLoginInfo.Location).FirstOrDefault();
                        if ((objQMS015 != null && objQMS015.HeaderId > 0))
                        {
                            objProjectUsedDtl = new ProjectUsedDtl();
                            objProjectUsedDtl.Module = "In-Process Inspection";

                            var item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "Index".ToLower() &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "QualityId".ToLower()).FirstOrDefault();
                            if (item != null)
                            {
                                if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Maintain"))
                                {
                                    objProjectUsedActionType = new ProjectUsedActionType();
                                    objProjectUsedActionType.ActionType = "Maintain";
                                    objProjectUsedActionType.Key = Key++;
                                    objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                }
                                else
                                {
                                    objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                                }


                                objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                                {
                                    MenuName = item.Process,
                                    URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/AddQualityIdHeader?id=" + objQMS015.HeaderId.ToString()
                                });
                            }

                            item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "Index".ToLower() &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "ApproveQualityId".ToLower()).FirstOrDefault();
                            if (item != null)
                            {
                                if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Approve"))
                                {
                                    objProjectUsedActionType = new ProjectUsedActionType();
                                    objProjectUsedActionType.ActionType = "Approve";
                                    objProjectUsedActionType.Key = Key++;
                                    objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                }
                                else
                                {
                                    objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Approve").FirstOrDefault();
                                }

                                objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                                {
                                    MenuName = item.Process,
                                    URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/ViewQualityId?headerId=" + objQMS015.HeaderId.ToString()
                                });
                            }
                        }
                        listProjectUsedDtl.Add(objProjectUsedDtl);
                    }
                    #endregion

                    #region Test Plan / Shop Weld Plan
                    QMS020 objQMS020 = null;
                    objProjectUsedDtl = null;
                    if (db.QMS020.Any(i => i.QualityProject.ToLower() == QualityProject.ToLower() && i.Location == objClsLoginInfo.Location))
                    {
                        objQMS020 = db.QMS020.Where(i => i.QualityProject.ToLower() == QualityProject.ToLower() && i.Location == objClsLoginInfo.Location).FirstOrDefault();
                        if ((objQMS020 != null && objQMS020.HeaderId > 0))
                        {
                            if (!listProjectUsedDtl.Any(i => i.Module == "In-Process Inspection"))
                            {
                                objProjectUsedDtl = new ProjectUsedDtl();
                                objProjectUsedDtl.Module = "In-Process Inspection";
                            }
                            else
                            {
                                objProjectUsedDtl = listProjectUsedDtl.Where(i => i.Module == "In-Process Inspection").FirstOrDefault();
                            }

                            var item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "Index".ToLower() &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "TestPlan".ToLower()).FirstOrDefault();
                            if (item != null)
                            {
                                if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Maintain"))
                                {
                                    objProjectUsedActionType = new ProjectUsedActionType();
                                    objProjectUsedActionType.ActionType = "Maintain";
                                    objProjectUsedActionType.Key = Key++;
                                    objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                }
                                else
                                {
                                    objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                                }

                                objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                                {
                                    MenuName = item.Process,
                                    URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/AddHeader?HeaderID=" + objQMS020.HeaderId.ToString()
                                });
                            }

                            item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "Index".ToLower() &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "ApproveTestPlan".ToLower()).FirstOrDefault();
                            if (item != null)
                            {
                                if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Approve"))
                                {
                                    objProjectUsedActionType = new ProjectUsedActionType();
                                    objProjectUsedActionType.ActionType = "Approve";
                                    objProjectUsedActionType.Key = Key++;
                                    objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                }
                                else
                                {
                                    objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Approve").FirstOrDefault();
                                }

                                objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                                {
                                    MenuName = item.Process,
                                    URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/TestPlanDetails?HeaderID=" + objQMS020.HeaderId.ToString()
                                });
                            }
                        }
                        if (!listProjectUsedDtl.Any(i => i.Module == "In-Process Inspection"))
                        {
                            listProjectUsedDtl.Add(objProjectUsedDtl);
                        }
                    }
                    #endregion

                    #region Seam ICL
                    QMS030 objQMS030 = null;
                    objProjectUsedDtl = null;
                    if (db.QMS030.Any(i => i.QualityProject.ToLower() == QualityProject.ToLower() && i.Location == objClsLoginInfo.Location))
                    {
                        objQMS030 = db.QMS030.Where(i => i.QualityProject.ToLower() == QualityProject.ToLower() && i.Location == objClsLoginInfo.Location).FirstOrDefault();
                        if ((objQMS030 != null && objQMS030.HeaderId > 0))
                        {
                            if (!listProjectUsedDtl.Any(i => i.Module == "In-Process Inspection"))
                            {
                                objProjectUsedDtl = new ProjectUsedDtl();
                                objProjectUsedDtl.Module = "In-Process Inspection";
                            }
                            else
                            {
                                objProjectUsedDtl = listProjectUsedDtl.Where(i => i.Module == "In-Process Inspection").FirstOrDefault();
                            }

                            var item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "Index".ToLower() &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "MaintainSeamICL".ToLower()).FirstOrDefault();
                            if (item != null)
                            {
                                if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Maintain"))
                                {
                                    objProjectUsedActionType = new ProjectUsedActionType();
                                    objProjectUsedActionType.ActionType = "Maintain";
                                    objProjectUsedActionType.Key = Key++;
                                    objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                }
                                else
                                {
                                    objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Maintain").FirstOrDefault();
                                }

                                objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                                {
                                    MenuName = item.Process,
                                    URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/AddHeader?HeaderID=" + objQMS030.HeaderId.ToString()
                                });
                            }

                            item = list.Where(i => !string.IsNullOrEmpty(i.Action) && i.Action.ToLower() == "Index".ToLower() &&
                                         !string.IsNullOrEmpty(i.Controller) && i.Controller.ToLower() == "ApproveICLSeam".ToLower()).FirstOrDefault();
                            if (item != null)
                            {
                                if (!objProjectUsedDtl.listProjectUsedActionType.Any(i => i.ActionType == "Approve"))
                                {
                                    objProjectUsedActionType = new ProjectUsedActionType();
                                    objProjectUsedActionType.ActionType = "Approve";
                                    objProjectUsedActionType.Key = Key++;
                                    objProjectUsedDtl.listProjectUsedActionType.Add(objProjectUsedActionType);
                                }
                                else
                                {
                                    objProjectUsedActionType = objProjectUsedDtl.listProjectUsedActionType.Where(i => i.ActionType == "Approve").FirstOrDefault();
                                }

                                objProjectUsedActionType.listMenuDtl.Add(new ProjectUsedMenuDtl
                                {
                                    MenuName = item.Process,
                                    URL = WebsiteURL + "/" + item.Area + "/" + item.Controller + "/Viewdetails?headerid=" + objQMS030.HeaderId.ToString()
                                });
                            }
                        }
                        if (!listProjectUsedDtl.Any(i => i.Module == "In-Process Inspection"))
                        {
                            listProjectUsedDtl.Add(objProjectUsedDtl);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception exc)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exc);
            }
            return PartialView("_GetProjectDetails", listProjectUsedDtl);
        }

        public class ProjectUsedDtl
        {
            public ProjectUsedDtl()
            {
                listProjectUsedActionType = new List<ProjectUsedActionType>();
            }
            public int Key { get; set; }
            public string Module { get; set; }
            public List<ProjectUsedActionType> listProjectUsedActionType { get; set; }

        }

        public class ProjectUsedMenuDtl
        {
            public ProjectUsedMenuDtl()
            {

            }
            public int Key { get; set; }
            public string MenuName { get; set; }
            public string URL { get; set; }
        }

        public class ProjectUsedActionType
        {
            public ProjectUsedActionType()
            {
                listMenuDtl = new List<ProjectUsedMenuDtl>();
            }
            public int Key { get; set; }
            public string ActionType { get; set; }
            public List<ProjectUsedMenuDtl> listMenuDtl { get; set; }
        }
    }
}
