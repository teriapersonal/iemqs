﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.Dashboard.Controllers
{
    public class DashboardController : clsBase
    {
        /// <summary>
        /// Created by Dharmesh 27-06-2017
        /// For Making Dashboard
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [SessionExpireFilter]
        //[UserPermissions]
        public ActionResult Index()
        {
            bool ismobile = false;
            if (Request.Browser.IsMobileDevice)
            {
                ismobile = true;
            }

            if (!ismobile)
            {
                string currentUser = objClsLoginInfo.UserName.Trim();
                //list of Roles from authorization
                //var lstResult = (from a in db.ATH001
                //                 join b in db.ATH004 on a.Role equals b.Id
                //                 where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                //                 select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();
                var lstResult = objClsLoginInfo.ListRoles.Select(a => new { RoleId = a, RoleDesc = a });
                if (lstResult != null)
                {
                    //list of FDMS role
                    if (lstResult.Any(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
                    {
                        ViewBag.displayFDMS = "True";
                    }
                    else
                    {
                        ViewBag.displayFDMS = "False";
                    }
                }

                string ITAdmin = clsImplementationEnum.UserRoleName.ITA1.GetStringValue().Trim();
                //var lstResult2 = (from ath4 in db.ATH004
                //                  join ath1 in db.ATH001 on ath4.Id equals ath1.Role
                //                  where ITAdmin.Contains(ath4.Role) && ath1.Employee.Trim() == currentUser
                //                  select ath4).FirstOrDefault();
                if (lstResult.Any(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA1.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
                {
                    ViewBag.isITAdmin = "True";
                }
                else
                {
                    ViewBag.isITAdmin = "False";
                }
                //HTR ROLES
                if (lstResult != null)
                {
                    //Role access for HTR DPP
                    if (lstResult.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.MET3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.FS3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PUR1.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PUR2.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                          || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                    {
                        ViewBag.displayHTRDCC = "True";
                    }
                    else
                    {
                        ViewBag.displayHTRDCC = "False";
                    }
                }

                string whereCondition = "1=1 AND (CreatedUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() + "')) OR (AddresseeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() + "')) OR (AssigneeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() + "')) ";
                string deptwhereCondition = "1=1 AND (CreatedUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() + "')) OR (AddresseeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() + "')) OR (AssigneeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() + "')) ";

                //var result = db.SP_DASHBOARD_GET_SEAMANDPARTOFFERSUMMARY(objClsLoginInfo.UserName).FirstOrDefault();

                //if (result != null)
                //{
                //    ViewBag.PendingSeamsToOffer = result.TotalSeamReadyToOffer;
                //    ViewBag.PendingPartsToOffer = result.TotalPartReadyToOffer;
                //    ViewBag.TotalSeamsCleared = result.TotalSeamCleared;
                //    ViewBag.TotalPartsCleared = result.TotalPartCleared;
                //}
                //else
                //{
                //    ViewBag.PendingSeamsToOffer = 0;
                //    ViewBag.PendingPartsToOffer = 0;
                //    ViewBag.TotalSeamsCleared = 0;
                //    ViewBag.TotalPartsCleared = 0;
                //}

                var lstPam = db.SP_TCP_GET_TCP_Header(1, int.MaxValue, "", whereCondition).ToList();
                //var lstdeptPam = db.SP_TCP_GET_TCP_Header(1, int.MaxValue, "", deptwhereCondition).ToList();
                ViewBag.PendingTCP = lstPam.Count();
                ViewBag.PendingdeptTCP = lstPam.Count();

                //if user does not have any role in ATH001 then below condition are applie
                #region  Code for Access Request
                //var objATH001 = db.ATH001.Where(c => c.Employee == objClsLoginInfo.UserName).ToList();
                //if (objATH001.Count() == 0)
                //{
                //    //ViewBag.displayAccessReq = "false";
                //    string requested = clsImplementationEnum.AccessRequest.Requested.GetStringValue();
                //    string rejected = clsImplementationEnum.AccessRequest.Rejected.GetStringValue();
                //    bool IsRequested = db.ATH020.Any(c => c.PSNo == objClsLoginInfo.UserName && (c.Status == requested || c.Status == rejected));
                //    if (IsRequested)
                //    { ViewBag.accessgrid = "true"; }
                //}
                ///*else
                //{
                //    ViewBag.displayAccessReq = "true";
                //}*/
                //ViewBag.PSNo = currentUser;
                #endregion

                List<string> lstStrMileStone = clsImplementationEnum.getHeatTreatment().Where(x => x.ToString() != "NA").ToList();
                lstStrMileStone.Add("Post Hydro");
                List<BULocWiseCategoryModel> lstMileStone = lstStrMileStone.Select(x => new BULocWiseCategoryModel { CatDesc = x, CatID = x }).ToList();
                ViewBag.lstMileStone = new JavaScriptSerializer().Serialize(lstMileStone);
            }
            var PowerBIReportBaseURL = db.CONFIG.Where(i => i.Key == "PowerBIReportURL").FirstOrDefault().Value;
            ViewBag.SCUDashboardURL = Manager.GetSCUDashboardLink();
            ViewBag.COPDashboardURL = PowerBIReportBaseURL + "Power%20BI%20Reports/IEMQS/COP/COPDashboard";
            var ReportUrl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SSRS_Server_URL"]);
            ViewBag.DENLoadChartURL = ReportUrl + "/Pages/ReportViewer.aspx?/Reports/IEMQS/DEN/LoadChart";
            ViewBag.DENDPPChartURL = ReportUrl + "/Pages/ReportViewer.aspx?/Reports/IEMQS/DEN/DPPChart";

            ViewBag.IsProd3 = objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue());
            ViewBag.IsRoleFound = objClsLoginInfo.IsRoleFound;
            return View();
        }

        #region Favorite Tabs 
        [HttpPost]
        [SessionExpireFilter]
        public ActionResult LoadFavoriteTabPartial(string status)
        {
            string currentUser = objClsLoginInfo.UserName.Trim();
            var lstAth025 = (from ath25 in db.ATH025
                             join ath2 in db.ATH002 on ath25.ProcessId equals ath2.Id
                             where ath25.Employee.Trim() == currentUser
                             select new FavoriteTabData
                             {
                                 OrderNo = ath25.Order,
                                 Ath025Id = ath25.Id,
                                 ProcessId = ath25.ProcessId,
                                 ProcessName = ath2.Process,
                                 ProcessDesc = ath2.Description,
                                 UrlLink = WebsiteURL + "/" + ath2.Area + "/" + ath2.Controller + "/" + ath2.Action
                             }).ToList();
            if (lstAth025.Count > 0)
            {
                ViewBag.FavoriteTabs = lstAth025.OrderByDescending(x => x.OrderNo).ToList();
            }
            else
            { ViewBag.FavoriteTabs = null; }

            return PartialView("_LoadFavoriteTabPartial");
        }

        [HttpPost]
        public ActionResult DeleteFavoritesProcess(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                ATH025 objATH025 = db.ATH025.Where(x => x.Id == id && x.Employee.Trim() == objClsLoginInfo.UserName.Trim()).FirstOrDefault();
                if (objATH025 != null)
                {
                    int? deletedorderno = objATH025.Order;
                    var lstNextOrder = db.ATH025.Where(x => x.Employee.Trim() == objClsLoginInfo.UserName.Trim() && x.Order > deletedorderno).ToList();
                    db.ATH025.Remove(objATH025);
                    db.SaveChanges();
                    foreach (var item in lstNextOrder)
                    {
                        ATH025 updateATH025 = db.ATH025.Where(x => x.Id == item.Id).FirstOrDefault();
                        updateATH025.Order = updateATH025.Order - 1;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "if you want to add then delete existing any favorites item..";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult IsQueryString(string strpath)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string[] strArr = null;
            strArr = strpath.Split('/');
            string area = string.Empty;
            string controller = string.Empty;
            string action = string.Empty;
            try

            {
                if (strArr.Length > 0)
                {
                    area = strArr[0].Trim();
                }
                if (strArr.Length > 1)
                {
                    controller = strArr[1].Trim();
                }
                if (strArr.Length > 2)
                {
                    action = strArr[2].Trim();
                }
                if (action == string.Empty) { action = "Index"; }
                ATH025 objATH025 = new ATH025();
                if (strArr.Length > 3)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    var urlList = (from ATH002 in db.ATH002
                                   where ATH002.Area.ToLower() == area.ToLower() && ATH002.Controller.ToLower() == controller.ToLower() && ATH002.Action.ToLower() == action.ToLower()
                                   select new { text = "/" + ATH002.Area + "/" + ATH002.Controller + "/" + ATH002.Action, id = ATH002.Id }
                                ).FirstOrDefault();
                    //var isExists = urlList.Where(x => x.text.ToLower() == strpath.ToLower()).FirstOrDefault();                
                    if (urlList == null)
                    {
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        var processIdExist = db.ATH025.Where(x => x.ProcessId == urlList.id && x.Employee.Trim() == objClsLoginInfo.UserName.Trim()).FirstOrDefault();
                        if (processIdExist != null)
                        {
                            objResponseMsg.ActionKey = true;
                            objResponseMsg.HeaderId = processIdExist.Id;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReOrderQuickLink(List<FavoriteTabData> listFavoriteTabData)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                var objATH025 = db.ATH025.Where(i => i.Employee == objClsLoginInfo.UserName).ToList();
                objATH025.ForEach(i =>
                {
                    if (listFavoriteTabData.Any(x => x.Ath025Id == i.Id))
                    {
                        i.Order = listFavoriteTabData.Where(x => x.Ath025Id == i.Id).FirstOrDefault().OrderNo;
                    }
                });
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Mobile Menu 
        [HttpPost]
        [SessionExpireFilter]
        public ActionResult LoadMobileMenuPartial(string status)
        {
            List<SP_ATH_GET_USER_ACCESS_MENU_Result> lstMenuResult = db.SP_ATH_GET_USER_ACCESS_MENU(objClsLoginInfo.UserName).ToList();

            var lstAth = (from menu in lstMenuResult
                          join ath3 in db.ATH003 on menu.ProcessId equals ath3.ProcessId
                          join ath2 in db.ATH002 on ath3.ProcessId equals ath2.Id
                          where ath3.IsMobileMenu == true
                          select new
                          {
                              OrderNo = ath2.OrderNo,
                              ProcessId = ath2.Id,
                              ProcessName = ath2.Process,
                              ProcessDesc = ath2.Description,
                              UrlLink = WebsiteURL + "/" + ath2.Area + "/" + ath2.Controller + "/" + ath2.Action
                          }).Distinct().Select(ath2 => new FavoriteTabData
                          {
                              OrderNo = ath2.OrderNo,
                              ProcessId = ath2.ProcessId,
                              ProcessName = ath2.ProcessName,
                              ProcessDesc = ath2.ProcessDesc,
                              UrlLink = ath2.UrlLink
                          }).OrderByDescending(x => x.OrderNo).ToList();
            if (lstAth.Count > 0)
            {
                ViewBag.MobileMenu = lstAth;
            }
            else
            { ViewBag.MobileMenu = null; }

            return PartialView("_LoadMobileMenuPartial");
        }

        #endregion

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult LoadDispatchPartialView(string type, string term)
        {
            var lstContract = (new clsManager()).GetContractByUser(objClsLoginInfo.UserName);
            DateTime FromDate = DateTime.Today;
            DateTime ToDate = DateTime.Today;
            if (type.ToUpper() == "OVERDUE")
            {
                FromDate = DateTime.Today.AddYears(-200);
                ToDate = DateTime.Today.AddSeconds(-1);
            }
            else if (type.ToUpper() == "WEEK")
            {
                FromDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
                ToDate = FromDate.AddDays(7).AddSeconds(-1);
            }
            else if (type.ToUpper() == "MONTH")
            {
                FromDate = DateTime.Today.AddDays(1 - DateTime.Today.Day);
                ToDate = FromDate.AddMonths(1).AddSeconds(-1);
            }

            var result = db.SP_DASHBOARD_GET_DISPATCH_DATA(objClsLoginInfo.UserName, FromDate, ToDate, string.Join(",", lstContract), term).ToList();

            ViewBag.LoadData = result;

            return PartialView("_LoadDispatchData");
        }

        [HttpPost]
        public ActionResult LoadDispatchContainerPartialView()
        {
            return PartialView("DispatchWidget");
        }

        [HttpPost]
        public ActionResult LoadPendingFDMSContainerPartialView()
        {
            return PartialView("_PendingFDMSPartial");
        }

        [HttpPost]
        public ActionResult LoadHTRReportContainerPartialView()
        {
            return PartialView("_LoadHTRReportPartialView");
        }


        [HttpPost]
        public ActionResult LoadPendingFDMSPartialView(string type, string term)
        {
            var result = db.SP_FETCH_PENDING_FDMS(type, term).ToList();
            ViewBag.type = type;
            return PartialView("_LoadPendingFDMSData", result);
        }


        [HttpPost]
        public ActionResult GetDashboardData()
        {
            clsHelper.DashboardData objResponseMsg = new clsHelper.DashboardData();
            try
            {
                var result = db.SP_DASHBOARD_GET_SEAMANDPARTOFFERSUMMARY(objClsLoginInfo.UserName).FirstOrDefault();

                objResponseMsg.PendingSeamsToOffer = result.TotalSeamReadyToOffer;
                objResponseMsg.PendingPartsToOffer = result.TotalPartReadyToOffer;
                objResponseMsg.TotalSeamsCleared = result.TotalSeamCleared;
                objResponseMsg.TotalPartsCleared = result.TotalPartCleared;
                objResponseMsg.Key = true;
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                return Json(objResponseMsg);
            }
        }
        #region Seam clear section from dashboard
        [HttpPost]
        public ActionResult Chart_Seam_Part_Cleared_Data()
        {
            clsHelper.DashboardData objResponseMsg = new clsHelper.DashboardData();
            try
            {
                var result = db.SP_DASHBOARD_GET_SEAM_CLEARED_DATA(objClsLoginInfo.UserName);

                objResponseMsg.ChartSeamsPartsCleared = result.Select(s => new clsHelper.DashboardChartData { SrNO = s.SrNo, MonthDisplayName = s.MonthDisplayName, TotalPartCleared = s.TotalPartCleared, TotalSeamCleared = s.TotalSeamCleared }).ToList();
                objResponseMsg.Key = true;
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                return Json(objResponseMsg);
            }
        }
        #endregion

        //[HttpPost]
        //public ActionResult Get_Dispatch_Data(string type)
        //{
        //    clsHelper.DashboardData objResponseMsg = new clsHelper.DashboardData();
        //    try
        //    {
        //        var lstContract = (new clsManager()).GetContractByUser(objClsLoginInfo.UserName);
        //        DateTime FromDate = DateTime.Today;
        //        DateTime ToDate = DateTime.Today;
        //        if (type.ToUpper() == "WEEK")
        //        {
        //            FromDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
        //            ToDate = FromDate.AddDays(7).AddSeconds(-1);
        //        }
        //        else if (type.ToUpper() == "MONTH")
        //        {
        //            FromDate = DateTime.Today.AddDays(1 - DateTime.Today.Day);
        //            ToDate = FromDate.AddMonths(1).AddSeconds(-1);
        //        }


        //        var result = db.SP_DASHBOARD_GET_DISPATCH_DATA(objClsLoginInfo.UserName, FromDate, ToDate, string.Join(",", lstContract));

        //        //objResponseMsg.ChartSeamsPartsCleared = result.Select(s => new clsHelper.DashboardChartData { SrNO = s.SrNo, MonthDisplayName = s.MonthDisplayName, TotalPartCleared = s.TotalPartCleared, TotalSeamCleared = s.TotalSeamCleared }).ToList();
        //        objResponseMsg.Key = true;
        //        return Json(objResponseMsg);
        //    }
        //    catch (Exception ex)
        //    {
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.Message;
        //        return Json(objResponseMsg);
        //    }
        //}

        #region HTR Dashboard 
        /// <summary>
        /// created by nikita vibhandik
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadHTRReportPartialView()
        {
            return PartialView("_LoadHTRReportPartialView");
        }


        public JsonResult LoadHTRReportData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += " 1=1";

                string[] columnName = { "Project", "HTRType","com1.t_dsca", "HTRNo", "Status", "CreatedOn" , "RevNo", "DATEDIFF(DAY, CreatedOn, ReviewdOn)"
                                        , "DATEDIFF(DAY, CreatedOn, FurnaceOn)", "DATEDIFF(DAY, CreatedOn, InspectOn)" , "CONVERT(VARCHAR(10), CreatedOn, 103)"};

                #region Search by individual column
                //search by project column
                if (!string.IsNullOrWhiteSpace(param.Project))
                {
                    strWhere += "and (htr.Project like '%" + param.Project.Trim() +
                        "%' or com1.t_dsca like '%" + param.Project.Trim() +
                        "%' or  (htr.Project+ '-'+ com1.t_dsca) like '%" + param.Project.Trim() + "%')";
                }
                //serach by BU column
                if (!string.IsNullOrWhiteSpace(param.BU))
                { strWhere += "and (BU like '%" + param.BU.Trim() + "%')"; }
                //search by htr no
                if (!string.IsNullOrWhiteSpace(param.HTRNo))
                { strWhere += "and (HTRNo like '%" + param.HTRNo.Trim() + "%')"; }

                //search by days(filter days of inspector/furnace/reviewer
                if (!string.IsNullOrWhiteSpace(param.dbDays1))
                {
                    int searchDays = Convert.ToInt32(param.dbDays1);
                    strWhere += "and DATEDIFF(DAY, CreatedOn, ReviewdOn) >  " + searchDays + " or DATEDIFF(DAY, CreatedOn, FurnaceOn) > " + searchDays + " or  DATEDIFF(DAY, CreatedOn, InspectOn) > 10";
                }
                //search by htr type/flow type
                if (!string.IsNullOrWhiteSpace(param.dbDays2))
                {
                    string[] arraysearchflow = param.dbDays2.Split(',').ToArray();
                    string types = string.Format("`{0}`", string.Join("`,`", arraysearchflow)).Replace("'", "''").Replace("`", "'");

                    if (arraysearchflow.Length > 0)
                    {
                        strWhere += " and HTRType in (" + types + ") ";
                    }
                }
                #endregion

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                var lstResult = db.SP_DASHBOARD_HTR_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue() ? "Job Component" :uc.HTRType),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            htrLink(uc.HeaderId),
                            Convert.ToString("R" + uc.RevNo),
                            uc.CreatedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            Convert.ToString(uc.SubmittedOn.HasValue ? uc.SubmittedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                            Convert.ToString(uc.DaystakenbyMET3),
                            Convert.ToString(uc.DaystakenbyFS),
                            Convert.ToString(uc.DaystakenbyQI),
                            Convert.ToString(uc.Status),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string htrLink(int id)
        {
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role.Trim() }).ToList();

            string htrlink = "";
            string htmlControl = "";
            if (id > 0)
            {
                IEMQSEntitiesContext db = new IEMQSEntitiesContext();
                var objhtr = db.HTR001.Where(x => x.HeaderId == id).FirstOrDefault();
                //for reviewer
                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.MET3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    if (objhtr.Status == clsImplementationEnum.HTRStatus.InReview.GetStringValue())
                    { htrlink = "/HTR/Review/HTRDetails?HeaderID=" + id; }

                    else if (objhtr.Status == clsImplementationEnum.HTRStatus.Release.GetStringValue())
                    { htrlink = "/HTR/Release/HTRDetails?HeaderID=" + id; }

                    else
                    { htrlink = "/HTR/Maintain/AddHeader?HeaderID=" + id; }
                }
                //for furnace
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.FUR1.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    htrlink = "/HTR/FurnaseSupervisor/HTRDetails?HeaderID=" + id;
                }
                //for inspector
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    htrlink = "/HTR/Inspector/HTRDetails?HeaderID=" + id;
                }
                //for release
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PUR1.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PUR2.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    htrlink = "/HTR/Release/HTRDetails?HeaderID=" + id;
                }
                //for release
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                      || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                      || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                      || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue().Trim(), StringComparison.OrdinalIgnoreCase)
                                   ).ToList().Any())
                {
                    if (objhtr.Status == clsImplementationEnum.HTRStatus.Release.GetStringValue())
                    { htrlink = "/HTR/Release/HTRDetails?HeaderID=" + id; }
                    else
                    { htrlink = "/HTR/Maintain/AddHeader?HeaderID=" + id; }
                }
                //for maintain
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    htrlink = "/HTR/Maintain/AddHeader?HeaderID=" + id;
                }
                else
                {
                    htrlink = "/authentication/Authenticate/AccessDenied";
                }
                htmlControl = "<a href=" + WebsiteURL + htrlink + " name=\"documentLink\">" + objhtr.HTRNo + "</a>";
            }
            else
            {
                htmlControl = "";
            }

            return htmlControl;
        }
        #endregion

        #region Export Excell
        //Code By :Nikita on 05/12/2017, Export functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", string JobNo = "", string Assembly = "", string MileStone = "", string StageCode = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.DashboardHTRDPP.GetStringValue())
                {
                    var lst = db.SP_DASHBOARD_HTR_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      HTRType = Convert.ToString(uc.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue() ? "Job Component" : uc.HTRType),
                                      Project = Convert.ToString(uc.Project),
                                      BU = Convert.ToString(uc.BU),
                                      HTRNo = uc.HTRNo + " / " + Convert.ToString(uc.Status),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      CreatedOn = uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedOn = Convert.ToString(uc.SubmittedOn.HasValue ? uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                      DaystakenbyMET3 = Convert.ToString(uc.DaystakenbyMET3),
                                      DaystakenbyFS = Convert.ToString(uc.DaystakenbyFS),
                                      DaystakenbyQI = Convert.ToString(uc.DaystakenbyQI),
                                      //Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                if (gridType == clsImplementationEnum.GridType.IPIDashboardSeamGridData.GetStringValue())
                {
                    string condition = whereCondition.Split('|')[0];
                    string milestone = whereCondition.Split('|')[1];
                    var lst = db.SP_DASHBOARD_SEAMLIST_DETAILS(1, int.MaxValue, strSortOrder, condition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Project = h.Project,
                                      AssemblyNo = Convert.ToString(h.AssemblyNo),
                                      SeamNo = Convert.ToString(h.SeamNo),
                                      SeamDesc = Convert.ToString(h.SeamNo),
                                      StageCode = Convert.ToString(h.StageCode + " - " + h.StageCodeDescription),
                                      MileStone = milestone,
                                      InspectionStatus = Convert.ToString(h.InspectionStatus),
                                      ReturnRemarks = Convert.ToString(h.ReturnRemarks),
                                      OfferedDate = h.OfferedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      AttendedDate = h.AttendedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                if (gridType == clsImplementationEnum.GridType.IPIDashboardPieChartData.GetStringValue())
                {
                    whereCondition = "1=1 and qms.InspectionStatus  IS NOT NULL";
                    string[] arrayAssembly = Assembly.Split(',').ToArray();
                    string lstAssembly = string.Format("`{0}`", string.Join("`,`", arrayAssembly)).Replace("'", "''").Replace("`", "'");

                    string[] arrayStageCode = StageCode.Split(',').ToArray();
                    string lstStageCode = string.Format("`{0}`", string.Join("`,`", arrayStageCode)).Replace("'", "''").Replace("`", "'");


                    if (!string.IsNullOrWhiteSpace(JobNo))
                    {
                        whereCondition += " and qms.Project in('" + JobNo + "')";
                    }
                    if (!string.IsNullOrWhiteSpace(Assembly))
                    {
                        whereCondition += " and qms12.AssemblyNo in(" + lstAssembly + ")";
                    }

                    if (!string.IsNullOrWhiteSpace(StageCode))
                    {
                        whereCondition += " and qms.StageCode in(" + lstStageCode + ")";
                    }
                    whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                    var lst = db.SP_DASHBOARD_SEAMLIST_DETAILS(1, int.MaxValue, "", whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Project = h.Project,
                                      AssemblyNo = Convert.ToString(h.AssemblyNo),
                                      SeamNo = Convert.ToString(h.SeamNo),
                                      StageCode = Convert.ToString(h.StageCode + " - " + h.StageCodeDescription),
                                      MileStone = MileStone,
                                      InspectionStatus = Convert.ToString(h.InspectionStatus),
                                      ReturnRemarks = Convert.ToString(h.ReturnRemarks),
                                      OfferedDate = h.OfferedDate == null || h.OfferedDate.Value == DateTime.MinValue ? "NA" : h.OfferedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      AttendedDate = h.AttendedDate == null || h.AttendedDate.Value == DateTime.MinValue ? "NA" : h.AttendedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region IPI PIE chart views
        [HttpPost]
        public ActionResult GetPartSeamChartResult(string JobNo, string Assembly, string MileStone, string StageCode)
        {
            var lstResult = db.SP_DASHBOARD_GET_SEAM_PART_DATA_COUNT(JobNo, Assembly, MileStone, StageCode).ToList();
            return Json(lstResult);
        }

        [HttpPost]
        public ActionResult GetSeamPartDataPartial(string JobNo, string Assembly, string MileStone, string StageCode, string InspectionStatus)
        {
            ViewBag.JobNo = JobNo;
            ViewBag.Assembly = Assembly;
            ViewBag.MileStone = MileStone;
            ViewBag.StageCode = StageCode;
            ViewBag.InspectionStatus = InspectionStatus;
            return PartialView("_GetSeamPartDataPartial");
        }

        public ActionResult loadSeamListDataTable(JQueryDataTableParamModel param, string JobNo, string Assembly, string MileStone, string StageCode, string InspectionStatus)
        {
            try
            {
                string whereCondition = "1=1";
                string[] arrayAssembly = Assembly.Split(',').ToArray();
                string lstAssembly = string.Format("`{0}`", string.Join("`,`", arrayAssembly)).Replace("'", "''").Replace("`", "'");

                string[] arrayStageCode = StageCode.Split(',').ToArray();
                string lstStageCode = string.Format("`{0}`", string.Join("`,`", arrayStageCode)).Replace("'", "''").Replace("`", "'");


                if (!string.IsNullOrWhiteSpace(JobNo))
                {
                    whereCondition += " and qms.Project in('" + JobNo + "')";
                }
                if (!string.IsNullOrWhiteSpace(Assembly))
                {
                    whereCondition += " and qms12.AssemblyNo in(" + lstAssembly + ")";
                }
                //if (!string.IsNullOrWhiteSpace(MileStone))
                //{
                //    whereCondition += " and MileStone in('" + MileStone + "')";
                //}
                if (!string.IsNullOrWhiteSpace(StageCode))
                {
                    whereCondition += " and qms.StageCode in(" + lstStageCode + ")";
                }
                if (!string.IsNullOrWhiteSpace(InspectionStatus))
                {
                    whereCondition += " and qms.InspectionStatus in('" + InspectionStatus + "')";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.InspectionStatus", "qms.Location", "qms.StageCode", "qms.LNTInspectorResult", "qms.Remarks" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    foreach (var item in param.sSearch.Trim().Split(' '))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(item.Trim());
                    }
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_DASHBOARD_SEAMLIST_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           h.Project,
                           Convert.ToString(h.AssemblyNo),
                           Convert.ToString(h.SeamNo),
                           Convert.ToString(h.SeamNo),
                           Convert.ToString(h.StageCode + " - " + h.StageCodeDescription),
                           MileStone,//Convert.ToString(h.MileStone),
                           Convert.ToString(h.InspectionStatus),
                           Convert.ToString(h.ReturnRemarks),
                          h.OfferedDate.HasValue ? h.OfferedDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) :"",
                           h.AttendedDate.HasValue ?  h.AttendedDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) :"",
            };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition + "|" + MileStone
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Access Request Partial

        public ActionResult AccessRequestPartial(int RequestId)
        {
            ATH020 objATH020 = new ATH020();
            return PartialView("_AccessRequestPartial", objATH020);
        }

        [HttpPost]
        public JsonResult LoadAccessRequestDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                strWhereCondition += " 1=1 and PSNo= '" + param.UserTypeId.Trim() + "'";
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                //search Condition 
                string[] columnName = { "RequestNo", "PSNo", "BU", "Location", "RoleId", "Status", "Remarks", "RefId", "CreatedOn" };

                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strRequested = clsImplementationEnum.AccessRequest.Requested.GetStringValue();
                var lstATH020 = db.ATH020.Where(x => x.PSNo.Trim() == param.UserTypeId.Trim()).ToList();
                bool isHideButton = false;
                if (lstATH020.Any(x => x.Status.Trim() == strRequested.Trim()))
                {
                    isHideButton = true;
                }

                var lstResult = db.SP_ATH_GET_ACCESSREQUEST_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.RequestId),
                             Convert.ToString(uc.RequestNo),
                            Convert.ToString(uc.PSNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.RoleId),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Remarks),
                          }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder,
                    isHideButton = isHideButton
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveAccessRequest(ATH020 ath020)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ATH020 objATH020 = new ATH020();
                objATH020.RequestNo = Convert.ToInt64(AccessRequestNoFormat());
                objATH020.PSNo = ath020.PSNo;
                objATH020.BU = ath020.BU;
                objATH020.Location = ath020.Location;
                objATH020.RoleId = ath020.RoleId;
                objATH020.Status = clsImplementationEnum.AccessRequest.Requested.GetStringValue();
                objATH020.Remarks = ath020.Remarks;
                objATH020.RefId = ath020.RefId;
                objATH020.CreatedBy = objClsLoginInfo.UserName;
                objATH020.CreatedOn = DateTime.Now;
                db.ATH020.Add(objATH020);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();

                #region Send Notification
                ATH004 objATH004 = db.ATH004.FirstOrDefault(c => c.Id == objATH020.RoleId.Value);
                (new clsManager()).SendNotificationByLocation(clsImplementationEnum.UserRoleName.ITA1.GetStringValue(), string.Empty, ath020.Location, "Access Request: " + objATH020.RequestNo + " has come for your approval for role " + objATH004.Role + " from PS No. " + objClsLoginInfo.UserName, clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), "/Authorization/AccessRequest");
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }





        #endregion

        #region Autocomple/Multiselect functions for IPI Pie chart
        [HttpPost]
        public ActionResult GetProjectResult(string term, string location = "")
        {
            List<Projects> lstProjects = new List<Projects>();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).Distinct().ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).Distinct().ToList();
            }
            else
            {
                lstProjects = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStages(string term, string mileStone = "")
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(mileStone))
            {
                if (mileStone != "Post Hydro")
                    lstCategoryData = db.QMS002.Where(x => x.PrePostWeldHeadTreatment.Trim() == mileStone.Trim()).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                else
                    lstCategoryData = db.QMS002.Where(x => x.IsHydroStage == true).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
            }
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetAssembyFindNo(string term, string project)
        {
            List<AssemblyFindNo> lstAssemblyFindNo = new List<AssemblyFindNo>();
            List<string> findNos = new List<string>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                findNos = Manager.GetHBOMData(project).Where(x => x.ProductType == "ASM"
                && x.FindNo.Contains(term)).Select(s => s.Part).Distinct().ToList();
                //findNos = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project
                //&& x.ProductType == "ASM"
                //&& x.FindNo.Contains(term)).Select(s => s.Part).Distinct().ToList();
            }
            else
            {
                findNos = Manager.GetHBOMData(project).Where(x => x.ProductType == "ASM").Select(s => s.Part).Distinct().Take(10).ToList();
                //findNos = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project && x.ProductType == "ASM").Select(s => s.Part).Distinct().Take(10).ToList();
            }

            if (findNos.Count > 0)
            {
                lstAssemblyFindNo = (from x in findNos
                                     select new AssemblyFindNo
                                     {
                                         Value = x.ToString(),
                                         FindNo = x.ToString(),
                                         FindNoDescription = x.ToString()
                                     }).ToList();
            }

            return Json(lstAssemblyFindNo, JsonRequestBehavior.AllowGet);
        }

        //multi select dropdown for Assembly
        [HttpPost]
        public ActionResult GetAssembly(string project)
        {
            List<string> findNos = new List<string>();
            findNos = Manager.GetHBOMData(project).Where(x => x.ProductType == "ASM").Select(s => s.Part).Distinct().ToList();
            //findNos = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project && x.ProductType == "ASM").Select(s => s.Part).Distinct().ToList();
            var lstAssembly = (from x in findNos
                               select new //AssemblyFindNo
                               {
                                   id = x,
                                   text = x
                               }).ToList();
            return Json(lstAssembly, JsonRequestBehavior.AllowGet);
        }

        //multi select dropdown for Stages
        [HttpPost]
        public ActionResult GetAllStages(string mileStone)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(mileStone))
            {
                if (mileStone != "Post Hydro")
                    lstCategoryData = db.QMS002.Where(x => x.PrePostWeldHeadTreatment.Trim() == mileStone.Trim())
                                    .OrderBy(o => o.SequenceNo)
                                    .Select(i =>
                                    new CategoryData { id = i.StageCode, text = i.StageCode + "-" + i.StageDesc }).ToList();
                else
                    lstCategoryData = db.QMS002.Where(x => x.IsHydroStage == true).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { id = i.StageCode, text = i.StageCode + "-" + i.StageDesc }).ToList();
            }
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region UTility
        [HttpPost]
        public ActionResult GetBu(string term)
        {
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            var lstBUs = Manager.getBUsByID(BU);
            var listBUs = (from a in lstBUs
                           select new { id = a.t_dimx, text = a.t_desc }).ToList();
            return Json(listBUs, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllBU(string term)
        {
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());

            var lstBUs = Manager.getBUsByID(BU);

            try
            {
                var items = (from li in lstBUs
                             select new CategoryData
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllLocation(string term)
        {
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

            var lstLocations = Manager.getBUsByID(LOC);

            try
            {
                var items = (from li in lstLocations
                             select new CategoryData
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetRole(string term)
        {
            var lstRoles = (from roles in db.ATH004
                            orderby roles.Role
                            select new CategoryData() { text = roles.Role + " - " + roles.Description, id = roles.Id.ToString() }).Distinct().ToList();
            return Json(lstRoles, JsonRequestBehavior.AllowGet);

        }

        public string AccessRequestNoFormat()
        {
            string requestdateformat = DateTime.Now.ToString("yyMMdd");
            int initialrequest = 1;
            var objAth020 = (from a in db.ATH020
                             where a.RequestNo.ToString().Contains(requestdateformat)
                             orderby a.RequestNo descending
                             select a).FirstOrDefault();
            if (objAth020 != null)
            {
                requestdateformat = (objAth020.RequestNo + 1).ToString();
            }
            else
            {
                requestdateformat = requestdateformat + initialrequest.ToString("D4");
            }
            return requestdateformat;
        }
        #endregion


    }
}
