﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.PMB.Controllers
{
    public class HeaderController : clsBase
    { 
        //modified by:Nikita Vibhandik (13-09-2017)
        //changes: added common function for Planning Din (PDN002) (Function name - Updatepdn002)
        // GET: PMB/Header
        [SessionExpireFilter]

        public ActionResult Index()
        {
            //clsUpload.GetFilesFromFolder("FDS001/1");
            return View();
        }
        [SessionExpireFilter]
        public ActionResult PMBDetail(int Id = 0)
        {
            PMB001 objPMB001 = new PMB001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            //List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            //ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("PMB001/" + objPMB001.HeaderId));

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv==1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.PMB001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objPMB001 = db.PMB001.Where(x => x.HeaderId == Id).FirstOrDefault();

                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objPMB001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPMB001.HeaderId, objPMB001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
                //if (objPMB001.CreatedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            return View(objPMB001);
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PMB001 objPMB001 = db.PMB001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPMB001 != null)
                {
                    objPMB001.RevNo = Convert.ToInt32(objPMB001.RevNo) + 1;
                    objPMB001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPMB001.ReviseRemark = strRemarks;
                    objPMB001.EditedBy = objClsLoginInfo.UserName;
                    objPMB001.EditedOn = DateTime.Now;
                    objPMB001.ReturnRemark = null;
                    objPMB001.ApprovedOn = null;
                    objPMB001.SubmittedBy = null;
                    objPMB001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPMB001.HeaderId;
                    objResponseMsg.Status = objPMB001.Status;
                    objResponseMsg.rev = objPMB001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    int RevNum; string docNum;
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);
                    /*var Exists = db.PMB001.Any(x => x.RevNo == null && x.Project == project);
                    if (Exists == false)
                    {*/
                    RevNum = 0;
                    /*}
                    else
                    {
                        int? revNo = (from rev in db.PMB001
                                      where rev.Project == project
                                      select rev.RevNo).FirstOrDefault();
                        RevNum = Convert.ToInt32(revNo) + 1;
                    }*/
                    var Exists2 = db.PMB001.Any(x => x.Project == project);
                    if (Exists2 == false)
                    {
                        project = project.Split('-')[0].ToString().Trim();
                        var ccdd = Manager.GetContractWiseCdd(project);
                        docNum = "02";
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = customer + "|" + RevNum + "|" + ccdd + "|" + docNum;

                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "exists";
                    }
                    //var contract = (from a in db.COM005
                    //                where a.t_sprj == project
                    //                select a.t_cono).FirstOrDefault();

                    //var ccdd = (from a in db.COM008
                    //            where a.t_cono == contract
                    //            select a.t_ccdd).FirstOrDefault();


                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.PMB001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.PMB001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePMBHeader(PMB001 pmb001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string s1, s2;
                s1 = fc["txtdoc"].ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);

                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objPMB001.Project = fc["hfProject"].ToString();

                    objPMB001.DocumentNo = Convert.ToInt32(s2);
                    objPMB001.Document = fc["txtdoc"].ToString();
                    objPMB001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    if (objPMB001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objPMB001.RevNo = Convert.ToInt32(objPMB001.RevNo) + 1;
                        objPMB001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objPMB001.ReturnRemark = null;
                        objPMB001.ApprovedOn = null;
                    }
                    /*if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objPMB001.CDD = null;
                    }
                    else
                    {
                        objPMB001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }*/
                    objPMB001.Product = fc["ddlCat"].ToString();
                    objPMB001.ProcessLicensor = fc["txtlicence"].ToString();
                    //objPMB001.Attachment = "";
                    //objPMB001.JobDescription = fc["ddlJob"].ToString();
                    objPMB001.EditedBy = objClsLoginInfo.UserName;
                    objPMB001.EditedOn = DateTime.Now;
                    objPMB001.ApprovedBy = fc["hfApp"].ToString().Split('-')[0].Trim();
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.Revision = "R" + objPMB001.RevNo.ToString();
                    objResponseMsg.status = objPMB001.Status;
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                }
                else
                {
                    string project = fc["txtProject"].ToString().Split('-')[0].Trim();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    PMB001 objPMB001 = new PMB001();
                    objPMB001.Project = fc["txtProject"].ToString().Split('-')[0].Trim();
                    objPMB001.DocumentNo = Convert.ToInt32(s2);
                    objPMB001.Document = fc["txtdoc"].ToString();
                    objPMB001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    objPMB001.RevNo = Convert.ToInt32(rev);
                    objPMB001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    /*if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objPMB001.CDD = null;
                    }
                    else
                    {
                        objPMB001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }*/
                    objPMB001.Product = fc["ddlCat"].ToString();
                    objPMB001.ProcessLicensor = fc["txtlicence"].ToString();
                    //objPMB001.Attachment = "";
                    //objPMB001.JobDescription = fc["ddlJob"].ToString();
                    objPMB001.CreatedBy = objClsLoginInfo.UserName;
                    objPMB001.CreatedOn = DateTime.Now;
                    objPMB001.ApprovedBy = fc["hfApp"].ToString().Split('-')[0].Trim();
                    db.PMB001.Add(objPMB001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                    objResponseMsg.Revision = "R" + objPMB001.RevNo.ToString();
                    objResponseMsg.status = objPMB001.Status;
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult sentForApproval(string Approver, int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerid > 0)
                {
                    Approver = Approver.Split('-')[0].Trim();
                    if (Approver.Trim() == objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
                    }
                    else
                    {
                        PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        objPMB001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objPMB001.ApprovedBy = Approver.Trim();
                        objPMB001.SubmittedBy = objClsLoginInfo.UserName;
                        objPMB001.SubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                            objPMB001.Project,
                                                            "",
                                                            "",
                                                            Manager.GetPDINDocumentNotificationMsg(objPMB001.Project, clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(), objPMB001.RevNo.Value.ToString(), objPMB001.Status),
                                                            clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                            Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(), objPMB001.HeaderId.ToString(), true),
                                                            objPMB001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetPMBGridPartial");
        }
        [HttpPost]
        public JsonResult LoadPMBHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and CreatedBy=" + user;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_PMB_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                              // Convert.ToString(uc.HeaderId),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               //Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               //Convert.ToString(uc.CDD),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                                "<nobr><center>"+
                                "<a title='View' href='/PMB/Header/PMBDetail?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                                 Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PMB/Header/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblPMBHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                                  (uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PMB/Header/GetHistoryView','Pre Manufacturing')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='' class='disabledicon fa fa-history'></i></a>")
                                +"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PMB/Header/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i class='iconspace fa fa-clock-o'></i></a>"
                                 + "</center></nobr>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Pre_Manufacturing);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "PMB TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PMB001_Log OBJPMB001_Log = db.PMB001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = OBJPMB001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001_Log.CreatedBy) : null;
                model.CreatedOn = OBJPMB001_Log.CreatedOn;
                model.EditedBy = OBJPMB001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001_Log.EditedBy) : null;
                model.EditedOn = OBJPMB001_Log.EditedOn;
                model.SubmittedBy = OBJPMB001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001_Log.SubmittedBy) : null;
                model.SubmittedOn = OBJPMB001_Log.SubmittedOn;

                model.ApprovedBy = OBJPMB001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001_Log.ApprovedBy) : null;
                model.ApprovedOn = OBJPMB001_Log.ApprovedOn;

            }
            else
            {

                PMB001_Log OBJPMB001_Log = db.PMB001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = OBJPMB001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001_Log.CreatedBy) : null;
                model.CreatedOn = OBJPMB001_Log.CreatedOn;
                model.EditedBy = OBJPMB001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001_Log.EditedBy) : null;
                model.EditedOn = OBJPMB001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "PMB Timeline";

            if (HeaderId > 0)
            {
                PMB001 OBJPMB001 = db.PMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = OBJPMB001.CreatedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001.CreatedBy) : null;
                model.CreatedOn = OBJPMB001.CreatedOn;
                model.EditedBy = OBJPMB001.EditedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001.EditedBy) : null;
                model.EditedOn = OBJPMB001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = OBJPMB001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001.SubmittedBy) : null;
                model.SubmittedOn = OBJPMB001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = OBJPMB001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001.ApprovedBy) : null;
                model.ApprovedOn = OBJPMB001.ApprovedOn;

            }
            else
            {
                PMB001 OBJPMB001 = db.PMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = OBJPMB001.CreatedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001.CreatedBy) : null;
                model.CreatedOn = OBJPMB001.CreatedOn;
                model.EditedBy = OBJPMB001.EditedBy != null ? Manager.GetUserNameFromPsNo(OBJPMB001.EditedBy) : null;
                model.EditedOn = OBJPMB001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public JsonResult RetractPMBHeader(int Id = 0)
        {
            ResponseMsg objResponseMsg = new ResponseMsg();
            try
            {
                var objPMB001 = db.PMB001.Where(q => q.HeaderId == Id).FirstOrDefault();
                if (objPMB001 != null)
                {
                    if (objPMB001.CreatedBy.Equals(objClsLoginInfo.UserName))
                    {
                        if (objPMB001.Status.ToLower().Equals("sent for approval"))
                        {
                            objPMB001.Status = "Draft";
                            objPMB001.SubmittedOn = null;
                            objPMB001.SubmittedBy = null;
                            db.Entry(objPMB001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Retacted Successfully";
                            Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Only headers that are sent for approval can be retracted.";
                        }
                    }
                    else
                    {

                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You are not authorized.";
                    }
                }
                else
                {

                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not found.";
                }
            }
            catch (Exception)
            {

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error while retracting Header.";

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        //[HttpPost]
        //public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        //{
        //    ViewBag.Role = strRole;
        //    ViewBag.HeaderId = HeaderId;
        //    return PartialView("_GetHistoryDetailsPartial");
        //}
        [HttpPost]
        public ActionResult GetHistoryView( int headerid=0)
        {
            PMB001_Log objPMB001 = new PMB001_Log();
           // ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            return PartialView("getHistoryPartial", objPMB001);
        }

        [HttpPost]
        public JsonResult LoadPMBHeaderHistoryData(JQueryDataTableParamModel param,string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " HeaderId = '" + HeaderId + "'";
                }
                //strWhere += "(CreatedBy = " + user + " or ApprovedBy = " + user+")";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_PMB_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                Convert.ToString(uc.Document),
                                Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                // Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                                //Convert.ToString(uc.CDD),
                                Convert.ToString(uc.Product),
                                Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            PMB001_Log objPMB001 = new PMB001_Log();
            var user = objClsLoginInfo.UserName;
            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("PMB001/" + objPMB001.HeaderId + "/" + objPMB001.RevNo));
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv==1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.PMB001_Log
                               where a.Id == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objPMB001 = db.PMB001_Log.Where(x => x.Id == Id).FirstOrDefault();
                //if (objPMB001.CreatedBy != objClsLoginInfo.UserName && objPMB001.ApprovedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            var temp = Request.Url.AbsolutePath;
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=')+1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.PMB001_Log.Any(q => (q.HeaderId == objPMB001.HeaderId && q.RevNo == (objPMB001.RevNo - 1))) ? urlPrefix + db.PMB001_Log.Where(q => (q.HeaderId == objPMB001.HeaderId && q.RevNo == (objPMB001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PMB001_Log.Any(q => (q.HeaderId == objPMB001.HeaderId && q.RevNo == (objPMB001.RevNo + 1))) ? urlPrefix + db.PMB001_Log.Where(q => (q.HeaderId == objPMB001.HeaderId && q.RevNo == (objPMB001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objPMB001);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveHeader(PMB001 pmb001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string s1, s2;
                s1 = pmb001.Document.ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);

                if (pmb001.HeaderId > 0)
                {
                    PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == pmb001.HeaderId).FirstOrDefault();
                    if (objPMB001 != null)
                    {
                        objPMB001.Product = pmb001.Product;
                        objPMB001.ReviseRemark = pmb001.ReviseRemark;
                        //objPMB001.JobDescription = pmb001.JobDescription;
                        objPMB001.ProcessLicensor = pmb001.ProcessLicensor;
                        objPMB001.ApprovedBy = pmb001.ApprovedBy.Split('-')[0].ToString().Trim();
                        objPMB001.EditedBy = objClsLoginInfo.UserName;
                        objPMB001.EditedOn = DateTime.Now;
                        if (objPMB001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        {
                            objPMB001.RevNo = Convert.ToInt32(objPMB001.RevNo) + 1;
                            objPMB001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        db.SaveChanges();
                        var newId = db.PMB001.Where(q => q.Project.Equals(objPMB001.Project)).FirstOrDefault().HeaderId;
                        
                        objResponseMsg.Key = true;
                        objResponseMsg.projdesc = objPMB001.HeaderId.ToString();
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.HeaderId = objPMB001.HeaderId;
                        objResponseMsg.RevNo = objPMB001.RevNo;
                        Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);                        
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PMB001 objPMB001 = db.PMB001.Add(new PMB001
                    {
                        Project = pmb001.Project.Split('-')[0].ToString().Trim(),
                        DocumentNo = Convert.ToInt32(s2),
                        Document = pmb001.Document,
                        Customer = pmb001.Customer.Split('-')[0].ToString().Trim(),
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        //CDD = Helper.ToNullIfTooEarlyForDb(pmb001.CDD),
                        Product = pmb001.Product,
                        ProcessLicensor = pmb001.ProcessLicensor,
                        //JobDescription = pmb001.JobDescription,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pmb001.ApprovedBy.Split('-')[0].ToString().Trim()
                    });
                    db.SaveChanges();
                    var newId = db.PMB001.Where(q => q.Project.Equals(objPMB001.Project)).FirstOrDefault().HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.projdesc = newId.ToString();
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objPMB001.HeaderId;
                    objResponseMsg.RevNo = objPMB001.RevNo;
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);                    
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PMB_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_PMB_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}