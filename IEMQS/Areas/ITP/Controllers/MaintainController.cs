﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ITP.Controllers
{
    public class MaintainController : clsBase
    {
        // GET: ITP/Maintain
        #region Header List

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }
        //load tab wise partial
        [HttpPost]
        public ActionResult LoadITPGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_ITPIndexGridHtmlPartial");
        }
        //load datatable for index
        [HttpPost]
        public JsonResult LoadITPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "where 1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "where 1=1";
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "Project", "SupplierName", "PurchaseOdrerNo", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                var lstResult = db.SP_ITP_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.SupplierName),
                            Convert.ToString(uc.PurchaseOdrerNo),
                              Convert.ToString(uc.Status),
                               "<center>"+
                                "<a title='View Details' href='"+WebsiteURL +"/ITP/Maintain/AddHeader?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+"  "+
                                "<i title='Show Timeline' class='fa fa-clock-o' style='cursor: pointer;margin-left:5px;' href='javascript:void(0)' onclick=ShowTimeline('/ITP/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            "</center>",
                          //"<center style='white-space: nowrap;' class='clsDisplayInlineEle'><a class='btn btn-xs green' href='/ITP/Maintain/AddHeader?HeaderID=" + uc.HeaderId + "'>View<i class='fa fa-eye'></i></a>"
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //timeline
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "ITP History";
            model.Title = "ITP";
            ITP001 objITP001 = db.ITP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objITP001.CreatedBy);
            model.CreatedOn = objITP001.CreatedOn;
            model.HTRpqrStatus = objITP001.Status;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objITP001.QC);
            model.ReviewedOn = objITP001.QCAcceptedOn;
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objITP001.Designer);
            model.FurnaceOn = objITP001.DesignAcceptedOn;
            // if only QC is selected/QC is approver
            if (!string.IsNullOrWhiteSpace(objITP001.QC) && string.IsNullOrWhiteSpace(objITP001.Designer))
            {
                model.HTRType = "QC";
            }
            //if only Designer is selected/Designer is approver
            else if (!string.IsNullOrWhiteSpace(objITP001.Designer) && string.IsNullOrWhiteSpace(objITP001.QC))
            {
                model.HTRType = "Designer";
            }
            // both are selected for approve
            else
            {
                model.HTRType = "Both";
            }
            //if(model.HTRType == '')
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        #endregion

        #region Add header
        [SessionExpireFilter]
        public ActionResult AddHeader(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        //Load partial for header
        [HttpPost]
        public ActionResult LoadAddUpdateFormPartial(int HeaderID)
        {
            ITP001 objITP001 = new ITP001();

            if (HeaderID > 0)
            {
                objITP001 = db.ITP001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objITP001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Supplier = db.COM006.Where(i => i.t_bpid == objITP001.SupplierName).Select(i => i.t_bpid + " - " + i.t_nama).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(objITP001.QC))
                {
                    ViewBag.QC = objITP001.QC + " - " + Manager.GetUserNameFromPsNo(objITP001.QC);
                }
                else
                {
                    ViewBag.QC = "";
                }
                if (!string.IsNullOrWhiteSpace(objITP001.Designer))
                {
                    ViewBag.Designer = objITP001.Designer + " - " + Manager.GetUserNameFromPsNo(objITP001.Designer);
                }
                else
                {
                    ViewBag.Designer = "";
                }
                ViewBag.Action = "edit";
            }
            else
            {
                objITP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            }
            ViewBag.MaterialCategory = clsImplementationEnum.GetITPMaterialCategory().ToArray();
            return PartialView("_AddUpdateFormPartial", objITP001);
        }

        //CRUD ITP header
        [HttpPost]
        public ActionResult SaveITPHeader(ITP001 itp001)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                ITP001 objITP001 = new ITP001();
                bool IsEditable = false;
                if (itp001.HeaderId > 0)
                {
                    objITP001 = db.ITP001.Where(a => a.HeaderId == itp001.HeaderId).FirstOrDefault();
                    IsEditable = true;
                }
                //objITP001.HeaderId = itp001.HeaderId;

                objITP001.SupplierName = itp001.SupplierName;
                objITP001.PurchaseOdrerNo = itp001.PurchaseOdrerNo;
                objITP001.MaterialCategory = itp001.MaterialCategory;
                objITP001.QC = itp001.QC;
                objITP001.Designer = itp001.Designer;
                objITP001.QCAcceptedOn = itp001.QCAcceptedOn;
                objITP001.DesignAcceptedOn = itp001.DesignAcceptedOn;
                objITP001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                if (IsEditable)
                {
                    objITP001.EditedBy = objClsLoginInfo.UserName;
                    objITP001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                }
                else
                {
                    objITP001.Project = itp001.Project;
                    objITP001.BU = db.COM001.Where(i => i.t_cprj == objITP001.Project).FirstOrDefault().t_entu;
                    objITP001.Location = objClsLoginInfo.Location;
                    objITP001.CreatedBy = objClsLoginInfo.UserName;
                    objITP001.CreatedOn = DateTime.Now;
                    db.ITP001.Add(objITP001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString();
                }
                objResponseMsg.HeaderId = objITP001.HeaderId;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line data
        //Lines partial
        [HttpPost]
        public ActionResult LoadLinesFormPartial(int HeaderId)
        {
            ITP002 objITP002 = new ITP002();
            ITP001 objITP001 = new ITP001();

            objITP001 = db.ITP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objITP001 != null)
            {
                //sp for dropdown list of PO Lines by Order number
                var lstOrder = db.SP_ITP_PO_BY_ORDER(objITP001.PurchaseOdrerNo).ToList();
                //fetch PO with item
                var lstPurchaseOrderLines = (from a in lstOrder
                                             select new DropdownModel { Code = Convert.ToString(a.OrderNo), Description = Convert.ToString(a.Item) }).Distinct().ToList();

                ViewBag.PurchaseOrder = lstPurchaseOrderLines;
                var lstLines = db.ITP002.Where(x => x.HeaderId == HeaderId).ToList();
                ViewBag.Status = objITP001.Status;

                //show buttons to add Po lines
                if (lstPurchaseOrderLines.Count() == lstLines.Count())
                {
                    ViewBag.Action = "invisible";
                }
                ViewBag.PurchaseOdrerNo = objITP001.PurchaseOdrerNo;
                objITP002.HeaderId = objITP001.HeaderId;
            }

            return PartialView("_GetITPLinesForm", objITP002);
        }
        //Load grid data
        [HttpPost]
        public JsonResult LoadITPLineData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;

                strWhere += "HeaderId=" + param.CTQHeaderId;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string[] columnName = { "POLine", "Item", "ItemDescription" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_ITP_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    HTMLAutoComplete(newRecordId,"txtPOLine","","",false,"","POLine")+""+Helper.GenerateHidden(newRecordId,"POLine"),//Helper.GenerateTextbox(newRecordId, "StageCode"),
                                    //Helper.GenerateTextbox(newRecordId, "StageSequance"),
                                    "",
                                    "",
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId",param.CTQHeaderId),
                                     Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewLine();" ),
                                };

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            GenerateAutoComplete(uc.LineId, "txtPOLine", Convert.ToString(uc.POLine),"UpdateLineDetails(this, "+ uc.LineId  +");",false,"","POLine",(string.Equals(param.MTStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())  ? true : false))+""+Helper.GenerateHidden(uc.LineId,"POLine",uc.POLine),
                            Convert.ToString(uc.Item),
                            Convert.ToString(uc.ItemDescription),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                           "<center>"+ HTMLActionString(uc.LineId,param.MTStatus, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord(" + uc.LineId + ");")+""+"</center>",
                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region insert/Update/delete data for PO Lines
        //Insert sing line 
        [HttpPost]
        public ActionResult InsertNewLine(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int newRowIndex = 0;
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                string tableName = string.Empty;

                ITP001 objITP001 = new ITP001();
                ITP002 objITP002 = new ITP002();

                objITP001 = db.ITP001.Where(a => a.HeaderId == headerId).FirstOrDefault();
                string po = fc["POLine" + newRowIndex].ToString();
                var lst = db.ITP002.Where(a => a.HeaderId == headerId && a.POLine == po).ToList();
                if (lst.Count() > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
                else
                {
                    var lstLines = db.SP_ITP_FETCH_PO_LINES(objITP001.PurchaseOdrerNo, fc["POLine" + newRowIndex]).FirstOrDefault();
                    if (lstLines != null)
                    {
                        objITP002.HeaderId = headerId;
                        objITP002.POLine = lstLines.orderno.ToString();
                        objITP002.Item = lstLines.item.ToString();
                        objITP002.ItemDescription = lstLines.itemDesc.ToString();
                        objITP002.CreatedBy = objClsLoginInfo.UserName;
                        objITP002.CreatedOn = DateTime.Now;
                        db.ITP002.Add(objITP002);
                        db.SaveChanges();
                        //objResponseMsg.HeaderStatus = objQMS020.Status;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //insert all Lines from LN
        [HttpPost]
        public ActionResult InsertAllPOLines(int headerid, string puchaseorderno)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                int headerId = Convert.ToInt32(headerid);
                string tableName = string.Empty;
                ITP001 objITP001 = new ITP001();
                objITP001 = db.ITP001.Where(a => a.HeaderId == headerId).FirstOrDefault();
                var lstExistingLines = db.ITP002.Where(o => o.HeaderId == headerid).Select(u => u.POLine).ToList();

                var lstOrder = db.SP_ITP_PO_BY_ORDER(puchaseorderno).Distinct().ToList();
                List<ITP002> lstPOLines = new List<ITP002>();
                if (lstOrder != null)
                {
                    foreach (var item in lstOrder)
                    {
                        if (!lstExistingLines.Contains(item.OrderNo.ToString()) && !lstPOLines.Any(i => i.POLine == item.OrderNo.ToString()))
                        {
                            ITP002 objITP002 = new ITP002();
                            objITP002.HeaderId = headerId;
                            objITP002.POLine = item.OrderNo.ToString();
                            objITP002.Item = item.LnItem.ToString();
                            objITP002.ItemDescription = item.itemDesc.ToString();
                            objITP002.CreatedBy = objClsLoginInfo.UserName;
                            objITP002.CreatedOn = DateTime.Now;
                            lstPOLines.Add(objITP002);
                        }
                    }

                    if (lstPOLines != null && lstPOLines.Count > 0)
                    {
                        db.ITP002.AddRange(lstPOLines);
                    }
                    db.SaveChanges();
                    //objResponseMsg.HeaderStatus = objQMS020.Status;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //Update line 
        [HttpPost]
        public ActionResult UpdateLineData(int lineid, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = string.Empty;
                //tableName = "QMS021";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    ITP001 objITP001 = new ITP001();
                    ITP002 objITP002 = new ITP002();

                    objITP002 = db.ITP002.Where(a => a.LineId == lineid).FirstOrDefault();
                    if (objITP002 != null)
                    {
                        var lst = db.ITP002.Where(a => a.HeaderId == objITP002.HeaderId && a.POLine == columnValue && a.LineId != lineid).ToList();
                        if (lst.Count() > 0)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                        else
                        {
                            objITP001 = db.ITP001.Where(a => a.HeaderId == objITP002.HeaderId).FirstOrDefault();
                            var lstLines = db.SP_ITP_FETCH_PO_LINES(objITP001.PurchaseOdrerNo, columnValue).FirstOrDefault();
                            if (lstLines != null)
                            {
                                objITP002.POLine = lstLines.orderno.ToString();
                                objITP002.Item = lstLines.item.ToString();
                                objITP002.ItemDescription = lstLines.itemDesc.ToString();
                                objITP002.EditedBy = objClsLoginInfo.UserName;
                                objITP002.EditedOn = DateTime.Now;
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                            }
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //delete line
        [HttpPost]
        public ActionResult DeleteRecord(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ITP002 objITP002 = new ITP002();
                {
                    objITP002 = db.ITP002.Where(x => x.LineId == lineid).FirstOrDefault();
                    db.ITP002.Remove(objITP002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region General functions for Grid

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()) && !string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }
            return htmlControl;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string GenerateLable(int rowId, string columnName, string columnValue = "", string inputStyle = "")
        {
            string strlbl = string.Empty;
            string lblID = columnName + "" + rowId.ToString();
            //string inputValue = columnValue;
            strlbl = "<label id='" + lblID + "'></label>";
            return strlbl;
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        #endregion

        #region approval
        //Send document for approval
        [HttpPost]
        public ActionResult SendForApproval(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ITP001 objITP001 = db.ITP001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objITP001 != null)
                {
                    objITP001.Status = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();
                    objITP001.SubmittedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //retract header (when status is send for approval)
        [HttpPost]
        public ActionResult RetractStatus(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ITP001 objITP001 = db.ITP001.Where(u => u.HeaderId == id).SingleOrDefault();
                if (objITP001 != null)
                {
                    objITP001.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objITP001.EditedBy = objClsLoginInfo.UserName;
                    objITP001.EditedOn = DateTime.Now;
                    objITP001.SubmittedOn = null;
                    //objITP001.Submitted= null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    objResponseMsg.Status = objITP001.Status;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region function for purchase order no. from supplier
        [HttpPost]
        public ActionResult GetOrderNo(string term, string supplier)
        {
            var lstOrder = db.SP_ITP_PURCHASEORDER(supplier).ToList();
            var lstPurchaseOrder = (from a in lstOrder
                                    where a.ORDERNO.Contains(term)
                                    select new DropdownModel { Code = a.ORDERNO, Description = a.ORDERNO }).ToList();
            return Json(lstPurchaseOrder, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult GetLinesByOrderNo(string term, string order)
        //{
        //    var lstOrder = db.SP_ITP_PURCHASEORDER(supplier, "POLINES","").ToList();
        //    var lstPurchaseOrderLines = (from a in lstOrder
        //                                 select new DropdownModel { Code = a.ContractNo, Description = a.ContractNo }).ToList();
        //    return Json(lstPurchaseOrderLines, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Export Excel
        //Code By : nikita vibhandik 19/12/2017 (observation  14096)
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                //header grid
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ITP_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.Project),
                                      SupplierName = Convert.ToString(uc.SupplierName),
                                      PurchaseOdrerNo = Convert.ToString(uc.PurchaseOdrerNo),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                //line grid
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {


                    var lst = db.SP_ITP_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(uc.ROW_NO),
                                      POLine = Convert.ToString(uc.POLine),
                                      Item = Convert.ToString(uc.Item),
                                      ItemDescription = Convert.ToString(uc.ItemDescription),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
            public bool ActionKey;
            public string ActionValue;
        }
    }
}