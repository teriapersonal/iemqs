﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ITP.Controllers
{
    public class ApproveByDesignerController : clsBase
    {
        // GET: ITP/ApproveByDesigner
        #region Header

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult ViewHeader(int HeaderID)
        {
            ITP001 objITP001 = new ITP001();

            if (HeaderID > 0)
            {
                objITP001 = db.ITP001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objITP001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objITP001.SupplierName = db.COM006.Where(i => i.t_bpid == objITP001.SupplierName).Select(i => i.t_bpid + " - " + i.t_nama).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(objITP001.QC))
                {
                    ViewBag.QC = objITP001.QC + " - " + Manager.GetUserNameFromPsNo(objITP001.QC);
                }
                else
                {
                    ViewBag.QC = "";
                }
                if (!string.IsNullOrWhiteSpace(objITP001.Designer))
                {
                    ViewBag.Designer = objITP001.Designer + " - " + Manager.GetUserNameFromPsNo(objITP001.Designer);
                }
                else
                {
                    ViewBag.Designer = "";
                }
                if (objITP001.Designer.Trim() == objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.Role = "d";
                }
                ViewBag.Action = "edit";
            }

            return View(objITP001);
        }

        [HttpPost]
        public ActionResult LoadITPGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_ITPIndexGridHtmlPartial");
        }

        [HttpPost]
        public JsonResult LoadITPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "where 1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "','" + clsImplementationEnum.CommonStatus.AcceptedByQC.GetStringValue() + "')";
                    strWhere += " and Designer='" + objClsLoginInfo.UserName + "'";
                }
                else
                {
                    strWhere += "where 1=1";
                    strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "Project", "SupplierName", "PurchaseOdrerNo", "Status" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ITP_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.SupplierName),
                            Convert.ToString(uc.PurchaseOdrerNo),
                             Convert.ToString(uc.Status),
                              "<center>"+
                                "<a title='View Details' href='"+WebsiteURL +"/ITP/ApproveByDesigner/ViewHeader?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+"  "+
                                "<i  class='fa fa-clock-o' style='cursor: pointer;margin-left:5px;' title='Show Timeline' href='javascript:void(0)' onclick=ShowTimeline('/ITP/ApproveByDesigner/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            "</center>",
                          //"<center style='white-space: nowrap;' class='clsDisplayInlineEle'><a class='btn btn-xs green' href='/ITP/ApproveByDesigner/ViewHeader?HeaderID=" + uc.HeaderId + "'>View<i class='fa fa-eye'></i></a>"
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "ITP History";
            model.Title = "ITP";
            ITP001 objITP001 = db.ITP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objITP001.CreatedBy);
            model.CreatedOn = objITP001.CreatedOn;
            model.HTRpqrStatus = objITP001.Status;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objITP001.QC);
            model.ReviewedOn = objITP001.QCAcceptedOn;
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objITP001.Designer);
            model.FurnaceOn = objITP001.DesignAcceptedOn;
            if (!string.IsNullOrWhiteSpace(objITP001.QC) && string.IsNullOrWhiteSpace(objITP001.Designer))
            {
                model.HTRType = "QC";
            }
            else if (!string.IsNullOrWhiteSpace(objITP001.Designer) && string.IsNullOrWhiteSpace(objITP001.QC))
            {
                model.HTRType = "Designer";
            }
            else
            {
                model.HTRType = "Both";
            }
            //if(model.HTRType == '')
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public JsonResult LoadITPLineData(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = string.Empty;

                strWhere += "HeaderId=" + param.CTQHeaderId;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string[] columnName = { "POLine", "Item", "ItemDescription" };
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ITP_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                             Convert.ToString(uc.POLine),
                            Convert.ToString(uc.Item),
                            Convert.ToString(uc.ItemDescription),
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveRecord(int strHeaderId, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ITP001 objITP001 = db.ITP001.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                if (objITP001 != null)
                {
                    if (!string.IsNullOrWhiteSpace(objITP001.QC) && objITP001.Status != clsImplementationEnum.CommonStatus.AcceptedByQC.GetStringValue())
                    {
                        objITP001.DesignComments = remarks;
                        objITP001.Status = clsImplementationEnum.CommonStatus.AcceptedByDesign.GetStringValue();
                        objITP001.DesignAcceptedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        objITP001.DesignComments = remarks;
                        objITP001.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        objITP001.DesignAcceptedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnRecord(int strHeaderId, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ITP001 objITP001 = db.ITP001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objITP001 != null)
                {
                    objITP001.Status = clsImplementationEnum.CommonStatus.Returned.GetStringValue();
                    objITP001.DesignComments = remarks;
                    objITP001.DesignRejectedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}