﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.Authenticate.Models
{
    #region Login Model
    /// <summary>
    /// Created by Dharmesh
    /// 26-06-2017
    /// Login Class Fields
    /// </summary>
    public class clsLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? RememberMe { get; set; }
    }

    #endregion
}