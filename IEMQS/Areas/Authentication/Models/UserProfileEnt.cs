﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.Authentication.Models
{
    public class UserProfileEnt
    {
        public UserProfileEnt()
        {
            listBU = new List<AccessRightEnt>();
            listLocation = new List<AccessRightEnt>();
            listRole = new List<AccessRightEnt>();
        }

        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Sex { get; set; }
        public string Extension { get; set; }
        public string Initial { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Department { get; set; }
        public DateTime? JoinDate { get; set; }
        public string BloodGroup { get; set; }
        public string FatherName { get; set; }
        public string Designation { get; set; }
        public string Location { get; set; }
        public int NoOfProjects { get; set; }
        public int NoOfRoles { get; set; }
        public int NoOfLocation { get; set; }
        public int NoOfBU { get; set; }
        public List<AccessRightEnt> listBU { get; set; }
        public List<AccessRightEnt> listLocation { get; set; }
        public List<AccessRightEnt> listRole { get; set; }
        public string EmailId { get; set; }
        public string PSNumber { get; set; }

        public string url { get; set; }
        public string RoleGroup { get; set; }
    }

    public class AccessRightEnt
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}