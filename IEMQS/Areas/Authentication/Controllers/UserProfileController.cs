﻿using IEMQS.Areas.Authentication.Models;
using IEMQS.DESServices;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authenticate.Controllers
{
    public class UserProfileController : clsBase
    {
        // GET: Authentication/UserProfile
        public ActionResult Index()
        {
            return RedirectToAction("ViewProfile", "UserProfile");
        }
        public ActionResult ViewProfile()
        {
            if (objClsLoginInfo != null)
            {
                UserProfileEnt userProfileEnt = GetUserProfileDtlEnt();                
                return View(userProfileEnt);
            }
            else
            {
                return RedirectToAction("Index", "Authenticate");
            }
        }

        public UserProfileEnt GetUserProfileDtlEnt()
        {
            UserProfileEnt userProfileEnt = GetUserProfileDtl();
            var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
            var objBU = objATH001.Select(i => i.BU).Distinct().ToList();
            var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
            var objRole = objATH001.Select(i => i.Role).Distinct().ToList();
            userProfileEnt.listBU = (from c1 in db.COM002
                                     where c1.t_dtyp == 2 && objBU.Contains(c1.t_dimx)
                                     select new AccessRightEnt { Name = c1.t_desc, Code = c1.t_dimx }
                                   ).Distinct().ToList();
            userProfileEnt.listLocation = (from c1 in db.COM002
                                           where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                           select new AccessRightEnt { Name = c1.t_desc, Code = c1.t_dimx }
                                   ).Distinct().ToList();
            userProfileEnt.listRole = (from c4 in db.ATH004
                                       where objRole.Contains(c4.Id)
                                       select new AccessRightEnt { Name = c4.Description, Code = c4.Role }
                                   ).Distinct().ToList();
            userProfileEnt.NoOfLocation = objLocation.Count();
            userProfileEnt.NoOfRoles = objRole.Count();
            userProfileEnt.NoOfBU = objBU.Count();
            var getLoginInfo = CommonService.objClsLoginInfo;
            userProfileEnt.url = "/MyDashboard/Dashboard/Index";
           
            string[] rolelist = getLoginInfo.GetUserRole().Split(',');
            List<string> rolegrouplist = db.ATH004.Where(c => rolelist.Contains(c.Role)).Select(c => c.RoleGroup).Distinct().ToList();
            userProfileEnt.RoleGroup = string.Join(",", rolegrouplist);

            var IsDESLive = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsDESLive.GetStringValue()));
            if (IsDESLive)
            {
                if (rolegrouplist.Any(x => x.ToString() == "ENGG"))
                {
                    userProfileEnt.url = "/Des/Dashboard";
                }
                else
                {
                    userProfileEnt.url = "/Mydashboard/Dashboard/Index";
                }
            }

            return userProfileEnt;
        }
        public ActionResult AccountDetail()
        {
            UserProfileEnt userProfileEnt = GetUserProfileDtl();

            return PartialView("_AccountDetailPartial", userProfileEnt);
        }
        public ActionResult AuthorizationDetail()
        {
            return PartialView("_UserAuthorizationDetailPartial");
        }

        [HttpPost]
        public JsonResult LoadAuthMatrixData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                //var role = Convert.ToString(Request["role"]);
                //var bu = Convert.ToString(Request["bu"]);
                //var location = Convert.ToString(Request["location"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                Func<ATH001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                string strWhere = string.Empty;
                strWhere = "1=1 ";
                //if (!string.IsNullOrWhiteSpace(role))
                //{
                //    strWhere += " and Role like '%" + role + "%'";
                //}
                //if (!string.IsNullOrWhiteSpace(bu))
                //{
                //    strWhere += " and BU='" + bu + "'";
                //}
                //if (!string.IsNullOrWhiteSpace(location))
                //{
                //    strWhere += " and Location='" + location + "'";
                //}
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (BU like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or Contract like '%" + param.sSearch
                        + "%' or Project like '%" + param.sSearch
                        + "%' or Role like '%" + param.sSearch
                        + "%')";

                }

                #region Sorting                
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstATH001 = db.SP_ATH_GET_ATH_DETAILS_FOR_PROFILE
                                (
                               objClsLoginInfo.UserName, StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstATH001
                            select new[]
                           {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Contract),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.Role),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", string contract = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_ATH_GET_ATH_DETAILS_FOR_PROFILE_Result> lst = db.SP_ATH_GET_ATH_DETAILS_FOR_PROFILE(objClsLoginInfo.UserName, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = li.BU,
                                      Location = li.Location,
                                      Contract = li.Contract,
                                      Project = li.Project,
                                      Role = li.Role
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProfileProjects.GetStringValue())
                {
                    List<SP_AUTH_GET_PROFILE_PROJECT_DETAILS_Result> lst = db.SP_AUTH_GET_PROFILE_PROJECT_DETAILS(objClsLoginInfo.UserName, contract, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.t_cprj,
                                      Description = li.t_dsca
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProfileContracts.GetStringValue())
                {
                    List<SP_AUTH_GET_PROFILE_CONTRACT_DETAILS_Result> lst = db.SP_AUTH_GET_PROFILE_CONTRACT_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Contract = li.t_cono,
                                      Description = li.t_desc
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProfileProcess.GetStringValue())
                {
                    List<SP_AUTH_GET_ROLE_WISE_PROCESS_DETAILS_Result> lst = db.SP_AUTH_GET_ROLE_WISE_PROCESS_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Role = li.Role,
                                      Process = li.Process,
                                      Description = li.Description
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        private UserProfileEnt GetUserProfileDtl()
        {
            var objUserProfile = db.COM003.Where(i => i.t_psno == objClsLoginInfo.UserName && i.t_actv == 1).FirstOrDefault();
            var objEmailId = db.COM007.Where(i => i.t_emno == objClsLoginInfo.UserName).FirstOrDefault();
            UserProfileEnt userProfileEnt = new UserProfileEnt();
            if (objUserProfile != null)
            {
                //var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).ToList();
                var objDepartment = db.COM002.Where(i => i.t_dimx == objUserProfile.t_depc && i.t_dtyp == 3).FirstOrDefault();
                var objLocation = db.COM002.Where(i => i.t_dimx == objUserProfile.t_loca && i.t_dtyp == 1).FirstOrDefault();
                userProfileEnt.Name = objUserProfile.t_name;
                userProfileEnt.ShortName = objUserProfile.t_namb;
                userProfileEnt.Sex = objUserProfile.t_sexx == 1 ? "Male" : "Female";
                userProfileEnt.Extension = objUserProfile.t_extn;
                userProfileEnt.Initial = objUserProfile.t_init;
                userProfileEnt.BirthDate = objUserProfile.t_bdat;
                userProfileEnt.Department = objDepartment != null ? objDepartment.t_desc : string.Empty;
                userProfileEnt.JoinDate = objUserProfile.t_jdat;
                userProfileEnt.BloodGroup = objUserProfile.t_bgrp;
                userProfileEnt.FatherName = objUserProfile.t_fshb;
                userProfileEnt.Designation = objUserProfile.t_desi;
                userProfileEnt.Location = objLocation != null ? objLocation.t_desc : string.Empty;
                userProfileEnt.EmailId = objEmailId != null && objEmailId.t_mail != null ? objEmailId.t_mail : string.Empty;
                userProfileEnt.PSNumber = objUserProfile.t_psno ;
                userProfileEnt.NoOfProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, string.Empty).ToList().Count;

            }
            return userProfileEnt;
        }

        public ActionResult UserContractDetails(string bu)
        {
            ViewBag.BU = bu;
            return PartialView("_UserContractDetails");
        }

        public ActionResult UserProjectDetails(string bu, string contract)
        {
            ViewBag.BU = bu;
            ViewBag.Contract = contract;
            return PartialView("_UserProjectDetails");
        }

        public ActionResult RoleWiseProcessDetails(string role)
        {
            ViewBag.Role = role;
            return PartialView("_RoleWiseProcessDetails");
        }

        [HttpPost]
        public JsonResult LoadProjectDetails(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var contract = Convert.ToString(Request["contract"]);
                var bu = Convert.ToString(Request["bu"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                Func<ATH001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                string strWhere = string.Empty;
                strWhere = "1=1 ";
                strWhere += " and t_csbu = '" + bu + "' ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (t_cprj like '%" + param.sSearch
                        + "%' or t_dsca like '%" + param.sSearch
                        + "%')";
                }

                #region Sorting                
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstATH001 = db.SP_AUTH_GET_PROFILE_PROJECT_DETAILS
                                (
                                objClsLoginInfo.UserName, contract, StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstATH001
                            select new[]
                           {
                           Convert.ToString(uc.t_cprj),
                           Convert.ToString(uc.t_dsca)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadContractDetails(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var bu = Convert.ToString(Request["bu"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                Func<ATH001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                string strWhere = string.Empty;
                strWhere = "1=1 ";
                strWhere += " and t_csbu = '" + bu + "' ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (t_cono like '%" + param.sSearch
                        + "%' or t_desc like '%" + param.sSearch
                        + "%')";
                }

                #region Sorting                
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstATH001 = db.SP_AUTH_GET_PROFILE_CONTRACT_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstATH001
                            select new[]
                           {
                           Convert.ToString(uc.t_cono),
                           Convert.ToString(uc.t_desc)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadRoleWiseProcessDetails(JQueryDataTableParamModel param,string role)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);                
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
             
                string strWhere = string.Empty;
                strWhere = "a1.Employee='"+objClsLoginInfo.UserName+"'";
                if (!string.IsNullOrEmpty(role))
                {
                    strWhere += " and a4.Role='" + role + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (a2.Process like '%" + param.sSearch
                        + "%' or a2.Description like '%" + param.sSearch
                        + "%')";
                }

                #region Sorting                
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstdata = db.SP_AUTH_GET_ROLE_WISE_PROCESS_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstdata
                            select new[]
                           {
                           Convert.ToString(uc.Role),
                           Convert.ToString(uc.Process),
                           Convert.ToString(uc.Description)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstdata.Count > 0 && lstdata.FirstOrDefault().TotalCount > 0 ? lstdata.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstdata.Count > 0 && lstdata.FirstOrDefault().TotalCount > 0 ? lstdata.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}