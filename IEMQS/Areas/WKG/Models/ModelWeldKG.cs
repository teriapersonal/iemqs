﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;


namespace IEMQS.Areas.WKG.Models
{
    public class ModelWeldKG
    {
        public static double radianOf1 = 0.017453293;
        public double FaceSideArea { get; set; }
        public double CBArea { get; set; }

        public ModelWeldKG CalculateFaceSideCBackArea(string wepType, double minThk, double thk1, double thk2, int SeamLengthArea)
        {
            ModelWeldKG objModelWeldKG = new ModelWeldKG();

            //decimal fSArea = 0, cbackArea = 0;
            double faceSideArea = 0, cbArea = 0, half = (double)1 / 2, half3 = (double)3 / 2, th1 = Convert.ToDouble(thk1), th2 = Convert.ToDouble(thk2);

            switch (wepType)
            {
                case "W1":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * 30) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * 30)) * 3;
                    //faceSideArea = L4 * 1 + (L4 - 2) * (L4 - 2) * Math.Tan(RADIANS(30)) + (1 + 2 * (L4 - 2) * Math.Tan(RADIANS(30))) * 3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L4>30,12*12,6*6)*PI()/2+IF(L4>30,12,6)*2*3;
                    break;
                case "W2":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * 22.5) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * 22.5)) * 3;
                    //faceSideArea = L5*1+(L5-2)*(L5-2)*TAN(RADIANS(22.5))+(1+2*(L5-2)*TAN(RADIANS(22.5)))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L5>30,12*12,6*6)*PI()/2+IF(L5>30,12,6)*2*3;
                    break;
                case "W3":
                    faceSideArea = (Convert.ToDouble(2) / 3 * minThk + 2) * 1 + Convert.ToDouble(2) / 3 * minThk * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * 30) + (1 + 2 * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * 30)) * 3;
                    //faceSideArea = (2/3*L6+2)*1+2/3*L6*2/3*L6*TAN(RADIANS(30))+(1+2*2/3*L6*TAN(RADIANS(30)))*3;
                    cbArea = Convert.ToDouble(1) / 3 * minThk * Math.Tan(radianOf1 * 45) * Convert.ToDouble(1) / 3 * minThk + Math.PI * ((minThk > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * Convert.ToDouble(1) / 3 * minThk * Math.Tan(radianOf1 * 45)) * 3 + 1 * Convert.ToDouble(1) / 3 * minThk;
                    //cbArea = 1/3*L6*TAN(RADIANS(45))*1/3*L6+PI()*(IF(L6>30,12*12,6*6)/2)+(1+2*1/3*L6*TAN(RADIANS(45)))*3+1*1/3*L6;
                    break;
                case "W4":
                    faceSideArea = (Convert.ToDouble(2) / 3 * minThk + 2) * 1 + Convert.ToDouble(2) / 3 * minThk * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * 22.5) + (1 + 2 * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * 22.5)) * 3;
                    //faceSideArea = (2/3*L7+2)*1+2/3*L7*2/3*L7*TAN(RADIANS(22.5))+(1+2*2/3*L7*TAN(RADIANS(22.5)))*3;
                    cbArea = Convert.ToDouble(1) / 3 * minThk * Math.Tan(radianOf1 * 30) * Convert.ToDouble(1) / 3 * minThk + Math.PI * ((minThk > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * Convert.ToDouble(1) / 3 * minThk * Math.Tan(radianOf1 * 30)) * 3 + 1 * Convert.ToDouble(1) / 3 * minThk;
                    //cbArea = 1/3*L7*TAN(RADIANS(30))*1/3*L7+PI()*(IF(L7>30,12*12,6*6)/2)+(1+2*1/3*L7*TAN(RADIANS(30)))*3+1*1/3*L7;
                    break;
                case "W5":
                    faceSideArea = 2 * 0.55 * minThk + (180 - 5) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * 5 / 2) * 8 * Math.Cos(radianOf1 * 5 / 2) + (0.55 * minThk - 8) * 8 * Math.Cos(radianOf1 * 5 / 2) * 2 + (0.55 * minThk - 7.651) * (0.55 * minThk - 7.651) * Math.Tan(radianOf1 * 5 / 2) + (2 + 2 * 8 * Math.Cos(radianOf1 * 5 / 2) + 2 * (0.55 * minThk - 7.651) * Math.Tan(radianOf1 * 5 / 2)) * 3;
                    //faceSideArea = 2*0.55*L8+(180-5)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+(0.55*L8-8)*8*COS(RADIANS(5/2))*2+(0.55*L8-7.651)*(0.55*L8-7.651)*TAN(RADIANS(5/2))+(2+2*8*COS(RADIANS(5/2))+2*(0.55*L8-7.651)*TAN(RADIANS(5/2)))*3;
                    cbArea = ((minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + 2 * (0.45 * minThk) + (180 - 5) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * 5 / 2) * 8 * Math.Cos(radianOf1 * 5 / 2) + ((0.45 * minThk - 8) - 8) * 8 * Math.Cos(radianOf1 * 5 / 2) * 2 + ((0.45 * minThk - 8) - 7.651) * ((0.45 * minThk - 8) - 7.651) * Math.Tan(radianOf1 * 5 / 2) + (2 + 2 * 8 * Math.Cos(radianOf1 * 5 / 2) + 2 * ((0.45 * minThk - 8) - 7.651) * Math.Tan(radianOf1 * 5 / 2)) * 3);
                    //cbArea = (IF(L8>30,12*12,6*6)*PI()/2+2*(0.45*L8)+(180-5)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+((0.45*L8-8)-8)*8*COS(RADIANS(5/2))*2+((0.45*L8-8)-7.651)*((0.45*L8-8)-7.651)*TAN(RADIANS(5/2))+(2+2*8*COS(RADIANS(5/2))+2*((0.45*L8-8)-7.651)*TAN(RADIANS(5/2)))*3);
                    break;
                case "W6":
                    faceSideArea = 2 * 0.55 * minThk + (180 - 5) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * 5 / 2) * 8 * Math.Cos(radianOf1 * 5 / 2) + (0.55 * minThk - 8) * 8 * Math.Cos(radianOf1 * 5 / 2) * 2 + (0.55 * minThk - 7.651) * (0.55 * minThk - 7.651) * Math.Tan(radianOf1 * 5 / 2) + (2 + 2 * 8 * Math.Cos(radianOf1 * 5 / 2) + 2 * (0.55 * minThk - 7.651) * Math.Tan(radianOf1 * 5 / 2)) * 3;
                    //faceSideArea = 2*0.55*L9+(180-5)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+(0.55*L9-8)*8*COS(RADIANS(5/2))*2+(0.55*L9-7.651)*(0.55*L9-7.651)*TAN(RADIANS(5/2))+(2+2*8*COS(RADIANS(5/2))+2*(0.55*L9-7.651)*TAN(RADIANS(5/2)))*3;
                    cbArea = ((minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + 2 * (0.45 * minThk) + (180 - 5) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * 5 / 2) * 8 * Math.Cos(radianOf1 * 5 / 2) + ((0.45 * minThk - 8) - 8) * 8 * Math.Cos(radianOf1 * 5 / 2) * 2 + ((0.45 * minThk - 8) - 7.651) * ((0.45 * minThk - 8) - 7.651) * Math.Tan(radianOf1 * 5 / 2) + (2 + 2 * 8 * Math.Cos(radianOf1 * 5 / 2) + 2 * ((0.45 * minThk - 8) - 7.651) * Math.Tan(radianOf1 * 5 / 2)) * 3);
                    //cbArea = (IF(L9>30,12*12,6*6)*PI()/2+2*(0.45*L9)+(180-5)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+((0.45*L9-8)-8)*8*COS(RADIANS(5/2))*2+((0.45*L9-8)-7.651)*((0.45*L9-8)-7.651)*TAN(RADIANS(5/2))+(2+2*8*COS(RADIANS(5/2))+2*((0.45*L9-8)-7.651)*TAN(RADIANS(5/2)))*3);
                    break;
                case "W7":
                    faceSideArea = minThk + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 1 / 2) * 10 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 10) * 10 * Math.Cos(radianOf1 * 1 / 2) * 2 + (minThk - 18 - 9.913) * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 1 / 2) + 2 * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 1 / 2)) * 3;
                    //faceSideArea = L10+(180-1)*PI()*10*10/360+10*SIN(RADIANS(1/2))*10*COS(RADIANS(1/2))+(L10-18-10)*10*COS(RADIANS(1/2))*2+(L10-18-9.913)*(L10-18-9.913)*TAN(RADIANS(1/2))+(1+2*10*COS(RADIANS(1/2))+2*(L10-18-9.913)*TAN(RADIANS(1/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L10>30,12,6)*(IF(L10>30,12,6)+18)*PI()/2+IF(L10>30,12,12)*2*3;
                    break;
                case "W8":
                    faceSideArea = minThk + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 1 / 2) * 10 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 10) * 10 * Math.Cos(radianOf1 * 1 / 2) * 2 + (minThk - 8 - 9.913) * (minThk - 8 - 9.913) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 1 / 2) + 2 * (minThk - 8 - 9.913) * Math.Tan(radianOf1 * 1 / 2)) * 3;
                    //faceSideArea = L11+(180-1)*PI()*10*10/360+10*SIN(RADIANS(1/2))*10*COS(RADIANS(1/2))+(L11-8-10)*10*COS(RADIANS(1/2))*2+(L11-8-9.913)*(L11-8-9.913)*TAN(RADIANS(1/2))+(1+2*10*COS(RADIANS(1/2))+2*(L11-8-9.913)*TAN(RADIANS(1/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L11>30,12,6)*(IF(L11>30,12,6)+8)*PI()/2+IF(L11>30,12,12)*2*3;
                    break;
                case "W9":
                    faceSideArea = minThk + (90 - 0.5) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 2) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 20) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 19.83) * (minThk - 18 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * (minThk - 18) * Math.Tan(radianOf1 * 1 / 2) * 3)) + ((minThk - 18) * (minThk - 18) * Math.Tan(radianOf1 * 1 / 2) / 2);
                    //faceSideArea = L12+(90-0.5)*PI()*20*20/360+20*SIN(RADIANS(1/2))*20*COS(RADIANS(1/2))+(L12-18-20)*20*COS(RADIANS(1/2))+(L12-18-19.83)*(L12-18-19.83)*TAN(RADIANS(1/2))+(1+2*20*COS(RADIANS(1/2))+(L12-18-19.83)*TAN(RADIANS(1/2))+(1+2*(L12-18)*TAN(RADIANS(1/2))*3))+((L12-18)*(L12-18)*TAN(RADIANS(1/2))/2);
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L12>30,12,6)*(IF(L12>30,12,6)+18)*PI()/2+IF(L12>30,12,12)*2*3;
                    break;
                case "W10":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 2) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * (minThk - 8) * Math.Tan(radianOf1 * 1 / 2) * 3)) + ((minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 1 / 2) / 2);
                    //faceSideArea = L13+(90-1)*PI()*20*20/360+20*SIN(RADIANS(1/2))*20*COS(RADIANS(1/2))+(L13-8-20)*20*COS(RADIANS(1/2))+(L13-8-19.83)*(L13-8-19.83)*TAN(RADIANS(1/2))+(1+2*20*COS(RADIANS(1/2))+(L13-8-19.83)*TAN(RADIANS(1/2))+(1+2*(L13-8)*TAN(RADIANS(1/2))*3))+((L13-8)*(L13-8)*TAN(RADIANS(1/2))/2);
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L13>30,12,6)*(IF(L13>30,12,6)+8)*PI()/2+IF(L13>30,12,12)*2*3;
                    break;
                case "W11":
                    faceSideArea = (minThk + 12) + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 1 / 2) * 10 * Math.Cos(radianOf1 * 1 / 2) + ((minThk + 4) - 10) * 10 * Math.Cos(radianOf1 * 1 / 2) * 2 + ((minThk + 4) - 9.913) * ((minThk + 4) - 9.913) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 1 / 2) + 2 * ((minThk + 4) - 9.913) * Math.Tan(radianOf1 * 1 / 2)) * 3;
                    //faceSideArea =(L14+12)+(180-1)*PI()*10*10/360+10*SIN(RADIANS(1/2))*10*COS(RADIANS(1/2))+((L14+4)-10)*10*COS(RADIANS(1/2))*2+((L14+4)-9.913)*((L14+4)-9.913)*TAN(RADIANS(1/2))+(1+2*10*COS(RADIANS(1/2))+2*((L14+4)-9.913)*TAN(RADIANS(1/2)))*3;
                    break;
                case "W12":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * 30) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * 30)) * 5;
                    //faceSideArea =L15*1+(L15-2)*(L15-2)*TAN(RADIANS(30))+(1+2*(L15-2)*TAN(RADIANS(30)))*5
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //faceSideArea =IF(L15>30,12*12,6*6)*PI()/2+IF(L15>30,12,6)*2*3;
                    break;
                case "W13":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * 22.5) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * 22.5)) * 5;
                    //faceSideArea = L16*1+(L16-2)*(L16-2)*TAN(RADIANS(22.5))+(1+2*(L16-2)*TAN(RADIANS(22.5)))*5;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L16>30,12*12,6*6)*PI()/2+IF(L16>30,12,6)*2*3;
                    break;
                case "W14":
                    faceSideArea = (Convert.ToDouble(2) / 3 * minThk) * 1 + (Convert.ToDouble(2) / 3 * minThk - 2) * (Convert.ToDouble(2) / 3 * minThk - 2) * Math.Tan(radianOf1 * 22.5) + (1 + 2 * (Convert.ToDouble(2) / 3 * minThk - 2) * Math.Tan(radianOf1 * 22.5)) * 5;
                    //faceSideArea =(2/3*L17)*1+(2/3*L17-2)*(2/3*L17-2)*TAN(RADIANS(22.5))+(1+2*(2/3*L17-2)*TAN(RADIANS(22.5)))*5;
                    cbArea = (Convert.ToDouble(1) / 3 * minThk + 2) * Math.Tan(radianOf1 * 30) * (Convert.ToDouble(1) / 3 * minThk + 2) + Math.PI * ((minThk > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * (Convert.ToDouble(1) / 3 * minThk + 2) * Math.Tan(radianOf1 * 30)) * 3 + 1 * Convert.ToDouble(1) / 3 * minThk;
                    //cbArea = (1/3*L17+2)*TAN(RADIANS(30))*(1/3*L17+2)+PI()*(IF(L17>30,12*12,6*6)/2)+(1+2*(1/3*L17+2)*TAN(RADIANS(30)))*3+1*1/3*L17;
                    break;
                case "W15":
                    faceSideArea = minThk + (90 - 0.5) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 2) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * (minThk - 8) * Math.Tan(radianOf1 * 1 / 2) * 5)) + ((minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 1 / 2) / 2);
                    //faceSideArea = L18+(90-0.5)*PI()*20*20/360+20*SIN(RADIANS(1/2))*20*COS(RADIANS(1/2))+(L18-8-20)*20*COS(RADIANS(1/2))+(L18-8-19.83)*(L18-8-19.83)*TAN(RADIANS(1/2))+(1+2*20*COS(RADIANS(1/2))+(L18-8-19.83)*TAN(RADIANS(1/2))+(1+2*(L18-8)*TAN(RADIANS(1/2))*5))+((L18-8)*(L18-8)*TAN(RADIANS(1/2))/2);
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L18>30,12,6)*(IF(L18>30,12,6)+8)*PI()/2+IF(L18>30,12,12)*2*3;
                    break;
                case "W16":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (30)) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * (30))) * 3;
                    //faceSideArea = L19*1+(L19-2)*(L19-2)*TAN(RADIANS(30))+(1+2*(L19-2)*TAN(RADIANS(30)))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L19>30,12*12,6*6)*PI()/2+IF(L19>30,12,6)*2*3;
                    break;
                case "W17":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (22.5)) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * (22.5))) * 3;
                    //faceSideArea = L20*1+(L20-2)*(L20-2)*TAN(RADIANS(22.5))+(1+2*(L20-2)*TAN(RADIANS(22.5)))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L20>30,12*12,6*6)*PI()/2+IF(L20>30,12,6)*2*3;
                    break;
                case "W18":
                    faceSideArea = (minThk - 12 + 2) * 1 + (minThk - 12) * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * (22.5)) + (1 + 2 * (minThk - 12) * Math.Tan(radianOf1 * (22.5))) * 3;
                    //faceSideArea = (L21-12+2)*1+(L21-12)*2/3*L21*TAN(RADIANS(22.5))+(1+2*(L21-12)*TAN(RADIANS(22.5)))*3;
                    cbArea = 10 * Math.Tan(radianOf1 * (45)) * 10 + Math.PI * ((minThk > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * 10 * Math.Tan(radianOf1 * (45))) * 3 + 1 * Convert.ToDouble(1) / 3 * minThk;
                    //cbArea = 10*TAN(RADIANS(45))*10+PI()*(IF(L21>30,12*12,6*6)/2)+(1+2*10*TAN(RADIANS(45)))*3+1*1/3*L21;
                    break;
                case "W19":
                    faceSideArea = minThk + (90 - 0.5) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 2) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 20) * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 19.83) * (minThk - 18 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 20 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * (minThk - 18) * Math.Tan(radianOf1 * 1 / 2) * 3)) + ((minThk - 18) * (minThk - 18) * Math.Tan(radianOf1 * 1 / 2) / 2);
                    //faceSideArea = L22+(90-0.5)*PI()*20*20/360+20*SIN(RADIANS(1/2))*20*COS(RADIANS(1/2))+(L22-18-20)*20*COS(RADIANS(1/2))+(L22-18-19.83)*(L22-18-19.83)*TAN(RADIANS(1/2))+(1+2*20*COS(RADIANS(1/2))+(L22-18-19.83)*TAN(RADIANS(1/2))+(1+2*(L22-18)*TAN(RADIANS(1/2))*3))+((L22-18)*(L22-18)*TAN(RADIANS(1/2))/2);
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L22>30,12,6)*(IF(L22>30,12,6)+18)*PI()/2+IF(L22>30,12,12)*2*3;
                    break;
                case "W20":
                    faceSideArea = minThk * 1 + 13 * 13 * Math.Tan(radianOf1 * 75 / 2) + 13 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 15) + (minThk - 15) * (minThk - 15) * Math.Tan(radianOf1 * 2 / 2) + (1 + 13 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 15) * Math.Tan(radianOf1 * 2 / 2) * 2) * 3;
                    //faceSideArea = L23*1+13*13*TAN(RADIANS(75/2))+13*TAN(RADIANS(75/2))*2*(L23-15)+(L23-15)*(L23-15)*TAN(RADIANS(2/2))+(1+13*TAN(RADIANS(75/2))*2+(L23-15)*TAN(RADIANS(2/2))*2)*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L23>30,12*12,6*6)*PI()/2+IF(L23>30,12,6)*2*3;
                    break;
                case "W21":
                    faceSideArea = minThk * 1 + 13 * 13 * Math.Tan(radianOf1 * 75 / 2) + 13 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 15) + (minThk - 15) * (minThk - 15) * Math.Tan(radianOf1 * 1 / 2) + (1 + 13 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 15) * Math.Tan(radianOf1 * 1 / 2) * 2) * 3;
                    //faceSideArea = L24*1+13*13*TAN(RADIANS(75/2))+13*TAN(RADIANS(75/2))*2*(L24-15)+(L24-15)*(L24-15)*TAN(RADIANS(1/2))+(1+13*TAN(RADIANS(75/2))*2+(L24-15)*TAN(RADIANS(1/2))*2)*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L24>30,12*12,6*6)*PI()/2+IF(L24>30,12,6)*2*3;
                    break;
                case "W22":
                    faceSideArea = (minThk * minThk * Math.Tan(radianOf1 * 15 / 2)) + minThk * 10 + (10 + minThk * Math.Tan(radianOf1 * 15 / 2)) * 3;
                    //faceSideArea = (L25*L25*TAN(RADIANS(15/2)))+L25*10+(10+L25*TAN(RADIANS(15/2)))*3;
                    cbArea = (minThk > 30 ? 20 * 12 : 20 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L25>30,20*12,20*6)*PI()/2+IF(L25>30,12,6)*2*3;
                    break;
                case "W23":
                    faceSideArea = (minThk * minThk * Math.Tan(radianOf1 * 1 / 2)) + minThk * 20 + (20 + minThk * Math.Tan(radianOf1 * 1 / 2)) * 3;
                    //faceSideArea = (L26*L26*TAN(RADIANS(1/2)))+L26*20+(20+L26*TAN(RADIANS(1/2)))*3;
                    cbArea = (minThk > 30 ? 24 * 12 : 24 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L26>30,24*12,24*6)*PI()/2+IF(L26>30,12,6)*2*3;
                    break;
                case "W24":
                    faceSideArea = (minThk - 40 - 2) * 1 + (minThk - 40 - 2) * (minThk - 40 - 2) * Math.Tan(radianOf1 * (22.5)) + (1 + 2 * (minThk - 40 - 2) * Math.Tan(radianOf1 * (22.5))) * 3;
                    //faceSideArea = (L27-40-2)*1+(L27-40-2)*(L27-40-2)*TAN(RADIANS(22.5))+(1+2*(L27-40-2)*TAN(RADIANS(22.5)))*3;
                    cbArea = (40 + 2) * Math.Tan(radianOf1 * (30)) * (40 + 2) + Math.PI * ((minThk > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * (40 + 2) * Math.Tan(radianOf1 * (30))) * 3 + 1 * 42;
                    //cbArea = IF(L26>30,24*12,24*6)*PI()/2+IF(L26>30,12,6)*2*3;
                    break;
                case "W25":
                    faceSideArea = 2 * (minThk - 100 - 10) + (180 - 4) * Math.PI * 12 * 12 / 360 + 12 * Math.Sin(radianOf1 * 4 / 2) * 12 * Math.Cos(radianOf1 * 4 / 2) + (minThk - 100 - 10 - 12) * 12 * Math.Cos(radianOf1 * 4 / 2) * 2 + (minThk - 100 - 10 - 11.9) * (minThk - 100 - 10 - 11.9) * Math.Tan(radianOf1 * 4 / 2) + (2 + 2 * 12 * Math.Cos(radianOf1 * 4 / 2) + 2 * (minThk - 100 - 10 - 11.9) * Math.Tan(radianOf1 * 4 / 2)) * 3;
                    //faceSideArea = 2*(L28-100-10)+(180-4)*PI()*12*12/360+12*SIN(RADIANS(4/2))*12*COS(RADIANS(4/2))+(L28-100-10-12)*12*COS(RADIANS(4/2))*2+(L28-100-10-11.9)*(L28-100-10-11.9)*TAN(RADIANS(4/2))+(2+2*12*COS(RADIANS(4/2))+2*(L28-100-10-11.9)*TAN(RADIANS(4/2)))*3;
                    cbArea = ((minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + 2 * (110) + (180 - 7) * Math.PI * 12 * 12 / 360 + 12 * Math.Sin(radianOf1 * 7 / 2) * 12 * Math.Cos(radianOf1 * 7 / 2) + ((100 - 12)) * 12 * Math.Cos(radianOf1 * 7 / 2) * 2 + ((100) - 7.651) * ((100) - 11.9) * Math.Tan(radianOf1 * 7 / 2) + (2 + 2 * 12 * Math.Cos(radianOf1 * 7 / 2) + 2 * ((100) - 11.9) * Math.Tan(radianOf1 * 7 / 2)) * 3);
                    //cbArea = (IF(L28>30,12*12,6*6)*PI()/2+2*(110)+(180-7)*PI()*12*12/360+12*SIN(RADIANS(7/2))*12*COS(RADIANS(7/2))+((100-12))*12*COS(RADIANS(7/2))*2+((100)-7.651)*((100)-11.9)*TAN(RADIANS(7/2))+(2+2*12*COS(RADIANS(7/2))+2*((100)-11.9)*TAN(RADIANS(7/2)))*3);
                    break;
                case "W26":
                    faceSideArea = minThk * 1 + 13.5 * 13.5 * Math.Tan(radianOf1 * 75 / 2) + 13.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 15) + (minThk - 15) * (minThk - 15) * Math.Tan(radianOf1 * 4 / 2) + (1 + 13.5 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 15) * Math.Tan(radianOf1 * 4 / 2) * 2) * 3;
                    //faceSideArea = L29 * 1 + 13.5 * 13.5 * TAN(RADIANS(75 / 2)) + 13.5 * TAN(RADIANS(75 / 2)) * 2 * (L29 - 15) + (L29 - 15) * (L29 - 15) * TAN(RADIANS(4 / 2)) + (1 + 13.5 * TAN(RADIANS(75 / 2)) * 2 + (L29 - 15) * TAN(RADIANS(4 / 2)) * 2) * 3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L29>30,12*12,6*6)*PI()/2+IF(L29>30,12,6)*2*3;
                    break;
                case "W27":
                    faceSideArea = minThk * 1 + 13.5 * 13.5 * Math.Tan(radianOf1 * 75 / 2) + 13.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 15) + (minThk - 15) * (minThk - 15) * Math.Tan(radianOf1 * 2 / 2) + (1 + 13.5 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 15) * Math.Tan(radianOf1 * 2 / 2) * 2) * 3;
                    //faceSideArea = L30*1+13.5*13.5*TAN(RADIANS(75/2))+13.5*TAN(RADIANS(75/2))*2*(L30-15)+(L30-15)*(L30-15)*TAN(RADIANS(2/2))+(1+13.5*TAN(RADIANS(75/2))*2+(L30-15)*TAN(RADIANS(2/2))*2)*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L30>30,12*12,6*6)*PI()/2+IF(L30>30,12,6)*2*3;
                    break;

                //Observation 15196 Start
                case "W28":
                    faceSideArea = (180 - 20) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 20 / 2) * 10 * Math.Cos(radianOf1 * 20 / 2) + (0.5 * minThk - 3 - 10) * 10 * Math.Cos(radianOf1 * 20 / 2) * 2 + (0.5 * minThk - 3 - 9.913) * (0.5 * minThk - 3 - 9.913) * Math.Tan(radianOf1 * 20 / 2) + (0 + 2 * 10 * Math.Cos(radianOf1 * 20 / 2) + 2 * (0.5 * minThk - 3 - 9.913) * Math.Tan(radianOf1 * 10 / 2)) * 3;
                    //faceSideArea = (180-20)*PI()*10*10/360+10*SIN(RADIANS(20/2))*10*COS(RADIANS(20/2))+(0.5*L31-3-10)*10*COS(RADIANS(20/2))*2+(0.5*L31-3-9.913)*(0.5*L31-3-9.913)*TAN(RADIANS(20/2))+(0+2*10*COS(RADIANS(20/2))+2*(0.5*L31-3-9.913)*TAN(RADIANS(10/2)))*3;
                    cbArea = (180 - 20) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 20 / 2) * 10 * Math.Cos(radianOf1 * 20 / 2) + (0.5 * minThk - 3 - 10) * 10 * Math.Cos(radianOf1 * 20 / 2) * 2 + (0.5 * minThk - 3 - 9.913) * (0.5 * minThk - 3 - 9.913) * Math.Tan(radianOf1 * 20 / 2) + (0 + 2 * 10 * Math.Cos(radianOf1 * 20 / 2) + 2 * (0.5 * minThk - 3 - 9.913) * Math.Tan(radianOf1 * 10 / 2)) * 3;
                    //cbArea = (180-20)*PI()*10*10/360+10*SIN(RADIANS(20/2))*10*COS(RADIANS(20/2))+(0.5*L31-3-10)*10*COS(RADIANS(20/2))*2+(0.5*L31-3-9.913)*(0.5*L31-3-9.913)*TAN(RADIANS(20/2))+(0+2*10*COS(RADIANS(20/2))+2*(0.5*L31-3-9.913)*TAN(RADIANS(10/2)))*3;
                    break;
                case "W29":
                    faceSideArea = minThk + (180 - 10) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 10 / 2) * 10 * Math.Cos(radianOf1 * 10 / 2) + (minThk - 18 - 10) * 10 * Math.Cos(radianOf1 * 10 / 2) * 2 + (minThk - 18 - 9.913) * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 10 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 10 / 2) + 2 * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 10 / 2)) * 3;
                    //faceSideArea = L32+(180-10)*PI()*10*10/360+10*SIN(RADIANS(10/2))*10*COS(RADIANS(10/2))+(L32-18-10)*10*COS(RADIANS(10/2))*2+(L32-18-9.913)*(L32-18-9.913)*TAN(RADIANS(10/2))+(1+2*10*COS(RADIANS(10/2))+2*(L32-18-9.913)*TAN(RADIANS(10/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L32>30,12,6)*(IF(L32>30,12,6)+18)*PI()/2+IF(L32>30,12,12)*2*3;
                    break;
                case "W30":
                    faceSideArea = minThk * 1 + ((minThk - 2) * (minThk - 2) / 2 * Math.Tan(radianOf1 * 40) + ((minThk - 2) * (minThk - 2) / 2 * Math.Tan(radianOf1 * 5) + (1 + (minThk - 2) * Math.Tan(radianOf1 * 5 + (minThk - 2) * Math.Tan(radianOf1 * 40)) * 3)));
                    //faceSideArea = L33*1+((L33-2)*(L33-2)/2*TAN(RADIANS(40))+((L33-2)*(L33-2)/2*TAN(RADIANS(5))+(1+(L33-2)*TAN(RADIANS(5)+(L33-2)*TAN(RADIANS(40)))*3)));
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L33>30,12*12,6*6)*PI()/2+IF(L33>30,12,6)*2*3;
                    break;
                case "W31":
                    faceSideArea = (minThk - 10 - 12 + 2) * 1 + (minThk - 10 - 12) * (minThk - 10 - 12) * Math.Tan(radianOf1 * 30) + (1 + 2 * (minThk - 10 - 12) * Math.Tan(radianOf1 * 30)) * 3;
                    //faceSideArea = (L34-10-12+2)*1+(L34-10-12)*(L34-10-12)*TAN(RADIANS(30))+(1+2*(L34-10-12)*TAN(RADIANS(30)))*3;
                    cbArea = 10 * Math.Tan(radianOf1 * 45) * 10 + (24 * 24 / 2) + (1 + 2 * 10 * Math.Tan(radianOf1 * 45)) * 3 + 1 * 22;
                    //cbArea = 10*TAN(RADIANS(45))*10+(24*24/2)+(1+2*10*TAN(RADIANS(45)))*3+1*22;
                    break;
                case "W32":
                    faceSideArea = (0.63 * minThk + 2) * 1 + (0.63 * minThk - 8) * (0.63 * minThk - 8) * Math.Tan(radianOf1 * 22.5) + (1 + 2 * (0.63 * minThk - 8) * Math.Tan(radianOf1 * 22.5)) * 3;
                    //faceSideArea = (0.63*L35+2)*1+(0.63*L35-8)*(0.63*L35-8)*TAN(RADIANS(22.5))+(1+2*(0.63*L35-8)*TAN(RADIANS(22.5)))*3;
                    cbArea = 0.37 * minThk * Math.Tan(radianOf1 * 45) * 0.37 * minThk + (400 / 2) + (1 + 2 * 0.37 * minThk * Math.Tan(radianOf1 * 30)) * 3 + 1 * 0.37 * minThk;
                    //cbArea = 0.37*L35*TAN(RADIANS(45))*0.37*L35+(400/2)+(1+2*0.37*L35*TAN(RADIANS(30)))*3+1*0.37*L35;
                    break;
                case "W33":
                    faceSideArea = minThk + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 1 / 2) * 10 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 10) * 10 * Math.Cos(radianOf1 * 1 / 2) * 2 + (minThk - 18 - 9.913) * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 1 / 2) + 2 * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 1 / 2)) * 3;
                    //faceSideArea = L36+(180-1)*PI()*10*10/360+10*SIN(RADIANS(1/2))*10*COS(RADIANS(1/2))+(L36-18-10)*10*COS(RADIANS(1/2))*2+(L36-18-9.913)*(L36-18-9.913)*TAN(RADIANS(1/2))+(1+2*10*COS(RADIANS(1/2))+2*(L36-18-9.913)*TAN(RADIANS(1/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L36>30,12,6)*(IF(L36>30,12,6)+18)*PI()/2+IF(L36>30,12,12)*2*3;
                    break;
                case "W34":
                    faceSideArea = minThk + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 1 / 2) * 10 * Math.Cos(radianOf1 * 1 / 2) + (minThk - 18 - 10) * 10 * Math.Cos(radianOf1 * 1 / 2) * 2 + (minThk - 18 - 9.913) * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 1 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 1 / 2) + 2 * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 1 / 2)) * 3;
                    //faceSideArea = L37+(180-1)*PI()*10*10/360+10*SIN(RADIANS(1/2))*10*COS(RADIANS(1/2))+(L37-18-10)*10*COS(RADIANS(1/2))*2+(L37-18-9.913)*(L37-18-9.913)*TAN(RADIANS(1/2))+(1+2*10*COS(RADIANS(1/2))+2*(L37-18-9.913)*TAN(RADIANS(1/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L37>30,12,6)*(IF(L37>30,12,6)+18)*PI()/2+IF(L37>30,12,12)*2*3;
                    break;
                case "W35":
                    faceSideArea = minThk + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 5 / 2) * 10 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 18 - 10) * 10 * Math.Cos(radianOf1 * 5 / 2) * 2 + (minThk - 18 - 9.913) * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 5 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 5 / 2) + 2 * (minThk - 18 - 9.913) * Math.Tan(radianOf1 * 5 / 2)) * 3;
                    //faceSideArea = L38+(180-1)*PI()*10*10/360+10*SIN(RADIANS(5/2))*10*COS(RADIANS(5/2))+(L38-18-10)*10*COS(RADIANS(5/2))*2+(L38-18-9.913)*(L38-18-9.913)*TAN(RADIANS(5/2))+(1+2*10*COS(RADIANS(5/2))+2*(L38-18-9.913)*TAN(RADIANS(5/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 18) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L38>30,12,6)*(IF(L38>30,12,6)+18)*PI()/2+IF(L38>30,12,12)*2*3;
                    break;
                case "W36":
                    faceSideArea = minThk + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 5 / 2) * 10 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 8 - 10) * 10 * Math.Cos(radianOf1 * 5 / 2) * 2 + (minThk - 8 - 9.913) * (minThk - 8 - 9.913) * Math.Tan(radianOf1 * 5 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 5 / 2) + 2 * (minThk - 8 - 9.913) * Math.Tan(radianOf1 * 5 / 2)) * 3;
                    //faceSideArea = L39+(180-1)*PI()*10*10/360+10*SIN(RADIANS(5/2))*10*COS(RADIANS(5/2))+(L39-8-10)*10*COS(RADIANS(5/2))*2+(L39-8-9.913)*(L39-8-9.913)*TAN(RADIANS(5/2))+(1+2*10*COS(RADIANS(5/2))+2*(L39-8-9.913)*TAN(RADIANS(5/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L39>30,12,6)*(IF(L39>30,12,6)+8)*PI()/2+IF(L39>30,12,12)*2*3;
                    break;
                case "W37":
                    faceSideArea = minThk + (90 - 2.5) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 5 / 2) * 20 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 22 - 20) * 20 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 22 - 19.83) * (minThk - 22 - 19.83) * Math.Tan(radianOf1 * 5 / 2) + (1 + 2 * 20 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 18 - 19.83) * Math.Tan(radianOf1 * 5 / 2) + (1 + 2 * (minThk - 22) * Math.Tan(radianOf1 * 5 / 2) * 3)) + ((minThk - 22) * (minThk - 22) * Math.Tan(radianOf1 * 5 / 2) / 2);
                    //faceSideArea = L40+(90-2.5)*PI()*20*20/360+20*SIN(RADIANS(5/2))*20*COS(RADIANS(5/2))+(L40-22-20)*20*COS(RADIANS(5/2))+(L40-22-19.83)*(L40-22-19.83)*TAN(RADIANS(5/2))+(1+2*20*COS(RADIANS(5/2))+(L40-18-19.83)*TAN(RADIANS(5/2))+(1+2*(L40-22)*TAN(RADIANS(5/2))*3))+((L40-22)*(L40-22)*TAN(RADIANS(5/2))/2);
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 22) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L40>30,12,6)*(IF(L40>30,12,6)+22)*PI()/2+IF(L40>30,12,12)*2*3;
                    break;
                case "W38":
                    faceSideArea = minThk + (90 - 2.5) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 5 / 2) * 20 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 5 / 2) + (1 + 2 * 20 * Math.Cos(radianOf1 * 5 / 2) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 5 / 2) + (1 + 2 * (minThk - 8) * Math.Tan(radianOf1 * 1 / 2) * 5)) + ((minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 5 / 2) / 2);
                    //faceSideArea = L41+(90-2.5)*PI()*20*20/360+20*SIN(RADIANS(5/2))*20*COS(RADIANS(5/2))+(L41-8-20)*20*COS(RADIANS(5/2))+(L41-8-19.83)*(L41-8-19.83)*TAN(RADIANS(5/2))+(1+2*20*COS(RADIANS(5/2))+(L41-8-19.83)*TAN(RADIANS(5/2))+(1+2*(L41-8)*TAN(RADIANS(1/2))*5))+((L41-8)*(L41-8)*TAN(RADIANS(5/2))/2);
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L41>30,12,6)*(IF(L41>30,12,6)+8)*PI()/2+IF(L41>30,12,12)*2*3;
                    break;
                #region Observation30533-HardikMasalawala       
                case "W39":
                    faceSideArea = (180 - 0) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * 5 / 2) * 8 * Math.Cos(radianOf1 * 5 / 2) + (0.45 * minThk - 0 - 8) * 8 * Math.Cos(radianOf1 * 5 / 2) * 2 + (0.45 * minThk - 1 - 7.7) * (0.45 * minThk - 0 - 7.7) * Math.Tan(radianOf1 * 5 / 2) + (0 + 2 * 8 * Math.Cos(radianOf1 * 5 / 2) + 2 * (0.45 * minThk - 0 - 7.7) * Math.Tan(radianOf1 * 8 / 2)) * 3;
                    //faceSideArea = =(180-0)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+(0.45*L45-0-8)*8*COS(RADIANS(5/2))*2+(0.45*L45-1-7.7)*(0.45*L45-0-7.7)*TAN(RADIANS(5/2))+(0+2*8*COS(RADIANS(5/2))+2*(0.45*L45-0-7.7)*TAN(RADIANS(8/2)))*3
                    cbArea = (180 - 0) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * 5 / 2) * 8 * Math.Cos(radianOf1 * 5 / 2) + (0.55 * minThk - 8 - 8) * 8 * Math.Cos(radianOf1 * 5 / 2) * 2 + (0.55 * minThk - 8 - 7.7) * (0.55 * minThk - 8 - 7.7) * Math.Tan(radianOf1 * 5 / 2) + (0 + 2 * 8 * Math.Cos(radianOf1 * 5 / 2) + 2 * (0.55 * minThk - 8 - 7.7) * Math.Tan(radianOf1 * 8 / 2)) * 3 + (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = (180-0)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+(0.55*L45-8-8)*8*COS(RADIANS(5/2))*2+(0.55*L45-8-7.7)*(0.55*L45-8-7.7)*TAN(RADIANS(5/2))+(0+2*8*COS(RADIANS(5/2))+2*(0.55*L45-8-7.7)*TAN(RADIANS(8/2)))*3+ IF(L45>30,12,6)*(IF(L45>30,12,6)+8)*PI()/2+IF(L45>30,12,12)*2*3
                    break;
                #endregion
                case "W40":
                    faceSideArea = (180 - 0) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 4 / 2) * 10 * Math.Cos(radianOf1 * 4 / 2) + (minThk - 10 - 10) * 10 * Math.Cos(radianOf1 * 4 / 2) * 2 + (minThk - 10 - 9.913) * (minThk - 10 - 9.913) * Math.Tan(radianOf1 * 4 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 4 / 2) + 2 * (minThk - 10 - 9.913) * Math.Tan(radianOf1 * 4 / 2)) * 3;
                    //=(180-0)*PI()*10*10/360+10*SIN(RADIANS(4/2))*10*COS(RADIANS(4/2))+(L46-10-10)*10*COS(RADIANS(4/2))*2+(L46-10-9.913)*(L46-10-9.913)*TAN(RADIANS(4/2))+(1+2*10*COS(RADIANS(4/2))+2*(L46-10-9.913)*TAN(RADIANS(4/2)))*3
                    //OLD
                    //faceSideArea = minThk + (180 - 4) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * 4 / 2) * 10 * Math.Cos(radianOf1 * 4 / 2) + (minThk - 10 - 10) * 10 * Math.Cos(radianOf1 * 4 / 2) * 2 + (minThk - 10 - 9.913) * (minThk - 10 - 9.913) * Math.Tan(radianOf1 * 4 / 2) + (1 + 2 * 10 * Math.Cos(radianOf1 * 4 / 2) + 2 * (minThk - 10 - 9.913) * Math.Tan(radianOf1 * 4 / 2)) * 3;
                    //faceSideArea = L42+(180-4)*PI()*10*10/360+10*SIN(RADIANS(4/2))*10*COS(RADIANS(4/2))+(L42-10-10)*10*COS(RADIANS(4/2))*2+(L42-10-9.913)*(L42-10-9.913)*TAN(RADIANS(4/2))+(1+2*10*COS(RADIANS(4/2))+2*(L42-10-9.913)*TAN(RADIANS(4/2)))*3;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 10) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //=IF(L46>30,12,6)*(IF(L46>30,12,6)+10)*PI()/2+IF(L46>30,12,12)*2*3
                    //OLD
                    //cbArea = 22 * 22 + 22 * 3;
                    //cbArea = 22*22+22*3;
                    break;
                case "W43":
                    faceSideArea = minThk * 10 + (0.5 * minThk * minThk * Math.Tan(radianOf1 * 20) + (0.5 * minThk * minThk * Math.Tan(radianOf1 * 30))) + minThk * Math.Tan(radianOf1 * 20) + minThk * Math.Tan(radianOf1 * 20) * 3;
                    //= L49 * 10 + (0.5 * L49 * L49 * TAN(RADIANS(20)) + (0.5 * L49 * L49 * TAN(RADIANS(30)))) + L49 * TAN(RADIANS(20)) + L49 * TAN(RADIANS(20)) * 3
                    //OLD
                    //faceSideArea = (minThk + 1) * 10 + (minThk - 0) * (minThk - 0) * Math.Tan(radianOf1 * 25) + (10 + 2 * (minThk - 0) * Math.Tan(radianOf1 * 25)) * 3;
                    //faceSideArea = (L43+1)*10+(L43-0)*(L43-0)*TAN(RADIANS(25))+(10+2*(L43-0)*TAN(RADIANS(25)))*3;
                    cbArea = 0;
                    //cbArea = 0;
                    break;
                //Observation 15196 End
                #region  Observation 30533 START
                case "W45":
                    faceSideArea = minThk * 12 * 4 + 1 * minThk;
                    //L50 * 12 * 4 + 1 * L50
                    cbArea = minThk * 12 * 4 + 1 * minThk;
                    //L50 * 12 * 4 + 1 * L50
                    break;
                case "W46":
                    faceSideArea = 12 * 4 + 1 * minThk;
                    //=12*4+1*L51
                    cbArea = 12 * 4 + 1 * minThk;
                    //=12*4+1*L51
                    break;
                case "W47":
                    faceSideArea = 12 * 4 + 1 * minThk + (0.577 * (minThk * 0.5 - 7) * 2) * 1 / 2 * (minThk * 0.5 - 7);
                    //12*4+1*L52+(0.577*(L52*0.5-7)*2)*1/2*(L52*0.5-7)
                    cbArea = 12 * 4 + 1 * minThk;
                    //=12*4+1*L51
                    break;
                case "W112":
                    faceSideArea = minThk * 3 + 5 * 5 * Math.Tan(radianOf1 * 75 / 2) + 5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 5) + (minThk - 5) * (minThk - 5) * Math.Tan(radianOf1 * 2 / 2) + minThk * 10 + (3 + 10 + 5 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 5) * Math.Tan(radianOf1 * 2 / 2)) * 3;
                    //L53*3+5*5*TAN(RADIANS(75/2))+5*TAN(RADIANS(75/2))*2*(L53-5)+(L53-5)*(L53-5)*TAN(RADIANS(2/2))+L53*10+(3+10+5*TAN(RADIANS(75/2))*2+(L53-5)*TAN(RADIANS(2/2)))*3
                    cbArea = 0;
                    //=12*4+1*L51
                    break;

                case "W113":
                    faceSideArea = minThk * 3 + (12 * 0.767 * 12 / 2) + ((minThk - 12 + 4) * 0.1763 * (minThk - 12 + 4) / 2) + (4 * 0.767 * 4 / 2) + minThk * 0.1763 * minThk / 2 + (3 + 12 * 0.767 + 4 * 0.767 + 2 * minThk * 0.1763) * 3;
                    //L54*3+ (12*0.767*12/2)+((L54-12+4)*0.1763*(L54-12+4)/2)+(4*0.767*4/2)+L54*0.1763*L54/2 + (3+12*0.767+4*0.767+2*L54*0.1763)*3
                    cbArea = 0;
                    //=12*4+1*L51
                    break;


                case "W201":
                    faceSideArea = minThk * 3 + 0.5774 * minThk * minThk / 2 + (3 + 0.5774 * minThk) * 3;
                    //L55*3+ 0.5774*L55*L55/2+(3+0.5774*L55)*3
                    cbArea = 6 * 6;
                    //=6*6
                    break;


                case "W202":
                    faceSideArea = 0.5 * minThk * 3 + 0.5774 * (0.5 * minThk) * (0.5 * minThk) / 2 + (3 + 0.5774 * minThk * 0.5) * 3;
                    //=0.5*L56*3+ 0.5774*(0.5*L56)*(0.5*L56)/2+(3+0.5774*L56*0.5)*3
                    cbArea = 6 * 6 + 0.5 * minThk * 3 + 0.5774 * (0.5 * minThk) * (0.5 * minThk) / 2 + (3 + 0.5774 * minThk * 0.5) * 3;
                    //=6*6+0.5*L56*3+0.5774*(0.5*L56)*(0.5*L56)/2+(3+0.5774*L56*0.5)*3
                    break;


                case "W203":
                    faceSideArea = minThk * 3 + 19 * 19 * 1.1918 / 2 + (minThk - 19) * 0.364 * (minThk - 19) / 2 + ((19 * 1.1918) + (minThk - 19) * 0.364 + 3) * 3;
                    //L57*3+19*19*1.1918/2+(L57-19)*0.364*(L57-19)/2+((19*1.1918)+(L57-19)*0.364+3)*3
                    cbArea = 0;
                    //0
                    break;


                case "W204":
                    faceSideArea = 0.5 * minThk * 3 + 0.5774 * (0.5 * minThk) * (0.5 * minThk) / 2 + (3 + 0.5774 * minThk * 0.5) * 3;
                    //=0.5*L58*3+ 0.5774*(0.5*L58)*(0.5*L58)/2+(3+0.5774*L58*0.5)*3
                    cbArea = 0.5 * minThk * 3 + 0.5774 * (0.5 * minThk) * (0.5 * minThk) / 2 + (3 + 0.5774 * minThk * 0.5) * 3;
                    //=0.5*L58*3+ 0.5774*(0.5*L58)*(0.5*L58)/2+(3+0.5774*L58*0.5)*3
                    break;


                case "W205":
                    faceSideArea = minThk * 3 + 19 * 19 * 1.1918 + (0.5 * minThk - 19) * 0.364 * (0.5 * minThk - 19) + (((19 * 1.1918) + (0.5 * minThk - 19) * 0.364) * 2 + 3) * 3;
                    //=L59*3+19*19*1.1918+(0.5*L59-19)*0.364*(0.5*L59-19)+(((19*1.1918)+(0.5*L59-19)*0.364)*2+3)*3
                    cbArea = 0;
                    break;


                case "W206":
                    faceSideArea = minThk * 3 + 19 * 19 * 1.1918 / 2 + (minThk - 19) * 0.364 * (minThk - 19) / 2 + ((19 * 1.1918) + (minThk - 19) * 0.364 + 3) * 3;
                    //L60*3+19*19*1.1918/2+(L60-19)*0.364*(L60-19)/2+((19*1.1918)+(L60-19)*0.364+3)*3
                    cbArea = 0;
                    break;


                case "W208":
                    faceSideArea = 0.5 * minThk * 3 + 0.5774 * (0.5 * minThk) * (0.5 * minThk) / 2 + (3 + 0.5774 * minThk * 0.5) * 3;
                    //=0.5*L61*3+ 0.5774*(0.5*L61)*(0.5*L61)/2+(3+0.5774*L61*0.5)*3
                    cbArea = 0.5 * minThk * 3 + 0.5774 * (0.5 * minThk) * (0.5 * minThk) / 2 + (3 + 0.5774 * minThk * 0.5) * 3;
                    //=0.5*L61*3+0.5774*(0.5*L61)*(0.5*L61)/2+(3+0.5774*L61*0.5)*3
                    break;


                case "W209":
                    faceSideArea = 3 * minThk + ((minThk <= 15 ? 25 : (minThk > 15 && minThk < 20 ? 30 : (minThk > 20 ? 40 : 30)))) * minThk / 2 + (3 + ((minThk <= 15 ? 25 : (minThk > 15 && minThk < 20 ? 30 : (minThk > 20 ? 40 : 30))))) * 3;
                    //=3*L62+(IF(L62<=15,25,IF(L62>15<20,30,IF(L62>20,40,30))))*L62/2+ (3+(IF(L62<=15,25,IF(L62>15<20,30,IF(L62>20,40,30)))))*3
                    cbArea = 0;
                    //=6*6
                    break;


                case "W210":
                    faceSideArea = 3 * minThk + ((minThk <= 30 ? 25 : (minThk > 30 && minThk < 40 ? 30 : (minThk >= 40 ? 40 : 30)))) * 0.5 * minThk + 3 * 2 + (((minThk <= 30 ? 25 : (minThk > 30 && minThk < 40 ? 30 : (minThk >= 40 ? 40 : 30)))) * 3 * 2);
                    //=3*L63+(IF(L63<=30,25,IF(L63>30<40,30,IF(L63>=40,40,30))))*0.5*L63+3*2+((IF(L63<=30,25,IF(L63>30<40,30,IF(L63>=40,40,30))))*3*2)
                    cbArea = 0;
                    //=6*6
                    break;
                #endregion
                case "N1":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 7.5 / 1) * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 7.5 / 1) + (1 + 1 * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 7.5 / 1) + (1 + 1 * (minThk - 8) * Math.Tan(radianOf1 * (0 / 2)) * 3)) + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * (0 / 2));
                    //faceSideArea = L32+(90-1)*PI()*20*20/360+20*SIN(RADIANS(7.5/1))*20*COS(RADIANS(7.5/1))+(L32-8-20)*20*COS(RADIANS(7.5/1))+(L32-8-19.83)*(L32-8-19.83)*TAN(RADIANS(7.5/1))+(1+1*20*COS(RADIANS(7.5/1))+(L32-8-19.83)*TAN(RADIANS(7.5/1))+(1+1*(L32-8)*TAN(RADIANS(0/2))*3))+(L32-8)*(L32-8)*TAN(RADIANS(0/2));
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L32>30,12,6)*(IF(L32>30,12,6)+8)*PI()/2+IF(L32>30,12,12)*2*3;
                    break;
                case "N2":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 7.5 / 1) * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 7.5 / 1) + (1 + 1 * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 7.5 / 1) + (1 + 1 * (minThk - 8) * Math.Tan(radianOf1 * (0 / 2)) * 3)) + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * (0 / 2));
                    //faceSideArea = L33+(90-1)*PI()*20*20/360+20*SIN(RADIANS(7.5/1))*20*COS(RADIANS(7.5/1))+(L33-8-20)*20*COS(RADIANS(7.5/1))+(L33-8-19.83)*(L33-8-19.83)*TAN(RADIANS(7.5/1))+(1+1*20*COS(RADIANS(7.5/1))+(L33-8-19.83)*TAN(RADIANS(7.5/1))+(1+1*(L33-8)*TAN(RADIANS(0/2))*3))+(L33-8)*(L33-8)*TAN(RADIANS(0/2));
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L33>30,12,6)*(IF(L33>30,12,6)+8)*PI()/2+IF(L33>30,12,12)*2*3;
                    break;
                case "N3":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 1) * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 1) + (1 + 1 * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 1 * (minThk - 8) * Math.Tan(radianOf1 * 5 / 1) * 3)) + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 5 / 1);
                    //faceSideArea = L34+(90-1)*PI()*20*20/360+20*SIN(RADIANS(1/1))*20*COS(RADIANS(1/1))+(L34-8-20)*20*COS(RADIANS(1/1))+(L34-8-19.83)*(L34-8-19.83)*TAN(RADIANS(1/1))+(1+1*20*COS(RADIANS(1/1))+(L34-8-19.83)*TAN(RADIANS(1/2))+(1+1*(L34-8)*TAN(RADIANS(5/1))*3))+(L34-8)*(L34-8)*TAN(RADIANS(5/1));
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L34>30,12,6)*(IF(L34>30,12,6)+8)*PI()/2+IF(L34>30,12,12)*2*3;
                    break;
                case "N4":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 1) * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 1) + (1 + 1 * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 1 * (minThk - 8) * Math.Tan(radianOf1 * 5 / 1) * 3)) + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 5 / 1);
                    //faceSideArea = L35+(90-1)*PI()*20*20/360+20*SIN(RADIANS(1/1))*20*COS(RADIANS(1/1))+(L35-8-20)*20*COS(RADIANS(1/1))+(L35-8-19.83)*(L35-8-19.83)*TAN(RADIANS(1/1))+(1+1*20*COS(RADIANS(1/1))+(L35-8-19.83)*TAN(RADIANS(1/2))+(1+1*(L35-8)*TAN(RADIANS(5/1))*3))+(L35-8)*(L35-8)*TAN(RADIANS(5/1));
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L35>30,12,6)*(IF(L35>30,12,6)+8)*PI()/2+IF(L35>30,12,12)*2*3;
                    break;
                case "N5":
                    faceSideArea = 1 * minThk + (90 - 7.5) * Math.PI * 12 * 12 / 360 + 12 * Math.Cos(radianOf1 * 7.5) * 12 * Math.Sin(radianOf1 * 7.5) / 2 + (minThk - 2 - 12) * 12 * Math.Cos(radianOf1 * 7.5) + (minThk - 12 - 12 * Math.Sin(radianOf1 * 7.5)) * (minThk - 12 - 12 * Math.Sin(radianOf1 * 7.5)) * Math.Tan(radianOf1 * 7.5) / 2 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * 7.5) / 2;
                    //faceSideArea = 1*L36+(90-7.5)*PI()*12*12/360+12*COS(RADIANS(7.5))*12*SIN(RADIANS(7.5))/2+(L36-2-12)*12*COS(RADIANS(7.5))+(L36-12-12*SIN(RADIANS(7.5)))*(L36-12-12*SIN(RADIANS(7.5)))*TAN(RADIANS(7.5))/2+(L36-2)*(L36-2)*TAN(RADIANS(7.5))/2;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 2) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L36>30,12,6)*(IF(L36>30,12,6)+2)*PI()/2+IF(L36>30,12,12)*2*3;
                    break;
                case "N6":
                    faceSideArea = 1 * minThk + (90 - 7.5) * Math.PI * 12 * 12 / 360 + 12 * Math.Cos(radianOf1 * 7.5) * 12 * Math.Sin(radianOf1 * (7.5)) / 2 + (minThk - 2 - 12) * 12 * Math.Cos(radianOf1 * (7.5)) + (minThk - 12 - 12 * Math.Sin(radianOf1 * (7.5))) * (minThk - 12 - 12 * Math.Sin(radianOf1 * (7.5))) * Math.Tan(radianOf1 * (7.5)) / 2 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (7.5)) / 2;
                    //faceSideArea = 1*L37+(90-7.5)*PI()*12*12/360+12*COS(RADIANS(7.5))*12*SIN(RADIANS(7.5))/2+(L37-2-12)*12*COS(RADIANS(7.5))+(L37-12-12*SIN(RADIANS(7.5)))*(L37-12-12*SIN(RADIANS(7.5)))*TAN(RADIANS(7.5))/2+(L37-2)*(H23-2)*TAN(RADIANS(7.5))/2;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 2) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L37>30,12,6)*(IF(L37>30,12,6)+2)*PI()/2+IF(L37>30,12,12)*2*3;
                    break;
                case "N7":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 7.5 / 1) * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 7.5 / 1) + (1 + 1 * 20 * Math.Cos(radianOf1 * 7.5 / 1) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 7.5 / 1) + (1 + 1 * (minThk - 8) * Math.Tan(radianOf1 * (0 / 2)) * 3)) + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * (0 / 2));
                    //faceSideArea = L38+(90-1)*PI()*20*20/360+20*SIN(RADIANS(7.5/1))*20*COS(RADIANS(7.5/1))+(L38-8-20)*20*COS(RADIANS(7.5/1))+(L38-8-19.83)*(L38-8-19.83)*TAN(RADIANS(7.5/1))+(1+1*20*COS(RADIANS(7.5/1))+(L38-8-19.83)*TAN(RADIANS(7.5/1))+(1+1*(L38-8)*TAN(RADIANS(0/2))*3))+(L38-8)*(L38-8)*TAN(RADIANS(0/2));
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L38>30,12,6)*(IF(L38>30,12,6)+8)*PI()/2+IF(L38>30,12,12)*2*3;
                    break;
                case "N8":
                    faceSideArea = minThk + (90 - 1) * Math.PI * 20 * 20 / 360 + 20 * Math.Sin(radianOf1 * 1 / 1) * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 19.83) * (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 1) + (1 + 1 * 20 * Math.Cos(radianOf1 * 1 / 1) + (minThk - 8 - 19.83) * Math.Tan(radianOf1 * 1 / 2) + (1 + 1 * (minThk - 8) * Math.Tan(radianOf1 * 5 / 1) * 3)) + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 5 / 1);
                    //faceSideArea = L39+(90-1)*PI()*20*20/360+20*SIN(RADIANS(1/1))*20*COS(RADIANS(1/1))+(L39-8-20)*20*COS(RADIANS(1/1))+(L39-8-19.83)*(L39-8-19.83)*TAN(RADIANS(1/1))+(1+1*20*COS(RADIANS(1/1))+(L39-8-19.83)*TAN(RADIANS(1/2))+(1+1*(L39-8)*TAN(RADIANS(5/1))*3))+(L39-8)*(L39-8)*TAN(RADIANS(5/1));
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L39>30,12,6)*(IF(L39>30,12,6)+8)*PI()/2+IF(L39>30,12,12)*2*3;
                    break;
                case "N9":
                    faceSideArea = 1 * minThk + (90 - 7.5) * Math.PI * 12 * 12 / 360 + 12 * Math.Cos(radianOf1 * (7.5)) * 12 * Math.Sin(radianOf1 * (7.5)) / 2 + (minThk - 2 - 12) * 12 * Math.Cos(radianOf1 * (7.5)) + (minThk - 12 - 12 * Math.Sin(radianOf1 * (7.5))) * (minThk - 12 - 12 * Math.Sin(radianOf1 * (7.5))) * Math.Tan(radianOf1 * (7.5)) / 2 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (7.5)) / 2;
                    //faceSideArea = 1*L40+(90-7.5)*PI()*12*12/360+12*COS(RADIANS(7.5))*12*SIN(RADIANS(7.5))/2+(L40-2-12)*12*COS(RADIANS(7.5))+(L40-12-12*SIN(RADIANS(7.5)))*(L40-12-12*SIN(RADIANS(7.5)))*TAN(RADIANS(7.5))/2+(L40-2)*(H26-2)*TAN(RADIANS(7.5))/2;
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 2) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L40>30,12,6)*(IF(L40>30,12,6)+2)*PI()/2+IF(L40>30,12,12)*2*3;
                    break;
                case "N10":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (35)) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * (35))) * 3;
                    //faceSideArea = L41*1+(L41-2)*(L41-2)*TAN(RADIANS(35))+(1+2*(L41-2)*TAN(RADIANS(35)))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 3;
                    //cbArea = IF(L41>30,12*12,6*6)*PI()/2+IF(L41>30,12,6)*3;
                    break;
                case "N11":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (35)) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * (35))) * 3;
                    //faceSideArea = L42*1+(L42-2)*(L42-2)*TAN(RADIANS(35))+(1+2*(L42-2)*TAN(RADIANS(35)))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 3;
                    //cbArea = IF(L42>30,12*12,6*6)*PI()/2+IF(L42>30,12,6)*3;
                    break;
                case "N12":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (30)) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * (30))) * 3;
                    //faceSideArea = L42*1+(L42-2)*(L42-2)*TAN(RADIANS(35))+(1+2*(L42-2)*TAN(RADIANS(35)))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 3;
                    //cbArea = IF(L42>30,12*12,6*6)*PI()/2+IF(L42>30,12,6)*3;
                    break;
                case "N13":
                    faceSideArea = minThk * 1 + (minThk - 2) * (minThk - 2) * Math.Tan(radianOf1 * (30)) + (1 + 2 * (minThk - 2) * Math.Tan(radianOf1 * (30))) * 3;
                    //faceSideArea = L44 * 1 + (L44 - 2) * (L44 - 2) * TAN(RADIANS(30)) + (1 + 2 * (L44 - 2) * TAN(RADIANS(30))) * 3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L44>30,12*12,6*6)*PI()/2+IF(L44>30,12,6)*2*3;
                    break;
                case "N14":
                    faceSideArea = (Convert.ToDouble(2) / 3 * minThk + 2) * 1 + Convert.ToDouble(2) / 3 * minThk * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * 35 / 2) + (1 + 2 * Convert.ToDouble(2) / 3 * minThk * Math.Tan(radianOf1 * 35 / 2)) * 3;
                    //faceSideArea = (2/3*L45+2)*1+2/3*L45*2/3*L45*TAN(RADIANS(35/2))+(1+2*2/3*L45*TAN(RADIANS(35/2)))*3;
                    cbArea = Convert.ToDouble(1) / 3 * minThk * Math.Tan(radianOf1 * 45 / 2) * Convert.ToDouble(1) / 3 * minThk + Math.PI * ((minThk > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * Convert.ToDouble(1) / 3 * minThk * Math.Tan(radianOf1 * 45 / 2)) * 3 + 1 * Convert.ToDouble(1) / 3 * minThk;
                    //cbArea = 1/3*L45*TAN(RADIANS(45/2))*1/3*L45+PI()*(IF(L45>30,12*12,6*6)/2)+(1+2*1/3*L45*TAN(RADIANS(45/2)))*3+1*1/3*L45;
                    break;
                case "N16":
                    faceSideArea = (minThk * minThk * Math.Tan(radianOf1 * 10 / 1)) / 2 + minThk * 16 + (16 + minThk * Math.Tan(radianOf1 * 10 / 1)) * 3;
                    //faceSideArea = (L46*L46*TAN(RADIANS(10/1)))/2+L46*16+(16+L46*TAN(RADIANS(10/1)))*3;
                    cbArea = (minThk > 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L46>30,20*12,20*6)*PI()/2;
                    break;
                case "N17":
                    faceSideArea = (minThk * minThk * Math.Tan(radianOf1 * 10 / 1)) / 2 + minThk * 16 + (16 + minThk * Math.Tan(radianOf1 * 10 / 1)) * 3;
                    //faceSideArea = (L47 * L47 * TAN(RADIANS(10 / 1))) / 2 + L47 * 16 + (16 + L47 * TAN(RADIANS(10 / 1))) * 3;
                    cbArea = (minThk > 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L47>30,20*12,20*6)*PI()/2;
                    break;
                case "N18":
                    faceSideArea = minThk * 3 + (minThk * minThk / 2) + (3 + (minThk)) * 3;
                    //faceSideArea = L48*3+(L48*L48/2)+(3+(L48))*3;
                    cbArea = (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (minThk > 30 ? 12 : 6) * 3;
                    //cbArea = IF(L48>30,12*12,6*6)*PI()/2+IF(L48>30,12,6)*3;
                    break;
                case "N51":
                    faceSideArea = thk2 * 3 + (thk2 - 2) * (thk2 - 2) * Math.Tan(radianOf1 * (35)) / 2 + (3 + (thk2 - 2) * Math.Tan(radianOf1 * (35))) * 3;
                    //faceSideArea= G49 * 3 + (G49 - 2) * (G49 - 2) * TAN(RADIANS(35)) / 2 + (3 + (G49 - 2) * TAN(RADIANS(35))) * 3;
                    //faceSideArea = minThk* 3 + (minThk- 2) * (minThk- 2) * Math.Tan(radianOf1 * (35)) / 2 + (3 + (minThk- 2) * Math.Tan(radianOf1 * (35))) * 3;
                    //faceSideArea = G49*3+(G49-2)*(G49-2)*TAN(RADIANS(35))/2+(3+(G49-2)*TAN(RADIANS(35)))*3;
                    cbArea = thk1 * Math.Tan(radianOf1 * (35)) * thk1 / 2 + Math.PI * ((thk1 > 30 ? 12 * 12 : 6 * 6) / 2) + (3 + thk1 * Math.Tan(radianOf1 * (35))) * 3 + 3 * thk1;
                    //cbArea = F49*TAN(RADIANS(35))*F49/2+PI()*(IF(F49>30,12*12,6*6)/2)+(3+F49*TAN(RADIANS(35)))*3+3*F49;
                    break;
                case "N52":
                    faceSideArea = (thk1 * thk1 * Math.Tan(radianOf1 * 35 / 1)) / 2 + (thk1 * Math.Tan(radianOf1 * 35 / 1) + 5) * thk2 + half * thk2 * Math.Tan(radianOf1 * (10)) * thk2 + (thk1 + thk2) * 1 + (1 + (thk1 * Math.Tan(radianOf1 * 35 / 1) + 5) + (thk2 * Math.Tan(radianOf1 * (10)))) * 3;
                    //var faceSideArea1 = (th1 * th1 * Math.Tan(radianOf1 * (35 / 1))) / 2 + (th1 * Math.Tan(radianOf1 * (35 / 1)) + 5) * th2 + 1/2 * th2 * Math.Tan(radianOf1 * (10)) * th2 + (th1 + th2) * 1 + (1 + (th1 * Math.Tan(radianOf1 * (35 / 1)) + 5) + (th2 * Math.Tan(radianOf1 * (10)))) * 3;

                    //faceSideArea = (F50*F50*TAN(RADIANS(35/1)))/2+(F50*TAN(RADIANS(35/1))+5)*G50+1/2*G50*TAN(RADIANS(10))*G50+(F50+G50)*1+(1+(F50*TAN(RADIANS(35/1))+5)+(G50*TAN(RADIANS(10))))*3;
                    //faceSideArea = (thk1 * thk1 * Math.Tan(radianOf1 * (35 / 1))) / 2 + (thk1 * Math.Tan(radianOf1 * (35 / 1)) + 5) * thk2 + 1 / 2 * thk2 * Math.Tan(radianOf1 * (10)) * thk2 + (thk1 + thk2) * 1 + (1 + (thk1 * Math.Tan(radianOf1 * (35 / 1)) + 5) + (thk2 * Math.Tan(radianOf1 * (10)))) * 3;
                    //faceSideArea = (F50*F50*TAN(RADIANS(35/1)))/2+(F50*TAN(RADIANS(35/1))+5)*G50+1/2*G50*TAN(RADIANS(10))*G50+(F50+G50)*1+(1+(F50*TAN(RADIANS(35/1))+5)+(G50*TAN(RADIANS(10))))*3;
                    cbArea = (minThk > 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "N53":
                    faceSideArea = (thk1 * thk1 * Math.Tan(radianOf1 * 15 / 1)) / 2 + (thk1 * Math.Tan(radianOf1 * 15 / 1) + 5) * thk2 + half * thk2 * Math.Tan(radianOf1 * (10)) * thk2 + (thk1 + thk2) * 10 + (10 + (thk1 * Math.Tan(radianOf1 * 15 / 1) + 5) + (thk2 * Math.Tan(radianOf1 * (10)))) * 3;
                    //faceSideArea = (F51 * F51 * TAN(RADIANS(15 / 1))) / 2 + (F51 * TAN(RADIANS(15 / 1)) + 5) * G51 + 1 / 2 * G51 * TAN(RADIANS(10)) * G51 + (F51 + G51) * 10 + (10 + (F51 * TAN(RADIANS(15 / 1)) + 5) + (G51 * TAN(RADIANS(10)))) * 3;
                    //faceSideArea = (thk1 * thk1 * Math.Tan(radianOf1 * (15 / 1))) / 2 + (thk1 * Math.Tan(radianOf1 * (15 / 1)) + 5) * thk2 + 1 / 2 * thk2 * Math.Tan(radianOf1 * (10)) * thk2 + (thk1 + thk2) * 10 + (10 + (thk1 * Math.Tan(radianOf1 * (15 / 1)) + 5) + (thk2 * Math.Tan(radianOf1 * (10)))) * 3;
                    //faceSideArea = (thk1*thk1*TAN(RADIANS(15/1)))/2+(thk1*TAN(RADIANS(15/1))+5)*G51+1/2*G51*TAN(RADIANS(10))*G51+(thk1+G51)*10+(10+(thk1*TAN(RADIANS(15/1))+5)+(G51*TAN(RADIANS(10))))*3;
                    cbArea = (minThk > 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L51>30,20*12,20*6)*PI()/2;
                    break;
                case "N54":
                    faceSideArea = Math.Max(thk1, thk2) * 3 + ((thk1 * thk1 / 2) + (thk2 * thk2 / 2)) / 2 + (3 + (Math.Max(thk1, thk2)) * 3) + 7 * thk2;
                    //faceSideArea = MAX(F52, G52) * 3 + ((F52 * F52 / 2) + (G52 * G52 / 2)) / 2 + (3 + (MAX(F52, G52)) * 3) + 7 * G52;
                    //faceSideArea = Math.Max(thk1, thk2) * 3 + ((thk1 * thk1 / 2) + (thk2 * thk2 / 2)) / 2 + (3 + (Math.Max(thk1, thk2)) * 3) + 7 * thk2;
                    //faceSideArea = MAX(F52,G52)*3+((F52*F52/2)+(G52*G52/2))/2+(3+(MAX(F52,G52))*3)+7*G52;
                    cbArea = half3 * thk1 + (half * thk1 * thk1) / 2 + (minThk > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2;
                    //cbArea = 3/2*F52+(1/2*F52*F52)/2+IF(L52>30,12*12,6*6)*PI()/2;
                    break;
                case "N55":
                    faceSideArea = Math.Max(thk1, thk2) * 3 + ((thk1 * thk1 / 2) + (thk2 * thk2 / 2)) / 2 + (3 + (Math.Max(thk1, thk2)) * 3) + 7 * thk2;
                    //faceSideArea = MAX(F53, G53) * 3 + ((F53 * F53 / 2) + (G53 * G53 / 2)) / 2 + (3 + (MAX(F53, G53)) * 3) + 7 * G53
                    //faceSideArea = Math.Max(thk1, thk2) * 3 + ((thk1 * thk1 / 2) + (thk2 * thk2 / 2)) / 2 + (3 + (Math.Max(thk1, thk2)) * 3) + 7 * thk2;
                    //faceSideArea = MAX(F53,G53)*3+((F53*F53/2)+(G53*G53/2))/2+(3+(MAX(F53,G53))*3)+7*G53;
                    cbArea = half3 * thk1 + (half * thk1 * thk1) / 2 + (Math.Min(thk1, thk2) > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2;
                    //cbArea = 3/2*F53+(1/2*F53*F53)/2+IF(L53>30,12*12,6*6)*PI()/2;
                    break;
                case "N56":
                    faceSideArea = 1 * minThk + (90 - 7.5) * Math.PI * 20 * 20 / 360 + 20 * Math.Cos(radianOf1 * 7.5) * 20 * Math.Sin(radianOf1 * 7.5) / 2 + (minThk - 8 - 20) * 20 * Math.Cos(radianOf1 * 7.5) + (minThk - 20 - 20 * Math.Sin(radianOf1 * 7.5)) * (minThk - 20 - 20 * Math.Sin(radianOf1 * 7.5)) * Math.Tan(radianOf1 * 7.5) / 2 + (minThk - 8) * (minThk - 8) * Math.Tan(radianOf1 * 7.5) / 2;
                    //faceSideArea = 1*L67+(90-7.5)*PI()*20*20/360+20*COS(RADIANS(7.5))*20*SIN(RADIANS(7.5))/2+(L67-8-20)*20*COS(RADIANS(7.5))+(L67-20-20*SIN(RADIANS(7.5)))*(L67-20-20*SIN(RADIANS(7.5)))*TAN(RADIANS(7.5))/2+(L67-8)*(L67-8)*TAN(RADIANS(7.5))/2;                    
                    cbArea = (minThk > 30 ? 12 : 6) * ((minThk > 30 ? 12 : 6) + 8) * Math.PI / 2 + (minThk > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L67>30,12,6)*(IF(L67>30,12,6)+8)*PI()/2+IF(L67>30,12,12)*2*3;
                    break;
                case "N151":
                    faceSideArea = (thk1 * thk1 * Math.Tan(radianOf1 * 35 / 1)) / 2 + (thk1 * Math.Tan(radianOf1 * 35 / 1) + 5) * thk2 + half * thk2 * Math.Tan(radianOf1 * (10)) * thk2 + (thk1 + thk2) * 3 + (3 + (thk1 * Math.Tan(radianOf1 * 35 / 1) + 5) + (thk2 * Math.Tan(radianOf1 * (10)))) * 3;
                    //faceSideArea= (F54 * F54 * TAN(RADIANS(35 / 1))) / 2 + (F54 * TAN(RADIANS(35 / 1)) + 5) * G54 + 1 / 2 * G54 * TAN(RADIANS(10)) * G54 + (F54 + G54) * 3 + (3 + (F54 * TAN(RADIANS(35 / 1)) + 5) + (G54 * TAN(RADIANS(10)))) * 3
                    //faceSideArea = (thk1 * thk1 * Math.Tan(radianOf1 * (35 / 1))) / 2 + (thk1 * Math.Tan(radianOf1 * (35 / 1)) + 5) * thk2 + 1 / 2 * thk2 * Math.Tan(radianOf1 * (10)) * thk2 + (thk1 + thk2) * 3 + (3 + (thk1 * Math.Tan(radianOf1 * (35 / 1)) + 5) + (thk2 * Math.Tan(radianOf1 * (10)))) * 3;
                    //faceSideArea = (F54*F54*TAN(RADIANS(35/1)))/2+(F54*TAN(RADIANS(35/1))+5)*G54+1/2*G54*TAN(RADIANS(10))*G54+(F54+G54)*3+(3+(F54*TAN(RADIANS(35/1))+5)+(G54*TAN(RADIANS(10))))*3;
                    cbArea = (minThk > 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L54>30,20*12,20*6)*PI()/2;
                    break;
                case "W101":
                    faceSideArea = minThk * 3 + (minThk - 1.5) * (minThk - 1.5) * Math.Tan(radianOf1 * (37.5)) + (3 + 2 * (minThk - 1.5) * Math.Tan(radianOf1 * (37.5))) * 3;
                    //faceSideArea = L55*3+(L55-1.5)*(L55-1.5)*TAN(RADIANS(37.5))+(3+2*(L55-1.5)*TAN(RADIANS(37.5)))*3;
                    cbArea = 0;//(minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W102":
                    faceSideArea = minThk * 3 + (minThk - 1.5) * (minThk - 1.5) * Math.Tan(radianOf1 * (30)) + (3 + 2 * (minThk - 1.5) * Math.Tan(radianOf1 * (30))) * 3;
                    //faceSideArea = L56*3+(L56-1.5)*(L56-1.5)*TAN(RADIANS(30))+(3+2*(L56-1.5)*TAN(RADIANS(30)))*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W103":
                    faceSideArea = minThk * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) + 17.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 19) + (minThk - 19) * (minThk - 19) * Math.Tan(radianOf1 * 20 / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 19) * Math.Tan(radianOf1 * 20 / 2) * 2) * 3;
                    //faceSideArea = L57*3+17.5*17.5*TAN(RADIANS(75/2))+17.5*TAN(RADIANS(75/2))*2*(L57-19)+(L57-19)*(L57-19)*TAN(RADIANS(20/2))+(3+19*TAN(RADIANS(75/2))*2+(L57-19)*TAN(RADIANS(20/2))*2)*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W104":
                    faceSideArea = minThk * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) + 17.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 19) + (minThk - 19) * (minThk - 19) * Math.Tan(radianOf1 * 10 / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 19) * Math.Tan(radianOf1 * 10 / 2) * 2) * 3;
                    //faceSideArea = L58*3+17.5*17.5*TAN(RADIANS(75/2))+17.5*TAN(RADIANS(75/2))*2*(L58-19)+(L58-19)*(L58-19)*TAN(RADIANS(10/2))+(3+19*TAN(RADIANS(75/2))*2+(L58-19)*TAN(RADIANS(10/2))*2)*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W105":
                    faceSideArea = minThk * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) + 17.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 19) + (minThk - 19) * (minThk - 19) * Math.Tan(radianOf1 * 4 / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 19) * Math.Tan(radianOf1 * 4 / 2) * 2) * 3;
                    //faceSideArea = L59*3+17.5*17.5*TAN(RADIANS(75/2))+17.5*TAN(RADIANS(75/2))*2*(L59-19)+(L59-19)*(L59-19)*TAN(RADIANS(4/2))+(3+19*TAN(RADIANS(75/2))*2+(L59-19)*TAN(RADIANS(4/2))*2)*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W106":
                    faceSideArea = minThk * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) / 2 + 17.5 * Math.Tan(radianOf1 * 75 / 2) * (minThk - 19) + (minThk - 19) * (minThk - 19) * Math.Tan(radianOf1 * 10 / 1) / 2 + (minThk * minThk * Math.Tan(radianOf1 * 30 / 1) / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 1 + (minThk - 19) * Math.Tan(radianOf1 * 10 / 1) + minThk * Math.Tan(radianOf1 * 30 / 1) * 3);
                    //faceSideArea = L60*3+17.5*17.5*TAN(RADIANS(75/2))/2+17.5*TAN(RADIANS(75/2))*(L60-19)+(L60-19)*(L60-19)*TAN(RADIANS(10/1))/2+(L60*L60*TAN(RADIANS(30/1))/2)+(3+19*TAN(RADIANS(75/2))*1+(L60-19)*TAN(RADIANS(10/1))+L60*TAN(RADIANS(30/1))*3);
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W107":
                    faceSideArea = (minThk + 2) * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) / 2 + 17.5 * Math.Tan(radianOf1 * 75 / 2) * (minThk - 19 + 2) + (minThk - 19 + 2) * (minThk - 19 + 2) * Math.Tan(radianOf1 * 5 / 1) / 2 + ((minThk + 2) * (minThk + 2) * Math.Tan(radianOf1 * 30 / 1) / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 1 + (minThk - 19 + 2) * Math.Tan(radianOf1 * 5 / 1) + (minThk + 2) * Math.Tan(radianOf1 * 30 / 1) * 3);
                    //faceSideArea = (L61+2)*3+17.5*17.5*TAN(RADIANS(75/2))/2+17.5*TAN(RADIANS(75/2))*(L61-19+2)+(L61-19+2)*(L61-19+2)*TAN(RADIANS(5/1))/2+((L61+2)*(L61+2)*TAN(RADIANS(30/1))/2)+(3+19*TAN(RADIANS(75/2))*1+(L61-19+2)*TAN(RADIANS(5/1))+(L61+2)*TAN(RADIANS(30/1))*3);
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W108":
                    faceSideArea = (90 - 5) * Math.PI * 24 * 24 / 360 + 24 * Math.Sin(radianOf1 * 5 / 1) * 24 * Math.Cos(radianOf1 * 5 / 1) + (minThk - 0 - 24) * 24 * Math.Cos(radianOf1 * 5 / 1) + (minThk - 0 - 23.8) * (minThk - 0 - 23.8) * Math.Tan(radianOf1 * 5 / 1) + (0 + 1 * 24 * Math.Cos(radianOf1 * 5 / 1) + (minThk - 0 - 23.8) * Math.Tan(radianOf1 * 5 / 1) + (0 + 1 * (minThk - 0) * Math.Tan(radianOf1 * (0 / 2)) * 3)) + (minThk - 0) * (minThk - 0) * Math.Tan(radianOf1 * (0 / 2));
                    //faceSideArea = (90-5)*PI()*24*24/360+24*SIN(RADIANS(5/1))*24*COS(RADIANS(5/1))+(L62-0-24)*24*COS(RADIANS(5/1))+(L62-0-23.8)*(L62-0-23.8)*TAN(RADIANS(5/1))+(0+1*24*COS(RADIANS(5/1))+(L62-0-23.8)*TAN(RADIANS(5/1))+(0+1*(L62-0)*TAN(RADIANS(0/2))*3))+(L62-0)*(L62-0)*TAN(RADIANS(0/2));
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W109":
                    faceSideArea = minThk * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) + 17.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 19) + (minThk - 19) * (minThk - 19) * Math.Tan(radianOf1 * 4 / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 19) * Math.Tan(radianOf1 * 4 / 2) * 2) * 3;
                    //faceSideArea = L63*3+17.5*17.5*TAN(RADIANS(75/2))+17.5*TAN(RADIANS(75/2))*2*(L63-19)+(L63-19)*(L63-19)*TAN(RADIANS(4/2))+(3+19*TAN(RADIANS(75/2))*2+(L63-19)*TAN(RADIANS(4/2))*2)*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W110":
                    faceSideArea = minThk * 3 + 17.5 * 17.5 * Math.Tan(radianOf1 * 75 / 2) + 17.5 * Math.Tan(radianOf1 * 75 / 2) * 2 * (minThk - 19) + (minThk - 19) * (minThk - 19) * Math.Tan(radianOf1 * 2 / 2) + (3 + 19 * Math.Tan(radianOf1 * 75 / 2) * 2 + (minThk - 19) * Math.Tan(radianOf1 * 2 / 2) * 2) * 3;
                    //faceSideArea = L64*3+17.5*17.5*TAN(RADIANS(75/2))+17.5*TAN(RADIANS(75/2))*2*(L64-19)+(L64-19)*(L64-19)*TAN(RADIANS(2/2))+(3+19*TAN(RADIANS(75/2))*2+(L64-19)*TAN(RADIANS(2/2))*2)*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "W111":
                    faceSideArea = minThk * 3 + (minThk / 2 + 0.75) * (minThk / 2 + 0.75) * GetTANValue(22.5) + (minThk / 2 + 0.75) * (minThk / 2 + 0.75) * GetTANValue(3.75) + (3 + (minThk / 2) * GetTANValue(22.5) + (minThk / 2) * GetTANValue(3.75) * 3);
                    //faceSideArea = L79*3+(L79/2+0.75)*(L79/2+0.75)*TAN(RADIANS(22.5))+(L79/2+0.75)*(L79/2+0.75)*TAN(RADIANS(3.75))+(3+(L79/2)*TAN(RADIANS(22.5))+(L79/2)*TAN(RADIANS(3.75))*3);
                    cbArea = minThk * 3 + (minThk / 2 + 0.75) * (minThk / 2 + 0.75) * GetTANValue(22.5) + (minThk / 2 + 0.75) * (minThk / 2 + 0.75) * GetTANValue(3.75) + (3 + (minThk / 2) * GetTANValue(22.5) + (minThk / 2) * GetTANValue(3.75) * 3);
                    //cbArea = L79*3+(L79/2+0.75)*(L79/2+0.75)*TAN(RADIANS(22.5))+(L79/2+0.75)*(L79/2+0.75)*TAN(RADIANS(3.75))+(3+(L79/2)*TAN(RADIANS(22.5))+(L79/2)*TAN(RADIANS(3.75))*3);
                    break;
                case "N101":
                    faceSideArea = minThk * 3 + (minThk - 1.5) * (minThk - 1.5) * Math.Tan(radianOf1 * (50)) / 2 + (3 + (minThk - 1.5) * Math.Tan(radianOf1 * (50))) * 3;
                    //faceSideArea = L65*3+(L65-1.5)*(L65-1.5)*TAN(RADIANS(50))/2+(3+(L65-1.5)*TAN(RADIANS(50)))*3;
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "N102":
                    faceSideArea = minThk * 3 + 8.5 * 8.5 * Math.Tan(radianOf1 * 50 / 1) / 2 + 8.5 * Math.Tan(radianOf1 * 50 / 1) * (minThk - 8.5) + (minThk - 8.5) * (minThk - 8.5) * Math.Tan(radianOf1 * 15 / 1) / 2 + (minThk * minThk * Math.Tan(radianOf1 * (0 / 1)) / 2) + (3 + 10 * Math.Tan(radianOf1 * 50 / 1) * 1 + (minThk - 10) * Math.Tan(radianOf1 * 15 / 1) + minThk * Math.Tan(radianOf1 * (0 / 1)) * 3);
                    //faceSideArea = L66*3+8.5*8.5*TAN(RADIANS(50/1))/2+8.5*TAN(RADIANS(50/1))*(L66-8.5)+(L66-8.5)*(L66-8.5)*TAN(RADIANS(15/1))/2+(L66*L66*TAN(RADIANS(0/1))/2)+(3+10*TAN(RADIANS(50/1))*1+(L66-10)*TAN(RADIANS(15/1))+L66*TAN(RADIANS(0/1))*3);
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                case "OW":
                case "CR":
                    faceSideArea = SeamLengthArea;
                    //faceSideArea = L66*3+8.5*8.5*TAN(RADIANS(50/1))/2+8.5*TAN(RADIANS(50/1))*(L66-8.5)+(L66-8.5)*(L66-8.5)*TAN(RADIANS(15/1))/2+(L66*L66*TAN(RADIANS(0/1))/2)+(3+10*TAN(RADIANS(50/1))*1+(L66-10)*TAN(RADIANS(15/1))+L66*TAN(RADIANS(0/1))*3);
                    cbArea = 0;// (minThk> 30 ? 20 * 12 : 20 * 6) * Math.PI / 2;
                    //cbArea = IF(L50>30,20*12,20*6)*PI()/2;
                    break;
                default:
                    break;

            }
            objModelWeldKG.FaceSideArea = Convert.ToDouble(Math.Round(faceSideArea, 2));//Convert.ToDecimal(string.Format("{0:0.##}", faceSideArea));
            objModelWeldKG.CBArea = Convert.ToDouble(Math.Round(cbArea, 2));

            return objModelWeldKG;
        }

        public double GetTANValue(double rad)
        {
            return Math.Tan(radianOf1 * (rad));
        }
        public double GetCOSValue(double rad)
        {
            return Math.Cos(radianOf1 * (rad));
        }
        public double GetSINValue(double rad)
        {
            return Math.Sin(radianOf1 * (rad));
        }
        public double CalculateWeldKGFaceside(string seamNo, int lengthArea, double csss, double minThk, double faceside, QMS012 qms12)
        {
            double weldKGfaceSide = 0;

            if (!string.IsNullOrEmpty(seamNo))
            {
                IEMQSEntitiesContext db = new IEMQSEntitiesContext();
                //var qms12 = db.QMS012.Where(i => i.SeamNo.Equals(seamNo, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                var str1 = "N51".Substring(1, "N51".Length - 1);
                var str2 = "N151".Substring(1, "N151".Length - 1);
                if (qms12.SeamCategory.StartsWith("OW") || qms12.SeamCategory.StartsWith("CR"))
                {
                    // old formula is : (faceside * lengthArea * csss) / 1000000000 
                    // new formula is : (faceside * minThk * csss) / 1000000000 as discussed on 05/11/2018.
                    weldKGfaceSide = (faceside * minThk * csss) / 1000000000;
                }
                else
                {
                    var wepT = !string.IsNullOrEmpty(qms12.WEPDetail) ? qms12.WEPDetail.Substring(1, qms12.WEPDetail.Length - 1) : "-1";

                    if ((qms12.SeamCategory.StartsWith("LW") || qms12.SeamCategory.StartsWith("CW") || qms12.SeamCategory.StartsWith("NW")))// && !(Convert.ToInt32(wepT) >= Convert.ToInt32(str1) && Convert.ToInt32(wepT) <= Convert.ToInt32(str2)))
                    {
                        if (seamNo.Contains("CW"))
                        {
                            weldKGfaceSide = (faceside * (lengthArea - (Convert.ToDouble(Math.PI) * minThk)) * csss) / 1000000000;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(qms12.WEPDetail) && qms12.WEPDetail.StartsWith("N") && !(Convert.ToInt32(wepT) >= Convert.ToInt32(str1) && Convert.ToInt32(wepT) <= Convert.ToInt32(str2)))
                            {
                                weldKGfaceSide = (faceside * lengthArea * csss) / 1000000000;
                            }
                            else
                            {
                                weldKGfaceSide = (faceside * (lengthArea - (Convert.ToDouble(Math.PI) * minThk)) * csss) / 1000000000;
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(qms12.WEPDetail) && qms12.WEPDetail.StartsWith("N") && (Convert.ToInt32(wepT) >= Convert.ToInt32(str1) && Convert.ToInt32(wepT) <= Convert.ToInt32(str2)))
                    {
                        weldKGfaceSide = (faceside * lengthArea * csss) / 1000000000;
                    }
                    else
                    {
                        weldKGfaceSide = (faceside * lengthArea * csss) / 1000000000;
                    }
                }
            }

            return Convert.ToDouble(Math.Round(weldKGfaceSide, 2));
        }

        public double CalculateWeldKGCBSide(string seamNo, int lengthArea, double csss, double minThk, double cbArea, QMS012 qms12)
        {
            double weldKGCBSide = 0;
            if (!string.IsNullOrEmpty(seamNo))
            {
                IEMQSEntitiesContext db = new IEMQSEntitiesContext();
                //var qms12 = db.QMS012.Where(i => i.SeamNo.Equals(seamNo, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                var str1 = "N51".Substring(1, "N51".Length - 1);
                var str2 = "N151".Substring(1, "N151".Length - 1);
                var wepT = !string.IsNullOrEmpty(qms12.WEPDetail) ? qms12.WEPDetail.Substring(1, qms12.WEPDetail.Length - 1) : "-1";

                if ((qms12.SeamCategory.StartsWith("LW") || qms12.SeamCategory.StartsWith("CW") || qms12.SeamCategory.StartsWith("NW")))// && !(Convert.ToInt32(wepT) >= Convert.ToInt32(str1) && Convert.ToInt32(wepT) <= Convert.ToInt32(str2)))
                {
                    if (seamNo.Contains("CW"))
                    {
                        weldKGCBSide = (cbArea * (lengthArea - (Convert.ToDouble(Math.PI) * minThk)) * csss) / 1000000000;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(qms12.WEPDetail) && qms12.WEPDetail.StartsWith("N") && !(Convert.ToInt32(wepT) >= Convert.ToInt32(str1) && Convert.ToInt32(wepT) <= Convert.ToInt32(str2)))
                        {
                            weldKGCBSide = (cbArea * lengthArea * csss) / 1000000000;
                        }
                        else
                        {
                            weldKGCBSide = (cbArea * (lengthArea - (Convert.ToDouble(Math.PI) * minThk)) * csss) / 1000000000;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(qms12.WEPDetail) && qms12.WEPDetail.StartsWith("N") && (Convert.ToInt32(wepT) >= Convert.ToInt32(str1) && Convert.ToInt32(wepT) <= Convert.ToInt32(str2)))
                {
                    weldKGCBSide = (cbArea * lengthArea * csss) / 1000000000;
                }
                else
                {
                    weldKGCBSide = (cbArea * lengthArea * csss) / 1000000000;
                }
            }
            return Convert.ToDouble(Math.Round(weldKGCBSide, 2));
        }

        public double CalculateCBArea(string wepType, int L4)
        {
            //decimal cbackArea = 0;
            double cbArea = 0;
            switch (wepType)
            {
                case "W1":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L4>30,12*12,6*6)*PI()/2+IF(L4>30,12,6)*2*3;
                    break;
                case "W2":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L5>30,12*12,6*6)*PI()/2+IF(L5>30,12,6)*2*3;
                    break;
                case "W3":
                    cbArea = 1 / 3 * L4 * Math.Tan(radianOf1 * (45)) * 1 / 3 * L4 + Math.PI * ((L4 > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * 1 / 3 * L4 * Math.Tan(radianOf1 * (45))) * 3 + 1 * 1 / 3 * L4;
                    //cbArea = 1/3*L6*TAN(RADIANS(45))*1/3*L6+PI()*(IF(L6>30,12*12,6*6)/2)+(1+2*1/3*L6*TAN(RADIANS(45)))*3+1*1/3*L6;
                    break;
                case "W4":
                    cbArea = 1 / 3 * L4 * Math.Tan(radianOf1 * (30)) * 1 / 3 * L4 + Math.PI * ((L4 > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * 1 / 3 * L4 * Math.Tan(radianOf1 * (30))) * 3 + 1 * 1 / 3 * L4;
                    //cbArea = 1/3*L7*TAN(RADIANS(30))*1/3*L7+PI()*(IF(L7>30,12*12,6*6)/2)+(1+2*1/3*L7*TAN(RADIANS(30)))*3+1*1/3*L7;
                    break;
                case "W5":
                    cbArea = ((L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + 2 * (0.45 * L4) + (180 - 5) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * (5 / 2)) * 8 * Math.Cos(radianOf1 * (5 / 2)) + ((0.45 * L4 - 8) - 8) * 8 * Math.Cos(radianOf1 * (5 / 2)) * 2 + ((0.45 * L4 - 8) - 7.651) * ((0.45 * L4 - 8) - 7.651) * Math.Tan(radianOf1 * (5 / 2)) + (2 + 2 * 8 * Math.Cos(radianOf1 * (5 / 2)) + 2 * ((0.45 * L4 - 8) - 7.651) * Math.Tan(radianOf1 * (5 / 2))) * 3);
                    //cbArea = (IF(L8>30,12*12,6*6)*PI()/2+2*(0.45*L8)+(180-5)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+((0.45*L8-8)-8)*8*COS(RADIANS(5/2))*2+((0.45*L8-8)-7.651)*((0.45*L8-8)-7.651)*TAN(RADIANS(5/2))+(2+2*8*COS(RADIANS(5/2))+2*((0.45*L8-8)-7.651)*TAN(RADIANS(5/2)))*3);
                    break;
                case "W6":
                    cbArea = ((L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + 2 * (0.45 * L4) + (180 - 5) * Math.PI * 8 * 8 / 360 + 8 * Math.Sin(radianOf1 * (5 / 2)) * 8 * Math.Cos(radianOf1 * (5 / 2)) + ((0.45 * L4 - 8) - 8) * 8 * Math.Cos(radianOf1 * (5 / 2)) * 2 + ((0.45 * L4 - 8) - 7.651) * ((0.45 * L4 - 8) - 7.651) * Math.Tan(radianOf1 * (5 / 2)) + (2 + 2 * 8 * Math.Cos(radianOf1 * (5 / 2)) + 2 * ((0.45 * L4 - 8) - 7.651) * Math.Tan(radianOf1 * (5 / 2))) * 3);
                    //cbArea = (IF(L9>30,12*12,6*6)*PI()/2+2*(0.45*L9)+(180-5)*PI()*8*8/360+8*SIN(RADIANS(5/2))*8*COS(RADIANS(5/2))+((0.45*L9-8)-8)*8*COS(RADIANS(5/2))*2+((0.45*L9-8)-7.651)*((0.45*L9-8)-7.651)*TAN(RADIANS(5/2))+(2+2*8*COS(RADIANS(5/2))+2*((0.45*L9-8)-7.651)*TAN(RADIANS(5/2)))*3);
                    break;
                case "W7":
                    cbArea = (L4 > 30 ? 12 : 6) * ((L4 > 30 ? 12 : 6) + 18) * Math.PI / 2 + (L4 > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L10>30,12,6)*(IF(L10>30,12,6)+18)*PI()/2+IF(L10>30,12,12)*2*3;
                    break;
                case "W8":
                    cbArea = (L4 > 30 ? 12 : 6) * ((L4 > 30 ? 12 : 6) + 8) * Math.PI / 2 + (L4 > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L11>30,12,6)*(IF(L11>30,12,6)+8)*PI()/2+IF(L11>30,12,12)*2*3;
                    break;
                case "W9":
                    cbArea = (L4 > 30 ? 12 : 6) * ((L4 > 30 ? 12 : 6) + 18) * Math.PI / 2 + (L4 > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L12>30,12,6)*(IF(L12>30,12,6)+18)*PI()/2+IF(L12>30,12,12)*2*3;
                    break;
                case "W10":
                    cbArea = (L4 > 30 ? 12 : 6) * ((L4 > 30 ? 12 : 6) + 8) * Math.PI / 2 + (L4 > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L13>30,12,6)*(IF(L13>30,12,6)+8)*PI()/2+IF(L13>30,12,12)*2*3;
                    break;
                case "W11":
                    //faceSideArea = (L4 + 12) + (180 - 1) * Math.PI * 10 * 10 / 360 + 10 * Math.Sin(radianOf1 * (1 / 2)) * 10 * Math.Cos(radianOf1 * (1 / 2)) + ((L4 + 4) - 10) * 10 * Math.Cos(radianOf1 * (1 / 2)) * 2 + ((L4 + 4) - 9.913) * ((L4 + 4) - 9.913) * Math.Tan(radianOf1 * (1 / 2)) + (1 + 2 * 10 * Math.Cos(radianOf1 * (1 / 2)) + 2 * ((L4 + 4) - 9.913) * Math.Tan(radianOf1 * (1 / 2))) * 3;
                    //faceSideArea =(L14+12)+(180-1)*PI()*10*10/360+10*SIN(RADIANS(1/2))*10*COS(RADIANS(1/2))+((L14+4)-10)*10*COS(RADIANS(1/2))*2+((L14+4)-9.913)*((L14+4)-9.913)*TAN(RADIANS(1/2))+(1+2*10*COS(RADIANS(1/2))+2*((L14+4)-9.913)*TAN(RADIANS(1/2)))*3;
                    break;
                case "W12":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //faceSideArea =IF(L15>30,12*12,6*6)*PI()/2+IF(L15>30,12,6)*2*3;
                    break;
                case "W13":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L16>30,12*12,6*6)*PI()/2+IF(L16>30,12,6)*2*3;
                    break;
                case "W14":
                    cbArea = (1 / 3 * L4 + 2) * Math.Tan(radianOf1 * (30)) * (1 / 3 * L4 + 2) + Math.PI * ((L4 > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * (1 / 3 * L4 + 2) * Math.Tan(radianOf1 * (30))) * 3 + 1 * 1 / 3 * L4;
                    //cbArea = (1/3*L17+2)*TAN(RADIANS(30))*(1/3*L17+2)+PI()*(IF(L17>30,12*12,6*6)/2)+(1+2*(1/3*L17+2)*TAN(RADIANS(30)))*3+1*1/3*L17;
                    break;
                case "W15":
                    cbArea = (L4 > 30 ? 12 : 6) * ((L4 > 30 ? 12 : 6) + 8) * Math.PI / 2 + (L4 > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L18>30,12,6)*(IF(L18>30,12,6)+8)*PI()/2+IF(L18>30,12,12)*2*3;
                    break;
                case "W16":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L19>30,12*12,6*6)*PI()/2+IF(L19>30,12,6)*2*3;
                    break;
                case "W17":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L20>30,12*12,6*6)*PI()/2+IF(L20>30,12,6)*2*3;
                    break;
                case "W18":
                    cbArea = 10 * Math.Tan(radianOf1 * (45)) * 10 + Math.PI * ((L4 > 30 ? 12 * 12 : 6 * 6) / 2) + (1 + 2 * 10 * Math.Tan(radianOf1 * (45))) * 3 + 1 * 1 / 3 * L4;
                    //cbArea = 10*TAN(RADIANS(45))*10+PI()*(IF(L21>30,12*12,6*6)/2)+(1+2*10*TAN(RADIANS(45)))*3+1*1/3*L21;
                    break;
                case "W19":
                    cbArea = (L4 > 30 ? 12 : 6) * ((L4 > 30 ? 12 : 6) + 18) * Math.PI / 2 + (L4 > 30 ? 12 : 12) * 2 * 3;
                    //cbArea = IF(L22>30,12,6)*(IF(L22>30,12,6)+18)*PI()/2+IF(L22>30,12,12)*2*3;
                    break;
                case "W20":
                    cbArea = (L4 > 30 ? 12 * 12 : 6 * 6) * Math.PI / 2 + (L4 > 30 ? 12 : 6) * 2 * 3;
                    //cbArea = IF(L23>30,12*12,6*6)*PI()/2+IF(L23>30,12,6)*2*3;
                    break;
                default:
                    break;

            }


            return Convert.ToDouble(string.Format("{0:0.##}", cbArea));
        }
    }

    public class ResponseWeldKG : clsHelper.ResponseMsgWithStatus
    {
        public double? MinThk { get; set; }
        public double? FaceSideArea { get; set; }
        public double? CBArea { get; set; }
        public double? WeldKGFaceSide { get; set; }
        public double? WeldKgCBSide { get; set; }
        public double? TotalWeldKg { get; set; }
    }
}