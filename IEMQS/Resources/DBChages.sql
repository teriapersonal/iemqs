/**************************************  Section A: MENTION TABLE CHANGES HERE  ************************************/

/*



----To add Table Description





---- To Change Column Size

*/

/**************************************  End A ************************************/
/**************************************  Section B: MENTION FUNCTION CHANGES HERE  ************************************/
--Template for function
/*
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_NAME]') AND type in (N'FN', N'IF',N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_NAME]
Go
Create function [dbo].[FN_NAME]
( 
	@P_Location nvarchar(9)=null,
	@ActionByDeptcode nvarchar(Max) = null
)
RETURNS TABLE
AS
RETURN
(
	select distinct cm2.t_dimx,cm2.t_desc from COM002 cm2
	inner join COM003 cm3 on cm2.t_dimx = cm3.t_depc
	where cm3.t_depc is not null and LEN( cm3.t_depc) > 0 and cm3.t_loca = @P_Location and cm2.t_dimx + ' - ' + cm2.t_desc like '%'+@ActionByDeptcode+'%'
)
*/






--------------------------

--------------------------------------------------------------------------

/**************************************  End B ************************************/

/**************************************  Section C: MENTION VIEWS CHANGES HERE  ************************************/
--Template for View
/*
Go
IF EXISTS(SELECT 1 FROM sys.views WHERE NAME = 'VIEW_NAME')  
EXEC('DROP View VIEW_NAME')
Go
CREATE Viewj
	---------
End
Go*/


--Exec  [dbo].[SP_GET_ABL_LINES]  1,10,'','1=1 and BU in (''01'',''02'',''04'',''05'') and Location in (''HZW'',''PEW'',''RNW'')','0017005A','RT','RT_1','Not Attended','2019-01-01','2020-07-27'

GO

/****** Object:  StoredProcedure [dbo].[SP_SFM_GET_LIST_ITEM]    Script Date: 22-09-2020 18:31:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SP_SFM_GET_LIST_ITEM]
@project nvarchar(max),  
@itemNo nvarchar(max) = ''  
  
as   
begin
   DECLARE @SqlQuery NVARCHAR(MAX)=Null
	
	IF(@itemNo='1')
	BEGIN
			SET @SqlQuery = 'Select Distinct FindNo,Project,Length,Width,NoOfPieces,MaterialDescription as MaterialCode,Weight,Thickness 
			                 From dbo.FN_GET_HBOM_DATA( ''' + @project +''') WHERE Project=''' + @project + ''' AND  (LEFT (FindNo, 1)=1	OR LEFT (FindNo, 1)=2) AND LEN(FindNo)=3'
    END
	ELSE IF(@itemNo='All')
	 BEGIN
	     SET @SqlQuery = 'Select Distinct FindNo,Project,Length,Width,NoOfPieces,MaterialDescription as MaterialCode,Weight,Thickness 
			                 From dbo.FN_GET_HBOM_DATA( ''' + @project +''') WHERE Project=''' + @project + ''''
	 END
	 ELSE 
	 BEGIN
	     SET @SqlQuery = 'Select Distinct FindNo,Project,Length,Width,NoOfPieces,MaterialDescription as MaterialCode,Weight,Thickness 
			                 From dbo.FN_GET_HBOM_DATA( ''' + @project +''') WHERE Project=''' + @project + ''' AND  LEFT (FindNo, 1)=5 AND LEN(FindNo)=3'
	 END
   PRINT (@SqlQuery);
   EXEC (@SqlQuery);
  --   IF(@itemNo='1')
	 --BEGIN
  --   Select Distinct FindNo,Project,Length,Width,NoOfPieces,MaterialCode,Weight,Thickness 
		--	                 From dbo.FN_GET_HBOM_DATA( '' + @project +'') WHERE Project='' + @project + '' AND (LEFT (FindNo, 1)=1	OR LEFT (FindNo, 1)=2) AND LEN(FindNo)=3	
	 --END
	 --ELSE IF(@itemNo='All')
	 -- BEGIN
	 --    Select Distinct FindNo,Project,Length,Width,NoOfPieces,MaterialCode,Weight,Thickness 
		--	                 From dbo.FN_GET_HBOM_DATA( '' + @project +'') WHERE Project='' + @project + '' 
	 -- END
	 -- ELSE 
	 -- BEGIN
	 --    Select Distinct FindNo,Project,Length,Width,NoOfPieces,MaterialCode,Weight,Thickness 
		--	                 From dbo.FN_GET_HBOM_DATA( '' + @project +'') WHERE Project='' + @project + '' AND LEFT(FindNo, 1)=5 AND LEN(FindNo)=3
	 -- END
End

GO

