﻿using IEMQS.Areas.DES.Notifications;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace IEMQS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        string con = ConfigurationManager.ConnectionStrings["SQLNoti"].ConnectionString;
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            CustomModelBindersConfig.RegisterCustomModelBinders();

            //here in application_ster we will start sql dependency
            SqlDependency.Start(con);
        }
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            #region Date Formata Set
            var culturename = System.Configuration.ConfigurationManager.AppSettings["Culture"] != null ? System.Configuration.ConfigurationManager.AppSettings["Culture"].ToString() : "en-GB";
            var dateformat = System.Configuration.ConfigurationManager.AppSettings["DateFormat"] != null ? System.Configuration.ConfigurationManager.AppSettings["DateFormat"].ToString() : "dd/MM/yyyy";
            CultureInfo cInf = new CultureInfo(culturename, false);
            cInf.DateTimeFormat.DateSeparator = "/";
            cInf.DateTimeFormat.ShortDatePattern = dateformat;
            cInf.DateTimeFormat.LongDatePattern = dateformat + " hh:mm:ss tt";

            System.Threading.Thread.CurrentThread.CurrentCulture = cInf;
            System.Threading.Thread.CurrentThread.CurrentUICulture = cInf;
            #endregion
        }

        
        protected void Application_End()
        {
            SqlDependency.Stop(con);
        }

        #region Date Format Class Using
        public class CustomDateBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                if (controllerContext == null)
                    throw new ArgumentNullException("controllerContext", "controllerContext is null.");
                if (bindingContext == null)
                    throw new ArgumentNullException("bindingContext", "bindingContext is null.");

                var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

                if (value == null)
                    throw new ArgumentNullException(bindingContext.ModelName);

                CultureInfo cultureInf = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                var dateformat = System.Configuration.ConfigurationManager.AppSettings["DateFormat"] != null ? System.Configuration.ConfigurationManager.AppSettings["DateFormat"].ToString() : "dd/MM/yyyy";
                cultureInf.DateTimeFormat.ShortDatePattern = dateformat;

                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, value);

                try
                {
                    var date = value.ConvertTo(typeof(DateTime), cultureInf);

                    return date;
                }
                catch (Exception ex)
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                    return null;
                }
            }
        }

        public class NullableCustomDateBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                if (controllerContext == null)
                    throw new ArgumentNullException("controllerContext", "controllerContext is null.");
                if (bindingContext == null)
                    throw new ArgumentNullException("bindingContext", "bindingContext is null.");

                var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

                if (value == null) return null;
                var dateformat = System.Configuration.ConfigurationManager.AppSettings["DateFormat"] != null ? System.Configuration.ConfigurationManager.AppSettings["DateFormat"].ToString() : "dd/MM/yyyy";

                CultureInfo cultureInf = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                cultureInf.DateTimeFormat.ShortDatePattern = dateformat;

                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, value);

                try
                {
                    var date = value.ConvertTo(typeof(DateTime), cultureInf);

                    return date;
                }
                catch (Exception ex)
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                    return null;
                }
            }
        }
        #endregion
    }

    #region Date Format Class Using
    public static class CustomModelBindersConfig
    {
        public static void RegisterCustomModelBinders()
        {
            ModelBinders.Binders.Add(typeof(DateTime), new MvcApplication.CustomDateBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new MvcApplication.NullableCustomDateBinder());
        }
    }
    #endregion
}
