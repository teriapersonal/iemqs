﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Models
{
    /// <summary>
    /// Class that encapsulates most common parameters sent by DataTables plugin
    /// </summary>
    public class JQueryDataTableParamModel
    {
        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>       
        public object[] aoData { get; set; }

        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>       
        public string sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        private string _sSearch { get; set; }
        public string sSearch { get { return _sSearch; } set { _sSearch = value == null ? null : value.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]"); } }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }

        /// <summary>
        /// Is Main All Checkbox checked
        /// </summary>
        public bool bCheckedAll { get; set; }

        public string CompanyId { get; set; }
        public string UserTypeId { get; set; }

        public string CategoryId { get; set; }
        public int CTQLineHeaderId { get; set; }
        public string CTQHeaderId { get; set; }
        public string RevNo { get; set; }
        public string CTQCompileStatus { get; set; }
        public string MTStatus { get; set; }
        public string Status { get; set; }
        public string Department { get; set; }
        public string Headerid { get; set; }
        public string Nodeid { get; set; }

        //added for dashboard data (HTR Partial)
        public string dbDays1 { get; set; }
        public string dbDays2 { get; set; }
        public string dbDays3 { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string HTRNo { get; set; }
        public bool IsVisible { get; set; }

        //added for Temptt(Getgriddatatpartial)
        public string Location { get; set; }
        public string StageCode { get; set; }
        public string StageDesc { get; set; }
        public string Stagetype { get; set; }
        public string Sequenceno { get; set; }
        public string Notedraw { get; set; }
        public string Gpascal { get; set; }
        public string OverlayStage { get; set; }
        public string PTCRelated { get; set; }
        public string PrePostWeldHead { get; set; }
        public string IsHydro { get; set; }
        public string IsWelder { get; set; }
        public string StageDepartment { get; set; }
        public string Createdby { get; set; }
        public string CreatedOn { get; set; }

        public string TravelerNo { get; set; }
        public string IdentifictionNo { get; set; }
        public string Customer { get; set; }
        //
        public string RefDocument { get; set; }
        public string SearchFilter { get; set; }

        //added for Temptt(Getgriddatatpartial)
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Roles { get; set; }
        public string Address { get; set; }
        public string Birthdate { get; set; }
        public string Gender { get; set; }
        public string Salary { get; set; }
        public bool Married { get; set; }
        public int subassembly { get; set; }
        public string Filterstatus { get; set; }
        public string FilterTitle { get; set; }
        public string RouteNo { get; set; }
        public string ProductType { get; set; }
        public string FilterDocNo { get; set; }
        public bool isHistory { get; set; }
        public bool isActionPoint { get; set; }
        public bool Flag { get; set; }
        public string QualityProject { get; set; }
        public string SeamNo { get; set; }
        public bool IsDisplayOnly { get; set; }

        public string Shop { get; set; }
        public string CJCNo { get; set; }
    }
    public class LTFSOfferModel
    {
        public int LineID { get; set; }
        public string Shop { get; set; }
        public string ShopLocation { get; set; }
        public string ShopRemark { get; set; }
        public int? OfferedQuantity { get; set; }
        public string surfaceCondition { get; set; }
        public string folderPath { get; set; }
        public bool ISDocRequired { get; set; }

        public Nullable<int> weldThicknessReinforcement { get; set; }
    }
    public class SeamDetails
    {
        public string SeamNo { get; set; }
        public string SeamListNo { get; set; }
        public List<PartDetails> lstPartDetails { get; set; }
    }

    public class PartDetails
    {
        public string FindNo { get; set; }
        public string Part { get; set; }
    }
}