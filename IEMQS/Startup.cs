﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IEMQS.Startup))]
namespace IEMQS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
