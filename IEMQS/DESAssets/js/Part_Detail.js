﻿var tblChildItemList;
var tblParentItemList;

$(document).ready(function () {

   
    tblParentItemList = $('#tblParentItemList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Part/ParentItemList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "fixedColumns": {
            leftColumns: 5
        },
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": true, "targets": 0 },
            { "orderable": true, "targets": 1 },
            { "orderable": true, "targets": 2 },
            { "orderable": true, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": true, "visible": true, "targets": 5 },
            { "orderable": false, "visible": true, "targets": 6 },
            { "orderable": true, "visible": true, "targets": 7 },
            { "orderable": true, "visible": true, "targets": 8 },
            { "orderable": true, "visible": true, "targets": 9 },
            { "orderable": true, "visible": true, "targets": 10 },
            { "orderable": false, "visible": true, "targets": 11 },
            { "orderable": false, "visible": true, "targets": 12 },
            { "orderable": true, "visible": true, "targets": 13 },
            { "orderable": true, "visible": true, "targets": 14 },
            { "orderable": true, "visible": true, "targets": 15 },
            { "orderable": true, "visible": true, "targets": 16 },
            { "orderable": true, "visible": true, "targets": 17 },
            { "orderable": true, "visible": true, "targets": 18 },
            { "orderable": true, "visible": true, "targets": 19 },
            { "orderable": true, "visible": true, "targets": 20 },
            { "orderable": false, "visible": true, "targets": 21 },
            { "orderable": false, "visible": true, "targets": 22 },
            { "orderable": false, "visible": true, "targets": 23 },
            { "orderable": false, "visible": true, "targets": 24 },
            { "orderable": false, "visible": true, "targets": 25 },
            { "orderable": false, "visible": true, "targets": 26 },
            { "orderable": false, "visible": true, "targets": 27 },
            { "orderable": false, "visible": true, "targets": 28 },
            { "orderable": false, "visible": true, "targets": 29 },
            { "orderable": false, "visible": true, "targets": 30 },
            { "orderable": false, "visible": true, "targets": 31 },
            { "orderable": false, "visible": true, "targets": 32 },
            { "orderable": true, "visible": true, "targets": 33 },
            { "orderable": false, "visible": true, "targets": 34 },
            { "orderable": false, "visible": true, "targets": 35 },
            { "orderable": false, "visible": true, "targets": 36 },
            { "orderable": false, "visible": true, "targets": 37 },
            { "orderable": true, "visible": true, "targets": 38 },
            { "orderable": true, "visible": true, "targets": 39 },
            { "orderable": false, "visible": true, "targets": 40 },
            { "orderable": true, "visible": true, "targets": 41 },
            { "orderable": false, "visible": true, "targets": 42 },
            { "orderable": true, "visible": true, "targets": 43 },
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ItemId", value: $("#ItemId").val() });
            data.push({ name: "Project", value: $("#Project").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "Actions", "className": "nowrap", render: function (data, type, row, meta) {
                    var srt = "";

                    srt += '\n <a href="/DES/Part/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                    srt += '';
                    return srt;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    var state = "";
                    if (row.Type.toLowerCase() == "part") {
                        state = "brand";
                    }
                    else if (row.Type.toLowerCase() == "plate part") {
                        state = "info";
                    }
                    else if (row.Type.toLowerCase() == "clad part") {
                        state = "accent";
                    }
                    else if (row.Type.toLowerCase() == "jigfix") {
                        state = "success";
                    }
                    return '<span class="m--font-bold m--font-' + state + '">' + row.Type + "</span>"
                }
            },
            {
                "sTitle": "Item Id", "className": "", render: function (data, type, row, meta) {

                    var name = "<a  href='/DES/Part/PowerView?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                    return name;
                }
            },
            {
                "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                    return row.PerentItemKey;
                }
            },
            {
                "sTitle": "Rev No.", "className": "", render: function (data, type, row, meta) {
                    return ('R' + row.RevNo);
                }
            },
            {
                "sTitle": "DRG No.", "className": "", render: function (data, type, row, meta) {
                    return row.DRGNoDocumentNo;
                }
            },

            {
                "sTitle": "Child Part Status", "className": "", render: function (data, type, row, meta) {
                    return row.ItemState;
                }
            },

            {
                "sTitle": "Find No.", "className": "", render: function (data, type, row, meta) {

                    return row.FindNumber;
                }
            },
            {
                "sTitle": "Ext. Find No.", "className": "", render: function (data, type, row, meta) {
                    var ReExtFindNumbervNo = row.ExtFindNumber || '';
                    return ReExtFindNumbervNo;
                }
            },
            {
                "sTitle": "Find No. Desc", "className": "", render: function (data, type, row, meta) {
                    var FindNumberDescription = row.FindNumberDescription || '';
                    return FindNumberDescription;
                }
            },
            {
                "sTitle": "ARM Set", "className": "", render: function (data, type, row, meta) {
                    return row.ARMSet;
                }
            },
            {
                "sTitle": "ARM Rev", "className": "", render: function (data, type, row, meta) {
                    return row.ARMRev;
                }
            },
            {
                "sTitle": "Order Policy", "className": "", render: function (data, type, row, meta) {
                    return row.OrderPolicy;
                }
            },
            {
                "sTitle": "Product Form", "className": "", render: function (data, type, row, meta) {
                    return row.ProductForm;
                }
            },
            {
                "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                    return row.Material;
                }
            },
            {
                "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                    return row.ItemGroup;
                }
            },
            {
                "sTitle": "Item Description", "className": "", render: function (data, type, row, meta) {
                    return row.ItemName;
                }
            },
            {
                "sTitle": "String2", "className": "", render: function (data, type, row, meta) {
                    return row.String2;
                }
            },
            {
                "sTitle": "String3", "className": "", render: function (data, type, row, meta) {
                    return row.String3;
                }
            },
            {
                "sTitle": "String4", "className": "", render: function (data, type, row, meta) {
                    return row.String4;
                }
            },
            {
                "sTitle": "Procurement Drg", "className": "", render: function (data, type, row, meta) {
                    return row.ProcurementDrgDocumentNo;
                }
            },

            {
                "sTitle": "Item Weight (kg)", "className": "", render: function (data, type, row, meta) {
                    return row.ItemWeight;
                }
            },
            {
                "sTitle": "BOM Weight (kg)", "className": "", render: function (data, type, row, meta) {
                    return row.BOMWeight;
                }
            },
            {
                "sTitle": "Thickness", "className": "", render: function (data, type, row, meta) {
                    return row.Thickness;
                }
            },
            {
                "sTitle": "Clad Plate Part Thickness1", "className": "", render: function (data, type, row, meta) {
                    return row.CladPlatePartThickness1;
                }
            },
            {
                "sTitle": "IsDoubleClad", "className": "", render: function (data, type, row, meta) {
                    return row.IsDoubleClad;
                }
            },
            {
                "sTitle": "Clad Plate Part Thickness2", "className": "", render: function (data, type, row, meta) {
                    return row.CladPlatePartThickness2;
                }
            },
            {
                "sTitle": "Clad Specific Gravity2", "className": "", render: function (data, type, row, meta) {
                    return row.CladSpecificGravity2;
                }
            },
            {
                "sTitle": "CRS(Yes/No)", "className": "", render: function (data, type, row, meta) {
                    return row.CRSYN;
                }
            },
            {
                "sTitle": "Job Qty", "className": "", render: function (data, type, row, meta) {
                    return row.JobQty;
                }
            },
            {
                "sTitle": "Commissioning Spare Qty", "className": "", render: function (data, type, row, meta) {
                    return row.CommissioningSpareQty;
                }
            },
            {
                "sTitle": "Mandatory Spare Qty", "className": "", render: function (data, type, row, meta) {
                    return row.MandatorySpareQty;
                }
            },
            {
                "sTitle": "Extra Qty", "className": "", render: function (data, type, row, meta) {
                    return row.ExtraQty;
                }
            },
            {
                "sTitle": "Remarks", "className": "", render: function (data, type, row, meta) {
                    return row.Remarks;
                }
            },
            {
                "sTitle": "Quantity", "className": "", render: function (data, type, row, meta) {
                    return (row.Quantity || 0) + " " + row.UOM;
                }
            },
            {
                "sTitle": "Length (mm)", "className": "", render: function (data, type, row, meta) {
                    return row.Length;
                }
            },
            {
                "sTitle": "Width", "className": "", render: function (data, type, row, meta) {
                    return row.Width;
                }
            },
            {
                "sTitle": "Number Of Pieces", "className": "", render: function (data, type, row, meta) {
                    return row.NumberOfPieces;
                }
            },
            {
                "sTitle": "BOM Report Size", "className": "", render: function (data, type, row, meta) {
                    return row.BomReportSize;
                }
            },
            {
                "sTitle": "Drawing BOM Size", "className": "", render: function (data, type, row, meta) {
                    return row.DrawingBomSize;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item"> Type</li> <li class="list-inline-item"> Item Id </li><li class="list-inline-item"> DRG No </li><li class="list-inline-item"> Parent Item </li> <li class="list-inline-item"> Find No </li><li class="list-inline-item"> Ext Find No </li><li class="list-inline-item"> ARM Set </li><li class="list-inline-item"> Product Form </li><li class="list-inline-item"> Material </li><li class="list-inline-item"> Item Group </li><li class="list-inline-item"> Item Description </li> <li class="list-inline-item"> Procurement Drg </li></ul ></div ></div > ');
            $('#tblParentItemList_wrapper .dataTables_filter').append($searchButton);
            var $filterBoxPartBOMList = $('#tblParentItemList_wrapper .dataTables_filter input').unbind(), selfPartBOMList = this.api();
            var stoppedTypingPartBOMList;

            $filterBoxPartBOMList.on('keyup', function () {
                if (stoppedTypingPartBOMList) clearTimeout(stoppedTypingPartBOMList);
                stoppedTypingPartBOMList = setTimeout(function () {
                    selfPartBOMList.search($filterBoxPartBOMList.val()).draw();
                }, 500);
            });
        },

        "fnRowCallback": function () {
            tblParentItemList.columns.adjust();
        }

    });

    tblChildItemList = $('#tblChildItemList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Part/ChildItemList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "fixedColumns": {
            leftColumns: 5
        },
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": true, "targets": 0 },
            { "orderable": true, "targets": 1 },
            { "orderable": true, "targets": 2 },
            { "orderable": true, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": true, "visible": true, "targets": 5 },
            { "orderable": false, "visible": true, "targets": 6 },
            { "orderable": true, "visible": true, "targets": 7 },
            { "orderable": true, "visible": true, "targets": 8 },
            { "orderable": true, "visible": true, "targets": 9 },
            { "orderable": true, "visible": true, "targets": 10 },
            { "orderable": false, "visible": true, "targets": 11 },
            { "orderable": false, "visible": true, "targets": 12 },
            { "orderable": true, "visible": true, "targets": 13 },
            { "orderable": true, "visible": true, "targets": 14 },
            { "orderable": true, "visible": true, "targets": 15 },
            { "orderable": true, "visible": true, "targets": 16 },
            { "orderable": true, "visible": true, "targets": 17 },
            { "orderable": true, "visible": true, "targets": 18 },
            { "orderable": true, "visible": true, "targets": 19 },
            { "orderable": true, "visible": true, "targets": 20 },
            { "orderable": false, "visible": true, "targets": 21 },
            { "orderable": false, "visible": true, "targets": 22 },
            { "orderable": false, "visible": true, "targets": 23 },
            { "orderable": false, "visible": true, "targets": 24 },
            { "orderable": false, "visible": true, "targets": 25 },
            { "orderable": false, "visible": true, "targets": 26 },
            { "orderable": false, "visible": true, "targets": 27 },
            { "orderable": false, "visible": true, "targets": 28 },
            { "orderable": false, "visible": true, "targets": 29 },
            { "orderable": false, "visible": true, "targets": 30 },
            { "orderable": false, "visible": true, "targets": 31 },
            { "orderable": false, "visible": true, "targets": 32 },
            { "orderable": true, "visible": true, "targets": 33 },
            { "orderable": false, "visible": true, "targets": 34 },
            { "orderable": false, "visible": true, "targets": 35 },
            { "orderable": false, "visible": true, "targets": 36 },
            { "orderable": false, "visible": true, "targets": 37 },
            { "orderable": true, "visible": true, "targets": 38 },
            { "orderable": true, "visible": true, "targets": 39 },
            { "orderable": false, "visible": true, "targets": 40 },
            { "orderable": true, "visible": true, "targets": 41 },
            { "orderable": false, "visible": true, "targets": 42 },
            { "orderable": true, "visible": true, "targets": 43 },
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ItemId", value: $("#ItemId").val() });
            data.push({ name: "Project", value: $("#Project").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "Actions", "className": "nowrap", render: function (data, type, row, meta) {
                    var srt = "";

                    srt += '\n <a href="/DES/Part/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                    srt += '';
                    return srt;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    var state = "";
                    if (row.Type.toLowerCase() == "part") {
                        state = "brand";
                    }
                    else if (row.Type.toLowerCase() == "plate part") {
                        state = "info";
                    }
                    else if (row.Type.toLowerCase() == "clad part") {
                        state = "accent";
                    }
                    else if (row.Type.toLowerCase() == "jigfix") {
                        state = "success";
                    }
                    return '<span class="m--font-bold m--font-' + state + '">' + row.Type + "</span>"
                }
            },
            {
                "sTitle": "Item Id", "className": "", render: function (data, type, row, meta) {

                    var name = "<a  href='/DES/Part/PowerView?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                    return name;
                }
            },
            {
                "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                    return row.PerentItemKey;
                }
            },
            {
                "sTitle": "Rev No.", "className": "", render: function (data, type, row, meta) {
                    return ('R' + row.RevNo);
                }
            },
            {
                "sTitle": "DRG No.", "className": "", render: function (data, type, row, meta) {
                    return row.DRGNoDocumentNo;
                }
            },

            {
                "sTitle": "Child Part Status", "className": "", render: function (data, type, row, meta) {
                    return row.ItemState;
                }
            },

            {
                "sTitle": "Find No.", "className": "", render: function (data, type, row, meta) {

                    return row.FindNumber;
                }
            },
            {
                "sTitle": "Ext. Find No.", "className": "", render: function (data, type, row, meta) {
                    var ReExtFindNumbervNo = row.ExtFindNumber || '';
                    return ReExtFindNumbervNo;
                }
            },
            {
                "sTitle": "Find No. Desc", "className": "", render: function (data, type, row, meta) {
                    var FindNumberDescription = row.FindNumberDescription || '';
                    return FindNumberDescription;
                }
            },
            {
                "sTitle": "ARM Set", "className": "", render: function (data, type, row, meta) {
                    return row.ARMSet;
                }
            },
            {
                "sTitle": "ARM Rev", "className": "", render: function (data, type, row, meta) {
                    return row.ARMRev;
                }
            },
            {
                "sTitle": "Order Policy", "className": "", render: function (data, type, row, meta) {
                    return row.OrderPolicy;
                }
            },
            {
                "sTitle": "Product Form", "className": "", render: function (data, type, row, meta) {
                    return row.ProductForm;
                }
            },
            {
                "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                    return row.Material;
                }
            },
            {
                "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                    return row.ItemGroup;
                }
            },
            {
                "sTitle": "Item Description", "className": "", render: function (data, type, row, meta) {
                    return row.ItemName;
                }
            },
            {
                "sTitle": "String2", "className": "", render: function (data, type, row, meta) {
                    return row.String2;
                }
            },
            {
                "sTitle": "String3", "className": "", render: function (data, type, row, meta) {
                    return row.String3;
                }
            },
            {
                "sTitle": "String4", "className": "", render: function (data, type, row, meta) {
                    return row.String4;
                }
            },
            {
                "sTitle": "Procurement Drg", "className": "", render: function (data, type, row, meta) {
                    return row.ProcurementDrgDocumentNo;
                }
            },

            {
                "sTitle": "Item Weight (kg)", "className": "", render: function (data, type, row, meta) {
                    return row.ItemWeight;
                }
            },
            {
                "sTitle": "BOM Weight (kg)", "className": "", render: function (data, type, row, meta) {
                    return row.BOMWeight;
                }
            },
            {
                "sTitle": "Thickness", "className": "", render: function (data, type, row, meta) {
                    return row.Thickness;
                }
            },
            {
                "sTitle": "Clad Plate Part Thickness1", "className": "", render: function (data, type, row, meta) {
                    return row.CladPlatePartThickness1;
                }
            },
            {
                "sTitle": "IsDoubleClad", "className": "", render: function (data, type, row, meta) {
                    return row.IsDoubleClad;
                }
            },
            {
                "sTitle": "Clad Plate Part Thickness2", "className": "", render: function (data, type, row, meta) {
                    return row.CladPlatePartThickness2;
                }
            },
            {
                "sTitle": "Clad Specific Gravity2", "className": "", render: function (data, type, row, meta) {
                    return row.CladSpecificGravity2;
                }
            },
            {
                "sTitle": "CRS(Yes/No)", "className": "", render: function (data, type, row, meta) {
                    return row.CRSYN;
                }
            },
            {
                "sTitle": "Job Qty", "className": "", render: function (data, type, row, meta) {
                    return row.JobQty;
                }
            },
            {
                "sTitle": "Commissioning Spare Qty", "className": "", render: function (data, type, row, meta) {
                    return row.CommissioningSpareQty;
                }
            },
            {
                "sTitle": "Mandatory Spare Qty", "className": "", render: function (data, type, row, meta) {
                    return row.MandatorySpareQty;
                }
            },
            {
                "sTitle": "Extra Qty", "className": "", render: function (data, type, row, meta) {
                    return row.ExtraQty;
                }
            },
            {
                "sTitle": "Remarks", "className": "", render: function (data, type, row, meta) {
                    return row.Remarks;
                }
            },
            {
                "sTitle": "Quantity", "className": "", render: function (data, type, row, meta) {
                    return (row.Quantity || 0) + " " + row.UOM;
                }
            },
            {
                "sTitle": "Length (mm)", "className": "", render: function (data, type, row, meta) {
                    return row.Length;
                }
            },
            {
                "sTitle": "Width", "className": "", render: function (data, type, row, meta) {
                    return row.Width;
                }
            },
            {
                "sTitle": "Number Of Pieces", "className": "", render: function (data, type, row, meta) {
                    return row.NumberOfPieces;
                }
            },
            {
                "sTitle": "BOM Report Size", "className": "", render: function (data, type, row, meta) {
                    return row.BomReportSize;
                }
            },
            {
                "sTitle": "Drawing BOM Size", "className": "", render: function (data, type, row, meta) {
                    return row.DrawingBomSize;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
        ],

        "initComplete": function () {
            $searchButton1 = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item"> Type</li> <li class="list-inline-item"> Item Id </li><li class="list-inline-item"> DRG No </li><li class="list-inline-item"> Parent Item </li> <li class="list-inline-item"> Find No </li><li class="list-inline-item"> Ext Find No </li><li class="list-inline-item"> ARM Set </li><li class="list-inline-item"> Product Form </li><li class="list-inline-item"> Material </li><li class="list-inline-item"> Item Group </li><li class="list-inline-item"> Item Description </li> <li class="list-inline-item"> Procurement Drg </li></ul ></div ></div > ');
            $('#tblChildItemList_wrapper .dataTables_filter').append($searchButton1);
            var $filterBoxPartBOMList1 = $('#tblChildItemList_wrapper .dataTables_filter input').unbind(), selfPartBOMList1 = this.api();
            var stoppedTypingPartBOMList1;

            $filterBoxPartBOMList1.on('keyup', function () {
                if (stoppedTypingPartBOMList1) clearTimeout(stoppedTypingPartBOMList1);
                stoppedTypingPartBOMList1 = setTimeout(function () {
                    selfPartBOMList1.search($filterBoxPartBOMList1.val()).draw();
                }, 500);
            });
        },

        "fnRowCallback": function () {
            tblChildItemList.columns.adjust();
        }

    });
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblParentItemList != null)
        tblParentItemList.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblChildItemList != null)
        tblChildItemList.columns.adjust();
});