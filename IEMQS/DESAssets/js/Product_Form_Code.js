﻿var tblProductFormCode;
$(document).ready(function () {
    var IsITRoleGroup = $("#IsITRoleGroup").val();
    tblProductFormCode = $('#tblProductFormCode').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "auto-width": false,
        "sAjaxSource": "/ProductForm/GetProductFormList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },

        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],

        "columnDefs": [
            { "orderable": true, "targets": 0 },
            { "orderable": false, "targets": 9 },
            { "orderable": false, "targets": 10 },
            { "orderable": false, "targets": 11 },
            { "orderable": false, "targets": 12 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },

        "aoColumns": [
            {
                "sTitle": " BU <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var BUname = "<strong style='text-transform: capitalize;'>" + row.BU + "-" + row.PBU + "</strong>";
                    return BUname;
                }
            },
            {
                "sTitle": "Product Form <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var productform = "<strong style='text-transform: capitalize;'>" + row.ProductForm + "</strong>";
                    return productform;
                }
            },
            {
                "sTitle": "Sub-Product Form<span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    // var strHtml = '';
                    // strHtml = "<textarea name='GenralRemark' type='text' style='width:150px !important' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'   />";
                    var strHtml = "<strong style='text-transform: capitalize;' class='SubProductForm'>" + row.SubProductForm + "</strong>";
                    return strHtml;
                }
            },
            {
                "sTitle": "Product Form Code <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var productformcode = "<strong style='text-transform: capitalize;'>" + (row.ProductFormCode||'') + "</strong>";
                    return productformcode;
                }
            },
            {
                "sTitle": "String-2 <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Description1;
                }
            },
            {
                "sTitle": "String-3 &nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Description2;
                }
            },
            {
                "sTitle": "String-4 &nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Description3;
                }
            },
            {
                "sTitle": "BOM Report Size <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.BOMReportSize;
                }
            },
            {
                "sTitle": "Drawing BOM Size <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.DrawingBOMSize;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    if (IsITRoleGroup == "True") {
                        srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    }
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddProFormcodeRow();
        },

        "fnRowCallback": function () {
            tblProductFormCode.columns.adjust();
            AddProFormcodeRow();
        }

    });

});


function AddProFormcodeRow() {
    setTimeout(function () {
        $("#BUList_0").focus();
        if ($("#BUList_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td ><select class="form-control" id="BUList_0"><option value="">Select</option>';
        for (let i = 0; i < ddlbulist.length; i++) {
            html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        }
        html += '</select><br><label class="text-danger" id="errorBUList_0" style="display:none" >BU-PBU is required </label></td>'

            + '<td ><select class="form-control" id="ProductFormName_0"><option value="">Select</option>';
        for (let i = 0; i < ddlprodlist.length; i++) {
            html += "<option value='" + ddlprodlist[i] + "'>" + ddlprodlist[i] + "</option>";
        }
        html += '</select><br><label class="text-danger" id="errorProductFormName_0" style="display:none" >Product Form is required </label></td>'

            + '<td><input type="text" maxlength="100" class="form-control blockSpecialChar SubProductForm" id="SubProductFormName_0" name="SubProductFormName_0" placeholder="SubProduct Form" /> <label class="text-danger" id="errorSubProductFormName_0" style="display:none" >SubProduct Form is required </label></td> '
            + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar noSpace" id="ProductFormCodeName_0" name="ProductFormCodeName_0" placeholder="Product Form Code" /> <label class="text-danger" id="errorProductFormCodeName_0" style="display:none" >Product Form Code is required </label></td> '
            + '<td><input type="text" maxlength="50" class="form-control" id="Description1_0" name="Description1_0" placeholder="String-2" /><label class="text-danger" id="errorDescription1_0" style="display:none" >String-2 is required </label></td> '
            + '<td><input type="text" maxlength="50" class="form-control" id="Description2_0" name="Description2_0" placeholder="String-3" /><label class="text-danger" id="errorDescription2_0" style="display:none" >String-3 is required </label></td> '
            + '<td><input type="text" maxlength="50" class="form-control" id="Description3_0" name="Description3_0" placeholder="String-4" /><label class="text-danger" id="errorDescription3_0" style="display:none" >String-4 is required </label></td> '
            + '<td><input type="text" maxlength="300" class="form-control" id="BOMReportSize_0" name="BOMReportSize_0" placeholder="BOM Report Size" /><label class="text-danger" id="errorBOMReportSize_0" style="display:none" >BOM Report Size is required </label></td> '
            + '<td><input type="text" maxlength="300" class="form-control" id="DrawingBOMSize_0" name="DrawingBOMSize_0" placeholder="Drawing BOM Size" /><label class="text-danger" id="errorDrawingBOMSize_0" style="display:none" >Drawing BOM Size is required </label></td> '
            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddProductForm(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>'
        $('#tblProductFormCode tbody').prepend(html);
        $("#BUList_0").select2();
        $("#ProductFormName_0").select2();
    }, 1000);
}

function AddProductForm(id) {
    $("#errorBUList_" + id).hide();
    $("#errorProductFormName_" + id).hide();
    $("#errorSubProductFormName_" + id).hide();
    $("#errorProductFormCodeName_" + id).hide();
    $("#errorDescription1_" + id).hide();
    $("#errorDescription2_" + id).hide();
    $("#errorDescription3_" + id).hide();
    $("#errorBOMReportSize_" + id).hide();
    $("#errorDrawingBOMSize_" + id).hide();
    var bu = $("#BUList_" + id).val();
    var productform = $("#ProductFormName_" + id).val();
    var subproductform = $("#SubProductFormName_" + id).val();
    var productformcode = $("#ProductFormCodeName_" + id).val();
    var desc1 = $("#Description1_" + id).val();
    var desc2 = $("#Description2_" + id).val();
    var desc3 = $("#Description3_" + id).val();
    var bomreport = $("#BOMReportSize_" + id).val();
    var drawingbom = $("#DrawingBOMSize_" + id).val();
    if (bu != "" && productform != "" && subproductform != "" && productformcode != "" && desc1 != "" && bomreport != "" && drawingbom != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/ProductForm/AddProductForm',
            data: JSON.stringify({ ProductFormCodeId: id, BUId: bu, ProductForm: productform, SubProductForm: subproductform, ProductFormCode: productformcode, Description1: desc1, Description2: desc2, Description3: desc3, BOMReportSize: bomreport, DrawingBOMSize: drawingbom }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblProductFormCode.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (bu == "")
            $("#errorBUList_" + id).show();
        if (productform == "")
            $("#errorProductFormName_" + id).show();
        if (subproductform == "")
            $("#errorSubProductFormName_" + id).show();
        if (productformcode == "")
            $("#errorProductFormCodeName_" + id).show();
        if (desc1 == "")
            $("#errorDescription1_" + id).show();
        //if (desc2 == "")
        //    $("#errorDescription2_" + id).show();
        //if (desc3 == "")
        //    $("#errorDescription3_" + id).show();
        if (bomreport == "")
            $("#errorBOMReportSize_" + id).show();
        if (drawingbom == "")
            $("#errorDrawingBOMSize_" + id).show();
    }

}

$('#tblProductFormCode').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblProductFormCode.row(tr);
    var data = row.data();

    var v2 = data.Description2;
    var v3 = data.Description3;
    if (v2 == 'null' || v2 == '' || v2 == undefined)
        v2 = "";
    if (v3 == 'null' || v3 == '' || v3 == undefined)
        v3 = "";

    var html = '<td><select disabled id="BUList_' + data.ProductFormCodeId + '" name="BUList_' + data.ProductFormCodeId + '" value="' + data.BU + '" class="form-control">';
    for (let i = 0; i < ddlbulist.length; i++) {
        if (ddlbulist[i].BUId == data.BUId)
            html += "<option selected='selected' value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        else
            html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
    }
    html += '</select><label class="text-danger" id="errorBUList_0" style="display:none" >BU-PBU is required </label></td> '

        + '<td><select disabled id="ProductFormName_' + data.ProductFormCodeId + '" name="ProductFormName_' + data.ProductFormCodeId + '" value="' + data.t_prod + '" class="form-control">';
    for (let i = 0; i < ddlprodlist.length; i++) {
        if (ddlprodlist[i] == data.ProductForm)
            html += "<option selected='selected' value='" + ddlprodlist[i] + "'>" + ddlprodlist[i] + "</option>";
        else
            html += "<option value='" + ddlprodlist[i] + "'>" + ddlprodlist[i] + "</option>";
    }
    html += '</select><label class="text-danger" id="errorProductFormName_0" style="display:none" >Product Form is required </label></td> '

        //+ '<td><input type="text" maxlength="100" class="form-control blockSpecialChar" id="ProductFormName_' + data.ProductFormCodeId + '" name="ProductFormName_' + data.ProductFormCodeId + '" placeholder="Product Form" value="' + data.ProductForm + '" /> <label class="text-danger" id="errorProductFormName_' + data.ProductFormCodeId + '" style="display:none" >Product Form is required </label></td> '
        + '<td><input disabled type="text" maxlength="100" class="form-control blockSpecialChar SubProductForm" id="SubProductFormName_' + data.ProductFormCodeId + '" name="SubProductFormName_' + data.ProductFormCodeId + '" placeholder="Sub-Product Form" value="' + data.SubProductForm + '" /> <label class="text-danger" id="errorSubProductFormName_' + data.ProductFormCodeId + '" style="display:none" >Sub-Product Form is required </label></td> '
        + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar noSpace" id="ProductFormCodeName_' + data.ProductFormCodeId + '" name="ProductFormCodeName_' + data.ProductFormCodeId + '" placeholder="Product Form Code"  value="' + data.ProductFormCode + '" /> <label class="text-danger" id="errorProductFormCodeName_' + data.ProductFormCodeId + '" style="display:none" >Product Form Code is required </label></td> '
        + '<td><input type="text" maxlength="50" class="form-control" id="Description1_' + data.ProductFormCodeId + '" name="Description1_' + data.ProductFormCodeId + '" placeholder="String-2" value="' + data.Description1 + '" /><label class="text-danger" id="errorDescription1_' + data.ProductFormCodeId + '" style="display:none" >String-2 is required </label></td> '
        + '<td><input type="text" maxlength="50" class="form-control" id="Description2_' + data.ProductFormCodeId + '" name="Description2_' + data.ProductFormCodeId + '" placeholder="String-3" value="' + v2 + '" /><label class="text-danger" id="errorDescription2_' + data.ProductFormCodeId + '" style="display:none" >String-3 is required </label></td> '
        + '<td><input type="text" maxlength="50" class="form-control" id="Description3_' + data.ProductFormCodeId + '" name="Description3_' + data.ProductFormCodeId + '" placeholder="String-4" value="' + v3 + '" /><label class="text-danger" id="errorDescription3_' + data.ProductFormCodeId + '" style="display:none" >String-4 is required </label></td> '
        + '<td><input type="text" maxlength="300" class="form-control" id="BOMReportSize_' + data.ProductFormCodeId + '" name="BOMReportSize_' + data.ProductFormCodeId + '" placeholder="BOM Report Size" value="' + data.BOMReportSize + '"/><label class="text-danger" id="errorBOMReportSize_' + data.ProductFormCodeId + '" style="display:none" >BOM Report Size is required </label></td> '
        + '<td><input type="text" maxlength="300" class="form-control" id="DrawingBOMSize_' + data.ProductFormCodeId + '" name="DrawingBOMSize_' + data.ProductFormCodeId + '" placeholder="Drawing BOM Size" value="' + data.DrawingBOMSize + '"/><label class="text-danger" id="errorDrawingBOMSize_' + data.ProductFormCodeId + '" style="display:none" >Drawing BOM Size is required </label></td> '
        + '<td></td><td></td><td></td><td></td>'
        + '<td><a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddProductForm(' + data.ProductFormCodeId + ')" title="Update"><i class="la la-check text-info"></i></a>'
        + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblProductFormCodeClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>'
    $(tr).html(html);
    $("#BUList_" + data.ProductFormCodeId).focus();
    $("#BUList_" + data.ProductFormCodeId).select2();
    $("#ProductFormName_" + data.ProductFormCodeId).select2();
});

function tblProductFormCodeClear() {
    tblProductFormCode.ajax.reload();
}

$('#tblProductFormCode').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblProductFormCode.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/ProductForm/DeleteProductForm',
                    data: { Id: data.ProductFormCodeId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblProductFormCode.data().count() == 1) {
                                tblProductFormCode.ajax.reload();
                            }
                            else {
                                tblProductFormCode.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$('#tblProductFormCode').on('focusout', 'td .SubProductForm', function () {
    //var subproduct = $("#SubProductFormName_0").val();
    //var product = $("#ProductFormName_0").val();
    var ProductFormCodeId;
    var tr = $(this).closest('tr');
    var row = tblProductFormCode.row(tr);
    var data = row.data();

    if (data == undefined) {
        ProductFormCodeId = 0;
    }
    else {
        ProductFormCodeId = data.ProductFormCodeId;
    }
    //var subproduct = $("#SubProductFormName_0").val();
    var subproduct = $("#SubProductFormName_" + ProductFormCodeId).val();
    var product = $("#ProductFormName_" + ProductFormCodeId).val();
    //var ProductFormCodeId = data.SubProductForm;  

    var model = {
        SubProductForm: subproduct,
        ProductForm: product
        //SubProductForm: $('#SubProductForm' + SubProductForm).val(),
        //ProductFormCodeId: productformcodeid
    };

    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/ProductForm/UpdateSubProduct',
        data: JSON.stringify(model),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                mApp.unblockPage();
                toastr.info(response.Msg, "Info");
                //tblProductFormCode.ajax.reload();
            }
            else {
                mApp.unblockPage();
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblProductFormCode != null)
        tblProductFormCode.columns.adjust();
});
