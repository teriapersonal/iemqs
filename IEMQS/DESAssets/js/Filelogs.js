﻿var tblShowFilelogs;
$(document).ready(function () {

    tblShowFilelogs = $('#tblShowFilelogs').DataTable({
        "searching": true,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/AdvanceSearch/GetFilelogs",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "fixedColumns": {
            "leftColumns": 5
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            },
            { "orderable": false, "targets": 13 }
            //{ "orderable": false, "targets": 0 },
            //{ "orderable": false, "targets": 0 },
            //{ "orderable": false, "targets": 3 },
            //{ "orderable": false, "targets": 4 },
            //{ "orderable": false, "targets": 5 }
        ],
        "fnServerData": function (sSource, data, fnCallback) {
            //data.push({ name: "SearchText", value: $("#SearchText").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "DocumentMapping Id", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentMappingId;
                }
            },
            {
                "sTitle": "Project", "className": "", render: function (data, type, row, meta) {
                    var Project = "";
                    Project = "<i class='fa fa-folder text-warning'></i> <span class='m--font-bolder'>" + row.Project + "</span>";

                    return Project;
                }
            },
            {
                "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                    var DocumentNo = "";
                    DocumentNo = "<i class='flaticon-doc text-primary'></i> <span class='m--font-bolder'>" + row.DocumentNo + "</span>";

                    return DocumentNo;

                }
            },
            {
                "sTitle": "File Name", "className": "", render: function (data, type, row, meta) {
                    var FileName = "<span class='text-primary m--font-bolder'>" + row.FileName + "</span>";
                    return FileName;

                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    var DocumentRevision = "<span class='text-primary m--font-bolder'>" + row.DocumentRevision + "</span>";
                    return DocumentRevision;

                }
            },
            {
                "sTitle": "Ver", "className": "text-center", render: function (data, type, row, meta) {
                    var FileVersion = "<span class='text-primary m--font-bolder'>" + row.FileVersion + "</span>";
                    return FileVersion;

                }
            },
            {
                "sTitle": "From Loc", "className": "", render: function (data, type, row, meta) {
                    var FromLocation = "";
                    if (row.FromLocation == "HZMC") {
                        FromLocation = "<span class='text-primary m--font-bolder'>" + row.FromLocation + "</span>";
                    }
                    else if (row.FromLocation == "Powai") {
                        FromLocation = "<span class='text-warning m--font-bolder'>" + row.FromLocation + "</span>";
                    }
                    else if (row.FromLocation == "VHEW") {
                        FromLocation = "<span class='text-success m--font-bolder'>" + row.FromLocation + "</span>";
                    }
                    else {
                        FromLocation = "<span class='text-primary m--font-bolder'>" + row.FromLocation + "</span>";
                    }
                    return FromLocation;
                }
            },
            {
                "sTitle": "To Loc", "className": "", render: function (data, type, row, meta) {
                    var ToLocation = "<span class='text-primary m--font-bolder'>" + row.ToLocation + "</span>";
                    return ToLocation;

                }
            },
            {
                "sTitle": "Uploaded On", "className": "", render: function (data, type, row, meta) {
                    var FileIndex = "<span class='text-primary m--font-bolder'>" + row.CreatedOn + "</span>";
                    return FileIndex;

                }
            },
            {
                "sTitle": "Transfer Status", "className": "", render: function (data, type, row, meta) {
                    var FileStatus = "";
                    if (row.FileStatus == "Pending") {
                        FileStatus = "<span class=' m-badge m-badge--warning  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.FileStatus + "</span>";
                    }
                    else if (row.FileStatus == "In Process") {
                        FileStatus = "<span class=' m-badge m-badge--primary  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.FileStatus + "</span>";
                    }
                    else if (row.FileStatus == "Transferred") {
                        FileStatus = "<span class=' m-badge m-badge--success  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.FileStatus + "</span>";
                    }
                    return FileStatus;
                }
            },
            {
                "sTitle": "Transferred On", "className": "", render: function (data, type, row, meta) {
                    var FileTransfer = "<span class='text-primary m--font-bolder'>" + row.FileTransfredOn + "</span>";
                    return FileTransfer;

                }
            },
            {
                "sTitle": "Index Status", "className": "", render: function (data, type, row, meta) {
                    var FileIndexStatus = "";
                    if (row.FileIndexStatus == "Pending") {
                        FileIndexStatus = "<span class=' m-badge m-badge--warning  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.FileIndexStatus + "</span>";
                    }
                    else if (row.FileIndexStatus == "In Process") {
                        FileIndexStatus = "<span class=' m-badge m-badge--primary  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.FileIndexStatus + "</span>";
                    }
                    else if (row.FileIndexStatus == "Indexed") {
                        FileIndexStatus = "<span class=' m-badge m-badge--success  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.FileIndexStatus + "</span>";
                    }
                    return FileIndexStatus;

                }
            },
            {
                "sTitle": "Indexed On", "className": "", render: function (data, type, row, meta) {
                    var FileIndex = "<span class='text-primary m--font-bolder'>" + row.FileIndexedOn + "</span>";
                    return FileIndex;

                }
            },
            {
                "sTitle": "Schedular Date", "className": "", render: function (data, type, row, meta) {
                    var FileIndex = "<span class='text-primary m--font-bolder'>" + row.SolrSchedularLastRunDate + "</span>";
                    return FileIndex;

                }
            }
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Project</li><li class= "list-inline-item">Doc No</li><li class="list-inline-item">File Name</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);
            setTimeout(function () {
                tblShowFilelogs.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        }

    });
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblShowFilelogs != null)
        tblShowFilelogs.columns.adjust();
});
