﻿var tblJEPdata;

$(document).ready(function () {

    if ('@TempData["Info"]' == "JEP is already use by another user") {
        toastr.Info('JEP is already use by another user');
    }
    tblJEPdata = $('#tblJEPdata').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/JEP/Index",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Authorized > 0) {

                        if (IsInprocess.toLowerCase() == 'false') {
                            srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                        }
                        else {
                            if (row.ENGGRole > 0) {
                                if (row.Status != 'Draft') {
                                    srt += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnCustFeed" title="Add Customer Feedback"><i class="la la-comment text-info"></i></a>';
                                }
                            }
                            if (row.Status === 'Draft' || row.Status === 'Created') {
                                srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit m--font-brand"></i></a>';

                                if ((row.IsJEPCheckIn == true)) {
                                    if (row.JEPCheckInBy == getCurrentUser) {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                        srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                                    }
                                    else {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.JEPCheckInByName + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                    }
                                }
                                else {
                                    srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                                }
                            }
                            else {

                                if ((row.IsJEPCheckIn == true)) {
                                    if (row.JEPCheckInBy == getCurrentUser) {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                        srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                                    }
                                    else {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.JEPCheckInByName + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                    }
                                }
                                else {
                                    srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                                }
                            }
                        }
                    }
                    else
                    {
                        srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                    }
                    
                     //if (row.Status == 'Completed') {
                        //    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                        //}
                    return srt;
                }
            },
            {
                "sTitle": "JEP No", "className": "", "orderable": true,render: function (data, type, row, meta) {
                    return row.JEPNumber;
                }
            },
            {
                "sTitle": "JEP Description", "className": "text-wrap custom-table-width", "orderable": true, render: function (data, type, row, meta) {
                    return row.JEPDescription;
                }
            },
            {
                "sTitle": "Status", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.Status;
                }
            },
            {
                "sTitle": "Customer", "className": "", "orderable": true,render: function (data, type, row, meta) {
                    return row.Customer;
                }
            },
            {
                "sTitle": "Licensor", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.Licensor;
                }
            },
            {
                "sTitle": "Project End User", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.Purchaser;
                }
            },
            {
                "sTitle": "MR No", "className": "", "orderable": false,render: function (data, type, row, meta) {
                    return row.MRNo;
                }
            },
            {
                "sTitle": "Project Manager", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.ProjectManager;
                }
            }
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">JEP No</li> <li class="list-inline-item">JEP Description</li> <li class= "list-inline-item" >Customer</li><li class="list-inline-item">Project End User</li><li class="list-inline-item">Project Manager</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);
        },

        "fnRowCallback": function () {
            tblJEPdata.columns.adjust();
        }
    });
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblJEPdata != null)
        tblJEPdata.columns.adjust();
});

$('#tblJEPdata').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblJEPdata.row(tr);
    var data = row.data();
    var JEPMapp = {
        JEPId: data.JEPId
    };
    swal({
        title: "Do you want to release JEP?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            mApp.blockPage();
            $.ajax({
                url: "/DES/JEP/UpdateJEPCheckOut",
                data: JSON.stringify(JEPMapp),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        if (response.Status) {
                            toastr.info(response.Msg, "Info");
                            tblJEPdata.ajax.reload();
                            mApp.unblockPage();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }

            });
        }
    });
});


$('#tblJEPdata').on('click', 'td .btnCheckIn', function () {
    var tr = $(this).closest('tr');
    var row = tblJEPdata.row(tr);
    var data = row.data();
    if ((data.IsJEPCheckIn == true)) {
        if (data.JEPCheckInBy != getCurrentUser) {
            toastr.info("JEP is already in use by " + data.JEPCheckInByName, "Info");
        }
    }
});

$('#tblJEPdata').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblJEPdata.row(tr);
    var data = row.data();
    if ((data.IsJEPCheckIn == true)) {
        if (data.JEPCheckInBy == getCurrentUser) {
            window.location = "/DES/JEP/Edit?q=" + data.QString;
        }
        else {
            toastr.info("JEP is already in use by " + data.JEPCheckInByName, "Info");
        }
    }
    else {
        window.location = "/DES/JEP/Edit?q=" + data.QString;
    }
});

$('#tblJEPdata').on('click', 'td .btnCustFeed', function () {
    var tr = $(this).closest('tr');
    var row = tblJEPdata.row(tr);
    var data = row.data();
    if ((data.IsJEPCheckIn == true)) {
        if (data.JEPCheckInBy == getCurrentUser) {
            window.location = "/DES/JEP/GetCustomerFeedback?q=" + data.QString;
        }
        else {
            toastr.info("JEP is already in use by " + data.JEPCheckInByName, "Info");
        }
    }
    else {
        window.location = "/DES/JEP/GetCustomerFeedback?q=" + data.QString;
    }
});

//$('#tblJEPdata').on('click', 'td .btnUV', function () {
//    var tr = $(this).closest('tr'); 0
//    var row = tblJEPdata.row(tr);
//    var data = row.data();
//    var DocPathList = data.MainDocumentPath.split(",");
//    $.each(DocPathList, function (i) {
//        UniverSalFile(DocPathList[i]);
//    });
//});