﻿var tblAgencyWiseDoc;
var tblTransmittal;
$(document).ready(function () {
    //DownloadDoc();

    tblAgencyWiseDoc = $('#tblAgencyDoc').DataTable({
        "searching": true,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Transmittal/GetTransmittalAgency",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
        ],
        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "bSortable": false, "className": "", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Authorized > 0) {
                        if (IsInprocess.toLowerCase() == 'false') {
                        }
                        else {
                            srt += "<a  class='m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnGenerateTransmittal' title='Generate Transmittal'><i class='la la-external-link m--font-success'></i></a>";
                            srt += "<a  class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEdit' title='Edit'><i class='la la-edit  m--font-brand'></i></a>";
                            if ((row.IsAgencyCheckIn == true)) {
                                if (row.AgencyCheckInBy == getCurrentUser) {
                                    srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                    if (parseInt(row.TransTotal) > 0 && parseInt(row.IsCreated) > 0) {
                                    }
                                    else {
                                        srt += "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='Delete' onclick='DeleteAgencyWiseAllDoc(" + row.AgencyDocId + ")'><i class='la la-trash m--font-danger'></i></a>";
                                    }
                                }
                                else {
                                    srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.AgencyCheckInByName + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                }
                            }
                            else {
                                if (parseInt(row.TransTotal) > 0 && parseInt(row.IsCreated) > 0) {
                                }
                                else {
                                    srt += "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='Delete' onclick='DeleteAgencyWiseAllDoc(" + row.AgencyDocId + ")'><i class='la la-trash m--font-danger'></i></a>";
                                }
                            }
                        }
                    }
                    else {

                    }
                    return srt;
                }
            },
            {
                "sTitle": "Name", "className": "", render: function (data, type, row, meta) {
                    return row.AgencyName;
                }
            },
            {
                "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                    return row.AgencyDescription;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created Date", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited Date", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {

            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Name</li>  <li class= "list-inline-item" >Description</li></ul ></div ></div > ');
            $('#tblAgencyDoc_filter').append($searchButton);

            setTimeout(function () {
                tblAgencyWiseDoc.columns.adjust();
            }, 500);

        },

        "fnRowCallback": function () {
            tblAgencyWiseDoc.columns.adjust();
        }

    });

    tblTransmittal = $('#tblTransmittal').DataTable({
        "searching": true,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Transmittal/GetGenerateTransmittalList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 3 },
            //{ "orderable": false, "targets": 4 },
            //{ "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 }
        ],
        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {

                "sTitle": "Action", "bSortable": false, "className": "", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Authorized > 0) {
                        if (IsInprocess.toLowerCase() == 'false') {
                        }
                        else {
                            srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                            //srt += "<a href='/DES/Transmittal/GenerateTransmittal?q=" + row.GString + "' class='m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill' title='Generate Transmittal'><i class='la la-external-link m--font-success'></i></a>";
                            //srt += "<a href='/DES/Transmittal/Edit?q=" + row.QString + "' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' title='Edit'><i class='la la-edit  m--font-brand'></i></a>";
                            //srt += "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='Delete' onclick='DeleteAgencyWiseAllDoc(" + row.AgencyDocId + ")'><i class='la la-trash m--font-danger'></i></a>";
                            if (row.Status !== 'Draft') {
                                //if (!row.IsEmailSentToAgency) {
                                srt += "<a href='javascript:void(0);' class='m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill' title='Email' onClick = 'UpdateEmailFlag(" + row.TransmittalId + "," + row.AgencyId + ")'><i class='la la-envelope m--font-success'></i></a>";
                                //}
                            }
                            if (row.Status !== 'Draft') {
                                srt += "<a href='#'  class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='View Email History' onClick='BindEmailHistory(" + row.TransmittalId + ")'><i class='la la-history m--font-info'></i></a>";
                                //srt += "<a href='#'  class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='View Transmittal' onClick='BindTransmittalDetail(" + row.TransmittalId + ")'><i class='la la-eye m--font-info'></i></a>";
                            }
                            if (row.Status === 'Draft') {
                                //srt += "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='Edit' onclick='DeleteAgencyWiseAllDoc(" + row.TransmittalNo + ")'><i class='la la-edit m--font-brand'></i></a>";
                                srt += "<a href='/DES/Transmittal/TransmittalEdit?q=" + row.QString + "' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' title='Edit' ><i class='la la-edit  m--font-brand'></i></a>";
                            }

                            if (row.Status === 'Draft') {
                                //srt += "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='Edit' onclick='DeleteAgencyWiseAllDoc(" + row.TransmittalNo + ")'><i class='la la-edit m--font-brand'></i></a>";
                                //srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="DeleteTrasmittalWiseDoc(" + row.Id + ")" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                srt += "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete' title='Delete' ><i class='la la-trash m--font-danger'></i></a>"; //onclick='DeleteAgencyWiseAllDoc(" + row.Status + ")'
                            }
                        }
                    }
                    else {

                    }
                    return srt;
                }
            },
            {
                "sTitle": "Transmittal No", "className": "", render: function (data, type, row, meta) {
                    return row.TransmittalNo;
                }
            },
            {
                "sTitle": "Agency Name", "className": "", render: function (data, type, row, meta) {
                    return row.AgencyName;
                }
            },
            {
                "sTitle": "Status", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    if (row.Status === 'Draft') {
                        return "<span class='m-badge m-badge--warning m-badge--wide'>" + row.Status + "</span>";
                    }
                    else {
                        return row.Status;
                    }

                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created Date", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited Date", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {

            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Transmittal No</li>  <li class= "list-inline-item" >Agency Name</li></ul ></div ></div > ');
            $('#tblTransmittal_filter').append($searchButton);

            setTimeout(function () {
                tblTransmittal.columns.adjust();
            }, 500);

        },

        "fnRowCallback": function () {
            tblTransmittal.columns.adjust();
        }

    });
});

var path;
$('#tblTransmittal').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblTransmittal.row(tr);
    var data = row.data();

    var model = {
        TransmittalId: data.TransmittalId,
        TransmittalNo: data.TransmittalNo,
        AgencyId: data.AgencyId
    };
    $.ajax({
        type: "POST",
        url: "/DES/Transmittal/GetDownloadFiles", //If get File Then Document Mapping Add
        data: JSON.stringify(model),
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            if (response) {
                DownlaodReport(response);

                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Not Success");
                mApp.unblockPage();
            }
        },
        error: function (response) {
            toastr.error(response.Msg, "Error");
            mApp.unblockPage();
        }
    });

});

function DownlaodReport(response) {
    var dat = [];
    dat.push({ Param: "BU", Value: response.BU });
    dat.push({ Param: "ProjectNo", Value: response.Project });
    dat.push({ Param: "AgencyId", Value: response.AgencyId });
    dat.push({ Param: "TransmittalNo", Value: response.TransmittalNo });
    ShowReport(dat, "/DES/Transmittal - BU Wise", false, true, 'pdf', response.TransmittalNo, response);
}

function ShowReport(ParameterData, ReportPath, ShowAttachment, downloadFile, format, downloadName, response) {
    var newDocPathSplit = response.DocPathList[0].split('/');
    var newDocRemoveLast = newDocPathSplit.pop();
    var newDocPath = newDocPathSplit.join("/");
    if (typeof (ShowAttachment) == "undefined")
        ShowAttachment = false;

    if (typeof (downloadFile) == "undefined")
        downloadFile = false;
    if (typeof (format) == "undefined")
        format = "pdf";
    if (typeof (downloadName) == "undefined")
        downloadName = "";

    if (downloadFile) {
        $.ajax({
            type: "POST",
            url: WebsiteURL + "/Utility/General/ViewReportAndUploadToNode",
            dataType: "json",
            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            data: {
                serverRelativeUrlPath: ReportPath,
                ReportParameters: ParameterData,
                ShowAttachment: ShowAttachment,
                format: format,
                NewFileName: downloadName,
                PathCreate: newDocPath
            },
            success: function (result) {
                result.fullPath;
                response.DocPathList.push(result.fullPath);
                DownloadDoc(response);
                //result.fileName;
                //var docMapp = {
                //    newDocPath: response.DocPathList[0].split('//')[1].split('/')[2] + "/" + result.fileName,
                //    oldDocPath: result.fullPath
                //};
                //var FCSurl = response.CurrentLocationIp + "/api/DupData";
                //jQuery.ajax({
                //    url: FCSurl,
                //    data: JSON.stringify(docMapp),
                //    contentType: 'application/json; charset=utf-8',
                //    dataType: 'json',
                //    cache: false,
                //    async: true,
                //    processData: false,
                //    method: 'POST',
                //    type: 'POST', // For jQuery < 1.9
                //    success: function (response) {
                //        response.DocPathList.push(response.UpdatedFile);
                //        DownloadDoc(response);
                //    }
                //});
            }
        });
    }

}


function showLoading(flag) {
    if (flag) {
        $(".quick-nav-overlay").show();
        $("#divLoader").show();
    }
    else {
        $("#divLoader").hide();
        $(".quick-nav-overlay").hide();
    }
}

function DisplayNotiMessage(Type, Message, Title) {
    if (Type.toLowerCase() == "success") {
        toastr.success(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "error") {
        toastr.error(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "info") {
        toastr.info(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "warning") {
        toastr.warning(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
}




function DownloadDoc(response) {
    var FCSurl = response.CurrentLocationIp + "/Download";
    //window.open("" + FCSurl + "?DocList=" + DocPathList, "DownloadWindow", "width=750, height=750, top=0, screenX=1000, screenY=1000");
    popupWindow("" + FCSurl + "?DocList=" + response.DocPathList + "&TransmittalNo=" + response.TransmittalNo, "DownloadWindow", window, 500, 500);
}

function popupWindow(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}

function DeleteAgencyWiseAllDoc(AgencyDocId) {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Transmittal/DeleteAgencyWiseAllDoc',
                    data: { Id: AgencyDocId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblAgencyWiseDoc.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Info");
                    }
                });
            }
        });
}

$('#tblTransmittal').on('click', 'td .btnDelete', function () {
    //function DeleteTrasmittalWiseDoc(Id) {
    var tr = $(this).closest('tr');
    var row = tblTransmittal.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Transmittal/DeleteTransmittalWiseDoc',
                    data: { Id: data.TransmittalId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblTransmittal.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Info");
                    }
                });
            }
        });
});

function UpdateEmailFlag(TransmittalID, AgencyID) {
    var model = {};
    model.TransmittalId = TransmittalID;
    $.ajax({
        type: "POST",
        url: '/DES/Transmittal/AddEmailHistory',
        data: JSON.stringify({ model: model }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            OpenMailInOutLook(TransmittalID);
        },
        error: function (error) {
        }
    });
    return true;
}

function BindEmailHistory(TransmittalID) {
    //var model = {};
    //model.TransmittalId = TransmittalID;
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/Transmittal/ShowEmailHistory',
        data: JSON.stringify({ TransmittalId: TransmittalID }),
        dataType: 'Html',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyEmailHistory").html(result);
            $("#m_modal_1_3").modal("show");

        },
        error: function (error) {
        }
    });
    return true;
}

function OpenMailInOutLook(TransmittalId) {
    //$.ajax({
    //    type: "GET",
    //    url: '/DES/Transmittal/OpenTransmittalEmail',
    //    dataType: 'json',
    //    data: { AgencyId: AgencyID },
    //    contentType: "application/json;charset=utf-8",
    //    success: function (response) {
    //    },
    //    error: function (error) {
    //    }
    //})
    popupWindow("/DES/Transmittal/OpenTransmittalEmail?TransmittalId=" + TransmittalId, "EmailTemplateDownload", window, 500, 500);
}

function popupWindow(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}

function AdjustTransmittal() {
    setTimeout(function () {
        tblTransmittal.columns.adjust();
    }, 500);
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblAgencyWiseDoc != null)
        tblAgencyWiseDoc.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblTransmittal != null)
        tblTransmittal.columns.adjust();
});


$('#tblAgencyDoc').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblAgencyWiseDoc.row(tr);
    var data = row.data();
    var AgencyMapp = {
        AgencyId: data.AgencyId
    };
    swal({
        title: "Do you want to release Agency?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            mApp.blockPage();
            $.ajax({
                url: "/DES/Transmittal/UpdateAgencyCheckOut",
                data: JSON.stringify(AgencyMapp),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        if (response.Status) {
                            toastr.info(response.Msg, "Info");
                            tblAgencyWiseDoc.ajax.reload();
                            mApp.unblockPage();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }

            });
        }
    });
});


$('#tblAgencyDoc').on('click', 'td .btnCheckIn', function () {
    var tr = $(this).closest('tr');
    var row = tblAgencyWiseDoc.row(tr);
    var data = row.data();
    if ((data.IsAgencyCheckIn == true)) {
        if (data.AgencyCheckInBy != getCurrentUser) {
            toastr.info("Agency is already in use by " + data.AgencyCheckInByName, "Info");
        }
    }
});

$('#tblAgencyDoc').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblAgencyWiseDoc.row(tr);
    var data = row.data();
    if ((data.IsAgencyCheckIn == true)) {
        if (data.AgencyCheckInBy == getCurrentUser) {
            window.location = "/DES/Transmittal/Edit?q=" + data.QString;
        }
        else {
            toastr.info("Agency is already in use by " + data.AgencyCheckInByName, "Info");
        }
    }
    else {
        window.location = "/DES/Transmittal/Edit?q=" + data.QString;
    }
});

$('#tblAgencyDoc').on('click', 'td .btnGenerateTransmittal', function () {
    var tr = $(this).closest('tr');
    var row = tblAgencyWiseDoc.row(tr);
    var data = row.data();
    if ((data.IsAgencyCheckIn == true)) {
        if (data.AgencyCheckInBy == getCurrentUser) {
            window.location = "/DES/Transmittal/GenerateTransmittal?q=" + data.GString;
        }
        else {
            toastr.info("Agency is already in use by " + data.AgencyCheckInByName, "Info");
        }
    }
    else {
        window.location = "/DES/Transmittal/GenerateTransmittal?q=" + data.GString;
    }
});