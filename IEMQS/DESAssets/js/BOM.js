﻿var BOMItemChange = function () {
    GetBOMItemData();
}

var Itemxhr = {
    valueField: 'Id',
    labelField: 'Lookup',
    searchField: ['Lookup'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetItemListForBOM',
            type: 'GET',
            dataType: 'json',
            data: {
                Item: query,
                Contract: $("#Contract").val()
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    },
    onChange: BOMItemChange
}

var ParentItemxhr = {
    valueField: 'Id',
    labelField: 'Lookup',
    searchField: ['Lookup'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetParentItemListForBOM',
            type: 'GET',
            dataType: 'json',
            data: {
                Item: query,
                Project: $("#Project").val()
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    }
}

var ManufacturingDrawingxhr = {
    valueField: 'Id',
    labelField: 'Lookup',
    searchField: ['Lookup'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetManufacturingDrawing',
            type: 'GET',
            dataType: 'json',
            data: {
                DocNo: query,
                Contract: $("#Contract").val()
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    }
}
$(document).ready(function () {
    $("#ItemId").selectize(Itemxhr);
    $("#BOMModel_ParentItemId").selectize(ParentItemxhr);
    $("#BOMModel_DRGNoDocumentId").selectize(ManufacturingDrawingxhr);
    CalcAll();
});

function GetBOMItemData() {
    $("#Type").val("");
    $("#PolicyId").val("");
    $("#String2").val("");
    $("#String3").val("");
    $("#String4").val("");
    $("#Thickness").val("");
    $("#CladPlatePartThickness1").val("");
    $("#CladSpecificGravity1").val("");
    $("#IsDoubleClad").val("");
    $("#CladPlatePartThickness2").val("");
    $("#CladSpecificGravity2").val("");
    $("#ItemWeight").val("");
    $("#Density").val("");
    $("#WeightFactor").val("");
    $("#UOM").val("");
    $("#ProcurementDrgDocumentNo").val("");
    $("#BOMModel_BomReportSize").val("");
    $("#BOMModel_DrawingBomSize").val("");
    $("#BOMModel_HdnBomReportSize").val("");
    $("#BOMModel_HdnDrawingBomSize").val("");

    var itemid = $("#ItemId").val();
    if (parseFloat(itemid) > 0) {

        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetItemDataById',
            type: 'GET',
            traditional: true,
            data: { ItemId: itemid },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (result) {
                mApp.unblockPage();


                if (result != '' && result != null) {

                    $("#Type").val(result.Type);
                    $("#PolicyId").val(result.PolicyId);
                    $("#String2").val(result.String2);
                    $("#String3").val(result.String3);
                    $("#String4").val(result.String4);
                    $("#Thickness").val(result.Thickness);
                    $("#CladPlatePartThickness1").val(result.CladPlatePartThickness1);
                    $("#CladSpecificGravity1").val(result.CladSpecificGravity1);
                    $("#IsDoubleClad").val(result.IsDoubleClad);
                    $("#CladPlatePartThickness2").val(result.CladPlatePartThickness2);
                    $("#CladSpecificGravity2").val(result.CladSpecificGravity2);
                    $("#ItemWeight").val(result.ItemWeight);
                    $("#Density").val(result.Density);
                    $("#WeightFactor").val(result.WeightFactor);
                    $("#UOM").val(result.UOM);
                    $("#ProcurementDrgDocumentNo").val(result.ProcurementDrgDocumentNo);
                    $("#BOMModel_BomReportSize").val(result.ProductFormBOMReportSize);
                    $("#BOMModel_DrawingBomSize").val(result.ProductFormDrawingBOMSize);
                    $("#BOMModel_HdnBomReportSize").val(result.ProductFormBOMReportSize);
                    $("#BOMModel_HdnDrawingBomSize").val(result.ProductFormDrawingBOMSize);
                }
                //getBOMData();
                CalcAll();
                BOMReportDrawingStrChange();
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });

    } else {

    }
}

$("#BOMModel_Length,#BOMModel_Weight,#BOMModel_DRGNoDocumentId, #BOMModel_NumberOfPieces,#BOMModel_Quantity,#BOMModel_JobQty,#BOMModel_Width,#BOMModel_ParentItemId").on("keyup change", function () {
    CalcAll();
    BOMReportDrawingStrChange();
});

$("#BOMModel_NumberOfPieces,#BOMModel_JobQty").on("keyup", function () {
    var type = $("#Type").val();
    if (type == Plate_Part || type == Clad_Part) {
        if ("BOMModel_NumberOfPieces" == this.id) {
            $("#BOMModel_JobQty").val($("#BOMModel_NumberOfPieces").val());
        } else {
            $("#BOMModel_NumberOfPieces").val($("#BOMModel_JobQty").val());
        }
        CalcAll();
    }
});

function CalcAll() {

    if ($("#BOMModel_ParentItemId").val() != "") {
        $("#BOMModel_Weight").removeAttr("readonly");
        $("#BOMModel_NumberOfPieces").removeAttr("disabled");

        $("#BOMModel_Quantity").removeAttr("readonly");
    }
    var UOM = $("#UOM").val();
    var Length = parseFloat($("#BOMModel_Length").val() || 0);
    var Width = parseFloat($("#BOMModel_Width").val() || 0);
    var type = $("#Type").val();

    if ($("#BOMModel_ParentItemId").val() != "") {
        if (type == Part && (UOM != "m" && UOM != "kg" && UOM != "m2")) {
            $("#BOMModel_NumberOfPieces").val("");
            $("#BOMModel_NumberOfPieces").attr("disabled", "disabled");
        }

        if (UOM == "m" || UOM == "kg" || UOM == "m2") {
            $("#BOMModel_Length").removeAttr("disabled");
        } else {
            $("#BOMModel_Length").val("");
            $("#BOMModel_Length").attr("disabled", "disabled");
        }

        if (UOM == "kg" || UOM == "m2") {
            $("#BOMModel_Width").removeAttr("disabled");
        } else {
            $("#BOMModel_Width").val("");
            $("#BOMModel_Width").attr("disabled", "disabled");
        }
    }
    var NumberOfPieces = parseFloat($("#BOMModel_NumberOfPieces").val() || 0);



    var Density = parseFloat($("#Density").val() || 0);
    var Thickness = parseFloat($("#Thickness").val() || 0);
    var Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
    var CladPlatePartThickness1 = parseFloat($("#CladPlatePartThickness1").val() || 0);
    var CladPlatePartThickness2 = parseFloat($("#CladPlatePartThickness2").val() || 0);
    var WeightFactor = parseFloat($("#WeightFactor").val() || 0);
    var JobQty = parseFloat($("#BOMModel_JobQty").val() || 0);
    var isdou = $("#IsDoubleClad").val();



    if (type == Plate_Part) {
        //NumberOfPieces = JobQty;
        // $("#BOMModel_NumberOfPieces").val(JobQty);
        if (UOM == "kg") {
            var PartWeight = parseFloat(Thickness * Density).toFixed(2);

            var equ1 = ((Length / 1000) * (Width / 1000) * NumberOfPieces * PartWeight);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Quantity").val(equ)
                $("#BOMModel_Quantity").attr("readonly", "readonly");
            }
            Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);

            var BOMWeight = Quantity;
            //$("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
            //$("#ItemWeight").attr("readonly", "readonly");
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#BOMModel_Weight").attr("readonly", "readonly")
            }
        }

    }
    else if (type == Clad_Part) {
        //$("#BOMModel_NumberOfPieces").val(JobQty);
        //NumberOfPieces = JobQty;
        if (UOM == "kg") {
            var CladSpecificGravity1 = parseFloat($("#CladSpecificGravity1").val() || 0);
            var CladSpecificGravity2 = parseFloat($("#CladSpecificGravity2").val() || 0);

            var PartWeight = parseFloat((Thickness * Density) + (CladPlatePartThickness1 * CladSpecificGravity1)).toFixed(2);
            if (isdou == "Yes") {
                PartWeight = parseFloat(parseFloat(PartWeight) + parseFloat((CladPlatePartThickness2 * CladSpecificGravity2))).toFixed(2);
            }

            var equ1 = ((Length / 1000) * (Width / 1000) * NumberOfPieces * PartWeight);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Quantity").val(equ)
                $("#BOMModel_Quantity").attr("readonly", "readonly");
            }
            Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);

            var BOMWeight = Quantity;
            //$("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
            //$("#ItemWeight").attr("readonly", "readonly")
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#BOMModel_Weight").attr("readonly", "readonly")
            }
        }

    }
    else if (type == Part) {
        //$("#BOMModel_NumberOfPieces").val("");
        //NumberOfPieces = "";
        if (UOM == "m") {
            //$("#ItemWeight").attr("title", "Note that the user has to enter kg/m in weight of the part");
            var equ1 = ((Length / 1000) * NumberOfPieces);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Quantity").val(equ)
                $("#BOMModel_Quantity").attr("readonly", "readonly");
            }

            if (WeightFactor > 0) {
                var PartWeight = parseFloat(WeightFactor * Density).toFixed(2);
                Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
                var BOMWeight = parseFloat(Quantity * PartWeight).toFixed(2)
                //$("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
                //$("#ItemWeight").attr("readonly", "readonly")
                if ($("#BOMModel_ParentItemId").val() != "") {
                    $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#BOMModel_Weight").attr("readonly", "readonly")
                }
            }
            else {
                var PartWeight = $("#ItemWeight").val() || 0;
                Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
                var BOMWeight = Quantity * PartWeight;
                if ($("#BOMModel_ParentItemId").val() != "") {
                    $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#BOMModel_Weight").attr("readonly", "readonly")
                }
            }

        }
        else if (UOM == "m2") {
            //$("#ItemWeight").attr("title", "Note that the user has to enter kg/m2 in weight of the part");
            var equ1 = ((Length / 1000) * (Width / 1000) * NumberOfPieces);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Quantity").val(equ)
                $("#BOMModel_Quantity").attr("readonly", "readonly");
            }


            var PartWeight = $("#ItemWeight").val() || 0;
            Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
            var BOMWeight = Quantity * PartWeight;
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#BOMModel_Weight").attr("readonly", "readonly")
            }

        } else if (UOM == "kg") {
            //$("#ItemWeight").attr("title", "Note that the user has to enter kg/m in weight of the part");

            $("#BOMModel_Length").val("");
            $("#BOMModel_Width").val("");
            $("#BOMModel_Length").attr("disabled", "disabled");
            $("#BOMModel_Width").attr("disabled", "disabled");
            var Length = parseFloat($("#BOMModel_Length").val() || 0);
            var Width = parseFloat($("#BOMModel_Width").val() || 0);

            if (WeightFactor > 0) {
                var PartWeight = parseFloat(WeightFactor * Density).toFixed(2);
                Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
                var BOMWeight = parseFloat(Quantity * PartWeight).toFixed(2)
                //$("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
                //$("#ItemWeight").attr("readonly", "readonly")
                if ($("#BOMModel_ParentItemId").val() != "") {
                    $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#BOMModel_Weight").attr("readonly", "readonly")
                }
            }
            else {
                var PartWeight = $("#ItemWeight").val() || 0;
                Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
                var BOMWeight = Quantity * PartWeight;
                if ($("#BOMModel_ParentItemId").val() != "") {
                    $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#BOMModel_Weight").attr("readonly", "readonly")
                }
            }

        } else {
            var PartWeight = $("#ItemWeight").val() || 0;
            Quantity = parseFloat($("#BOMModel_Quantity").val() || 0);
            var BOMWeight = Quantity * PartWeight;
            if ($("#BOMModel_ParentItemId").val() != "") {
                $("#BOMModel_Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#BOMModel_Weight").attr("readonly", "readonly")
            }
        }



    }
    else if (type == Jigfix) {
        //$("#BOMModel_NumberOfPieces").val("");
        //NumberOfPieces = "";
    }



}

function BOMReportDrawingStrChange() {
    if ($("#ProcurementDrgDocumentNo").val() == "") {

        var bs = $("#BOMModel_HdnBomReportSize").val() || "";
        var ds = $("#BOMModel_HdnDrawingBomSize").val() || "";
        var ProcDrgNo = $("#ProcurementDrgDocumentNo").val() || "";
        var DrgNo = $("#BOMModel_DRGNoDocumentId option:selected").text() || "";




        if (PartFixList != null && PartFixList.length > 0) {
            for (var i = 0; i < PartFixList.length; i++) {
                var data = $("#" + PartFixList[i].Description).val() || "";

                var regExp = new RegExp(PartFixList[i].Lookup, 'gi');
                bs = String(bs).replace(regExp, data);
                ds = String(ds).replace(regExp, data);

            }
        }

        bs = String(bs).replace(/#ProcDrgNo#/gi, ProcDrgNo);
        bs = String(bs).replace(/#DrgNo#/gi, DrgNo);

        ds = String(ds).replace(/#ProcDrgNo#/gi, ProcDrgNo);
        ds = String(ds).replace(/#DrgNo#/gi, DrgNo);
        if (bs != "")
            $("#BOMModel_BomReportSize").val(bs);
        if (ds != "")
            $("#BOMModel_DrawingBomSize").val(ds);
    } else {
        var v = "Refer " + $("#ProcurementDrgDocumentNo").val();
        $("#BOMModel_BomReportSize").val(v);
        $("#BOMModel_DrawingBomSize").val(v);
    }
}



$("#ItemId,#BOMModel_ParentItemId,#BOMModel_FindNumber").on("focusout change", function () {
    getBOMData();
});

function getBOMData() {
    var DRGNoDocumentId = $("#BOMModel_DRGNoDocumentId").selectize(ManufacturingDrawingxhr);
    var selectizeDRGNoDocumentId = DRGNoDocumentId[0].selectize;
    selectizeDRGNoDocumentId.setValue("", false);
    $("#BOMModel_ExtFindNumber").val("");
    $("#BOMModel_FindNumberDescription").val("");
    $("#BOMModel_Weight").val("");
    $("#BOMModel_JobQty").val("");
    $("#BOMModel_CommissioningSpareQty").val("");
    $("#BOMModel_MandatorySpareQty").val("");
    $("#BOMModel_OperationSpareQty").val("");
    $("#BOMModel_ExtraQty").val("");
    $("#BOMModel_Remarks").val("");
    $("#BOMModel_Quantity").val("");
    $("#BOMModel_Length").val("");
    $("#BOMModel_Width").val("");
    $("#BOMModel_NumberOfPieces").val("");
    if ($("#ItemId").val() != '' && $("#BOMModel_ParentItemId").val() != '' && $("#BOMModel_FindNumber").val() != '') {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetDataByItemAndParentItem',
            type: 'GET',
            traditional: true,
            data: { ItemId: $("#ItemId").val(), ParentItemId: $("#BOMModel_ParentItemId").val(), Project: $("#Project").val(), FindNumber: $("#BOMModel_FindNumber").val() },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (result) {
                mApp.unblockPage();

                if (result != '' && result != null) {

                    if (result.DRGNoDocumentId != "") {
                        var DRGNoDocumentxhroptions = [{ Id: result.DRGNoDocumentId, Lookup: result.DRGNoDocumentNo }]
                        var DRGNoDocumentId = $("#BOMModel_DRGNoDocumentId").selectize(ManufacturingDrawingxhr);
                        var selectizeDRGNoDocumentId = DRGNoDocumentId[0].selectize;
                        selectizeDRGNoDocumentId.addOption(DRGNoDocumentxhroptions);
                        selectizeDRGNoDocumentId.setValue(result.DRGNoDocumentId, false);
                    }
                    $("#BOMModel_ActionForPCRelation").val("Update");
                    $("#BOMModel_ExtFindNumber").val(result.ExtFindNumber);
                    $("#BOMModel_FindNumberDescription").val(result.FindNumberDescription);
                    $("#BOMModel_Weight").val(result.Weight);
                    $("#BOMModel_JobQty").val(result.JobQty);
                    $("#BOMModel_CommissioningSpareQty").val(result.CommissioningSpareQty);
                    $("#BOMModel_MandatorySpareQty").val(result.MandatorySpareQty);
                    $("#BOMModel_OperationSpareQty").val(result.OperationSpareQty);
                    $("#BOMModel_ExtraQty").val(result.ExtraQty);
                    $("#BOMModel_Remarks").val(result.Remarks);
                    $("#BOMModel_Quantity").val(result.Quantity);
                    $("#BOMModel_Length").val(result.Length);
                    $("#BOMModel_Width").val(result.Width);
                    $("#BOMModel_NumberOfPieces").val(result.NumberOfPieces);
                    $("#BOMModel_BomReportSize").val(result.BomReportSize);
                    $("#BOMModel_DrawingBomSize").val(result.DrawingBomSize);
                    $("#BOMModel_HdnBomReportSize").val(result.HdnBomReportSize);
                    $("#BOMModel_HdnDrawingBomSize").val(result.HdnDrawingBomSize);
                }
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }

}



var funBOMSuccess = function (data) {

    if (data.Status) {
        location.href = data.RedirectAction;
    } else {
        mApp.unblockPage();
        if (data.Confirm) {
            swal({
                backdrop: false,
                title: data.Msg,
                type: "info",
                allowOutsideClick: false,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonClass: "btn btn-danger",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            }).then(
                function (isConfirm) {
                    if (isConfirm.value) {
                        $('#BOMModel_ConfirmRevision').val("true");
                        $("#FormBOM").submit();
                    }
                });
        } else {
            toastr.info(data.Msg, "Info");
        }
    }
}

var funBOMBegin = function (data) {
    $('span[data-valmsg-for="BOMModel.ItemId"]').html("");
    $("#FormBOM").valid();

    if ($("#BOMModel_ParentItemId").val() == $("#ItemId").val()) {
        $('span[data-valmsg-for="BOMModel.ItemId"]').html("Item Id and Parent Item Id can not be same")
        return false;
    }
    mApp.blockPage();
    return true;
}

