var DatatablesBasicScrollable2 = {
    init: function () {
        var e;
        (e = $("#jep-document-list")).DataTable({
            //dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            scrollY: "20vh",
            scrollX: !0,
            order: [[1, "desc"]],
            headerCallback: function (e, a, t, n, s) {
                e.getElementsByTagName("th")[0].innerHTML = '\n                    <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                        <input type="checkbox" value="" class="m-group-checkable">\n                        <span></span>\n                    </label>'
            },
            columnDefs: [
                {
                    targets: 0,
                    width: "30px",
                    className: "dt-right",
                    orderable: !1,
                    render: function (e, a, t, n) {
                        return '\n                        <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                            <input type="checkbox" value="" class="m-checkable">\n                            <span></span>\n                        </label>'
                    }
            }]
        }), e.on("change", ".m-group-checkable", function () {
            var e = $(this).closest("table").find("td:first-child .m-checkable"),
                a = $(this).is(":checked");
            $(e).each(function () {
                a ? ($(this).prop("checked", !0), $(this).closest("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).closest("tr").removeClass("active"))
            })
        }), e.on("change", "tbody tr .m-checkbox", function () {
            $(this).parents("tr").toggleClass("active")
        })
    }
};

var DatatablesBasicScrollable1 = {
    init: function () {
        var e;
        (e = $("#global-document-list")).DataTable({
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            scrollY: "20vh",
            scrollX: !0,
            order: [[1, "desc"]],
            headerCallback: function (e, a, t, n, s) {
                e.getElementsByTagName("th")[0].innerHTML = '\n                    <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                        <input type="checkbox" value="" class="m-group-checkable">\n                        <span></span>\n                    </label>'
            },
            columnDefs: [
                {
                    targets: 0,
                    width: "30px",
                    className: "dt-right",
                    orderable: !1,
                    render: function (e, a, t, n) {
                        return '\n                        <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                            <input type="checkbox" value="" class="m-checkable">\n                            <span></span>\n                        </label>'
                    }
            }]
        }), e.on("change", ".m-group-checkable", function () {
            var e = $(this).closest("table").find("td:first-child .m-checkable"),
                a = $(this).is(":checked");
            $(e).each(function () {
                a ? ($(this).prop("checked", !0), $(this).closest("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).closest("tr").removeClass("active"))
            })
        }), e.on("change", "tbody tr .m-checkbox", function () {
            $(this).parents("tr").toggleClass("active")
        })
    }
};
var DatatablesBasicScrollable3 = {
    init: function () {
        var e;
        (e = $("#link-document-list")).DataTable({
          /*  dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",*/
            scrollY: "20vh",
            scrollX: !0,
            order: [[1, "desc"]],
            columnDefs: [{
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Link to JEP">\n                          <i class="la la-link m--font-success"></i>\n </a>'
                }
            }, ]

        })
    }
};
jQuery(document).ready(function () {

    DatatablesBasicScrollable2.init()
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});
jQuery(document).ready(function () {
    DatatablesBasicScrollable1.init()

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});
jQuery(document).ready(function () {
    DatatablesBasicScrollable3.init()

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});
$(document).on('shown.bs.modal', function (e) {
    $.fn.dataTable.tables({
        visible: true,
        api: true
    }).columns.adjust();
});