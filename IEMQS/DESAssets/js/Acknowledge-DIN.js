﻿
    $(document).ready(function () {
        BindJepDocumentDetails();
    });

var tblJepDocumentDetails;
//Bind Jep Document Details
function BindJepDocumentDetails() {
    mApp.blockPage();
    tblJepDocumentDetails = $('#tblJepDocumentDetails').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DIN/GetJepDocumentDetails",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[0, 'desc']],
        "columnDefs": [
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
        {
            "sTitle": "Action", "className": "action",visible:false, "orderable": false, render: function (data, type, row, meta) {
                var srt = "";
                srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnCreateDCR" title="Create DCR"><i class="la la-external-link text-success"></i></a>';
                srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"><i class="la la-eye m--font-success"></i></a>';
                srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                return srt;
            }
        },
        {
            "sTitle": "SerialNo",
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },

       {
           "sTitle": "Document No", render: function (data, type, row, meta) {
               var strHtml = '<i class="flaticon-doc m--font-accent"></i>' + row.DocumentNo + '';
               return strHtml;
           }
       },
         {
             "sTitle": "Document Description", render: function (data, type, row, meta) {
                 return row.DocumentTitle;
             }
         },
              {
                  "sTitle": "Status", "orderable": false, render: function (data, type, row, meta) {
                      return "<i class='fa fa-arrow-up 'style='color: " + row.colorStatus + " '></i>";

                  }
              },
         {
             "sTitle": "Rev No", "orderable": false, render: function (data, type, row, meta) {
                 return "R" + row.DocumentRevision;
             }
         },

          {
              "sTitle": "Remarks", "orderable": false, render: function (data, type, row, meta) {
                  var strHtml = '';
                  if (row.GeneralRemarks == null || row.GeneralRemarks == "undifined") {
                      strHtml = "<textarea name='GenralRemark' type='text' id='GenralRemark" + row.DocumentId + "'  class='form-control m-input  m-input--square GenralRemark' disabled  />";
                  }
                  else {
                      strHtml = "<textarea name='GenralRemark'  type='text' id='GenralRemark" + row.DocumentId + "'  class='form-control m-input  m-input--square GenralRemark'  disabled >" + row.GeneralRemarks + "</textarea>";
                  }
                  return strHtml;
              }
          },
            {
                "sTitle": "DocumentId", "orderable": false, visible: false, render: function (data, type, row, meta) {
                    return row.DocumentId;
                }
            },
           {
               "sTitle": "Related DCR", "orderable": false, render: function (data, type, row, meta) {
                   return row.DCRNo;
               }
           }
        ],

        "initComplete": function () {
            tblJepDocumentDetails.columns.adjust();
            mApp.unblockPage();
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblJepDocumentDetails.columns.adjust();
            }, 1000);
            mApp.unblockPage();
        }
    });
    mApp.unblockPage();
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$('#DinACKNote').click(function () {
    var DINId = $('#DINId').val();
    var Note = $('#DINAcknowledgeNote').val();
    var AckData = {
        DINId: DINId,
        DINAcknowledgeNote: Note
    };
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: "/DES/DIN/AddAcknowledgeNote",
        data: JSON.stringify(AckData),
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (response) {
            toastr.error(response.Msg, "Error");
            mApp.unblockPage();
        }
    });
});
