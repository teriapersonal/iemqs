﻿var tblAllARMSet;
$(document).ready(function () {

    tblAllARMSet = $('#tblAllARMSet').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": false,
        "sAjaxSource": "/DES/Part/GetAllARMSetList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
            //{ "orderable": false, "targets": 5 },
            //{ "orderable": false, "targets": 6 },
            //{ "orderable": false, "targets": 7 },
            //{ "orderable": false, "targets": 8 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "searcharmset", value: $("#txtsearcharmset").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "", "className": "", render: function (data, type, row, meta) {
                    var srt = "";                    
                    srt += '<a href="javascript:;" onclick="btnARMSetCopy(this)" data-id="' + row.Arms + '" title="Select"><i class="fa fa-link m--font-accent"></i> </a>';
                    return srt;
                }
            },
            {
                "sTitle": "ARMSet", "className": "", render: function (data, type, row, meta) {
                    return row.Arms;
                }
            },           
            {
                "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                    return row.Desc;
                }
            },
            {
                "sTitle": "Ver", "className": "", render: function (data, type, row, meta) {
                    return row.Vrsn;
                }
            },
            {
                "sTitle": "ARMCode", "className": "", render: function (data, type, row, meta) {
                    return row.ARMName;
                }
            },
            {
                "sTitle": "File Name", "className": "", render: function (data, type, row, meta) {
                    return row.Fnam;
                }
            },
            {
                "sTitle": "Status", "className": "", render: function (data, type, row, meta) {
                    //return row.Status;
                    var strHtml = '';                    
                    if (row.Status == true) {
                        strHtml = "<i class='la la-check'></i>";
                    }
                    else {
                        strHtml = "<i class='la la-close'></i>";
                    }
                    return strHtml;
                } 
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    return row.Type;
                } 
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }
    });
    setTimeout(function () {
        tblAllARMSet.columns.adjust();
    }, 500);
});



