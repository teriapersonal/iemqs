﻿var tblCategoryAction;

$(document).ready(function () {

    tblCategoryAction = $('#tblCategoryAction').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "autoWidth": false,
        "sAjaxSource": "/Parameter/GetCategoryActionList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'asc']],
        "columnDefs": [
            //{ "orderable": true, "targets": 0, "width": "100px" },
            //{ "orderable": true, "targets": 1, "width": "100px" },
            //{ "orderable": true, "targets": 2, "width": "200px" },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Type <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.TypeName + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "Parent &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.ParentName;
                }
            },
            {
                "sTitle": "Name <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    return row.Name;
                }
            },
            {
                "sTitle": "URL <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    return row.URL;
                }
            },
            {
                "sTitle": "IsTarget ", "className": "", render: function (data, type, row, meta) {
                    //return row.IsTarget;
                    var strHtml = '';
                    if (row.IsTarget > 0) {
                        strHtml = "<i class='la la-check'></i>";
                    }
                    else {
                        strHtml = "<i class='la la-close'></i>";
                    }
                    return strHtml;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddcaRow();
        },

        "fnRowCallback": function () {
            AddcaRow();
        }
    });

});

function AddcaRow() {
    setTimeout(function () {
        $("#Type_0").focus();
        if ($("#Name_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td><select id="Type_0" class= "form-control"><option value="">Select</option>';
        for (let i = 0; i < MenuTypeList.length; i++) {
            html += "<option value='" + MenuTypeList[i].Value + "'>" + MenuTypeList[i].Text + "</option>";
        }
        html += '</select><label class="text-danger" id="errorType_0" style="display:none" > Type is required </label></td> '

            + '<td><select id="Parent_0" class= "form-control"><option value="">Select</option>';
        html += '</select><label class="text-danger" id="errorParent_0" style="display:none" > ParentName is required </label></td> '

            + '<td><input type="text" maxlength="100" class="form-control" id="Name_0" Name="Name_0" placeholder="Name" /><label class="text-danger" id="errorName_0" style="display:none" > Name is required </label></td> '
            + '<td><input type="text" maxlength="500" class="form-control" id="URL_0" Name="URL_0" placeholder="URL" /><label class="text-danger" id="errorURL_0" style="display:none" > URL is required </label></td> '
            + '<td><label class="m-checkbox m-checkbox--solid m-checkbox--success"><input type="checkbox" id="IsTarget_0" name="IsTarget_0"><span></span></label></td> '
            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddCategoryAction(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>';
        $('#tblCategoryAction tbody').prepend(html);
    }, 1000);
}

function AddCategoryAction(id) {
    $("#errorType_" + id).hide();
    $("#errorParent_" + id).hide();
    $("#errorName_" + id).hide();
    $("#errorURL_" + id).hide();
    $("#errorIsTarget_" + id).hide();
    var type = $("#Type_" + id).val();
    var parent = $("#Parent_" + id).val();
    var name = $("#Name_" + id).val();
    var url = $("#URL_" + id).val();
    var istarget = $("#IsTarget_" + id).prop("checked");
    if (type != "" && name != "" && url != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/Parameter/AddCategory',
            data: JSON.stringify({ MappingId: id, TypeId: type, ParentId: parent, Name: name, URL: url, IsTarget: istarget }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    ddlcategorylist = response.Catlist;
                    ddlactionlist = response.Actlist;
                    tblCategoryAction.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (type == "")
            $("#errorType_" + id).show();
        if (name == "")
            $("#errorName_" + id).show();
        if (url == "")
            $("#errorURL_" + id).show();
    }
}

$('#tblCategoryAction').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblCategoryAction.row(tr);
    var data = row.data();
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: "/DES/Parameter/ParentList",
        data: { TypeId: data.TypeId },
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (dataList) {
            mApp.unblockPage();
            var html = '<td><select style="width:120px;" id="Type_' + data.MappingId + '" name="Type_' + data.MappingId + '" value="' + data.TypeName + '" class= "form-control">';
            for (let i = 0; i < MenuTypeList.length; i++) {
                if (MenuTypeList[i].Value == data.TypeId)
                    html += "<option selected='selected' value='" + MenuTypeList[i].Value + "'>" + MenuTypeList[i].Text + "</option>";
                else
                    html += "<option value='" + MenuTypeList[i].Value + "'>" + MenuTypeList[i].Text + "</option>";
            }
            html += '</select><label class="text-danger" id="errorType" style="display:none" > Type is required </label></td> '

                + '<td><select style="width:120px;" id="Parent_' + data.MappingId + '" name="Parent_' + data.MappingId + '" value="' + data.Name + '" class= "form-control"><option value="">Select</option>';
            for (let i = 0; i < dataList.length; i++) {
                if (dataList[i].MappingId == data.ParentId)
                    html += "<option selected='selected' value='" + dataList[i].MappingId + "'>" + dataList[i].Name + "</option>";
                else
                    html += "<option value='" + dataList[i].MappingId + "'>" + dataList[i].Name + "</option>";
            }
            html += '</select><label class="text-danger" id="errorParent_" style="display:none" > ParentName is required </label></td> '

                + '<td><input type="text" style="width:220px;" maxlength="50" class="form-control" id="Name_' + data.MappingId + '" name="Name_' + data.MappingId + '" placeholder="Name" value="' + data.Name + '" /> <label class="text-danger" id="errorName_' + data.MappingId + '" style="display:none" > Name is required </label></td> '
                + '<td><input type="text" style="width:220px;" maxlength="500" class="form-control" id="URL_' + data.MappingId + '" name="URL_' + data.MappingId + '" placeholder="URL" value="' + data.URL + '" /><label class="text-danger" id="errorURL_' + data.MappingId + '" style="display:none" > URL is required </label></td> '
                + '<td><label class="m-checkbox m-checkbox--solid m-checkbox--success"><input ' + (data.IsTarget ? "Checked" : "") + ' type="checkbox" id="IsTarget_' + data.MappingId + '" name="IsTarget_' + data.MappingId + '" value="' + data.IsTarget + '"><span></span></label></td> '
                + '<td></td><td></td><td></td><td></td>'
                + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddCategoryAction(' + data.MappingId + ')" title="Update"><i class="la la-check text-info"></i></a>'
                + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblCategoryActionClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>'
            $(tr).html(html);
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
    $("#Type_" + data.MappingId).focus();
});

//Add and Edit time to call change Event
$("#tblCategoryAction").on("change", "select[Id^='Type_']", function () {
    var tr = $(this).closest('tr');
    var row = tblCategoryAction.row(tr);
    var data = row.data();
    var iid = 0;
    if (data != null && data != undefined) {
        iid = data.MappingId;
    }
    var type1 = $(this).val();
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: "/DES/Parameter/ParentList",
        data: { TypeId: type1 },
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (data1) {
            mApp.unblockPage();
            $("#Parent_" + iid).html("");
            $("#Parent_" + iid).append($('<option></option>').text('Select'));
            $.each(data1, function (key, entry) {
                $("#Parent_" + iid).append($('<option></option>').attr('value', entry.MappingId).text(entry.Name));
            });
        },
        error: function () {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "error");
        }
    });
});

function tblCategoryActionClear() {
    tblCategoryAction.ajax.reload();
}

$('#tblCategoryAction').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblCategoryAction.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,        
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Parameter/DeleteCategory',
                    data: { Id: data.MappingId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblCategoryAction.data().count() == 1) {
                                tblCategoryAction.ajax.reload();
                            }
                            else {
                                tblCategoryAction.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});




