﻿var tblBOMList;
$(document).ready(function () {

    setTimeout(function () {
        BOMList();
    }, 1000)

});

function BOMList() {
    tblBOMList = $('#tblBOMList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Part/BOMListByItemId",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "fixedColumns": {
            leftColumns: 5
        },
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[2, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 },
            { "orderable": false, "targets": 9 },
            { "orderable": false, "targets": 10 },
            { "orderable": false, "targets": 11 },
            { "orderable": false, "targets": 12 },
            { "orderable": false, "targets": 13 },
            { "orderable": false, "targets": 14 },
            { "orderable": false, "targets": 15 },
            { "orderable": false, "targets": 16 },
            { "orderable": false, "targets": 17 },
            { "orderable": false, "targets": 18 },
            { "orderable": false, "targets": 19 },
            { "orderable": false, "targets": 20 },
            { "orderable": false, "targets": 21 },
            { "orderable": false, "targets": 22 },
            { "orderable": false, "targets": 23 },
            { "orderable": false, "targets": 24 },
            { "orderable": false, "targets": 25 },
            { "orderable": false, "targets": 26 },
            { "orderable": false, "targets": 27 },
            { "orderable": false, "targets": 28 },
            { "orderable": false, "targets": 29 },
            { "orderable": false, "targets": 30 },
            { "orderable": false, "targets": 31 },
            { "orderable": false, "targets": 32 },
            { "orderable": false, "targets": 33 },
            { "orderable": false, "targets": 34 },
            { "orderable": false, "targets": 35 },
            { "orderable": false, "targets": 36 },
            { "orderable": false, "targets": 37 },
            { "orderable": false, "targets": 38 },
            { "orderable": false, "targets": 39 },
            { "orderable": false, "targets": 40 },
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ItemId", value: $("#ItemId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "Actions", "className": "nowrap", render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.IsFromLN != true && row.IsFromFKMS != true)
                        srt += '\n <a  href="/DES/Part/Edit?q=' + row.BString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditPart" title="Edit">\n<i class="la la-edit"></i>\n</a>';
                    //srt += '\n <a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" Onclick="swalPopup(this)">\n<i class="la la-trash"></i>\n</a>';
                    srt += '\n <a href="/DES/Part/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                    srt += '';
                    return srt;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    var state = "";
                    if (row.Type.toLowerCase() == "part") {
                        state = "brand";
                    }
                    else if (row.Type.toLowerCase() == "plate part") {
                        state = "info";
                    }
                    else if (row.Type.toLowerCase() == "clad part") {
                        state = "accent";
                    }
                    else if (row.Type.toLowerCase() == "jigfix") {
                        state = "success";
                    }
                    return '<span class="m--font-bold m--font-' + state + '">' + row.Type + "</span>"
                }
            },
            {
                "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {

                    var name = "<a  href='/DES/Part/PowerView?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                    return name;
                }
            },
            {
                "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                    return row.DRGNoDocumentNo;
                }
            },
            {
                "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                    return row.PerentItemKey;
                }
            },
            {
                "sTitle": "State", "className": "", render: function (data, type, row, meta) {
                    return row.ItemState;
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    return ('R' + row.RevNo);
                }
            },
            {
                "sTitle": "ARM Set", "className": "", render: function (data, type, row, meta) {
                    return row.ARMSet;
                }
            },
            {
                "sTitle": "ARMRev", "className": "", render: function (data, type, row, meta) {
                    return row.ARMRev;
                }
            },
            {
                "sTitle": "Order Policy", "className": "", render: function (data, type, row, meta) {
                    return row.OrderPolicy;
                }
            },
            {
                "sTitle": "Product Form", "className": "", render: function (data, type, row, meta) {
                    return row.ProductForm;
                }
            },
            {
                "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                    return row.Material;
                }
            },
            {
                "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                    return row.ItemGroup;
                }
            },
            {
                "sTitle": "Item Name", "className": "", render: function (data, type, row, meta) {
                    return row.ItemName;
                }
            },
            {
                "sTitle": "String2", "className": "", render: function (data, type, row, meta) {
                    return row.String2;
                }
            },
            {
                "sTitle": "String3", "className": "", render: function (data, type, row, meta) {
                    return row.String3;
                }
            },
            {
                "sTitle": "String4", "className": "", render: function (data, type, row, meta) {
                    return row.String4;
                }
            },
            {
                "sTitle": "Procurement Drg No", "className": "", render: function (data, type, row, meta) {
                    return row.ProcurementDrgDocumentNo;
                }
            },

            {
                "sTitle": "Item Weight", "className": "", render: function (data, type, row, meta) {
                    return row.ItemWeight;
                }
            },
            {
                "sTitle": "BOM Weight", "className": "", render: function (data, type, row, meta) {
                    return row.BOMWeight;
                }
            },
            {
                "sTitle": "Thickness", "className": "", render: function (data, type, row, meta) {
                    return row.Thickness;
                }
            },
            {
                "sTitle": "Clad Plate Part Thickness1", "className": "", render: function (data, type, row, meta) {
                    return row.CladPlatePartThickness1;
                }
            },
            {
                "sTitle": "IsDoubleClad", "className": "", render: function (data, type, row, meta) {
                    return row.IsDoubleClad;
                }
            },
            {
                "sTitle": "Clad Plate Part Thickness2", "className": "", render: function (data, type, row, meta) {
                    return row.CladPlatePartThickness2;
                }
            },
            {
                "sTitle": "Clad Specific Gravity2", "className": "", render: function (data, type, row, meta) {
                    return row.CladSpecificGravity2;
                }
            },
            {
                "sTitle": "CRS", "className": "", render: function (data, type, row, meta) {
                    return row.CRSYN;
                }
            },
            {
                "sTitle": "Job Qty", "className": "", render: function (data, type, row, meta) {
                    return row.JobQty;
                }
            },
            {
                "sTitle": "Commissioning Spare Qty", "className": "", render: function (data, type, row, meta) {
                    return row.CommissioningSpareQty;
                }
            },
            {
                "sTitle": "Mandatory Spare Qty", "className": "", render: function (data, type, row, meta) {
                    return row.MandatorySpareQty;
                }
            },
            {
                "sTitle": "Extra Qty", "className": "", render: function (data, type, row, meta) {
                    return row.ExtraQty;
                }
            },
            {
                "sTitle": "Remarks", "className": "", render: function (data, type, row, meta) {
                    return row.Remarks;
                }
            },
            {
                "sTitle": "Quantity", "className": "", render: function (data, type, row, meta) {
                    return row.Quantity;
                }
            },
            {
                "sTitle": "Length", "className": "", render: function (data, type, row, meta) {
                    return row.Length;
                }
            },
            {
                "sTitle": "Width", "className": "", render: function (data, type, row, meta) {
                    return row.Width;
                }
            },
            {
                "sTitle": "NumberOfPieces", "className": "", render: function (data, type, row, meta) {
                    return row.NumberOfPieces;
                }
            },
            {
                "sTitle": "BomReportSize", "className": "", render: function (data, type, row, meta) {
                    return row.BomReportSize;
                }
            },
            {
                "sTitle": "DrawingBomSize", "className": "", render: function (data, type, row, meta) {
                    return row.DrawingBomSize;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblBOMList.columns.adjust();
        }

    });
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblBOMList != null)
        tblBOMList.columns.adjust();
});