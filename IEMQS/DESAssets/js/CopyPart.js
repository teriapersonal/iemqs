﻿$(document).ready(function () {
    $("#Project").select2();
    $("#ManufactureItem").select2();
    $("#ContractProject").select2();
    $("#ManufactureItemProjectWise").select2();
    $("#Project").change();
});

$("#Project").change(function () {
    var Project = $('#Project').val();
    if (Project !="") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/Part/GetManufacturingItembyProject",
            data: { "Project": Project },
            datatype: "json",
            async: true,
            success: function (data) {

                mApp.unblockPage();
                $("#ManufactureItem").html("");
                $('#ManufactureItem').append("<option value=''>Select</option>");
                $.each(data, function (key, entry) {
                    $("#ManufactureItem").append($('<option></option>').attr('value', entry.ItemId).text(entry.ItemKey));
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
                $("#ManufactureItem").html("");
            }
        });
    }
    else {
        $("#ManufactureItem").html("");
        $("#ManufactureItem").append($('<option></option>').text('Select'));
    }
});

function AddCopyPart(Id) {
    var rowCount = $('#tbl_CopyPart tr').length;
    mApp.blockPage();
    $("#del_" + Id).show();
    $("#Add_" + Id).hide();
    $.ajax({
        url: '/DES/Part/AddCopyPart',
        type: 'GET',
        traditional: true,
        data: { TNo: rowCount },
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#tbl_CopyPart").append(result);
            CalculateCopyPart();
            $("#del_0").show();
            $("#Add_" + Id).hide();
            //$("#Add_" + Id).hide();
            //$("#del_" + (parseInt(Id) + 1)).hide();
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
    //}
    // make condition to null data // mukvani 6

}

function CalculateCopyPart() {
    var rowCount = $('#tbl_CopyPart tr').length;
    var step = 1;
    for (var i = 0; i < rowCount; i++) {
        //if ($("#CopyPartGroupList_" + i + " __IsAdd).val() !="true"){

        if ($("#CopyPartGroupList_" + i + "__IsDelete").val() != "true") {
            //$("#CopyPartGroupList_" + i + "__OrderNo").val(step);
            step = step + 1;
        }
    }
}

function RemoveCopyPart($this) {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var tid = $($this).attr("delete-id");
                $("#CopyPartGroupList_" + tid + "__IsDelete").val("true");
                $("#tr_" + tid).hide();
                var rowCount = $('#tbl_CopyPart tr:visible').length;
                if (!(rowCount > 0)) {
                    AddCopyPart(Id);
                }
                CalculateCopyPart();
            }
        });
}

CopyPartBegin = function (data) {
    mApp.blockPage();
    var flag = true;
    var step = [];
    var rowCount = $('#tbl_CopyPart tr').length;
    for (var i = 0; i < rowCount; i++) {
        $("span[data-valmsg-for='CopyPartGroupList[" + i + "].ManufactureItemId']").html("");
        if ($("#CopyPartGroupList_" + i + "__IsDelete").val() != "true") {

            if (i != 0 && i == (rowCount - 1)) {
                if ($("#CopyPartGroupList_" + i + "__ManufactureItemId").val() != "") {
                    if (step.indexOf($("#CopyPartGroupList_" + i + "__ManufactureItemId").val()) >= 0) {
                        flag = false;
                        $("span[data-valmsg-for='CopyPartGroupList[" + i + "].ManufactureItemId']").html("Manufacture Item already exist");
                    }


                    step.push($("#CopyPartGroupList_" + i + "__ManufactureItemId").val());

                }

            } else {
                if (step.indexOf($("#CopyPartGroupList_" + i + "__ManufactureItemId").val()) >= 0) {
                    flag = false;
                    $("span[data-valmsg-for='CopyPartGroupList[" + i + "].ManufactureItemId']").html("Manufacture Item already exist");
                }


                step.push($("#CopyPartGroupList_" + i + "__ManufactureItemId").val());

                if ($("#CopyPartGroupList_" + i + "__ManufactureItemId").val() == "") {
                    $("span[data-valmsg-for='CopyPartGroupList[" + i + "].ManufactureItemId']").html("Manufacture Item is required");
                    flag = false;
                }
            }
        }

    }
    if (flag == false) {
        mApp.unblockPage();
    }
    return flag;
};

CopyPartSuccess = function (data) {
    mApp.unblockPage();
    if (data != null) {
        var msg = "";
        for (var i = 0; i < data.Msg.length; i++) {
            msg += "Destination Project " + (i + 1)+"<br/>";
            for (var j = 0; j < data.Msg[i].length; j++) {
                if (data.Msg[i][j].ErrorMsg != "" && data.Msg[i][j].ErrorMsg != null) {
                    msg += data.Msg[i][j].ErrorMsg;
                }
            }
        }
        if (msg != "" && msg != null) {
            swal(msg);
        } else {
            swal("Copy part successfully");
        }
    } else {
        toastr.error("something went wrong try again later", "Error");
    }
};



