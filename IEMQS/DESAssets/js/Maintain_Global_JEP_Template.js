﻿var tblGlobalJEPTemplate;
$(document).ready(function () {

    tblGlobalJEPTemplate = $('#tblGlobalJEPTemplate').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/JEPTemplate/GetGlobalJEPTemplateList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },

        "aoColumns": [
            //{
            //    "sTitle": "", "className": "", render: function (data, type, row, meta) {
            //        //var name = "<strong style='text-transform: capitalize;'>" + row.BU + "</strong>";
            //        return 0;
            //    },
            //},
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    if ((row.IsJEPTemplateCheckIn == true)) {
                        if (row.JEPTemplateCheckInBy == getCurrentUser) {
                            srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                            srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        }
                        else {
                            srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.JEPTemplateCheckInByName + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                        }
                    }
                    else {
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    }
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnReport" title="Export to Excel"><i class="la la-file-excel-o text-danger"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "Template Name ", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.TemplateName + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "BU / PBU ", "className": "", render: function (data, type, row, meta) {
                    var Buname = "<strong style='text-transform: capitalize;'>" + row.BU + "-" + row.PBU + "</strong>";
                    return Buname;
                }
            },
            {
                "sTitle": "Product Type ", "className": "", render: function (data, type, row, meta) {
                    var PD = row.Product + "-" + row.Description;
                    return PD;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblGlobalJEPTemplate.columns.adjust();
        }

    });

});

$(document).ready(function () {
    $("#BUDropDownList").select2();
    $("#ProductDropDownList").select2();
    $("#ddlBUCopyTemplate").select2();
    $("#ddlProductCopyTemplate").select2();
    $("#ddlJEPTemplateId").select2();
});

$('#tblGlobalJEPTemplate').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblGlobalJEPTemplate.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/JEPTemplate/DeleteGlobalJEP',
                    data: { Id: data.JEPTemplateId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblGlobalJEPTemplate.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#BUDropDownList").change(function () {
    $('#displayproductid').show();
    var valBUId = $('#BUDropDownList').val();

    if (valBUId != '') {
        $('#BUId').val(valBUId);
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/JEPTemplate/ProductList",
            data: { "BuId": parseInt(valBUId) },
            datatype: "json",
            async: true,
            success: function (data) {
                mApp.unblockPage();
                $("#ProductDropDownList").html("");
                $("#ddlJEPTemplateId").html("");
                $.each(data, function (key, entry) {
                    $("#ProductDropDownList").append($('<option></option>').attr('value', entry.ProductId).text(entry.Product));
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
});

$("#ddlBUCopyTemplate").change(function () {
    //$('#displayproductid').show();

    var valBUId = $('#ddlBUCopyTemplate').val();

    if (valBUId != null) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/JEPTemplate/ProductList",
            data: { "BuId": parseInt(valBUId) },
            datatype: "json",
            async: true,
            success: function (data) {
                mApp.unblockPage();
                $("#ddlProductCopyTemplate").html("");
                $("#ddlJEPTemplateId").html("");
                $.each(data, function (key, entry) {
                    $("#ddlProductCopyTemplate").append($('<option></option>').attr('value', entry.ProductId).text(entry.Product));
                });
                $("#ddlProductCopyTemplate").trigger("change");
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
                $("#ddlProductCopyTemplate").html("");
                $("#ddlJEPTemplateId").html("");
            }
        });
    }
});

$("#ddlProductCopyTemplate").change(function () {
    var valBUId = $('#ddlProductCopyTemplate').val();

    if (valBUId != null) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/JEPTemplate/TemplateList",
            data: { "BuId": parseInt($('#ddlBUCopyTemplate').val()), "ProductId": parseInt(valBUId) },
            datatype: "json",
            async: true,
            success: function (data) {
                mApp.unblockPage();
                $("#ddlJEPTemplateId").html("");
                $.each(data, function (key, entry) {
                    $("#ddlJEPTemplateId").append($('<option></option>').attr('value', entry.JEPTemplateId).text(entry.TemplateName));
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
                $("#ddlJEPTemplateId").html("");
            }
        });
    }
});

$("#btnCopy").click(function () {
    if ($('#ddlJEPTemplateId > option').length > 0) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/JEPTemplate/CopyDocumentList",
            data: { "Id": parseInt($('#ddlJEPTemplateId').val()) },
            datatype: "json",
            async: true,
            success: function (data) {
                mApp.unblockPage();
                $("#tbl_JEPDoc").html("");
                $("#tbl_JEPDoc").append(data);
                CalculateDocumentList();
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select atleast one template.", "Info");
    }
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblGlobalJEPTemplate != null)
        tblGlobalJEPTemplate.columns.adjust();
});


$('#tblGlobalJEPTemplate').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblGlobalJEPTemplate.row(tr);
    var data = row.data();
    var JEPTemplateMapp = {
        JEPTemplateId: data.JEPTemplateId
    };
    swal({
        title: "Do you want to release Global JEP Template?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            mApp.blockPage();
            $.ajax({
                url: "/DES/JEPTemplate/UpdateJEPTemplateCheckOut",
                data: JSON.stringify(JEPTemplateMapp),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        if (response.Status) {
                            toastr.info(response.Msg, "Info");
                            tblGlobalJEPTemplate.ajax.reload();
                            mApp.unblockPage();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }

            });
        }
    });
});


$('#tblGlobalJEPTemplate').on('click', 'td .btnCheckIn', function () {
    var tr = $(this).closest('tr');
    var row = tblGlobalJEPTemplate.row(tr);
    var data = row.data();
    if ((data.IsJEPTemplateCheckIn == true)) {
        if (data.JEPTemplateCheckInBy != getCurrentUser) {
            toastr.info("Global JEP Template is already in use by " + data.JEPTemplateCheckInByName, "Info");
        }
    }
});


$('#tblGlobalJEPTemplate').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblGlobalJEPTemplate.row(tr);
    var data = row.data();
    if ((data.IsJEPTemplateCheckIn == true)) {
        if (data.JEPTemplateCheckInBy == getCurrentUser) {
            window.location = "/DES/JEPTemplate/EditGlobal?q=" + data.QString;
        }
        else {
            toastr.info("Global JEP Template is already in use by " + data.JEPTemplateCheckInByName, "Info");
        }
    }
    else {
        window.location = "/DES/JEPTemplate/EditGlobal?q=" + data.QString;
    }
});

$('#tblGlobalJEPTemplate').on('click', 'td .btnReport', function () {
    var tr = $(this).closest('tr');
    var row = tblGlobalJEPTemplate.row(tr);
    var data = row.data();

    var dat = [];
    dat.push({ Param: "passJEPTemplateId", Value: data.JEPTemplateId });
    ShowReport(dat, "/DES/Maintain Global JEP Template", false, true, 'excel', data.TemplateName + "_" + data.BU + "-" + data.PBU);
});