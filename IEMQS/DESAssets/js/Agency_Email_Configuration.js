﻿var tblAgencyEmail;
$(document).ready(function () {
    mApp.blockPage();
    tblAgencyEmail = $('#tblAgencyEmail').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/AgencyEmail/GetAgencyEmailList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },

        "aoColumns": [           
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {

                    var srt = "";
                    srt += '<a href="/DES/AgencyEmail/EditAgencyEmail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                    ///DES/AgencyEmail / EditAgencyEmail ? Id = ' + row.TransmittalEmailId + '
                }
            },
            {
                "sTitle": "Agency Name ", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.AgencyName + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "Agency Description ", "className": "", render: function (data, type, row, meta) {
                    return row.AgencyDescription;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Agency Name</li><li class="list-inline-item">Agency Description </li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);
        },

        "fnRowCallback": function () {
            tblAgencyEmail.columns.adjust();
        }

    });
    mApp.unblockPage();
});


$('#tblAgencyEmail').on('click', 'td .btnDelete', function () {  
    var tr = $(this).closest('tr');
    var row = tblAgencyEmail.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/AgencyEmail/DeleteAgencyEmail',
                    data: { Id: data.TransmittalEmailId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblAgencyEmail.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblAgencyEmail != null)
        tblAgencyEmail.columns.adjust();
});