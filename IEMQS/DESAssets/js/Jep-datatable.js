var DatatablesBasicBasic = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
            paging: false,
            "ordering": false,
            responsive: !0,
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            
            language: {
                lengthMenu: "Display _MENU_"
            },
       
           
            columnDefs: [
                {
                targets: 0,
                width:'30px',
                },
                {
                targets: 1,
                width:'100px',
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                       <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n                          <i class="la la-edit text-info"></i>\n                        </a> \n                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\n                          <i class="la la-trash text-danger"></i>\n                        </a>'
                }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic.init()
});