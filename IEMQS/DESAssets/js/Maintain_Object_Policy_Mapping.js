﻿var tblObjectPolicyMapping;
$(document).ready(function () {

    tblObjectPolicyMapping = $('#tblObjectPolicyMapping').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Parameter/GetObjectPolicyMappingList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'asc']],
        "columnDefs": [
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [           
            {
                "sTitle": "Object Name <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var ObjectName = "<strong style='text-transform: capitalize;'>" + row.ObjectName + " (" + row.BU + " - " + row.PBU + ")" + "</strong>";                    
                    return ObjectName;               
                }
            },
            {
                "sTitle": "Policy Name <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var PolicyName = "<strong style='text-transform: capitalize;'>" + row.PolicyName + "</strong>";
                    return PolicyName;
                },
            },            
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddObjectPolicyRow();
        },

        "fnRowCallback": function () {
            AddObjectPolicyRow();
        }
    });
});

function AddObjectPolicyRow() {
    setTimeout(function () {
        $("#Object_0").focus();
        if ($("#Object_0").length === 0)
            var html = '<tr class="v-top">'

            + '<td><select id="Object_0" class= "form-control"><option value="">Select</option>';
        for (let i = 0; i < ddlobjectlist.length; i++) {
            html += "<option value='" + ddlobjectlist[i].ObjectId + "'>" + ddlobjectlist[i].ObjectName + " (" + ddlobjectlist[i].BU + ") </option>";
        }
        html += '</select><label class="text-danger" id="errorObject_0" style="display:none" > Object is required </label></td> '

        + '<td><select id="Policy_0" class= "form-control"><option value="">Select</option>';
        for (let i = 0; i < ddlpolicylist.length; i++) {
            html += "<option value='" + ddlpolicylist[i].PolicyId + "'>" + ddlpolicylist[i].Name + "</option>";
        }
        html += '</select><label class="text-danger" id="errorPolicy_0" style="display:none" > Policy is required </label></td> '

        + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddObjectPolicyMapping(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>'
        $('#tblObjectPolicyMapping tbody').prepend(html);
    }, 1000);
}

function AddObjectPolicyMapping(id) {
    $("#errorObject_" + id).hide();
    $("#errorPolicy_" + id).hide();    
    var object = $("#Object_" + id).val();
    var policy = $("#Policy_" + id).val();    
    if (object != "" && policy != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/Parameter/AddObjectPolicyMapping',
            data: JSON.stringify({ MappingId: id, ObjectId: object, PolicyId: policy }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    ddlobjectlist = response.Objlist;
                    ddlpolicylist = response.Policylist;
                    tblObjectPolicyMapping.ajax.reload();
                } else {
                    toastr.error(response.Msg, "Error");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (object == "")
            $("#errorObject_" + id).show();
        if (policy == "")
            $("#errorPolicy_" + id).show();        
    }

}

$('#tblObjectPolicyMapping').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectPolicyMapping.row(tr);
    var data = row.data();
    var html = '<td><select id="Object_' + data.MappingId + '" name="Object_' + data.MappingId + '" value="' + data.ObjectName + '(" + ddlobjectlist[i].BU + ")' + '" class= "form-control">';
        for (let i = 0; i < ddlobjectlist.length; i++) {
            if (ddlobjectlist[i].ObjectId == data.ObjectId)
                html += "<option selected='selected' value='" + ddlobjectlist[i].ObjectId + "'>" + ddlobjectlist[i].ObjectName + " (" + ddlobjectlist[i].BU + ") </option>";                
            else
                html += "<option value='" + ddlobjectlist[i].ObjectId + "'>" + ddlobjectlist[i].ObjectName + " (" + ddlobjectlist[i].BU + ") </option>";
        }
    html += '</select><label class="text-danger" id="errorObject" style="display:none" > Object is required </label></td> '

        + '<td><select id="Policy_' + data.MappingId + '" name="Policy_' + data.MappingId + '" value="' + data.Name + '" class= "form-control">';
        for (let i = 0; i < ddlpolicylist.length; i++) {
            if (ddlpolicylist[i].PolicyId == data.PolicyId)
                html += "<option selected='selected' value='" + ddlpolicylist[i].PolicyId + "'>" + ddlpolicylist[i].Name + "</option>";               
            else
                html += "<option value='" + ddlpolicylist[i].PolicyId + "'>" + ddlpolicylist[i].Name + "</option>";
        }
        html += '</select><label class="text-danger" id="errorPolicy" style="display:none" > Policy is required </label></td> '
        
        + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddObjectPolicyMapping(' + data.MappingId + ')" title="Update"><i class="la la-check text-info"></i></a>'
            + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblObjectPolicyMappingClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>'
    $(tr).html(html);
    $("#Object_" + data.MappingId).focus();
});

function tblObjectPolicyMappingClear() {
    tblObjectPolicyMapping.ajax.reload();
}

$('#tblObjectPolicyMapping').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectPolicyMapping.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Parameter/DeleteObjectPolicyMapping',
                    data: { Id: data.MappingId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblObjectPolicyMapping.data().count() == 1) {
                                tblObjectPolicyMapping.ajax.reload();
                            }
                            else {
                                tblObjectPolicyMapping.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

