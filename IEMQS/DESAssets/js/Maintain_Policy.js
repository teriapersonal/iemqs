﻿var tblPolicy;
var IsValidInput = false;

$(document).ready(function () {
    tblPolicy = $('#tblPolicy').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Parameter/GetPolicyList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'asc']],
        "columnDefs": [
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Policy Name", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.Name + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "Policy Description", "className": "", render: function (data, type, row, meta) {
                    var name = row.Description;
                    return name;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }

    });


});


$('#tblPolicy').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblPolicy.row(tr);
    var data = row.data();
    AddPolicy(data.PolicyId);
});

$('#tblPolicy').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblPolicy.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary", //primary  secondary
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Parameter/DeletePolicy',
                    data: { Id: data.PolicyId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblPolicy.data().count() == 1) {
                                tblPolicy.ajax.reload();
                            }
                            else {
                                tblPolicy.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

function AddPolicy(id) {
    mApp.blockPage();
    $.ajax({
        url: '/DES/Parameter/AddPolicy',
        type: 'GET',
        traditional: true,
        data: { Id: id },
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyPoliy").html(result);
            $("#m_modal_Policy").modal("show");            
            CalculatePolicyStep();
            
            setTimeout(function () {
                $("#Name").focus();  
                //ddlpolicylist = response.Policylist;
                //$("#PolicyStepList_0__StepName").focus();
            }, 1000);        
           
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
}

function AddPolicyStep() {
    var rowCount = $('#tbl_PolicyStep tr').length;
    mApp.blockPage();
    $.ajax({
        url: '/DES/Parameter/AddPolicyStep',
        type: 'GET',
        traditional: true,
        data: { TNo: rowCount },
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#tbl_PolicyStep").append(result);
            CalculatePolicyStep();
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });

}

function RemovePolicyStep($this) {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var tid = $($this).attr("delete-id");
                $("#PolicyStepList_" + tid + "__IsDelete").val("true");
                $("#tr_" + tid).hide();
                var rowCount = $('#tbl_PolicyStep tr:visible').length;
                if (!(rowCount > 0)) {
                    AddPolicyStep();
                }
                CalculatePolicyStep();
            }
        });
}

function CalculatePolicyStep() {
    var rowCount = $('#tbl_PolicyStep tr').length;
    var step = 1;
    for (var i = 0; i < rowCount; i++) {
        if ($("#PolicyStepList_" + i + "__IsDelete").val() != "true") {
            $("#PolicyStepList_" + i + "__OrderNo").val(step);
            step = step + 1;
        }
    }
}

PolicyBegin = function (data) {
    var flag = true;
    var step = [];
    var rowCount = $('#tbl_PolicyStep tr').length;
    for (var i = 0; i < rowCount; i++) {
        $("span[data-valmsg-for='PolicyStepList[" + i + "].StepName']").html("");
        if ($("#PolicyStepList_" + i + "__IsDelete").val() != "true") {
            if (step.indexOf(String($("#PolicyStepList_" + i + "__StepName").val()).toLowerCase()) >= 0) {
                flag = false;
                $("span[data-valmsg-for='PolicyStepList[" + i + "].StepName']").html("Step name already exist");
            }
            else {
                step.push(String($("#PolicyStepList_" + i + "__StepName").val()).toLowerCase())
            }
            if ($("#PolicyStepList_" + i + "__StepName").val() == "") {
                $("span[data-valmsg-for='PolicyStepList[" + i + "].StepName']").html("Step name is required");
                flag = false;
            }
        }
    }
    return flag;
};

PolicySuccess = function (data) {
    if (data.Status) {
        toastr.success(data.Msg, "Success");
        tblPolicy.ajax.reload();
        $("#m_modal_Policy").modal("hide");
        ddlpolicylist = data.Policylist;
    } else {
        toastr.info(data.Msg, "Info");
    }
};






