﻿var tblAgencyWiseDoc;
//var selectedAgencyDocIDs = [];
var DocPath = 'C:/Users/bhavin.patel/Downloads/TestDownload.pdf';

$(document).ready(function () {
    //DownloadDoc();
    tblAgencyWiseDoc = $('#tblAgencyWiseDoc').DataTable({
        "searching": true,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Transmittal/GetAgencywiseDocs",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
          
        ],
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "AgencyID", value: $("#AgencyId").val() });
            data.push({ name: "TransmittalId", value: $("#TransmittalId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderAgencyDoc' onclick='SelectAllAgencyDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Status == "Completed") {
                        srt += '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">';
                        var IDIndex = selectedAgencyDocIDs.indexOf(row.AgencyDocListId);
                        if (IDIndex == -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.AgencyDocListId + '"  data-id="' + row.AgencyDocListId + '" onclick="AddToAgencyDocList(this)">';
                        }
                        else {
                            //selectedAgencyDocIDs.push(row.GenDocID);
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.AgencyDocListId + '"  data-id="' + row.AgencyDocListId + '" onclick="AddToAgencyDocList(this)" checked>';
                        }
                        srt += "<span></span></label>";
                    }
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                    return row.DocNo;
                }
            },
            {
                "sTitle": "Rev", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.RevisionNo + "</strong>";
                    return "R" + row.RevisionNo;
                }
            },
            {
                "sTitle": "Description", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.DocDescription;
                }
            },
            {
                "sTitle": "Planned Submit Date", "orderable": true, "className": "", render: function (data, type, row, meta) {
                    return row.PlannedSubmitDate;
                }
            },
            {
                "sTitle": "Status", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.Status != null) {
                        strHtml = row.Status
                    }
                    else {
                        strHtml = "Not Created";
                    }
                    return strHtml;
                }
            }
        ],

        "initComplete": function () {

            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Doc no</li>  <li class= "list-inline-item" >Planned Submit Date</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);

            setTimeout(function () {
                tblAgencyWiseDoc.columns.adjust();
            }, 500);

        },

        "fnRowCallback": function () {
            tblAgencyWiseDoc.columns.adjust();
        }

    });
});



function AddToAgencyDocList($this) {
    //var data = tblAgencyWiseDoc.rows().data();
    var data = tblAgencyWiseDoc.rows().data()[0]["AgencyDocIDs"];
    var DocIdArray = data.split(',');
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    if (select == true) {
        selectedAgencyDocIDs.push(id);
        selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
        if (DocIdArray.length == selectedAgencyDocIDs.length) {
            $("#" + chbID).prop("checked", true);
        }
    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedAgencyDocIDs.indexOf(id);
        if (index > -1) {
            selectedAgencyDocIDs.splice(index, 1);
        }
        selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
    }
}

function SelectAllAgencyDoc() {
    //var data = tblAgencyWiseDoc.rows().data();
    var data = tblAgencyWiseDoc.rows().data()[0]["AgencyDocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderAgencyDoc").prop("checked");
    if (select == true) {
        //data.each(function (value, index) {
        //    if (data[index].Status == "Completed") {
        //        $("#IsSelect_" + data[index].AgencyDocListId).prop("checked", true);
        //        selectedAgencyDocIDs.push(data[index].AgencyDocListId);
        //    }
        //});

        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedAgencyDocIDs.push(item);
        });
    }
    else {
        //data.each(function (value, index) {
        //    if (data[index].Status == "Completed") {
        //        $("#IsSelect_" + data[index].AgencyDocListId).prop("checked", false);
        //        var index = selectedAgencyDocIDs.indexOf(data[index].AgencyDocListId);
        //        if (index > -1) {
        //            selectedAgencyDocIDs.splice(index, 1);
        //        }
        //    }
        //});
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            //if (data[index].Status == "Completed") 
            {
                $("#IsSelect_" + item).prop("checked", false);
                var index = selectedAgencyDocIDs.indexOf(item);
                if (index > -1) {
                    selectedAgencyDocIDs.splice(index, 1);
                }
            }
        });
    }
    selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
}

$("#btnGenerate").on("click", function () {
    AddTransmittal("Created");
});

$("#btnSaveasDraft").on("click", function () {
    AddTransmittal("Draft");
});

function AddTransmittal(type) {
    mApp.blockPage();
    if (selectedAgencyDocIDs.length > 0) {
        var FinalDoclist = Array.from(new Set(selectedAgencyDocIDs));
        var DocIDs = new Array();

        for (var id in FinalDoclist) {
            DocIDs.push({ AgencyDocListId: FinalDoclist[id] });
        }

        var model = {};
        model.AgencyId = $("#AgencyId").val();
        model.TransmittalId = $("#TransmittalId").val();
        model.Status = type;
        model.TransmittalDocumentList = DocIDs;

        $.ajax({
            type: "POST",
            url: '/DES/Transmittal/AddTransmittal',
            data: JSON.stringify({ model: model }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                if (response.Status) {
                    mApp.unblockPage();
                    if (type == "Created") {

                        DownlaodReport(response);
                        //  DownloadDoc(response);
                        toastr.success(response.Msg, "Success");
                        //tblAgencyWiseDoc.ajax.reload();
                        selectedAgencyDocIDs = [];
                        window.location.href = "/DES/Transmittal";
                    }
                    else {
                        toastr.success(response.Msg, "Success");
                        //tblAgencyWiseDoc.ajax.reload();
                        selectedAgencyDocIDs = [];
                        window.location.href = "/DES/Transmittal";
                    }

                } else {
                    mApp.unblockPage();
                    toastr.error(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Info");
            }
        })
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select atleast one document!", "Info");
        //return;
    }
}

function DownlaodReport(response) {
    var dat = [];
    dat.push({ Param: "BU", Value: $('#CurrentBU').val() });
    dat.push({ Param: "ProjectNo", Value: $('#Project').val() });
    dat.push({ Param: "AgencyId", Value: $("#AgencyId").val() });
    dat.push({ Param: "TransmittalNo", Value: response.TransmittalNo });
    ShowReport(dat, "/DES/Transmittal - BU Wise", false, true, 'pdf', response.TransmittalNo, response);
}


function ShowReport(ParameterData, ReportPath, ShowAttachment, downloadFile, format, downloadName, response) {
    var newDocPathSplit = response.DocPathList[0].split('/');
    var newDocRemoveLast = newDocPathSplit.pop();
    var newDocPath = newDocPathSplit.join("/");
    if (typeof (ShowAttachment) == "undefined")
        ShowAttachment = false;

    if (typeof (downloadFile) == "undefined")
        downloadFile = false;
    if (typeof (format) == "undefined")
        format = "pdf";
    if (typeof (downloadName) == "undefined")
        downloadName = "";

    if (downloadFile) {
        $.ajax({
            type: "POST",
            url: WebsiteURL + "/Utility/General/ViewReportAndUploadToNode",
            dataType: "json",
            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            data: {
                serverRelativeUrlPath: ReportPath,
                ReportParameters: ParameterData,
                ShowAttachment: ShowAttachment,
                format: format,
                NewFileName: downloadName,
                PathCreate: newDocPath
            },
            success: function (result) {
                result.fullPath;
                response.DocPathList.push(result.fullPath);
                DownloadDoc(response);
                //result.fileName;
                //var docMapp = {
                //    newDocPath: response.DocPathList[0].split('//')[1].split('/')[2] + "/" + result.fileName,
                //    oldDocPath: result.fullPath
                //};
                //var FCSurl = response.CurrentLocationIp + "/api/DupData";
                //jQuery.ajax({
                //    url: FCSurl,
                //    data: JSON.stringify(docMapp),
                //    contentType: 'application/json; charset=utf-8',
                //    dataType: 'json',
                //    cache: false,
                //    async: true,
                //    processData: false,
                //    method: 'POST',
                //    type: 'POST', // For jQuery < 1.9
                //    success: function (response) {
                //        response.DocPathList.push(response.UpdatedFile);
                //        DownloadDoc(response);
                //    }
                //});
            }
        });
    }

}


function showLoading(flag) {
    if (flag) {
        $(".quick-nav-overlay").show();
        $("#divLoader").show();
    }
    else {
        $("#divLoader").hide();
        $(".quick-nav-overlay").hide();
    }
}

function DisplayNotiMessage(Type, Message, Title) {
    if (Type.toLowerCase() == "success") {
        toastr.success(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "error") {
        toastr.error(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "info") {
        toastr.info(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "warning") {
        toastr.warning(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
}

function DownloadDoc(response) {

    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/Download";
    //window.open("" + FCSurl + "?DocList=" + DocPathList, "DownloadWindow", "width=750, height=750, top=0, screenX=1000, screenY=1000");
    popupWindow("" + FCSurl + "?DocList=" + response.DocPathList + "&TransmittalNo=" + response.TransmittalNo, "DownloadWindow", window, 500, 500);
}

function popupWindow(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblAgencyWiseDoc != null)
        tblAgencyWiseDoc.columns.adjust();
});