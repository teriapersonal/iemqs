﻿var tblAdvanceSearchDoc;
var tblfrequentSearched;
$(document).ready(function () {
    $('#drpFilter').show();
    $('#txtfilterdiv').hide();
    $('#btnfilterdiv').hide();
    $('#btnfilterdel').hide();

    var filtervalue = $("#drpFilter").val();
    if (filtervalue != "0" && filtervalue != "00") {
        $('#btnfilterdel').show();
    }

    tblAdvanceSearchDoc = $('#tblAdvanceSearchDoc').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/AdvanceSearch/GetAdvanceSerchDoc",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
            //{ "orderable": false, "targets": 3 },
            //{ "orderable": false, "targets": 4 },
            //{ "orderable": false, "targets": 5 }
        ],
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "customername", value: $("#CustomerName").val() });
            data.push({ name: "BUId", value: $('option:selected', "#drpBUId").text() });
            data.push({ name: "dept", value: $("#Department").val() });
            data.push({ name: "ObjectId", value: $('option:selected', "#drpObjectType").text() });
            data.push({ name: "StatusId", value: $('option:selected', "#dropstatus").text() });
            data.push({ name: "objName", value: $("#Name").val() });
            data.push({ name: "rev", value: $("#Revision").val() });
            data.push({ name: "Originator", value: $("#Originator").text() });
            data.push({ name: "Owner", value: $("#OwnerName").val() });
            data.push({ name: "keyword", value: $("#SearchText").val() });
            data.push({ name: "FProjectNo", value: $("#ProjectNumber").val() });
            data.push({ name: "SubObject", value: $("#drpSubObjectType").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    //var srt = '<a href="/DES/Dashboard/FindObject?Id=' + row.DCRNo + '&Type=DCR" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                    var srt = "";
                    var typeofobject = row.Type;
                    //if (row.Type == "JEP-CustomerFeedback") {
                    //    typeofobject = "JEP";
                    //}
                    if (row.Type == "Clad Part" || row.Type == "Jigfix" || row.Type == "Part" || row.Type == "Plate Part") {
                        typeofobject = "Part";
                    }
                    if (row.Type == "JEP-CustomerFeedback" || row.Type == "ROC-Comment-Attached" || row.Type == "ROC" || row.Type == "ROC Comment") {
                        srt = '<a href="/DES/Dashboard/FindObject?Id=' + row.ObjectId + '&Type=' + typeofobject + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>' +
                            '<a target="_blank" href="/DES/Dashboard/FindObject?Id=' + row.ObjectId + '&Type=' + typeofobject + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-external-link m--font-success"></i></a>';
                    }
                    else {
                        srt = '<a href="/DES/Dashboard/FindObject?Id=' + row.Name + '&Type=' + typeofobject + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View Object Details"><i class="la la-eye m--font-success"></i></a>' +
                            '<a target="_blank" href="/DES/Dashboard/FindObject?Id=' + row.Name + '&Type=' + typeofobject + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View Object Details - New Window"><i class="la la-external-link m--font-success"></i></a>';
                    }
                    if (row.Type == "DOC" || row.Type == "JEP-CustomerFeedback" || row.Type == "DCR" || row.Type == "ROC" || row.Type == "ROC-Comment-Attached") {
                        srt += "<a href='javascript:;' class='m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV' title='Universal Viewer'> <i class='la la-desktop m--font-focus'></i></a> <a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload " + row.Type + "' title='Download'><i class='fa fa-download m--font-brand' ></i></a>";
                        //srt += '';
                        //return srt;
                    }
                    else {
                        srt += "";

                    }
                    return srt;
                }
            },
            {
                "sTitle": "Name", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                    var name = "";
                    if (row.Type == "DOC") {
                        name = "<i class='flaticon-doc text-primary'></i> <span class='m--font-bolder'>" + row.Name + "</span>";
                    }
                    else {
                        name = "<i class='fa fa-folder text-warning'></i> <span class='m--font-bolder'>" + row.Name + "</span>";
                    }
                    return name;
                }
            },
            {
                "sTitle": "Rev/Issue", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                    var revision = "<span class='text-primary m--font-bolder'>" + row.Revision + "</span>";
                    return revision;
                    //return row.Revision;
                }
            },
            {
                "sTitle": "Object Type", "className": "", render: function (data, type, row, meta) {
                    return row.Type;
                }
            },
            {
                "sTitle": "File Name", "className": "", render: function (data, type, row, meta) {
                    var attach = "";
                    if (row.Type == "DOC" || row.Type == "JEP-CustomerFeedback" || row.Type == "DCR" || row.Type == "ROC" || row.Type == "ROC-Comment-Attached") {
                        attach = row.FileName;
                    }
                    else {
                        attach = "<span class='text-primary m--font-bolder'>-</span>";
                    }

                    return attach;
                }
            },
            //{
            //    "sTitle": "File Formate", "className": "", render: function (data, type, row, meta) {
            //        var attach = "";
            //        if (row.Type == "DOC") {
            //            attach = row.FileFormat;
            //        }
            //        else {
            //            attach = "<span class='text-primary m--font-bolder'>-</span>";
            //        }

            //        return attach;
            //    }
            //},
            {
                "sTitle": "Project", "className": "", render: function (data, type, row, meta) {
                    return row.ProjectNumber;
                }
            },            
            {
                "sTitle": "Status", "className": "", render: function (data, type, row, meta) {
                    var status = "";
                    if (row.Status == "InProcess") {
                        status = "<span class=' m-badge m-badge--success  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.Status + "</span>";
                    }
                    else if (row.Status == "Release") {
                        status = "<span class=' m-badge m-badge--warning  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.Status + "</span>";
                    }
                    else if (row.Status == "Create") {
                        status = "<span class=' m-badge m-badge--primary  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.Status + "</span>";
                    }
                    else {
                        status = "<span class=' m-badge m-badge--primary  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.Status + "</span>";
                    }

                    return status;
                }
            }
            //{
            //    "sTitle": "Attachment", "className": "text-center", render: function (data, type, row, meta) {
            //        var attach = "";
            //        if (row.Type == "DOC") {
            //            attach = "<i class='la la-paperclip'></i>1";
            //        }
            //        else {
            //            attach = "<span class='text-primary m--font-bolder'>-</span>";
            //        }

            //        return attach;
            //    }
            //},

            //,
            //{
            //    "sTitle": "Report", "className": "text-center", "orderable": false,render: function (data, type, row, meta) {
            //        return row.Revision;
            //    }
            //}
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblAdvanceSearchDoc.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        }

    });

    //mApp.blockPage();
    //tblAdvanceSearchDoc.ajax.reload();
    //mApp.unblockPage();
});


$('#tblAdvanceSearchDoc').on('click', 'td .btnDownload', function () {
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    //var CurrentLocationIp = 'https://10.7.66.185';
    var FCSurl = CurrentLocationIp + "/Download";
    var tr = $(this).closest('tr');
    var row = tblAdvanceSearchDoc.row(tr);
    var data = row.data();
    if ($(this).hasClass("DCR")) {
        var Id = data.ObjectId.split(/-/).pop();
        $.ajax({
            type: 'POST',
            url: '/AdvanceSearch/GetDCRIdByDCRSupportedDocId',
            data: { "Id": Id },
            datatype: "json",
            success: function (response) {
                if (response.DCRId > 0) {
                    var dat = [];
                    dat.push({ Param: "DCRId", Value: response.DCRId });
                    ShowReport(dat, "/DES/DCR Report", false, true, 'pdf', response.DCRNo);
                }
                else {
                    toastr.info("DCR Report does not exist", "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                console.log(error);
            }
        })
        
    }
    downloadFile(data.MainDocumentPath, CurrentLocationIp);    
    //var DocPathList = [];
    //DocPathList.push(data.MainDocumentPath);

    //window.open("" + FCSurl + "?DocList=" + DocPathList, "DownloadWindow", "width = 750, height = 750, top = 0, screenX = 1000, screenY = 1000");
});

$('#btnsearch').click(function (e) {
    mApp.blockPage();
    tblAdvanceSearchDoc.ajax.reload();
    mApp.unblockPage();
});

$('#frequentSearchTab').on("click", function () {

    //alert("tab no 2 is clicked....");

    tblfrequentSearched = $('#tblfrequentSearched').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/AdvanceSearch/GetMostSearchedDocuments",
        "iDisplayLength": 10,
        "ordering": false,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        //"order": [[1, 'desc']],
        //"columnDefs": [
        //    { "orderable": false, "targets": 0 }
        //    { "orderable": false, "targets":  1},
        //    //{ "orderable": false, "targets": 4 },
        //    //{ "orderable": false, "targets": 5 }
        //],
        "fnServerData": function (sSource, data, fnCallback) {
            //data.push({ name: "customername", value: $("#CustomerName").val() });
            //data.push({ name: "BUId", value: $('option:selected', "#drpBUId").text() });
            //data.push({ name: "dept", value: $("#Department").val() });
            //data.push({ name: "ObjectId", value: $('option:selected', "#drpObjectType").text() });
            //data.push({ name: "StatusId", value: $('option:selected', "#dropstatus").text() });
            //data.push({ name: "objName", value: $("#Name").val() });
            //data.push({ name: "rev", value: $("#Revision").val() });
            //data.push({ name: "Originator", value: $("#Originator").text() });
            //data.push({ name: "Owner", value: $("#OwnerName").val() });
            //data.push({ name: "keyword", value: $("#SearchText").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    if (row.ObjectType == "DOC" || row.ObjectType == "JEP-CustomerFeedback" || row.ObjectType == "DCR" || row.ObjectType == "ROC" || row.ObjectType == "ROC-Comment-Attached") {
                        var srt = "<a href='javascript:;' class='m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV' title='Universal Viewer'> <i class='la la-desktop m--font-focus'></i></a> <a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload' title='Download'><i class='fa fa-download m--font-brand' ></i></a>";
                        //srt += '';
                        return srt;
                    }
                    else {
                        var srt1 = "";
                        return srt1;
                    }
                }
            },
            {
                "sTitle": "Name", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                    var name = "";
                    if (row.ObjectType == "DOC") {
                        name = "<i class='flaticon-doc text-primary'></i> <span class='m--font-bolder'>" + row.ObjectName + "</span>";
                    }
                    else {
                        name = "<i class='fa fa-folder text-warning'></i> <span class='m--font-bolder'>" + row.ObjectName + "</span>";
                    }
                    return name;
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                    var revision = "<span class='text-primary m--font-bolder'>" + row.Revision + "</span>";
                    return revision;
                    //return row.Revision;
                }
            },
            {
                "sTitle": "Object Type", "className": "", render: function (data, type, row, meta) {
                    return row.ObjectType;
                }
            },
            {
                "sTitle": "File Name", "className": "", render: function (data, type, row, meta) {
                    var attach = "";
                    if (row.ObjectType == "DOC" || row.ObjectType == "JEP-CustomerFeedback" || row.ObjectType == "DCR" || row.ObjectType == "ROC" || row.ObjectType == "ROC-Comment-Attached") {
                        attach = row.FileName;
                    }
                    else {
                        attach = "<span class='text-primary m--font-bolder'>-</span>";
                    }

                    return attach;
                }
            },
            //{
            //    "sTitle": "File Formate", "className": "", render: function (data, type, row, meta) {
            //        var attach = "";
            //        if (row.Type == "DOC") {
            //            attach = row.FileFormat;
            //        }
            //        else {
            //            attach = "<span class='text-primary m--font-bolder'>-</span>";
            //        }

            //        return attach;
            //    }
            //},
            {
                "sTitle": "File Remarks", "className": "", render: function (data, type, row, meta) {
                    return row.Descripion;
                }
            }
            //{
            //    "sTitle": "Status", "className": "", render: function (data, type, row, meta) {
            //        var status = "";
            //        if (row.ObjectStatus == "InProcess") {
            //            status = "<span class=' m-badge m-badge--success  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.ObjectStatus + "</span>";
            //        }
            //        else if (row.ObjectStatus == "Release") {
            //            status = "<span class=' m-badge m-badge--warning  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.ObjectStatus + "</span>";
            //        }
            //        else if (row.ObjectStatus == "Create") {
            //            status = "<span class=' m-badge m-badge--primary  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.ObjectStatus + "</span>";
            //        }
            //        else {
            //            status = "<span class=' m-badge m-badge--primary  m-badge-c ' data-container='body' data-toggle='m - tooltip' data-placement='top' data-original-title='Release'>" + row.ObjectStatus + "</span>";
            //        }

            //        return status;
            //    }
            //}
            //{
            //    "sTitle": "Attachment", "className": "text-center", render: function (data, type, row, meta) {
            //        var attach = "";
            //        if (row.Type == "DOC") {
            //            attach = "<i class='la la-paperclip'></i>1";
            //        }
            //        else {
            //            attach = "<span class='text-primary m--font-bolder'>-</span>";
            //        }

            //        return attach;
            //    }
            //},

            //,
            //{
            //    "sTitle": "Report", "className": "text-center", "orderable": false,render: function (data, type, row, meta) {
            //        return row.Revision;
            //    }
            //}
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblfrequentSearched.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        }

    });
});

$('#tblfrequentSearched').on('click', 'td .btnDownload', function () {
    //var CurrentLocationIp = $('#CurrentLocationIp').val();
    var CurrentLocationIp = 'https://10.7.66.185';
    var FCSurl = CurrentLocationIp + "/Download";
    var tr = $(this).closest('tr');
    var row = tblfrequentSearched.row(tr);
    var data = row.data();
    downloadFile(data.DocumentPath, CurrentLocationIp);
    //var DocPathList = [];
    //DocPathList.push(data.MainDocumentPath);

    //window.open("" + FCSurl + "?DocList=" + DocPathList, "DownloadWindow", "width = 750, height = 750, top = 0, screenX = 1000, screenY = 1000");
});

$('#SearchTab').on("click", function () {
    var table = $('#tblfrequentSearched').DataTable();

    table.destroy();
});


$('#tblJepDocumentDetails').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblJepDocumentDetails.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});



$('#tblAdvanceSearchDoc').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblAdvanceSearchDoc.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblfrequentSearched').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblfrequentSearched.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#btndelfilterdata').click(function (e) {
    var filtervalue = $("#drpFilter").val();

    mApp.unblockPage();
    $.ajax({
        type: 'POST',
        url: '/AdvanceSearch/DeleteFilterData',
        data: { filterid: filtervalue },
        success: function (response) {

            if (response == "Filter Data has been removed successfully.") {
                ResetControls_InnerSearch();
                BindSearchFilterDrp();
                toastr.success(response, "Success");
            }
            else {
                toastr.error(response, "Error");
            }
            mApp.unblockPage();
        },
        error: function (error) {
            mApp.unblockPage();
            console.log(error);
        }
    })
    mApp.unblockPage();
});
