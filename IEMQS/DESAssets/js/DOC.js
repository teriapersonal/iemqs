﻿

//Wizard
var WizardDemo = function () {
    $("#m_wizard");
    var e, r, i = $("#m_form"), f = $("#FormDOCAttr");
    return {
        init: function () {
            var n;
            $("#m_wizard"), i = $("#m_form"), (r = new mWizard("m_wizard", {
                startStep: 1
            })).on("beforeNext", function (r) {
                //!0 !== e.form() && r.stop()

            }), r.on("change", function (e) {
                mUtil.scrollTop()
            }), r.on("change", function (e) {
                1 === e.getStep();
            })
        }

        , Gotonext: function () {
            r.goNext();
        }
        , GotoBack: function () {
            r.goPrev();
            e.goNext();
        }

        , GoTo: function (i) {
            if ($("#DocumentId").val() != "" && $('#IsRevision').val() != "True" && $('#IsDOCCheckIn').val() != "True") {
                r.goTo(i);
                getGrid("");
            }
        }
    }
}();

var funSuccessDocID;
var funSuccessDocNo;
var funSuccessRevision;
var funSuccessProject;
var funSuccessIsEdit;
var funSuccessIsRevision;
//Fill Grid.
var tblDocumentFiles;
var tblDocument;

$(document).ready(function () {
    //$('#txtDocumentNo').bind('copy paste cut', function (e) {
    //    e.preventDefault(); //disable cut,copy,paste
    //});

    $('#txtDocumentNo').focusout(function () {

        //var txtValue = $(this).val().split("");
        //var getValue = "";
        //var i;
        //for (i = 0; i < txtValue.length; i++) {
        //    getValue += txtValue[i].replace(/\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/, "");
        //}
        //$(this).val(getValue);    
        if ((/\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/).test($(this).val()) == true) {
            $(this).val('');
        }
    });


    $('.getDocList').select2();
    $('.getJEPDocList').select2();
    WizardDemo.init()
    if ($("#IsJepDrop").val() == "true" && $("#IsEdit").val() == "False") {
        $("#jepDocNumber").css('display', 'block');
        $("#DocNumber").css('display', 'none');
    }
    else {
        $("#jepDocNumber").css('display', 'none');
        $("#DocNumber").css('display', 'block');

    }

    if ($("#IsJepDrop").val() == "true" && $("#IsEdit").val() == "True") {
        $('#IsJepDrop').css('background-color', '#F4F5F8 !important');
        $('#IsJepDrop').prop('disabled', 'disabled');


        $("#jepDocNumber2").css('display', 'block');
        $("#jepDocNumber").css('display', 'none');
        $("#DocNumber").css('display', 'none');

        $('#JEPDocumentTitle').attr('readonly', 'readonly');
        // $('#JEPClientDocumentNumber').attr('readonly', 'readonly');
        //$('#JEPClientDocumentRevision').attr('readonly', 'readonly');

        $('#JEPDocumentTypeId').css('background-color', '#F4F5F8 !important');
        $('#JEPDocumentTypeId').prop('disabled', 'disabled');
        $('#JEPDocumentTypeId').css('background', 'none');

        // $('#JEPDocumentMilestone').css('background-color', '#F4F5F8 !important');
        // $('#JEPDocumentMilestone').prop('disabled', 'disabled');

    }
    tblDocumentFiles = $('#tblDocumentFiles').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DOC/Detail",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [[0, 'desc']],
        "columnDefs": [

        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DocId", value: $('#DocumentId').val() });
            data.push({ name: "currentUser", value: $('#GetCurrentUser').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    if (row.Authorized > 0) {
                        if ($('#IsDocDownloadRights').val() == "True" && $('#IsProjectRights').val() == "True") {
                            srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                        }
                        if (row.DocStatus == "Created" || row.DocStatus == "Draft") {
                            if ((row.CheckInStatus == null || row.CheckInStatus == false)) {
                                //srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnCheckIn" title="CheckIn"><i class="fa fa-lock-open m--font-success"></i></button>';
                                srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckIn" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                srt += ' <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDFM" title="Delete"><i class="la la-trash text-danger"></i></a>';
                            }

                            else if (row.CheckOutStatus == false && (row.CheckInStatus == true && row.CheckInBy == true)) {
                                //srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut" ><i class="fa fa-lock m--font-danger"></i></button>';
                                srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckOut" title="CheckIn" ><i class="fa fa-lock-open m--font-success"></i></button>';
                            }
                        }
                    }
                    return srt;
                }
            },
            {
                "sTitle": "DMId", "visible": false, "orderable": false, render: function (data, type, row, meta) {
                    return row.DocumentMappingId
                }
            },
            {
                "sTitle": "Name", render: function (data, type, row, meta) {
                    return row.Document_name
                },
            },
            {
                "sTitle": "Ver", "orderable": true, "className": "", render: function (data, type, row, meta) {
                    return row.FileVersion
                }
            },
            {
                "sTitle": "Main Document", "orderable": false, "className": "", render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.DocStatus == "InProcess" || row.DocStatus == "Completed") {
                        if (row.IsMainDocument.toString().toLowerCase() === "true") {
                            strHtml = "<input name='IsMDoc' type='checkbox' checked Id='IsMainDoc' disabled  class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + "  />";
                        }
                        else {
                            strHtml = "<input name='IsMDoc'  type='checkbox' Id='IsMainDoc' disabled class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + " />";
                        }
                    }
                    else {
                        if (row.IsMainDocument.toString().toLowerCase() === "true") {
                            strHtml = "<input name='IsMDoc' type='checkbox' checked Id='IsMainDoc' onclick='isMainDisable($(this));' class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + "  />";
                        }
                        else {
                            strHtml = "<input name='IsMDoc'  type='checkbox' Id='IsMainDoc' onclick='isMainDisable($(this));' class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + " />";
                        }
                    }
                    return strHtml
                }
            },
            {
                "sTitle": "Remark", "orderable": true, render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.DocStatus == "Completed") {
                        if (row.Remark == null || row.Remark == "undifined") {
                            strHtml = "<textarea name='GenralRemark' disabled  style='width:150px !important' type='text' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'   />";
                        }
                        else {
                            strHtml = "<textarea name='GenralRemark' disabled type='text' style='width:150px !important' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'  >" + row.Remark + "</textarea>";
                        }

                    }
                    else {
                        if (row.Remark == null || row.Remark == "undifined") {
                            strHtml = "<textarea name='GenralRemark' type='text' style='width:150px !important' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'   />";
                        }
                        else {
                            strHtml = "<textarea name='GenralRemark' type='text' style='width:150px !important' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'  >" + row.Remark + "</textarea>";
                        }
                    }
                    return strHtml;
                },
            },
            {
                "sTitle": "Status", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.Status
                },
            },
            {
                "sTitle": "Formate", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.FileFormate
                },
            },


            {
                "sTitle": "DocumentId", "visible": false, "orderable": false, render: function (data, type, row, meta) {
                    return row.DocumentId
                }
            },
            {
                "sTitle": "Main DocumentPath", "className": "", "visible": false, render: function (data, type, row, meta) {
                    return row.MainDocumentPath;
                }
            },

            {
                "sTitle": "Created By", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedBy
                }
            },
            {
                "sTitle": "Created On", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedOn
                }
            },
            {
                "sTitle": "Edited By", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.EditedBy
                }
            },
            {
                "sTitle": "Edited On", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.EditedOn
                }
            },
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }

    });
});

function getGrid(funSuccessDocID) {

    tblDocumentFiles.ajax.reload();
}

$('.getJEPDocList').change(function () {
    var getJEPID = $('.getJEPDocList option:selected').val();
    if (getJEPID != null && getJEPID != "" && getJEPID != 'undefined') {
        var model = {
            JEPDocumentDetailsId: getJEPID
        };

        $.ajax({
            url: "/DES/DOC/GetJEPData",
            data: JSON.stringify(model),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {

                    //toastr.success(response.Msg, "Success");
                    $('#JEPDocumentTitle').val(response.modelData.DocumentTitle);

                    $('#JEPDocumentTypeId').val(response.modelData.DocumentTypeId).trigger('change');
                    $('#JEPClientDocumentNumber').val(response.modelData.ClientDocumentNumber);
                    $('#JEPClientDocumentRevision').val(response.modelData.ClientVersion);
                    $('#DocumentRevisionStr').val(response.modelData.DocumentRevisionStr);
                    $('#DocumentRevision').val(response.modelData.DocumentRevision);
                    $('#JEPDocumentTitle').attr('readonly', 'readonly');
                    // $('#JEPClientDocumentNumber').attr('readonly', 'readonly');
                    //  $('#JEPClientDocumentRevision').attr('readonly', 'readonly');
                    $('#JEPDocumentTypeId').css('background-color', '#F4F5F8 !important');
                    $('#JEPDocumentTypeId').prop('disabled', 'disabled');
                    $('#JEPDocumentTypeId').css('background', 'none');

                    $('#JEPDocumentMilestone').val("" + response.modelData.MilestoneDocument + "");
                    $('#MilestoneDocument').val("" + response.modelData.MilestoneDocument + "");

                    // $('#JEPDocumentMilestone').css('background-color', '#F4F5F8 !important');
                    // $('#JEPDocumentMilestone').prop('disabled', 'disabled');

                    $('#DocumentTypeId').val(response.modelData.DocumentTypeId);

                    mApp.unblockPage();
                }
                else {
                    toastr.info(response.Msg, "Info");
                    mApp.unblockPage();
                }
            },
            error: function (response) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        });
    }
    else {

        $('#JEPDocumentTitle').val('');
        $('#JEPDocumentTypeId').val('').trigger('change');
        $('#JEPDocumentTypeId').removeAttr('disabled');
        $('#JEPDocumentTitle').removeAttr('readonly');
        $('#JEPClientDocumentNumber').removeAttr('readonly');
        $('#JEPClientDocumentRevision').removeAttr('readonly');

        $('#DocumentTypeId').val('');
        $('#JEPClientDocumentNumber').val('');
        $('#JEPClientDocumentRevision').val('');
        $('#DocumentRevisionStr').val("R0");
        $('#DocumentRevision').val("0");
        $('#JEPDocumentMilestone').val("false");
        $('#MilestoneDocument').val("false");

        $('#JEPDocumentMilestone').removeAttr('disabled');
        $('#JEPDocumentMilestone').css('background-color', 'white');

        toastr.info(response.Msg, "select JEP");
    }
});

var IsJEP;
var IsRevision;
var JEPDocumentNo;
var Project;
var DocNo;
var GetFirstTimeDocNo;
var Revision;

function ajaxValidate() {
    mApp.blockPage();
}

//If Ajax.Begin Get Scuccess Then
funSuccess = function (data) {
    mApp.unblockPage();
    if (data.Status) {
        $('#DocumentId').val(data.model.DocumentId);
        funSuccessDocID = data.model.DocumentId;
        funSuccessDocNo = data.model.DocumentNo;
        funSuccessRevision = data.model.DocumentRevision;
        funSuccessProject = data.model.Project;
        funSuccessIsRevision = data.model.IsRevision;
        var CurrentLocationIp = $('#CurrentLocationIp').val();
        var CurrentLocationPath = $('#FilePath').val();
        var FCSurl = CurrentLocationIp + "/api/ReviseData";
        var DocumentRevision = data.model.DocumentRevision;
        //If Revision
        if (funSuccessIsRevision == true) {
            var linksToCopy = data.model.NewDocumentPathForRevison;

            concatDocandProject = CurrentLocationPath+"/"+funSuccessProject + "-" + funSuccessDocNo + "-R" + DocumentRevision;

            var docMapp = {
                newDocPath: concatDocandProject,
                oldDocPath: linksToCopy
            };
            if ($('#CurrentLocationIp').val() != "") {
                mApp.blockPage();
                jQuery.ajax({
                    url: FCSurl, // Node Applictation Redrection
                    data: JSON.stringify(docMapp),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (response) {
                        if (response.ReviseData) {
                            var addDocMapp = {
                                OldDocIDForRevision: data.model.OldDocIDForRevision,
                                DocumentId: data.model.DocumentId,
                                newPathAfterRevise: response.paths
                            };
                            $.ajax({
                                type: "POST",
                                url: "/DES/DOC/NewRevision", //If get File Then Document Mapping Add
                                data: JSON.stringify(addDocMapp),
                                contentType: "application/json;charset=utf-8",
                                dataType: "json",
                                aysnc: true,
                                success: function (response) {
                                    if (response) {
                                        toastr.success(response.Msg, "Success");
                                        getGrid(funSuccessDocID);
                                        $('#IsRevision').val('');
                                        getGrid(funSuccessDocID);
                                        WizardDemo.Gotonext();
                                        mApp.unblockPage();
                                    } else {
                                        toastr.error(response.Msg, "Not Success");
                                        mApp.unblockPage();
                                    }
                                },
                                error: function (response) {
                                    toastr.error(response.Msg, "Error");
                                    mApp.unblockPage();
                                }
                            });


                            mApp.unblockPage();
                        }
                    },
                }).fail(function (data) {
                    toastr.error("Server response not received", "Error");
                    mApp.unblockPage();
                });

                $('#IsRevision').val(false);
            }
            else {
                toastr.error("Ip is not detecting", "Error");
                mApp.unblockPage();
            }
        }
        else {
            getGrid(funSuccessDocID);
            WizardDemo.Gotonext();
        }


    } else {
        if (data.Info) {
            toastr.info(data.Msg, "Info");
            mApp.unblockPage();
        }
        else {
            toastr.error(data.Msg, "Error");
            mApp.unblockPage();
        }
    }

}

function paramNameForSend() {
    return 'DocumentFile[]';
}


//When Docs are drop in DropZone
var iii = 1;
var regex = /\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;
var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    maxFilesize: 200000,
    maxFiles: 10,
    parallelUploads: 10,
    parallelChunkUploads: true,
    forceChunking: true,
    paramName: paramNameForSend,
    uploadMultiple: true,
    accept: function (file, done) {
        var fileName = regex.test(file.name);
        if (fileName) {
            toastr.info(file.name + " Contains special characters", "Info");
            myDropzone.removeFile(file);
        }
        else {
            var dotNameSplit = file.name.split('.');
            if (dotNameSplit.length > 2) {
                toastr.info(file.name + " Contains special characters", "Info");
                myDropzone.removeFile(file);
            }
            else {
                done();
            }
        }
    },

    init: function () {
        this.on("maxfilesexceeded", function (file) {

            if (this.files.length == 11) {
                // this.removeFile(this.files[0]);
                if (iii == 1) {
                    alert("10 Files can be uploded at once!");
                }
                iii++;
            }
            myDropzone.removeFile(file);
            myDropzone.removeAllFiles();
        });

    },
    queuecomplete: function () {//
        iii = 1;
    }
});

myDropzone.on('sendingmultiple', function (file, xhr, formData, progress, bytesSent) {

    $('.dz-error-message').css('display', 'none');
    $('.dz-success-mark').css('display', 'block');
    $('.dz-error-mark').css('display', 'none');

    funSuccessDocID = $("#DocumentId").val();
    if ($('#CurrentLocationIp').val() != "") {

        var CurrentLocationIp = $('#CurrentLocationIp').val();
        var CurrentLocationPath = $('#FilePath').val();
        var FCSurl = CurrentLocationIp + "/api/DocObjUpload";
        var filess = [];
        funSuccessProject = $('#Project').val();
        funSuccessRevision = $('#DocumentRevision').val();

        if (funSuccessDocNo == undefined) {
            funSuccessDocNo = $('#DocumentNoToGetInHidden').val();
        }
        var concatDocandProject = funSuccessProject + "-" + funSuccessDocNo + "-R" + funSuccessRevision;

        formData.append('DocumentPath', concatDocandProject.toString());
        formData.append('SavedPath', CurrentLocationPath.toString());
        for (var i = 0; i < file.length; i++) {
            filess.push(file[i]);
            formData.append('DocumentFile[]', file[i]);
        }
        //Need To get Status and other filed HERE 
        mApp.blockPage();

        window.onbeforeunload = function () {
            var prevent_leave = true;
            if (prevent_leave) {
                return "Your files are not completely uploaded yet...";
            }
        }

        jQuery.ajax({
            url: FCSurl,
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function (response) {
                if (response.Uploded) {
                          var docMapp = {
                            DocumentId: funSuccessDocID,
                            FileFormatData: response.FileData
                        };
                        $.ajax({
                            type: "POST",
                            url: "/DES/DOC/CreateDocumentMapping", //If get File Then Document Mapping Add
                            data: JSON.stringify(docMapp),
                            contentType: "application/json;charset=utf-8",
                            dataType: "json",
                            aysnc: true,
                            success: function (response) {
                                if (response) {
                                    toastr.success(response.Msg, "Success");
                                    myDropzone.removeAllFiles();
                                    getGrid(funSuccessDocID);
                                    mApp.unblockPage();
                                } else {
                                    toastr.error(response.Msg, "Not Success");
                                    mApp.unblockPage();
                                }
                            },
                            error: function (response) {
                                toastr.error(response.Msg, "Error");
                                myDropzone.removeAllFiles()
                                mApp.unblockPage();
                            }
                        });
                }
                else {
                    toastr.error("Fail to Uploaded on Server", "Error");
                    myDropzone.removeFile(file);
                    mApp.unblockPage();
                }
                window.onbeforeunload = function () {
                    var prevent_leave = false;
                    if (prevent_leave) {
                        return "Your files are not completely uploaded...";
                    }
                }
            },
        }).fail(function (data) {
            toastr.error("Server response not received", "Error");
            myDropzone.removeFile(file);
            mApp.unblockPage();

            window.onbeforeunload = function () {
                var prevent_leave = false;
                if (prevent_leave) {
                    return "Your files are not completely uploaded...";
                }
            }
        });
    }
    else {
        toastr.error("Ip is not detecting", "Error");
        myDropzone.removeFile(file);
        mApp.unblockPage();
    }
});

var myDropzoneThree = new Dropzone('div#m-dropzone-three', {

    timeout: 18000000,
    parallelUploads: 1000,
    maxFilesize: 200000,
    maxFiles: 1,
    accept: function (file, done) {
        var fileName = regex.test(file.name);
        if (fileName) {
            toastr.info(file.name + " Contains special characters", "Info");
            myDropzoneThree.removeFile(file);
        }
        else {
            var dotNameSplit = file.name.split('.');
            if (dotNameSplit.length > 2) {
                toastr.info(file.name + " Contains special characters", "Info");
                myDropmyDropzoneThreezone.removeFile(file);
            }
            else {
                done();
            }
        }
    },
    init: function () {
        this.on("maxfilesexceeded", function (file) {
            alert("No more files please!");
            myDropzoneThree.removeFile(file);
            myDropzoneThree.removeAllFiles();
        });
    },
});
var DocMappingID;
var DocId;
myDropzoneThree.on('sending', function (file, xhr, formData) {
    $('.dz-error-message').css('display', 'none');
    $('.dz-success-mark').css('display', 'block');
    $('.dz-error-mark').css('display', 'none');

    var concatDocandProject;
    var DocumentMappingID = DocMappingID;

    if ($('#CurrentLocationIp').val() != "") {
        var CurrentLocationIp = $('#CurrentLocationIp').val();
        var CurrentLocationPath = $('#FilePath').val();
        var FCSurl = CurrentLocationIp + "/api/Upload";
        funSuccessProject = $('#Project').val();
        funSuccessRevision = $('#DocumentRevision').val();

        if (funSuccessDocNo == undefined) {
            funSuccessDocNo = $('#DocumentNoToGetInHidden').val();
        }

        concatDocandProject = funSuccessProject + "-" + funSuccessDocNo + "-R" + funSuccessRevision;
        formData.append('SavedPath', CurrentLocationPath.toString());
        formData.append('DocumentPath', concatDocandProject.toString());
        formData.append('DocumentFile', file);
        mApp.blockPage();
        window.onbeforeunload = function () {
            var prevent_leave = true;
            if (prevent_leave) {
                return "Your files are not completely uploaded yet...";
            }
        }
        jQuery.ajax({
            url: FCSurl,
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function (response) {
                if (response.Uploded) {
                    var fname = response.SavedFileName;
                    $('#CheckOutFilename').val(response.OriginalFileName);
                    $('#CheckOutPath').val(response.SavedPath + "/" + fname);
                    $('#CheckOutGetExtention').val(response.OriginalFileName.split('.')[1]);
                    mApp.unblockPage();
                }
                else {
                    toastr.error("Fail to File Uploaded to Server", "Error");
                    myDropzoneThree.removeFile(file);
                    mApp.unblockPage();
                }

                window.onbeforeunload = function () {
                    var prevent_leave = false;
                    if (prevent_leave) {
                        return "Your files are not completely uploaded...";
                    }
                }
                mApp.unblockPage();
            },
        }).fail(function (data) {
            toastr.error("Server response not received", "Error");
            myDropzoneThree.removeFile(file);
            window.onbeforeunload = function () {
                var prevent_leave = false;
                if (prevent_leave) {
                    return "Your files are not completely uploaded...";
                }
            }
            mApp.unblockPage();
        });

    }
    else {
        toastr.error("Ip is not detecting", "Error");
        mApp.unblockPage();
    }
});

// Update the total progress bar
//If Dropdown change Then JEP dropdwon Show/Hide
$("#IsJepDrop").change(function () {
    if ($("#IsJepDrop").val() == "true") {
        $("#jepDocNumber").css('display', 'block');
        $("#DocNumber").css('display', 'none');
        if ($('#JEPDocumentDetailsId').val() != "") {
            var getJEPID = $('#JEPDocumentDetailsId').val();
            var model = {
                JEPDocumentDetailsId: getJEPID
            };

            $.ajax({
                url: "/DES/DOC/GetJEPData",
                data: JSON.stringify(model),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        $('#JEPDocumentTitle').val(response.modelData.DocumentTitle);
                        $('#JEPDocumentTypeId').val(response.modelData.DocumentTypeId).trigger('change');
                        $('#JEPClientDocumentNumber').val(response.modelData.ClientDocumentNumber);
                        $('#JEPClientDocumentRevision').val(response.modelData.ClientVersion);

                        //toastr.success(response.Msg, "Success");
                        $('#JEPDocumentTitle').attr('readonly', 'readonly');
                        // $('#JEPClientDocumentNumber').attr('readonly', 'readonly');
                        // $('#JEPClientDocumentRevision').attr('readonly', 'readonly');

                        $('#JEPDocumentTypeId').css('background-color', '#F4F5F8 !important');
                        $('#JEPDocumentTypeId').prop('disabled', 'disabled');
                        $('#JEPDocumentTypeId').css('background', 'none');
                        $('#DocumentTypeId').val(response.modelData.DocumentTypeId);
                        $('#DocumentRevisionStr').val(response.modelData.DocumentRevisionStr);
                        $('#DocumentRevision').val(response.modelData.DocumentRevision);

                        $('#JEPDocumentMilestone').val("" + response.modelData.MilestoneDocument + "");
                        $('#MilestoneDocument').val("" + response.modelData.MilestoneDocument + "");

                        // $('#JEPDocumentMilestone').css('background-color', '#F4F5F8 !important');
                        // $('#JEPDocumentMilestone').prop('disabled', 'disabled');

                        mApp.unblockPage();
                    }
                    else {
                        toastr.info(response.Msg, "Info");
                        mApp.unblockPage();
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }
            });
        }
    }
    else {
        $('#JEPDocumentTitle').val('');
        $('#JEPDocumentTypeId').val('').trigger('change');
        $('#JEPDocumentTypeId').removeAttr('disabled');
        $('#JEPDocumentTitle').removeAttr('readonly');
        $('#JEPClientDocumentNumber').removeAttr('readonly');
        $('#JEPClientDocumentRevision').removeAttr('readonly');
        $('#DocumentTypeId').val('');
        $('#JEPClientDocumentNumber').val('');
        $('#JEPClientDocumentRevision').val('');
        $("#jepDocNumber").css('display', 'none');
        $("#DocNumber").css('display', 'block');
        $('#DocumentRevisionStr').val("R0");
        $('#DocumentRevision').val("0");
        $('#JEPDocumentMilestone').val("false");
        $('#MilestoneDocument').val("false");

        $('#JEPDocumentMilestone').removeAttr('disabled');
        $('#JEPDocumentMilestone').css('background-color', 'white');
    }
});


//This stand for CheckBox change event in Grid
function isMainDisable(t) {

    if (t.is(':checked')) {
        var DocumentMappingId = tblDocumentFiles.row(t.closest('tr')).data().DocumentMappingId;
        var IsMainDocument = "True";
        var docMapp = {
            DocumentMappingId: DocumentMappingId,
            IsMainDocument: IsMainDocument
        };
        mApp.blockPage();
        $.ajax({
            url: "/DES/DOC/UpdateMainFileStatus",
            data: JSON.stringify(docMapp),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentFiles.ajax.reload();
                    mApp.unblockPage();
                }
                else {
                    toastr.info(response.Msg, "Info");
                    mApp.unblockPage();
                }
            },
            error: function (response) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        });
    } else {
        var DocumentMappingId = tblDocumentFiles.row(t.closest('tr')).data().DocumentMappingId;
        var IsMainDocument = "False";
        var docMapp = {
            DocumentMappingId: DocumentMappingId,
            IsMainDocument: IsMainDocument
        };
        mApp.blockPage();
        $.ajax({
            url: "/DES/DOC/UpdateMainFileStatus",
            data: JSON.stringify(docMapp),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentFiles.ajax.reload();
                    mApp.unblockPage();
                } else {
                    toastr.info(response.Msg, "Info");
                    mApp.unblockPage();
                }
            },
            error: function (response) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        });
    }
}


//Delete From Document Table

$('#tblDocumentFiles').on('click', 'td .btnDeleteDFM', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";
    var documentName = data.Document_name;
    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                var docdelete = {
                    deletePath: data.MainDocumentPath
                };
                if ($('#CurrentLocationIp').val() != "") {
                    $.ajax({
                        url: FCSurl,
                        data: JSON.stringify(docdelete),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        cache: false,
                        async: true,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function (response) {
                            if (response.DeleteDOC) {
                                // toastr.success(response.Msg, "Successfully removed file from server");
                                $.ajax({
                                    type: "GET",
                                    url: '/DES/DOC/DeleteDocFile',
                                    data: { Id: data.DocumentMappingId },
                                    dataType: 'json',
                                    contentType: "application/json;charset=utf-8",
                                    async: true,
                                    success: function (response) {
                                        toastr.success(documentName + response.Msg, "Success");
                                        if (tblDocumentFiles.data().count() == 1) {
                                            tblDocumentFiles.ajax.reload();
                                        }
                                        else {
                                            tblDocumentFiles.ajax.reload(null, false);
                                        }
                                        mApp.unblockPage();
                                    },
                                    error: function (error) {
                                        toastr.error("Error while removing file from grid", "Error");
                                        mApp.unblockPage();
                                    }
                                })
                            }
                            else {
                                toastr.error("Error while removing file", "Error");
                                mApp.unblockPage();
                            }
                        },
                        error: function (error) {
                            toastr.error("Error while removing file from server", "Error");
                            mApp.unblockPage();
                        }
                    })
                }
                else {
                    toastr.error("Ip is not detecting", "Error");
                    mApp.unblockPage();
                }
            } else {
                mApp.unblockPage();
            }
        });
});

$('#tblDocumentFiles').on('click', 'td .btnCheckIn', function () {
    $('#CheckInDocumentPath').val('');
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    DocMappingID = data.DocumentMappingId;
    $('#CheckInDocumentPath').val(data.MainDocumentPath);
    // $(this).closest('tr').find('.btnCheckOut').prop('disabled', false);
    $('#myCehckInMarkModal').modal({ backdrop: 'static', keyboard: false })
    $('#myCehckInMarkModal').modal('show');
});

$('#tblDocumentFiles').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    DocMappingID = data.DocumentMappingId;
    DocId = data.DocumentId;
    $('#myCehckOutMarkModal').modal({ backdrop: 'static', keyboard: false });
    $('#myCehckOutMarkModal').modal('show');
    $('#CheckedOut_Remark').val('');
    myDropzoneThree.removeAllFiles();
    $('#CheckOutFilename').val('');
    $('#CheckOutPath').val('');
    $('#CheckOutGetExtention').val('');
});

var path;
$('#tblDocumentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.MainDocumentPath, CurrentLocationIp);
});

$('#tblDocumentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    //var DocPath = data.MainDocumentPath;
    UniverSalFileWTAnnotation(data.MainDocumentPath, data.DocumentMappingId, $('#GetCurrentUser').val(), data.DocumentNo, data.DocumentId);
});


//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 100;  //time in ms (3 seconds)

$('#CheckedOut_Remark').on('keyup', function () {
    clearTimeout(typingTimer);
    if ($('#CheckedOut_Remark').val()) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }
    function doneTyping() {
        var getCountOfCheckedOut_Remark = $('#CheckedOut_Remark').val().length;
        if (getCountOfCheckedOut_Remark > 51) {
            toastr.info("Remark text must be less then 50 character", "Info");
            $('#CheckedOut_Remark').val('');
        }
    }
});

$('#CheckedIN_Remark').on('keyup', function () {
    clearTimeout(typingTimer);
    if ($('#CheckedIN_Remark').val()) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }
    function doneTyping() {
        var getCountOfCheckedIN_Remark = $('#CheckedIN_Remark').val().length;
        if (getCountOfCheckedIN_Remark > 51) {
            toastr.info("Remark text must be less then 50 character", "Info");
            $('#CheckedIN_Remark').val('');
        }
    }
});

$("#addRemarkBtn").click(function () {
    var CurrentLocationIp = $('#CurrentLocationIp').val();

    var mainDocPathVal = $('#CheckInDocumentPath').val();
    downloadFile(mainDocPathVal, CurrentLocationIp);
    var CheckedIn_Remark = $('#CheckedIN_Remark').val();
    var DocumentMappingID = DocMappingID;
    var checkedIn = {
        DocumentMappingID: DocumentMappingID,
        CheckedIn_Remark: CheckedIn_Remark,
    };
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/DOC/AddCheckedInOut',
        data: JSON.stringify(checkedIn),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                $('#CheckedIN_Remark').val("");
                tblDocumentFiles.ajax.reload();
                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })
});

$("#addCheckOutRemarkBtn").click(function () {
    Dropzone.forElement('div#m-dropzone-three').removeAllFiles(true);

    funSuccessDocID = $("#DocumentId").val();
    if (($('#CheckOutPath').val() != null && $('#CheckOutPath').val() != undefined && $('#CheckOutPath').val() != "") && ($('#CheckOutFilename').val() != null && $('#CheckOutFilename').val() != undefined && $('#CheckOutFilename').val() != "")) {
        var docMapp = {
            DocumentId: funSuccessDocID,
            DocumentMappingID: DocMappingID,
            DocumentName: $('#CheckOutFilename').val(),
            MainDocumentPath: $('#CheckOutPath').val(),
            FileFormate: $('#CheckOutGetExtention').val()
        };
        $.ajax({
            url: "/DES/DOC/UpdateFileOnCheckOutMark", //If get File Then Document Mapping Add
            data: JSON.stringify(docMapp),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {
                    toastr.success(response.Msg, "Success");
                    mApp.unblockPage();

                } else {
                    toastr.error(response.Msg, "Not Success");
                    mApp.unblockPage();

                }
            },
            error: function (response) {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        });


        var CheckedOut_Remark = $('#CheckedOut_Remark').val();
        var DocumentMappingID = DocMappingID;
        var docMapp = {
            DocumentMappingID: DocumentMappingID,
            CheckedOut_Remark: CheckedOut_Remark,
        };
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/DOC/AddCheckedInOut',
            data: JSON.stringify(docMapp),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                if (response.Status) {
                    if (response.infoMsg == "true") {
                        toastr.success(response.Msg, "Success");
                    }

                    $('#CheckedOut_Remark').val("");
                    tblDocumentFiles.ajax.reload();
                    mApp.unblockPage();
                } else {
                    toastr.error(response.Msg, "Error");
                    mApp.unblockPage();
                }
            },
            error: function (error) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        })

    }
    else {
        toastr.info("Please uplode document for CheckIn", "info");
    }
});

// Update Data of Created/Date in In Life Cycle
$('#btnSubmit').click(function () {
    var docMapp = {
        DocumentId: $('#DocumentId').val(),
    };
    mApp.blockPage();
    $.ajax({
        url: "/DES/DOC/UpdateDataInLifCycle",
        data: JSON.stringify(docMapp),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            if (response) {
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentFiles.ajax.reload();
                    mApp.unblockPage();
                    if (response.RedirectAction != undefined || response.RedirectAction != null) {
                        window.location.href = response.RedirectAction;
                    }
                }
                else {
                    mApp.unblockPage();
                    toastr.info(response.Msg, "Info");
                }
            }
            else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (response) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    });

});

$('#tblDocumentFiles').on('focusout', 'td .GenralRemark', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var DocumentMappingId = data.DocumentMappingId;

    var Remarkdata = {
        Remark: $('#GenralRemark' + DocumentMappingId).val(),
        DocumentMappingId: DocumentMappingId
    };

    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/DOC/UpdateDOCRemark',
        data: JSON.stringify(Remarkdata),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                //toastr.success(response.Msg, "Success");
                $('#btnSubmit').prop('disabled', false);
                tblDocumentFiles.ajax.reload();
                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDocumentFiles != null) {
        tblDocumentFiles.columns.adjust();
    }
    if (tblDocument != null) {
        tblDocument.columns.adjust();
    }
});


$("#JEPDocumentTypeId").change(function () {
    if ($("#JEPDocumentTypeId").val() == arm) {
        var dno = $("#txtDocumentNo").val().substr(0, 30);
        var desc = $("#JEPDocumentTitle").val().substr(0, 50);
        $("#txtDocumentNo").val(dno).attr('maxlength', '30');
        $("#JEPDocumentTitle").val(desc).attr('maxlength', '50');
    }
    else {
        $("#txtDocumentNo").attr('maxlength', '200');
        $("#JEPDocumentTitle").attr('maxlength', '500');
    }
});