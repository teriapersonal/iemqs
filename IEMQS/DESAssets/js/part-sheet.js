﻿var armxhr = {
    valueField: 'Arms',
    labelField: 'Desc',
    searchField: ['Desc'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetAllARM',
            type: 'GET',
            dataType: 'json',
            data: {
                arm: query,
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    }
}

var SizeCodeChange = function () {
    var inputid = this.$input[0].id || "";
    var id = $("#" + inputid).closest("tr").attr("data-id");
    getSizeCode(id);
}

var SizeCodexhr = {
    valueField: 'SizeCode',
    labelField: 'SizeCode',
    searchField: ['SizeCode'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetAllSizeCode',
            type: 'GET',
            dataType: 'json',
            data: {
                SizeCode: query,
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    },
    onChange: SizeCodeChange
}

var SizeCodewithoutchangexhr = {
    valueField: 'SizeCode',
    labelField: 'SizeCode',
    searchField: ['SizeCode'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetAllSizeCode',
            type: 'GET',
            dataType: 'json',
            data: {
                SizeCode: query,
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    }
}


$(document).ready(function () {
    mApp.blockPage();
    $(".fullscreen-btn").click(function () {
        $(this).parents(".excel-screen").toggleClass("full-screen");
    });
    CaculateSrNo();
    CaculateItemKey();
    $("#ddlProject").select2();
    $("#ddlmanufacturingitem").select2();


    mApp.unblockPage();
});
$(function () {
    var thHeight = $("table#demo-table th:first").height();
    $("table#demo-table th").resizable({
        handles: "e",
        minHeight: thHeight,
        maxHeight: thHeight,
        minWidth: 40,
        resize: function (event, ui) {
            var sizerID = "#" + $(event.target).attr("id") + "-sizer";
            $(sizerID).width(ui.size.width);
        }
    });
});

function CaculateSrNo() {
    var list = $("#tbodyPartSheetList input[id$='__SrNo']");
    var idx = 1;
    for (var i = 0; i < list.length; i++) {
        var id = $(list[i]).closest("tr").attr("data-id");
        if (String($("#PartList_" + id + "__IsDeleted").val()).toLowerCase() !== "true") {
            $(list[i]).val(idx);
            idx = idx + 1;
        }
    }
}

function CaculateItemKey() {
    var list = $("#tbodyPartSheetList input[id$='__ItemKey']");
    for (var i = 0; i < list.length; i++) {
        var id = $(list[i]).closest("tr").attr("data-id");
        if ($("#PartList_" + id + "__ItemKeyType").val() === AutoGenerate || $("#PartList_" + id + "__ItemKeyType").val() === AutoGenerate_FindNo) {
            $(list[i]).attr("disabled", "disabled");
        }
        DdlTypeChange(id);
        ParentItemChange(id);
        IsDoubleCladChange(id);
        CalcAll(id);
    }
}

$("#tbodyPartSheetList").on("change", "select[id$='__ItemKeyType']", function () {
    var id = $(this).closest("tr").attr("data-id");
    if ($("#PartList_" + id + "__ItemKeyType").val() === AutoGenerate || $("#PartList_" + id + "__ItemKeyType").val() === AutoGenerate_FindNo) {
        $("#PartList_" + id + "__ItemKey").attr("disabled", "disabled");
    } else {
        $("#PartList_" + id + "__ItemKey").removeAttr("disabled")
    }
});


var ItemGroupChange = function () {
    //var id = $(this).closest("tr").attr("data-id");
    var id = $(this)[0].$input.closest("tr").attr("data-id");
    if ($("#PartList_" + id + "__ItemGroup").val() !== "") {
        for (var i = 0; i < ItemGroupUOMList.length; i++) {
            if (ItemGroupUOMList[i].t_citg === $("#PartList_" + id + "__ItemGroup").val()) {
                var valuom = ItemGroupUOMList[i].t_cuni;
                var kitm = ItemGroupUOMList[i].t_kitm;
                $("#PartList_" + id + "__UOM").val(valuom);
                $("#PartList_" + id + "__ManufactureItem").val(kitm);
                CalcAll(id);
                break;
            }
        }
    } else {
        $("#PartList_" + id + "__UOM").val("");
        $("#PartList_" + id + "__ManufactureItem").val("");
        CalcAll(id);
    }
}


var ProcurementDrgDocumentIdChange = function () {
    var inputid = this.$input[0].id || "";
    var id = $("#" + inputid).closest("tr").attr("data-id");
    if ($("#PartList_" + id + "__ProcurementDrgDocumentId").val() !== "") {
        $("#PartList_" + id + "__String2").val("");
        $("#PartList_" + id + "__String3").val("");
        $("#PartList_" + id + "__String4").val("");
        $("#PartList_" + id + "__BomReportSize").val("");
        $("#PartList_" + id + "__DrawingBomSize").val("");
        var v = $("#PartList_" + id + "__ProcurementDrgDocumentId option:selected").text();
        v = "Refer " + v;
        $("#PartList_" + id + "__String2").val(v);
        $("#PartList_" + id + "__BomReportSize").val(v);
        $("#PartList_" + id + "__DrawingBomSize").val(v);
    } else {
        getProductFormDescriptions(id);
    }
    CalcAll(id);
    BOMReportDrawingStrChange(id);
}

$("#tbodyPartSheetList").on("keyup change", "input[id$='__Thickness'],select[id$='__ProcurementDrgDocumentId'],input[id$='__CladPlatePartThickness1'],input[id$='__CladPlatePartThickness2'],input[id$='__Length'],input[id$='__Weight'],select[id$='__DRGNoDocumentId'], input[id$='__NumberOfPieces'],input[id$='__Quantity'],input[id$='__WeightFactor'],input[id$='__JobQty'],input[id$='__Width'],input[id$='__ItemWeight']", function () {
    var id = $(this).closest("tr").attr("data-id");
    CalcAll(id);
    BOMReportDrawingStrChange(id);
});

$("#tbodyPartSheetList").on("keyup", "input[id$='__String2'], input[id$='__String3'], input[id$='__String4']", function (e) {
    var id = $(this).closest("tr").attr("data-id");

    var code = e.keyCode || e.which;
    if (String(code) != '9') {
        if (String(this.id) == "PartList_" + id + "__String2")
            $("#PartList_" + id + "__HdnString2").val("");
        if (String(this.id) == "PartList_" + id + "__String3")
            $("#PartList_" + id + "__HdnString3").val("");
        if (String(this.id) == "PartList_" + id + "__String4")
            $("#PartList_" + id + "__HdnString4").val("");

        CalcAll(id);
        BOMReportDrawingStrChange(id);
    }
});



function getSizeCode(id) {


    if ($("#PartList_" + id + "__SizeCode").val() !== "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetSizeCodeDetailsById", //If get File Then Document Mapping Add
            data: {
                Id: $("#PartList_" + id + "__SizeCode").val()
            },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                var wf = parseFloat(response.WeightFactor).toFixed(2);
                $("#PartList_" + id + "__WeightFactor").val(wf);


                swal({
                    backdrop: false,
                    title: "You want to use String2, String3 and String4 base on Size code?",
                    type: "info",
                    allowOutsideClick: false,
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonClass: "btn btn-secondary",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No"
                }).then(
                    function (isConfirm) {
                        if (isConfirm.value) {
                            $("#PartList_" + id + "__String2").val(response.Description1);
                            $("#PartList_" + id + "__String3").val(response.Description2);
                            $("#PartList_" + id + "__String4").val(response.Description3);
                            $("#PartList_" + id + "__HdnString2").val(response.Description1);
                            $("#PartList_" + id + "__HdnString3").val(response.Description2);
                            $("#PartList_" + id + "__HdnString4").val(response.Description3);
                        }
                        $("#PartList_" + id + "__SizeCode").focus();
                    });


                CalcAll(id);
            },
            error: function (response) {

            }
        });
    } else {
        $("#PartList_" + id + "__WeightFactor").val("");
        CalcAll(id);
    }
}

$("#tbodyPartSheetList").on("change", "select[id$='__ARMSet']", function () {
    var id = $(this).closest("tr").attr("data-id");
    getDecOfARM(id);
});

function getDecOfARM(id) {
    var aid = $("#PartList_" + id + "__ARMSet").val();
    var docMapp = {
        Id: aid
    };
    if (aid != "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetARMSetDetailsById", //If get File Then Document Mapping Add
            data: docMapp,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {

                $("#PartList_" + id + "__ARMRev").val(response.Vrsn);
            },
            error: function (response) {

            }
        });
    } else {
        $("#PartList_" + id + "__ARMRev").val("");
    }
}


//$("#tbodyPartSheetList").on("change", "select[id$='__ParentPartId']", function () {
//    var id = $(this).closest("tr").attr("data-id");
//    ParentItemChange(id);
//});

var ParentPartIdChange = function () {
    var inputid = this.$input[0].id || "";
    var id = $("#" + inputid).closest("tr").attr("data-id");
    ParentItemChange(id);
    getBOMData(id);
}

function ParentItemChange(id) {
    if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
        $("#PartList_" + id + "__FindNumber").removeAttr('disabled');
        $("#PartList_" + id + "__ExtFindNumber").removeAttr('disabled');
        $("#PartList_" + id + "__FindNumberDescription").removeAttr('disabled');

        var ActionForPCRelation = $("#PartList_" + id + "__ActionForPCRelation").selectize();
        var selectizeActionForPCRelation = ActionForPCRelation[0].selectize;
        selectizeActionForPCRelation.enable();

        var ProcurementDrgDocumentId = $("#PartList_" + id + "__ProcurementDrgDocumentId").selectize({ onChange: ProcurementDrgDocumentIdChange });
        var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
        selectizeProcurementDrgDocumentId.enable();

        $("#PartList_" + id + "__Weight").removeAttr('disabled');
        $("#PartList_" + id + "__JobQty").removeAttr('disabled');
        $("#PartList_" + id + "__CommissioningSpareQty").removeAttr('disabled');
        $("#PartList_" + id + "__MandatorySpareQty").removeAttr('disabled');
        $("#PartList_" + id + "__OperationSpareQty").removeAttr('disabled');
        $("#PartList_" + id + "__ExtraQty").removeAttr('disabled');
        $("#PartList_" + id + "__Remarks").removeAttr('disabled');
        $("#PartList_" + id + "__Quantity").removeAttr('disabled');
        $("#PartList_" + id + "__Length").removeAttr('disabled');
        $("#PartList_" + id + "__Width").removeAttr('disabled');
        $("#PartList_" + id + "__NumberOfPieces").removeAttr('disabled');
        $("#PartList_" + id + "__BomReportSize").removeAttr('disabled');
        $("#PartList_" + id + "__DrawingBomSize").removeAttr('disabled');

    } else {
        $("#PartList_" + id + "__FindNumber").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__ExtFindNumber").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__FindNumberDescription").val("").attr('disabled', 'disabled');

        var ActionForPCRelation = $("#PartList_" + id + "__ActionForPCRelation").selectize();
        var selectizeActionForPCRelation = ActionForPCRelation[0].selectize;
        selectizeActionForPCRelation.disable();

        var ProcurementDrgDocumentId = $("#PartList_" + id + "__ProcurementDrgDocumentId").selectize({ onChange: ProcurementDrgDocumentIdChange });
        var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
        if ($("#PartList_" + id + "__ProcurementDrgDocumentId").val() != "")
            selectizeProcurementDrgDocumentId.setValue("", false);
        selectizeProcurementDrgDocumentId.disable();

        $("#PartList_" + id + "__Weight").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__JobQty").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CommissioningSpareQty").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__MandatorySpareQty").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__OperationSpareQty").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__ExtraQty").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__Remarks").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__Quantity").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__Length").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__Width").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__NumberOfPieces").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__BomReportSize").val("").attr('disabled', 'disabled');
        $("#PartList_" + id + "__DrawingBomSize").val("").attr('disabled', 'disabled');
    }
}


//$("#tbodyPartSheetList").on("change", "select[id$='__Type']", function () {
//    var id = $(this).closest("tr").attr("data-id");
//    TypeChange(id);
//    $("#PartList_" + id + "__IsDoubleClad").change();
//    getPolicyType(id);
//    CalcAll(id);
//});

var typeChange = function () {
    var inputid = this.$input[0].id || "";
    var id = $("#" + inputid).closest("tr").attr("data-id");
    DdlTypeChange(id);
    $("#PartList_" + id + "__IsDoubleClad").change();
    getPolicyType(id);
    CalcAll(id);
}

function DdlTypeChange(id) {

    $("#PartList_" + id + "__Thickness").removeAttr('disabled');
    $("#PartList_" + id + "__CladPlatePartThickness1").removeAttr('disabled');
    $("#PartList_" + id + "__IsDoubleClad").removeAttr('disabled');
    var IsDoubleClad = $("#PartList_" + id + "__IsDoubleClad").selectize();
    var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
    selectizeIsDoubleClad.enable()
    $("#PartList_" + id + "__CladPlatePartThickness2").removeAttr('disabled');
    $("#PartList_" + id + "__CladSpecificGravity2").removeAttr('disabled');

    if ($("#PartList_" + id + "__Type").val() === Part) {
        $("#PartList_" + id + "__Thickness").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladPlatePartThickness1").attr('disabled', 'disabled');
        $("#PartList_" + id + "__IsDoubleClad").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladPlatePartThickness2").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladSpecificGravity2").attr('disabled', 'disabled');
        $("#PartList_" + id + "__Thickness").val("");
        $("#PartList_" + id + "__CladPlatePartThickness1").val("");
        $("#PartList_" + id + "__CladPlatePartThickness2").val("");
        $("#PartList_" + id + "__CladSpecificGravity2").val("");
        var IsDoubleClad = $("#PartList_" + id + "__IsDoubleClad").selectize();
        var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
        selectizeIsDoubleClad.disable()

        var SizeCode = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.enable()

    }
    else if ($("#PartList_" + id + "__Type").val() === Plate_Part) {

        $("#PartList_" + id + "__Weight").val('');
        $("#PartList_" + id + "__CladPlatePartThickness1").attr('disabled', 'disabled');
        $("#PartList_" + id + "__IsDoubleClad").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladPlatePartThickness2").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladSpecificGravity2").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladPlatePartThickness1").val("");
        $("#PartList_" + id + "__CladPlatePartThickness2").val("");
        $("#PartList_" + id + "__CladSpecificGravity2").val("");
        var IsDoubleClad = $("#PartList_" + id + "__IsDoubleClad").selectize();
        var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
        selectizeIsDoubleClad.disable()

        var SizeCode = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.destroy();
        $("#PartList_" + id + "__SizeCode").val("");
        var SizeCode = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.disable();
        $("#PartList_" + id + "__WeightFactor").val("");
    }
    else if ($("#Type").val() === Clad_Part) {

        $("#PartList_" + id + "__Weight").val('');
        var SizeCode = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.destroy();
        $("#PartList_" + id + "__SizeCode").val("");
        var SizeCode = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.disable();
        $("#PartList_" + id + "__WeightFactor").val("");
    }
    else if ($("#PartList_" + id + "__Type").val() === Jigfix) {
        $("#PartList_" + id + "__Thickness").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladPlatePartThickness1").attr('disabled', 'disabled');
        $("#PartList_" + id + "__IsDoubleClad").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladPlatePartThickness2").attr('disabled', 'disabled');
        $("#PartList_" + id + "__CladSpecificGravity2").attr('disabled', 'disabled');
        $("#PartList_" + id + "__Thickness").val("");
        $("#PartList_" + id + "__CladPlatePartThickness1").val("");
        $("#PartList_" + id + "__CladPlatePartThickness2").val("");
        $("#PartList_" + id + "__CladSpecificGravity2").val("");
        var IsDoubleClad = $("#PartList_" + id + "__IsDoubleClad").selectize();
        var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
        selectizeIsDoubleClad.disable()
        var SizeCode = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.enable()
    }
}

function getPolicyType(id) {

    getValueOfTpye = $("#PartList_" + id + "__Type").val()

    var docMapp = {
        Type: getValueOfTpye
    };
    if (getValueOfTpye !== "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GePolicyByType", //If get File Then Document Mapping Add
            data: {
                Type: getValueOfTpye
            },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {

                $("#PartList_" + id + "__Policy").val(response.PolicyName);
                $("#PartList_" + id + "__PolicyId").val(response.PolicyId);
            },
            error: function (response) {

            }
        });
    } else {
        $("#PartList_" + id + "__Policy").val("");
        $("#PartList_" + id + "__PolicyId").val("");
    }
}
$("#tbodyPartSheetList").on("change", "select[id$='__IsDoubleClad']", function () {
    var id = $(this).closest("tr").attr("data-id");
    IsDoubleCladChange(id);
    CalcAll(id);
});
function IsDoubleCladChange(id) {
    if ($("#PartList_" + id + "__IsDoubleClad").val() === "No") {
        $("#PartList_" + id + "__CladPlatePartThickness2").attr("disabled", "disabled");
        $("#PartList_" + id + "__CladSpecificGravity2").attr("disabled", "disabled");
        $("#PartList_" + id + "__CladPlatePartThickness2").val("");
        $("#PartList_" + id + "__CladSpecificGravity2").val("");
    } else {
        $("#PartList_" + id + "__CladPlatePartThickness2").removeAttr("disabled");
        $("#PartList_" + id + "__CladSpecificGravity2").removeAttr("disabled");
    }
}

//$("#tbodyPartSheetList").on("change", "select[id$='__ProductFormCodeId']", function () {
//    var id = $(this).closest("tr").attr("data-id");
//    getProductFormDescriptions(id);
//});

var ProductFormCodeIdChanges = function () {
    var inputid = this.$input[0].id || "";
    var id = $("#" + inputid).closest("tr").attr("data-id");
    getProductFormDescriptions(id);
}

function getProductFormDescriptions(id) {
    var pfid = $("#PartList_" + id + "__ProductFormCodeId").val();
    if (pfid !== "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetProductFomDesc",
            data: { productFormId: pfid },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                $("#PartList_" + id + "__String2").val(response.Description1);
                $("#PartList_" + id + "__String3").val(response.Description2);
                $("#PartList_" + id + "__String4").val(response.Description3);
                $("#PartList_" + id + "__HdnString2").val(response.Description1);
                $("#PartList_" + id + "__HdnString3").val(response.Description2);
                $("#PartList_" + id + "__HdnString4").val(response.Description3);
                $("#PartList_" + id + "__BomReportSize").val(response.BOMReportSize);
                $("#PartList_" + id + "__DrawingBomSize").val(response.DrawingBOMSize);
                $("#PartList_" + id + "__HdnBomReportSize").val(response.BOMReportSize);
                $("#PartList_" + id + "__HdnDrawingBomSize").val(response.DrawingBOMSize);
                $("#PartList_" + id + "__ItemName").val(response.ProductForm)

                var Material = $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
                var selectizeMaterial = Material[0].selectize;
                selectizeMaterial.destroy()
                var v = '';
                $.each(response.MaterialList, function (i, v1) {
                    v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#PartList_" + id + "__Material").html(v);

                var Material = $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
                var selectizeMaterial = Material[0].selectize;
                selectizeMaterial.trigger("change");


                var Type = $("#PartList_" + id + "__Type").selectize({ onChange: typeChange });
                var selectizeType = Type[0].selectize;
                selectizeType.destroy()
                var v = '';
                $.each(response.TypeList, function (i, v1) {
                    v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#PartList_" + id + "__Type").html(v);

                var Type = $("#PartList_" + id + "__Type").selectize({ onChange: typeChange });
                var selectizeType = Type[0].selectize;
                selectizeType.trigger("change");


                mApp.unblockPage();
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $("#PartList_" + id + "__String2").val("");
        $("#PartList_" + id + "__String3").val("");
        $("#PartList_" + id + "__String4").val("");

        $("#PartList_" + id + "__HdnString2").val("");
        $("#PartList_" + id + "__HdnString3").val("");
        $("#PartList_" + id + "__HdnString4").val("");

        $("#PartList_" + id + "__BomReportSize").val("");
        $("#PartList_" + id + "__DrawingBomSize").val("");
        $("#PartList_" + id + "__HdnBomReportSize").val("");
        $("#PartList_" + id + "__HdnDrawingBomSize").val("");
        $("#PartList_" + id + "__ItemName").val("")

        var Material = $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
        var selectizeMaterial = Material[0].selectize;
        selectizeMaterial.destroy()
        var v = '';
        $("#PartList_" + id + "__Material").html(v);
        var Material = $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
        var selectizeMaterial = Material[0].selectize;
        selectizeMaterial.trigger("change");
    }
}



function GetMatrialdata(id, selected, typeselected, ProductFormCodeId) {
    if (ProductFormCodeId !== "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetProductFomDesc",
            data: { productFormId: ProductFormCodeId },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                var Material = $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
                var selectizeMaterial = Material[0].selectize;
                selectizeMaterial.destroy()
                var v = '';
                $.each(response.MaterialList, function (i, v1) {
                    if (selected === v1.Value)
                        v += "<option selected='selected' value='" + v1.Value + "'>" + v1.Text + "</option>";
                    else
                        v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });
                mApp.unblockPage();
                $("#PartList_" + id + "__Material").html(v);

                $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });

                var Type = $("#PartList_" + id + "__Type").selectize({ onChange: typeChange });
                var selectizeType = Type[0].selectize;
                selectizeType.destroy()
                var v = '';
                $.each(response.TypeList, function (i, v1) {
                    if (typeselected === v1.Value)
                        v += "<option selected='selected' value='" + v1.Value + "'>" + v1.Text + "</option>";
                    else
                        v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#PartList_" + id + "__Type").html(v);

                var Type = $("#PartList_" + id + "__Type").selectize({ onChange: typeChange });
                var selectizeType = Type[0].selectize;
                selectizeType.trigger("change");
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $("#PartList_" + id + "__String2").val("");
        $("#PartList_" + id + "__String3").val("");
        $("#PartList_" + id + "__String4").val("");

        $("#PartList_" + id + "__HdnString2").val("");
        $("#PartList_" + id + "__HdnString3").val("");
        $("#PartList_" + id + "__HdnString4").val("");

        $("#PartList_" + id + "__BomReportSize").val("");
        $("#PartList_" + id + "__DrawingBomSize").val("");
        $("#PartList_" + id + "__HdnBomReportSize").val("");
        $("#PartList_" + id + "__HdnDrawingBomSize").val("");
        $("#PartList_" + id + "__ItemName").val("")

        var Material = $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
        var selectizeMaterial = Material[0].selectize;
        selectizeMaterial.destroy()
        var v = '';
        $("#PartList_" + id + "__Material").html(v);
        $("#PartList_" + id + "__Material").selectize({ onChange: MaterialChange });
    }
}
var MaterialChange = function () {
    var inputid = this.$input[0].id || "";
    var id = $("#" + inputid).closest("tr").attr("data-id");
    var mid = $("#PartList_" + id + "__Material").val() || "";
    if (mid !== "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetItemGroupFromMaterial",
            data: { Material: mid },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                var ds = parseFloat(response.Density).toFixed(2);
                $("#PartList_" + id + "__Density").val(ds)
                var csg = parseFloat(response.CladSpecificGravity1).toFixed(2);
                $("#PartList_" + id + "__CladSpecificGravity1").val(csg);

                var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
                var selectizeItemGroup = ItemGroup[0].selectize;
                selectizeItemGroup.destroy()
                var v = '';
                if (response.ItemGroupUOM !== null && response.ItemGroupUOM !== "") {
                    var l = response.ItemGroupUOM.length;
                    if (l > 1)
                        v = "<option value>Select</option>";
                    $.each(response.ItemGroupUOM, function (i, v1) {
                        v += "<option value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                    });
                }
                mApp.unblockPage();
                $("#PartList_" + id + "__ItemGroup").html(v);
                var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
                var selectizeItemGroup = ItemGroup[0].selectize;
                selectizeItemGroup.trigger("change");
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $("#PartList_" + id + "__Density").val("")
        $("#PartList_" + id + "__CladSpecificGravity1").val("");

        var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
        var selectizeItemGroup = ItemGroup[0].selectize;
        selectizeItemGroup.destroy()
        var v = '';
        v = "<option value>Select</option>";
        $("#PartList_" + id + "__ItemGroup").html(v);
        var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
        var selectizeItemGroup = ItemGroup[0].selectize;
        selectizeItemGroup.trigger("change");
    }
};

function GetItemGroup(id, selected, Material) {
    if (Material !== "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetItemGroupFromMaterial",
            data: { Material: Material },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {

                var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
                var selectizeItemGroup = ItemGroup[0].selectize;
                selectizeItemGroup.destroy()
                var v = '';
                if (response.ItemGroupUOM !== null && response.ItemGroupUOM !== "") {
                    var l = response.ItemGroupUOM.length;
                    if (l > 1)
                        v = "<option value>Select</option>";
                    $.each(response.ItemGroupUOM, function (i, v1) {
                        if (selected === v1.t_citg)
                            v += "<option selected='selected' value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                        else
                            v += "<option value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                    });
                }
                $("#PartList_" + id + "__ItemGroup").html(v);
                $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
            },
            error: function (response) {

            }
        });
    } else {
        $("#PartList_" + id + "__Density").val("")
        $("#PartList_" + id + "__CladSpecificGravity1").val("");

        var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
        var selectizeItemGroup = ItemGroup[0].selectize;
        selectizeItemGroup.destroy()
        var v = '';
        v = "<option value>Select</option>";
        $("#PartList_" + id + "__ItemGroup").html(v);
        $("#PartList_" + id + "__ItemGroup").selectize({ onChange: ItemGroupChange });
    }
}

$("#tbodyPartSheetList").on("keyup", "input[id$='__NumberOfPieces'],input[id$='__JobQty']", function () {
    var id = $(this).closest("tr").attr("data-id");
    var type = $("#PartList_" + id + "__Type").val();
    if (type === Plate_Part || type === Clad_Part) {
        if ("PartList_" + id + "__NumberOfPieces" === this.id) {
            $("#PartList_" + id + "__JobQty").val($("#PartList_" + id + "__NumberOfPieces").val());
        } else {
            $("#PartList_" + id + "__NumberOfPieces").val($("#PartList_" + id + "__JobQty").val());
        }
        CalcAll(id)
    }
});

function CalcAll(id) {
    $("#PartList_" + id + "__ItemWeight").removeAttr("title");
    $("#PartList_" + id + "__ItemWeight").removeAttr("readonly");

    if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
        $("#PartList_" + id + "__Weight").removeAttr("readonly");
        $("#PartList_" + id + "__NumberOfPieces").removeAttr("disabled");

        $("#PartList_" + id + "__Quantity").removeAttr("readonly");
    }
    var UOM = $("#PartList_" + id + "__UOM").val();
    var Length = parseFloat($("#PartList_" + id + "__Length").val() || 0);
    var Width = parseFloat($("#PartList_" + id + "__Width").val() || 0);
    var type = $("#PartList_" + id + "__Type").val();

    if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
        if (type === Part && (UOM !== "m" && UOM !== "kg" && UOM !== "m2")) {
            $("#PartList_" + id + "__NumberOfPieces").val("");
            $("#PartList_" + id + "__NumberOfPieces").attr("disabled", "disabled");
        }

        if (UOM === "m" || UOM === "kg" || UOM === "m2") {
            $("#PartList_" + id + "__Length").removeAttr("disabled");
        } else {
            $("#PartList_" + id + "__Length").attr("disabled", "disabled");
        }

        if (UOM === "kg" || UOM === "m2") {
            $("#PartList_" + id + "__Width").removeAttr("disabled");
        } else {
            $("#PartList_" + id + "__Width").attr("disabled", "disabled");
        }
    }
    var NumberOfPieces = parseFloat($("#PartList_" + id + "__NumberOfPieces").val() || 0);



    var Density = parseFloat($("#PartList_" + id + "__Density").val() || 0);
    var Thickness = parseFloat($("#PartList_" + id + "__Thickness").val() || 0);
    var Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
    var CladPlatePartThickness1 = parseFloat($("#PartList_" + id + "__CladPlatePartThickness1").val() || 0);
    var CladPlatePartThickness2 = parseFloat($("#PartList_" + id + "__CladPlatePartThickness2").val() || 0);
    var WeightFactor = parseFloat($("#PartList_" + id + "__WeightFactor").val() || 0);
    var JobQty = parseFloat($("#PartList_" + id + "__JobQty").val() || 0);
    var isdou = $("#PartList_" + id + "__IsDoubleClad").val();



    if (type === Plate_Part) {
        if (UOM === "kg") {
            var PartWeight = parseFloat(Thickness * Density).toFixed(2);

            var equ1 = ((Length / 1000) * (Width / 1000) * NumberOfPieces * PartWeight);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Quantity").val(equ)
                $("#PartList_" + id + "__Quantity").attr("readonly", "readonly");
            }
            Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);

            var BOMWeight = Quantity;
            $("#PartList_" + id + "__ItemWeight").val(parseFloat(PartWeight).toFixed(2));
            $("#PartList_" + id + "__ItemWeight").attr("readonly", "readonly")
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
            }
        }

    }
    else if (type === Clad_Part) {
        if (UOM === "kg") {
            var CladSpecificGravity1 = parseFloat($("#PartList_" + id + "__CladSpecificGravity1").val() || 0);
            var CladSpecificGravity2 = parseFloat($("#PartList_" + id + "__CladSpecificGravity2").val() || 0);

            var PartWeight = parseFloat((Thickness * Density) + (CladPlatePartThickness1 * CladSpecificGravity1)).toFixed(2);
            if (isdou === "Yes") {
                PartWeight = parseFloat(parseFloat(PartWeight) + parseFloat((CladPlatePartThickness2 * CladSpecificGravity2))).toFixed(2);
            }

            var equ1 = ((Length / 1000) * (Width / 1000) * NumberOfPieces * PartWeight);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Quantity").val(equ)
                $("#PartList_" + id + "__Quantity").attr("readonly", "readonly");
            }
            Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);

            var BOMWeight = Quantity;
            $("#PartList_" + id + "__ItemWeight").val(parseFloat(PartWeight).toFixed(2));
            $("#PartList_" + id + "__ItemWeight").attr("readonly", "readonly")
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
            }
        }

    }
    else if (type === Part) {

        if (UOM === "m") {
            $("#PartList_" + id + "__ItemWeight").attr("title", "Note that the user has to enter kg/m in weight of the part");
            var equ1 = ((Length / 1000) * NumberOfPieces);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Quantity").val(equ)
                $("#PartList_" + id + "__Quantity").attr("readonly", "readonly");
            }
            if (WeightFactor > 0) {
                var PartWeight = parseFloat(WeightFactor * Density).toFixed(2);
                Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
                var BOMWeight = parseFloat(Quantity * PartWeight).toFixed(2)
                $("#PartList_" + id + "__ItemWeight").val(parseFloat(PartWeight).toFixed(2));
                $("#PartList_" + id + "__ItemWeight").attr("readonly", "readonly")
                if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                    $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
                }
            }
            else {
                var PartWeight = $("#PartList_" + id + "__ItemWeight").val() || 0;
                Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
                var BOMWeight = Quantity * PartWeight;
                if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                    $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
                }
            }
        }
        else if (UOM === "m2") {
            $("#PartList_" + id + "__ItemWeight").attr("title", "Note that the user has to enter kg/m2 in weight of the part");
            var equ1 = ((Length / 1000) * (Width / 1000) * NumberOfPieces);
            var equ = 0;
            if (String(equ1).indexOf('e') > 0)
                equ = parseFloat(String(equ1).substring(0, String(equ1).indexOf('e'))).toFixed(2);
            else
                equ = parseFloat(equ1).toFixed(2);
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Quantity").val(equ)
                $("#PartList_" + id + "__Quantity").attr("readonly", "readonly");
            }

            var PartWeight = $("#PartList_" + id + "__ItemWeight").val() || 0;
            Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
            var BOMWeight = Quantity * PartWeight;
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
            }
        } else if (UOM === "kg") {
            $("#PartList_" + id + "__ItemWeight").attr("title", "Note that the user has to enter kg/m in weight of the part");

            $("#PartList_" + id + "__Length").val("");
            $("#PartList_" + id + "__Width").val("");
            $("#PartList_" + id + "__Length").attr("disabled", "disabled");
            $("#PartList_" + id + "__Width").attr("disabled", "disabled");
            var Length = parseFloat($("#PartList_" + id + "__Length").val() || 0);
            var Width = parseFloat($("#PartList_" + id + "__Width").val() || 0);

            if (WeightFactor > 0) {
                var PartWeight = parseFloat(WeightFactor * Density).toFixed(2);
                Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
                var BOMWeight = parseFloat(Quantity * PartWeight).toFixed(2)
                $("#PartList_" + id + "__ItemWeight").val(parseFloat(PartWeight).toFixed(2));
                $("#PartList_" + id + "__ItemWeight").attr("readonly", "readonly")
                if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                    $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
                }
            }
            else {
                var PartWeight = $("#PartList_" + id + "__ItemWeight").val() || 0;
                Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
                var BOMWeight = Quantity * PartWeight;
                if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                    $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                    $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
                }
            }

        } else {
            var PartWeight = $("#PartList_" + id + "__ItemWeight").val() || 0;
            Quantity = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
            var BOMWeight = Quantity * PartWeight;
            if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                $("#PartList_" + id + "__Weight").val(parseFloat(BOMWeight).toFixed(2));
                $("#PartList_" + id + "__Weight").attr("readonly", "readonly")
            }
        }


    }
    else if (type === Jigfix) {

    }



}

function BOMReportDrawingStrChange(id) {
    if ($("#PartList_" + id + "__ProcurementDrgDocumentId").val() === "") {

        var s1 = $("#PartList_" + id + "__HdnString2").val() || "";
        var s2 = $("#PartList_" + id + "__HdnString3").val() || "";
        var s3 = $("#PartList_" + id + "__HdnString4").val() || "";

        var bs = $("#PartList_" + id + "__HdnBomReportSize").val();
        var ds = $("#PartList_" + id + "__HdnDrawingBomSize").val();
        var ProcDrgNo = $("#PartList_" + id + "__ProcurementDrgDocumentId option:selected").text() || "";
        var DrgNo = $("#PartList_" + id + "__DRGNoDocumentId option:selected").text() || "";

        if (PartFixList != null && PartFixList.length > 0) {
            for (var i = 0; i < PartFixList.length; i++) {
                var iid = String(PartFixList[i].Description).replace(/BOMModel_/gi, "");
                var data = $("#PartList_" + id + "__" + iid).val() || "";
                if (data != "") {
                    var regExp = new RegExp(PartFixList[i].Lookup, 'gi');
                    s1 = String(s1).replace(regExp, data);
                    s2 = String(s2).replace(regExp, data);
                    s3 = String(s3).replace(regExp, data);
                }
            }
        }

        if (ProcDrgNo != "") {
            s1 = String(s1).replace(/#ProcDrgNo#/gi, ProcDrgNo);
            s2 = String(s2).replace(/#ProcDrgNo#/gi, ProcDrgNo);
            s3 = String(s3).replace(/#ProcDrgNo#/gi, ProcDrgNo);
        }

        if (DrgNo != "") {
            s1 = String(s1).replace(/#DrgNo#/gi, DrgNo);
            s2 = String(s2).replace(/#DrgNo#/gi, DrgNo);
            s3 = String(s3).replace(/#DrgNo#/gi, DrgNo);
        }

        if (s1 != "")
            $("#PartList_" + id + "__String2").val(s1);
        if (s2 != "")
            $("#PartList_" + id + "__String3").val(s2);
        if (s3 != "")
            $("#PartList_" + id + "__String4").val(s3);


        if (PartFixList != null && PartFixList.length > 0) {
            for (var i = 0; i < PartFixList.length; i++) {
                var iid = String(PartFixList[i].Description).replace(/BOMModel_/gi, "");
                var data = $("#PartList_" + id + "__" + iid).val() || "";
                if (data != "") {
                    var regExp = new RegExp(PartFixList[i].Lookup, 'gi');
                    bs = String(bs).replace(regExp, data);
                    ds = String(ds).replace(regExp, data);
                }
            }
        }

        bs = String(bs).replace(/#ProcDrgNo#/g, ProcDrgNo);
        bs = String(bs).replace(/#DrgNo#/g, DrgNo);

        ds = String(ds).replace(/#ProcDrgNo#/g, ProcDrgNo);
        ds = String(ds).replace(/#DrgNo#/g, DrgNo);

        $("#PartList_" + id + "__BomReportSize").val(bs);
        $("#PartList_" + id + "__DrawingBomSize").val(ds);
    }
}

$("#checkALL").click(function () {
    var v = $("#checkALL").prop("checked");
    if (v) {
        $("#tbodyPartSheetList input[id$='__IsSelected']").prop("checked", true);
    } else {
        $("#tbodyPartSheetList input[id$='__IsSelected']").prop("checked", false);
    }
})

var FormItemWebSheetBegin = function (data) {
    mApp.blockPage();
    var msg = "";
    var flag = true;
    var list = $("#tbodyPartSheetList input[id$='__SrNo']");
    var datafound = false;
    for (var i = 0; i < list.length; i++) {
        var id = $(list[i]).closest("tr").attr("data-id");
        if ($("#PartList_" + id + "__ProductFormCodeId").val() !== "") {
            datafound = true;
            if (String($("#PartList_" + id + "__IsDeleted").val()).toLowerCase() !== "true") {

                if ($("#PartList_" + id + "__ItemKeyType").val() === "UserDefined") {
                    if ($("#PartList_" + id + "__ItemKey").val() === "") {
                        msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Item Id is required <br/>";
                        flag = false;
                    }
                }
                if ($("#PartList_" + id + "__Material").val() === "") {
                    msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Material is required <br/>";
                    flag = false;
                }
                if ($("#PartList_" + id + "__ItemGroup").val() === "") {
                    msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Item Group is required <br/>";
                    flag = false;
                }

                if ($("#PartList_" + id + "__ParentPartId").val() !== "") {
                    if ($("#PartList_" + id + "__FindNumber").val() === "") {
                        msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Find No is required <br/>";
                        flag = false;
                    }

                    if ($("#PartList_" + id + "__UOM").val() === "m2" || $("#PartList_" + id + "__UOM").val() === "kg") {
                        var Width = parseFloat($("#PartList_" + id + "__Width").val() || 0);
                        if (!(Width > 0)) {
                            msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Width is required <br/>";
                            flag = false;
                        }
                    }

                    var qty = parseFloat($("#PartList_" + id + "__Quantity").val() || 0);
                    if (!(qty > 0)) {
                        msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Quantity is required <br/>";
                        flag = false;
                    }
                }
            }
        }
    }

    if (datafound === false) {
        mApp.unblockPage();
        swal("No record found!!!");
        return false;
    }

    if (flag) {
        mApp.blockPage();
        return true;
    }
    else {
        mApp.unblockPage();
        swal(msg);
        return false;
    }
}
var FormItemWebSheetSuccess = function (data) {
    mApp.unblockPage();
    if (data.Status) {
        toastr.success(data.Msg, "Success");
        var msg = "";
        if (data.MaintainPart) {
            for (var i = 0; i < data.MaintainPartList.length; i++) {
                var list = $("#tbodyPartSheetList input[id$='__TempItemId']");
                for (var x = 0; x < list.length; x++) {
                    var id = $(list[x]).closest("tr").attr("data-id");
                    if (String($(list[x]).val()) === String(data.MaintainPartList[i].TempItemId)) {


                        if (data.MaintainPartList[i].MultiFindNoItemKey) {
                            msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " Item already exist with same find no <br/>";
                        }
                        else if (data.MaintainPartList[i].ItemKeyConflict !== "" && data.MaintainPartList[i].ItemKeyConflict !== null) {
                            msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " " + data.MaintainPartList[i].ItemKeyConflict + " <br/>";
                        }
                        else if (data.MaintainPartList[i].ErrorMsg !== "" && data.MaintainPartList[i].ErrorMsg !== null) {
                            msg += "SrNo:" + $("#PartList_" + id + "__SrNo").val() + " " + data.MaintainPartList[i].ErrorMsg + " <br/>";
                        } else {
                            //  $("#PartList_" + id + "__SrNo").val()
                        }
                    }
                }
            }
        }
        if (msg !== "") {
            swal(msg);
        }
    } else {
        toastr.info(data.Msg, "Info")
    }
}
var FormItemWebSheetFail = function (data) {
    mApp.unblockPage();
}
var Srno = "";
var list = $('input[id$="__IsSelected"]');
function DeleteRow() {
    var count = 0;
    for (var i = 0; i < list.length; i++) {
        if ($(list[i]).prop("checked")) {
            var id = $(list[i]).closest("tr").attr("data-id");
            var isDisplayNone = $(list[i]).closest("tr").attr('style');
            $("#PartList_" + id + "__IsDeleted").val("true");
            if (isDisplayNone !== "display: none;") {
                count++;
            }
        }
    }
    if (count === 0) {
        toastr.info("Please select atleast one record.", "Info");
    }
    else {
        swal({
            title: "Do you want to delete..?",
            type: "warning",
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonClass: "btn btn-danger",
            cancelButtonClass: "btn btn-secondary",
            confirmButtonText: "Yes",
            cancelButtonText: "No"
        }).then(
            function (isConfirm) {
                if (isConfirm.value) {
                    SrnoDelete(isConfirm, list);
                    Srno = "";
                }
            });
    }
}

function DeleteOneRow($this) {

    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var id = $($this).closest("tr").attr("data-id");
                $("#PartList_" + id + "__IsDeleted").val("true");
                $("#tr_" + id).hide();
                toastr.success("Row deleted successfully", "Success");
            }
        });

}
//$('#tbodyPartSheetList tr').on("change keyup", "select,input", function () {
//    var id = $(this).closest("tr").attr("data-id");
//    var list = $('select[id$="__ProductFormCodeId"]');
//    var flag = true;
//    for (var i = 0; i < list.length; i++) {
//        var id = $(list[i]).closest("tr").attr("data-id");
//        if ($(list[i]).val() === "" && String($("#PartList_" + id + "__IsDeleted").val()).toLowerCase() !== "true") {
//            flag = false;
//            break;
//        }
//    }
//    if (flag) {
//        $("#FormItemWebSheet").submit();
//        //AddMoreRow();
//    }
//});
function AddMoreRow() {
    var rowCount = $('#tbodyPartSheetList tr').length;
    mApp.blockPage();
    $.ajax({
        url: '/DES/Part/AddMoreRow',
        type: 'GET',
        traditional: true,
        data: { TNo: rowCount, ItemSheetId: $("#ItemSheetId").val(), Project: $("#Project").val() },
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#tbodyPartSheetList").append(result);
            CaculateSrNo();
            CaculateItemKey();
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
}


$("#tbodyPartSheetList").on("keydown", "select,input", function (event) {

    if ((event.ctrlKey || event.metaKey) && event.which === 68) {
        // Save Function
        var i = $(this).closest("tr").attr("data-id") || "";
        var id = "";
        if (i !== "") {
            for (var x = (parseInt(i) - 1); x >= 0; x--) {
                if (String($("#PartList_" + x + "__IsDeleted").val()).toLowerCase() !== "true") {
                    id = x;
                    break;
                }
            }
            if (String(id) !== "") {

                var ItemKeyType = $("#PartList_" + i + "__ItemKeyType").selectize();
                var selectizeItemKeyType = ItemKeyType[0].selectize;
                selectizeItemKeyType.setValue($("#PartList_" + id + "__ItemKeyType").val(), false)
                $("#PartList_" + i + "__ItemKeyType").change();


                var OrderPolicy = $("#PartList_" + i + "__OrderPolicy").selectize();
                var selectizeOrderPolicy = OrderPolicy[0].selectize;
                selectizeOrderPolicy.setValue($("#PartList_" + id + "__OrderPolicy").val(), false);

                var ProductFormCodeId = $("#PartList_" + i + "__ProductFormCodeId").selectize({ onChange: ProductFormCodeIdChanges });
                var selectizeProductFormCodeId = ProductFormCodeId[0].selectize;
                selectizeProductFormCodeId.destroy();
                $("#PartList_" + i + "__ProductFormCodeId").val($("#PartList_" + id + "__ProductFormCodeId").val());
                $("#PartList_" + i + "__ProductFormCodeId").selectize({ onChange: ProductFormCodeIdChanges });
                //selectizeProductFormCodeId.setValue($("#PartList_" + id + "__ProductFormCodeId").val(), false);

                var Type = $("#PartList_" + i + "__Type").selectize({ onChange: typeChange });
                var selectizeType = Type[0].selectize;
                selectizeType.destroy()
                $("#PartList_" + i + "__Type").html("");
                //var Type = $("#PartList_" + i + "__Type").selectize({ onChange: typeChange });
                //var selectizeType = Type[0].selectize;
                //selectizeType.setValue($("#PartList_" + id + "__Type").val(), false);
                //selectizeType.trigger("change");

                $("#PartList_" + i + "__ItemKey").val($("#PartList_" + id + "__ItemKey").val())
                $("#PartList_" + i + "__PolicyId").val($("#PartList_" + id + "__PolicyId").val())
                $("#PartList_" + i + "__Policy").val($("#PartList_" + id + "__Policy").val())
                //if ($("#PartList_" + id + "__ReviseWithBOM").prop("checked"))
                //    $("#PartList_" + i + "__ReviseWithBOM").prop("checked", true);


                var DRGNoDocumentId = $("#PartList_" + i + "__DRGNoDocumentId").selectize();
                var selectizeDRGNoDocumentId = DRGNoDocumentId[0].selectize;
                selectizeDRGNoDocumentId.setValue($("#PartList_" + id + "__DRGNoDocumentId").val(), false);


                var ParentPartId = $("#PartList_" + i + "__ParentPartId").selectize({ onChange: ParentPartIdChange });
                var selectizeParentPartId = ParentPartId[0].selectize;
                selectizeParentPartId.destroy();
                $("#PartList_" + i + "__ParentPartId").val($("#PartList_" + id + "__ParentPartId").val());
                $("#PartList_" + i + "__ParentPartId").selectize({ onChange: ParentPartIdChange });
                ParentItemChange(i);
                //selectizeParentPartId.setValue($("#PartList_" + id + "__ParentPartId").val(), false);

                $("#PartList_" + i + "__FindNumber").val($("#PartList_" + id + "__FindNumber").val())
                $("#PartList_" + i + "__ExtFindNumber").val($("#PartList_" + id + "__ExtFindNumber").val())
                $("#PartList_" + i + "__FindNumberDescription").val($("#PartList_" + id + "__FindNumberDescription").val())

                //$("#PartList_" + i + "__ActionForPCRelation").val($("#PartList_" + id + "__ActionForPCRelation").val())
                var ActionForPCRelation = $("#PartList_" + i + "__ActionForPCRelation").selectize();
                var selectizeActionForPCRelation = ActionForPCRelation[0].selectize;
                selectizeActionForPCRelation.setValue($("#PartList_" + id + "__ActionForPCRelation").val(), false);

                //$("#PartList_" + i + "__Material").val($("#PartList_" + id + "__Material").val())
                GetMatrialdata(i, $("#PartList_" + id + "__Material").val(), $("#PartList_" + id + "__Type").val(), $("#PartList_" + id + "__ProductFormCodeId").val())
                //var Material = $("#PartList_" + i + "__Material").selectize();
                //var selectizeMaterial = Material[0].selectize;
                //selectizeMaterial.setValue($("#PartList_" + id + "__Material").val(), false);

                //$("#PartList_" + i + "__ItemGroup").val($("#PartList_" + id + "__ItemGroup").val())
                GetItemGroup(i, $("#PartList_" + id + "__ItemGroup").val(), $("#PartList_" + id + "__Material").val())
                //var ItemGroup = $("#PartList_" + i + "__ItemGroup").selectize();
                //var selectizeItemGroup = ItemGroup[0].selectize;
                //selectizeItemGroup.setValue($("#PartList_" + id + "__ItemGroup").val(), false);

                $("#PartList_" + i + "__ItemName").val($("#PartList_" + id + "__ItemName").val())
                $("#PartList_" + i + "__String2").val($("#PartList_" + id + "__String2").val())
                $("#PartList_" + i + "__String3").val($("#PartList_" + id + "__String3").val())
                $("#PartList_" + i + "__String4").val($("#PartList_" + id + "__String4").val())

                $("#PartList_" + i + "__HdnString2").val($("#PartList_" + id + "__HdnString2").val())
                $("#PartList_" + i + "__HdnString3").val($("#PartList_" + id + "__HdnString3").val())
                $("#PartList_" + i + "__HdnString4").val($("#PartList_" + id + "__HdnString4").val())


                if ($("#PartList_" + id + "__ARMSet").val() != "") {
                    //$("#PartList_" + i + "__ARMSet").val($("#PartList_" + id + "__ARMSet").val())
                    var armxhroptions = [{ Arms: $("#PartList_" + id + "__ARMSet").val(), Desc: $("#PartList_" + id + "__ARMSet option:selected").text() }]
                    var ARMSet = $("#PartList_" + i + "__ARMSet").selectize(armxhr);
                    var selectizeARMSet = ARMSet[0].selectize;
                    selectizeARMSet.addOption(armxhroptions);
                    selectizeARMSet.setValue($("#PartList_" + id + "__ARMSet").val(), false);

                    $("#PartList_" + i + "__ARMRev").val($("#PartList_" + id + "__ARMRev").val())
                }

                //$("#PartList_" + i + "__ProcurementDrgDocumentId").val($("#PartList_" + id + "__ProcurementDrgDocumentId").val())
                var ProcurementDrgDocumentId = $("#PartList_" + i + "__ProcurementDrgDocumentId").selectize({ onChange: ProcurementDrgDocumentIdChange });
                var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
                selectizeProcurementDrgDocumentId.destroy();
                $("#PartList_" + i + "__ProcurementDrgDocumentId").val($("#PartList_" + id + "__ProcurementDrgDocumentId").val());
                $("#PartList_" + i + "__ProcurementDrgDocumentId").selectize({ onChange: ProcurementDrgDocumentIdChange });
                //selectizeProcurementDrgDocumentId.setValue($("#PartList_" + id + "__ProcurementDrgDocumentId").val(), false);

                $("#PartList_" + i + "__Weight").val($("#PartList_" + id + "__Weight").val())

                if ($("#PartList_" + id + "__SizeCode").val() != "") {
                    var SizeCodeId = $("#PartList_" + i + "__SizeCode").selectize(SizeCodexhr);
                    var selectizeSizeCodeId = SizeCodeId[0].selectize;
                    selectizeSizeCodeId.destroy();

                    $("#PartList_" + i + "__SizeCode").html("<option value='" + $("#PartList_" + id + "__SizeCode").val() + "' selected='selected'>" + $("#PartList_" + id + "__SizeCode").val() + "</option>")
                    $("#PartList_" + i + "__SizeCode").selectize(SizeCodexhr);
                }

                $("#PartList_" + i + "__Thickness").val($("#PartList_" + id + "__Thickness").val())
                $("#PartList_" + i + "__CladPlatePartThickness1").val($("#PartList_" + id + "__CladPlatePartThickness1").val())
                $("#PartList_" + i + "__CladSpecificGravity1").val($("#PartList_" + id + "__CladSpecificGravity1").val())


                IsDoubleCladChange(i)
                // $("#PartList_" + i + "__IsDoubleClad").val($("#PartList_" + id + "__IsDoubleClad").val())
                var IsDoubleClad = $("#PartList_" + i + "__IsDoubleClad").selectize();
                var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
                selectizeIsDoubleClad.setValue($("#PartList_" + id + "__IsDoubleClad").val(), false);

                $("#PartList_" + i + "__CladPlatePartThickness2").val($("#PartList_" + id + "__CladPlatePartThickness2").val())
                $("#PartList_" + i + "__CladSpecificGravity2").val($("#PartList_" + id + "__CladSpecificGravity2").val())


                // $("#PartList_" + i + "__CRSYN").val($("#PartList_" + id + "__CRSYN").val())
                var CRSYN = $("#PartList_" + i + "__CRSYN").selectize();
                var selectizeCRSYN = CRSYN[0].selectize;
                selectizeCRSYN.setValue($("#PartList_" + id + "__CRSYN").val(), false);

                $("#PartList_" + i + "__JobQty").val($("#PartList_" + id + "__JobQty").val());
                $("#PartList_" + i + "__CommissioningSpareQty").val($("#PartList_" + id + "__CommissioningSpareQty").val());
                $("#PartList_" + i + "__MandatorySpareQty").val($("#PartList_" + id + "__MandatorySpareQty").val());
                $("#PartList_" + i + "__OperationSpareQty").val($("#PartList_" + id + "__OperationSpareQty").val());
                $("#PartList_" + i + "__ExtraQty").val($("#PartList_" + id + "__ExtraQty").val());
                $("#PartList_" + i + "__Remarks").val($("#PartList_" + id + "__Remarks").val());
                $("#PartList_" + i + "__Quantity").val($("#PartList_" + id + "__Quantity").val());
                $("#PartList_" + i + "__Length").val($("#PartList_" + id + "__Length").val());
                $("#PartList_" + i + "__Width").val($("#PartList_" + id + "__Width").val());
                $("#PartList_" + i + "__NumberOfPieces").val($("#PartList_" + id + "__NumberOfPieces").val());
                $("#PartList_" + i + "__BomReportSize").val($("#PartList_" + id + "__BomReportSize").val());
                $("#PartList_" + i + "__DrawingBomSize").val($("#PartList_" + id + "__DrawingBomSize").val());
                $("#PartList_" + i + "__RevNo").val($("#PartList_" + id + "__RevNo").val());
                $("#PartList_" + i + "__UOM").val($("#PartList_" + id + "__UOM").val());
                $("#PartList_" + i + "__ItemWeight").val($("#PartList_" + id + "__ItemWeight").val());
                $("#PartList_" + i + "__Density").val($("#PartList_" + id + "__Density").val());
                $("#PartList_" + i + "__WeightFactor").val($("#PartList_" + id + "__WeightFactor").val());
                $("#PartList_" + i + "__ManufactureItem").val($("#PartList_" + id + "__ManufactureItem").val());
                $("#PartList_" + i + "__HdnBomReportSize").val($("#PartList_" + id + "__HdnBomReportSize").val());
                $("#PartList_" + i + "__HdnDrawingBomSize").val($("#PartList_" + id + "__HdnDrawingBomSize").val());
                CalcAll(i);
            }
        }
        event.preventDefault();
        return false;

    }
}
);

$("#tbodyPartSheetList").on("focusout", "input[id$='__ItemKey']", function () {
    var id = $(this).closest("tr").attr("data-id");
    var keyattr = $("#PartList_" + id + "__ItemKey").attr('readonly') || "";
    if ($("#PartList_" + id + "__ItemKey").val() !== "" && keyattr == "") {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetUserDefinedItemKey',
            type: 'GET',
            traditional: true,
            data: { ItemKey: $("#PartList_" + id + "__ItemKey").val(), Project: $("#Project").val() },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (result) {
                mApp.unblockPage();

                if (result !== '' && result !== null) {
                    var ItemKeyType = $("#PartList_" + id + "__ItemKeyType").selectize();
                    var selectizeItemKeyType = ItemKeyType[0].selectize;
                    selectizeItemKeyType.setValue(result.ItemKeyType, false)
                    $("#PartList_" + id + "__ItemKeyType").change();

                    var OrderPolicy = $("#PartList_" + id + "__OrderPolicy").selectize();
                    var selectizeOrderPolicy = OrderPolicy[0].selectize;
                    selectizeOrderPolicy.setValue(result.OrderPolicy, false);

                    var ProductFormCodeId = $("#PartList_" + id + "__ProductFormCodeId").selectize({ onChange: ProductFormCodeIdChanges });
                    var selectizeProductFormCodeId = ProductFormCodeId[0].selectize;
                    selectizeProductFormCodeId.destroy();
                    $("#PartList_" + id + "__ProductFormCodeId").val(result.ProductFormCodeId);
                    $("#PartList_" + id + "__ProductFormCodeId").selectize({ onChange: ProductFormCodeIdChanges });
                    //selectizeProductFormCodeId.setValue(result.ProductFormCodeId, false);
                    GetMatrialdata(id, result.Material, result.Type, result.ProductFormCodeId);

                    $("#PartList_" + id + "__ItemId").val(result.ItemId);

                    //var Type = $("#PartList_" + id + "__Type").selectize({ onChange: typeChange });
                    //var selectizeType = Type[0].selectize;
                    //selectizeType.setValue(result.Type, false);
                    //selectizeType.trigger("change");

                    $("#PartList_" + id + "__ItemKey").val(result.ItemKey);
                    $("#PartList_" + id + "__PolicyId").val(result.PolicyId);
                    $("#PartList_" + id + "__Policy").val(result.Policy);


                    //var Material = $("#PartList_" + id + "__Material").selectize();
                    //var selectizeMaterial = Material[0].selectize;
                    //selectizeMaterial.setValue(result.Material, false);
                    GetItemGroup(id, result.ItemGroup, result.Material)

                    //var ItemGroup = $("#PartList_" + id + "__ItemGroup").selectize();
                    //var selectizeItemGroup = ItemGroup[0].selectize;
                    //selectizeItemGroup.setValue(result.ItemGroup, false);

                    $("#PartList_" + id + "__ItemName").val(result.ItemName);
                    $("#PartList_" + id + "__String2").val(result.String2);
                    $("#PartList_" + id + "__String3").val(result.String3);
                    $("#PartList_" + id + "__String4").val(result.String4);

                    $("#PartList_" + id + "__HdnString2").val(result.HdnString2);
                    $("#PartList_" + id + "__HdnString3").val(result.HdnString3);
                    $("#PartList_" + id + "__HdnString4").val(result.HdnString4);

                    if (result.ARMSet != "" && result.ARMSet != null) {
                        var armxhroptions = [{ Arms: result.ARMSet, Desc: result.ARMSet }]
                        var ARMSet = $("#PartList_" + id + "__ARMSet").selectize(armxhr);
                        var selectizeARMSet = ARMSet[0].selectize;
                        selectizeARMSet.addOption(armxhroptions);
                        selectizeARMSet.setValue(result.ARMSet, false);
                    }

                    if (result.SizeCode != "" && result.SizeCode != null) {
                        var SizeCodeId = $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
                        var selectizeSizeCodeId = SizeCodeId[0].selectize;
                        selectizeSizeCodeId.destroy();

                        $("#PartList_" + id + "__SizeCode").html("<option value='" + result.SizeCode + "' selected='selected'>" + result.SizeCode + "</option>")
                        $("#PartList_" + id + "__SizeCode").selectize(SizeCodexhr);
                    }

                    var CRSYN = $("#PartList_" + id + "__CRSYN").selectize();
                    var selectizeCRSYN = CRSYN[0].selectize;
                    selectizeCRSYN.setValue(result.CRSYN, false);

                    $("#PartList_" + id + "__RevNo").val(result.RevNo);
                    $("#PartList_" + id + "__Thickness").val(result.Thickness);
                    $("#PartList_" + id + "__CladPlatePartThickness1").val(result.CladPlatePartThickness1);
                    $("#PartList_" + id + "__CladSpecificGravity1").val(result.CladSpecificGravity1);
                    if (result.IsDoubleClad !== "")
                        $("#PartList_" + id + "__IsDoubleClad").val(result.IsDoubleClad);
                    $("#PartList_" + id + "__CladPlatePartThickness2").val(result.CladPlatePartThickness2);
                    $("#PartList_" + id + "__CladSpecificGravity2").val(result.CladSpecificGravity2);
                    $("#PartList_" + id + "__ItemWeight").val(result.ItemWeight);
                    $("#PartList_" + id + "__ManufactureItem").val(result.ManufactureItem);
                    $("#PartList_" + id + "__ARMRev").val(result.ARMRev);
                    $("#PartList_" + id + "__Density").val(result.Density);
                    $("#PartList_" + id + "__WeightFactor").val(result.WeightFactor);
                    $("#PartList_" + id + "__UOM").val(result.UOM);
                }
                getBOMData(id);
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
});

$("#tbodyPartSheetList").on("focusout", "input[id$='__FindNumber']", function () {
    var id = $(this).closest("tr").attr("data-id");
    getBOMData(id);
});

function getBOMData(id) {
    var itmid = $("#PartList_" + id + "__ItemId").val() || 0;
    var pitmid = $("#PartList_" + id + "__ParentPartId").val() || 0;
    if (parseFloat(itmid) > 0 && parseFloat(pitmid) > 0 && $("#PartList_" + id + "__FindNumber").val() !== '') {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetDataByItemAndParentItem',
            type: 'GET',
            traditional: true,
            data: { ItemId: $("#PartList_" + id + "__ItemId").val(), ParentItemId: $("#PartList_" + id + "__ParentPartId").val(), Project: $("#Project").val(), FindNumber: $("#PartList_" + id + "__FindNumber").val() },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (result) {
                mApp.unblockPage();

                if (result !== '' && result !== null) {
                    var DRGNoDocumentId = $("#PartList_" + id + "__DRGNoDocumentId").selectize();
                    var selectizeDRGNoDocumentId = DRGNoDocumentId[0].selectize;
                    selectizeDRGNoDocumentId.setValue(result.DRGNoDocumentId, false);


                    $("#PartList_" + id + "__FindNumber").val(result.FindNumber);
                    $("#PartList_" + id + "__ExtFindNumber").val(result.ExtFindNumber);
                    $("#PartList_" + id + "__FindNumberDescription").val(result.FindNumberDescription);


                    var ProcurementDrgDocumentId = $("#PartList_" + id + "__ProcurementDrgDocumentId").selectize({ onChange: ProcurementDrgDocumentIdChange });
                    var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
                    selectizeProcurementDrgDocumentId.destroy();
                    $("#PartList_" + id + "__ProcurementDrgDocumentId").val(result.ProcurementDrgDocumentId);
                    $("#PartList_" + id + "__ProcurementDrgDocumentId").selectize({ onChange: ProcurementDrgDocumentIdChange });
                    //selectizeProcurementDrgDocumentId.setValue(result.ProcurementDrgDocumentId, false);

                    $("#PartList_" + id + "__Weight").val(result.Weight);
                    $("#PartList_" + id + "__JobQty").val(result.JobQty);
                    $("#PartList_" + id + "__CommissioningSpareQty").val(result.CommissioningSpareQty);
                    $("#PartList_" + id + "__MandatorySpareQty").val(result.MandatorySpareQty);
                    $("#PartList_" + id + "__OperationSpareQty").val(result.OperationSpareQty);
                    $("#PartList_" + id + "__ExtraQty").val(result.ExtraQty);
                    $("#PartList_" + id + "__Remarks").val(result.Remarks);
                    $("#PartList_" + id + "__Quantity").val(result.Quantity);
                    $("#PartList_" + id + "__Length").val(result.Length);
                    $("#PartList_" + id + "__Width").val(result.Width);
                    $("#PartList_" + id + "__NumberOfPieces").val(result.NumberOfPieces);
                    $("#PartList_" + id + "__BomReportSize").val(result.BomReportSize);
                    $("#PartList_" + id + "__DrawingBomSize").val(result.DrawingBomSize);
                    $("#PartList_" + id + "__HdnBomReportSize").val(result.HdnBomReportSize);
                    $("#PartList_" + id + "__HdnDrawingBomSize").val(result.HdnDrawingBomSize);
                }
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }

}



function MaterialById($this) {
    var id = $($this).closest("tr").attr("data-id");
    var val = $('#PartList_' + id + '__Material').val();
    var productfrm = $('#PartList_' + id + '__ProductFormCodeId').val();
    //var mid = $("#PartList_" + id + "__Material").val(); //PartList_0__ProductFormCodeId
    if (productfrm !== "") {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetAllMaterial',  //GetMaterialById
            type: 'GET',
            data: { Id: id, Val: val, ProductFormCodeId: productfrm },
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $("#modelbodyMaterial").html(result);
                $("#m_modal_2_4").modal("show");
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select ProductForm", "Info");
    }
}

function btnMaterialCopy($this) {
    var id = $($this).attr('data-id');
    var i = $("#MaterialSelectId").val();

    var Material = $("#PartList_" + i + "__Material").selectize({ onChange: MaterialChange });
    var selectizeMaterial = Material[0].selectize;
    selectizeMaterial.setValue(id, false);
    selectizeMaterial.trigger("change");
    $("#m_modal_2_4").modal("hide");
}


function ItemGrpById($this) {
    var id = $($this).closest("tr").attr("data-id");
    var val = $('#PartList_' + id + '__ItemGroup').val();
    var material = $('#PartList_' + id + '__Material').val();
    //var mid = $("#PartList_" + id + "__ItemGroup").val();
    if (material !== "") {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetAllItemGroup',
            type: 'GET',
            data: { Id: id, Val: val, MaterialId: material },
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $("#modelbodyItemGrp").html(result);
                $("#m_modal_2_2").modal("show");
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select Material", "Info");
    }
}

function btnItemGroupCopy($this) {
    var id = $($this).attr('data-id');
    var i = $("#ItemGroupSelectId").val();

    var ItemGroup = $("#PartList_" + i + "__ItemGroup").selectize({ onChange: ItemGroupChange });
    var selectizeItemGroup = ItemGroup[0].selectize;
    selectizeItemGroup.setValue(id, false);
    selectizeItemGroup.trigger("change");
    $("#m_modal_2_2").modal("hide");
}

function SizeCodeById($this) {
    var id = $($this).closest("tr").attr("data-id");
    var val = $('#PartList_' + id + '__SizeCode').val();
    //var mid = $("#PartList_" + id + "__SizeCodeId").val();
    //if (mid !== "") {
    if ($('#PartList_' + id + '__SizeCode').prop('disabled') == false) {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetAllSize',
            type: 'GET',
            data: { Id: id, Val: val },
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $("#modelbodySizeCode").html(result);
                $("#m_modal_2_3").modal("show");
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    } else {
        toastr.info("Size Code is not applicable", "Info");
    }
    //}
}

function btnSizeCodeCopy($this) {
    var id = $($this).attr('data-id');
    var i = $("#SizeCodeSelectId").val();

    //var SizeCode = $("#PartList_" + i + "__SizeCode").selectize({ onChange: SizeCodeChange });
    //var selectizeSizeCode = SizeCode[0].selectize;
    //selectizeSizeCode.setValue(id, false);
    //selectizeSizeCode.trigger("change");

    var SizeCodeId = $("#PartList_" + i + "__SizeCode").selectize(SizeCodexhr);
    var selectizeSizeCodeId = SizeCodeId[0].selectize;
    var SizeCodexhroptions = [{ SizeCode: id, SizeCode: id }]
    selectizeSizeCodeId.addOption(SizeCodexhroptions);
    selectizeSizeCodeId.setValue(id, false);

    $("#m_modal_2_3").modal("hide");
}


function ARMSetById($this) {
    var id = $($this).closest("tr").attr("data-id");
    var val = $('#PartList_' + id + '__ARMSet').val();
    //var mid = $("#PartList_" + id + "__ARMSet").val();
    //if (mid !== "") {
    mApp.blockPage();
    $.ajax({
        url: '/DES/Part/GetAllARMSet',
        type: 'GET',
        data: { Id: id, Val: val },
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyARMCode").html(result);
            $("#m_modal_2_1").modal("show");
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
    //}
}

function btnARMSetCopy($this) {
    var id = $($this).attr('data-id');
    var i = $("#ARMSetSelectId").val();

    var armxhroptions = [{ Arms: id, Desc: id }]
    var ARMSet = $("#PartList_" + i + "__ARMSet").selectize(armxhr);
    var selectizeARMSet = ARMSet[0].selectize;
    selectizeARMSet.addOption(armxhroptions);
    selectizeARMSet.setValue(id, false);
    $("#PartList_" + i + "__ARMSet").change();
    $("#m_modal_2_1").modal("hide");
}

$("#ddlProject").change(function () {
    var Project = $('#ddlProject').val();
    if (Project) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/Part/GetManufacturingItembyProject",
            data: { "Project": Project },
            datatype: "json",
            async: true,
            success: function (data) {

                mApp.unblockPage();
                $("#ddlmanufacturingitem").html("");
                $('#ddlmanufacturingitem').append("<option value=''>Select</option>");
                $.each(data, function (key, entry) {
                    $("#ddlmanufacturingitem").append($('<option></option>').attr('value', entry.ItemId).text(entry.ItemKey));
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
                $("#ddlmanufacturingitem").html("");
            }
        });
    }
    else {
        $("#ddlmanufacturingitem").html("");
        $("#ddlmanufacturingitem").append($('<option></option>').text('Select'));
    }
});

function PartCopy() {

    if ($("#ddlmanufacturingitem").val() !== "") {
        swal({
            title: "It may be lost your current sheet data",
            type: "info",
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonClass: "btn btn-danger",
            cancelButtonClass: "btn btn-secondary",
            confirmButtonText: "Yes",
            cancelButtonText: "No"
        }).then(
            function (isConfirm) {
                if (isConfirm.value) {
                    mApp.blockPage();
                    $.ajax({
                        type: "GET",
                        url: "/DES/Part/PartCopyToSheet",
                        data: { "ManufacturingItem": $("#ddlmanufacturingitem").val(), "ItemSheetId": $("#ItemSheetId").val() },
                        datatype: "json",
                        async: true,
                        success: function (data) {
                            toastr.success("Item copy successfully", "Success")
                            location.reload();
                        },
                        error: function () {
                            mApp.unblockPage();
                        }
                    });
                }
            });

    }
    else {
        toastr.info("Please select Manufacuring Item", "Info");
    }
}

function funExportToExcel() {
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: "/DES/Part/ExportToExcel",
        data: { "ItemSheetId": $("#ItemSheetId").val() },
        datatype: "json",
        async: true,
        success: function (data) {
            mApp.unblockPage();
        },
        error: function () {
            mApp.unblockPage();
        }
    });
}

function SrnoDelete(isConfirm, list) {
    for (var i = 0; i < list.length; i++) {
        if ($(list[i]).prop("checked")) {
            var id = $(list[i]).closest("tr").attr("data-id");
            var isDisplayNone = $(list[i]).closest("tr").attr('style');
            $("#PartList_" + id + "__IsDeleted").val("true");
            $("#tr_" + id).hide();
            if (isDisplayNone !== "display: none;") {
                Srno += "SrNo " + ++id;
                Srno += ', ';
            }
        }
    }
    toastr.success("" + Srno.slice(0, -2) + " deleted successfully ", "Success");
    list = "";
}

function ClearCache() {
    mApp.blockPage()
    $.ajax({
        type: "GET",
        url: "/DES/Part/ClearCache", //If get File Then Document Mapping Add
        data: {},
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            mApp.unblockPage();
            toastr.success("Cache clear successfully.", "Success");
        },
        error: function (response) {
            mApp.unblockPage();
        }
    });

}