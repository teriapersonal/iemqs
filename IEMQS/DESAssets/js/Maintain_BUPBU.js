﻿var tblBUPBU;

$(document).ready(function () {

    tblBUPBU = $('#tblBUPBU').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Parameter/GetBUPBUList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'asc']],
        "columnDefs": [
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "BU <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.BU + "</strong>";
                    return name;
                },
            },
            {
                "sTitle": "BU Code <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.BUCode + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "PBU <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    return row.PBU;
                }
            },
            {
                "sTitle": "PBU Code <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    return row.PBUCode;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddBURow();
        },

        "fnRowCallback": function () {
            AddBURow();
        }
        //, "preDrawCallback": function() {
        //   alert()
        //}

    });


});
function AddBURow() {
    setTimeout(function () {
        $("#BUName_0").focus();
        if ($("#BUName_0").length == 0)
            $('#tblBUPBU tbody').prepend('<tr class="v-top">'
                + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="BUName_0" name="BUName_0" placeholder="BU" /> <label class="text-danger" id="errorBUName_0" style="display:none" >BU is required </label></td> '
                + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="BUCodeName_0" name="BUCodeName_0" placeholder="BU Code" /> <label class="text-danger" id="errorBUCodeName_0" style="display:none" >BU Code is required </label></td> '
                + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="PBUName_0" name="PBUName_0" placeholder="PBU" /><label class="text-danger" id="errorPBUName_0" style="display:none" >PBU is required </label></td> '
                + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="PBUCodeName_0" name="PBUCodeName_0" placeholder="PBU Code" /><label class="text-danger" id="errorPBUCodeName_0" style="display:none" >PBU Code is required </label></td> '
                + '<td></td><td></td><td></td><td></td>'
                + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddBUPBU(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>');

    }, 1000);
}


function AddBUPBU(id) {
    //if (ValidateInput())
    $("#errorBUName_" + id).hide();
    $("#errorBUCodeName_" + id).hide();
    $("#errorPBUName_" + id).hide();
    $("#errorPBUCodeName_" + id).hide();
    var bu = $("#BUName_" + id).val();
    var bucode = $("#BUCodeName_" + id).val();
    var pbu = $("#PBUName_" + id).val();
    var pbucode = $("#PBUCodeName_" + id).val();
    if (bu != "" && bucode != "" && pbu != "" && pbucode != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/Parameter/AddBU',
            data: JSON.stringify({ BUId: id, BU: bu, BUCode: bucode, PBU: pbu, PBUCode: pbucode }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    ddlbulist = response.BUlist;
                    tblBUPBU.ajax.reload(null, false);
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (bu == "")
            $("#errorBUName_" + id).show();
        if (pbu == "")
            $("#errorPBUName_" + id).show();
        if (bucode == "")
            $("#errorBUCodeName_" + id).show();
        if (pbucode == "")
            $("#errorPBUCodeName_" + id).show();
    }

}

$('#tblBUPBU').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblBUPBU.row(tr);
    var data = row.data();
    var html = ('<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="BUName_' + data.BUId + '" name="BUName_' + data.BUId + '" placeholder="BU" value="' + data.BU + '" /> <label class="text-danger" id="errorBUName_' + data.BUId + '" style="display:none" >BU is required </label></td> '
        + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="BUCodeName_' + data.BUId + '" name="BUCodeName_' + data.BUId + '" placeholder="BU Code"  value="' + data.BUCode + '" /> <label class="text-danger" id="errorBUCodeName_' + data.BUId + '" style="display:none" >BU Code is required </label></td> '
        + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="PBUName_' + data.BUId + '" name="PBUName_' + data.BUId + '" placeholder="PBU" value="' + data.PBU + '" /><label class="text-danger" id="errorPBUName_' + data.BUId + '" style="display:none" >PBU is required </label></td> '
        + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="PBUCodeName_' + data.BUId + '" name="PBUCodeName_' + data.BUId + '" placeholder="PBU Code" value="' + data.PBUCode + '" /><label class="text-danger" id="errorPBUCodeName_' + data.BUId + '" style="display:none" >PBU Code is required </label></td> '
        + '<td></td><td></td><td></td><td></td>'
        + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddBUPBU(' + data.BUId + ')" title="Edit"><i class="la la-check text-info"></i></a>'
        + ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblBUPBUClear()" title="close"><i class="la la-close text-danger"></i></a></td>');
    $(tr).html(html);
    $("#BUName_" + data.BUId).focus();
});

function tblBUPBUClear() {
    tblBUPBU.ajax.reload(null, false);
}

$('#tblBUPBU').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblBUPBU.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    cache: false,
                    type: "GET",
                    url: "/DES/Parameter/DeleteBU",
                    data: { Id: data.BUId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblBUPBU.data().count() == 1) {
                                tblBUPBU.ajax.reload();
                            }
                            else {
                                tblBUPBU.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

