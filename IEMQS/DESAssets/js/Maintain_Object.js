﻿var tblObject;
var IsValidInput = false;

$(document).ready(function () {

    tblObject = $('#tblObject').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Parameter/GetObjectList",
        "iDisplayLength": 10,
        "ordering": true,
       
        //"responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'asc']],
        "columnDefs": [
            { "width": "150px", "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 },
            { "orderable": false, "targets": 9 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Object Name <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var ObjectName = "<strong style='text-transform: capitalize;'>" + row.ObjectName + "</strong>";
                    return ObjectName;
                }
            },
            {
                "sTitle": "Apply BU", "className": "", render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.ApplyBU == true) {
                        strHtml = "<a href='javascript:;' onclick='funUpdateApplyBU(1," + row.ObjectId + ")'><i class='la la-check'></i></a>";
                    }
                    else {
                        strHtml = "<a href='javascript:;' onclick='funUpdateApplyBU(2," + row.ObjectId + ")'><i class='la la-close'></i></a>";                        
                    }
                    return strHtml;
                }
            },
            {
                "sTitle": "BU <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    return row.BU + "-" + row.PBU;
                }
            },
            {
                "sTitle": "Category <span class='text-danger'>*</span>", "className": "text-wrap", render: function (data, type, row, meta) {
                    return row.Category;
                }
            },
            {
                "sTitle": "Action <span class='text-danger'>*</span>", "className": "text-wrap", render: function (data, type, row, meta) {
                    return row.Action;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddObjectRow();
        },

        "fnRowCallback": function () {
            AddObjectRow();
        }
    });
});

function AddObjectRow() {
    setTimeout(function () {
        $("#ObjectName_0").focus();
        if ($("#ObjectName_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar" id="ObjectName_0" placeholder="Object Name" /> <label class="text-danger" id="errorObjectName_0" style="display:none" >Object Name is required </label></td> '
                + '<td></td><td width="150px"><select class="form-control" multiple="multiple" id="BUList_0">';

        for (let i = 0; i < ddlbulist.length; i++) {
            html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        }
        html += '</select></td> '

            + '<td><select class="form-control" multiple="multiple" id="CategoryList_0">';
        for (let i = 0; i < ddlcategorylist.length; i++) {
            html += "<option value='" + ddlcategorylist[i].MappingId + "'>" + ddlcategorylist[i].Name + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorCategory_0" style="display:none" >Category is required </label></td> '

            + '<td><select class="form-control" multiple="multiple" id="ActionList_0">';
        for (let i = 0; i < ddlactionlist.length; i++) {
            html += "<option value='" + ddlactionlist[i].MappingId + "'>" + ddlactionlist[i].Name + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorAction_0" style="display:none" >Action is required </label></td> '

            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddObject(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>';

        $('#tblObject tbody').prepend(html);
        $("#BUList_0").select2();
        $("#BUList_0").select2({
            placeholder: "Select all"
        });

        $("#CategoryList_0").select2();
        $("#CategoryList_0").select2({
            placeholder: "--Select--"
        });

        $("#ActionList_0").select2();
        $("#ActionList_0").select2({
            placeholder: "--Select--"
        });

    }, 1000);
}

function AddObject(id) {
    $("#errorObjectName_" + id).hide();
    var object = $("#ObjectName_" + id).val();

    if (id == 0) {
        var buids = JSON.stringify($("#BUList_" + id).val());
    }
    else {
        var buids = "[" + $("#BUList_" + id).val() + "]";
    }
    var categoryids = JSON.stringify($("#CategoryList_" + id).val());
    var actionids = JSON.stringify($("#ActionList_" + id).val());

    if (object != "" && $("#CategoryList_" + id).val() != "" && $("#ActionList_" + id).val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/Parameter/AddObject',
            data: JSON.stringify({ ObjectId: id, ObjectName: object, BUIds: buids, CategoryIds: categoryids, ActionIds: actionids }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    ddlobjectlist = response.Objectlist;
                    tblObject.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (object == "")
            $("#errorObjectName_" + id).show();
        if ($("#CategoryList_" + id).val() == "" || $("#CategoryList_" + id).val() == undefined || $("#CategoryList_" + id).val() == NaN)
            $("#errorCategory_" + id).show();
        if ($("#ActionList_" + id).val() == "" || $("#ActionList_" + id).val() == undefined || $("#ActionList_" + id).val() == NaN)
            $("#errorAction_" + id).show();
    }
}

$('#tblObject').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblObject.row(tr);
    var data = row.data();
    var html = '<td><input type="text" maxlength="50" class="form-control blockSpecialChar" id="ObjectName_' + data.ObjectId + '" name="ObjectName_' + data.ObjectId + '" placeholder="ObjectName" value="' + data.ObjectName + '"/> <label class="text-danger" id="errorObjectName_' + data.ObjectId + '"style="display:none" >Object Name is required </label></td> '
        + '<td>' + (data.ApplyBU ? "<i class='la la-check'></i >" :"<i class='la la-close'></i >") +'</td><td>' + data.BU + '-' + data.PBU + '</td><td style="display:none"><label id="BUList_' + data.ObjectId + '">' + data.BUId + '</label></td>'

        + '<td><select class="form-control" multiple="multiple" id="CategoryList_' + data.ObjectId + '">';
    for (let i = 0; i < ddlcategorylist.length; i++) {
        html += "<option value='" + ddlcategorylist[i].MappingId + "'>" + ddlcategorylist[i].Name + "</option>";
    }
    html += '</select></br><label class="text-danger" id="errorCategory_' + data.ObjectId + '" style="display:none" >Category is required </label></td> '
  
        + '<td><select class="form-control" multiple="multiple" id="ActionList_' + data.ObjectId + '">';
    for (let i = 0; i < ddlactionlist.length; i++) {
        html += "<option value='" + ddlactionlist[i].MappingId + "'>" + ddlactionlist[i].Name + "</option>";
    }
    html += '</select></br><label class="text-danger" id="errorAction_' + data.ObjectId + '" style="display:none" >Action is required </label></td> '

        + '<td></td><td></td><td></td><td></td>'
        + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddObject(' + data.ObjectId + ')" title="Edit"><i class="la la-check text-info"></i></a>'
        + ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblObjectClear()" title="close"><i class="la la-close text-danger"></i></a></td>';

    $(tr).html(html);

    var selectedValuesCategoryId;
    if (data.CategoryId) {
        selectedValuesCategoryId = data.CategoryId.split(',');
        var Catids = [];
        for (var i = 0; i < selectedValuesCategoryId.length; i++) {
            Catids.push(String(selectedValuesCategoryId[i].trim()));
        }
        
        $('#CategoryList_' + data.ObjectId).select2();
        $('#CategoryList_' + data.ObjectId).val(Catids).trigger('change');

    }
  
    var selectedValuesActionId;
    if (data.ActionId) {
        selectedValuesActionId = data.ActionId.split(',');
        var Actids = [];
        for (var i = 0; i < selectedValuesActionId.length; i++) {
            Actids.push(String(selectedValuesActionId[i].trim()));
        }
        $('#ActionList_' + data.ObjectId).select2();
        $('#ActionList_' + data.ObjectId).val(Actids).trigger('change');
    }

    $('#BUList_' + data.ObjectId).val(data.BUId);
    $("#ObjectName_" + data.ObjectId).focus();
});

function tblObjectClear() {
    tblObject.ajax.reload();
}

$('#tblObject').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblObject.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Parameter/DeleteObject',
                    data: { Id: data.ObjectId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblObject.data().count() == 1) {
                                tblObject.ajax.reload();
                            }
                            else {
                                tblObject.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

function funUpdateApplyBU(UpdateFlag, id) {
    var valApplyBU = false;
    if (UpdateFlag == 2) {
        valApplyBU = true;
    }
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: '/DES/Parameter/UpdateApplyBU',
        data: { Id: id, ApplyBU: valApplyBU },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                tblObject.ajax.reload();
            } else {
                toastr.info(response.Msg, "Info");
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}


