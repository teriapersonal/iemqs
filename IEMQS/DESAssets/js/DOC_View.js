﻿
var tblDocumentById;
var DocMappingID;
var DocId;
$(document).ready(function () {
    getCurrentUser = $('#CurrentPsNo').val();
    mApp.blockPage();
    // Document Files Mapping Table
    tblDocumentById = $('#tblDocumentById').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DOC/Detail",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 },
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DocId", value: $("#DocumentId").val() });
            data.push({ name: "currentUser", value: $('#GetCurrentUser').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {

                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    if (row.Authorized > 0) {
                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDocVersions" title="DOC Versions"><i class="fa fa-search-plus m--font-brand"></i></a>';
                        if ($('#IsDocDownloadRights').val() == "True" && $('#IsProjectRights').val() == "True") {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                        }

                        if (row.DocStatus == "Created" || row.DocStatus == "Draft") {

                            if ((row.IsDOCCheckIn == true)) {
                                if (row.DOCCheckInBy == getCurrentUser) {
                                    if (row.CheckInStatus == null || (row.CheckInStatus == false)) {
                                        //srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnCheckIn" title="CheckIn"><i class="fa fa-lock-open m--font-success"></i></button>';
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckIn" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                        srt += ' <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDFM" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                    }
                                    else if (row.CheckOutStatus == false && (row.CheckInStatus == true && row.CheckInBy == true)) {

                                        //srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut" ><i class="fa fa-lock m--font-danger"></i></button>';
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckIn" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                    }
                                }
                            }
                            else {
                                if (row.CheckInStatus == null || (row.CheckInStatus == false)) {
                                    //srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnCheckIn" title="CheckIn"><i class="fa fa-lock-open m--font-success"></i></button>';
                                    srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckIn" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                    srt += ' <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDFM" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                }
                                else if (row.CheckOutStatus == false && (row.CheckInStatus == true && row.CheckInBy == true)) {

                                    //srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut" ><i class="fa fa-lock m--font-danger"></i></button>';
                                    srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckIn" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                }
                            }
                        }
                    }
                    return srt;
                }
            },
            {
                "sTitle": "DMId", "className": "", "visible": false, render: function (data, type, row, meta) {
                    return row.DocumentMappingId;
                }
            },
            {
                "sTitle": "Document Name", "className": "", render: function (data, type, row, meta) {
                    return row.Document_name;
                },
            },
            {
                "sTitle": "Ver", "orderable": true, render: function (data, type, row, meta) {
                    return row.FileVersion;
                }
            },
            {
                "sTitle": "Main Document", render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.DocStatus == "InProcess" || row.DocStatus == "Completed") {
                        if (row.IsMainDocument.toString().toLowerCase() === "true") {
                            strHtml = "<input name='IsMDoc' type='checkbox' checked Id='IsMainDoc'  disabled  class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + "  />";
                        }
                        else {
                            strHtml = "<input name='IsMDoc'  type='checkbox' Id='IsMainDoc' disabled class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + " />";
                        }
                    }
                    else {
                        if (row.IsMainDocument.toString().toLowerCase() === "true") {
                            strHtml = "<input name='IsMDoc' type='checkbox' checked Id='IsMainDoc' disabled onclick='isMainDisable($(this));' class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + "  />";
                        }
                        else {
                            strHtml = "<input name='IsMDoc'  type='checkbox' Id='IsMainDoc' disabled onclick='isMainDisable($(this));' class='m-checkbox m-checkbox--solid m-checkbox--success' value=" + row.IsMainDocument + " />";
                        }
                    }
                    return strHtml;
                }
            },
            {
                "sTitle": "Remark", "orderable": false, render: function (data, type, row, meta) {
                    var strHtml = '';
                    //if (row.DocStatus == "Completed") {
                    //    if (row.Remark == null || row.Remark == "undifined") {
                    //        strHtml = "<textarea name='GenralRemark' disabled  style='width:150px !important' type='text' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'   />";
                    //    }
                    //    else {
                    //        strHtml = "<textarea name='GenralRemark' disabled type='text' style='width:150px !important' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'  >" + row.Remark + "</textarea>";
                    //    }

                    //}
                    // else {

                    if (row.Remark == null || row.Remark == "undifined") {
                        strHtml = "<textarea name='GenralRemark'  style='width:150px !important' disabled type='text' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'   />";
                    }
                    else {
                        strHtml = "<textarea name='GenralRemark' style='width:150px !important' disabled  type='text' id='GenralRemark" + row.DocumentMappingId + "'  class='form-control m-input  m-input--square GenralRemark'  >" + row.Remark + "</textarea>";
                    }
                    //   }
                    return strHtml;
                },
            },
            {
                "sTitle": "Status", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.Status;
                },
            },
            {
                "sTitle": "Formate", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.FileFormate;
                },
            },


            {
                "sTitle": "DocumentId", "visible": false, "orderable": false, render: function (data, type, row, meta) {
                    return row.DocumentId;
                }
            },
            {
                "sTitle": "Main DocumentPath", "className": "", "visible": false, render: function (data, type, row, meta) {
                    return row.MainDocumentPath;
                }
            },

            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", "visible": false, render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", "visible": false, render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblDocumentById.columns.adjust();
        }

    });
    mApp.unblockPage();
});
$('#tblDocumentById').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentById.row(tr);
    var data = row.data();
    //var DocPath = data.MainDocumentPath;
    UniverSalFileWTAnnotation(data.MainDocumentPath, data.DocumentMappingId, $('#CurrentPsNo').val(), data.DocumentNo, data.DocumentId);
});


//Delete From Document Table

$('#tblDocumentById').on('click', 'td .btnDeleteDFM', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentById.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";
    var documentName = data.Document_name;
    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                var docdelete = {
                    deletePath: data.MainDocumentPath
                };
                if ($('#CurrentLocationIp').val() != "") {
                    $.ajax({
                        url: FCSurl,
                        data: JSON.stringify(docdelete),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        cache: false,
                        async: true,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function (response) {
                            if (response.DeleteDOC) {
                                $.ajax({
                                    type: "GET",
                                    url: '/DES/DOC/DeleteDocFile',
                                    data: { Id: data.DocumentMappingId },
                                    dataType: 'json',
                                    contentType: "application/json;charset=utf-8",
                                    async: true,
                                    success: function (response) {
                                        toastr.success(documentName + response.Msg, "Success");
                                        tblDocumentById.ajax.reload();
                                        mApp.unblockPage();
                                    },
                                    error: function (error) {
                                        toastr.error("Error while removing file from grid", "Error");
                                        mApp.unblockPage();
                                    }
                                })
                            }
                            else {
                                toastr.error("Error while removing file from", "Error");
                                mApp.unblockPage();
                            }
                        },
                        error: function (error) {
                            toastr.error("Error while removing file", "Error");
                            mApp.unblockPage();
                        }
                    })
                }
                else {
                    toastr.error("Ip is not detecting", "Error");
                    mApp.unblockPage();
                }
            } else {
                mApp.unblockPage();
            }
        });
});

$('#tblDocumentById').on('click', 'td .btnCheckIn', function () {
    $('#CheckInDocumentPath').val('');
    var tr = $(this).closest('tr');
    var row = tblDocumentById.row(tr);
    var data = row.data();
    DocMappingID = data.DocumentMappingId;
    $('#CheckInDocumentPath').val(data.MainDocumentPath);
    $('#myCehckInMarkModal').modal({ backdrop: 'static', keyboard: false });
    $('#myCehckInMarkModal').modal('show');
});

$('#tblDocumentById').on('click', 'td .btnCheckOut', function () {

    var tr = $(this).closest('tr');
    var row = tblDocumentById.row(tr);
    var data = row.data();
    DocMappingID = data.DocumentMappingId;
    DocId = data.DocumentId;
    $('#myCehckOutMarkModal').modal({ backdrop: 'static', keyboard: false });
    $('#myCehckOutMarkModal').modal('show');
    $('#CheckedOut_Remark').val('');
    myDropzoneThree.removeAllFiles();
    $('#CheckOutFilename').val('');
    $('#CheckOutPath').val('');
    $('#CheckOutGetExtention').val('');
});

$('#tblDocumentById').on('click', 'td .btnDownload', function () {
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var tr = $(this).closest('tr');
    var row = tblDocumentById.row(tr);
    var data = row.data();

    downloadFile(data.MainDocumentPath, CurrentLocationIp);

});

$('#tblDocumentById').on('click', 'td .btnDocVersions', function () {

    var tr = $(this).closest('tr');
    var row = tblDocumentById.row(tr);
    var data = row.data();
    var docMapping = {
        VersionGUID: data.VersionGUID,
        DocumentId: data.DocumentId

    };
    mApp.blockPage();
    $.ajax({
        url: '/DES/DOC/GetAllVersion',
        data: JSON.stringify(docMapping),
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyAllVersion").html(result);
            $("#m_modal_1_2").modal("show");
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
});

$('#tblDocumentById').on('focusout', 'td .GenralRemark', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentById.row(tr);
    var data = row.data();
    var DocumentMappingId = data.DocumentMappingId;


    var getCountOfText = $('#GenralRemark' + DocumentMappingId).val().length;
    if (getCountOfText < 501) {

        var Remarkdata = {
            Remark: $('#GenralRemark' + DocumentMappingId).val(),
            DocumentMappingId: DocumentMappingId
        };

        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/DOC/UpdateDOCRemark',
            data: JSON.stringify(Remarkdata),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                if (response.Status) {
                    // toastr.success(response.Msg, "Success");
                    tblDocumentById.ajax.reload();
                    mApp.unblockPage();
                } else {
                    toastr.error(response.Msg, "Error");
                    mApp.unblockPage();
                }
            },
            error: function (error) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        })

    }
    else {
        toastr.info("Remark text must be less then 500 character", "Info");
        $('#GenralRemark' + DocumentMappingId).val('');
    }
});

$("#btnReturn").click(function () {
    $('#Return_Remark').val("");
    $('#returnRemark').modal({ backdrop: 'static', keyboard: false })
    $('#returnRemark').modal('show');
});

$("#addReturnBtn").click(function () {
    var documentId = $('#DocumentId').val();
    var returnRemark = $('#Return_Remark').val();

    var addReturnRemark = {
        DocumentId: documentId,
        ReturnNotes: returnRemark,
    };
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/DOC/AddReturnRemarks',
        data: JSON.stringify(addReturnRemark),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                if (response.RedirectAction != null || response.RedirectAction != undefined) {
                    toastr.success(response.Msg, "Success");
                    window.location.href = response.RedirectAction;
                }
                else {
                    toastr.info(response.Msg, "Info");
                }
                $('#Return_Remark').val("");
                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })
});

$("#addRemarkBtn").click(function () {
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var mainDocPathVal = $('#CheckInDocumentPath').val();
    downloadFile(mainDocPathVal, CurrentLocationIp);
    var CheckedIn_Remark = $('#CheckedIN_Remark').val();
    var DocumentMappingID = DocMappingID;

    var checkedIn = {
        DocumentMappingID: DocumentMappingID,
        CheckedIn_Remark: CheckedIn_Remark,
    };
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/DOC/AddCheckedInOut',
        data: JSON.stringify(checkedIn),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                if (response.infoMsg == "true") {
                    toastr.success(response.Msg, "Success");
                }
                $('#CheckedIN_Remark').val("");
                tblDocumentById.ajax.reload();
                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })

});


var regex = /\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;
var myDropzoneThree = new Dropzone('div#m-dropzone-three', {

    timeout: 18000000,
    parallelUploads: 1000,
    maxFilesize: 200000,
    maxFiles: 1,
    accept: function (file, done) {
        var fileName = regex.test(file.name);
        if (fileName) {
            toastr.info(file.name + " Contains special characters", "Info");
            myDropzoneThree.removeFile(file);
        }
        else {
            var dotNameSplit = file.name.split('.');
            if (dotNameSplit.length > 2) {
                toastr.info(file.name + " Contains special characters", "Info");
                myDropzoneThree.removeFile(file);
            }
            else {
                done();
            }
        }
    },
    init: function () {
        this.on("maxfilesexceeded", function (file) {
            alert("No moar files please!");
            myDropzoneThree.removeFile(file);
            myDropzoneThree.removeAllFiles();
        });
    }

});

myDropzoneThree.on('sending', function (file, xhr, formData) {
    $('.dz-error-message').css('display', 'none');
    $('.dz-success-mark').css('display', 'block');
    $('.dz-error-mark').css('display', 'none');

    var concatDocandProject;
    var DocumentMappingID = DocMappingID;
    var DocumentId = $('#DocumentId').val();
    var Project = $('#Project').val();
    var DocumentNo = $('#DocumentNo').val();
    var Revision = $('#DocumentRevision').val();

    if ($('#CurrentLocationIp').val() != "") {
        var CurrentLocationIp = $('#CurrentLocationIp').val();
        var CurrentLocationPath = $('#FilePath').val();
        var FCSurl = CurrentLocationIp + "/api/Upload";
        if (DocumentId != null) {
            concatDocandProject = Project + "-" + DocumentNo + "-R" + Revision;
        }
        formData.append('SavedPath', CurrentLocationPath.toString());
        formData.append('DocumentPath', concatDocandProject.toString());
        formData.append('DocumentFile', file);
        window.onbeforeunload = function () {
            var prevent_leave = true;
            if (prevent_leave) {
                return "Your files are not completely uploaded yet...";
            }
        }
        mApp.blockPage();
        jQuery.ajax({
            url: FCSurl,
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function (response) {
                if (response.Uploded) {
                    var fname = response.SavedFileName;
                    $('#CheckOutFilename').val(response.OriginalFileName);
                    $('#CheckOutPath').val(response.SavedPath + "/" + fname);
                    $('#CheckOutGetExtention').val(response.OriginalFileName.split('.')[1]);
                }
                else {
                    toastr.error("Fial to File Uploaded to Server", "Error");
                    myDropzoneThree.removeFile(file);
                    mApp.unblockPage();
                }
                window.onbeforeunload = function () {
                    var prevent_leave = false;
                    if (prevent_leave) {
                        return "Your files are not completely uploaded...";
                    }
                }
                mApp.unblockPage();
            },
        }).fail(function (data) {
            toastr.error("Server response not received", "Error");
            myDropzoneThree.removeFile(file);
            window.onbeforeunload = function () {
                var prevent_leave = false;
                if (prevent_leave) {
                    return "Your files are not completely uploaded...";
                }
            }
            mApp.unblockPage();
        });
    }
    else {
        toastr.error("Ip is not detecting", "Error");
    }
});

//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 100;  //time in ms (3 seconds)

$('#CheckedOut_Remark').on('keyup', function () {
    clearTimeout(typingTimer);
    if ($('#CheckedOut_Remark').val()) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }
    function doneTyping() {
        var getCountOfCheckedOut_Remark = $('#CheckedOut_Remark').val().length;
        if (getCountOfCheckedOut_Remark > 51) {
            toastr.info("Remark text must be less then 50 character", "Info");
            $('#CheckedOut_Remark').val('');
        }
    }
});

$('#CheckedIN_Remark').on('keyup', function () {
    clearTimeout(typingTimer);
    if ($('#CheckedIN_Remark').val()) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }
    function doneTyping() {
        var getCountOfCheckedIN_Remark = $('#CheckedIN_Remark').val().length;
        if (getCountOfCheckedIN_Remark > 51) {
            toastr.info("Remark text must be less then 50 character", "Info");
            $('#CheckedIN_Remark').val('');
        }
    }
});


$("#addCheckOutRemarkBtn").click(function () {

    Dropzone.forElement('div#m-dropzone-three').removeAllFiles(true);

    if (($('#CheckOutPath').val() != null && $('#CheckOutPath').val() != undefined && $('#CheckOutPath').val() != "") && ($('#CheckOutFilename').val() != null && $('#CheckOutFilename').val() != undefined && $('#CheckOutFilename').val() != "")) {
        var docMapp = {
            DocumentId: $('#DocumentId').val(),
            DocumentMappingID: DocMappingID,
            DocumentName: $('#CheckOutFilename').val(),
            MainDocumentPath: $('#CheckOutPath').val(),
            FileFormate: $('#CheckOutGetExtention').val()
        };
        $.ajax({
            url: "/DES/DOC/UpdateFileOnCheckOutMark", //If get File Then Document Mapping Add
            data: JSON.stringify(docMapp),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {
                    toastr.success(response.Msg, "Success");
                    mApp.unblockPage();

                } else {
                    toastr.error(response.Msg, "Not Success");
                    mApp.unblockPage();

                }
            },
            error: function (response) {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        });


        var getCountOfCheckedOut_Remark = $('#CheckedOut_Remark').val().length;
        if (getCountOfCheckedOut_Remark < 51) {
            var CheckedOut_Remark = $('#CheckedOut_Remark').val();
            var DocumentMappingID = DocMappingID;
            var docMapp = {
                DocumentMappingID: DocumentMappingID,
                CheckedOut_Remark: CheckedOut_Remark,
            };
            mApp.blockPage();
            $.ajax({
                type: "POST",
                url: '/DES/DOC/AddCheckedInOut',
                data: JSON.stringify(docMapp),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                async: true,
                success: function (response) {
                    if (response.Status) {
                        toastr.success(response.Msg, "Success");
                        $('#CheckedOut_Remark').val("");
                        tblDocumentById.ajax.reload();
                        mApp.unblockPage();
                    } else {
                        toastr.error(response.Msg, "Error");
                        mApp.unblockPage();
                    }
                },
                error: function (error) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }
            })
        }
        else {
            toastr.info("Remark text must be less then 50 character", "Info");
            $('#CheckedOut_Remark').val('');
        }
    }
    else {
        toastr.info("Please uplode document for CheckIn", "info");

    }
});

//This stand for CheckBox change event in Grid
function isMainDisable(t) {

    if (t.is(':checked')) {
        var DocumentMappingId = tblDocumentById.row(t.closest('tr')).data().DocumentMappingId;
        var IsMainDocument = "True";
        var docMapp = {
            DocumentMappingId: DocumentMappingId,
            IsMainDocument: IsMainDocument
        };
        mApp.blockPage();
        $.ajax({
            url: "/DES/DOC/UpdateMainFileStatus",
            data: JSON.stringify(docMapp),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentById.ajax.reload();
                    mApp.unblockPage();
                }
                else {
                    toastr.info(response.Msg, "Info");
                    mApp.unblockPage();
                }
            },
            error: function (response) {
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        var DocumentMappingId = tblDocumentById.row(t.closest('tr')).data().DocumentMappingId;
        var IsMainDocument = "False";
        var docMapp = {
            DocumentMappingId: DocumentMappingId,
            IsMainDocument: IsMainDocument
        };
        mApp.blockPage();
        $.ajax({
            url: "/DES/DOC/UpdateMainFileStatus",
            data: JSON.stringify(docMapp),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                if (response) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentById.ajax.reload();
                    mApp.unblockPage();
                } else {
                    toastr.info(response.Msg, "Info");
                    mApp.unblockPage();
                }
            },
            error: function (response) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        });
    }
}


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDocumentById != null)
        tblDocumentById.columns.adjust();
});