﻿var tblFavoriteProject;
$(document).ready(function () {

    tblFavoriteProject = $('#tblFavoriteProject').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Dashboard/GetFavoriteList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[6, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 }

        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "", "className": "dt-right fav-project", render: function (data, type, row, meta) {
                    if (row.IsFavorite == true) {
                        return '<label class="m-checkbox m-checkbox--solid"><input checked="checked" type="checkbox" id="IsSelect_' + row.Project + '"  data-id="' + row.Project + '" onclick="AddFavoriteProject(this)"><span></span></label>';
                    }
                    else {
                        return '<label class="m-checkbox m-checkbox--solid"><input type="checkbox" id="IsSelect_' + row.Project + '"  data-id="' + row.Project + '" onclick="AddFavoriteProject(this)"><span></span></label>';
                    }
                }
            },
             {
                 "sTitle": "BU", "className": "", render: function (data, type, row, meta) {
                     return row.BU;
                 }
             },
            {
                "sTitle": "Name", "className": "", render: function (data, type, row, meta) {
                    var str = '<a href="/DES/Project/Details?ProjectNo=' + row.Project + '">' + row.Project + '</a >';
                    return str;
                }
            },
             {
                 "sTitle": "Project Description", "className": "", render: function (data, type, row, meta) {
                     return row.ProjectDescription;
                 }
             },
              {
                  "sTitle": "Customer Name", "className": "", render: function (data, type, row, meta) {
                      return row.Customer;
                  }
              },
               {
                   "sTitle": "Owner", "className": "", render: function (data, type, row, meta) {
                       return row.Owner;
                   }
               },
                 {
                     "sTitle": "Category", "className": "", render: function (data, type, row, meta) {
                         return row.OrderCategory;
                     }
                 },






            {
                "sTitle": "PO Date", "className": "", render: function (data, type, row, meta) {
                    return row.PODate;
                }
            },
            {
                "sTitle": "LOI/FOA Date", "className": "", render: function (data, type, row, meta) {
                    return row.LOIDate;
                }
            },
            {
                "sTitle": "CDD", "className": "", render: function (data, type, row, meta) {
                    return row.ContractualDeliveryDate;
                }
            },
            {
                "sTitle": "Enquiry", "className": "", render: function (data, type, row, meta) {
                    return row.EnquiryName;
                }
            },
            {
                "sTitle": "Zero Date", "className": "", render: function (data, type, row, meta) {
                    return row.ZeroDate;
                }
            },
             {
                 "sTitle": "Status", "className": "", render: function (data, type, row, meta) {
                     return row.Status;
                 }
             }

        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblFavoriteProject.columns.adjust();
        }

    });
});


//$("#tblFavoriteProject").on("click", "#IsSelect", function () {
//    debugger;
//    var tr = $(this).closest('tr');
//    var row = tblFavoriteProject.row(tr);
//    var data = row.data();
//    console.log(data)
//    var html = '<td>' + data.FavoriteId + '</td> '
//        //+ '<td><label class="m-checkbox m-checkbox--solid"><input type="checkbox" id="IsSelect" onclick="AddFavoriteProject(' + data.FavoriteId + ')"><span></span></label></td> ';
//    $(tr).html(html);   
//});

function AddFavoriteProject($this) {
    var id = $($this).attr("data-id");
    var select = $("#IsSelect_" + id).prop("checked");
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/Dashboard/AddFavoriteProject',
        data: JSON.stringify({ Project: id, IsFavorite: select }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.success(response.Msg, "Success");
            } else {
                toastr.error(response.Msg, "Error");
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

