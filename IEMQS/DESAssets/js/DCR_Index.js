﻿var tblDocumentList;
var tblAffectedPart;

$(document).ready(function () {
    RelatedDCR();
    //GetDCRAndApart();
});

function GetDCRAndApart(dcrid) {
    if (tblDocumentList != null)
        $("#tblDocumentList").DataTable().destroy();
    if (tblAffectedPart != null)
        $("#tblAffectedPart").DataTable().destroy();
    GetAffectedDCRDoc(dcrid);
    GetAffectedDCRPart(dcrid);
}

function GetAffectedDCRDoc(dcrid) {
    setTimeout(function () {
        tblDocumentList = $('#tblDocumentList').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetAffectedDCRDoc",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: dcrid });
                data.push({ name: "IsInCorporate", value: false });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = "";
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        return row.DocNo
                        //var name = "<a target='_blank' href='/DES/DOC/Detail?q=" + row.QString + "'>" + row.DocNo + "</a>";
                        //return name;
                    }
                },
                {
                    "sTitle": "Document Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        var srt = '';
                        if (row.RevisionNo != null || row.RevisionNo != '') {
                            srt = 'R' + row.RevisionNo;
                        }
                        return srt;
                    }
                },
                {
                    "sTitle": "General Remarks", "className": "", render: function (data, type, row, meta) {
                        return row.GeneralRemarks
                    }
                }
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

function GetAffectedDCRPart(dcrid) {
    setTimeout(function () {
        tblAffectedPart = $('#tblAffectedPart').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetDCRAffectedPart",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: dcrid });
                data.push({ name: "IsInCorporate", value: false });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                 {
                     "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                         var srt = "";
                         if (row.DocumentNo != null) {
                             srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                         }
                         return srt;
                     }
                 },
                {
                    "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                        return row.Type
                    },
                },
                {
                    "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                        return row.ItemKey
                        //var name = "<a target='_blank' href='/DES/Part/Detail?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                        //return name;
                    },
                },
                {
                    "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                        return row.Parent_Item
                    }
                },
                {
                    "sTitle": "ItemName", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        return "R" + row.RevNo
                    }
                },
                {
                    "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                        return row.DocumentNo
                    }
                },
                {
                    "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                        return row.Material
                    }
                },
                {
                    "sTitle": "ItemGroup", "className": "", render: function (data, type, row, meta) {
                        return row.ItemGroup
                    }
                },
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

function RelatedDCR() {
    tblDCRList = $('#tblDCRList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/Index",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[2, 'desc']],
        'columnDefs': [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 9 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
        ],
        'select': {
            'style': 'multi'
        },
        "fnServerData": function (sSource, data, fnCallback) {
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if ((row.Authorized > 0) || row.CreatedBy == $('#CurrentUser').val()) {
                        if (IsInprocess.toLowerCase() == 'false') {
                            srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                            srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditPart"  data-toggle="modal" data-target="#m_modal_4" onclick="GetDCRAndApart(' + row.DCRId + ')"><i class="la la-files-o " title="Affected Part/Doc"></i></a>';
                        }
                        else {
                            if (row.Status == "Created") {
                                if (row.CreatedBy == psno) {
                                    srt += '<a href="/DES/DCR/Edit?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="la la-edit text-brand"></i></a>';
                                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnCancel" title="Cancel"><i class="la la-close text-info"></i></a>';
                                } 
                            }
                            else {
                                srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                                srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditPart"  data-toggle="modal" data-target="#m_modal_4" onclick="GetDCRAndApart(' + row.DCRId + ')"><i class="la la-files-o " title="Affected Part/Doc"></i></a>';
                            }
                        }
                    }
                    else {
                        srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                        srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditPart"  data-toggle="modal" data-target="#m_modal_4" onclick="GetDCRAndApart(' + row.DCRId + ')" title="Affected Part/Doc"><i class="la la-files-o"></i></a>';
                    }
                    return srt;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    return 'DCR'
                },
            },
            {
                "sTitle": "Name", "className": "", render: function (data, type, row, meta) {
                    return row.DCRNo
                },
            },
            {
                "sTitle": "State", "className": "", render: function (data, type, row, meta) {
                    var str = '';
                    if (row.Status == "Created") {
                        str = 'Draft';
                    }
                    else {
                        str = row.Status;
                    }
                    return str;
                }
            },
            {
                "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                    return row.DCRDescription
                },
            },
             {
                 "sTitle": "Affected Documents", "className": "", render: function (data, type, row, meta) {
                     return row.AffectedDocuments
                 },
             },
              {
                  "sTitle": "Affected Parts", "className": "", render: function (data, type, row, meta) {
                      return row.AffectedParts
                  },
              },
               {
                   "sTitle": "Creation Date", "className": "", render: function (data, type, row, meta) {
                       return row.CreatedOn
                   },
               },
                {
                    "sTitle": "Issue Date", "className": "", "orderable": false, render: function (data, type, row, meta) {
                        return row.DCRIssueDate
                    },
                },
                 {
                     "sTitle": "Completion Date", "className": "", render: function (data, type, row, meta) {
                         return row.DCRCompletionDate
                     },
                 },
            {
                "sTitle": "Category of Change", "className": "", render: function (data, type, row, meta) {
                    return row.Lookup
                },
            },
            //{
            //    "sTitle": "Design Engineer", "className": "", render: function (data, type, row, meta) {
            //        return row.RDE
            //    }
            //},
            //{
            //    "sTitle": "Senior Design Engineer", "className": "", render: function (data, type, row, meta) {
            //        return row.RSDE
            //    }
            //},
            
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Name</li><li class= "list-inline-item">Description</li><li class="list-inline-item">Category of Change </li></ul ></div ></div > ');
            $('tblDCRList_wrapper .dataTables_filter').append($searchButton);
 
            var $filterBoxPartList = $('#tblDCRList_wrapper .dataTables_filter input').unbind(), selfPartList = this.api();
            var stoppedTypingPartList;

            $filterBoxPartList.on('keyup', function () {
                if (stoppedTypingPartList) clearTimeout(stoppedTypingPartList);
                stoppedTypingPartList = setTimeout(function () {
                    selfPartList.search($filterBoxPartList.val()).draw();
                }, 500);
            });
        },

        "fnRowCallback": function () {
        }
    });
}

$('#tblDCRList').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDCRList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DCR/Delete',
                    data: { Id: data.DCRId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDCRList.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDCRList != null)
        tblDCRList.columns.adjust();
});

$("a[href='#m_tabs_3_1']").click(function () {
    tblDocumentList.ajax.reload();
});
$("a[href='#m_tabs_3_2']").click(function () {
    tblAffectedPart.ajax.reload();
});

$('#tblDCRList').on('click', 'td .btnCancel', function () {
    var tr = $(this).closest('tr');
    var row = tblDCRList.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to cancel..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
     function (isConfirm) {
         if (isConfirm.value) {
             mApp.blockPage();
             $.ajax({
                 type: "POST",
                 url: '/DES/DCR/UpdateDCRStatus',
                 data: JSON.stringify({ DCRId: data.DCRId, Status: 'Cancelled' }),
                 dataType: 'json',
                 contentType: "application/json;charset=utf-8",
                 async: true,
                 success: function (response) {
                     mApp.unblockPage();
                     if (response.Status) {
                         toastr.success(response.MSG, "Success");
                         tblDCRList.ajax.reload();
                     } else {
                         toastr.error(response.MSG, "Error");
                     }
                 },
                 error: function (error) {
                     toastr.error("something went wrong try again later", "Error");
                     mApp.unblockPage();
                 }
             })
         }
     });
});

$('#tblDocumentList').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblAffectedPart').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblAffectedPart.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});