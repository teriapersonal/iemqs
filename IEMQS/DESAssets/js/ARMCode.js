﻿
var tblARMCode;
$(document).ready(function () {
    mApp.blockPage();
    tblARMCode = $('#tblARMCode').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/ARMSet/ARMCode",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "order": [[0, 'desc']],
        "columnDefs": [

        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "ARMCode<span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var Name = '<td><strong style="text-transform: capitalize;">' + row.Name + '</strong></td> ';
                    //<td style="width: 30px"><a href="javascript;;" class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill" title="Upload Document" data-toggle="modal" data-target="#m_dropzone" onclick ="ARMDropzone(\'' + row.ARMCodeId + '\',\'' + row.Name + '\')"><i class="la la-paperclip"></i></a></td>'
                    return Name;
                },
            },
            {
                "sTitle": "Description<span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    return row.Description;
                }
            },
            {
                "sTitle": "Created By", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddARMCodeRow();
        },

        "fnRowCallback": function () {
            AddARMCodeRow();

        }

    });
    mApp.unblockPage();
});

var ARMCodeIDForDropZone;
var ARMNameForDropzone;
var tblARMFiles;
function ARMDropzone(ARMCodeId, Name) {
    //When files are drop in DropZone

    if (tblARMFiles != null) {
        $('#tblARMFiles').DataTable().destroy();
    }

    ARMCodeIDForDropZone = ARMCodeId;
    ARMNameForDropzone = Name;
    $("#m-dropzone-two").show();
    getARMFileTable(ARMCodeId);

}

var iii = 1;
var regex = /\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;
var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    parallelUploads: 10000,
    maxFilesize: 20000011,
    maxFiles: 10,
    accept: function (file, done) {
        var fileName = regex.test(file.name);
        if (fileName) {
            toastr.info(file.name + " Contains special characters", "Info");
            myDropzone.removeFile(file);
        }
        else {
            var dotNameSplit = file.name.split('.');
            if (dotNameSplit.length > 2) {
                toastr.info(file.name + " Contains special characters", "Info");
                myDropzone.removeFile(file);
            }
            else {
                done();
            }
        }
    },

    init: function () {
        this.on("maxfilesexceeded", function (file) {

            if (this.files.length == 11) {
                // this.removeFile(this.files[0]);
                if (iii == 1) {
                    alert("10 Files can be uploded at once!");
                }
                iii++;
            }
            myDropzone.removeFile(file);
            myDropzone.removeAllFiles();
        });

    },
    queuecomplete: function () {
        iii = 1;
    }
});

myDropzone.on('sending', function (file, xhr, formData, progress, bytesSent) {

    $('.dz-error-message').css('display', 'none');
    $('.dz-success-mark').css('display', 'block');
    $('.dz-error-mark').css('display', 'none');

    var ARMCodeId = ARMCodeIDForDropZone;
    var ARMCodeName = ARMNameForDropzone;
    if ($('#CurrentLocationIp').val() != "") {
        var CurrentLocationIp = $('#CurrentLocationIp').val();
        var CurrentLocationPath = $('#CurrentLocationSavedPath').val();
        var FCSurl = CurrentLocationIp + "/api/Upload";
        formData.append('SavedPath', CurrentLocationPath.toString());
        formData.append('DocumentPath', ARMCodeName.toString());
        formData.append('DocumentFile', file);
        //Need To get Status and other filed HERE 
        mApp.blockPage();

        window.onbeforeunload = function () {
            var prevent_leave = true;
            if (prevent_leave) {
                return "Your files are not completely uploaded yet...";
            }
        }

        jQuery.ajax({
            url: FCSurl,
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function (response) {
                if (response.Uploded) {
                    var fname = response.SavedFileName;
                    var filename = response.OriginalFileName;
                    var path = response.SavedPath + "/" + fname;

                    var Project = $('#Project').val();
                    var getExtention = filename.split('.')[1];
                    var armMapp = {
                        ARMCodeId: ARMCodeId,
                        FilePath: path,
                        FileName: filename,
                    };
                    $.ajax({
                        type: "POST",
                        url: "/DES/ARMSet/CreateARMDocMapping", //If get File Then Document Mapping Add
                        data: JSON.stringify(armMapp),
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        aysnc: true,
                        success: function (response) {
                            if (response) {
                                toastr.success(response.Msg, "Success");
                                myDropzone.removeFile(file);
                                tblARMFiles.ajax.reload();
                                mApp.unblockPage();
                            } else {
                                toastr.error(response.Msg, "Not Success");
                                mApp.unblockPage();
                            }
                        },
                        error: function (response) {
                            toastr.error(response.Msg, "Error");
                            myDropzone.removeFile(file);
                            mApp.unblockPage();
                        }
                    });
                }
                else {
                    toastr.error("Fial to File Uploaded to Server", "Error");
                    myDropzone.removeFile(file);
                    mApp.unblockPage();
                }
                window.onbeforeunload = function () {
                    var prevent_leave = false;
                    if (prevent_leave) {
                        return "Your files are not completely uploaded...";
                    }
                }
            },
        }).fail(function (data) {
            toastr.error("Server response not received", "Error");
            myDropzone.removeFile(file);
            mApp.unblockPage();

            window.onbeforeunload = function () {
                var prevent_leave = false;
                if (prevent_leave) {
                    return "Your files are not completely uploaded...";
                }
            }
        });
    }
    else {
        toastr.error("Ip is not detecting", "Error");
        myDropzone.removeFile(file);
        mApp.unblockPage();
    }
    mApp.unblockPage();
});


function getARMFileTable(ARMCodeId) {
    tblARMFiles = $('#tblARMFiles').DataTable({
        "searching": false,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "serverSide": true,
        "Destroy": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/ARMSet/GetARMCodeAttacheFile",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ARMCodeId", value: ARMCodeId });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action ", render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "File Name", render: function (data, type, row, meta) {
                    return row.FileName;
                }
            }
        ],
        "initComplete": function () {
            setTimeout(function () {
                tblARMFiles.columns.adjust();
            }, 1000);
        },

        "fnRowCallback": function () {
        }
    });
}


var path;
$('#tblARMFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblARMFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.FilePath, CurrentLocationIp);
});


$('#tblARMFiles').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblARMFiles.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to delete?",
        text: "",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                deleteARMAttach(data.FilePath, data.ARMCodeFileId);
            }
            mApp.unblockPage();
        });
});


function deleteARMAttach(DocumentPath, ARMCodeFileId) {
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";

    var docdelete = {
        deletePath: DocumentPath
    };

    $.ajax({
        url: FCSurl,
        data: JSON.stringify(docdelete),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        cache: false,
        async: true,
        processData: false,
        method: 'POST',
        success: function (response) {
            if (response) {
               // toastr.success(response.Msg, "Successfully removed file from server");
                $.ajax({
                    type: "GET",
                    url: "/DES/ARMSet/DeleteARMAttachFile", //If get File Then Document Mapping Add
                    data: { Id: ARMCodeFileId},
                    dataType: 'json',
                   // contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        toastr.success(response.Msg, "Success");
                        mApp.unblockPage();
                        if (tblARMFiles.data().count() == 1) {
                            tblARMFiles.ajax.reload();
                        }
                        else {
                            tblARMFiles.ajax.reload(null, false);
                        }
                    },
                    error: function (error) {
                        toastr.error("Error while removing file from grid", "Error");
                        mApp.unblockPage();
                    }
                })
            }
        },
        error: function (error) {
            toastr.error("Error while removing file from server", "Error");
            mApp.unblockPage();
        }
    });
}

function AddARMCodeRow() {
    setTimeout(function () {
        if ($("#Name_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td><input type="text" class="form-control" id="Name_0" name="Name_0" placeholder="ARMCode" /> <label class="text-danger" id="errorName_0" style="display:none" >ARMCode is required </label></td> '
                + '<td><input type="text" class="form-control" id="Description_0" name="Description_0" placeholder="Description" /> <label class="text-danger" id="errorDescription_0" style="display:none" >Description is required </label></td> '
                + '<td></td><td></td><td></td><td></td>'
                + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddARMCode(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>'
        $('#tblARMCode tbody').prepend(html);
    }, 1000);
}

$('#tblARMCode').on('click', 'td .btnEdit', function () {

    var tr = $(this).closest('tr');
    var row = tblARMCode.row(tr);
    var data = row.data();
    var html =
        '<td><input id="Name_' + data.ARMCodeId + '" name="Name_' + data.ARMCodeId + '" value="' + data.Name + '" type="text"  placeholder="Name" class="form-control blockSpecialChar noSpace"><label class="text-danger" id="errorName_' + data.ARMCodeId + '" style="display:none" >ARMCode is required </label></td>'
        + '<td><input id="Description_' + data.ARMCodeId + '" name="Description_' + data.ARMCodeId + '" value="' + data.Description + '"  type="text"  placeholder="Description" class= "form-control blockSpecialChar noSpace"><label class="text-danger" id="errorDescription_' + data.ARMCodeId + '" style="display:none" >Description is required </label></td>'

    html += '<td></td><td></td><td></td><td></td>'
    html += '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddARMCode(' + data.ARMCodeId + ')" title="Edit"><i class="la la-check text-info"></i></a>'
    html += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblARMCodeRelod()" title="close"><i class="la la-close text-danger"></i></a></td>';

    $(tr).html(html);
    $("#Name_" + data.ARMCodeId).focus();
});

$('#tblARMCode').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblARMCode.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/ARMSet/DeleteArmCode',
                    data: { ARMCodeId: data.ARMCodeId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblARMCode.ajax.reload();
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

function tblARMCodeRelod() {
    tblARMCode.ajax.reload();
}

function AddARMCode(id) {
    $("#errorName_" + id).hide();
    $("#errorDescription_" + id).hide();
    var Name = $("#Name_" + id).val();
    var Description = $("#Description_" + id).val();
    var ARMCodeId = (id);
    if (Name != "" && Description != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/ARMSet/AddEditARMCode',
            data: JSON.stringify({ Name: Name, Description: Description, ARMCodeId: ARMCodeId }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    ddlobjectlist = response.Objectlist;
                    tblARMCode.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    }
    else {
        if (Name == "")
            $("#errorName_" + id).show();
        if (Description == "")
            $("#errorDescription_" + id).show();
    }
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblARMCode != null)
        tblARMCode.columns.adjust();
});