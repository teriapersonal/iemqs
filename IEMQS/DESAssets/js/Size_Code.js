﻿var tblSizeCode;

$(document).ready(function () {

    tblSizeCode = $('#tblSizeCode').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "autoWidth": false,
        "sAjaxSource": "/SizeCode/GetSizeCodeList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            //{ "orderable": true, "targets": 1, "width": "185px" },
            //{ "orderable": true, "targets": 2, "width": "200px" },
            //{ "orderable": true, "targets": 3, "width": "200px" },
            //{ "orderable": false, "targets": 4, "width": "150px"},
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Size Code <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.SizeCode + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "String 2 <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Description1;
                }
            },
            {
                "sTitle": "String 3", "className": "", render: function (data, type, row, meta) {
                    return row.Description2;
                }
            },
            {
                "sTitle": "String 4", "className": "", render: function (data, type, row, meta) {
                    return row.Description3;
                }
            },
            {
                "sTitle": "Weight Factor <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {                    
                    return row.WeightFactor;
                }
            },                       
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddSizeCodeRow();
        },

        "fnRowCallback": function () {
            AddSizeCodeRow();
        }
    });
});

function AddSizeCodeRow() {
    setTimeout(function () {
        $("#SizeCode_0").focus();
        if ($("#SizeCode_0").length == 0)
            $('#tblSizeCode tbody').prepend('<tr class="v-top">'
                + '<td><input type="text" maxlength="20" class="form-control blockSpecialChar noSpace" id="SizeCode_0" name="SizeCode_0" placeholder="Size Code" /> <label class="text-danger" id="errorSizeCode_0" style="display:none" >Size Code is required </label></td> '
                + '<td><input type="text" maxlength="30" class="form-control" id="Description1_0" name="Description1_0" placeholder="String-2" /><label class="text-danger" id="errorDescription1_0" style="display:none" >String-2 is required </label></td> '
                + '<td><input type="text" maxlength="30" class="form-control" id="Description2_0" name="Description2_0" placeholder="String-3" /><label class="text-danger" id="errorDescription2_0" style="display:none" >String-3 is required </label></td> '
                + '<td><input type="text" maxlength="30" class="form-control" id="Description3_0" name="Description3_0" placeholder="String-4" /><label class="text-danger" id="errorDescription3_0" style="display:none" >String-4 is required </label></td> '
                + '<td><input type="text" class="form-control noSpace" id="WeightFactor_0" name="WeightFactor_0" placeholder="Weight Factor" /><label class="text-danger" id="errorWeightFactor_0" style="display:none" >Weight Factor is required </label></td> '
                + '<td></td><td></td><td></td><td></td>'
                + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddSizeCode(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>');
    }, 1000);
}

function AddSizeCode(ids) {
    var id;
    if (ids == 0) {
        id = 0;
    }
    else {
        id = $(ids).attr("data-arm");
    }

    $("#errorSizeCode_" + id).hide();
    $("#errorDescription1_" + id).hide();
    $("#errorDescription2_" + id).hide();
    $("#errorDescription3_" + id).hide();
    $("#errorWeightFactor_" + id).hide();
    var sizecode = $("#SizeCode_" + id).val();
    var desc1 = $("#Description1_" + id).val();
    var desc2 = $("#Description2_" + id).val();
    var desc3 = $("#Description3_" + id).val();
    var weight = $("#WeightFactor_" + id).val();
    if (sizecode != "" && desc1 != "" && weight != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/SizeCode/AddSizeCode',
            data: JSON.stringify({ SizeCode: id, SizeCode: sizecode, Description1: desc1, Description2: desc2, Description3: desc3, WeightFactor: weight }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblSizeCode.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (sizecode == "")
            $("#errorSizeCode_" + id).show();
        if (desc1 == "")
            $("#errorDescription1_" + id).show();
        //if (desc2 == "")
        //    $("#errorDescription2_" + id).show();
        //if (desc3 == "")
        //    $("#errorDescription3_" + id).show();
        if (weight == "")
            $("#errorWeightFactor_" + id).show();
    }

}

$('#tblSizeCode').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblSizeCode.row(tr);
    
    var data = row.data();
    var v2 = data.Description2;
    var v3 = data.Description3;
    if (v2 == 'null' || v2 == '' || v2 == undefined)     
        v2 = ""; 
    if (v3 == 'null' || v3 == '' || v3 == undefined)
        v3 = "";
    var html = ('<td><input type="text" maxlength="20" class="form-control blockSpecialChar noSpace" id="SizeCode_' + data.SizeCode + '" name="SizeCode_' + data.SizeCode + '" placeholder="Size Code" value="' + data.SizeCode + '" disabled/> <label class="text-danger" id="errorSizeCode_' + data.SizeCode + '" style="display:none" >Size Code is required </label></td> '
        + '<td><input type="text" maxlength="30" class="form-control" id="Description1_' + data.SizeCode + '" name="Description1_' + data.SizeCode + '" placeholder="String-2" value="' + data.Description1 + '" /><label class="text-danger" id="errorDescription1_' + data.SizeCode + '" style="display:none" >String-2 is required </label></td> '
        + '<td><input type="text" maxlength="30" class="form-control" id="Description2_' + data.SizeCode + '" name="Description2_' + data.SizeCode + '" placeholder="String-3" value="' + v2 + '" /><label class="text-danger" id="errorDescription2_' + data.SizeCode + '" style="display:none" >String-3 is required </label></td> '
        + '<td><input type="text" maxlength="30" class="form-control" id="Description3_' + data.SizeCode + '" name="Description3_' + data.SizeCode + '" placeholder="String-4" value="' + v3 + '" /><label class="text-danger" id="errorDescription3_' + data.SizeCode + '" style="display:none" >String-4 is required </label></td> '
        + '<td><input type="text" class="form-control noSpace" id="WeightFactor_' + data.SizeCode + '" name="WeightFactor_' + data.SizeCode + '" placeholder="Weight Factor" value="' + data.WeightFactor + '" /><label class="text-danger" id="errorWeightFactor_' + data.SizeCode + '" style="display:none" >Weight Factor is required </label></td> '
        + '<td></td><td></td><td></td><td></td>'
        + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" data-arm=' + data.SizeCode + ' onclick="AddSizeCode(this)" title="Edit"><i class="la la-check text-info"></i></a>'
        + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblSizeCodeClear()" title="close"><i class="la la-close text-danger"></i></a></td>');
    $(tr).html(html);
    $("#Description1_" + data.SizeCode).focus();
});

function tblSizeCodeClear() {
    tblSizeCode.ajax.reload();
}

$('#tblSizeCode').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblSizeCode.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    cache: false,
                    type: "GET",
                    url: "/DES/SizeCode/DeleteSizeCode",
                    data: { Id: data.SizeCode },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblSizeCode.ajax.reload();
                        } else {
                            toastr.info(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblSizeCode != null)
        tblSizeCode.columns.adjust();
});
