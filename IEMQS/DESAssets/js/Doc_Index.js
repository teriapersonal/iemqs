﻿
var tblDocument;
$(document).ready(function () {
    if ('@TempData["Info"]' == "DOC is alredy use by another user") {
        toastr.Info('DOC alredy in use by another user');
    }
    mApp.blockPage();
    getCurrentUser = $('#CurrentPsNo').val();
    //Get Document Table
    tblDocument = $('#tblDocument').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DOC/Index",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "fixedColumns": {
            "leftColumns": 2
        },
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ],

        //"fnDrawCallback": function (oSettings) {
        //    //$('.dataTables_filter').each(function () {
        //    //    $(this).append('<button class="btn btn-default mr-xs pull-right" type="button">Button</button>');
        //    //});
        //    $('#tblDocument_filter').append('<button class="btn btn-default mr-xs pull-right" type="button">Button</button>');

        //}
        //,
        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Authorized > 0) {
                        if (IsInprocess.toLowerCase() == 'false') {
                        }
                        else {
                            if (row.Status == "Draft" || row.Status == "Created") {
                                srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditDOC" title="Edit"><i class="la la-edit m--font-brand"></i></a>';
                                if ((row.IsDOCCheckIn == true)) {
                                    if (row.DOCCheckInBy == getCurrentUser) {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                        srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDOC" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                    }
                                    else {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.DocumentCheckinBy + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                    }
                                }
                                else {
                                    srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDOC" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                }
                            }
                            else if (row.Status == "Completed" && row.ENGGRole > 0) {
                                srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRevDOC"  onclick="funRevision()" title="Create Revision"><i class="la la-refresh m--font-brand"></i></a>';
                                srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info  m-btn--icon m-btn--icon-only m-btn--pill" title="Create ROC"><i class="la la-comments m--font-info" onclick="OpenROC(\'' + row.DocumentId + '\',\'' + row.Project + '\')"></i></a>';
                            }
                        }
                    }
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDOC" title="View"><i class="la la-eye m--font-success"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "Document Type", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.DocumnetTypeName;
                },
            },
            {
                "sTitle": "Document Title", "className": "text-wrap", "orderable": true, render: function (data, type, row, meta) {
                    return row.DocumentTitle;
                },
            },
            {
                "sTitle": "JEP Doc", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    if (row.IsJEP == true) {
                        row.IsJEP = "Yes";
                    }
                    else {
                        row.IsJEP = "No";
                    }
                    return row.IsJEP;
                }
            },
            {
                "sTitle": "Doc No", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.DocumentNo;
                }
            },
            {
                "sTitle": "Status", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.Status;
                }
            },
            {
                "sTitle": "Rev", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return "R" + row.DocumentRevision;
                }
            },
            {
                "sTitle": "Policy", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.PolicyName;
                }
            },

            {
                "sTitle": "Customer Doc No", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.ClientDocumentNumber;
                }
            },
            {
                "sTitle": "Customer Rev No", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.ClientVersion;
                }
            },
            {
                "sTitle": "DCR No", "className": "", "orderable": true, render: function (data, type, row, meta) {                    
                    var name;
                    if (row.DCRNo != null) {
                        var str = [];
                        if (row.DCRNo != null) {
                            var i;
                            var getDCR = row.DCRNo.split(',');
                            for (i = 0; i < getDCR.length; i++) {
                                var getValue = getDCR[i].split('__');
                                str.push('<a target="_blank" href="/DES/Dashboard/FindObject?Id=' + getValue[0] + '&Type=DCR">' + getValue[0] + '</a>');
                            }
                            name = str.join(", ");
                        }
                        else {
                            name = "N/A";
                        }
                    }
                    else {
                        name = row.DCRNo;
                    }
                    return name;
                }
            },
            {
                "sTitle": "Created By", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.EditBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Document type</li>  <li class= "list-inline-item" >Document title</li><li class="list-inline-item">Document no </li><li class="list-inline-item">Customer Doc no </li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);
        },

        "fnRowCallback": function () {
            tblDocument.columns.adjust();
        }

    });
    mApp.unblockPage();
});

var DocId;
$('#tblDocument').on('click', 'td .btnRevDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblDocument.row(tr);
    var data = row.data();
    DocId = data.QString
});

function funRevision() {
    swal({
        title: "Do you want to create new revision?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                window.location = "/DES/DOC/Edit?q=" + DocId + "&Revision=" + rev;
            } else {
            }
        });
}

$('#tblDocument').on('click', 'td .btnDeleteDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblDocument.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DOC/Delete',
                    data: { Id: data.DocumentId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblDocument.data().count() == 1) {
                                tblDocument.ajax.reload();
                            }
                            else {
                                tblDocument.ajax.reload(null, false);
                            }
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                });
            }
        });
});

$('#tblDocument').on('click', 'td .btnEditDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblDocument.row(tr);
    var data = row.data();
    if ((data.IsDOCCheckIn == true)) {
        if (data.DOCCheckInBy == getCurrentUser) {
            window.location = "/DES/DOC/Edit?q=" + data.QString;
        }
        else {
            toastr.info("Document is already in use by " + data.DocumentCheckinBy, "Info");
        }
    }
    else {
        window.location = "/DES/DOC/Edit?q=" + data.QString;
    }
});

$('#tblDocument').on('click', 'td .btnViewDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblDocument.row(tr);
    var data = row.data();
    window.location = "/DES/DOC/Detail?q=" + data.QString;
});

function OpenROC(documentid, Project) {
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: "/DES/ROC/ShowROCPopup",
        data: {
            documentId: documentid,
            docProject: Project
        },
        dataType: "html",
        success: function (response) {
            $("#divROC").html(response);
            $('#ROCList').modal('show');
            mApp.unblockPage();
        },
        failure: function (response) {
            toastr.error(response.Msg, "Error");
            mApp.unblockPage();
        },
        error: function (response) {
            toastr.error(response.Msg, "Error");
            mApp.unblockPage();
        }
    });
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDocument != null)
        tblDocument.columns.adjust();
});


$('#tblDocument').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblDocument.row(tr);
    var data = row.data();
    var docMapp = {
        DocumentId: data.DocumentId
    };
    swal({
        title: "Do you want to release Document?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            mApp.blockPage();
            $.ajax({
                url: "/DES/DOC/UpdateDOCCheckOut",
                data: JSON.stringify(docMapp),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        if (response.Status) {
                            toastr.info(response.Msg, "Info");
                            tblDocument.ajax.reload();
                            mApp.unblockPage();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }

            });
        }
    });
});


$('#tblDocument').on('click', 'td .btnCheckIn', function () {
    var tr = $(this).closest('tr');
    var row = tblDocument.row(tr);
    var data = row.data();
    if ((data.IsDOCCheckIn == true)) {
        if (data.DOCCheckInBy != getCurrentUser) {
            toastr.info("Document is already in use by " + data.DocumentCheckinBy, "Info");
        }
    }
});



