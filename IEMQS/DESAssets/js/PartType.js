﻿var tblPartType;

$(document).ready(function () {

    tblPartType = $('#tblPartType').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/PartType/GetPartTypeList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Product Form <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var productform = "<strong style='text-transform: capitalize;'>" + row.ProductForm + "</strong>";
                    return productform;
                }
            },
            {
                "sTitle": "Part Type <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var parttype = "<strong style='text-transform: capitalize;'>" + row.ItemType + "</strong>";
                    return parttype;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>'; /*onclick="GetAllRoleByFunctionId(' + row.FunctionId + ');"*/
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddPartTypeRow();
        },

        "fnRowCallback": function () {
            AddPartTypeRow();

        }
    });
});


function AddPartTypeRow() {    
    setTimeout(function () {
        $("#ProductFormName_0").focus();
        if ($("#ProductFormName_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td><select class="form-control" id="ProductFormName_0"><option value="">Select</option>';
        for (let i = 0; i < ddlprodlist.length; i++) {
            html += "<option value='" + ddlprodlist[i] + "'>" + ddlprodlist[i] + "</option>";
        }
        html += '</select><br><label class="text-danger" id="errorProductFormName_0" style="display:none" >Product Form is required </label></td>'

            + '<td><select class="form-control" multiple="multiple" id="ItemTypeList_0">';
        for (let i = 0; i < ddlparttypelist.length; i++) {
            html += "<option value='" + ddlparttypelist[i].Value + "'>" + ddlparttypelist[i].Text + "</option>";
        }
        html += '</select><br><label class="text-danger" id="errorItemTypeList_0" style="display:none" >Part Type is required </label></td>'
            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddPartType(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>';
        $('#tblPartType tbody').prepend(html);
        $("#ItemTypeList_0").select2();
        $("#ItemTypeList_0").select2({
            placeholder: "-----Select-----"
        });
        $("#ProductFormName_0").select2();
    }, 1000);
}

function AddPartType(id) {
    var productform = $("#ProductFormName_" + id).val();
    var itemtype = $("#ItemTypeList_" + id).val().join(",");

    if ($("#ProductFormName_" + id).val() != "" && $("#ItemTypeList_" + id).val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/PartType/AddPartType',
            data: JSON.stringify({ ItemTypeId: id, ProductForm: productform, ItemType: itemtype }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblPartType.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if ($("#ProductFormName_" + id).val() == "")
            $("#errorProductFormName_" + id).show();
        if ($("#ItemTypeList_" + id).val() == "")
            $("#errorItemTypeList_" + id).show();
    }
}

$('#tblPartType').on('click', 'td .btnEdit', function () {   
    var tr = $(this).closest('tr');
    var row = tblPartType.row(tr);
    var data = row.data();

    var html = '<td><select class="form-control" id="ProductFormName_' + data.ItemTypeId + '" name="ProductFormName_' + data.ItemTypeId + '" value="' + data.t_prod + '">';
    for (let i = 0; i < ddlprodlist.length; i++) {
        if (ddlprodlist[i] == data.ProductForm)
            html += "<option value='" + ddlprodlist[i] + "' selected>" + ddlprodlist[i] + "</option>";
        else
            html += "<option value='" + ddlprodlist[i] + "'>" + ddlprodlist[i] + "</option>";
    }
    html += '</select><label class="text-danger" id="errorProductFormName_' + data.ItemTypeId + '" style="display:none" >Product Form is required </label></td>'

        + '<td><select class="form-control" multiple="multiple" id="ItemTypeList_' + data.ItemTypeId + '" name="ItemTypeList_' + data.ItemTypeId + '" value="' + data.ItemType + '">';
    for (let i = 0; i < ddlparttypelist.length; i++) {
        html += "<option value='" + ddlparttypelist[i].Value + "'>" + ddlparttypelist[i].Text + "</option>";
    }
    html += '</select></br><label class="text-danger" id="errorItemTypeList_' + data.ItemTypeId + '" style="display:none" >Part Type is required </label></td> '
        + '<td></td><td></td><td></td><td></td>'
        + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddPartType(' + data.ItemTypeId + ')" title="Update"><i class="la la-check text-info"></i></a>'
        + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblPartTypeClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>';
    $(tr).html(html);

    var selectedValues;
    if (data.ItemType) {
        selectedValues = data.ItemType.split(',');
        var parts = [];
        for (var i = 0; i < selectedValues.length; i++) {
            parts.push(String(selectedValues[i].trim()));
        }
        $('#ItemTypeList_' + data.ItemTypeId).select2();
        $('#ItemTypeList_' + data.ItemTypeId).val(parts).trigger('change');
    }
    
    $("#ProductFormName_" + data.ItemTypeId).focus();
    $("#ProductFormName_" + data.ItemTypeId).select2(); 
});

function tblPartTypeClear() {
    tblPartType.ajax.reload();
}

$('#tblPartType').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblPartType.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/PartType/DeletePartType',
                    data: { Id: data.ItemTypeId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblPartType.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblPartType != null)
        tblPartType.columns.adjust();
});

