﻿var tblCustomerFeedbackGrid;
var tblDocumentFiles;
var Dropzonefile;
var Count;
var documentNo;
var GroupId;

$(document).ready(function () {

    if ($("#IsDetail").val() == "true" || IsInprocess.toLowerCase() == 'false') {
        $("#m-dropzone-two").css("display", "none");
    }

    tblCustomerFeedbackGrid = $('#tblCustomerFeedbackGrid').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/JEP/GetCustomerFeedbackGrid",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "fixedColumns": {
            leftColumns: 4
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 9 },
            { "orderable": false, "targets": 11 },
            { "orderable": false, "targets": 5 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "Agency", value: $("#AgencyId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },

        "aoColumns": [
            {
                "sTitle": "Action", "className": "", render: function (data, type, row, meta) {
                    var action = '<a href="javascript;;" class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill btnUpload" title="Upload Document" data-toggle="modal" data-target="#m_dropzone" onclick="getCustomerFeedbackDocumentGrid(' + row.JEPCustomerFeedbackId + ')" > <i class="la la-paperclip"></i></a > ';
                    return action;
                }
            },
            {
                "sTitle": "Document No", "className": "text-wrap custom-table-width", render: function (data, type, row, meta) {
                    return row.DocNo;
                }
            },
             {
                 "sTitle": "Title", "className": "text-wrap custom-table-width", render: function (data, type, row, meta) {
                     return row.Title;
                 }
             },
            {
                "sTitle": "Rev.No.", "className": "", render: function (data, type, row, meta) {
                    return 'R' + row.RevisionNo;
                }
            },
             {
                 "sTitle": "Transmittal No", "className": "", render: function (data, type, row, meta) {
                     return row.RefTransmittalNo;
                 }
             },
            {
                "sTitle": "Return Transmittal No", "className": "", render: function (data, type, row, meta) {
                    var ReturnTransmittal = '';
                    if ($("#IsDetail").val() == "true") {
                        ReturnTransmittal = row.ReturnTransmittalNo;
                    } else {
                        ReturnTransmittal = '<input type="text" class="form-control m-input m-input--square            alphnumericspacedashunddot" name="JEPCustFeedbackList[' + meta.row + '].ReturnTransmittalNo" placeholder="Return Transmittal No" value="' + (row.ReturnTransmittalNo || '') + '" maxlength = "100" placeholder = "Ref   Transmittal    No"  />';
                    }
                    return ReturnTransmittal;
                }
            },
            {
                "sTitle": "Approval Code  <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var docType = '';
                    if ($("#IsDetail").val() == "true") {
                        docType += '<select disabled class="form-control m-input  m-input--square ClsApprovalCode"  name="JEPCustFeedbackList[' + meta.row + '].ApprovalId">';
                        docType += "<option value=''>Select</option>";
                        for (let i = 0; i < ddlApprovalList.length; i++) {
                            if (ddlApprovalList[i].Id == row.ApprovalCode)
                                docType += "<option value='" + ddlApprovalList[i].Id + "' selected>" + ddlApprovalList[i].Lookup + "</option>";
                            else
                                docType += "<option value='" + ddlApprovalList[i].Id + "'>" + ddlApprovalList[i].Lookup + "</option>";
                        }
                        docType += '</select></br>'

                    } else {
                        docType += '<select class="form-control m-input  m-input--square ClsApprovalCode"       name="JEPCustFeedbackList[' + meta.row + '].ApprovalId">';
                        docType += "<option value=''>Select</option>";
                        for (let i = 0; i < ddlApprovalList.length; i++) {
                            if (ddlApprovalList[i].Id == row.ApprovalCode)
                                docType += "<option value='" + ddlApprovalList[i].Id + "' selected>" + ddlApprovalList[i].Lookup + "</option>";
                            else
                                docType += "<option value='" + ddlApprovalList[i].Id + "'>" + ddlApprovalList[i].Lookup + "</option>";
                        }
                        docType += '</select></br>'
                    }

                    return docType;
                }
            },
                {
                    "sTitle": "Actual Sub. Date", "className": "", render: function (data, type, row, meta) {
                        return row.ActualSubDate;
                    }
                },
                {
                    "sTitle": "Planned Sub. Date", "className": "", render: function (data, type, row, meta) {
                        return row.PlannedSubDate;
                    }
                },

            {
                "sTitle": "Actual Receipt Date  <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {

                    var date = '';

                    if ($("#IsDetail").val() == "true") {
                        date = row.ReceiptDate;
                    } else {
                        date = '<input type="text" class="form-control  datepickerroc" name="JEPCustFeedbackList[' + meta.row + '].ReceiptDate" autocomplete="off" id = "txtReceiptDate_' + row.JEPCustomerFeedbackId + '"    placeholder="Receipt Date" value="' + (row.ReceiptDate || '') + '" />';
                        date += '<input type="hidden" class="form-control" name="JEPCustFeedbackList[' + meta.row + '].ActualSubDate"  id = "txtActualSubDate_' + row.JEPCustomerFeedbackId + '"   value="' + (row.ActualSubDate || '') + '" />';

                       
                        date += '<input type="hidden" name="JEPCustFeedbackList[' + meta.row + '].JEPNumber"  value="' + row.JEPNumber + '"/>';
                        date += '<input type="hidden" name="JEPCustFeedbackList[' + meta.row + '].DocNo"  value="' + row.DocNo + '"/>';
                        date += '<input type="hidden" name="JEPCustFeedbackList[' + meta.row + '].CustomerDocNo"  value="' + row.CustomerDocNo + '"/>';
                        date += '<input type="hidden" name="JEPCustFeedbackList[' + meta.row + '].JEPCustomerFeedbackId"  value="' + row.JEPCustomerFeedbackId + '"/>';
                        date += '<input type="hidden" name="JEPCustFeedbackList[' + meta.row + '].AgencyId"  value="' + $("#AgencyId").val() + '"/>';
                        date += '<input type="hidden" name="JEPCustFeedbackList[' + meta.row + '].RefTransmittalNo"  value="' + row.RefTransmittalNo + '"/>';

                    }
                    return date;
                }
            },
           
            {
                "sTitle": "Customer Doc No", "className": "", render: function (data, type, row, meta) {
                    var date = '';
                    if ($("#IsDetail").val() == "true") {
                        date = row.CustomerDocNo;
                    } else {
                        date = '<input type="text" maxlength="" class="form-control" name="JEPCustFeedbackList[' + meta.row + '].CustomerDocNo" placeholder="Customer Doc No" value="' + (row.CustomerDocNo || '') + '" maxlength = "100"/>';
                    }
                    return date;
                }
            },
             {
                "sTitle": "Remark", "className": "", render: function (data, type, row, meta) {
                    var Remark = '';
                    if ($("#IsDetail").val() == "true") {
                        Remark = row.Remark;
                    } else {
                        Remark = '<input type="text" class="form-control m-input m-input--square alphnumericspacedashunddot" name="JEPCustFeedbackList[' + meta.row + '].Remark" placeholder="Remark" value="' + (row.Remark || '') + '" maxlength = "200" placeholder = "Remark"  />';
                    }
                    return Remark;
                }
            },
        ],

        "initComplete": function (row) {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Document No</li><li class= "list-inline-item">Title</li><li class="list-inline-item">Transmittal No</li><li class="list-inline-item">Approval Code</li><li class="list-inline-item">Actual Sub. Date</li><li class="list-inline-item">Planned Sub. Date</li><li class="list-inline-item">Customer Doc No</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);
            // AddDocList();
        },

        "fnRowCallback": function () {

            Datepickeradd();
            //AddDocList();

        },

        "preDrawCallback": function () {
            if (String($("#IsUpdateChanges").val()) == "true") {
                if ($("#FormAddCustomerFeedback").valid() == true)
                    $("#FormAddCustomerFeedback").submit();
                $("#IsUpdateChanges").val("false");
            }
        },
    });

    //getCustomerFeedbackDocumentGrid();
});

$("#tblAgencyDocs").on("focus", ".datepickerAprDate", function () {
    var id = this.id;
    var pid = String(id).replace("txtPlnAprDate_", "");

    var PlnSubDate = $("#txtPlnSubDate_" + pid).val() || "";

    var jepdatesplit = String(PlnSubDate).split("/");
    var v = new Date();
    if (PlnSubDate != "")
        v = new Date(jepdatesplit[2] + '-' + jepdatesplit[1] + '-' + jepdatesplit[0])
    else {
        toastr.info("Planned Submit Date is required", "Info");
        return;
    }

    $("#" + id).datepicker({
        startDate: v,
        todayHighlight: !0, orientation: "bottom left", format: "dd/mm/yyyy", autoclose: true
    });
});

function getCustomerFeedbackDocumentGrid(strJEPCustomerFeedbackId) {
    if (tblDocumentFiles != null)
        $("#tblDocumentFiles").DataTable().destroy();
    $("#JEPCustomerFeedbackId").val(strJEPCustomerFeedbackId);

    setTimeout(function () {
        tblDocumentFiles = $('#tblDocumentFiles').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/JEP/GetCustomerFeedbackDocumentList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "GroupId", value: $("#GroupId").val() });
                data.push({ name: "JEPcustomerFeedbackId", value: strJEPCustomerFeedbackId });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "text-left", "orderable": false, render: function (data, type, row, meta) {
                        var srt = "";

                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                        srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';

                        var strIsDetail = getUrlVars()["IsDetail"];
                        if (strIsDetail != 'true') {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        }
                        return srt;
                    }
                },
                {
                    "sTitle": "Document Title", "className": "text-left", render: function (data, type, row, meta) {
                        return row.DocumentTitle;
                    }
                }
            ],

            "initComplete": function () {
                setTimeout(function () {
                    tblDocumentFiles.columns.adjust();
                }, 500);
            },

            "fnRowCallback": function () {
                setTimeout(function () {
                    tblDocumentFiles.columns.adjust();
                }, 500);
            }
        });
    }, 500);
}

function adjustGrid() {
    setTimeout(function () {
        tblDocumentFiles.columns.adjust();
    }, 700);
}

funSuccessCustmerFeedback = function (data) {
    $("#IsUpdateChanges").val("false");
    if (data.Status) {
        if (data.Submitted) {
            toastr.success(data.Msg, "Success");
            $("#AgencyId").val("");
            tblCustomerFeedbackGrid.ajax.reload();
        }
        else {
            //toastr.info(data.Msg, "Info");
        }

    }
    else {
        toastr.info(data.Msg.join('</br>'), "Info");
    }
}

$('#tblCustomerFeedbackGrid').on("keyup change", "input, select, textarea", function () {
    $("#IsUpdateChanges").val("true");
});

function funAgencyChange() {  
    mApp.blockPage();
    $("#IsUpdateChanges").val("false");
    $.ajax({
        type: "GET",
        url: '/DES/JEP/AddCustFeedbackByAgency',
        data: { Agency: $("#AgencyId").val() },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",

        success: function (response) {
            mApp.unblockPage();
            if (!response.InfoMsg) {
                setTimeout(function () {
                    tblCustomerFeedbackGrid.ajax.reload();
                }, 1000);
            }
            else {
                toastr.info(response.Msg, "Info");
            }           
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    });
}

//$('#btnSubmit').click(function () {
//    if ($("#AgencyId").val() > 0) {
//        $("#IsUpdateChanges").val("true");
//        window.location.href = "/DES/JEP/GetCustomerFeedbackgrid";
//        return true;
//    }
//    else {
//        toastr.info("Please select agency", "Info");
//        return false;
//    }
//});

$("#tblCustomerFeedbackGrid").on("focus", ".datepickerroc", function () {
    var id = this.id;
    var pid = String(id).replace("txtReceiptDate_", "");

    var PlnSubDate = $("#txtActualSubDate_" + pid).val() || "";

    var jepdatesplit = String(PlnSubDate).split("/");
    var v = new Date();
    if (PlnSubDate != "")
        v = new Date(jepdatesplit[2] + '-' + jepdatesplit[1] + '-' + jepdatesplit[0])

    $("#" + id).datepicker({
        startDate: v,
        todayHighlight: !0, orientation: "bottom left", format: "dd/mm/yyyy", autoclose: true
    });
});


//$("#tblCustomerFeedbackGrid").on("focus", ".datepickerroc", function () {
//    var tr = $(this).closest('tr');
//    var row = tblCustomerFeedbackGrid.row(tr);
//    var data = row.data();

//    //var id = this.name;
//    //var pid = String(id).replace("txtPlnAprDate_", "");

//    var PlnSubDate = (data.PlannedSubDate) || "";

//    var jepdatesplit = String(PlnSubDate).split("/");
//    var v = new Date();
//    if (PlnSubDate != "")
//        v = new Date(jepdatesplit[2] + '-' + jepdatesplit[1] + '-' + jepdatesplit[0])
    

//    $(this.name).datepicker({
//        startDate: v,
//        todayHighlight: !0, orientation: "bottom left", format: "dd/mm/yyyy", autoclose: true
//    });
//});

function Datepickeradd() {
    setTimeout(function () {
        $("#tblCustomerFeedbackGrid").focus();
        //var today = new Date();
        //$(".datepickerroc").datepicker({
        //    endDate:"today",
        //    maxDate: today,
        //    rtl: mUtil.isRTL(),
        //    todayHighlight: !0,
        //    orientation: "bottom left",
        //    format: 'dd/mm/yyyy',
        //    autoclose: !0
        //});

        var datarow = tblCustomerFeedbackGrid.rows().data();
        datarow.each(function (value, index) {
            $("select[name$='JEPCustFeedbackList[" + index + "].ApprovalId']").select2();
        });

    }, 1000);

}

$('#tblDocumentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

$('#tblDocumentFiles').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/JEP/DeleteCustomerFeedbackDocs',
                    data: { Id: data.JEPCustFeedbackDocumentId, JEPId: $('#JEPId').val() },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDocumentFiles.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

//$('#btnSubmit').click(function () {
//    if (!tblDocumentFiles.data().any()) {
//        toastr.info("At least one Document is Required", "Info");
//        return false;
//    }
//});

var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    parallelUploads: 10000,
    maxFilesize: 200000,
});

myDropzone.on('sending', function (file, xhr, formData, progress, bytesSent) {
    Count = myDropzone.files.length;
    if ($('#JEPDocumentDetailsId').val() != "" && $('#AgencyId').val() != "" && $('#ReceiptDate').val() != "") {
        var regex = /\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;
        var fileName = regex.test(file.name);
        if (fileName) {
            toastr.error(file.name + " Contains special characters", "Error");
            myDropzone.removeFile(file);
            myDropzoneThree.removeAllFiles();
        }
        else {
            var dotNameSplit = file.name.split('.');
            if (dotNameSplit.length > 2) {
                toastr.error(file.name + " Contains special characters", "Error");
                myDropzone.removeFile(file);
                myDropzoneThree.removeAllFiles();
            }
            else {

                if ($('#CurrentLocationIp').val() != "") {
                    var CurrentLocationIp = $('#CurrentLocationIp').val();
                    var CurrentLocationPath = $('#FilePath').val();
                    var FCSurl = CurrentLocationIp + "/api/Upload";

                    var DocumentNo = file.name;
                    var JEPNumber = $('#JEPNumber').val();

                    var DocumentPath = JEPNumber.toString().trim() + "-" + "CustomerFeedback";
                    formData.append('SavedPath', CurrentLocationPath.toString());
                    formData.append('DocumentPath', DocumentPath.toString());
                    formData.append('DocumentFile', file);
                    Dropzonefile = file;

                    var formate = file.name.substr(file.name.lastIndexOf('.') + 1);
                    mApp.blockPage();
                    window.onbeforeunload = function () {
                        var prevent_leave = true;
                        if (prevent_leave) {
                            return "Your files are not completely uploaded yet...";
                        }
                    }

                    //Need To get Status and other filed HERE 
                    jQuery.ajax({
                        url: FCSurl,
                        data: formData,
                        enctype: 'multipart/form-data',
                        cache: false,
                        async: true,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function (response) {

                            mApp.unblockPage();
                            var fname = response.SavedFileName;
                            var path = response.SavedPath + "/" + fname;

                            jQuery.ajax({
                                type: "POST",
                                url: '/DES/JEP/UploadCustomerFeedbackFiles',
                                data: JSON.stringify({ JEPCustomerFeedbackId: $("#JEPCustomerFeedbackId").val(), GroupId: $("#GroupId").val(), DocumentTitle: DocumentNo, DocumentFormat: formate, DocumentPath: path }),
                                dataType: 'json',
                                contentType: "application/json;charset=utf-8",
                                async: true,
                                success: function (response) {
                                    if (response.Status) {

                                        toastr.success(response.Msg, "File uploaded successfully");
                                        GroupId = $("#GroupId").val();
                                        tblDocumentFiles.ajax.reload();
                                        myDropzone.removeFile(file);
                                        mApp.unblockPage();
                                        $('#btnSubmit').attr('disabled', false);
                                    } else {
                                        toastr.error(response.Msg, "Error");
                                        mApp.unblockPage();
                                    }
                                },
                                error: function (error) {
                                    toastr.error("something went wrong try again later", "Error");
                                    myDropzone.removeFile(file);
                                    mApp.unblockPage();
                                }
                            });

                            window.onbeforeunload = function () {
                                var prevent_leave = false;
                                if (prevent_leave) {
                                    return "Your files are not completely uploaded...";
                                }
                            }
                        },
                    }).fail(function (data) {
                        mApp.unblockPage();
                        myDropzone.removeFile(file);
                        toastr.error("Server response not received", "Error");
                        window.onbeforeunload = function () {
                            var prevent_leave = false;
                            if (prevent_leave) {
                                return "Your files are not completely uploaded...";
                            }
                        }
                    });
                }
                else {
                    toastr.info("Ip is not detecting", "Info");
                    myDropzone.removeFile(file);
                    mApp.unblockPage();
                }
            }
        }
    }
    else {
        mApp.unblockPage();
        file.status = "canceled";
        myDropzone.removeFile(file);
        Count = Count - 1;
        if (Count == 0) {
            toastr.info("Select Document, Agency and Receipt Date first.", "Info");
        }
    }
});

$('#tblCustomerFeedbackGrid').on('click', 'td .btnUpload', function () {
    tblDocumentFiles.columns.adjust();
    adjustGrid();
});

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$('#tblDocumentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

