var DatatablesBasicBasic = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
            scrollY: "40vh",
            scrollX: !0,
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 10,
            language: {
                lengthMenu: "Display _MENU_"
            },
            order: [[1, "desc"]],
            headerCallback: function (e, a, t, n, s) {
                e.getElementsByTagName("th")[0].innerHTML = '\n                    <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                        <input type="checkbox" value="" class="m-group-checkable">\n                        <span></span>\n                    </label>'
            },
            columnDefs: [{
                targets: 0,
                width: "30px",
                className: "dt-right",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                        <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                            <input type="checkbox" value="" class="m-checkable">\n                            <span></span>\n                        </label>'
                }
            }, {
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                       <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-edit"></i>\n                        </a> \n                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-trash"></i>\n                        </a>'
                }
            }, {
                targets: 23,
                render: function (e, a, t, n) {
                    var s = {
                        2: {
                            title: "Send for review",
                            class: "m-badge--brand"
                        },

                        3: {
                            title: "Release",
                            class: " m-badge--success"
                        },
                        1: {
                            title: "Created",
                            class: " m-badge--info"
                        }
                    };
                    return void 0 === s[e] ? e : '<span class="m-badge ' + s[e].class + ' m-badge--wide">' + s[e].title + "</span>"
                }
            }, {
                targets: 3,
                render: function (e, a, t, n) {
                    var s = {
                        1: {
                            title: "Part",
                            state: "brand"
                        },
                        2: {
                            title: "Plate part",
                            state: "info"
                        },
                        3: {
                            title: "Clad Part",
                            state: "accent"
                        }
                    };
                    return void 0 === s[e] ? e : '<span class="m-badge m-badge--' + s[e].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + s[e].state + '">' + s[e].title + "</span>"
                }
            }]
        }), e.on("change", ".m-group-checkable", function () {
            var e = $(this).closest("table").find("td:first-child .m-checkable"),
                a = $(this).is(":checked");
            $(e).each(function () {
                a ? ($(this).prop("checked", !0), $(this).closest("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).closest("tr").removeClass("active"))
            })
        }), e.on("change", "tbody tr .m-checkbox", function () {
            $(this).parents("tr").toggleClass("active")
        })
        e.on("click", "tbody td:not(:first-child)", function () {
            editor.inline( this );
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic.init()
});