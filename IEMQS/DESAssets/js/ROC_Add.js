﻿var tblROC;
var tblROCCommentFiles;
var tblROCCommentedDocs;

$(document).ready(function () {
    mApp.blockPage();
    tblROC = $('#tblROC').DataTable({
        "searching": false,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/ROC/GetROCCommentsList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 },
            { "orderable": false, "targets": 9 },
            { "orderable": false, "targets": 10 },
            { "orderable": false, "targets": 11 },
            { "orderable": false, "targets": 12 },
            { "orderable": false, "targets": 13 },
            { "orderable": false, "targets": 14 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ROCId", value: $("#hdnRocId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Status", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name = '<input type="hidden" name="ROCCommentList[' + meta.row + '].ROCCommentsId"  value="' + row.ROCCommentsId + '"/>';
                    if (row.Status == 'Open') {
                        name += '<input type="hidden" name="ROCCommentList[' + meta.row + '].IsStatusClosed"  value="' + false + '"/>';
                        name += '<td class=""><select disabled name="ROCCommentList[' + meta.row + '].Status" class="form-control"> <option value = "Open" selected> Open</option ><option value="Closed">Closed</option></select></td >';
                    }
                    else {
                        //  name += '<input type="hidden" name="ROCCommentList[' + meta.row + '].IsStatusClosed"  value="' + true + '"/>';
                        //name += '<input type="hidden" name="ROCCommentList[' + meta.row + '].Status"  value="' + row.Status + '"/>' + row.Status;
                        name += '<input type="hidden" name="ROCCommentList[' + meta.row + '].IsStatusClosed"  value="' + false + '"/>';
                        name += '<td class=""><select disabled  name="ROCCommentList[' + meta.row + '].Status" class="form-control"> <option value = "Open" > Open</option ><option value="Closed" selected>Closed</option></select></td >';
                    }
                    return name;
                },
            },
            {
                "sTitle": "SR No.", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    return row.SrNo;
                },
            },
            {
                "sTitle": "Rev", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    return row.DocumentRevision;
                }
            },
            {
                "sTitle": "Customer Doc No", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var ClientDocNo = row.ClientDocNo || "";
                    //if (row.Status == 'Open') {
                    name = '<input type="text" readonly name="ROCCommentList[' + meta.row + '].ClientDocNo"  style="border: 1px solid #cccccc;"  class="form-control" maxlength="500" value="' + ClientDocNo + '"/>';
                    // }
                    // else {
                    //     name = '<input type="hidden" name="ROCCommentList[' + meta.row + '].ClientDocNo"  maxlength="500" value="' + ClientDocNo + '"/>' + ClientDocNo;
                    // }
                    return name;
                }
            },
            {
                "sTitle": "Customer Doc Rev", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var ClientDocRev = row.ClientDocRev || '';
                    // if (row.Status == 'Open') {
                    name = '<input type="text" readonly name="ROCCommentList[' + meta.row + '].ClientDocRev"  style="border: 1px solid #cccccc;"  class="form-control" maxlength="500" value="' + ClientDocRev + '"/>';
                    //  }
                    // else {
                    //     name = "<input type='hidden' maxlength='500' name='ROCCommentList[" + meta.row + "].ClientDocRev' value='" + ClientDocRev + "'/>" + ClientDocRev;
                    // }
                    return name;
                }
            },
            {
                "sTitle": "Comment Location", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var CommentLocation = row.CommentLocation || '';
                    // if (row.Status == 'Open') {
                    name = '<span class=""> <table class= "table" ><tr ><td><input type="text" class="form-control" style="border: 1px solid #cccccc;" maxlength="10"  name="ROCCommentList[' + meta.row + '].CommentLocation" value="' + CommentLocation + '"/></table></span>';
                   
                    //   }
                    //   else {
                    //      name = "<input type='hidden' maxlength='10' name='ROCCommentList[" + meta.row + "].CommentLocation'  value='" + CommentLocation + "'/>" + CommentLocation;
                    //  }
                    return name;
                }
            },
            {
                "sTitle": "Reviewing Agency Comment / Clarifications", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var AgencyComments = row.AgencyComments || '';
                    //if (row.Status == 'Open') {
                    //name = "<span class=''> <table class= 'table' ><tr><td><input type='text' maxlength='200' style='border: 1px solid #cccccc;' name='ROCCommentList[" + meta.row + "].AgencyComments'  rows='' class='form-control editroccomment' value ='"+ AgencyComments + "'/></td><td style='width: 30px'><a tabindex='-1' data-skip-on-tab='true' href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn - sm m-btn--icon-only m-btn--pill' title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",true,false,false)'><i class='la la-paperclip'></i></a></td></tr></table></span>";
                    name = "<span class=''> <table class= 'table' ><tr ><td><textarea name='ROCCommentList[" + meta.row + "].AgencyComments' style='border: 1px solid #cccccc;'   rows='' class='form-control editroccomment'>" + AgencyComments + "</textarea></td><td style='width: 30px'><a tabindex='-1' data-skip-on-tab='true' href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn - sm m-btn--icon-only m-btn--pill' title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",true,false,false)'><i class='la la-paperclip'></i></a></td></tr></table></span>";
                    //   }
                    //   else {
                    //      name = "<span class=''> <table class= 'table' ><tr ><td><input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyComments' value='" + AgencyComments + "'/> " + AgencyComments + "</td><td style='width: 30px'><a tabindex='-1' data-skip-on-tab='true' href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn - sm m-btn--icon-only m-btn--pill' title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",true,false,false)'><i class='la la-paperclip'></i></a></td></tr></table></span>";
                    //   }
                    return name;
                }
            },
            {
                "sTitle": "Date", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    // if (row.Status == 'Open') {
                    if (row.AgencyCommentsDate != null) {
                        name =  "<span class=''> <table class='table'><tr><td><input type='text' class='form-control datepickerroc'  style='border: 1px solid #cccccc;' name='ROCCommentList[" + meta.row + "].AgencyCommentsDate' value='" + row.AgencyCommentsDate + "'/> </table></span>";
                    } else {
                        name = "<span class=''> <table class='table'><tr><td><input type='text' class='form-control datepickerroc' style='border: 1px solid #cccccc;'  name='ROCCommentList[" + meta.row + "].AgencyCommentsDate' value=''/> </table></span>";
                    }
                    // }
                    //else {
                    //  if (row.AgencyResponseDate != null) {
                    //        name = "<input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyCommentsDate' value='" + row.AgencyCommentsDate + "'/>" + row.AgencyCommentsDate;
                    //    } else {
                    //        name = "<input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyCommentsDate' value='" + row.AgencyCommentsDate + "'/>";
                    //    }
                    // }
                    return name;
                }
            },
            {
                "sTitle": "L&T HEIC Reply / Resolution", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var LandTComment = row.LandTComment || '';
                    //    if (row.Status == 'Open') {
                    name = "<span class=''><table class= 'table'><tr><td><textarea name='ROCCommentList[" + meta.row + "].LandTComment' rows=''  style='border: 1px solid #cccccc;' class='form-control'>" + LandTComment + "</textarea></td><td style='width: 30px'><a href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",false,true,false)' ><i class='la la-paperclip'></i></a></td></tr></table></span>";
                    //    }
                    //    else {
                    //        name = "<span class=''><table class= 'table'><tr><td><input type='hidden' name='ROCCommentList[" + meta.row + "].LandTComment' value='" + LandTComment + "'/>" + LandTComment + "</td><td style='width: 30px'><a href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",false,true,false)' ><i class='la la-paperclip'></i></a></td></tr></table></span>";
                    //    }
                    return name;
                }
            },
            {
                "sTitle": "Date", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    //   if (row.Status == 'Open') {

                    if (row.LandTCommentDate != null) {
                        name = "<span class=''> <table class='table'><tr><td> <input type='text' class='form-control datepickerroc' style='border: 1px solid #cccccc;' name='ROCCommentList[" + meta.row + "].LandTCommentDate' value='" + row.LandTCommentDate + "'/> </table></span>";
                    } else {
                        name = "<span class=''> <table class='table'><tr><td><input type='text' class='form-control datepickerroc' style='border: 1px solid #cccccc;'  name='ROCCommentList[" + meta.row + "].LandTCommentDate' value=''/></table></span>";
                    }
                    //    }
                    //else {
                    //    if (row.AgencyResponseDate != null) {
                    //        name = "<input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyResponseDate' value='" + row.AgencyResponseDate + "'/>" + row.AgencyResponseDate;
                    //    } else {
                    //        name = "<input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyResponseDate' value='" + row.AgencyResponseDate + "'/>";
                    //    }

                    //}
                    return name;
                }
            },
            {
                "sTitle": "Reviewing Agency Response", "className": "", "sWidth": "", render: function (data, type, row, meta) {

                    var name;
                    var AgencyResponse = row.AgencyResponse || '';
                    // if (row.Status == 'Open') {
                    name = "<td class=''><table class= 'table'><tr><td><textarea  name='ROCCommentList[" + meta.row + "].AgencyResponse'  value='" + AgencyResponse + "' style='border: 1px solid #cccccc;' rows='' class='form-control editroccomment'>" + AgencyResponse + "</textarea></td><td style='width: 30px'><a href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",false,false,true)' ><i class='la la-paperclip'></i></a></td></tr></table></td>";
                    //  }
                    //else {
                    //    name = "<td class=''><table class= 'table'><tr><td><input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyResponse' value='" + AgencyResponse + "' value=''/>" + AgencyResponse + "</td><td style='width: 30px'><a href='javascript;;' class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' title='Upload Document' data-toggle='modal' data-target='#m_dropzone' onclick ='ROCCommentDropzone(" + row.ROCCommentsId + ",false,false,true)' ><i class='la la-paperclip'></i></a></td></tr></table></td>";
                    //}
                    return name;
                }
            },
            {
                "sTitle": "Date", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    // if (row.Status == 'Open') {

                    if (row.AgencyResponseDate != null) {
                        name = "<span class=''> <table class='table'><tr><td> <input type='text' class='form-control datepickerroc' style='border: 1px solid #cccccc;' name='ROCCommentList[" + meta.row + "].AgencyResponseDate' value='" + row.AgencyResponseDate + "'/></table></span>";
                    } else {
                        name = "<span class=''> <table class='table'><tr><td> <input type='text' class='form-control datepickerroc' style='border: 1px solid #cccccc;'  name='ROCCommentList[" + meta.row + "].AgencyResponseDate'/> </table></span>";
                    }
                    //}
                    //else {

                    //    if (row.AgencyResponseDate != null) {
                    //        name = "<input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyResponseDate' value='" + row.AgencyResponseDate + "'/>" + row.AgencyResponseDate;
                    //    } else {
                    //        name = "<input type='hidden' name='ROCCommentList[" + meta.row + "].AgencyResponseDate' value='" + row.AgencyResponseDate + "'/>";
                    //    }

                    //}
                    return name;
                }
            },
            {
                "sTitle": "Customer Sr. No.", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var CustomerSrNo = row.CustomerSrNo || '';
                    //   if (row.Status == 'Open') {
                    name = "<span class=''> <table class='table'><tr><td> <input type='text' maxlength='200' style='border: 1px solid #cccccc;' class='form-control' name='ROCCommentList[" + meta.row + "].CustomerSrNo' value='" + CustomerSrNo + "'/></table></span>";
                    //}
                    //else {
                    //    name = "<input type='hidden' maxlength='500' name='ROCCommentList[" + meta.row + "].CustomerSrNo' value='" + CustomerSrNo + "'/>" + CustomerSrNo;
                    //}
                    return name;
                }
            },
            {
                "sTitle": "Comments from - Customer Depts", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                    var name;
                    var CommfromCustDepts = row.CommfromCustDepts || '';
                    // if (row.Status == 'Open') {
                    name = "<span class=''> <table class='table'><tr><td> <textarea style='border: 1px solid #cccccc;' name='ROCCommentList[" + meta.row + "].CommfromCustDepts'  class='form-control'  value='" + CommfromCustDepts + "'>" + CommfromCustDepts +"</textarea></table></span>";
                    //}
                    //else {
                    //    name = "<input type='hidden'  name='ROCCommentList[" + meta.row + "].CommfromCustDepts' value='" + CommfromCustDepts + "'/>" + CommfromCustDepts;
                    //}
                    return name;
                }
            },
            {
                "sTitle": "Action", "className": "action ", render: function (data, type, row, meta) {
                    // if (row.Status == 'Open') {
                    var srt = "";
                    srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteROC" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    //srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onClick="DeleteROCComment(' + row.ROCCommentsId + ')"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                    //}
                    //else {
                    //    return '';
                    //}

                }
            }
        ],

        "initComplete": function () {
            var IsView = $("#IsView").val();
            if (IsView == "False") {
                test();
                AddDefaultRow();
            }
            tblROC.columns.adjust();
        },

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-roccommentid', aData.ROCCommentsId);
            var IsView = $("#IsView").val();
            if (IsView == "False") {
                test();
                AddDefaultRow();
            }
            tblROC.columns.adjust();
        },
        "preDrawCallback": function () {
            if (String($("#IsCommentChanges").val()) == "true") {
                $("#FormROC").submit();
                $("#IsCommentChanges").val("false");
            }
        }

    });

    GridROCCommentFiles();
    GridROCCommentedDocs();

    mApp.unblockPage();
});

$('#tblROC').on("keyup change", "input, select, textarea", function () {
    $("#IsCommentChanges").val("true");
});

function test() {
    var docMapp = {
        ROCId: $("#hdnRocId").val()
    };
    $.ajax({
        type: "POST",
        url: "/DES/ROC/GetJEPData", //If get File Then Document Mapping Add
        data: JSON.stringify(docMapp),
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            if (response.Status) {
                $('#JEPClientDocNo').val(response.Data[0]);
                $('#JEPClientDocRev').val(response.Data[1]);
                //toastr.success(response.Msg, "Success");
            } else {
                //toastr.error(response.Msg, "Not Success");
                //mApp.unblockPage();
            }
        },
        error: function (response) {
            // toastr.error(response.Msg, "Error");
            //mApp.unblockPage();
        }
    });

}

var ROCSuccess = function (d) {
    mApp.unblockPage();
    $("#IsCommentChanges").val("false");
    if (d != null) {
        if (d.Status) {
            toastr.success(d.Msg, "Success");
            if (d.SubmitType === "submit") {
                window.location = "/DES/ROC/Index";
            }
            else {
                window.location = "/DES/ROC/Index";
                //tblROC.ajax.reload();
            }

        } else {
            toastr.info(d.Msg, "Info");
        }
    }
};

var ROCBegin = function (d) {
    mApp.blockPage();
};

var CustomerDocNo;
var CustomerRevisionNo;
function AddDefaultRow() {

    setTimeout(function () {
        if ($("#status").length == 0) {

            //var srno = parseFloat($('#hdnlastSrNo').val());
            $('#tblROC > tbody').prepend('<tr>' +
                '<td class=""><select name="" id="status" class="form-control"> <option value = "Open"> Open</option ><option value="Closed">Closed</option></select></td > ' +
                '<td class="" id="srno"></td>' +
                '<td class=""><select name="" id="docrevision" class="form-control" onchange="BindSrNo();"></select></td>' + //<option value = "">R0</option> <option value="">R1</option>
                '<td class=""><input type="text" id="clientdocno" style="border: 1px solid #cccccc;" class="form-control" readonly value=' + $('#JEPClientDocNo').val() + '> </td>' +
                '<td class=""><input type="text" id="clientdocrev" style="border: 1px solid #cccccc;" class="form-control" readonly value=' + $('#JEPClientDocRev').val() + '></td>' +
                '<td class=""><input type="text" class="form-control" style="border: 1px solid #cccccc;" id="commentlocation" maxlength="10"></td>' +
                '<td class=""> <textarea style="border: 1px solid #cccccc;" name="" id="txtreviewagency" rows="" class="form-control"></textarea></td>' +            
                '<td class=""><input type="text" style="border: 1px solid #cccccc;" class="form-control datepickerroc" id="dtpreviewagency"  /> </td>' +
                '<td class=""><textarea name="" style="border: 1px solid #cccccc;" id="txtlntreply" rows="" class="form-control"></textarea></td >' +               
                '<td class=""><input type="text" style="border: 1px solid #cccccc;" class="form-control datepickerroc" id="dtplntreply"></td> ' +
                '<td class=""><textarea name="" style="border: 1px solid #cccccc;" id="txtreviewresponse" rows="" class="form-control"></textarea></td>' +               
                '<td class=""><input type="text" style="border: 1px solid #cccccc;" class="form-control datepickerroc" id="dtpreviewresponse"></td> ' +
                '<td class=""><input type="text" style="border: 1px solid #cccccc;" class="form-control" id="customersrno" maxlength="500"></td> ' +
                '<td class=""><textarea name="" style="border: 1px solid #cccccc;" id="txtdepts" rows="" class="form-control"></textarea></td>' +                 
                '<td class=""><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" title="Add" onclick="AddROCComments()"><i class="fa fa-plus"></i> Add </a ></td>' +
                '</tr>');
            tblROC.columns.adjust();
            BindDocRevision();
            Datepickeradd();
        }
    }, 1000);
}

function AddROCComments() {
    $("#IsCommentChanges").val("false");
    mApp.blockPage();
    var model = {};
    model.Status = $("#status").val();
    model.SrNo = $("#srno").html();
    model.ROCId = $("#hdnRocId").val();
    model.DocumentId = $("#DocumentId").val();
    model.DocumentRevision = $("#docrevision").val();
    model.ClientDocNo = $("#clientdocno").val();
    model.ClientDocRev = $("#clientdocrev").val();
    model.CommentLocation = $("#commentlocation").val();
    model.AgencyComments = $("#txtreviewagency").val();
    model.AgencyCommentsDate = $("#dtpreviewagency").val();
    model.LandTComment = $("#txtlntreply").val();
    model.LandTCommentDate = $("#dtplntreply").val();
    model.AgencyResponse = $("#txtreviewresponse").val();
    model.AgencyResponseDate = $("#dtpreviewresponse").val();
    model.CustomerSrNo = $("#customersrno").val();
    model.CommfromCustDepts = $("#txtdepts").val();

    $.ajax({
        type: "POST",
        url: '/DES/ROC/AddROCComment',
        data: JSON.stringify({ model: model }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                tblROC.ajax.reload();
                tblROCClear();

            } else {
                toastr.error(response.Msg, "Error");
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

function tblROCClear() {
    $("#status").prop("selectedIndex", 0);
    $("#docrevision").prop("selectedIndex", 0);
    $("#clientdocno").val('');
    $("#clientdocrev").val('');
    $("#commentlocation").val('');
    $("#txtreviewagency").val('');
    $("#dtpreviewagency").val('');
    $("#txtlntreply").val('');
    $("#dtplntreply").val('');
    $("#txtreviewresponse").val('');
    $("#dtpreviewresponse").val('');
    $("#customersrno").val('');
    $("#txtdepts").val('');
    BindSrNo();
}

function BindDocRevision() {
    mApp.blockPage();
    var docno = $("#hdnDocNo").val();
    $.ajax({
        url: "/DES/ROC/BindDocRevision",
        method: "post",
        data: { DocNo: docno },
        success: function (data) {
            mApp.unblockPage();
            if (data.length > 0) {
                $('#docrevision').html('');
                for (var i = 0; i < data.length; i++) {
                    $('#docrevision').append("<option value=" + data[i].RevisionId + ">" + data[i].DocRev + "</option>");
                }
                BindSrNo();
            }
            else {
                $('#docrevision').html('');
            }
        },
        failure: function (response) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        },
        error: function (response) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

function BindSrNo() {
    mApp.blockPage();
    var rocId = $("#hdnRocId").val();
    var documentRevision = $("#docrevision").val();
    $.ajax({
        url: "/DES/ROC/GetSrNo",
        method: "post",
        data: { ROCId: rocId, DocumentRevision: documentRevision },
        success: function (data) {
            mApp.unblockPage();
            $("#srno").html(parseFloat(data.SrNo.toFixed(1)));
        },
        failure: function (response) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        },
        error: function (response) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

function Datepickeradd() {
    mApp.blockPage();
    $(".datepickerroc").datepicker({
        rtl: mUtil.isRTL(),
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    mApp.unblockPage();
}

function EditROCComments() {
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/ROC/EditROCComment',
        data: JSON.stringify($('form').serialize()),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                window.location.href = response.RedURL;
                toastr.success(response.Msg, "Success");
                tblROC.ajax.reload();
            } else {
                toastr.info(response.Msg, "Info");

            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

function ROCCommentDropzone(ROCCommentsId, IsAgencyComment, IsLandTComment, IsAgencyResponse) {
    //When files are drop in DropZone
    mApp.blockPage();
    $('#hdnROCCommentsId').val(ROCCommentsId);
    $('#hdnIsAgencyComment').val(IsAgencyComment);
    $('#hdnIsLandTComment').val(IsLandTComment);
    $('#hdnIsAgencyResponse').val(IsAgencyResponse);

    var IsView = $("#IsView").val();
    if (IsView == "True") {
        $("#m-dropzone-two").hide();
    }
    else {
        $("#m-dropzone-two").show();
    }


    setTimeout(function () {
        tblROCCommentFiles.ajax.reload();
    }, 1000);
    mApp.unblockPage();
}

function GridROCCommentFiles() {
    tblROCCommentFiles = $('#tblROCCommentFiles').DataTable({
        "searching": false,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "serverSide": true,
        "Destroy": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/ROC/GetROCCommentsAttachList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ROCCommentsId", value: $('#hdnROCCommentsId').val() });
            data.push({ name: "IsAgencyComment", value: $('#hdnIsAgencyComment').val() });
            data.push({ name: "IsLandTComment", value: $('#hdnIsLandTComment').val() });
            data.push({ name: "IsAgencyResponse", value: $('#hdnIsAgencyResponse').val() });

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action ", render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                    var IsView = $("#IsView").val();
                    if (IsView == "False") {
                        srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteROCCommentsAttach" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    }

                    //srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onClick="DeleteROCCommentsAttach(' + row.ROCCommentsAttachId + ')"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "Document Name", render: function (data, type, row, meta) {
                    return row.DocumentTitle;
                }
            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            setGrid();
        }
    });
}

//function ROCCommentedDocDropzone(ROCId) {
//    mApp.blockPage();
//    //When files are drop in DropZone
//    $('#hdnRocId').val(ROCId);
//    var IsView = $("#IsView").val();

//    if (IsView == "True") {
//        $("#m-dropzone-commenteddoc").hide();
//    }
//    else {
//        $("#m-dropzone-commenteddoc").show();
//    }

//    setTimeout(function () {


//        tblROCCommentedDocs.ajax.reload();
//    }, 1000);
//    mApp.unblockPage();
//}

function GridROCCommentedDocs() {
    tblROCCommentedDocs = $('#tblROCCommentedDocs').DataTable({
        "searching": false,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/ROC/GetROCCommentedDocumentList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ROCId", value: $('#hdnRocId').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action ", render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    //srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                    //var IsView = $("#IsView").val();
                    //if (IsView == "False") {
                    //   srt += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteROCCommentedDoc" title="Delete"><i class="la la-trash text-danger"></i></a>';

                    //}

                    //srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onClick="DeleteROCCommentedDoc(' + row.ROCCommentedDocId + ',' + row.DocumentPath + ')"><i class="la la-trash text-danger"></i></a>';
                    //srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDOC" title="View"><i class="la la-eye m--font-success"></i></a>';
                    return srt;
                }
            },
            {

                "sTitle": "Document Name", render: function (data, type, row, meta) {
                    return row.DocumentTitle;
                }
            },
            {
                "sTitle": "Document No", "visible": false, render: function (data, type, row, meta) {
                    $('#documentNo').val(row.DocNo);
                    return row.DocNo;
                }
            },
            {
                "sTitle": "Document Rev", "visible": false, render: function (data, type, row, meta) {
                    $('#documentRevision').val("R" + row.DocRev);
                    return row.DocRev;
                }
            },
            {
                "sTitle": "Document Remark", "visible": false, render: function (data, type, row, meta) {
                    $('#documentRemark').val(row.Remark);
                    return row.Remark;
                }
            }
            ,
            {
                "sTitle": "Created By", render: function (data, type, row, meta) {

                    return row.CreatedBy;
                }
            }
                ,
                  {
                "sTitle": "Created On", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            }           
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        }
    });
}

function setGrid() {
    setTimeout(function () {
        tblROCCommentedDocs.columns.adjust();
    }, 700);
}

var path;
$('#tblROCCommentedDocs').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblROCCommentedDocs.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

$('#tblROCCommentedDocs').on('click', 'td .btnViewDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblROCCommentedDocs.row(tr);
    var data = row.data();
    window.location = "/DES/DOC/Detail?q=" + data.QString;
});

$('#tblROCCommentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblROCCommentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

//var myDropzonecommenteddoc = new Dropzone('div#m-dropzone-commenteddoc');
//myDropzonecommenteddoc.on('sending', function (file, xhr, formData) {
//    var CurrentLocationIp = $('#CurrentLocationIp').val();
//    var FCSurl = CurrentLocationIp + "/api/Upload";
//    if (CurrentLocationIp) {
//        mApp.blockPage();

//        var RocId = $('#hdnRocId').val();
//        var Project = $('#hdnProject').val();
//        var ROCRefNo = $('#hdnROCRefNo').val();
//        var concatDocandProject = Project.toString().trim() + "-ROC-" + ROCRefNo.toString().trim();
//        formData.append('DocumentPath', concatDocandProject.toString());
//        formData.append('DocumentFile', file);
//        //Need To get Status and other filed HERE 
//        jQuery.ajax({
//            url: FCSurl,
//            data: formData,
//            enctype: 'multipart/form-data',
//            cache: false,
//            async: true,
//            contentType: false,
//            processData: false,
//            method: 'POST',
//            type: 'POST', // For jQuery < 1.9
//            success: function (response) {
//                mApp.unblockPage();

//                var fname = response.SavedFileName;
//                var filename = response.OriginalFileName;

//                var getExtention = filename.split('.')[1];


//                var path = response.SavedPath + '/' + fname;
//                var ROCCommentedDoc = {
//                    ROCId: RocId,
//                    DocumentPath: path,
//                    DocumentFormat: getExtention,
//                    DocumentTitle: filename
//                };
//                $.ajax({
//                    url: "/DES/ROC/CreateCommentedDocument", //If get File Then Document Mapping Add
//                    data: JSON.stringify(ROCCommentedDoc),
//                    type: "POST",
//                    contentType: "application/json;charset=utf-8",
//                    dataType: "json",
//                    aysnc: true,
//                    success: function (response) {
//                        mApp.unblockPage();
//                        if (response.Status) {
//                            toastr.success(response.Msg, "Success");
//                            tblROCCommentedDocs.ajax.reload();
//                        } else {
//                            toastr.error(response.Msg, "Not Success");
//                        }
//                        myDropzonecommenteddoc.removeFile(file);
//                    },
//                    error: function (response) {
//                        mApp.unblockPage();
//                        toastr.error(response.Msg, "Error");
//                    }
//                });
//            }
//        }).fail(function (data) {
//            mApp.unblockPage();
//            myDropzonecommenteddoc.removeFile(file);
//            toastr.error("Server response not received", "Error");
//            $('div.dz-filename').css('display', 'none');
//            $('div.dz-filename').closest($('.dz-details').css('display', 'none'));
//            $('div.dz-filename').closest($('.dz-image').css('display', 'none'));
//        });
//    }
//    else {
//        toastr.error("Ip is not detecting", "Error");
//        myDropzonecommenteddoc.removeFile(file);
//    }


//});


var iii = 1;
var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    parallelUploads: 10000,
    maxFilesize: 200000,
    maxFiles: 10,
    init: function () {
        this.on("maxfilesexceeded", function (file) {

            if (this.files.length == 11) {
                // this.removeFile(this.files[0]);
                if (iii == 1) {
                    alert("10 Files can be uploded at once!");
                }
                iii++;
            }
            myDropzone.removeFile(file);
            myDropzone.removeAllFiles();
        });

    },
    queuecomplete: function () {
        iii = 1;
    }

});

myDropzone.on('sending', function (file, xhr, formData) {
    mApp.blockPage();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var CurrentLocationPath = $('#FilePath').val();
    var FCSurl = CurrentLocationIp + "/api/Upload";
    if (CurrentLocationIp) {
        var ROCCommentsId = $('#hdnROCCommentsId').val();
        var Project = $('#hdnProject').val();
        var IsAgencyComment = $('#hdnIsAgencyComment').val();
        var IsLandTComment = $('#hdnIsLandTComment').val();
        var IsAgencyResponse = $('#hdnIsAgencyResponse').val();
        var ROCRefNo = $('#hdnROCRefNo').val();
        var Version = $('#Version').val();
        var concatDocandProject = Project.toString().trim() + "-ROC-" + ROCRefNo.toString().trim() + "-" + Version;
        formData.append('SavedPath', CurrentLocationPath.toString());
        formData.append('DocumentPath', concatDocandProject.toString());
        formData.append('DocumentFile', file);
        //Need To get Status and other filed HERE 
        jQuery.ajax({
            url: FCSurl,
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function (response) {
                mApp.unblockPage();
                var fname = response.SavedFileName;
                var filename = response.OriginalFileName;
                var getExtention = filename.split('.')[1];

                var path = response.SavedPath + '/' + fname;
                var ROCComment = {
                    ROCCommentsId: ROCCommentsId,
                    DocumentPath: path,
                    DocumentFormat: getExtention,
                    IsAgencyComment: IsAgencyComment,
                    IsLandTComment: IsLandTComment,
                    IsAgencyResponse: IsAgencyResponse,
                    DocumentTitle: filename
                };
                $.ajax({
                    url: "/DES/ROC/CreateROCCommentsAttach", //If get File Then Document Mapping Add
                    data: JSON.stringify(ROCComment),
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    aysnc: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            myDropzone.removeFile(file);
                            tblROCCommentFiles.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Not Success");
                        }
                    },
                    error: function (response) {
                        mApp.unblockPage();
                        toastr.error(response.Msg, "Error");
                    }
                });
            }
        }).fail(function (data) {
            mApp.unblockPage();
            myDropzone.removeFile(file);
            toastr.error("Server response not received", "Error");
            $('div.dz-filename').css('display', 'none');
            $('div.dz-filename').closest($('.dz-details').css('display', 'none'));
            $('div.dz-filename').closest($('.dz-image').css('display', 'none'));
        });
    }
    else {
        toastr.error("Ip is not detecting", "Error");
        myDropzone.removeFile(file);
    }

});

$('#tblROC').on('click', 'td .btnDeleteROC', function () {
    var tr = $(this).closest('tr');
    var row = tblROC.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";
    swal({
        title: "Do you want to delete?",
        text: "",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                deleteroccomment(data.ROCCommentsId);
            }
            mApp.unblockPage();
        });
});

function deleteroccomment(ROCCommentsId) {
    DeleteAllAttachFilesByROCCommentId(ROCCommentsId);
}

function DeleteAllAttachFilesByROCCommentId(ROCCommentsId) {
    var obj = {
        ROCCommentId: ROCCommentsId
    };

    $.ajax({
        url: "/DES/ROC/GetAllAttachmentPath", //If get File Then Document Mapping Add
        data: JSON.stringify(obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                var i;
                for (i = 0; i < response.AttachFiles.length; i++) {
                    deleteROCCommentsAttach(response.AttachFiles[i].DocumentPath, response.AttachFiles[i].ROCCommentsAttachId);
                }
                //delete main record
                var obj = {
                    ROCCommentId: ROCCommentsId
                };

                $.ajax({
                    url: "/DES/ROC/DeleteROCComment", //If get File Then Document Mapping Add
                    data: JSON.stringify(obj),
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    aysnc: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblROC.ajax.reload();
                            var IsView = $("#IsView").val();
                            if (IsView == "False") {
                                test();
                                AddDefaultRow();
                            }
                        } else {
                            toastr.error(response.Msg, "Not Success");
                        }
                    },
                    error: function (response) {
                        mApp.unblockPage();
                        toastr.error(response.Msg, "Error");
                    }
                });
            }
        },
        error: function (response) {
            mApp.unblockPage();
            toastr.error(response.Msg, "Error");
        }
    });
}

$('#tblROCCommentFiles').on('click', 'td .btnDeleteROCCommentsAttach', function () {
    var tr = $(this).closest('tr');
    var row = tblROCCommentFiles.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to delete?",
        text: "",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                deleteROCCommentsAttach(data.DocumentPath, data.ROCCommentsAttachId);
            }
            mApp.unblockPage();
        });
});

function deleteROCCommentsAttach(DocumentPath, ROCCommentsAttachId) {
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";

    var docdelete = {
        deletePath: DocumentPath
    };

    $.ajax({
        url: FCSurl,
        data: JSON.stringify(docdelete),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        cache: false,
        async: true,
        processData: false,
        method: 'POST',
        type: 'POST', // For jQuery < 1.9
        success: function (response) {
            if (response) {
                toastr.success(response.Msg, "Successfully removed file from server");

                var obj = {
                    Id: ROCCommentsAttachId
                };

                $.ajax({
                    url: "/DES/ROC/DeleteROCCommentsAttach", //If get File Then Document Mapping Add
                    data: JSON.stringify(obj),
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    aysnc: true,
                    success: function (response) {

                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblROCCommentFiles.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Not Success");
                        }
                    },
                    error: function (response) {
                        mApp.unblockPage();
                        toastr.error(response.Msg, "Error");
                    }
                });
            }
        },
        error: function (error) {
            toastr.error("Error while removing file from server", "Error");
            mApp.unblockPage();
        }
    });
}

$('#tblROCCommentedDocs').on('click', 'td .btnDeleteROCCommentedDoc', function () {
    var tr = $(this).closest('tr');
    var row = tblROCCommentedDocs.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";
    swal({
        title: "Do you want to delete?",
        text: "",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();

                var docdelete = {
                    deletePath: data.DocumentPath
                };

                $.ajax({
                    url: FCSurl,
                    data: JSON.stringify(docdelete),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (response) {
                        if (response) {
                            toastr.success(response.Msg, "Successfully removed file from server");

                            var obj = {
                                Id: data.ROCCommentedDocId
                            };

                            $.ajax({
                                url: "/DES/ROC/DeleteROCCommentedDocument", //If get File Then Document Mapping Add
                                data: JSON.stringify(obj),
                                type: "POST",
                                contentType: "application/json;charset=utf-8",
                                dataType: "json",
                                aysnc: true,
                                success: function (response) {
                                    mApp.unblockPage();
                                    if (response.Status) {
                                        toastr.success(response.Msg, "Success");
                                        tblROCCommentedDocs.ajax.reload();
                                    } else {
                                        toastr.error(response.Msg, "Not Success");
                                    }
                                },
                                error: function (response) {
                                    mApp.unblockPage();
                                    toastr.error(response.Msg, "Error");
                                }
                            });
                        }
                    },
                    error: function (error) {
                        toastr.error("Error while removing file from server", "Error");
                        mApp.unblockPage();
                    }
                })

            }
            mApp.unblockPage();

        });
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblROC != null)
        tblROC.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblROCCommentedDocs != null)
        tblROCCommentedDocs.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblROCCommentFiles != null)
        tblROCCommentFiles.columns.adjust();
});

$("#btnView").click(function () {
    setGrid();
});

$('#tblROCCommentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblROCCommentFiles.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblROCCommentedDocs').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblROCCommentedDocs.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});
