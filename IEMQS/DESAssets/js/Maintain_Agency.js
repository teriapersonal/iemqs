﻿var tblAgency;
$(document).ready(function () {

    tblAgency = $('#tblAgency').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "auto-width": false,
        "sAjaxSource": "/Agency/GetAgencyList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Agency Name <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var AgencyName = "<strong style='text-transform: capitalize;'>" + row.AgencyName + "</strong>";
                    return AgencyName;
                }
            },
            {
                "sTitle": "Agency Description <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.AgencyDescription;
                }
            },
            {
                "sTitle": "BU / PBU <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", "sWidth": "180", render: function (data, type, row, meta) {
                    return row.BU + "-" + row.PBU;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddAgencyRow();
        },

        "fnRowCallback": function () {
            AddAgencyRow();
        }
    });
});

function AddAgencyRow() {
    setTimeout(function () {
        $("#AgencyName_0").focus();
        if ($("#AgencyName_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar noSpace" id="AgencyName_0" Name="AgencyName_0" placeholder="Agency Name" /><label class="text-danger" id="errorAgencyName_0" style="display:none" > Agency Name is required </label></td> '
                + '<td><input type="text" maxlength="200" class="form-control" id="AgencyDescription_0" Name="AgencyDescription_0" placeholder="Description" /><label class="text-danger" id="errorAgencyDescription_0" style="display:none" > Agency Description is required </label></td> '
                + '<td><select id="BU_0" class= "form-control"><option value="">Select</option>';
        for (let i = 0; i < ddlbulist.length; i++) {
            html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        }
        html += '</select><br><label class="text-danger" id="errorBU_0" style="display:none" > BU / PBU is required </label></td> '
            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddAgency(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>'
        $('#tblAgency tbody').prepend(html);
        $("#BU_0").select2();
    }, 1000);
}

function AddAgency(id) {
    $("#errorAgencyName_" + id).hide();
    $("#errorAgencyDescription_" + id).hide();
    $("#errorBU_" + id).hide();
    var name = $("#AgencyName_" + id).val();
    var desc = $("#AgencyDescription_" + id).val();
    var bu = $("#BU_" + id).val();
    if (name != "" && desc != "" && bu != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/Agency/AddAgency',
            data: JSON.stringify({ AgencyId: id, AgencyName: name, AgencyDescription: desc, BUId: bu }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblAgency.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (name == "")
            $("#errorAgencyName_" + id).show();
        if (desc == "")
            $("#errorAgencyDescription_" + id).show();
        if (bu == "")
            $("#errorBU_" + id).show();
    }

}

$('#tblAgency').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblAgency.row(tr);
    var data = row.data();
    var html = '<td><input type="text" maxlength="50" class="form-control blockSpecialChar noSpace" id="AgencyName_' + data.AgencyId + '" name="AgencyName_' + data.AgencyId + '" placeholder="Agency Name" value="' + data.AgencyName + '" /> <label class="text-danger" id="errorAgencyName_' + data.AgencyId + '" style="display:none" > Agency Name is required </label></td> '
        + '<td><input type="text" maxlength="200" class="form-control" id="AgencyDescription_' + data.AgencyId + '" name="AgencyDescription_' + data.AgencyId + '" placeholder="Description" value="' + data.AgencyDescription + '" /> <label class="text-danger" id="errorAgencyDescription_' + data.AgencyId + '" style="display:none" > Agency Description is required </label></td> '
        + '<td><select id="BU_' + data.AgencyId + '" name="BU_' + data.AgencyId + '" value="' + data.BU + '" class= "form-control">';
        for (let i = 0; i < ddlbulist.length; i++) {
            if (ddlbulist[i].BUId == data.BUId)
                html += "<option selected='selected' value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
            else
                html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        }
        html += '</select><label class="text-danger" id="errorBU" style="display:none" > BU/PBU is required </label></td> '
        + '<td></td><td></td><td></td><td></td>'
        + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddAgency(' + data.AgencyId + ')" title="Update"><i class="la la-check text-info"></i></a>'
        + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblAgencyClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>'
    $(tr).html(html);
    $("#AgencyName_" + data.AgencyId).focus();
    $("#BU_" + data.AgencyId).select2();
});

function tblAgencyClear() {
    tblAgency.ajax.reload();
}

$('#tblAgency').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblAgency.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Agency/DeleteAgency',
                    data: { Id: data.AgencyId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblAgency.ajax.reload();
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblAgency != null)
        tblAgency.columns.adjust();
});