﻿$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    tblDocumentList.columns.adjust();
});
var selectedAgencyDocIDs = [];
var WeightageList = [];
var TotalWeightagesum = 0;
var TotalWeightage = 0;
var activeWeightagevalue = 0;
$(document).ready(function () {
    BindGlobalJEPDoc();
    BindpreviewJEPDoc();
    funGetWeightage();
    WizardDemo.init();
});

//BeginDocumentList = function (data) {
//    if ($("#FormMaintainJEP").valid() == true)
//        $("#btnSaveContinue").submit();
//};


var DocBegin = function (d) {
    mApp.blockPage();
};

var WizardDemo = function () {
    $("#m_wizard");
    var e, r, i = $("#m_form"), f = $("#FormMaintainJEP");
    return {
        init: function () {
            var n;
            $("#m_wizard"), i = $("#m_form"), (r = new mWizard("m_wizard", {
                startStep: 1
            })).on("beforeNext", function (r) {

            }), r.on("change", function (e) {
                mUtil.scrollTop()
            }), r.on("change", function (e) {
                1 === e.getStep();
            })
        }

        , Gotonext: function () {
            r.goNext();

        }
        , GotoBack: function () {
            r.goPrev();
            e.goNext();
        }
        , GoTo: function (i) {
            if ($("#JEPId").val() != "") {
                if ($('#IsJEPCheckIn').val() != "True") {
                    r.goTo(i);
                    if (i == 2) {
                        $("#Department_0").select2();
                        $("#DocumentType_0").select2();
                        $("#MilestoneDocument_0").select2();

                        var datarow = tblDocumentList.rows().data();
                        datarow.each(function (value, index) {
                            $("select[name$='JEPDocumentList[" + index + "].DocumentTypeId']").select2();
                            $("select[name$='JEPDocumentList[" + index + "].Department']").select2();
                            $("select[name$='JEPDocumentList[" + index + "].MilestoneDocument']").select2();
                        });

                        $('#tblDocumentList tr').addClass('v-top');
                        tblDocumentList.columns.adjust();
                        tblDocumentList.ajax.reload();
                    }
                }
            }
        }
    };
}();

var DOCSuccess = function (d) {
    mApp.unblockPage();
    $("#IsUpdateChanges").val("false");
    if (d != null) {
        if (d.Status) {
            //toastr.success(d.Msg, "Success");
            if (d.Submitted) {
                //toastr.success(d.Msg, "Success");
                //tblDocumentList.ajax.reload();
                window.location.href = "/DES/JEP";
            }
            else {
                //tblDocumentList.ajax.reload();
                //window.location.href = "/DES/JEP";
            }

        } else {
            toastr.info((d.Msg).replace(/&lt;/g, '<').replace(/&gt;/g, '>'), "Info");
            //window.location.href = "/DES/JEP";
        }
        // window.location.href = "/DES/JEP";
    }
};

funBegin = function (data) {
    mApp.blockPage();
};
funBeginImportExcel = function (data) {
    mApp.blockPage();
};

funSuccess = function (data) {
    mApp.unblockPage();
    if (data.Status) {
        if (!data.InfoMsg) {
            $('#JEPId').val(data.model.JEPId);
            $('#ExcelImportJEPId').val(data.model.JEPId);
            $('#Doc_JEPId').val(data.model.JEPId);
            $('#IsDEENGGUSer').val(data.model.IsDEENGGUSer);
            var JEPId = (data.model.JEPId);
            var docID = {
                JEPId: JEPId
            };

            WizardDemo.Gotonext();
            $('#tblDocumentList tr').addClass('v-top');
            tblDocumentList.ajax.reload();

            AddDocList();
            funGetWeightage();
        }
        else {
            toastr.info(data.Msg, "Info");
        }

    } else {
        toastr.error(data.Msg, "Error");
    }
};

$("#ddlProduct").change(function () {

    var valProductId = $('#ddlProduct').val();
    if (valProductId) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/JEP/TemplateListByProductId",
            data: { "ProductId": parseInt(valProductId) },
            datatype: "json",
            async: true,
            success: function (data) {

                mApp.unblockPage();
                $("#ddlTemplate").html("");
                $('#ddlTemplate').append("<option value=''>Select</option>");
                $.each(data, function (key, entry) {
                    $("#ddlTemplate").append($('<option></option>').attr('value', entry.JEPTemplateId).text(entry.TemplateName));
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
                $("#ddlTemplate").html("");
            }
        });
    }
    else {
        $("#ddlTemplate").html("");
        $("#ddlTemplate").append($('<option></option>').text('Select'));
    }
});

var tblDocumentList;
var tblpreviewDoc;
var Authorized = parseInt($("#Authorized").val() || 0);

$(document).ready(function () {

    // mApp.blockPage();
    tblDocumentList = $('#tblDocumentList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/JEP/GetDocumentList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "fixedColumns": {
            leftColumns: 3
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ],



        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "JEPId", value: $("#JEPId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    var Authorized = parseInt($("#Authorized").val() || 0);
                    if (Authorized > 0) {
                        if (row.MainDocumentPath != null && row.MainDocumentPath != '') {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                        }
                        if (!row.IsDOCTransmittalCreated) {
                            //srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                            if (row.DOCStatus !== "Completed") {
                                srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                            }
                        }

                    }
                    return srt;
                }
            },
            {
                "sTitle": "Doc No <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {

                    //if (row.DOCStatus == 'Draft') {
                    //    var name = '<input type="text" maxlength="50" class="form-control" name="JEPDocumentList[' + meta.row + '].DocNo" placeholder="Doc No/> <label class="text-danger" id=errorDocNo_' + meta.row + ' style="display:none" >Doc No is required </label>';
                    //    //id='DocNo_" + meta.row + "'  name='JEPDocumentList DocNo_" + meta.row + "' placeholder='Doc No' /> <label class='text-danger' id='errorDocNo_" + meta.row + "' style='display:none' >Doc No is required </label>";
                    //}
                    //else {
                    //    var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                    //}
                    var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocNo"  value="' + row.DocNo + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocDescription"  value="' + row.DocDescription + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].JEPDocumentDetailsId"  value="' + row.JEPDocumentDetailsId + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocumentId"  value="' + row.DocumentId + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].RevisionNo"  value="' + row.RevisionNo + '"/>';
                    return name;
                }
            },
            {
                "sTitle": "Description <span class='text-danger'>*</span>", "className": "text-wrap custom-table-width", render: function (data, type, row, meta) {
                    var Description = "<strong style='text-transform: capitalize;'>" + (row.DocDescription || '') + "</strong>";
                    return Description;
                    //var Description = "<strong style='text-transform: capitalize;'>" + row.DocDescription + "</strong>";
                    //return Description;
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    return "R" + row.RevisionNo;
                }
            },
            {
                "sTitle": "Customer Doc No", "className": "", render: function (data, type, row, meta) {
                    if (Authorized > 0) {
                        if (!row.IsDOCTransmittalCreated) {
                            var Description = '<input type="text" maxlength="100" class="form-control" name="JEPDocumentList[' + meta.row + '].CustomerDocNo" placeholder="Cust Doc No" value="' + (row.CustomerDocNo || '') + '"/>';
                            return Description;
                        }
                        else {
                            return row.CustomerDocNo;
                        }
                    }
                    else {
                        return row.CustomerDocNo || '';
                    }
                }
            },
            {
                "sTitle": "Customer Doc Rev", "className": "", render: function (data, type, row, meta) {
                    if (Authorized > 0) {
                        if (!row.IsDOCTransmittalCreated) {
                            var Description = '<input type="text" maxlength="20" class="form-control" name="JEPDocumentList[' + meta.row + '].CustomerDocRevision" placeholder="Customer Doc Rev" value="' + (row.CustomerDocRevision || '') + '"/>';
                            return Description;
                        }
                        else {
                            return row.CustomerDocRevision;
                        }
                    }
                    else {
                        return row.CustomerDocRevision || '';
                    }

                }
            },
            {
                "sTitle": "Document Type <span class='text-danger'>*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>", "className": "", render: function (data, type, row, meta) {
                    if ($('#IsDEENGGUSer').val().toLowerCase() == "True".toLowerCase()) {
                        if (row.DocumentId > 0) {
                            var name = '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocumentTypeId"  value="' + row.DocumentTypeId + '"/>';
                            name += row.DocumentType;
                            return name;
                        }
                        else {
                            var docType = '';
                            docType += '<select class="form-control" name="JEPDocumentList[' + meta.row + '].DocumentTypeId">';
                            docType += "<option value=''>Select</option>";
                            for (let i = 0; i < ddlDocumentList.length; i++) {
                                if (ddlDocumentList[i].Id == row.DocumentTypeId)
                                    docType += "<option value='" + ddlDocumentList[i].Id + "' selected>" + ddlDocumentList[i].Lookup + "</option>";
                                else
                                    docType += "<option value='" + ddlDocumentList[i].Id + "'>" + ddlDocumentList[i].Lookup + "</option>";
                            }
                            docType += '</select></br><label class="text-danger" id="errorDocumentType_' + row.JEPDocumentDetailsId + '" style="display:none" >Document Type is required </label>'
                            return docType;
                        }
                    }
                    else {
                        return row.DocumentType;
                    }
                }
            },
            {
                "sTitle": "Department <span class='text-danger'>*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>", "className": "", render: function (data, type, row, meta) {
                    if ($('#IsDEENGGUSer').val().toLowerCase() == "True".toLowerCase()) {
                        if (row.DocumentId > 0) {
                            var name = '<input type="hidden" name="JEPDocumentList[' + meta.row + '].Department"  value="' + row.Department + '"/>';
                            name += row.DepartmentName;
                            return name;
                        }
                        else {
                            var deptName = '';
                            deptName += '<select class="form-control" name="JEPDocumentList[' + meta.row + '].Department" >';
                            deptName += "<option value=''>Select</option>";
                            for (let i = 0; i < ddlRole.length; i++) {
                                if (ddlRole[i].t_dimx == row.Department)
                                    deptName += "<option value='" + ddlRole[i].t_dimx + "' selected>" + ddlRole[i].t_desc + "</option>";
                                else
                                    deptName += "<option value='" + ddlRole[i].t_dimx + "'>" + ddlRole[i].t_desc + "</option>";
                            }
                            deptName += '</select></br><label class="text-danger" id="errorDepartment_' + row.JEPDocumentDetailsId + '" style="display:none" >Department is required </label> ';
                            return deptName;
                        }
                    }
                    return row.DepartmentName;
                }
            },
            {
                "sTitle": "Planned Date <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    if (Authorized > 0) {
                        if (!row.IsDOCTransmittalCreated) {
                            var date = '<input type="text" maxlength="" class="form-control datepickerroc" name="JEPDocumentList[' + meta.row + '].PlannedDate" placeholder="Planned Date" value="' + (row.PlannedDate || '') + '" /><label class="text-danger" id="errorPlannedDate_' + row.JEPDocumentDetailsId + '" style="display:none" >Planned Date is required </label>';
                            return date;
                        }
                        else {
                            var plandate = row.PlannedDate || '';
                            var planndate = "<strong style='text-transform: capitalize;'>" + plandate + "</strong>";
                            return planndate;
                        }
                    }
                    else {
                        return row.PlannedDate || '';
                    }

                }
            },
            //{
            //    "sTitle": "Weightage ", "className": "", render: function (data, type, row, meta) {
            //        if (Authorized > 0) {
            //            if (!row.IsDOCTransmittalCreated) {
            //                var Weightage = '<input type="text" maxlength="3" class="form-control onlyNumeric numeric percentage" name="JEPDocumentList[' + meta.row + '].Weightage" placeholder="Weightage" value="' + (row.Weightage || '') + '" /><label class="text-danger" id="errorWeightage_' + row.JEPDocumentDetailsId + '" style="display:none" >Total Weightage should be 100%. </label>';
            //                return Weightage;
            //            }
            //            else {
            //                return row.Weightage;
            //            }
            //        }
            //        else {
            //            return row.Weightage || '';
            //        }
            //    }
            //},
            {
                "sTitle": "Milestone Document <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    if (Authorized > 0) {
                        if (!row.IsDOCTransmittalCreated) {
                            var Weightage = '<select class="form-control" name="JEPDocumentList[' + meta.row + '].MilestoneDocument">';
                            Weightage += "<option value=''>Select</option>";
                            var MilestoneDocument = row.MilestoneDocument || false;

                            if (MilestoneDocument) {
                                Weightage += "<option value='false' >No</option>";
                                Weightage += "<option value='true' selected>Yes</option>";
                            }
                            else {
                                Weightage += "<option value='false' selected>No</option>";
                                Weightage += "<option value='true'>Yes</option>";
                            }

                            Weightage += '</select></br><label class="text-danger" id="errorMilestoneDocument_' + row.JEPDocumentDetailsId + '" style="display:none" >Milestone Document is required </label>';
                            return Weightage;
                        }
                        else {
                            var milestoneDocument = row.MilestoneDocument || false;
                            var milestoneDocumentdata = (milestoneDocument) ? 'Yes' : 'No';
                            return milestoneDocumentdata;
                        }
                    }
                    else {
                        var milestoneDocument = row.MilestoneDocument || false;
                        var milestoneDocumentdata = (milestoneDocument) ? 'Yes' : 'No';
                        return milestoneDocumentdata;
                    }
                }
            }
        ],

        "initComplete": function (row) {


            // AddDocList();


        },

        "fnRowCallback": function () {


            AddDocList();


        },
        "preDrawCallback": function () {
            if (String($("#IsUpdateChanges").val()) == "true") {
                if ($("#formAddCustomerFeedback").valid() == true)
                    $("#formAddCustomerFeedback").submit();
                $("#IsUpdateChanges").val("false");
            }
        },


        //fnFooterCallback: function (row, data, start, end, display) {
        //    var api = this.api();

        //    if (!($("#TxtTotalWeightage").length > 0)) {
        //        var footer = $(this).append('<tfoot><tr></tr></tfoot>');
        //        $(footer).append('<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>Total Weightage:</th><th><input type="text" style="width:100%;" class="form-control" readonly="readonly" id="TxtTotalWeightage"></th><th></th><th></th>');
        //        funPrintTotalWeightage();
        //    }
        //}
    });
    // mApp.unblockPage();

    $("#Department_0").select2();
    $("#DocumentType_0").select2();
    $("#MilestoneDocument_0").select2();
    Datepickeradd();
    funPrintTotalWeightage();
});

$('#tblDocumentList').on("keyup change", "input, select, textarea", function () {
    $("#IsUpdateChanges").val("true");

});

function AddDocList() {

    $("#IsUpdateChanges").val("false");
    setTimeout(function () {

        var Authorized = parseInt($("#Authorized").val() || 0);

        if (Authorized > 0) {


            if ($("#DocNo_0").length == 0)
                var html = '<tr>'
                    + '<td><input type="text" maxlength="50" class="form-control" id="DocNo_0" name="DocNo_0" placeholder="Doc No" /> <label class="text-danger" id="errorDocNo_0" style="display:none" >Doc No is required </label></td> '
                    + '<td><input type="text" maxlength="200" class="form-control" id="Description_0" name="Description_0" placeholder="Description" /> <label class="text-danger" id="errorDescription_0" style="display:none" >Description is required </label></td> '
                    + '<td><input type="text" maxlength="4" readonly="readonly" value="" class="form-control" id="RevNo_0" name="RevNo_0" placeholder="" style="width: 55px;" /><label class="text-danger" id="errorRevNo_0" style="display:none;" >Rev No is required </label></td> '
                    + '<td><input type="text" maxlength="100" class="form-control" id="CustomerDocNo_0" name="CustomerDocNo_0" placeholder="Cust Doc No" /></td> '
                    + '<td><input type="text" maxlength="20" class="form-control" id="CustomerDocRevision_0" name="CustomerDocRevision_0" placeholder="Cust Doc Revision" /> </td> '

                    + '<td><select class="form-control" id="DocumentType_0">';
            html += "<option value=''>Select</option>";
            for (let i = 0; i < ddlDocumentList.length; i++) {
                html += "<option value='" + ddlDocumentList[i].Id + "'>" + ddlDocumentList[i].Lookup + "</option>";
            }
            html += '</select></br><label class="text-danger" id="errorDocumentType_0" style="display:none" >Document Type is required </label></td> '


                + '<td><select class="form-control" id="Department_0">';
            html += "<option value=''>Select</option>";
            for (let i = 0; i < ddlRole.length; i++) {
                html += "<option value='" + ddlRole[i].t_dimx + "'>" + ddlRole[i].t_desc + "</option>";
            }
            html += '</select></br><label class="text-danger" id="errorDepartment_0" style="display:none" >Department is required </label></td> '


                + '<td><input type="text"  class="form-control datepickerroc" id="PlannedDate_0" name="PlannedDate_0" placeholder="Planned Date" /><label class="text-danger" id="errorPlannedDate_0" style="display:none" value="">Planned Date is required </label></td> '
                + '<td><input type="text" maxlength="3" class="form-control onlyNumeric numeric percentage" id="Weightage_0" name="Weightage_0" placeholder="Weightage" value="0" /><label class="text-danger" id="errorWeightage_0" style="display:none" >Total Weightage should be 100%. </label></td> '
                + '<td><select class="form-control" id="MilestoneDocument_0">';
            html += "<option value=''>Select</option>";
            html += "<option value='false' selected>No</option>";
            html += "<option value='true'>Yes</option>";

            html += '</select></br><label class="text-danger" id="errorMilestoneDocument_0" style="display:none" >Milestone Document is required </label></td> '

                + '<td><a href="javascript:;" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddDocumentList(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>';
            $('#tblDocumentList tbody').prepend(html);
            tblDocumentList.columns.adjust();
            Datepickeradd();
            $("#Department_0").select2();
            $("#DocumentType_0").select2();
            $("#MilestoneDocument_0").select2();

            var datarow = tblDocumentList.rows().data();
            if (datarow.length > 0) {
                datarow.each(function (value, index) {
                    $("select[name$='JEPDocumentList[" + index + "].DocumentTypeId']").select2();
                    $("select[name$='JEPDocumentList[" + index + "].Department']").select2();
                    $("select[name$='JEPDocumentList[" + index + "].MilestoneDocument']").select2();
                });
                $('#tblDocumentList tr').addClass('v-top');
            }

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '/' + mm + '/' + yyyy;
            $("#PlannedDate_0").val(today);

            $("#Description_0").focusout(function () {
                $("#RevNo_0").val("R0");
            });


            $("#Weightage_0").on("keyup", function () {

                funCalcTotalWeightage(0, "Weightage_0");
                //TotalWeightage = 0;
                //tblDocumentList.rows().every(function (rowIdx, tableLoop, rowLoop) {
                //    var data = this.data();
                //    if (data.Weightage != "")
                //        TotalWeightage += parseInt(data.Weightage);
                //});
                //var inputweightage = (isNaN(parseInt($("#Weightage_0").val()))) ? 0 : parseInt($("#Weightage_0").val());
                //TotalWeightagesum = parseInt("100") - parseInt(TotalWeightage);
                //var weightage = (TotalWeightagesum > inputweightage) ? inputweightage : TotalWeightagesum;
                //if (parseFloat(weightage) < 0) {
                //    weightage = 0;
                //}
                //$("#Weightage_0").val(weightage);

            });

            //$("#Weightage_0").focusout(function () {

            //    var inputweightage = (isNaN(parseInt($("#Weightage_0").val()))) ? 0 : parseInt($("#Weightage_0").val());
            //    TotalWeightagesum = parseInt("100") - parseInt(TotalWeightage);
            //    var weightage = (TotalWeightagesum > inputweightage) ? inputweightage : TotalWeightagesum;


            //    $("#Weightage_0").val(weightage);
            //});
        }
    }, 1000);
}

function AddDocumentList(id) {

    $("#errorDocNo_" + id).hide();
    $("#errorDescription_" + id).hide();
    $("#errorRevNo_" + id).hide();
    $("#errorDocumentType_" + id).hide();
    $("#errorDepartment_" + id).hide();
    $("#errorPlannedDate_" + id).hide();
    $("#errorWeightage_" + id).hide();
    $("#errorMilestoneDocument_" + id).hide();

    var docno = $("#DocNo_" + id).val();
    var description = $("#Description_" + id).val();
    var revno = $("#RevNo_" + id).val();
    var customerdocno = $("#CustomerDocNo_" + id).val();
    var Customerdocrevision = $("#CustomerDocRevision_" + id).val();

    var documenttypeid = $("#DocumentType_" + id).val();
    var department = $("#Department_" + id).val();
    var milestoneDocument = $("#MilestoneDocument_" + id).val();
    var planneddate = $("#PlannedDate_" + id).val();
    var inputweightage = parseInt($("#Weightage_" + id).val());
    //var weightage = (TotalWeightagesum > inputweightage) ? inputweightage : TotalWeightagesum;


    if (docno != "" && description != "" && revno != "" && documenttypeid != "" && department != "" && planneddate != "" && milestoneDocument != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/JEP/AddDocument',
            data: JSON.stringify({ JEPDocumentDetailsId: id, JEPId: $("#JEPId").val(), DocNo: docno, DocDescription: description, RevisionNo: revno, CustomerDocNo: customerdocno, CustomerDocRevision: Customerdocrevision, DocumentTypeId: documenttypeid, Department: department, PlannedDate: planneddate, Weightage: inputweightage, MilestoneDocument: milestoneDocument }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentList.ajax.reload();
                    funGetWeightage();
                    clearFields();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (docno == "")
            $("#errorDocNo_" + id).show();
        if (description == "")
            $("#errorDescription_" + id).show();
        if (revno == "")
            $("#errorRevNo_" + id).show();
        if (documenttypeid == "")
            $("#errorDocumentType_" + id).show();
        if (department == "")
            $("#errorDepartment_" + id).show();
        if (planneddate == "")
            $("#errorPlannedDate_" + id).show();
        if (milestoneDocument == "")
            $("#errorMilestoneDocument_" + id).show();
        tblDocumentList.columns.adjust();
    }
}

$('#tblDocumentList').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var CustomerDocNo = data.CustomerDocNo || '';
    var CustomerDocRevision = data.CustomerDocRevision || '';

    var html = '<td><input type="text" readonly="readonly" maxlength="50" class="form-control" id="DocNo_' + data.JEPDocumentDetailsId + '" name="DocNo_' + data.JEPDocumentDetailsId + '" placeholder="Doc No" value="' + data.DocNo + '" /> <label class="text-danger" id="errorDocNo_' + data.JEPDocumentDetailsId + '" style="display:none" >Doc No is required </label></td> '
        + '<td><input type="text" maxlength="200" readonly="readonly" class="form-control" id="Description_' + data.JEPDocumentDetailsId + '" name="Description_' + data.JEPDocumentDetailsId + '" placeholder="Description"  value="' + data.DocDescription + '" /> <label class="text-danger" id="errorDescription_' + data.JEPDocumentDetailsId + '" style="display:none" >Description is required </label></td> '
        + '<td><input type="text" maxlength="4" readonly="readonly" class="form-control" id="RevNo_' + data.JEPDocumentDetailsId + '" name="RevNo_' + data.JEPDocumentDetailsId + '" placeholder="Rev No" style="width: 55px;" value="R' + data.RevisionNo + '" /><label class="text-danger" id="errorRevNo_' + data.JEPDocumentDetailsId + '" style="display:none" >Revision No is required </label></td> '

        + '<td><input type="text" maxlength="100" class="form-control" id="CustomerDocNo_' + data.JEPDocumentDetailsId + '" name="CustomerDocNo_' + data.JEPDocumentDetailsId + '" placeholder="Cust Doc No" value="' + CustomerDocNo + '" /></td> '
        + '<td><input type="text" maxlength="20" class="form-control" id="CustomerDocRevision_' + data.JEPDocumentDetailsId + '" name="CustomerDocRevision_' + data.JEPDocumentDetailsId + '" placeholder="Cust Doc No" value="' + CustomerDocRevision + '" /></td> '

    //disable if IsApproved approve
    var IsApproved = data.IsApproved || false;
    if (IsApproved) {
        html += '<td><select class="form-control" id="DocumentType_' + data.JEPDocumentDetailsId + '" disabled>';
        html += "<option value=''>Select</option>";
        for (let i = 0; i < ddlDocumentList.length; i++) {
            if (ddlDocumentList[i].Id == data.DocumentTypeId)
                html += "<option value='" + ddlDocumentList[i].Id + "' selected>" + ddlDocumentList[i].Lookup + "</option>";
            else
                html += "<option value='" + ddlDocumentList[i].Id + "'>" + ddlDocumentList[i].Lookup + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorDocumentType_' + data.JEPDocumentDetailsId + '" style="display:none" >Document Type is required </label></td> ';
    }
    else {
        html += '<td><select class="form-control" id="DocumentType_' + data.JEPDocumentDetailsId + '">';
        html += "<option value=''>Select</option>";
        for (let i = 0; i < ddlDocumentList.length; i++) {
            if (ddlDocumentList[i].Id == data.DocumentTypeId)
                html += "<option value='" + ddlDocumentList[i].Id + "' selected>" + ddlDocumentList[i].Lookup + "</option>";
            else
                html += "<option value='" + ddlDocumentList[i].Id + "'>" + ddlDocumentList[i].Lookup + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorDocumentType_' + data.JEPDocumentDetailsId + '" style="display:none" >Document Type is required </label></td> ';
    }

    //disable if IsJEP approve
    var IsJEP = data.IsJEP || false;
    if (IsJEP) {
        html += '<td><select class="form-control" id="Department_' + data.JEPDocumentDetailsId + '" disabled>';
        html += "<option value=''>Select</option>";
        for (let i = 0; i < ddlRole.length; i++) {
            if (ddlRole[i].t_dimx == data.Department)
                html += "<option value='" + ddlRole[i].t_dimx + "' selected>" + ddlRole[i].t_desc + "</option>";
            else
                html += "<option value='" + ddlRole[i].t_dimx + "'>" + ddlRole[i].t_desc + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorDepartment_' + data.JEPDocumentDetailsId + '" style="display:none" >Department is required </label></td> ';

    }
    else {
        html += '<td><select class="form-control" id="Department_' + data.JEPDocumentDetailsId + '">';
        html += "<option value=''>Select</option>";
        for (let i = 0; i < ddlRole.length; i++) {
            if (ddlRole[i].t_dimx == data.Department)
                html += "<option value='" + ddlRole[i].t_dimx + "' selected>" + ddlRole[i].t_desc + "</option>";
            else
                html += "<option value='" + ddlRole[i].t_dimx + "'>" + ddlRole[i].t_desc + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorDepartment_' + data.JEPDocumentDetailsId + '" style="display:none" >Department is required </label></td> ';

    }


    html += '<td><input type="text" maxlength="" class="form-control datepickerroc" id="PlannedDate_' + data.JEPDocumentDetailsId + '" name="PlannedDate_' + data.JEPDocumentDetailsId + '" placeholder="Planned Date" value="' + data.PlannedDate + '" /><label class="text-danger" id="errorPlannedDate_' + data.JEPDocumentDetailsId + '" style="display:none" >Planned Date is required </label></td> '
        + '<td><input type="text" maxlength="3" class="form-control onlyNumeric numeric percentage" id="Weightage_' + data.JEPDocumentDetailsId + '" name="Weightage_' + data.JEPDocumentDetailsId + '" placeholder="Weightage" value="' + data.Weightage + '" /><label class="text-danger" id="errorWeightage_' + data.JEPDocumentDetailsId + '" style="display:none" >Total Weightage should be 100%. </label></td> '


        + '<td><select class="form-control" id="MilestoneDocument_' + data.JEPDocumentDetailsId + '">';
    html += "<option value=''>Select</option>";
    var MilestoneDocument = data.MilestoneDocument || false;

    if (MilestoneDocument) {
        html += "<option value='false' >No</option>";
        html += "<option value='true' selected>Yes</option>";
    }
    else {
        html += "<option value='false' selected>No</option>";
        html += "<option value='true'>Yes</option>";
    }

    html += '</select></br><label class="text-danger" id="errorMilestoneDocument_' + data.JEPDocumentDetailsId + '" style="display:none" >Milestone Document is required </label></td> '


        + '<td><a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddDocumentList(' + data.JEPDocumentDetailsId + ')" title="Edit"><i class="la la-check text-info"></i></a>'
        + ' <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblDocumentListClear()" title="close"><i class="la la-close text-danger"></i></a></td>';
    $(tr).html(html);
    $("#DocNo_" + data.JEPDocumentDetailsId).focus();
    Datepickeradd();
    $("#Department_" + data.JEPDocumentDetailsId).select2();
    $("#DocumentType_" + data.JEPDocumentDetailsId).select2();
    $("#MilestoneDocument_" + data.JEPDocumentDetailsId).select2();


});



//$("#tblDocumentList").on("focusout", "input[name$='Weightage']", function () {
//    
//    var id = this.name;
//    var tr = $(this).closest('tr');
//    var row = tblDocumentList.row(tr);
//    var data = row.data();

//    activeWeightagevalue = 0;
//    TotalWeightagesum = parseInt("100") - (parseInt(TotalWeightage) - parseInt(data.Weightage));
//    var JEPDocumentDetailID = $("input[name$='" + id + "']").val();
//    var inputweightage = isNaN(parseInt(JEPDocumentDetailID)) ? 0 : parseInt(JEPDocumentDetailID);
//    var weightage = (TotalWeightagesum > inputweightage) ? inputweightage : TotalWeightagesum;
//    if (parseFloat(weightage) < 0) {
//        weightage = 0;
//    }
//    $("input[name$='" + id + "']").val(weightage);
//    activeWeightagevalue = parseInt(data.Weightage);
//});

function tblDocumentListClear() {
    if (tblDocumentList.data().count() == 1) {
        tblDocumentList.ajax.reload();
    }
    else {
        tblDocumentList.ajax.reload(null, false);
    }
}

function clearFields() {
    $("#DocNo_0").val("");
    $("#Description_0").val("");
    $("#RevNo_0").val("");
    $("#CustomerDocNo_0").val("");
    $("#CustomerDocRevision_0").val("");
    $("#DocumentType_0").val("");
    $("#Department_0").val("");
    $("#MilestoneDocument_0").val("false").select2();
    $("#PlannedDate_0").val("");
    $("#Weightage_0").val(0);
}

$('#tblDocumentList').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/JEP/DeleteDocument',
                    data: { Id: data.JEPDocumentDetailsId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {

                            toastr.success(response.Msg, "Success");
                            if (tblDocumentList.data().count() == 1) {
                                tblDocumentList.ajax.reload();
                            }
                            else {
                                tblDocumentList.ajax.reload(null, false);
                            }
                            AddDocList();
                            WeightageList = [];
                            funGetWeightage();
                            //funGetWeightage();
                            //funPrintTotalWeightage();
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

function btnAddProductTemplate() {
    var ddlProduct = $("#ddlProduct").val();
    var ddlTemplate = $("#ddlTemplate").val();
    var JEPId = $("#JEPId").val();
    $("#errorddlProduct").hide();
    $("#errorddlTemplate").hide();
    if (ddlProduct && ddlTemplate && JEPId) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/JEP/AddDcoumentByProductANDTemplate',
            data: JSON.stringify({ Product: $("#ddlProduct").val(), JEPTemplateId: $("#ddlTemplate").val(), JEPId: $("#JEPId").val() }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    $("#m_modal_preview").modal("hide");
                    toastr.success(response.Msg, "Success");
                    tblDocumentList.ajax.reload();
                    AddDocList();
                    funGetWeightage();
                } else {
                    toastr.error(response.Msg, "Error");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    }
    else {
        if (!ddlProduct) {
            $("#errorddlProduct").show();
        }
        if (!ddlTemplate) {
            $("#errorddlTemplate").show();
        }

    }
}

function btnAddDocumentProjectwise() {
    var ddlProjectwiseDocument = $("#ddlProjectwiseDocument").val();
    var JEPId = $("#JEPId").val();
    $("#errorddlProjectwiseDocument").hide();
    if (JEPId && ddlProjectwiseDocument) {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/JEP/AddDcoumentByProjectwise',
            data: JSON.stringify({ JEPDocumentDetailsId: $("#ddlProjectwiseDocument").val(), JEPId: $("#JEPId").val() }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblDocumentList.ajax.reload();

                    AddDocList();
                    funGetWeightage();
                } else {
                    toastr.error(response.Msg, "Error");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    }
    else {
        $("#errorddlProjectwiseDocument").show();

    }
}

function Datepickeradd() {
    var dateToday = new Date();
    $(".datepickerroc").datepicker({
        startDate: dateToday,
        rtl: mUtil.isRTL(),
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        autoclose: !0
    });

}

function ToJavaScriptDate(value) {
    if (value != null && value != "") {
        var currentTime = new Date(parseInt(value.substr(6)));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var date = (day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0" + month : month) + "/" + (year < 10 ? "0" + year : year);   //format MM/dd/yyyy
        return date;
    }
    else {
        var today = new Date();
        var dateCur = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        return dateCur;
    }
}

//function SaveDocumentList() {

//    var TotalWeightage = 0;
//    var Isdepartmentexist = false;
//    tblDocumentList.rows().every(function (rowIdx, tableLoop, rowLoop) {
//        var data = this.data();
//        if (data.Weightage != "")
//            TotalWeightage += parseInt(data.Weightage);
//        if (!data.DepartmentName)
//            Isdepartmentexist = true;
//    });
//    if (TotalWeightage != 100) {
//        toastr.info("Total Weightage should be 100%.", "Info");
//    }
//    else if (Isdepartmentexist) {
//        toastr.info("Please maintain department for all documents.", "Info");
//    }
//    else {
//        if (String($("#IsUpdateChanges").val()) == "true") {
//            if ($("#FormAddJEPDocument").valid() == true)
//                $("#FormAddJEPDocument").submit();
//            $("#IsUpdateChanges").val("false");
//        }
//        mApp.blockPage();
//        $.ajax({
//            type: "POST",
//            url: '/DES/JEP/AddEditDocumnet',
//            data: JSON.stringify({ JEPId: $("#JEPId").val() }),
//            dataType: 'json',
//            contentType: "application/json;charset=utf-8",
//            async: true,
//            success: function (response) {
//                mApp.unblockPage();
//                if (response.Status) {
//                    toastr.success(response.Msg, "Success");
//                    tblDocumentList.ajax.reload();
//                    window.location.href = response.RedirectAction;
//                } else {
//                    toastr.error(response.Msg, "Error");
//                }
//            },
//            error: function (error) {
//                mApp.unblockPage();
//                toastr.error("something went wrong try again later", "Error");
//            }
//        });
//    }
//}

function BindGlobalJEPDoc() {


    tblGlobalJepDoc = $('#tblGlobalJepDoc').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/JEP/GetAllSearchDocumnet",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "Project", value: $("#ContractProjectList").val() });
            data.push({ name: "DocumentNo", value: $("#txtDocumentNo").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderAgencyDoc' onclick='SelectAllAgencyDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">';
                    var IDIndex = selectedAgencyDocIDs.indexOf(row.DocumentId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.DocumentId + '"  data-id="' + row.DocumentId + '" onclick="AddToAgencyDocList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.DocumentId + '"  data-id="' + row.DocumentId + '" onclick="AddToAgencyDocList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocumentNo;
                }
            },
            {
                "sTitle": "Rev", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocumentRevision;
                }
            },
            {
                "sTitle": "Project", "className": "text-left", render: function (data, type, row, meta) {
                    return row.Project;
                }
            },
            {
                "sTitle": "Description", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocumentTitle;
                }
            },
            {
                "sTitle": "State", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocStatus;
                }
            }
            //{
            //    "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
            //        var srt = "";
            //        srt = "<a href='javascript:;' class='m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill' title='Link to JEP' onclick='btnlink(" + row.DocumentId + ")'><i class='la la-link m--font-success'></i></a >";
            //        return srt;
            //    }
            //}
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblGlobalJepDoc.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblGlobalJepDoc.columns.adjust();
            }, 500);
        }
    });
}

function BindpreviewJEPDoc() {

    tblpreviewDoc = $('#tblpreviewDoc').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/JEP/BindpreviewProductJEPDoc",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {


            //data.push({ name: "ProductId", value: $("#ddlProduct").val()});
            data.push({ name: "JEPTemplateId", value: $("#ddlTemplate").val() });
            //data.push({ name: "JEPId", value: $("#JEPId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "Doc No", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocumentNo;
                }
            },
            {
                "sTitle": "Description", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocumentTitle;
                }
            }
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblpreviewDoc.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblpreviewDoc.columns.adjust();
            }, 500);
        }
    });
}

function SelectAllAgencyDoc() {
    var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderAgencyDoc").prop("checked");

    if (select == true) {

        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedAgencyDocIDs.push(item);
        });

    }
    else {

        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedAgencyDocIDs.indexOf(item);
            if (index > -1) {
                selectedAgencyDocIDs.splice(index, 1);
            }
        });
    }
    selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
}

//function AddToAgencyDocList($this) {
//    var data = tblGlobalJepDoc.rows().data();
//    var id = $($this).attr("data-id");
//    id = parseInt(id, 10);
//    var select = $("#IsSelect_" + id).prop("checked");
//    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
//    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
//    if (select == true) {
//        selectedAgencyDocIDs.push(id);
//        selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
//        if (data.length == selectedAgencyDocIDs.length) {
//            $("#" + chbID).prop("checked", true);
//        }
//    }
//    else {
//        $("#" + chbID).prop("checked", false);
//        var index = selectedAgencyDocIDs.indexOf(id);
//        if (index > -1) {
//            selectedAgencyDocIDs.splice(index, 1);
//        }
//        selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
//    }
//}

function AddToAgencyDocList($this) {
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');

    if (select == true) {
        selectedAgencyDocIDs.push(id);
        selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            if (!selectedAgencyDocIDs.includes(item)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        $("#" + chbID).prop("checked", false);
        var index = selectedAgencyDocIDs.indexOf(id);
        if (index > -1) {
            selectedAgencyDocIDs.splice(index, 1);
        }

    }
    selectedAgencyDocIDs = Array.from(new Set(selectedAgencyDocIDs));
}

function btnBindGlobalJEPDoc() {
    $("#ContractProjectList").val($("#Project").val());
    $("#txtDocumentNo").val("");
    if (tblGlobalJepDoc != null)
        tblGlobalJepDoc.ajax.reload();
    $('#m_modal_4').modal('show');
    $("#ContractProjectList").select2();
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 500);
}

function btnSearchGlobalJEPDoc() {
    tblGlobalJepDoc.ajax.reload();
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 500);
}

function AddGlobalJEPList() {
    mApp.blockPage();
    if (selectedAgencyDocIDs.length > 0) {
        var FinalDoclist = Array.from(new Set(selectedAgencyDocIDs));
        var DocIDs = new Array();

        for (var id in FinalDoclist) {
            DocIDs.push({ JEPDocListId: FinalDoclist[id] });
        }

        var model = {};
        model.JEPId = $("#JEPId").val();
        model.JEPDocumentList = DocIDs;

        $.ajax({
            type: "POST",
            url: '/DES/JEP/AddJEPLinkDocuments',
            data: JSON.stringify({ model: model }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                if (response.Status) {

                    mApp.unblockPage();
                    selectedAgencyDocIDs = [];
                    if (response.SuccessMsg) {
                        toastr.success(response.SuccessMsg, "Success");
                    }

                    if (response.Msg) {
                        toastr.info(response.Msg, "Info");
                    }
                    $("#chbHeaderAgencyDoc").prop("checked", false);
                    tblGlobalJepDoc.ajax.reload();
                    tblDocumentList.ajax.reload();


                    AddDocList();
                    funGetWeightage();

                } else {
                    mApp.unblockPage();
                    toastr.error(response.Msg, "Error");
                }
            },
            error: function (error) {
                selectedAgencyDocIDs = [];
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        })
    }
    else {
        selectedAgencyDocIDs = [];
        mApp.unblockPage();
        toastr.info("Please select atleast one document!", "Info");
    }
}

function btnlink(Id) {
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/JEP/AddLinkDocument',
        data: JSON.stringify({ DocumentId: Id, JEPId: $("#JEPId").val() }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                tblGlobalJepDoc.ajax.reload();
                tblDocumentList.ajax.reload();
                //$('#m_modal_4').modal('hide');
            } else {
                mApp.unblockPage();
                toastr.error(response.Msg, "Error");
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

function CancelPreview() {
    $("#m_modal_preview").modal("hide");
}

function btnBindProductJEPDoc() {

    var ddlProduct = $("#ddlProduct").val();
    var ddlTemplate = $("#ddlTemplate").val();
    var JEPId = $("#JEPId").val();
    $("#errorddlProduct").hide();
    $("#errorddlTemplate").hide();
    if (ddlProduct && ddlTemplate && JEPId) {
        tblpreviewDoc.ajax.reload();
        $('#m_modal_preview').modal('show');
        setTimeout(function () {
            tblpreviewDoc.columns.adjust();
        }, 500);
    }
    else {
        if (!ddlProduct) {
            $("#errorddlProduct").show();
        }
        if (!ddlTemplate) {
            $("#errorddlTemplate").show();
        }

    }


}

$("#btnReturn").click(function () {
    $('#Return_Remark').val("");
    $('#returnRemark').modal({ backdrop: 'static', keyboard: false })
    $('#returnRemark').modal('show');
});

$("#addReturnBtn").click(function () {
    var JEPId = $("#JEPId").val();
    var returnRemark = $('#Return_Remark').val();


    var addReturnRemark = {
        JEPId: JEPId,
        ReturnNotes: returnRemark
    };
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/JEP/AddReturnRemarks',
        data: JSON.stringify(addReturnRemark),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                window.location.href = response.RedirectAction;
                $('#Return_Remark').val("");
                mApp.unblockPage();
            } else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDocumentList != null)
        tblDocumentList.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblpreviewDoc != null)
        tblpreviewDoc.columns.adjust();
});

$("#txtDocumentNo").keyup(function (event) {
    if (event.keyCode === 13) {
        btnSearchGlobalJEPDoc();
    }
});

function funGetWeightage() {
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: '/DES/JEP/GetWeightage',
            data: JSON.stringify({ JEPId: $("#JEPId").val() }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                WeightageList = response
                var tot = 0;
                for (var i = 0; i < WeightageList.length; i++) {
                    tot += WeightageList[i].Item2;
                }

                $("#TxtTotalWeightage").val(tot);
            },
            error: function (error) {

            }
        });
    }, 1000);
}

$("#tblDocumentList").on("keyup focusout", "input[name$='Weightage']", function () {

    var id = this.name;
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();

    funCalcTotalWeightage(data.JEPDocumentDetailsId, id);
    $("#Weightage_0").val(0);
});

function funCalcTotalWeightage(JEPDocumentDetailsId, id) {

    for (var i = 0; i < WeightageList.length; i++) {
        if (WeightageList[i].Item1 == JEPDocumentDetailsId) {
            WeightageList[i].Item2 = 0
            break;
        }
    }

    var JEPDocWeightage = $("input[name$='" + id + "']").val();
    if (WeightageList != null && WeightageList != "" && WeightageList.length > 0) {
        var wt = 0, Fwt = 0;
        for (var i = 0; i < WeightageList.length; i++) {
            wt = (parseFloat(wt) + parseFloat(WeightageList[i].Item2))
        }
        var newwt = (parseFloat(wt) + parseFloat(JEPDocWeightage || 0))
        if (newwt > 100) {
            Fwt = (parseFloat(100) - parseFloat(wt));
            if (Fwt < 0)
                Fwt = 0;
            $("input[name$='" + id + "']").val(parseFloat(Fwt || 0))
        } else {
            Fwt = parseFloat(JEPDocWeightage || 0);
            if (Fwt < 0)
                Fwt = 0;
            $("input[name$='" + id + "']").val(parseFloat(Fwt || 0))
        }

        for (var i = 0; i < WeightageList.length; i++) {
            if (WeightageList[i].Item1 == JEPDocumentDetailsId) {
                WeightageList[i].Item2 = Fwt
                break;
            }
        }
    }
    funPrintTotalWeightage();
}

function funPrintTotalWeightage() {
    var tot = 0;
    for (var i = 0; i < WeightageList.length; i++) {
        tot += WeightageList[i].Item2;
    }

    $("#TxtTotalWeightage").val(tot);
}

$("#Description_0").focusout(function () {
    $("#RevNo_0").val("R0");
});

//function CallImportExcel() {
//    $('#importExcel').fileupload({
//        dataType: 'json',
//        url: '/JEP/ImportExcel?JEPId=' + $("#JEPId").val(),
//        autoUpload: false,
//        type: File,
//        Default: true,
//        success: function (msg) {
//            alert(msg);
//        }
//    });
//}

function CallImportExcel(sender) {
    var iframe = $("<iframe>").hide();
    var newForm = $("<FORM>");
    newForm.attr({ method: "POST", enctype: "multipart/form-data", action: "/JEP/ImportExcel?JEPId=" + $("#JEPId").val() + "&&JEPNo=" + $("#JEPNumber").val() });
    var $this = $(sender), $clone = $this.clone();
    $this.after($clone).appendTo($(newForm));
    iframe.appendTo($("html")).contents().find('body').html($(newForm));
    newForm.submit();
}

OnSuccessFileUpload = function (data) {
    mApp.unblockPage();

    if (data.Status) {
        if (data.IsSwal) {
            swal({
                title: data.Msg,
                type: "warning",
                allowOutsideClick: false,
                showConfirmButton: true,
                confirmButtonClass: "btn btn-danger",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Close",
                width: '1000px'
            });
        }
        else {
            toastr.success(data.Msg, "Success");
            toastr.success(data.DocumentMsg, "Success");
            tblDocumentList.ajax.reload();
        }
    }
    else {
        toastr.info(data.Msg, "Info");
    }
    $("#importExcel").val('');
}

$('#tblDocumentList').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    //$.each(DocPathList, function (i) {
    //    UniverSalFile(DocPathList[i]);
    //});

    for (i = 0; i < DocPathList.length; i++) {
        UniverSalFile(DocPathList[i]);
    }
});




$("#DocumentType_0").change(function () {
    if ($("#DocumentType_0").val() == arm) {
        var dno = $("#DocNo_0").val().substr(0, 30);
        var desc = $("#Description_0").val().substr(0, 50);
        $("#DocNo_0").val(dno).attr('maxlength', '30');
        $("#Description_0").val(desc).attr('maxlength', '50');
    }
    else {
        $("#DocNo_0").attr('maxlength', '200');
        $("#Description_0").attr('maxlength', '500');
    }
});


function getExcel() {
    var jepIID = $("#JEPId").val();
    if (jepIID != null) {
        $.ajax({
            type: "POST",
            url: '/DES/JEP/IsJEPHasDocument',
            data: JSON.stringify({ JEPId: jepIID }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                if (response.Status) {
                    window.open('/DES/JEP/ExportToExcel?JEPId=' + jepIID, '_blank');
                }
                else {
                    toastr.info(response.Msg, "Info");
                }
                mApp.unblockPage();
            },
            error: function (error) {
                toastr.error("Error while removing file from grid", "Error");
                mApp.unblockPage();
            }
        });
    }
    else {
        toastr.info("Please add atleast one Document", "Info");
    }

}