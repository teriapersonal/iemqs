﻿//var Material = $('#Val').val();
var tblAllMaterial;
$(document).ready(function () {
    setTimeout(function () {
        if (tblAllMaterial != null)
            tblAllMaterial.destroy();

        tblAllMaterial = $('#tblAllMaterial').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": false,
            //"filter": "true? Material & ",
            //"filter": {
            //    "true": Material & ""
            //},        
            "sAjaxSource": "/DES/Part/GetAllMaterialList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "fixedColumns": {
                "leftColumns": 1
            },
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
                //{ "orderable": false, "targets": 5 },
                //{ "orderable": false, "targets": 6 },
                //{ "orderable": false, "targets": 7 },
                //{ "orderable": false, "targets": 8 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "search", value: $("#txtsearchmateiral").val() });
                data.push({ name: "ProductFormId", value: $("#txtsearchProductFormId").val() });

                //data.push({ name: "MaterialSelectId", value: $("#MaterialId").val() });
                //data.push({ name: "ProductFormId", value: $('#PartList_' + id + '__ProductFormCodeId').val() });
                //data.push({ name: "search", value: $("#tblAllMaterial_filter").val() });
                //data.push({ name: "iId", value: Material });  tblAllMaterial_filter
                //var Material = $('#Val').val(); ProductFromId
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "", "className": "", render: function (data, type, row, meta) {
                        var srt = "";
                        srt += '<a href="javascript:;" onclick="btnMaterialCopy(this)" data-id="' + row.t_mtrl + '" title="Select"><i class="fa fa-link m--font-accent"></i> </a>';
                        return srt;
                    }
                },
                {
                    "sTitle": "Material Name", "className": "", render: function (data, type, row, meta) {
                        return row.t_mtrl;
                    }
                },
                {
                    "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                        return row.t_desc;
                    }
                },
                {
                    "sTitle": "Product Form", "className": "", render: function (data, type, row, meta) {
                        return row.t_prod;
                    }
                },
                {
                    "sTitle": "Category", "className": "", render: function (data, type, row, meta) {
                        return row.t_mcat;
                    }
                },
                {
                    "sTitle": "Specific Gravity", "className": "", render: function (data, type, row, meta) {
                        return row.t_spgr;
                    }
                },
                {
                    "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                        return row.t_itgr;
                    }
                },
                {
                    "sTitle": "Material Number", "className": "", render: function (data, type, row, meta) {
                        return row.t_prno;
                    }
                },
                {
                    "sTitle": "CP", "className": "", render: function (data, type, row, meta) {
                        return row.t_cpcp;
                    }
                },
                {
                    "sTitle": "Clad Density", "className": "", render: function (data, type, row, meta) {
                        return row.t_dens;
                    }
                }
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });

    }, 500);
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblAllMaterial != null)
        tblAllMaterial.columns.adjust();
});

    //var id;
    //if (data != null && data != undefined) {
    //    id = data.t_mtrl;
    //}

    //var Id = $($this).closest("tr").attr("data-id");
    //var id = $("#PartList_" + id + "__Material").val();
    //var id = $($this).val();
    //mApp.blockPage();
    //$.ajax({
    //    cache: false,
    //    type: "GET",
    //    url: "/DES/Part/GetBindMaterial",
    //    data: { Id: id }, //data.t_mtrl
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",

    //    success: function (data) {
    //        mApp.unblockPage();
    //        //$("#PartList_" + id + "__Material").html("");           
    //        //$("#PartList_" + i + "__Material").val($("#PartList_" + id + "__Material").val());
    //        Material = $("#PartList_" + id + "__Material").selectize();
    //        var selectizeMaterial = Material[0].selectize;
    //        selectizeMaterial.setValue($("#PartList_" + id + "__Material").val(), false);           
    //    },
    //    error: function (error) {
    //        mApp.unblockPage();
    //        toastr.error("something went wrong try again later", "Error");
    //    }
    //});




