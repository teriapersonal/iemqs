﻿var tblDocumentList;
//var IsJEP;
var documentNo;
var tblDocumentFiles;
var GroupId;
var Dropzonefile;
var DocumentListId;
var AgencyListId;
var Count;

function getCustomerFeedbackDocumentGrid() {
    setTimeout(function () {
        tblDocumentFiles = $('#tblDocumentFiles').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/JEP/GetCustomerFeedbackDocumentList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 },
                { "orderable": false, "targets": 2 },
                { "orderable": false, "targets": 3 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "GroupId", value: GroupId });
                data.push({ name: "JEPcustomerFeedbackId", value: $('#JEPCustomerFeedbackId').val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "text-left", "orderable": false, render: function (data, type, row, meta) {
                        var srt = "";

                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                        srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        return srt;
                    }
                },
                {
                    "sTitle": "Document Title", "className": "text-left", render: function (data, type, row, meta) {
                        return row.DocumentTitle;
                    }
                },
                {
                    "sTitle": "Created By", "className": "text-left", render: function (data, type, row, meta) {
                        return row.CreatedBy;
                    }
                },
                {
                    "sTitle": "Created On", "className": "text-left", render: function (data, type, row, meta) {
                        return row.CreatedOn;
                    }
                }
            ],

            "initComplete": function () {
                setTimeout(function () {
                    tblDocumentFiles.columns.adjust();
                }, 500);
            },

            "fnRowCallback": function () {
                setTimeout(function () {
                    tblDocumentFiles.columns.adjust();
                }, 500);
            }
        });
    }, 500);
}

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    tblDocumentFiles.columns.adjust();
    if (tblDocumentList != null)
        tblDocumentList.ajax.reload();
});

$(document).ready(function () {

    // mApp.blockPage();
    tblDocumentList = $('#tblDocumentList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/JEP/GetDocumentList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "fixedColumns": {
            leftColumns: 3
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ],



        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "JEPId", value: $("#JEPId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, "visible": false, render: function (data, type, row, meta) {
                    var srt = "";
                    
                    return srt;
                }
            },
            {
                "sTitle": "Doc No <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {

                    //if (row.DOCStatus == 'Draft') {
                    //    var name = '<input type="text" maxlength="50" class="form-control" name="JEPDocumentList[' + meta.row + '].DocNo" placeholder="Doc No/> <label class="text-danger" id=errorDocNo_' + meta.row + ' style="display:none" >Doc No is required </label>';
                    //    //id='DocNo_" + meta.row + "'  name='JEPDocumentList DocNo_" + meta.row + "' placeholder='Doc No' /> <label class='text-danger' id='errorDocNo_" + meta.row + "' style='display:none' >Doc No is required </label>";
                    //}
                    //else {
                    //    var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                    //}
                    var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocNo"  value="' + row.DocNo + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocDescription"  value="' + row.DocDescription + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].JEPDocumentDetailsId"  value="' + row.JEPDocumentDetailsId + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].DocumentId"  value="' + row.DocumentId + '"/>';
                    name += '<input type="hidden" name="JEPDocumentList[' + meta.row + '].RevisionNo"  value="' + row.RevisionNo + '"/>';
                    return name;
                }
            },
            {
                "sTitle": "Description <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var Description = "<strong style='text-transform: capitalize;'>" + (row.DocDescription || '') + "</strong>";
                    return Description;
                    //var Description = "<strong style='text-transform: capitalize;'>" + row.DocDescription + "</strong>";
                    //return Description;
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    return "R" + row.RevisionNo;
                }
            },
            {
                "sTitle": "Customer Doc No", "className": "", render: function (data, type, row, meta) {
                  
                        return row.CustomerDocNo || '';
                    
                }
            },
            {
                "sTitle": "Customer Doc Rev", "className": "", render: function (data, type, row, meta) {
                   
                        return row.CustomerDocRevision || '';
                    

                }
            },
            {
                "sTitle": "Document Type <span class='text-danger'>*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>", "className": "", render: function (data, type, row, meta) {
                    
                        return row.DocumentType;
                    
                }
            },
            {
                "sTitle": "Department <span class='text-danger'>*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>", "className": "", render: function (data, type, row, meta) {
                   
                    return row.DepartmentName;
                }
            },
            {
                "sTitle": "Planned Date <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                   
                        return row.PlannedDate || '';
                    

                }
            },
            //{
            //    "sTitle": "Weightage ", "className": "", render: function (data, type, row, meta) {
                   
            //            return row.Weightage || '';
                    
            //    }
            //},
            {
                "sTitle": "Milestone Document <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    
                        var milestoneDocument = row.MilestoneDocument || false;
                        var milestoneDocumentdata = (milestoneDocument) ? 'Yes' : 'No';
                        return milestoneDocumentdata;
                    
                }
            }
        ],

        "initComplete": function (row) {


        },

        "fnRowCallback": function () {

        },
        "preDrawCallback": function () {
           
        },

    });

});

funSuccessCustmerFeedback = function (data) {
    if (data.Status) {
        $('#JEPId').val(data.model.JEPId);
        GroupId = data.model.GroupId;
        toastr.success(data.Msg, "Success");
        var JEPId = (data.model.JEPId);

        window.location.href = "/DES/JEP";

    } else {
        toastr.error(data.Msg, "Error");
    }
}

$(document).ready(function () {
    //BindGlobalJEPDoc();
    $('.ddlList').select2();
    getCustomerFeedbackDocumentGrid();
    if ($('#JEPDocumentDetailsId').val() != "" && $('#AgencyId').val() != "") {
        GETCustomerFeedbackByDoc_AGS($('#JEPDocumentDetailsId').val(), $('#AgencyId').val());
    }
    var today = new Date();
    $(".datepickerfeedback").datepicker({
        endDate: "today",
        maxDate: today,
        rtl: mUtil.isRTL(),
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        autoclose: !0
    });
    $(".datepickerfeedbacksibling").on("click", function () {
        $(this).siblings("input").datepicker("show");
    });

});
var iii = 1;
var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    parallelUploads: 10000,
    maxFilesize: 200000,
    maxFiles: 10,
    init: function () {
        this.on("maxfilesexceeded", function (file) {

            if (this.files.length == 11) {
                // this.removeFile(this.files[0]);
                if (iii == 1) {
                    alert("10 Files can be uploded at once!");
                }
                iii++;
            }
            myDropzone.removeFile(file);
            myDropzone.removeAllFiles();
        });

    },
    queuecomplete: function () {
        iii = 1;
    }
});

myDropzone.on('sending', function (file, xhr, formData, progress, bytesSent) {
    Count = myDropzone.files.length;
    if ($('#JEPDocumentDetailsId').val() != "" && $('#AgencyId').val() != "" && $('#ReceiptDate').val() != "") {
        var regex = /\@|\#|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;
        var fileName = regex.test(file.name);
        if (fileName) {
            toastr.error(file.name + " Contains special characters", "Error");
            myDropzone.removeFile(file);
            myDropzoneThree.removeAllFiles();
        }
        else {
            var dotNameSplit = file.name.split('.');
            if (dotNameSplit.length > 2) {
                toastr.error(file.name + " Contains special characters", "Error");
                myDropzone.removeFile(file);
                myDropzoneThree.removeAllFiles();
            }
            else {

                if ($('#CurrentLocationIp').val() != "") {
                    var CurrentLocationIp = $('#CurrentLocationIp').val();
                    var CurrentLocationPath = $('#FilePath').val();
                    var FCSurl = CurrentLocationIp + "/api/Upload";

                    var DocumentNo = file.name;
                    var JEPNumber = $('#JEPNumber').val();

                    var DocumentPath = JEPNumber.toString().trim() + "-" + "CustomerFeedback";
                    formData.append('SavedPath', CurrentLocationPath.toString());
                    formData.append('DocumentPath', DocumentPath.toString());
                    formData.append('DocumentFile', file);
                    Dropzonefile = file;

                    var formate = file.name.substr(file.name.lastIndexOf('.') + 1);
                    mApp.blockPage();
                    window.onbeforeunload = function () {
                        var prevent_leave = true;
                        if (prevent_leave) {
                            return "Your files are not completely uploaded yet...";
                        }
                    }

                    //Need To get Status and other filed HERE 
                    jQuery.ajax({
                        url: FCSurl,
                        data: formData,
                        enctype: 'multipart/form-data',
                        cache: false,
                        async: true,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function (response) {

                            mApp.unblockPage();
                            var fname = response.SavedFileName;
                            var path = response.SavedPath + "/" + fname;

                            jQuery.ajax({
                                type: "POST",
                                url: '/DES/JEP/UploadCustomerFeedbackFiles',
                                data: JSON.stringify({ JEPCustomerFeedbackId: $("#JEPCustomerFeedbackId").val(), GroupId: $("#GroupId").val(), DocumentTitle: DocumentNo, DocumentFormat: formate, DocumentPath: path }),
                                dataType: 'json',
                                contentType: "application/json;charset=utf-8",
                                async: true,
                                success: function (response) {
                                    if (response.Status) {

                                        toastr.success(response.Msg, "File uploaded successfully");
                                        GroupId = $("#GroupId").val();
                                        tblDocumentFiles.ajax.reload();
                                        myDropzone.removeFile(file);
                                        mApp.unblockPage();
                                        $('#btnSubmit').attr('disabled', false);
                                    } else {
                                        toastr.error(response.Msg, "Error");
                                        mApp.unblockPage();
                                    }
                                },
                                error: function (error) {
                                    toastr.error("something went wrong try again later", "Error");
                                    myDropzone.removeFile(file);
                                    mApp.unblockPage();
                                }
                            });

                            window.onbeforeunload = function () {
                                var prevent_leave = false;
                                if (prevent_leave) {
                                    return "Your files are not completely uploaded...";
                                }
                            }
                        },
                    }).fail(function (data) {
                        mApp.unblockPage();
                        myDropzone.removeFile(file);
                        toastr.error("Server response not received", "Error");
                        window.onbeforeunload = function () {
                            var prevent_leave = false;
                            if (prevent_leave) {
                                return "Your files are not completely uploaded...";
                            }
                        }
                    });
                }
                else {
                    toastr.info("Ip is not detecting", "Info");
                    myDropzone.removeFile(file);
                    mApp.unblockPage();
                }
            }
        }
    }
    else {
        mApp.unblockPage();
        file.status = "canceled";
        myDropzone.removeFile(file);
        Count = Count - 1;
        if (Count == 0) {
            toastr.info("Select Document, Agency and Receipt Date first.", "Info");
        }
    }
});

$('#JEPDocumentDetailsId').change(function () {
    if ($('#JEPDocumentDetailsId').val() != "" && $('#AgencyId').val() != "") {
        GETCustomerFeedbackByDoc_AGS($('#JEPDocumentDetailsId').val(), $('#AgencyId').val());
    }
});

$('#AgencyId').change(function () {
    if ($('#AgencyId').val() != "") {
        $('#ReceiptDate').val("");
        //$('#RefTransmittalDate').val("");
        $('#Remark').val("");
        $('#ApprovalId').val("");
        $('#JEPCustomerFeedbackId').val("");
        $('#RefTransmittalNo').val("");

        $('#DocumentTitle').val("");
        $('#RevisionNo').val("");
        $('#RevisionNostr').val("");
        $('#ReturnTransmittalNo').val("");
        $('#ActualSubDate').val("");
        $('#PlannedSubDate').val("");
        $('#CustomerDocNo').val("");
        tblDocumentFiles.ajax.reload();
        $("#JEPDocumentDetailsId").empty();

        $.ajax({
            type: "GET",
            url: '/DES/JEP/GETAgencyWiseDocuments',
            data: { JEPId: $('#JEPId').val(), AgencyId: $('#AgencyId').val() },
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            success: function (result) {
                console.log(result)
                if (result != "") {
                    $('#JEPDocumentDetailsId').append("<option value=''>Select</option>");
                    for (let i = 0; i < result.length; i++) {
                        if (result[i].AgencyId == parseInt($('#AgencyId').val())) {
                            $('#JEPDocumentDetailsId').append('<option value="' + result[i].JEPDocumentDetailsId + '">' + result[i].DocNo + '</option>');
                        }
                    }
                }
            },
            error: function (error) {
                toastr.error("something went wrong try again later", "Error");
            }
        });


    }
    //if ($('#JEPDocumentDetailsId').val() != "" && $('#AgencyId').val() != "") {
    //    GETCustomerFeedbackByDoc_AGS($('#JEPDocumentDetailsId').val(), $('#AgencyId').val());
    //}
});

function GETCustomerFeedbackByDoc_AGS(DocumentId, AgencyId) {
    $.ajax({
        type: "GET",
        url: '/DES/JEP/GET_JEP_CustomerFeedbackByDOCId_AGSId',
        data: { JEPDocumentDetailsId: DocumentId, AgencyId: AgencyId },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",

        success: function (response) {
            if (response != "") {
                //$('#JEPDocumentDetailsId').prop("disabled", true);
                //$('#AgencyId').prop("disabled", true);
                $('#ReceiptDate').val(response.ReceiptDateStr);
                //$('#ReceiptDate').val(response.ReceiptDateStr).prop("disabled", true);
                $('#RefTransmittalNo').val(response.RefTransmittalNo).prop("disabled", true);
                $('#Remark').val(response.Remark);
                $('#ApprovalId').val(response.ApprovalId);
                //$('#ApprovalId').val(response.ApprovalId).prop("disabled", true);
                $('#JEPCustomerFeedbackId').val(response.JEPCustomerFeedbackId);

                $('#DocumentTitle').val(response.DocumentTitle);
                $('#RevisionNo').val(response.RevisionNo);
                $('#RevisionNostr').val(response.RevisionNostr);
                $('#ReturnTransmittalNo').val(response.ReturnTransmittalNo);
                $('#ActualSubDate').val(response.ActualSubDate).prop("disabled", true);;
                $('#PlannedSubDate').val(response.PlannedSubDate).prop("disabled", true);;
                $('#CustomerDocNo').val(response.CustomerDocNo);
                tblDocumentFiles.ajax.reload();

                $("#ReceiptDate").datepicker("remove");
                var jepdate = $("#ActualSubDate").val() || "";
                var jepdatesplit = String(jepdate).split("/");
                var v = new Date();
                if (jepdate != "")
                    v = new Date(jepdatesplit[2] + '-' + jepdatesplit[1] + '-' + jepdatesplit[0]);

                $("#ReceiptDate").datepicker({
                    startDate: v,
                    todayHighlight: !0, orientation: "bottom left", format: "dd/mm/yyyy", autoclose: true,
                })
            }
            else {
                $('#ReceiptDate').val("");
                //$('#RefTransmittalDate').val("");
                $('#Remark').val("");
                $('#ApprovalId').val("");
                $('#JEPCustomerFeedbackId').val("");
                $('#RefTransmittalNo').val("");

                $('#DocumentTitle').val("");
                $('#RevisionNo').val("");
                $('#RevisionNostr').val("");
                $('#ReturnTransmittalNo').val("");
                $('#ActualSubDate').val("");
                $('#PlannedSubDate').val("");
                $('#CustomerDocNo').val("");
                tblDocumentFiles.ajax.reload();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

$('#tblDocumentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

$('#tblDocumentFiles').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/JEP/DeleteCustomerFeedbackDocs',
                    data: { Id: data.JEPCustFeedbackDocumentId, JEPId: $('#JEPId').val() },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDocumentFiles.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$('#btnSubmit').click(function () {
    if (!tblDocumentFiles.data().any()) {
        toastr.info("At least one Document is Required", "Info");
        return false;
    }
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDocumentFiles != null)
        tblDocumentFiles.columns.adjust();
});

$('#tblDocumentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});




