var DatatablesBasicBasic = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({


            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row datatable-bottom'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

            language: {
                lengthMenu: "Display _MENU_"
            },


            columnDefs: [

                {
                    targets: 0,
                    width: '100px',
                    title: "Actions",
                    orderable: !1,
                    render: function (e, a, t, n) {
                        return '\n                       <a href="Create-DCR.html" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Create DCR">\n                          <i class="la la-external-link text-success"></i>\n                        </a>\n                       <a href="Create-DIN.html" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="view">\n                          <i class="la la-eye text-brand"></i>\n                        </a> \n                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\n                          <i class="la la-trash text-danger"></i>\n                        </a>'
                    }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic.init()
});
