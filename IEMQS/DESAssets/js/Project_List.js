﻿var tblProjectList;
$(document).ready(function () {

    tblProjectList = $('#tblProjectList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Project/Index",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "fixedColumns": {
            "leftColumns": 3
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            //{ "orderable": false, "targets": 5 },
            //{ "orderable": false, "targets": 6 },
            //{ "orderable": false, "targets": 7 },
            //{ "orderable": false, "targets": 8 },
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    varstatusClass = 'btn-primary';
                    if (row.Status == "Completed") {
                        varstatusClass = "btn-success";
                    }
                    else if (row.Status == "InProcess") {
                        varstatusClass = "btn-info";
                    }

                    srt += '<a class="btn m-btn--pill ' + varstatusClass + ' btn-sm  py-1" href="javascript:;" data-status="' + row.Status + '" data-project="' + row.Project + '" onclick="swalPopup(this)" title="Click here for change status">';
                    if (row.Status == "Completed") {
                        srt += "Re-open";
                    }
                    else if (row.Status == "Created") {
                        srt += "Active";
                    }
                    else {
                        srt += row.Status;
                    }
                    srt += '</a>';
                    return srt;
                }
            },
             {
                 "sTitle": "Project No.", "className": "", render: function (data, type, row, meta) {
                     return row.Project;
                 }
             },
              {
                  "sTitle": "Owner", "className": "", render: function (data, type, row, meta) {
                      return row.Owner;
                  }
              },
              {
                  "sTitle": "Contract No.", "className": "", render: function (data, type, row, meta) {
                      return row.Contract;
                  }
              },
              {
                  "sTitle": "Customer Name", "className": "", render: function (data, type, row, meta) {
                      return row.Customer;
                  }
              },
              {
                  "sTitle": "Project Description", "className": "", render: function (data, type, row, meta) {
                      return row.ProjectDescription;
                  }
              },
               {
                   "sTitle": "Customer Order No.", "className": "", render: function (data, type, row, meta) {
                       return row.CustomerPONumber;
                   }
               },
               {
                   "sTitle": "Contract Description", "className": "", render: function (data, type, row, meta) {
                       return row.ContractName;
                   }
               },
               {
                   "sTitle": "Customer Order Date", "className": "", render: function (data, type, row, meta) {
                       return row.PODate;
                   }
               },
             {
                 "sTitle": "BU", "className": "", render: function (data, type, row, meta) {
                     return row.BU;
                 }
             },
             {
                 "sTitle": "Consultant", "className": "", render: function (data, type, row, meta) {
                     return row.Consultant;
                 }
             },
            {
                "sTitle": "PBU", "className": "", render: function (data, type, row, meta) {
                    return row.PBU;
                }
            },
              {
                  "sTitle": "Product End User", "className": "", render: function (data, type, row, meta) {
                      return row.ProductEndUser;
                  }
              },
             {
                 "sTitle": "LOI/FOA/PO No.", "className": "", render: function (data, type, row, meta) {
                     return row.CustomerLOINumber;
                 }
             },
            {
                "sTitle": "Licensor", "className": "", render: function (data, type, row, meta) {
                    return row.Licensor;
                }
            },
            {
                "sTitle": "LOI/FOA/PO Date", "className": "", render: function (data, type, row, meta) {
                    return row.LOIDate;
                }
            },
            {
                "sTitle": "Design Code", "className": "", render: function (data, type, row, meta) {
                    return row.DesignCode;
                }
            },
             {
                 "sTitle": "Product Type", "className": "", render: function (data, type, row, meta) {
                     return row.ProductType;
                 }
             },
              {
                  "sTitle": "Edition", "className": "", render: function (data, type, row, meta) {
                      return row.Edition;
                  }
              },
              {
                  "sTitle": "Zero Date", "className": "", render: function (data, type, row, meta) {
                      return row.ZeroDate;
                  }
              },
              {
                  "sTitle": "Regulatory Requirements", "className": "", render: function (data, type, row, meta) {
                      return row.RegulatoryRequirement;
                  }
              },
              {
                  "sTitle": "Contractual Delivery Date", "className": "", render: function (data, type, row, meta) {
                      return row.ContractualDeliveryDate;
                  }
              },
              {
                  "sTitle": "Code Stamp", "className": "", render: function (data, type, row, meta) {
                      return row.CodeStamp;
                  }
              },
              {
                  "sTitle": "Delivery Term", "className": "", render: function (data, type, row, meta) {
                      return row.DeliveryTerms;
                  }
              },
              {
                  "sTitle": "Equipment Diameter (mm)", "className": "", render: function (data, type, row, meta) {
                      return row.EquipmentDiameter;
                  }
              },
              {
                  "sTitle": "Warranty Terms", "className": "", render: function (data, type, row, meta) {
                      return row.WarrantyTerms;
                  }
              },
              {
                  "sTitle": "TL to TL (mm)", "className": "", render: function (data, type, row, meta) {
                      return row.TLtoTL;
                  }
              },
               {
                   "sTitle": "Warranty Expiry Date", "className": "", render: function (data, type, row, meta) {
                       return row.WarrantyExpiryDate;
                   }
               },
               {
                   "sTitle": "Major Material", "className": "", render: function (data, type, row, meta) {
                       return row.MajorMaterial;
                   }
               },
               {
                   "sTitle": "Category (Order Category)", "className": "", render: function (data, type, row, meta) {
                       return row.OrderCategory;
                   }
               },
               {
                   "sTitle": "Fabricated Weight (kg)", "className": "", render: function (data, type, row, meta) {
                       return row.FabricatedWeight
                   }
               },
                {
                    "sTitle": "Enquiry Name", "className": "", render: function (data, type, row, meta) {
                        return row.EnquiryName;
                    }
                },
                {
                    "sTitle": "Is Site work require", "className": "", render: function (data, type, row, meta) {
                        var str = '';
                        if (row.SiteWorkRequire == '' || row.SiteWorkRequire == null) {
                            str = 'No';
                        }
                        else {
                            str = row.SiteWorkRequire;
                        }
                        return str;
                    }
                },

            //{
            //    "sTitle": "Main Project", "className": "", render: function (data, type, row, meta) {
            //        return row.MainProject;
            //    }
            //},

            //{
            //    "sTitle": "PIM Date", "className": "", render: function (data, type, row, meta) {
            //        return row.PIMDate;
            //    }
            //},
            
             //{
             //    "sTitle": "Delivery Condition", "className": "", render: function (data, type, row, meta) {
             //        return row.DeliveryCondition;
             //    }
             //},
           
            {
                "sTitle": "JEP No", "className": "", render: function (data, type, row, meta) {
                    return row.JEPNumber;
                }
            },
            {
                "sTitle": "Location", "className": "", render: function (data, type, row, meta) {
                    return row.location;
                }
            },
           
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblProjectList.columns.adjust();
        }

    });
});

function funEdit(valProjectNo) {
    window.location.href = 'editviewposition.aspx?id=' + hidposid.value;
}

function swalPopup($this) {
    var Msg = "";
    if ($this.text == "Created" || $this.text == "Active") {
        Msg = "Do you want to activate?";
    }
    if ($this.text == "InProcess") {
        Msg = "Do you want to Complete?";
    }
    if ($this.text == "Re-open") {
        Msg = "Do you want to re-open?";
    }
    swal({
        title: "Sure",
        text: Msg,
        type: "info",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            funUpdateStatus(isConfirm, $this);
        });
}

function funUpdateStatus(isConfirm, $this) {
    if (isConfirm.value) {
        var valStatus = $($this).attr("data-status");
        var ProjectNo = $($this).attr("data-project");
        if (valStatus.toLowerCase() == 'Created'.toLowerCase() || valStatus.toLowerCase() == 'Completed'.toLowerCase()) {
            valStatus = 'InProcess';
        }
        else if (valStatus.toLowerCase() == 'InProcess'.toLowerCase()) {
            valStatus = 'Completed';
        }
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: '/DES/Project/UpdateProjectStatus',
            data: { Project: ProjectNo, Status: valStatus },
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.result) {
                    if ($this.text == "Re-open") {
                        toastr.success("Project re-open successfully", "Success");
                    }
                    else {
                        toastr.success(response.Msg, "Success");
                    }
                    tblProjectList.ajax.reload();
                } else {
                    toastr.error(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    }
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblProjectList != null)
        tblProjectList.columns.adjust();
});

