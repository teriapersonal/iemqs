﻿var parameterList = [];
var userAssignrole;

function FillProtocolAutoComplete(element, sourceList, hdElement, isEditable, callbackfn, extraPara) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CategoryDescription,
                    value: item.CategoryDescription,
                    id: item.Code
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        async:false,
        minLength: 0,
        select: function (event, ui) {
            $(element).val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val('');
                $(this).focus();
            }
            else {
                if ($(element).val() != ui.item.value) {
                    $(this).val('');
                    $(this).focus();
                }
            }
            if (callbackfn != null && callbackfn != undefined) {
                callbackfn($(this).val(), extraPara[0]);
            }
           
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function showCladtripTable(selectedVal, dvID) {
    if (selectedVal == "Yes") {
        $("#" + dvID).show();
    }
    else {
        $("#" + dvID).hide();
    }
}
function LoadProtocolLineTables(tableid, newbtnid, Columns, AjaxSource, parameterArray, extrafn) {
    var table = $('#' + tableid);
    var oTable = table.dataTable({
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "bProcessing": false,
        "bServerSide": true,
        "stateSave": false,
        buttons: [],
        "bDestroy": true,
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + AjaxSource + GetQueryStringParameterList(parameterArray),
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }
                        );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': function (data) {
                    fnCallback(data);

                    if (data.outby != null && data.outby != undefined && data.outby != "undefined") {
                        SetOutByRow(tableid);
                    }

                    var parentdvid = $('#' + tableid).parents('div.inlineparentdiv').attr('id');

                    InlineGridProtocolEdit.init($('#' + newbtnid), $('#' + tableid), $('#' + parentdvid));
                },
                'async': false
            });
            $('input[maxlength]').maxlength({
                alwaysShow: true,
            });
            ResetAddRowLine(tableid, newbtnid);
            SetControlNumeric();
            if (extrafn && (typeof extrafn == "function")) {
                extrafn();
            }
        },
        createdRow: function (row, data, dataIndex) {
            $(row).attr("data-id", data[2]);
            $(row).attr("data-hid", data[1]);
            $(row).attr("data-gid", data[0]);
        },
        "aoColumns": Columns
    });

}

function SetOutByRow(tableid) {
    $("#" + tableid + " tr:last").each(function () {
        jQuery.each($('td', this), function () {
            $(this).addClass("datatableFooter");
            $(this).attr("bSortable",false);
        });
    });
}

function SetControlNumeric() {
    $(".numeric-only").autoNumeric('init', { aSep: '', vMax: "9999999.99" });
    $('.number-only').autoNumeric('init', { vMin: '0', vMax: '9999999999', aSep: '' });
    $('input[maxlength],textarea[maxlength]').maxlength({
        alwaysShow: true,
    });
}

function GetQueryStringParameterList(list) {
    var q = "";
    for (var i = 0; i < parameterList.length; i++) {
        for (var j = 0; j < list.length; j++) {
            if (list[j] == parameterList[i]["id"]) {
                if (q.length == 0) {
                    q = "?" + parameterList[i]["id"] + "=" + parameterList[i]["value"];
                }
                else {
                    q = q + "&" + parameterList[i]["id"] + "='" + parameterList[i]["value"] + "'";
                }
            }
        }
    }
    //console.log(q);
    return q;
}

function DisbaleControls(UserRole, isDisbale) {
    if ((UserRole.toUpperCase() == 'PROD3')) {
        $("[role=QC]").prop("disabled", isDisbale);
    }
    else {
        $("[role=PROD]").prop("disabled", isDisbale);
    }
}

function ResetAddRowLine(table, btnNew) {
    $("#" + table).find("tbody tr:first").hide();
    $("#" + table).find("tbody tr:first").css("background-color", "#fbffb2");
    $("#" + btnNew).html('<i class="fa fa-plus"></i> New');
    clearFirstRowLine($("#" + table).find("tbody tr:first"));
}

function clearFirstRowLine(objFirstLine) {
    $('input[type!="hidden"]', objFirstLine).val('');
}

function ShowNewLineRecord(obj, table) {
    $('#' + table).find("tbody tr:first").toggle();
    $('#' + table).find("tbody tr:first select:first").focus();
    if ($('#' + table).find("tbody tr:first").is(":visible")) {
        $(obj).html('<i class="fa fa-times"></i> Cancel');
        
        var $tr = $('#' + table).find("tbody tr:first");
        $tr.css('background-color', '#ffffff');
        $tr.find("input,select,textarea").each(function () {
            if ((userAssignrole.toUpperCase() == 'PROD3')) {
                if ($(this).attr("role").toUpperCase() == "PROD") {
                    $(this).removeAttr("readonly");
                    $(this).removeAttr("disabled");
                    $(this).addClass("PRODTextbox");
                    $(this).addClass("EditableTextColor");
                }
                else {
                    $(this).addClass("QCTextbox");
                    $(this).prop('readonly', true);
                    $(this).addClass("NonEditableTextColor");
                }

            }
            else {
                if ($(this).attr("role").toUpperCase() == "QC") {
                    $(this).removeAttr("readonly");
                    $(this).removeAttr("disabled");
                    $(this).addClass("EditableTextColor");
                    $(this).addClass("QCTextbox");
                }
                else {
                    $(this).addClass("PRODTextbox");
                    $(this).prop('readonly', true);
                    $(this).addClass("NonEditableTextColor");
                }
            }
        });
    }
    else {
        $(obj).html('<i class="fa fa-plus"></i> New');
        clearFirstRowLine($('#' + table).find("tbody tr:first"));
    }
}

function DeleteProtocolLineWithConfirmation(rowID, headerId, Action, table) {
    bootbox.dialog({
        title: "Delete Confirmation",
        message: "Do you want to delete?",
        size: "medium",
        buttons: {
            Yes: {
                Label: "Yes",
                className: 'btn green',
                callback: function () {
                    DeleteProtocolLine(rowID, headerId, Action, table);
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }
        }
    });
}

function DeleteProtocolLine(rowID, headerId, Action, table) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + Action,
        datatype: 'json',
        async: false,
        data: { lineId: rowID, headerId: headerId },
        success: function (result) {
            if (result.Key) {
                DisplayNotiMessage("success", result.Value, "Success");
                $('#' + table).DataTable().ajax.reload();
            }
        },
        error: function () {
            alert('error...');
        }
    });
}

function DeleteProtocolHeaderData(HeaderId, URL) {
    var formdata = new FormData();
    formdata.append("MainHeaderId", HeaderId);

    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {

        },
        error: function (data) { }
    });
}

function SaveProtocolLineData(formdata, URL, table, btnNew) {
    var tabledata = $("#" + table).find("input,select,textarea,hidden").serializeArray();
    if (tabledata != null && tabledata != undefined) {
        $.each(tabledata, function (key, input) {
            formdata.append(input.name, input.value);
        });
    }

    //$("#" + table + " tbody>tr:first").find('input,select,textarea').each(function () {
    //    //console.log($(this).attr("name"));
    //    if ($(this).attr("type") == "text" || $(this).attr("type") == "select" || $(this).attr("type") == "textarea") {
    //        formdata.append($(this).attr("name"), $(this).val())
    //    }
    //    else if ((type == "checkbox" || type == "radio")) {
    //        formdata.append($(this).attr("name"), $(this).is(":checked"))
    //    }
    //})

    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.Key && res.HeaderId > 0) {
                DisplayNotiMessage("success", res.Value, "Success");
                $('#' + table).DataTable().ajax.reload();
                ShowNewLineRecord($('#' + btnNew));
            }
            else
                DisplayNotiMessage("error", res.Value, "Error");
        },
        error: function (data) { }
    });
}

function SaveProtocolHeaderData(frmProtocolHeader, HeaderId, URL, userRole) {
    if ($("#" + frmProtocolHeader).valid()) {

        DisbaleControls(userRole, false);
        var frmHeaderValues = $("#" + frmProtocolHeader).serializeArray();
        DisbaleControls(userRole, true);

        var formdata = new FormData();
        if (frmHeaderValues != null && frmHeaderValues != undefined) {
            $.each(frmHeaderValues, function (key, input) {
                formdata.append(input.name, input.value);
            });
            formdata.append("MainHeaderId", HeaderId);
            $.ajax({
                url: WebsiteURL + URL,
                type: "POST",
                data: formdata,
                async: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading(true);
                },
                complete: function () {
                    showLoading(false);
                },
                success: function (res) {
                    if (res.Key && res.HeaderId > 0) {
                        DisplayNotiMessage("success", res.Value, "Success");
                    }
                    else
                        DisplayNotiMessage("error", res.Value, "Error");
                },
                error: function (data) { }
            });
        }
    }
    else {
        DisplayNotiMessage("info", "All mandatory (*) fields are required", "Info");
    }
}

function SetAccessControlForRole(UserRole) {
    userAssignrole = UserRole;
    if ((UserRole.toUpperCase() == 'PROD3')) {
        $("[role=QC]").prop("disabled", true);
        $("[role=QC]").addClass("NonEditableTextColor");
        $("[role=PROD]").addClass("EditableTextColor");
        if ($('#btncal1').closest('div.date-picker').find("[role=QC]").length > 0) {
            $('#btncal1').prop("disabled", true)
            $('.calender1').prop("disabled", true)
            
        }
    }
    else if ((UserRole.toUpperCase() == 'QC3')) {
        $("[role=PROD]").prop("disabled", true);
        $("[role=QC]").addClass("EditableTextColor");
        $("[role=PROD]").addClass("NonEditableTextColor");
        if ($('#btncal1').closest('div.date-picker').find("[role=PROD]").length > 0) {
            $('#btncal1').prop("disabled", true);  $('.calender1').prop("disabled", true)
        }
    }
    else {
        $("[role=PROD]").prop("disabled", true);
        $("[role=QC]").prop("disabled", true);
        $("[role=QC]").addClass("EditableTextColor");
        $("[role=PROD]").addClass("EditableTextColor");
        if ($('#btncal1').closest('div.date-picker').find("[role=QC]").length > 0) {
            $('#btncal1').prop("disabled", true); $('.calender1').prop("disabled", true);
        }
        if ($('#btncal1').closest('div.date-picker').find("[role=PROD]").length > 0) {
            $('#btncal1').prop("disabled", true); $('.calender1').prop("disabled", true)
        }
    }

    $("[role=QC]").addClass("QCTextbox");
    $("[role=PROD]").addClass("PRODTextbox");
}

function totalOvality(txt) {

    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];
    $(".calc").each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + txt).val(Ovality);
    }
    else {
        $("#" + txt).val("");
    }
}
function totalOvalityCustom(clsSelector, resid) {

    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];
    $("." + clsSelector).each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + resid).val(Ovality);
    }
    else {
        $("#" + resid).val("");
    }
}
function calculateMaxMin(cls,txt) {

    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];
    $("." + cls).each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + txt).val(Ovality);
    }
    else {
        $("#" + txt).val("");
    }
}
function SetEditable(isEdit) {
    if (isEdit.toLowerCase() == "true") { }
    else {
        $('div.dvDisable :input')
            .not($('.dataTables_length').find('input,select,search,textarea'))
            .not($('.dataTables_filter').find('input,select,search,textarea'))
            .prop('disabled', true);
        $('div.dvDisable a')
            .not($('.dataTables_paginate').find('input,select,search,textarea'))
            .not("#btnPrint")
            .hide();
    }
}
function SetEditableDetails(isEdit) {
    if (isEdit.toLowerCase() == "true") { }
    else {
        $('form.form-horizontal :input')
            .not($('.dataTables_length').find('input,select,search,textarea'))
            .not($('.dataTables_filter').find('input,select,search,textarea'))
            .prop('disabled', true);
        $('form.form-horizontal a')
            .not($('.dataTables_paginate').find('input,select,search,textarea'))
            .not("#btnPrint")
            .hide();
    }
}
function PrintReport(url, HeaderId) {
    var dat = [];
    dat.push({ Param: "HeaderId", Value: HeaderId });
    ShowReport(dat, url);
    return false;
}

//function SaveProtocolLine(formdata,URL, table, btnNew) {
//    //var formdata = new FormData();    
//    $("#" + table + " tbody>tr:first").find('input,select,textarea').each(function () {
//        //console.log($(this).attr("name"));
//        if ($(this).attr("type") == "text" || $(this).attr("type") == "select" || $(this).attr("type") == "textarea") {
//            formdata.append($(this).attr("name"), $(this).val())
//        }
//        else if ((type == "checkbox" || type == "radio")) {
//            formdata.append($(this).attr("name"), $(this).is(":checked"))
//        }
//    })
//    SaveProtocolLineData(formdata, URL, table, btnNew);
//}

var InlineGridProtocolEdit = function () {
    var edittbl;
    var btnAddNew;
    //var contentdiv;
    var editetr = {};
    function Initialize(btnid, tblid, dv) {
        edittbl = $(dv).find($(tblid));
        btnAddNew = $(dv).find($(btnid));

        //contentdiv = dv;   
        //console.log(edittbl);
        //console.log(btnAddNew);
        editetr = {};
        btnAddNew.unbind("click");
        btnAddNew.click(function () {
            //InsertNewRecordInline($(this), tbl);
        });
        EditLineEnable();
        SetNumericField();
        SetAddNewIntialStage();
    }
    function InsertNewRecordInline(obj, tbl) {
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });
        tbl.find("tbody tr:first").toggle();
        tbl.find("tbody tr:first select:first").focus();
        $(obj).html('<i class="fa fa-times"></i> Cancel');
        if (tbl.find("tbody tr:first").is(":visible")) {
            $(obj).val("Cancel");
            var delayedFn, blurredFrom;
            tbl.find("tbody tr:first").on('blur', 'a', function (event) {
                blurredFrom = event.delegateTarget;
                delayedFn = setTimeout(function () { SaveLineRecord(0); }, 0);
            });
            tbl.find("tbody tr:first").on('focus', 'a', function (event) {
                if (blurredFrom === event.delegateTarget) {
                    clearTimeout(delayedFn);
                }
            });

            if (editetr.id != null && editetr.id != undefined && editetr.id != "undefined") {
                var $tr = $("[data-id='" + editetr.id + "']");
                $tr.html(editetr.text);
            }
            EditLineEnable();
        }
        else {
            $(obj).val("New");
            $(obj).html('<i class="fa fa-plus"></i> New');
        }
    }

    function EditLineEnable() {
        //remove previous click event
        $(edittbl).find(".editline").unbind("click");

        //add new click event
        $(edittbl).find(".editline").click(function () {
            //If any rows already in edit mode then set as normal row
            edittbl.find('tbody tr').not("tbody tr:first").each(function () {
                $trold = $(this);
                //set readonly or disabled property to all element
                $trold.find('input,select,textarea').removeClass('PRODTextbox QCTextbox EditableTextColor NonEditableTextColor');
                $trold.find('input,select,textarea').attr("readonly", "readonly");

                //$tr.find('input,select,textarea').attr("disabled", "disabled");                
                //find any div edit with editmode. If any then hide update and cancel button and show edit button
                if ($trold.find('.editmode').length > 0) {
                    $trold.find('.editmode').hide();
                    $trold.find('.editline').show();
                }
            })

            var $tr = $(this).closest("tr");

            edittbl = $(this).closest('table');
            EditLine($(this), $tr.data("hid"), $tr.data("id"), $tr.data("gid"));
        });
    }

    function EditLine(btnEdit, headerId, lineId, gridId) {
        var $tr = $(btnEdit).closest("tr");
        editetr.text = $tr.html();
        editetr.id = $tr.data("id");
        var $td = $(btnEdit).closest("td");
        //console.log($tr);
        //remove readonly attribute for row so user can edit data.
        $tr.find("input,select,textarea").each(function () {
                if ((userAssignrole.toUpperCase() == 'PROD3')) {
                    if ($(this).attr("role").toUpperCase() == "PROD") {
                        $(this).removeAttr("readonly");
                        $(this).removeAttr("disabled");
                        $(this).addClass("PRODTextbox");
                        $(this).addClass("EditableTextColor");
                    }
                    else {
                        $(this).addClass("QCTextbox");
                        $(this).prop('readonly', true);
                        $(this).addClass("NonEditableTextColor");
                    }

                }
                else {
                    if ($(this).attr("role").toUpperCase() == "QC") {
                        $(this).removeAttr("readonly");
                        $(this).removeAttr("disabled");
                        $(this).addClass("EditableTextColor");
                        $(this).addClass("QCTextbox");
                    }
                    else {
                        $(this).addClass("PRODTextbox");
                        $(this).prop('readonly', true);
                        $(this).addClass("NonEditableTextColor");
                    }
                }
            //$(this).removeAttr("readonly");
            //$(this).removeAttr("disabled");
        });
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });


        //check there is any div exist with update and cancel button div. If exist then show div otherwise create new div for update and cancel button.
        if ($td.find('.editmode').length == 0) {
            var editdiv = "<div class='editmode' style='display:inline;'><i id='Update" + lineId + "' name='Update" + lineId + "' style='cursor: pointer;' title='Update Record' class='fa fa-floppy-o iconspace ' onclick='UpdateProtocolLine(" + lineId + "," + gridId + ");'></i>";
            editdiv += "<i id='btnCancel" + lineId + "' name='btnCancel" + lineId + "' style='cursor: pointer;' title='Cancel Edit' class='fa fa-ban iconspace canceledit'></i></div> "
            $td.append(editdiv);
        }
        else {
            $td.find('.editmode').show();
        }
        //hide edit button while enable edit mode
        $(btnEdit).hide();

        SetAddNewIntialStage();
        CancelEditEnable();
        SetNumericField();
    }

    function CancelEditEnable() {
        $(edittbl).find(".canceledit").unbind("click");
        $(edittbl).find(".canceledit").click(function () {
            CancelEditLine($(this));
        });
    }
    function CancelEditLine(btnCancel) {
        var $tr = $(btnCancel).closest("tr");
        $tr.html(editetr.text);
        editetr = {};
        SetAddNewIntialStage();
        EditLineEnable();
    }

    function SetAddNewIntialStage() {
        btnAddNew.html('<i class="fa fa-plus"></i> New');
        if (edittbl.find("tbody tr:first").is(":visible")) {
            edittbl.find("tbody tr:first").toggle();
        }
    }

    function SetNumericField() {
        jQuery(function ($) {
            $('.numeric').autoNumeric('init', { aPad: "fase", vMax: "1000000000", aForm: "false" });
            $('.numeric18').autoNumeric('init', { aPad: "fase", vMax: "999999999999999999", aForm: "false" });
            $('.numeric4').autoNumeric('init', { aPad: "fase", vMax: "9999", aForm: "false" });
        });
    }

    return {
        init: function (btnid, tblid, dv) {
            Initialize(btnid, tblid, dv);
        }
    }
}();
function ExpandCollapsChild(id, obj) {
    var src = $(obj).attr("src");
    if (src == WebsiteURL + "/Images/details_open.png") {
        $(obj).attr("src", WebsiteURL + "/Images/details_close.png");
    }
    else if (src == WebsiteURL + "/Images/details_close.png") {
        $(obj).attr("src", WebsiteURL + "/Images/details_open.png");
    }
    $('span.' + id).parent().parent().toggle();
}