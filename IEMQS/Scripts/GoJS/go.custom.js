﻿var tempHoldLayoutType = "";
function UpdateLayout(myDiagram, LayoutType) {
    tempHoldLayoutType = LayoutType;
    showLoading(true);
    var otherlinks = [];
    myDiagram.links.each(function (item) {
        if (item.$d && item.$d.frompid != "t_iasp" && item.$d.frompid != "t_iaspO") {
            item.$d.points = null;
            otherlinks.push(item.$d);
        }
    });
    myDiagram.model.removeLinkDataCollection(otherlinks);
    setTimeout(function () {
        if (LayoutType == "GridLayout")
            myDiagram.layout = go.GraphObject.make(go.GridLayout, { isOngoing: false });
        else if (LayoutType == "ForceDirectedLayout")
            myDiagram.layout = go.GraphObject.make(go.ForceDirectedLayout, { isOngoing: false });
        else if (LayoutType == "LayeredDigraphLayout")
            myDiagram.layout = go.GraphObject.make(go.LayeredDigraphLayout, { direction: 90, layeringOption: go.LayeredDigraphLayout.LayerOptimalLinkLength, isOngoing: false });
        else if (LayoutType == "CircularLayout")
            myDiagram.layout = go.GraphObject.make(go.CircularLayout, { isOngoing: false });
        else {
            if (LayoutType == "Tree-CenterChildren") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentCenterChildren;
            else if (LayoutType == "Tree-CenterSubtrees") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentCenterSubtrees;
            else if (LayoutType == "Tree-Start") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentStart;
            else if (LayoutType == "Tree-End") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentEnd;
            else if (LayoutType == "Tree-Bus") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentBus;
            else if (LayoutType == "Tree-BusBranching") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentBusBranching;
            else if (LayoutType == "Tree-TopLeftBus") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentTopLeftBus;
            else if (LayoutType == "Tree-BottomRightBus") myDiagram_layout_prop.alternateAlignment = go.TreeLayout.AlignmentBottomRightBus;

            myDiagram.layout = go.GraphObject.make(go.TreeLayout, myDiagram_layout_prop);
        }

        setTimeout(function () {
            myDiagram.model.addLinkDataCollection(otherlinks);
            showLoading(false);
        }, 2000);
    }, 1000);
}

function searchDiagram(search, gojsDiagram) {  // called by search button
    gojsDiagram.startTransaction("highlight search");
    if (search) {
        // search four different data properties for the string, any of which may match for success
        // create a case insensitive RegExp from what the user typed
        var regex = new RegExp(search, "i");
        var results = gojsDiagram.findNodesByExample({ name: regex });
        gojsDiagram.highlightCollection(results);
        // try to center the diagram at the first node that was found
        if (results.count > 0) gojsDiagram.centerRect(results.first().actualBounds);
    } else {  // empty string only clears highlighteds collection
        gojsDiagram.clearHighlighteds();
    }
    gojsDiagram.commitTransaction("highlight search");
}

function makeButton(text, action, visiblePredicate) {
    return GoJs("ContextMenuButton",
        GoJs(go.TextBlock, text), { click: action },
        visiblePredicate ? new go.Binding("visible", "", function (o, e) { return o.diagram ? visiblePredicate(o, e) : false; }).ofObject() : {});
}

function MoveNode(myDiagram, move, isshift) {
    if (["ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(move) == -1)
        return false;
    myDiagram.model.commit(function (m) {
        var movestep = isshift ? 30 : 2;
        myDiagram.selection.each(function (node) {
            if (node instanceof go.Node && node.data["loc"]) {
                var loc = node.data["loc"].split(' ');
                switch (move) {
                    case "ArrowUp":
                        loc = loc[0] + ' ' + (+loc[1] - movestep);
                        break;
                    case "ArrowDown":
                        loc = loc[0] + ' ' + (+loc[1] + movestep);
                        break;
                    case "ArrowLeft":
                        loc = (+loc[0] - movestep) + ' ' + loc[1];
                        break;
                    case "ArrowRight":
                        loc = (+loc[0] + movestep) + ' ' + loc[1];
                        break;
                }
                m.set(node.data, "loc", loc);
            }
        });
    }, "change");
}

function SetOverview(divId, gojsDiagram) {
    myOverview = GoJs(go.Overview, divId , {
        observed: gojsDiagram,
        contentAlignment: go.Spot.Center,
        'box.resizable': true,
        'resizingTool': new OverviewResizingTool()
    });
}

function SetDragZoom(elem, gojsDiagram) {
    if ($(elem).hasClass('activebutton')) {
        $(elem).removeClass('activebutton');
        gojsDiagram.toolManager.mouseMoveTools.removeAt(2);
    } else {
        $(elem).addClass('activebutton');
        gojsDiagram.toolManager.mouseMoveTools.insertAt(2, new DragZoomingTool());
    }
}

function gojsCollapseAll(gojsDiagram, collapseLevel, isFit) {
    //collapseLevel = collapseLevel | 0;
    //for (var i = 20; i > collapseLevel ; i--) {
    //    gojsDiagram.findTreeRoots().each(function (n) { n.collapseTree(i) });
    //}
    gojsDiagram.findNodesByExample({ producttype: "PLT" }).each(function (n) {
        var parentNode = gojsDiagram.findNodeForKey(n.data.parent);
        if (parentNode) parentNode.collapseTree();
    });

    if (isFit != false)
        setTimeout(function () { myDiagram.commandHandler.resetZoom() }, 500);
}

function gojsExpandAll(gojsDiagram,isFit) {
    gojsDiagram.findTreeRoots().each(function (n) { n.expandTree(1000); });
    if (isFit != false) setTimeout(function () { myDiagram.zoomToFit(); }, 500)
}

function SetSearch(elem, gojsDiagram) {
    var ParantDiv = $(elem).parent();
    if ($(elem).hasClass('activebutton')) {
        $(elem).removeClass('activebutton');
        $("#GojsSearchBox", ParantDiv).hide(500);
        setTimeout(function () { $("#GojsSearchBox", ParantDiv).remove() }, 500);
        searchDiagram("", gojsDiagram);
    } else {
        $(elem).addClass('activebutton');
        $(elem).before('<input id="GojsSearchBox" style="position: absolute; right: 25px; display:none;" />');
        $("#GojsSearchBox", ParantDiv).show(500).focus()
        $("#GojsSearchBox", ParantDiv).keydown(function (e) {
            if (e.keyCode == 13 || e.keyCode == 9)
                searchDiagram($("#GojsSearchBox", ParantDiv).val(), gojsDiagram);
        });
    }
}

function GenNewKey() {
    return +(Date.now() + "").substring(7) * -1; // new add should be nagative key
}

function FullScreen(divID) {
    fnFullScreen(divID, !IsFullScreen());
}
function IsFullScreen() {
    return (document.fullscreenElement && document.fullscreenElement !== null) ||
             (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
             (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
             (document.msFullscreenElement && document.msFullscreenElement !== null);
}
function fnFullScreen(divID, TurnOn) { 
    if (TurnOn) {
        var docElm = $('#' + divID).parent()[0];
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (docElm.msRequestFullscreen) {
            docElm.msRequestFullscreen();
        }
        $('#' + divID).css("min-height", "100vh");
        $('.btnFullScreen').html('<i class="fa fa-compress"></i>');
    } else { 
        if (document.exitFullscreen) {
            document.exitFullscreen().catch(() => { });
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        $('#' + divID).css("min-height", "");
        $('.btnFullScreen').html('<i class="fa fa-expand"></i>');
    }
}
