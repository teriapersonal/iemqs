﻿ 
function ejDiagramCMD(EjID, cmd, element) {
    if (cmd == "hand") {
        if ($(element).html() == '<i class="fa fa-hand-grab-o"></i>') {
            $(element).html('<i class="fa fa-arrows"></i>');
            $('#' + EjID).ejDiagram('instance').update({ tool: ej.datavisualization.Diagram.Tool.All });
        } else {
            $(element).html('<i class="fa fa-hand-grab-o"></i>');
            $('#' + EjID).ejDiagram('instance').update({ tool: ej.datavisualization.Diagram.Tool.ZoomPan });
        }
    } else if (cmd == "search") {
        $('#ejSearch', '#ejControl_' + EjID).toggle(1000);
    } else if (cmd == "zoomin") {
        var zoom = ej.datavisualization.Diagram.Zoom();
        zoom.zoomFactor = 1;
        zoom.zoomCommand = 1;
        $('#' + EjID).ejDiagram('instance').zoomTo(zoom);
    } if (cmd == "zoomout") {
        var zoom = ej.datavisualization.Diagram.Zoom();
        zoom.zoomFactor = 1;
        zoom.zoomCommand = 2;
        $('#' + EjID).ejDiagram('instance').zoomTo(zoom);
    } if (cmd == "orientation") {
        $('#' + EjID).ejDiagram({
            layout: { orientation: element.value }
        });
    } if (cmd == "overview") {
        $('#ejOverview', '#ejControl_' + EjID).toggle(1000);
        $('#ejOverview', '#ejControl_' + EjID).ejOverview({ sourceID: EjID, width: "200px", height: "140px" });
    }
    if (cmd == "fullscreen") {
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
       (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
       (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
       (document.msFullscreenElement && document.msFullscreenElement !== null);
        if (!isInFullScreen) {
            var docElm = $('#' + EjID).parent()[0];
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            } else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            } else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            } else if (docElm.msRequestFullscreen) {
                docElm.msRequestFullscreen();
            }
            $('#' + EjID).css("min-height", "100%");

        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
            $('#' + EjID).css("min-height", "");
        }
    }
}

function ejHighlightNode(target, txtsearch) {
    $('.highlightNode').removeClass('highlightNode');
    if (txtsearch) {
        var reg = new RegExp(txtsearch, "i");
        $(".ej-d-group", "#" + target).each(function (idx, item) {
            if ($(item).text().match(reg))
                $(item).addClass('highlightNode')
        });
    }
}

jQuery.fn.EjDiagram = function (Params) {
    try {
        Params.sDivID = this[0].id;
        Params.sEjID = "ejDiagram_" + Params.sDivID;

        // ----- Default ----- //
        {
            if (Params.sHeight == undefined)
                Params.sHeight = "70vh";
            if (Params.iNodeMargin == undefined)
                Params.iNodeMargin = 30;
            if (Params.oNodeDefaultSettings == undefined) {
                Params.oNodeDefaultSettings = {
                    width: 100, height: 70, borderColor: "red", fillColor: "#ccf7e3", type: "bpmn", shape: "activity",
                };
            }
            if (Params.oNodeDefaultSettings.labels == undefined)
                Params.oNodeDefaultSettings.labels = [{ name: Params.sDisplay, bold: true, fontSize: 15, offset: { x: 0.50, y: 0.50 } }, { name: "nodeData", fontSize: 0 }];
            if (Params.iDisplayMaxLength == undefined)
                Params.iDisplayMaxLength = 30;
            if (Params.bIsTooltip == undefined) 
                Params.bIsTooltip = false;
            if (Params.aTooltipData)
                Params.bIsTooltip = true;

            Params.ejSetting = {
                width: "100%", height: Params.sHeight,
                tool: ej.datavisualization.Diagram.Tool.ZoomPan,
                layout: {
                    type: ej.datavisualization.Diagram.LayoutTypes.OrganizationalChart, horizontalSpacing: Params.iNodeMargin, verticalSpacing: Params.iNodeMargin, orientation: ej.datavisualization.Diagram.LayoutOrientations.TopToBottom,
                },
                nodeTemplate: function (diagram, node) {
                    var text = node[Params.sDisplay];
                    if (text && text.length > Params.iDisplayMaxLength)
                        text = text.substr(0, Params.iDisplayMaxLength - 3)+'...';

                    node.labels[0].text = text;
                    if (Object.values) // validate for firebox not support
                        node.labels[1].text = Object.values(node).join(',');
                },
                //set the default properties of the node & connector
                defaultSettings: {
                    node: Params.oNodeDefaultSettings,
                    connector: {
                        segments: [{ type: "orthogonal" }],
                        cornerRadius: 10,
                       // constraints: ej.datavisualization.Diagram.ConnectorConstraints.Default | ej.datavisualization.Diagram.ConnectorConstraints.Bridging
                    }
                },
                //pageSettings: { scrollLimit: "diagram" },
                enableContextMenu: false
            };

            if (Params.fnOnClick || Params.sLink) {
                Params.ejSetting.click = function (args) {
                    if (Params.fnOnClick && typeof Params.fnOnClick == "function")
                        Params.fnOnClick(args.element)
                    if (Params.sLink) {
                        var linkparams = Params.sLink.match(/\{\{\:(\w)+\}\}/g);
                        var link = Params.sLink;
                        $.each(linkparams, function (idx, item) {
                            link = link.replace(item, args.element[item.match(/(\w)+/g)[0]])
                        })
                        if (link && link.indexOf('undefined') == -1 && link.indexOf('null') == -1)
                            window.open((Params.sLink.indexOf('://') == -1 ? WebsiteURL : "") + link);
                    }
                };
            }
        }
        // ----- Functions ----- //
        {
            Params.fnDestroy = function () {
                if (Params.oEjDiagram) {
                    Params.oEjDiagram.remove();
                    $("#" + Params.sEjID).empty().attr({ 'style': '', 'class': '' });
                    Params.oEjDiagram = undefined;
                }
            };

            function GetNodeIndex(Parent) {
                for (var i = 0; i < Params.aoNodes.length; i++) {
                    if (Params.aoNodes[i][Params.sChild] == Parent)
                        return i;
                }
                return -1;
            }

            function GetGroupIndex(Group) {
                for (var i = 0; i < Params.aoNodeGroup.length; i++) {
                    if (Params.aoNodeGroup[i][Params.Group] == Group)
                        return i;
                }
                return -1;
            }

            Params.fnSetNodes = function (Nodes, Display, Child, Parent, Group) {
                Params.aoNodes = Nodes || Params.aoNodes;
                Params.sDisplay = Display || Params.sDisplay;
                Params.sChild = Child || Params.sChild;
                Params.sParent = Parent || Params.sParent;
                Params.Group = Group || Params.Group;
                if (Params.sChild && Params.sParent) {
                    Params.aoNodeConnection = []
                    Params.aoNodeGroup = [];
                    $.each(Params.aoNodes, function (idx, item) {
                        item.name = "node" + idx;
                        if (Params.Group && item[Params.Group]) {
                            var GroupIndex = GetGroupIndex(item[Params.Group]);
                            if (GroupIndex == -1) {
                                var group = {
                                    name: "group" + idx,
                                    tooltip:false,
                                    borderColor: "darkCyan",
                                    borderDashArray: "4,2",
                                    borderWidth: 10,
                                    children: [item.name],
                                    labels: [{ name: "label1", bold: true, fontSize: 15 }]
                                };
                                group[Params.Group] = item[Params.Group];
                                Params.aoNodeGroup.push(group)
                            } else {
                                Params.aoNodeGroup[GroupIndex].children.push(item.name)
                            }
                        }
                        if (typeof item[Params.sParent] == "object") {
                            $.each(item[Params.sParent], function (jidx, jitem) {
                                var target = GetNodeIndex(jitem);
                                if (target == -1)
                                    return;
                                var conn = { name: "conn" + idx + jidx, sourceNode: "node" + target, targetNode: "node" + idx };
                                conn[Params.sChild] = item[Params.sChild];
                                conn[Params.sParent] = jitem;
                                if (item[Params.sConnStyle] == 2) {
                                    conn.lineWidth = 2;
                                    conn.lineDashArray = "2,2";
                                }
                                Params.aoNodeConnection.push(conn);
                            });
                        } else {
                            var target = GetNodeIndex(item[Params.sParent]);
                            if (target == -1)
                                return;
                            var conn = { name: "conn" + idx, sourceNode: "node" + target, targetNode: "node" + idx };
                            conn[Params.sChild] = item[Params.sChild];
                            conn[Params.sParent] = item[Params.sParent];
                            if (item[Params.sConnStyle] == 2) {
                                conn.lineWidth = 2;
                                conn.lineDashArray = "2,2";
                            }
                            Params.aoNodeConnection.push(conn);
                        }
                    });
                }
                if (Params.oEjDiagram) {
                    Params.oEjDiagram.clear();
                    $("#" + Params.sEjID).ejDiagram({ nodes: Params.aoNodes, connectors: Params.aoNodeConnection });
                    Params.oEjDiagram.layout();
                    if (Params.aoNodeGroup && Params.aoNodeGroup.length)
                        Params.oEjDiagram.add(Params.aoNodeGroup);
                }
                return Params.aoNodes;
            };

            Params.fnSetTooltipData = function (TooltipData) {
                var tooltipData = '';
                Params.aTooltipData = TooltipData || Params.aTooltipData;
                if (!Params.aTooltipData && Params.aoNodes) {// auto set on first node
                    Params.aTooltipData = [];
                    for (var column in Params.aoNodes[0]) {
                        Params.aTooltipData.push({ column: column })
                    }
                }
                $.each(Params.aTooltipData, function (idx, item) {
                    tooltipData += '<tr><th> &nbsp; ' + (item.label || item.column || item) + ' &nbsp; </th><td> &nbsp; {{:' + (item.column || item) + '}} &nbsp; </td></tr>';
                });

                $("#tooltip_" + Params.sEjID).remove();
                $("#" + Params.sDivID).append('<script type="text/x-jsrender" id="tooltip_' + Params.sEjID + '">\
                    <table border="1" style="background-color: #fbfffb; border-radius: 100px; border:3px solid red; white-space: nowrap; box-shadow: 0 10px 6px -6px #777;">' + tooltipData + '</table>\
                </script>')
            }
            if (Params.oEjDiagram) {
                var tooltip = {
                    templateId: "tooltip_" + Params.sEjID,
                    relativeMode: ej.datavisualization.Diagram.RelativeMode.Mouse,
                    margin: { top: 0, bottom: 0, left: 10 },
                }
                $("#" + Params.sEjID).ejDiagram({ tooltip: tooltip });

                Params.oEjDiagram.layout();
            }
        }

        $(this).html('<style>.highlightNode {box-shadow: 0px 0px 20px 15px #ffbc00;}</style>\
        <div id="ejControl_' + Params.sEjID + '" style="text-align: right; position: absolute;z-index: 1;right: 90px;">\
            <input id="ejSearch" type="text" hidden placeholder="Search..." onkeyup="ejHighlightNode(\'' + Params.sEjID + '\',this.value)" />\
            <div id="ejOverview" style="display:none;background-color: #b7b7b738;box-shadow: -2px 4px 5px 0px #989191;"></div>\
        </div>\
        <div id="ejCMD_' + Params.sDivID + '" style="text-align: right; position: absolute;z-index: 1;right: 57px;display:none">\
            <button title="Search" onclick="ejDiagramCMD(\'' + Params.sEjID + '\',\'search\',this)"><i class="fa fa-search"></i></button><br />\
            <button title="Overview" onclick="ejDiagramCMD(\'' + Params.sEjID + '\',\'overview\',this)"><i class="fa fa-square-o"></i></button><br />\
            <button title="Zoom In" onclick="ejDiagramCMD(\'' + Params.sEjID + '\',\'zoomin\')"><i class="fa fa-plus"></i></button><br />\
            <button title="Zoom Out" onclick="ejDiagramCMD(\'' + Params.sEjID + '\',\'zoomout\')"><i class="fa fa-minus"></i></button><br />\
            <button title="Full Screen" onclick="ejDiagramCMD(\'' + Params.sEjID + '\',\'fullscreen\',this)"><i class="glyphicon glyphicon-fullscreen"></i></button><br />\
            <button title="Move" onclick="ejDiagramCMD(\'' + Params.sEjID + '\',\'hand\',this)" hidden><i class="fa fa-hand-grab-o"></i></button>\
            <select title="Orientation" onchange="ejDiagramCMD(\'' + Params.sEjID + '\',\'orientation\',this)">\
                <option title="Top-Bottom" value="toptobottom">T</option>\
                <option title="Left-Right" value="lefttoright">L</option>\
                <option title="Right-Left" value="righttoleft">R</option>\
                <option title="Bottom-Top" value="bottomtotop">B</option>\
            </select>\
        </div>\
        <div id="' + Params.sEjID + '"></div>')

        if (Params.bIsTooltip) {
            Params.fnSetTooltipData(Params.aTooltipData);

            Params.ejSetting.tooltip = {
                templateId: "tooltip_" + Params.sEjID,
                relativeMode: ej.datavisualization.Diagram.RelativeMode.Mouse,
                margin: { top: 0, bottom: 0,left:10 },
            };
        }
        
        if (Params.aoNodes) {
            Params.ejSetting.nodes = Params.fnSetNodes(Params.aoNodes, Params.sChild, Params.sParent)
            Params.ejSetting.connectors = Params.aoNodeConnection;
        }
        $("#" + Params.sEjID).ejDiagram(Params.ejSetting);
        Params.oEjDiagram = $("#" + Params.sEjID).ejDiagram('instance');
        $("#ejCMD_" + Params.sDivID).show();
        
        return Params;
    } catch (e) {
        console.log(e);
        return false;
    }
}



