﻿var loadingCnt = 0;
$(document).ready(function () {
    $('input[maxlength],textarea[maxlength]').maxlength({ alwaysShow: true, });
    $(document).keydown(function (e) {
        if (e.keyCode == 27) { // To Close Bootbox Popup
            $('button[data-dismiss="modal"]').click();
        }
        if (e.ctrlKey && e.keyCode == 112) { // Set Ready to open Table Column Detail pop
            isElementSelectedforTableInfo = false;
            $("#tblTableColumn > tr").css("background", "unset");
            DisplayNotiMessage("info", "Click on any textbox or element to see table column detail", "Info");
        }
        /* ----- Developed By Ravi Makwana ----- */
        if (e.ctrlKey) {
            $('[class*="ctrl-"]:not([data-ctrl])').each(function (idx, item) {
                var Key = $(item).prop('class').substr($(item).prop('class').indexOf('ctrl-') + 5, 1).toUpperCase();
                $(item).attr("data-ctrl", Key);
                $(item).append('<div class="tooltip fade top in tooltip-ctrl" role="tooltip" style="top: -32.0278px; display: block; visibility: visible;"><div class="tooltip-arrow" style="left: 49%;"></div><div class="tooltip-inner"> CTRL + ' + Key + '</div></div>');
            });
        }

        if (e.ctrlKey && e.which != 17) {
            var Key = String.fromCharCode(e.which).toLowerCase();
            if ($('.ctrl-' + Key).length == 1) {
                e.preventDefault();
                if (!$('#divLoader').is(":visible")) $('.ctrl-' + Key).click();
            }
        }
        // ------------------------------------- //
    })
    $(document).keyup(function (e) {
        if (!e.ctrlKey) {
            $('[class*="ctrl-"]').removeAttr("data-ctrl");
            $(".tooltip-ctrl").remove();
        }
    });
});

function showLoading(flag) {
    if (flag) {
        $(".quick-nav-overlay").show();
        $("#divLoader").show();
        loadingCnt = loadingCnt + 1;
        //console.log("loadingCnt : " + loadingCnt);
    }
    else {        
        loadingCnt = loadingCnt - 1;
        //console.log("loadingCnt : " + loadingCnt);
        if (loadingCnt <= 0) {
            $("#divLoader").hide();
            $(".quick-nav-overlay").hide();
        }
    }
}

function clearFirstRow(objFirstLine) {
    $('input[type="text"],select', objFirstLine).val('');
    $('input[type="checkbox"]', objFirstLine).prop('checked', false);
}

function DisplayNotiMessage(Type, Message, Title) {
    if (Type.toLowerCase() == "success") {
        toastr.success(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "error") {
        toastr.error(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "info") {
        toastr.info(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "warning") {
        toastr.warning(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
}

function GetGlobalData(ID, Key, strBU, strLocation) {
    $.ajax({
        url: "/Utility/General/GetGlobalData",
        type: 'POST',
        async: false,
        data: { Key: Key, strBU: strBU, strLocation: strLocation },
        success: function (result) {

        }
    });
}

jQuery.validator.addMethod(
    "multiemail",
     function (value, element) {
         if (this.optional(element)) // return true on optional element 
             return true;
         var emails = value.split(/[;,]+/); // split element by , and ;
         valid = true;
         for (var i in emails) {
             value = emails[i];
             valid = valid &&
                     jQuery.validator.methods.email.call(this, $.trim(value), element);
         }
         return valid;
     },

    jQuery.validator.messages.email
);

function DisableBtn(objectctrl) {
    $(objectctrl).prop("disabled", true);
}

function EnableBtn(objectctrl) {
    $(objectctrl).removeAttr("disabled", true);
}

function SetReadonly(objectctrl) {
    $(objectctrl).prop("readonly", "readonly");
}

function HideControl(objectctrl) {
    $(objectctrl).addClass('hideControl');
}

function RemoveReadonly(objectctrl) {
    $(objectctrl).removeAttr("readonly");
}

$('.set-numeric').keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
// Added by M. Ozair Khatri (90385894) on 24-08-2017
/**
 * Opens a new Tab with the report defined by ReportPath and ParameterData
 * @param {Array[string,string]} ParameterData - data in the format : [{Param:"Param1", Value:"Param1Value"}, {Param:"Param2", Value:"Param2Value1"}, {Param:"Param2", Value:"Param2Value2"} ]
 * @param {String} ReportPath - Server Relative report path : '/Reports/SubFolder/SubFolder2/SampleReport'
 */
function ShowReport(ParameterData, ReportPath, ShowAttachment, downloadFile, format,downloadName) {
    if (typeof (ShowAttachment) == "undefined")
        ShowAttachment = false;

    if (typeof (downloadFile) == "undefined")
        downloadFile = false;
    if (typeof (format) == "undefined")
        format = "pdf";
    if (typeof (downloadName) == "undefined")
        downloadName = "";

    if (downloadFile) {
        $.ajax({
            type: "POST",
            url: WebsiteURL + "/Utility/General/ViewReport",
            dataType: "json",
            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            data: {
                serverRelativeUrlPath: ReportPath,
                ReportParameters: ParameterData,
                ShowAttachment: ShowAttachment,
                format: format
            },
            success: function (result) {
                if (result.errorMessage == "") {
                    //debugger
                    window.open(WebsiteURL + "/Utility/General/DownloadReport?file=" + result.fileName + "&format=" + result.format + "&downloadName=" + downloadName, '_blank');
                }
                else {
                    DisplayNotiMessage("error", result.errorMessage, "Error !");
                }
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: WebsiteURL + "/Utility/General/OpenSSRSReport",
            dataType: "html",
            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            data: {
                serverRelativeUrlPath: ReportPath,
                ReportParameters: ParameterData,
                ShowAttachment: ShowAttachment
            },
            success: function (result) {
                if (result != 'Error') {
                    var x = window.open();
                    if (x == null) {
                        DisplayNotiMessage('Warning', 'Pop-up blocker has interrupted your request, please allow pop-ups to continue', 'Pop-ups blocked!');
                        return;
                    }
                    x.target = '_blank';
                    x.document.write(result);
                }
                else {
                    DisplayNotiMessage("error", "An Error occured while fetching the report.", "Error !");
                }
            }
        });
    }
}

function GetSeamFilteredData(seamNo, Project, QualityProject, tooltip1) {    
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/ApproveSeamList/GetSeamDetails",
        datatype: 'json',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { Project: Project, QualityProject: QualityProject, seamNo: seamNo },
        success: function (data) {         
            if (data.SeamNo != null) {
                GetFilteredData(data.QualityProject, data.Project, data.BU, data.Location, data.SeamNo, "", "", "", "", "", true, "Seam Details");
            }
            else {
                //DisplayNotiMessage("error", "Seam Details not available", "Error");
                bootbox.alert("Seam Details not available");
            }
        },
        error: function () {
        }
    });
}
function GetIPIPendingInspectionDataForSpecialStage(project, qualityproject, bu, location, childPartNo) {

    $.ajax({
        url: WebsiteURL + "/IPI/PartlistOfferInspection/GetPendingInspectionDataForSpecialStage",
        datatype: 'html',
        type: "POST",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { project: project, qualityProject: qualityproject, bu: bu, location: location, partcode: childPartNo },
        success: function (result) {
            bootbox.dialog({
                title: "Pending Inspection",
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        }
    });
}
function GetIPIPendingInspectionDataForPart(project, qualityproject, bu, location, partNo) {

    $.ajax({
        url: WebsiteURL + "/IPI/PartlistOfferInspection/GetPendingInspectionData",
        datatype: 'html',
        type: "POST",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { project: project, qualityProject: qualityproject, bu: bu, location: location, partno: partNo },
        success: function (result) {
            bootbox.dialog({
                title: "Pending Inspection For Part - " + partNo,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        }
    });
}
function GetIPIPendingInspectionDataForSeam(project, qualityproject, bu, location, seamNo) {

    $.ajax({
        url: WebsiteURL + "/IPI/MaintainSeamListOfferInspection/GetPendingInspectionData",
        datatype: 'html',
        type: "POST",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { project: project, qualityProject: qualityproject, bu: bu, location: location, seamno: seamNo },
        success: function (result) {
            bootbox.dialog({
                title: "Pending Inspection For Seam - " + seamNo,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        }
    });
}
function GetIPIPendingInspectionReport(InspectionType, TestType) {
    $.ajax({
        url: WebsiteURL + "/IPI/PartlistOfferInspection/GetPendingInspectionReportPartial",
        dataType: "html",
        type: "GET",
        delay: 250,
        data: { InspectionType: InspectionType, TestType: TestType },
        async: false,
        success: function (result) {
            bootbox.dialog({
                title: "Pending Inspection Report",
                message: result,
                size: 'medium',
                buttons: {
                    'confirm': {
                        label: 'Print',
                        className: 'btn green',
                        callback: function () {
                            if ($("#frnPIReport").valid()) {
                                var url = "/IPI/PendingInspection";
                                var dat = [];
                                //var BU = $("#BU").val(); var lstBU = "";
                                //if (BU != '' && BU != null && BU != undefined) {
                                //    lstBU = BU.join(",");
                                //}
                                //else {
                                //    lstBU = BU;
                                //}
                                //var Project = $("#Project").val(); var lstProject = "";
                                //if (Project != '' && Project != null && Project != undefined) {
                                //    lstProject = Project.join(",");
                                //}
                                //else {
                                //    lstProject = ' ';
                                //    console.log('proj :"' + lstProject + '"');
                                //}
                                //dat.push({ Param: "pBU", Value: lstBU });
                                //dat.push({ Param: "pLocation", Value: $("#Location").val() });
                                //dat.push({ Param: "pInspectionType", Value: InspectionType });
                                //dat.push({ Param: "pTestType", Value: TestType });
                                //dat.push({ Param: "cUser", Value: $("#txtUserName").val() });
                                var InspectionStatus = $("#InspectionStatus").val(); var lstInspectionStatus = "";
                                if (InspectionStatus != '' && InspectionStatus != null && InspectionStatus != undefined) {
                                    lstInspectionStatus = InspectionStatus.join(",");
                                }
                                else {
                                    lstInspectionStatus = " ";
                                }
                                var StageType = $("#StageType").val(); var lstStageType = "";
                                if (StageType != '' && StageType != null && StageType != undefined) {
                                    lstStageType = StageType.join(",");
                                }
                                else {
                                    lstStageType = " ";
                                }
                                dat.push({ Param: "pLocation", Value: $("#Location").val() == "" ? " " : $("#Location").val() });
                                dat.push({ Param: "QualityProject_f", Value: $("#FromQualityProject").val() == "" ? " " : $("#FromQualityProject").val() });
                                dat.push({ Param: "QualityProject_t", Value: $("#ToQualityProject").val() == "" ? " " : $("#ToQualityProject").val() });
                                if (TestType == "SEAM") {
                                    dat.push({ Param: "SeamNo_f", Value: $("#FromSeam").val() == "" ? " " : $("#FromSeam").val() });
                                    dat.push({ Param: "SeamNo_t", Value: $("#ToSeam").val() == "" ? " " : $("#ToSeam").val() });
                                }
                                else {
                                    dat.push({ Param: "SeamNo_f", Value: $("#FromPart").val() == "" ? " " : $("#FromPart").val() });
                                    dat.push({ Param: "SeamNo_t", Value: $("#ToPart").val() == "" ? " " : $("#ToPart").val() });
                                }
                                dat.push({ Param: "StageCode_f", Value: $("#FromStage").val()  == "" ? " " : $("#FromStage").val()});
                                dat.push({ Param: "StageCode_t", Value: $("#ToStage").val() == "" ? " " : $("#ToStage").val() });
                                dat.push({ Param: "Shop_f", Value: $("#FromShop").val()  == "" ? " " :$("#FromShop").val()});
                                dat.push({ Param: "Shop_t", Value: $("#ToShop").val() == "" ? " " : $("#ToShop").val() });
                                dat.push({ Param: "InspectionStatus", Value: lstInspectionStatus });
                                dat.push({ Param: "StageType", Value: lstStageType });
                                dat.push({ Param: "RequestStatus", Value: $("#RequestStatus").val() == "" ? " " :$("#RequestStatus").val() });
                                dat.push({ Param: "pInspectionType", Value: InspectionType });
                                dat.push({ Param: "pTestType", Value: TestType });
                                dat.push({ Param: "cUser", Value: $("#txtUserName").val() == "" ? " " : $("#txtUserName").val() });
                                //console.log(dat);
                                ShowReport(dat, url);
                            }
                            else {
                                HighlightInValidControls();
                                return false;
                            }
                        }
                    },
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        }
    });
}

function GetIPINotAttendedSeamRequestReport(StageType,RequestStatus) {
    $.ajax({
        url: WebsiteURL + "/IPI/SeamListNDEInspection/GetNotAttendedSeamRequestReportPartial",
        dataType: "html",
        type: "GET",
        delay: 250,
        data: { StageType: StageType },
        async: false,
        success: function (result) {
            bootbox.dialog({
                title: 'Not Attended Seam Request Report for ' + StageType,
                message: result,
                size: 'medium',
                buttons: {
                    'confirm': {
                        label: 'Print',
                        className: 'btn green',
                        callback: function () {
                            if ($("#frmNotAttendedPrint").valid()) {
                                //var url = "/IPI/PendingInspection";
                                var url = "/NDE/NOTAttendSeamRequest";
                                var dat = [];
                                var Location = "";
                                if ($("#ddlLocation").val() != null && $("#ddlLocation").val() != undefined)
                                    Location = $("#ddlLocation").val().join(',');
                                dat.push({ Param: "RequestNoFrom", Value: '' });
                                dat.push({ Param: "RequestNoTo", Value: '' });
                                dat.push({ Param: "projectFrom", Value: '' });
                                dat.push({ Param: "projectTo", Value: '' });
                                dat.push({ Param: "StageCodeFrom", Value: '' });
                                dat.push({ Param: "StageCodeTo", Value: '' });
                                dat.push({ Param: "IterationNoFrom", Value: '' });
                                dat.push({ Param: "IterationNoTo", Value: '' });
                                dat.push({ Param: "ShopFrom", Value: '' });
                                dat.push({ Param: "ShopTo", Value: '' });
                                dat.push({ Param: "OfferdateFrom", Value: '' });
                                dat.push({ Param: "offerdateTo", Value: '' });
                                dat.push({ Param: "Location", Value: Location });
                                dat.push({ Param: "StageType", Value: StageType });
                                dat.push({ Param: "RequestStatus", Value: RequestStatus });
                                dat.push({ Param: "SeamNoFrom", Value: '' });
                                dat.push({ Param: "SeamNoTo", Value: '' });
                                dat.push({ Param: "StageSequenceFrom", Value: '' });
                                dat.push({ Param: "StageSequenceTo", Value: '' });
                                dat.push({ Param: "IsLTFPS", Value: 'false' })
                                ShowReport(dat, url);
                            }
                            else {
                                HighlightInValidControls();
                                return false;
                            }
                        }
                    },
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        }
    });
}


function GetFilteredData(qualityProject, project, bu, location, partNo, projDesc, buDesc, locDesc, parentPart, childPart, isFromSeam, Title) {    
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/PartlistOfferInspection/LoadPartlistFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: projDesc,
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: parentPart,
            ChildPartNo: childPart,
            isFromSeam: isFromSeam
        },
        success: function (result) {            
            if (result == "") {
                bootbox.alert("Seam Details not available");
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}
function GetReqFilteredData(Title, qualityProject, project, bu, location, partNo, projDesc, buDesc, locDesc, parentPart, childPart, stageCode, stageDesc, stageSeq, tableID, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/PartlistOfferInspection/LoadFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: projDesc,
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: parentPart,
            ChildPartNo: childPart,
            StageCode: stageCode,
            StageDesc: stageDesc,
            StageSeq: stageSeq,
            tableID: tableID
        },
        success: function (result) {
            if (isFromPopup) {
                $("#divReqDetails").html(result);
                $("#frmReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}

function GetLTFPSFilteredData(LTFPSNo, qualityProject, project, bu, location, partNo, buDesc, locDesc, isFromSeam, Title) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/LTFPS/OfferInspection/LoadLTFPSFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            LTFPSNo: LTFPSNo,
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: "",
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: "",
            ChildPartNo: "",
            isFromSeam: isFromSeam
        },
        success: function (result) {
            bootbox.dialog({
                title: Title,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            }).find("div.modal-dialog").addClass("clsBootBoxWidth");
        },
        error: function () {
        }
    });
}

function GetLTFPSReqFilteredData(Title, qualityProject, project, bu, location, partNo, projDesc, buDesc, locDesc, stageCode, stageDesc, stageSeq, isNDEStage, isFromSeam, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/LTFPS/OfferInspection/LoadLTFPSReqFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: projDesc,
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: "",
            ChildPartNo: "",
            StageCode: stageCode,
            StageDesc: stageDesc,
            StageSeq: stageSeq,
            isNDEStage: isNDEStage,
            isFromSeam: isFromSeam
        },
        success: function (result) {
            if (isFromPopup) {
                $("#divLTFPSReqDetails").html(result);
                $("#frmLTFPSReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}
//created by Nikita for traveller
function PrintTVLReport(projectno, travelerno, travelerrevno, identificationNo, subassemblyno, isGetPDFreq) {
    var url = "/TVL/Traveler";
    var dat = [];
    dat.push({ Param: "Project", Value: projectno });
    dat.push({ Param: "TravelerNo", Value: travelerno });
    dat.push({ Param: "TravelerRevNo", Value: parseInt(travelerrevno) });
    dat.push({ Param: "AssemblyNo", Value: identificationNo });
    dat.push({ Param: "SubAssemblyNo", Value: subassemblyno });
    if (isGetPDFreq) {
        $.ajax({
            url: WebsiteURL + "/tvl/CompleteTraveler/GetPDFReportFilePath",
            type: "GET",

            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            data: { projectno: projectno, travelerno: travelerno, travelerrevno: travelerrevno, identificationNo: identificationNo, subassemblyno: subassemblyno },
            success: function (result) {

                if (result.Key) {
                    DownloadPDF(result.folderpath, result.dataValue)
                    //var win = window.open(result.Value, '_blank');
                    //win.focus();                 
                }

                else {
                    ShowReport(dat, url);
                }


            }
        });
    }
    else { ShowReport(dat, url); }
    // ShowReport(dat, url);
}

function DownloadPDF(path, fileName) {

    $('' +
        '<form action="' + WebsiteURL + '/Utility/FileUpload/DownloadSharePointFile" method="POST">' +
            '<input name="fileName" value="' + fileName + '" />' +
            '<input name="folderpath" value="' + path + '" />' +
            '<input name="Type" value="" />' +
        '</form>' +
    '').appendTo('body').submit().remove();
}
function GetTravelerFilteredData(ProjectNo, Customer, travellerno, revno, identificationno, bu, location, subassemblyno, Title) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/TVL/OfferInspection/LoadOfferFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            ProjectNo: ProjectNo,
            Customer: Customer,
            TravelerNo: travellerno,
            TravelRev: revno,
            IdentificationNo: identificationno,
            BU: bu,
            Location: location,
            SubAssemblyNo: subassemblyno
        },
        success: function (result) {
            bootbox.dialog({
                title: Title,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            }).find("div.modal-dialog").addClass("clsBootBoxWidth");
        },
        error: function () {
        }
    });
}

//created By Nikita For traveller request pop up
function GetTravelerReqFilteredData(ProjectNo, Customer, travellerno, revno, identificationno, bu, location, operationno, activity, subassemblyno, hdrId, Title, tableID, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/TVL/OfferInspection/LoadRequestFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            ProjectNo: ProjectNo,
            Customer: Customer,
            TravelerNo: travellerno,
            TravelRev: revno,
            IdentificationNo: identificationno,
            BU: bu,
            Location: location,
            OperationNo: operationno,
            Activity: activity,
            SubAssemblyNo: subassemblyno,
            HeaderId: hdrId,
            tableID: tableID
        },
        success: function (result) {

            if (isFromPopup) {
                $("#divReqDetails").html(result);
                $("#frmReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}

function GetMIPFilteredData(lineId, Title) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/General/MIPLoadOfferFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            LineId: lineId
        },
        success: function (result) {
            bootbox.dialog({
                title: Title,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            }).find("div.modal-dialog").addClass("clsBootBoxWidth");
        },
        error: function () {
        }
    });
}

//created By Ravi For MIP request pop up
function GetMIPReqFilteredData(lineId, Title, tableID, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/General/MIPLoadRequestFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            LineId: lineId,
            tableID: tableID
        },
        success: function (result) {
            if (isFromPopup) {
                $("#divReqDetails").html(result);
                $("#frmReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}

function ViewTVLRefDocAttachment(lineid, revno) {
    var objFileUploadLine = new FileUploadEnt('dvFileUploadLineID' + lineid, 'FileUploadLineID' + lineid);
    objFileUploadLine.ShowDeleteAction = false;
    objFileUploadLine.ShowAttachmentBtn = false;
    objFileUploadLine.FolderPath = "TVL011/" + lineid + "/R" + revno;
    objFileUploadLine.LoadInPopup = true;
    LoadFileUploadPartialView(objFileUploadLine);
}

function LoadIPIMaintainSamplingPlan(headerid, lineid, revno, isEditable, title) {
    bootbox.hideAll();
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/ChemicalAnalysis/MaintainSamplingPlan",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { headerid: headerid, lineid: lineid, revno: revno, isEditable: isEditable },
        success: function (result) {
            bootbox.dialog({
                title: title,
                message: result,
                size: 'large',
                className: "higherzindex clsBootbox",

            })
        }
    })
}

function LoadIPIMaintainSamplingPlan(headerid, lineid, qProj, Project, bu, loc, qId, qRev, stage, stageseq, isFromQID, isEditable, title) {
    debugger
    bootbox.hideAll();
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/ChemicalAnalysis/MaintainSamplingPlan",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            //headerid: headerid, lineid: lineid, revno: revno, isEditable: isEditable,
            headerid: headerid,
            lineid: lineid,
            qProj: qProj,
            Project: Project,
            bu: bu,
            loc: loc,
            qId: qId,
            qRev: qRev,
            stage: stage,
            stageseq: stageseq,
            isFromQID: isFromQID,
            isEditable: isEditable
        },
        success: function (result) {
            bootbox.dialog({
                title: title,
                message: result,
                size: 'large',
                className: "higherzindex clsBootbox",

            })
        }
    })
}
function GetPDNIdenticalprojectPartial(project, refheaderid) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/PDIN/Maintain/LoadIdenticalProjectPartial",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            project: project,
            refheaderid: refheaderid,
        },
        success: function (result) {
            $("#divPDNIdenticalproject").html(result);
        },
        error: function () {
        }
    });
}

function LoadIPIMaintainChemicalTestDetails(headerid, iteration, role, isEditable, reqid, IsHistory) {
    bootbox.hideAll();
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/ChemicalAnalysis/ChemicalTestDetails",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            headerid: headerid,
            iteration: iteration,
            role: role,
            isEditable: isEditable,
            reqid: reqid,
            isHistory: IsHistory
        },
        success: function (result) {
            bootbox.dialog({
                title: "Maintain Chemical Test Details",
                message: result,
                size: 'large',
                className: "higherzindex clsBootbox",
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            })
        }
    })
}

function LoadHardnessFerritein_TP_ICL_Partial(lineid, qProj, Project, bu, loc, qId, qRev, stage,isEditable, title) {
    bootbox.hideAll();
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/QualityId/GetHardnessFerritePartial",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            //headerid: headerid, lineid: lineid, revno: revno, isEditable: isEditable,
            lineid: lineid,
            isEditable: isEditable,
            qProj: qProj,
            Project: Project,
            bu: bu,
            loc: loc,
            qId: qId,
            qRev: qRev,
            stage: stage,
        },
        success: function (result) {
            bootbox.dialog({
                title: title,
                message: result,
                size: 'medium',
                className: "higherzindex clsBootbox",
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            })
        }
    })
}

function LoadIPIMaintainTestPlanSamplingPlan(headerid, lineid, qProj, Project, bu, loc, qId, qRev, stage, stageseq, isFromTP, isEditable, title) {
    bootbox.hideAll();
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/ChemicalAnalysis/MaintainTestPlanSamplingPlan",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            //headerid: headerid, lineid: lineid, revno: revno, isEditable: isEditable,
            headerid: headerid,
            lineid: lineid,
            qProj: qProj,
            Project: Project,
            bu: bu,
            loc: loc,
            qId: qId,
            qRev: qRev,
            stage: stage,
            stageseq: stageseq,
            isFromTP: isFromTP,
            isEditable: isEditable
        },
        success: function (result) {
            bootbox.dialog({
                title: title,
                message: result,
                size: 'large',
                className: "higherzindex clsBootbox",

            })
        }
    })
}

function LoadTestPlanHardnessFerritePartial(lineid, isEditable, title) {
    bootbox.hideAll();
    var cls = "";
    if (isEditable == "false" || isEditable == "" || isEditable == null) {
        cls = "hide";
    }
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/TestPlan/GetTestPlanHardnessFerritePartial",
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { lineid: parseInt(lineid), isEditable: isEditable },
        success: function (result) {
            bootbox.dialog({
                title: title,
                message: result,
                size: 'medium',
                className: "higherzindex clsBootbox",
                buttons: {
                    Maintain: {
                        Label: "Maintain",
                        className: 'btn green ' + cls,
                        callback: function () {
                            if ($("#frmRequiredValues").valid()) {
                                var frmLineValues = $("#frmRequiredValues").serializeArray();
                                var formdata = new FormData();
                                if (frmLineValues != null && frmLineValues != undefined) {
                                    $.each(frmLineValues, function (key, input) {
                                        formdata.append(input.name, input.value);
                                    });
                                }

                                $.ajax({
                                    url: WebsiteURL + "/IPI/TestPlan/SaveHardness",
                                    type: "POST",
                                    data: formdata,
                                    beforeSend: function () {
                                        showLoading(true);
                                    },
                                    complete: function () {
                                        showLoading(false);
                                    },
                                    contentType: false,
                                    processData: false,
                                    success: function (res) {
                                        if (res.Key) {
                                            $('#tblTestPlanDetails').DataTable().ajax.reload();
                                            DisplayNotiMessage("success", res.Value, "Success");
                                        }
                                        else {
                                            DisplayNotiMessage("error", res.Value, "Error");
                                        }
                                        if (res.Remarks != null && res.Remarks.length > 0)
                                            DisplayNotiMessage("info", res.Remarks, "Info");
                                    },
                                    error: function (data) { }
                                })
                            }
                            else {
                                HighlightInValidControls();
                                return false;
                            }
                        }
                    },
                    Cancel: {
                        label: 'Close',
                        className: 'btn red',
                    }
                }
            })
        }
    })
}