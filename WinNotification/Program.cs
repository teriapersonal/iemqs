﻿using System;
using System.IO;
using System.Media;
using System.Reflection;
using System.Windows.Forms;
using WinNotification;

namespace WinNotification
{
    public class Program
    {
        public static Timer timer1;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitTimer();
            ShowNotification();
            Application.Run();
        }

        /// <summary>
        /// Added By Dharmesh Vasani
        /// 09/11/2017
        /// For Set Notification Interval
        /// </summary>
        public static void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 15000; // in miliseconds
            timer1.Start();
        }

        public static void timer1_Tick(object sender, EventArgs e)
        {
            ShowNotification();
        }

        /// <summary>
        /// Added By Dharmesh Vasani
        /// 09/11/2017
        /// For Toast Notification on Windows
        /// </summary>

        public static void ShowNotification()
        {
            int duration;
            int.TryParse("10", out duration);
            if (duration <= 0)
            {
                duration = -1;
            }

            var animationMethod = FormAnimator.AnimationMethod.Slide;
            foreach (FormAnimator.AnimationMethod method in Enum.GetValues(typeof(FormAnimator.AnimationMethod)))
            {
                if (string.Equals(method.ToString(), "Slide"))
                {
                    animationMethod = method;
                    break;
                }
            }

            var animationDirection = FormAnimator.AnimationDirection.Up;
            foreach (FormAnimator.AnimationDirection direction in Enum.GetValues(typeof(FormAnimator.AnimationDirection)))
            {
                if (string.Equals(direction.ToString(), "Up"))
                {
                    animationDirection = direction;
                    break;
                }
            }
            ServiceReference.IEMQSServiceClient objServiceReference = new ServiceReference.IEMQSServiceClient();

            string userName = System.Environment.UserName;

            string Message = objServiceReference.GetWinNotification(userName);
            if (!string.IsNullOrWhiteSpace(Message))
            {
                var toastNotification = new Notification("IEMQS", Message, duration, animationMethod, animationDirection);
                PlayNotificationSound("normal");
                toastNotification.Show();
            }
        }

        /// <summary>
        /// Added By Dharmesh Vasani
        /// 09/11/2017
        /// For Play Sound when Notification Display
        /// </summary>
        /// <param name="sound"></param>

        public static void PlayNotificationSound(string sound)
        {
            string cwd = System.IO.Directory.GetCurrentDirectory();
            if (cwd.EndsWith("\\bin\\Debug"))
            {
                cwd = cwd.Replace("\\bin\\Debug", "\\Sounds");
            }
            var soundFile = Path.Combine(cwd, sound + ".wav");
            using (var player = new System.Media.SoundPlayer(soundFile))
            {
                player.Play();
            }
        }
        
    }
}
