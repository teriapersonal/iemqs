﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class FolderStructureModel
    {
        public Int64? FolderId { get; set; }
        public Int64? BUId { get; set; }
        public Int64? ParentFolderId { get; set; }

        [Required(ErrorMessage = "Folder Name is required")]
        [Display(Name = "Folder Name")]
        [Remote("IsAlreadyExist", "Parameter", AdditionalFields = "ParentFolderId,BUId", HttpMethod = "POST", ErrorMessage = "Folder Name Already Exist.")]
        public string FolderName { get; set; }

        [Required(ErrorMessage = "Folder Order No is required")]
        public double? FolderOrderNo { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<CategoryActionModel> ListCategory { get; set; }
        public List<CategoryActionModel> ListAction{ get; set; }
        public string CategoryIds { get; set; }

        public string ActionIds { get; set; }
        public string FunctionIds { get; set; }

        [Required(ErrorMessage = "Please select Functions")]
        [Display(Name = "Functions")]
        public List<string> RoleGroupList { get; set; }
    }
}
