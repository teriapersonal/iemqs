﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES002 Table Model
    /// Ravi Patel 12-11-2019
    /// </summary>
    public class CategoryActionModel
    {
        public Int64? MappingId { get; set; }
        public int? TypeId { get; set; }
        public Int64? ParentId { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public bool IsTarget { get; set; }
        public bool IsChecked { get; set; }
    }
}
