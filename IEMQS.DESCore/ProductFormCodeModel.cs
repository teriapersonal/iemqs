﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES028 Table Model
    /// Ravi Patel 25-11-2019
    /// </summary>
    public class ProductFormCodeModel
    {
        public Int64? ProductFormCodeId { get; set; }
        public Int64? BUId { get; set; }
        public string ProductForm { get; set; }
        public string SubProductForm { get; set; }
        public string ProductFormCode { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string BOMReportSize { get; set; }
        public string DrawingBOMSize { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<SelectListItem> MaterialList { get; set; }
        public List<SelectListItem> TypeList { get; set; }

        public bool IsITRoleGroup { get; set; }
    }
}
