﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class ARMCodeModal
    {
        public Int64? ARMCodeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }
    }
    public class ARMModel : ARMCodeModal
    {

        public string Arms { get; set; }
        public string Vrsn { get; set; }
        public string Fnam { get; set; }
        public string Desc { get; set; }
        public Int64? ArmCode { get; set; }
        public string CurrentLocationIp { get; set; }
        public string CurrentLocationSavedPath { get; set; }
        public bool IsITRoleGroup { get; set; }

    }

    public class ARMListModel
    {

        public string Arms { get; set; }
        public string Vrsn { get; set; }
        public string Desc { get; set; }

    }


}
