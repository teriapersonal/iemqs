﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class DCRModel
    {
        //[Required(ErrorMessage = "DCR Name is Required")]
        //public string DCRName { get; set; }
        public string DCRNameDisplay { get; set; }
        [Required(ErrorMessage = "DCR Name is Required")]
        public int? DCRNameOn { get; set; }
        public string Project { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }
        public long? FolderId { get; set; }
        public Int64? DCRSupportedDocId { get; set; }
        public string CurrentLocationIp { get; set; }

        public Int64? DCRId { get; set; }
        public string DCRNo { get; set; }

        public string DCRDescription { get; set; }

        [Required(ErrorMessage = "Category of Change is Required")]
        public Int64? CategoryOfChange { get; set; }

        [Required(ErrorMessage = "Responsible Design Engineer is Required")]
        public string ResponsibleDesignEngineer { get; set; }

        [Required(ErrorMessage = "Responsible Senior Design Engineer is Required")]
        public string ResponsibleSeniorDesignEngineer { get; set; }

        [AllowHtml]
        public string ReasonForChange { get; set; }

        [Required(ErrorMessage = "change from is Required")]
        [AllowHtml]
        public string ChangeFrom { get; set; }

        [Required(ErrorMessage = "change to is Required")]
        [AllowHtml]
        public string ChangeTo { get; set; }

        [Required(ErrorMessage = "Nature of Change is Required")]
        public Int64? NatureOfChange { get; set; }
        public string StatusOfTheItems { get; set; }

        public string Originator { get; set; }

        public string Owner { get; set; }
        public DateTime? OriginatedDate { get; set; }
        public string OriginatedDatestr { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public string ModifiedDatestr { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Page { get; set; }
        public string Remark { get; set; }
        public Int64? DCRReviewId { get; set; }
        public Int64? DCREvaluatId { get; set; }

        public bool IsReviewed { get; set; }
        public bool IsEvaluated { get; set; }
        public bool IsReleased { get; set; }

        public bool IsAllPartialSelect { get; set; }
        public Int64? BUId { get; set; }

        public List<DCREvaluateCommentModel> DepartmentGroupList { get; set; }
        public DCREvaluateCommentModel DCREvaluateCommentModel { get; set; }
        public DCRReviewModel DCRReviewModel { get; set; }
        public DCREvaluateModel DCREvaluateModel { get; set; }
        public Nullable<int> ReleasedStepId { get; set; }

        [AllowHtml]
        public string ReleasedRemark { get; set; }

        public string InCorporateBy { get; set; }
        public DateTime? InCorporateOn { get; set; }

        public DCRReleaseModel DCRReleaseModel { get; set; }

        public string ResponsibleDesignEngineerText { get; set; }
        public string ResponsibleSeniorDesignEngineerText { get; set; }
        public string CurrentUser { get; set; }

        public string PolicyStepForView { get; set; }
        public string FilePath { get; set; }

        public string Identical { get; set; }
    }
    public class DCRDocumentMappingModel
    {
        public Int64? DCRId { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentFormat { get; set; }
        public string DocumentPath { get; set; }
        public string DCRSupportedDocId { get; set; }
        public int FileLocation { get; set; }
    }

    public class DCRDocumentModel
    {
        public long DCRAffectedDocumentId { get; set; }
        public Int64? DCRId { get; set; }
        public string GeneralRemarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<DCRDocumentListModel> dCRDocumentList { get; set; }
    }

    public class DCRPartModel
    {
        public Int64? DCRAffectedPartsId { get; set; }
        public Int64? DCRId { get; set; }
        public Int64? ItemId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<DCRPartListModel> dCRPartList { get; set; }
    }
    public class DCRDocumentListModel
    {
        public Int64? JEPDocumentDetailsId { get; set; }
    }
    public class DCRPartListModel
    {
        public Int64? ItemId { get; set; }
    }

    public class DCRReviewModel
    {
        public long DCRReviewId { get; set; }
        public Nullable<long> DCRId { get; set; }

        [AllowHtml]
        public string Remark { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Status { get; set; }
        public bool IsPolicyStep { get; set; }
    }

    public class DCREvaluateModel
    {
        public long DCREvaluateId { get; set; }
        public Nullable<long> DCRId { get; set; }

        [AllowHtml]
        public string Remark { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Status { get; set; }
    }
    public class DCREvaluateCommentModel
    {
        public long DCREvaluateCommentId { get; set; }
        public Nullable<long> DCREvaluateId { get; set; }
        public Nullable<long> DCRId { get; set; }
        public string DeptId { get; set; }

        [Required(ErrorMessage = "Remark is Required")]
        [AllowHtml]
        public string Remark { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public List<string> PSNoList { get; set; }
        public List<SelectListItem> FunctionList { get; set; }
        public List<SelectListItem> PSNumberList { get; set; }
        public bool IsDelete { get; set; }
    }

    public class DCRCommentPersonModel
    {
        public long DCRCommentPersonId { get; set; }
        public Nullable<long> DCREvaluateCommentId { get; set; }
        public string PSNumber { get; set; }
    }

    public class DCRLifeCycleModel
    {
        public long DCRLifeCycleId { get; set; }
        public Nullable<long> DCRId { get; set; }
        public Nullable<long> PolicyID { get; set; }
        public string PolicyStep { get; set; }
        public Nullable<int> PolicyOrderID { get; set; }
        public string ColorCode { get; set; }
        public Nullable<int> DesignationLevels { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }

    public class DCRReleaseModel
    {
        public long DCRReleaseId { get; set; }
        public Nullable<long> DCRId { get; set; }

        [AllowHtml]
        public string Remark { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Status { get; set; }
    }
}
