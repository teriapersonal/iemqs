﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class RecentProjectModel
    {
        public Int64? RecentId { get; set; }
        public string Project { get; set; }
        public string PSNumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }

    }
}
