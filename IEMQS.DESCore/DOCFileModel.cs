﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class DOCFileModel
    {
        public Int64? DocumentMappingId { get; set; }
        public Int64? DocumentId { get; set; }
        public bool IsMainDocument { get; set; }
        public string MainDocumentPath { get; set; }

        //public string Document_name { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string DocumentName { get; set; }
        public string FileFormate { get; set; }
        public bool IsRevisionForDocMapping { get; set; }

        public string Remark { get; set; }
        public int? FileVersion { get; set; }
        public bool IsLatest { get; set; }
        public string PreFixVesion { get; set; }

        public bool IsFileFromAnotherDropzone { get; set; }

        public string VersionGUID { get; set; }

        public int FileLocation { get; set; }
        public string CurrentLocationIp { get; set; }
        public Int64? OldDocIDForRevision { get; set; }
        public List<string> newPathAfterRevise { get; set; }

        public List<FileData> FileFormatData { get; set; }
    }

    public class FileData
    {
        public string filename { get; set; }
        public string originalname { get; set; }
        public string destination { get; set; }
    }
    public class CheckeINOUT : DOCFileModel
    {
        public Int64? CheckInCheckOutId { get; set; }
        public string CheckedBy { get; set; }
        public string CheckedIn_Remark { get; set; }
        public string CheckedOut_Remark { get; set; }
        public DateTime? CheckInOn { get; set; }
        public DateTime? CheckOutOn { get; set; }
        public bool CheckInStatus { get; set; }
        public bool CheckOutStatus { get; set; }

    }

    public class DocumentHistory
    {
        public Int64? DocumentHistoryId { get; set; }
        public Int64? DocumentId { get; set; }
        public Int64? DocumentMappingId { get; set; }
        public Int64? DocumentLifeCycleId { get; set; }
        public Int64? CheckInCheckOutId { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
    }
}
