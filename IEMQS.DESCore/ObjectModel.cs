﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// Created by: Niki
    /// Created on: 12th Nov 2019
    /// Note: DES003
    /// </summary>
    public class ObjectModel
    {
        public Int64? ObjectId { get; set; }
        public string ObjectName { get; set; }
        public bool? ApplyBU { get; set; }
        public Int64? BUId { get; set; }
        public string BU { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string BUIds { get; set; }
        public List<Int64> BUIdList { get; set; }
        public string CategoryIds { get; set; }
        public List<Int64> CategoryIdList { get; set; }
        public string ActionIds { get; set; }
        public List<Int64> ActionIdList { get; set; }
    }
}
