﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES030 Table Model
    /// Ravi Patel 26-11-2019
    /// </summary>
    public class GlobalJEPTemplateModel
    {
        public Int64? JEPTemplateId { get; set; }
        public string Project { get; set; }

        [Remote("CheckTemplateNameAlreadyExist", "JEPTemplate", AdditionalFields = "JEPTemplateId", HttpMethod = "POST", ErrorMessage = "  Template Name already exist.")]        
        [Required(ErrorMessage = " Template Name is required")]
        public string TemplateName { get; set; }

        [Required(ErrorMessage = " BU-PBU is required")]
        public Int64? BUId { get; set; }

        [Required(ErrorMessage = " Product is required")]
        public Int64? ProductId { get; set; }
        public string Product { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<JEPTemplateDocumentModel> JEPTemplateDocList { get; set; }        
    }

    /// <summary>
    /// DES031 Table Model
    /// Ravi Patel 26-11-2019
    /// </summary>
    public class JEPTemplateDocumentModel
    {
        public Int64? JEPTemplateDocumentId { get; set; }
        public Int64? JEPTemplateId { get; set; }
        
        [Required(ErrorMessage = " Document Number is required")]
        public string DocumentNo { get; set; }
        
        [Required(ErrorMessage = " Document Title is required")]
        public string DocumentTitle { get; set; }        
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public bool IsDelete { get; set; }
        public int? OrderNo { get; set; }
    }
}
