﻿using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using IEMQS.DESCore.Data;

namespace IEMQS.DESCore
{

    public class CommonModel
    {
        public const string DOCApprove = "{0} has create DOC {1} and promote it to Approve State";  //{0} person_name,{1} doc_no
        public const string DOCCreate = "{0} has return DOC {1} promote from Create State";         //{0} person_name,{1} doc_no
        public const string DOCApproved = "{0} promote DOC {1} to Approved";                        //{0} person_name,{1} doc_no
        public const string DOCLifeCycleNextSteps = "{0} promote DOC {1} to {2} State";             //{0} person_name,{1} doc_no {3} Policy Step

        public const string JEPApprove = "{0} has create JEP {1} and promote it to Approve State";  //{0} person_name,{1} jep_no
        public const string JEPCreate = "{0} has  return JEP {1} promote from  Create State";       //{0} person_name,{1} jep_no
        public const string JEPApproved = "{0} promote JEP {1} to Approved";                        //{0} person_name,{1} jep_no
        public const string JEPLifeCycleNextSteps = "{0} promote JEP {1} to {2} State";             //{0} person_name,{1} jep_no {3} Policy Step

        public const string DINApprove = "{0} has create DIN {1} and promote it to Approve State";  //{0} person_name,{1} din_no 
        public const string DINCreate = "{0} has  return DIN {1} promote from  Create State";       //{0} person_name,{1} din_no
        public const string DINApproved = "{0} promote DIN {1} to Acknowledge";                     //{0} person_name,{1} din_no

        //public const string JigfixCreate = "{person_name} has create Jigfix and promote to Review state";
        //public const string JigfixReview = "{person_name} promote Jigfix to Review";

        public const string PartApprove = "{0} has create Part {1} and promote it to Approve State";
        public const string PartCreate = "{0} has return Part {1} promote from Create State";
        public const string PartApproved = "{0} promote Part {1} to Approved";
        public const string PartLifeCycleNextSteps = "{0} promote Part {1} to {2} State";

        //public const string JEP1 = "When Customer return  document Customer has written Document {doc_no} with notes";
        //public const string JEP2 = "If any document is returned with commented/Rejected status, intimation shall be provided to respective department";
        //public const string JEP3 = "notification to respective department for customer comment";

        //public const string JEPCustomerFeedback = "Respective department notification Customer has written Document {document_no} with notes";

    }

    public class CommonUseModel
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string Other { get; set; }

    }

    public class GetCommonIPConfig
    {
        public string Hazira = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_NODE_PATH");//   //"https://10.7.66.185:443";
        public string Powai = ConfigurationValues.GetConfigvalues("FCS_POWAI_NODE_PATH");// "https://172.16.7.242:443";
        public string Ranoli = ConfigurationValues.GetConfigvalues("FCS_RANOLI_NODE_PATH");// "https://192.168.27.9";
    }

    public class GetCommonPathConfig
    {
        public string Hazira = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");//   //"https://10.7.66.185:443";
        public string Powai = ConfigurationValues.GetConfigvalues("FCS_POWAI_FILE_PATH");// "https://172.16.7.242:443";
        public string Ranoli = ConfigurationValues.GetConfigvalues("FCS_RANOLI_FILE_PATH");// "https://192.168.27.9";
    }


    public enum MenuType
    {
        Category = 1,
        Action = 2
    }

    public enum ObjectName
    {
        DIN,
        DOC = 1,
        Part,
        Jigfix,
        Plate_Part,
        Clad_Part,
        JEP,
        DCR
    }

    public class LookupModel
    {
        public Int64? Id { get; set; }
        public string Lookup { get; set; }

        public string Description { get; set; }
    }

    public class IpLocation
    {
        public string Ip { get; set; }
        public int Location { get; set; }
        public string File_Path { get; set; }

    }

    public enum ObjectStatus
    {
        Draft = 0,
        Created = 1,
        InProcess = 2,
        Completed = 3,
        Rejected = 4,
        Release = 5,
        Approve = 6,
        Cancelled = 7
    }

    public enum DINStatus
    {
        Added = 1,
        Revised = 2,
        CarryForward = 3,
        NotReleased = 4
    }
    public enum NotificationObject
    {
        Create = 1,
        LifeCycle = 2,
        Return = 3,
        Acknowledge = 4,
        LastStep=5,
        CustomerFeedBack=6,
        AddDocument=7,
        CustomerFBApCode=8
    }


    public enum OrderPolicy
    {
        Customised = 1,
        Standard = 2
    }

    public enum ItemMakeBuy
    {
        Buy = 1,
        Make = 2
    }

    public enum ItemKeyType
    {
        AutoGenerate = 1,
        AutoGenerate_FindNo = 2,
        UserDefined = 3
    }

    public enum PartAction
    {
        Create = 1,
        Update = 2,
        Revise = 3
    }

    public enum ItemType
    {
        Part = 1,
        Plate_Part = 2,
        Clad_Part = 3,
        Jigfix = 4
    }

    public enum ActionforPCRelation
    {
        Add = 1,
        Update = 2,
        Remove = 3
    }

    public enum YesOrNo
    {
        Yes = 1,
        No = 2
    }

    public enum LookupType
    {
        DOCType = 1,
        ApprovalCode,
        CategoryOfChanges,
        NatureOfChanges,
        ApplicableLocalRegulation,
        DesignGroup,
        Report,
        PartFix
    }

    #region DCR Yash Shah
    public enum ReviewOrEvaluate
    {
        Accepted,
        Rejected,
        Review,
        Evaluate,
        Return,
        Approval
    }
    #endregion

    public enum LocationType
    {
        Hazira = 1,
        Powai = 2,
        Ranoli = 3
    }

    public enum DOCType
    {
        /// <summary>
        /// DOCType come from the DES026 enum value is same as table PK
        /// </summary>
        Manufacturing_Drawing = 1,
        Procurement_Drawing = 2,
        FEA_Report = 3,
        ARM = 4,
        External_Agency_Drawing = 5,
        External_Agency_Procedure = 6,
        External_Agency_Calculation = 7,
        CRS_BOM = 8,
    }

    public enum UOM
    {
        m = 1,
        m2 = 2,
        kg = 3
    }

    public enum DCRReleasedSteps
    {
        Step1 = 1,
        Step2,
        Step3,
        Step4
    }

    public enum DCRLifeCyclePolicyStep
    {
        Create = 1,
        Submit = 2,
        Evaluate = 3,
        Review = 4,
        Release = 5,
        InCorporate = 6,
        Complete = 7,
        ReviseDCR = 8,
        RevisePart = 9,
        ReviseDoc = 10,
    }

    public enum FolderObjectName
    {
        Part = 1,
        JEP,
        DOC,
        DIN,
        DCR
    }

    public enum CodeStamp
    {
        NO,
        ASME,
        ASME_Nuclear
    }
    public class ConfigurationValues
    {
        public static string GetConfigvalues(string ConfigName)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    var HaziraNodePath = dbContext.DES104.Where(c => c.ConfigName == ConfigName).FirstOrDefault();
                    return HaziraNodePath.ConfigValues;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

}
