﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{

    public class UserIPConfigModel
    {
        //public int Location { get; set; }
        //public string LocationName { get; set; }
        //public string UserIP { get; set; }
        //public string FCSUrl { get; set; }


        public Int64 IPRangeId { get; set; }
        public string VlenName { get; set; }
        public string IP_SubnetMask { get; set; }
        public string IPStart { get; set; }
        public string IPEnd { get; set; }
        public int? Location { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

    }
}
