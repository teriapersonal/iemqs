﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class NotificationModel
    {
        public int Id { get; set; }

        public string NotificationMsg { get; set; }

        public string UserRole { get; set; }

        public long PSNo { get; set; }

        public string NotificationType { get; set; }

        public bool IsRead { get; set; }

        public string RedirectionPath { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsForDES { get; set; }
    }
}
