﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES032 Table Model
    /// Ravi Patel 04-12-2019
    /// </summary>
    
    public class FunctionRoleModel
    {
        public Int64? FunctionRoleMappingId { get; set; }
        public string FunctionId { get; set; }
        public int? RoleId { get; set; }
    }
}
