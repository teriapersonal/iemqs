﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
   public class MaterialModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string ProductForm { get; set; }
        public string ItemGroup { get; set; }
    }
}
