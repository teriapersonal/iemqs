﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class DOCModel
    {
        public List<DOCFileModel> ListDOCFileModel { get; set; }
        public Int64? DocumentId { get; set; }

        public string Project { get; set; }

        [Required(ErrorMessage = "Document Type is required")]
        public Int64? DocumentTypeId { get; set; }

        public string DocumentType { get; set; }
        [Remote("CheckDocumentNo", "DOC", AdditionalFields = "DocumentId,Project,DocumentRevision", HttpMethod = "POST", ErrorMessage = "Document No already exist.")]
        [Required(ErrorMessage = "Document No is required")]
        [StringLength(200, ErrorMessage = "length can't be more than 200")]
        public string DocumentNo { get; set; }

        [Required(ErrorMessage = "JEP Document is required")]
        public Int64? JEPDocumentDetailsId { get; set; }

        [Required(ErrorMessage = "Document Title is required")]
        [StringLength(500, ErrorMessage = "length can't be more than 500")]
        public string DocumentTitle { get; set; }

        [Required(ErrorMessage = "Document Revision is required")]
        public int? DocumentRevision { get; set; }
        public string DocumentRevisionStr { get; set; }
        [StringLength(50, ErrorMessage = "length can't be more than 50")]

        public string ClientDocumentNumber { get; set; }


        [Required(ErrorMessage = "Milestone Document is required")]
        public bool MilestoneDocument { get; set; }

        public Int64? PolicyId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Status { get; set; }

        public int Location { get; set; }

        public bool IsJEP { get; set; }

        public Int64? BUId { get; set; }

        public List<ObjectPolicyStepsModel> PolicySteps { get; set; }


        public bool IsRevision { get; set; }

        public Int64? FolderId { get; set; }

        public string CurrentLocationIp { get; set; }
        public string AddToLocationIp { get; set; }

        public bool IsEdit { get; set; }

        public string Department { get; set; }
        public string RoleGroup { get; set; }

        [StringLength(20, ErrorMessage = "length can't be more than 20")]
        public string ClientVersion { get; set; }

        public bool IsLatestRevision { get; set; }

        public string GetCurrentUser { get; set; }

        public string CheckOutFilename { get; set; }

        public bool CheckOutPath { get; set; }

        public string CheckOutGetExtention { get; set; }

        public string CheckInDocumentPath { get; set; }

        public string ReturnNotes { get; set; }
        public string Roles { get; set; }

        public List<string> NewDocumentPathForRevison { get; set; }

        public Int64? OldDocIDForRevision { get; set; }

        public string DocumentNoToGetInHidden { get; set; }

        public bool IsDOCCheckIn { get; set; }
        public string CurrentPsNo { get; set; }

        public bool IsProjectRights { get; set; }
        public bool IsDocDownloadRights { get; set; }

        public string FilePath { get; set; }

    }

    public class GetListOfDocumentId
    {
        public List<Int64?> DocumentId { get; set; }

        public string BtnText { get; set; }
    }
}

