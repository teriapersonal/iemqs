﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class ItemGroupUOMModel
    {
        public string t_cuni { get; set; }
        public int t_kitm { get; set; }
        public string t_citg { get; set; }
        public string t_csel { get; set; }
        public string t_csig { get; set; }
        public string t_cpcl { get; set; }
        public string t_cpln { get; set; }
        public string t_deca { get; set; }

        public bool Selected { get; set; }
    }
    public class ItemMatrialModel
    {
        public double? Density { get; set; }
        public double? CladSpecificGravity1 { get; set; }
        public List<ItemGroupUOMModel> ItemGroupUOM { get; set; }
    }
}
