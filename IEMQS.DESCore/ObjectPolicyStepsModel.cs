﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{

    public class PartPolicyStepsModel
    {
        public Int64? PartLifeCycleId { get; set; }
        public Int64? PolicyId { get; set; }
        public string PolicyStep { get; set; }
        public int? PolicyOrderID { get; set; }
        public bool IsCompleted { get; set; }
        public string PolicyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Int64? PolicyStepID { get; set; }
        public string ColorCode { get; set; }
        public int? DesignationLevels { get; set; }
        public string EditedBy { get; set; }
        public DateTime EditedOn { get; set; }
    }
    public class ObjectPolicyStepsModel
    {
        public Int64? DocumentLifeCycleId { get; set; }
        public Int64? PolicyId { get; set; }
        public string PolicyStep { get; set; }
        public int? PolicyOrderID { get; set; }
        public bool IsCompleted { get; set; }
        public string PolicyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Int64? PolicyStepID { get; set; }
        public string ColorCode { get; set; }
        public int? DesignationLevels { get; set; }
        public string EditedBy { get; set; }
        public DateTime EditedOn { get; set; }
    }

    public class JEPPolicyStepsModel
    {
        public Int64? JEPLifeCycleId { get; set; }
        public Int64? PolicyId { get; set; }
        public string PolicyStep { get; set; }
        public int? PolicyOrderID { get; set; }
        public bool IsCompleted { get; set; }
        public string PolicyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Int64? PolicyStepID { get; set; }
        public string ColorCode { get; set; }
        public int? DesignationLevels { get; set; }
        public string EditedBy { get; set; }
        public DateTime EditedOn { get; set; }
    }

    public class DINPolicyStepsModel
    {
        public Int64? DINLifeCycleId { get; set; }
        public Int64? PolicyId { get; set; }
        public string PolicyStep { get; set; }
        public int? PolicyOrderID { get; set; }
        public bool IsCompleted { get; set; }
        public string PolicyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Int64? PolicyStepID { get; set; }
        public string ColorCode { get; set; }
        public int? DesignationLevels { get; set; }
        public string EditedBy { get; set; }
        public DateTime EditedOn { get; set; }
    }

    public class DCRPolicyStepsModel
    {
        public Int64? DCRLifeCycleId { get; set; }
        public Int64? PolicyId { get; set; }
        public string PolicyStep { get; set; }
        public int? PolicyOrderID { get; set; }
        public bool IsCompleted { get; set; }
        public string PolicyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Int64? PolicyStepID { get; set; }
        public string ColorCode { get; set; }
        public int? DesignationLevels { get; set; }
        public string EditedBy { get; set; }
        public DateTime EditedOn { get; set; }
    }
}
