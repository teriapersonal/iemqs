//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_ROC_HISTORY_Result
    {
        public long ROCCommentsHistoryId { get; set; }
        public Nullable<long> ROCId { get; set; }
        public Nullable<long> ROCCommentedDocId { get; set; }
        public Nullable<long> ROCCommentsAttachId { get; set; }
        public string HistoryDescription { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public Nullable<int> TableCount { get; set; }
    }
}
