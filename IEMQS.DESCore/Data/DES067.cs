//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES067
    {
        public long BOMId { get; set; }
        public long ItemId { get; set; }
        public long ParentItemId { get; set; }
        public Nullable<long> DRGNoDocumentId { get; set; }
        public Nullable<int> FindNumber { get; set; }
        public string ExtFindNumber { get; set; }
        public string FindNumberDescription { get; set; }
        public string ActionForPCRelation { get; set; }
        public Nullable<double> Weight { get; set; }
        public Nullable<double> JobQty { get; set; }
        public Nullable<double> CommissioningSpareQty { get; set; }
        public Nullable<double> MandatorySpareQty { get; set; }
        public Nullable<double> OperationSpareQty { get; set; }
        public Nullable<double> ExtraQty { get; set; }
        public string Remarks { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<double> Length { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<int> NumberOfPieces { get; set; }
        public string BomReportSize { get; set; }
        public string DrawingBomSize { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string HdnBomReportSize { get; set; }
        public string HdnDrawingBomSize { get; set; }
    
        public virtual DES066 DES066 { get; set; }
        public virtual DES066 DES0661 { get; set; }
        public virtual DES059 DES059 { get; set; }
    }
}
