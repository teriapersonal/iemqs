//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_SOURCE_TO_DEST_COPYPART_CHILD_ITEM_Result
    {
        public Nullable<long> SourceItemId { get; set; }
        public Nullable<long> DestinationItemId { get; set; }
        public Nullable<long> BOMId { get; set; }
        public string SourceItemKey { get; set; }
        public string DestinationItemKey { get; set; }
        public string ErrorMsg { get; set; }
    }
}
