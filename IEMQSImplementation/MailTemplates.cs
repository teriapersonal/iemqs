﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation
{
    public static class MailTemplates
    {
        public static class IMB
        {
            public const string SentforApproval = "IMB-Sent For Approval";
            //public const string Approve = "IMB-Approve";
            //public const string Return = "IMB-Return";
        }
        public static class QCP
        {
            public const string SentforApproval = "QCP-Sent For Approval";
            //public const string Approve = "QCP-Approve";
            //public const string Return = "QCP-Return";
        }
        public static class PTR
        {
            public const string SentforApproval = "PTR-Sent For Approval";
            public const string Return = "PTR-Return";
            public const string Close = "PTR-Close";
            public const string LabId = "PTR-LabId";
        }
        public static class PAM
        {
            public const string BUHeadApproval = "PAM-Request BU Head Approval";
            public const string Return = "PAM-Return";
            public const string Close = "PAM-Close";
            public const string Rejection = "PAM-BU Head Rejection";
            public const string SentforApproval = "PAM-Send for Approval";
            public const string Approval = "PAM-Approved";
            public const string PamReturn = "PAM-Returned by PMG";
        }
        public static class FDMS
        {
            public const string PromoteFDMS = "FDMS-Promote";
            public const string DemoteFDMS = "FDMS-Demote";
            public const string CompleteFDMS = "FDMS-Complete";
            public const string LineApprove = "FDMS-LineApprove";
            public const string LineReject = "FDMS-LineReject";
            public const string AttachedDocFDMS = "FDMS-Email For FDR Attachment";
        }
        public static class WDIN
        {
            public const string ReleasedWDN = "WDIN-Released";
        }
        public static class CFAR
        {
            public const string PromoteCFAR = "CFAR- Promote";
            public const string DemoteCFAR = "CFAR- Demote";
            public const string ReleaseCFAR = "CFAR- Release";
            public const string PendingCFAR = "CFAR-Pending-Report";
        }

        public static class ICS
        {
            public const string ICSReport = "ICS-Report Email";
        }
        public static class WMI
        {
            public const string WMIApproval= "WMI Approval";
        }
        public static class PSC
        {
            public const string PSCReport = "PSC-Report Email";
        }
        public static class PSCNC
        {
            public const string PSCReport = "PSCNC-Report Email";
        }
        public static class BAL
        {
            public const string BALReport = "BAL-Report Email";
        }

        public static class IMS
        {
            public const string RecallNotification = "IMS-RecallNotification";
        }

        public static class IPI
        {
            public const string RepairDesignationWarning = "Repair Designation-Warning";
            public const string OffertoTPI = "TPI Report Mail";
        }
        public static class CSR
        {
            public const string CSRSentforApproval = "CSR MOM - Sent for Approval";
            public const string CSRApprovedbyDE = "CSR MOM - Approve by Design Lead";
            public const string CSRReturnedbyDE = "CSR MOM - Return by Design Lead";
            public const string CSRApprovedbyPMG = "CSR MOM - Approve by PMG";
            public const string CSRApprovedbyPMGGroup = "CSR MOM - Approve by PMG Group";
            public const string CSRReturnedbyPMG = "CSR MOM - Return by PMG";
        }

        public static class NCR
        {
            public const string NCRCreation = "NCR- Create";
            public const string NCRStageWiseEmail = "NCR- StageWiseEmail";
            public const string NCRClosed = "NCR - Closed";

        }

        public static class ICC
        {
            public const string ItemCreation = "ItemCreation";
            public const string ItemCreatedPushToLN = "ItemCreatedPushToLN";
            public const string ItemUpdateRequest = "ItemUpdateRequest";
        }
        public static class SCR
        {
            public const string SCRInitiator = "SCR-initiator";
        }
    }
}
