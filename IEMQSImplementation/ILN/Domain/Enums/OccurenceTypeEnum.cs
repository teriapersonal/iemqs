﻿namespace IEMQSImplementation.ILN.Domain.Enums
{
    public enum OccurenceTypeEnum
    {
        [StringValue("Single Occurence")]
        Single = 0,
        [StringValue("Multi Occurence")]
        Multi = 1
    }
}
