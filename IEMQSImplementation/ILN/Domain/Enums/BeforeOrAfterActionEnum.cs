﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.ILN.Domain.Enums
{
    public enum BeforeOrAfterActionEnum
    {
        NotRequired = 0,
        ShowPrompt = 1,
        DoSomeCalculation = 2,
        ReloadScreen = 3,
        ReloadScreenAndShowMessage = 4,
        ValidateData = 5,
        RevertToOriginalState = 6,
        ShowMessageOnly = 7,
        ShowMessageAndExecuteOtherJSAction = 8
    }
}
