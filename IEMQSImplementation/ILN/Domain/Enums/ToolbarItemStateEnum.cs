﻿namespace IEMQSImplementation.ILN.Domain.Enums
{   
    public enum ToolbarItemStateEnum
    {
        [StringValue("Enabled")]
        Enabled = 0,
        [StringValue("Disabled")]
        Disabled = 1,
        [StringValue("Selected and enabled")]
        SelectedAndEnabled = 2,
    }       
}
