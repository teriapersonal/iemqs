﻿namespace IEMQSImplementation.ILN.Domain.Enums
{   
    public enum WindowTypeEnum
    {
        [StringValue("Single Window")]
        SingleWindow = 0,
        [StringValue("Multi Main Window")]
        MultiMainWindow = 1,
        [StringValue("Dynamic Window")]
        DynamicWindow = 2
    }    
}
