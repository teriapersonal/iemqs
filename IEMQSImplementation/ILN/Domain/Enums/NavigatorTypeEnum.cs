﻿namespace IEMQSImplementation.ILN.Domain.Enums
{
    public enum NavigatorTypeEnum
    {
        [StringValue("CURRENT")]
        Current = 0,
        [StringValue("FIRST")]
        First = 1,
        [StringValue("LAST")]
        Last = 2,
        [StringValue("NEXT")]
        Next = 3,
        [StringValue("PREV")]
        Previous = 4
    }
}
