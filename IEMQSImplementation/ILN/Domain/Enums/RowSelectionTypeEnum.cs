﻿namespace IEMQSImplementation.ILN.Domain.Enums
{
    public enum RowSelectionTypeEnum
    {
        [StringValue("Single")]
        Single = 0,
        [StringValue("Multi")]
        Multi = 1
    }
}
