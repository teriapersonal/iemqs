﻿namespace IEMQSImplementation.ILN.Domain.Enums
{   
    public enum AccessTypeEnum
    {
        [StringValue("View Only")]
        Display = 0,
        [StringValue("Maintain")]
        Maintain = 1,
        [StringValue("Form update")]
        Update = 2
    }       
}
