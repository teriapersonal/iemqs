﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.ILN.Domain.Enums
{
    public enum ActualActionEnum
    {
        JSOnly = 0,
        JSAndServer = 1,
        ServerOnly = 2
    }
}
