﻿namespace IEMQSImplementation.ILN.Domain.Enums
{   
    public enum ToolbarItemTypeEnum
    {
        [StringValue("Button")]
        Button = 0,
        [StringValue("Split Button")]
        SplitButton = 1,
        [StringValue("Menu Button")]
        MenuButton = 2,
        [StringValue("Menu Item")]
        MenuItem = 3
    }       
}
