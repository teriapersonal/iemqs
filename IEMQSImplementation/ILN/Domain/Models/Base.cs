﻿
namespace IEMQSImplementation.ILN.Domain.Models
{
    public class Base
    {
        public int Id;
        public string Name;
        public string FriendlyName;
    }
}
