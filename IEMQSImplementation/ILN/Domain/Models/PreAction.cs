﻿using IEMQSImplementation.ILN.Domain.Enums;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class PreAction
    {
        public BeforeOrAfterActionEnum Type;
        public string FunctionName;
        public string PromptMessage;
        public string[] PromptOptions = new string[] { "Yes", "No" };
    }
}
