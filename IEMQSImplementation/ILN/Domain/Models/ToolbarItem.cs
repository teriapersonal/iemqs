﻿
using IEMQSImplementation.ILN.Domain.Enums;
using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class ToolbarItem: Base
    {        
        public string Title;
        public string Src;
        public string State;
        public bool IsVisible;
        public bool IsCustomItem;
        public List<ToolbarItem> ChildMenuToolbarItems;
        public int? ParentItemId;
        public ToolbarItemTypeEnum? ItemType;
        public string DisplayText;
        public ToolbarAction Action;
        public int? Order;
        public string ParameterValue;
    }
}
