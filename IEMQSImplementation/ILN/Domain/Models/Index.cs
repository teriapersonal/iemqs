﻿using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class Index : Base
    {
        public List<IndexColumn> Columns;
        public bool IsDefault;
    }
}
