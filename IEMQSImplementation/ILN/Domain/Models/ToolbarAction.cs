﻿
namespace IEMQSImplementation.ILN.Domain.Models
{
    public class ToolbarAction
    {
        public PreAction PreValue;
        public ActualAction ActualValue;
        public PostAction PostValue;
    }
}
