﻿using IEMQSImplementation.ILN.Domain.Enums;
using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class Screen : ToolbarBase
    {   
        public string FriendlyName;
        public WindowTypeEnum WindowTypeId;
        public int OccurenceTypeId;
        public int MainSessionId;
        public List<int> SatelliteSessionIds;
        public List<List<IDictionary<string, object>>> DataInDictionaryForm;
        public string TableData;
        public string DataTablesColumnsConfig;
        //Derived Properties Start
        public OccurenceType OccurenceType;
        public Session MainSession;
        public List<Session> SatelliteSessions;
        //Derived Properties End
    }
}
