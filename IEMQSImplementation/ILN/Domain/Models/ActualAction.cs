﻿using IEMQSImplementation.ILN.Domain.Enums;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class ActualAction
    {
        public ActualActionEnum Type;
        public ServerActionTypeEnum ServerActionType;
        public string[] FunctionNames;
    }
}
