﻿using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class Group : Base
    {
        public List<int> InvolvedColumnsIds;
    }
}
