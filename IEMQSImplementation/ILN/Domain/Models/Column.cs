﻿namespace IEMQSImplementation.ILN.Domain.Models
{
    public class Column: Base
    {
        public int DisplayOrder;
        public string Type;
        public string DisplayName;
    }
}

