﻿using IEMQSImplementation.ILN.Domain.Enums;
using System;
using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class Session : ToolbarBase
    {
        public int TableId;
        public AccessTypeEnum AccessTypeId;        
        public ColumnDefinition ReferencedThrough;
        public string TableName;
        public Table Table;
        public List<Group> Groups;
        public MultiOccurence MultiOccurenceInstance;
        public SingleOccurence SingleOccurenceInstance;
    }
    public class Table : Base
    {
        public bool IsLNTable;
        public List<Index> Indices;
        public List<ColumnDefinition> Columns;
    }
    public class Occcurence : Base
    {
        //Group 1 - Col1, Col2, Col3.   Group 2 - Col 2, 3, 4
        /*
         * Show only selected groups
         * Show all columns
         * Show all groups
         * Show all columns except certain groups
         * Show all groups except certain groups
         * */

    }
    public class SingleOccurence : Occcurence
    {
        public bool IsSplitMode;
        public List<SplitTab> SplitTabs;
        public List<TabColumn> TabColumns;
    }
    public class MultiOccurence : Occcurence
    {
        public string RowSelectionType;
        public bool ShowCheckbox;
        public bool IsSelectAllRowsAllowedViewCheckbox;
        public bool NavigateKeys;        
        public List<ViewColumn> ViewColumns;
        public List<GridColumn> GridColumns;
    }
    public class SplitTab : Base
    {
        public List<TabColumn> TabColumns;
    }
}
