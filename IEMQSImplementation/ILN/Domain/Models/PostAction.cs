﻿using IEMQSImplementation.ILN.Domain.Enums;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class PostAction
    {
        public BeforeOrAfterActionEnum Type;
        public string Message;
        public string FunctionName;
    }
}
