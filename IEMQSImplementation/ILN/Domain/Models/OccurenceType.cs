﻿using IEMQSImplementation.ILN.Domain.Enums;
using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class OccurenceType: ToolbarBase
    {        
        public string ProcedureName;
        public OccurenceTypeEnum Type;
        public int ResultsCount;
    }
}
