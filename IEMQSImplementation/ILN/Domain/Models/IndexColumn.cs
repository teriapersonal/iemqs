﻿namespace IEMQSImplementation.ILN.Domain.Models
{
    public class IndexColumn : Column
    {
        public int Order;
        public string Direction;
    }
}
