﻿using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Domain.Models
{
    public class ToolbarBase : Base
    {
        public List<List<ToolbarItem>> ToolbarItems;
    }
}
