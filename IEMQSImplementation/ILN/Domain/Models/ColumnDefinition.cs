﻿namespace IEMQSImplementation.ILN.Domain.Models
{
    public class ColumnDefinition : Column
    {
        public string ReferenceColumn;
        public string ReferenceTable;
        public string DescriptionColumn;
        public string DependentOnColumn;
        public bool? IsMandatory;        
        public object Constraints;
    }
    public class GridColumn : ColumnDefinition
    {
        public bool? IsSearchable;
        public bool? IsVisible;
        public bool? IsSortable;
    }
    public class TabColumn: ColumnDefinition
    {

    }
}
