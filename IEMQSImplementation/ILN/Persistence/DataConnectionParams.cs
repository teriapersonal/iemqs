﻿using Dapper;
using IEMQSImplementation.ILN.Persistence.Enums;
using System.Collections.Generic;
using System.Data;

namespace IEMQSImplementation.ILN.Persistence
{
    public class DataConnectionParams 
    {
        protected object _Params;
        /// <summary>
        /// DataConnectionParams
        /// </summary>
        /// <param name="queryType"></param>
        public DataConnectionParams(DataConnectionTypeEnum queryType = DataConnectionTypeEnum.Text)
        {
            QueryType = queryType == DataConnectionTypeEnum.Text
                ? CommandType.Text
                : CommandType.StoredProcedure;            
        }

        public virtual object Params { get; set; }
        
        /// <summary>
        /// Query can return multiple results set. So this property value helps to get those many results set from DB
        /// </summary>
        public int ResultCount { get; set; }

        /// <summary>
        /// Query
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// QueryType
        /// </summary>
        public CommandType QueryType { get; private set; }        
    }
    public class DataConnectionParams<T> : DataConnectionParams
    {  

        public DataConnectionParams(DataConnectionTypeEnum queryType = DataConnectionTypeEnum.StoredProcedure): base(queryType)
        {

        }

        public override object Params {
            get
            {
                return _Params as DynamicParameters;
            }
            set
            {
                if(typeof(T) == typeof(DynamicParameter))
                {
                    List<DynamicParameter> parametersList = value as List<DynamicParameter>;
                    DynamicParameters dynamicParameters = new DynamicParameters();
                    foreach (DynamicParameter dynamicParameter in parametersList)
                    {
                        dynamicParameters.Add(dynamicParameter.Name, dynamicParameter.Value, dynamicParameter.DbType);
                    }
                    _Params = dynamicParameters;
                }
                else if(typeof(T)== typeof(object))
                {
                    _Params = value;
                }
            }
        }        
    }
}
