﻿using System.Data;

namespace IEMQSImplementation.ILN.Persistence
{
    public class DynamicParameter
    {
        public DbType? DbType;
        public string Name;
        public object Value;
        public ParameterDirection? Direction;
        public int? Size;
    }
}
