//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class MAL012
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MAL012()
        {
            this.MAL012_2 = new HashSet<MAL012_2>();
            this.MAL012_3 = new HashSet<MAL012_3>();
        }
    
        public int HeaderId { get; set; }
        public string ProjectNo { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string EquipmentNameAndNo { get; set; }
        public string InspectionAgency { get; set; }
        public string ManufacturingCode { get; set; }
        public string Client { get; set; }
        public string PoNo { get; set; }
        public string Stage { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string PhotoHeaderDetails { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL012_2> MAL012_2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL012_3> MAL012_3 { get; set; }
    }
}
