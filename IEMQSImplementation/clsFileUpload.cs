﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.SharePoint.Client;
using Shp = Microsoft.SharePoint.Client;
using SFolderKM = IEMQSImplementation.SharepointReference1;
using SUploadKM = IEMQSImplementation.SharepointReference3;
using System.Xml;
using System.IO;
using System.Text;
using Ionic.Zip;
using System.Threading.Tasks;

namespace IEMQSImplementation
{
    public class clsFileUpload : clsBase
    {
        public string Type = "";
        public SharePointConfigurationEnt spcEnt = null;
        SharePointServer server = null;
        public clsFileUpload()
        {
            spcEnt = GetSharePointConfiguration(Type);
            server = GetServer(spcEnt);
        }
        public clsFileUpload(string type)
        {
            Type = type;
            spcEnt = GetSharePointConfiguration(Type);
            server = GetServer(spcEnt);
        }

        //#region Data Variable
        ////public static string[] allowedExtensions = new string[] { ".jpg", ".jpeg", ".pdf", ".png", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".xlsm" };
        //private static string FileURL = System.Configuration.ConfigurationManager.AppSettings["File_Upload_URL"];
        //private static string UserID = System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID"];
        //private static string Password = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password"];
        //private static string Library = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Library"];
        //private static string NetworkDomain = System.Configuration.ConfigurationManager.AppSettings["NetworkDomain"];
        //private static string FTPDMSName = System.Configuration.ConfigurationManager.AppSettings["FTPDMSName"];
        //#endregion

        #region Get Document List
        public SharePointFile[] GetDocuments(string folderPath, bool includeFromSubfolders = false, string orderby = "", string FileName = "", bool CompareWithExt = true)
        {
            if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
            {
                return GetNormalDocuments(folderPath, includeFromSubfolders, orderby, FileName, CompareWithExt);
            }
            else
            {
                return GetKMDocuments(folderPath, orderby, FileName, CompareWithExt);
            }
        }
        public Stream GetFileStream(string folderPath, string fileName)
        {
            if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
            {
                return GetNormalFileStream(folderPath, fileName);
            }
            else
            {
                return GetKMFileStream(folderPath, fileName);
            }
        }
        //public void CreateZipForFolder(string folderPath, string FileName, Stream outputStream)
        //{
        //    List<Stream> streams = new List<Stream>();
        //    ZipFile zf = new ZipFile();
        //    Stream responseStream = GetFTPFileStream(folderPath, FileName);
        //    if (responseStream != null)
        //    {
        //        zf.AddEntry(FileName, responseStream);
        //        streams.Add(responseStream);
        //    }
        //    zf.Save(Server.MapPath("~/App_Data/File.zip"));
        //    foreach (FileStream stream in streams)
        //    {
        //        stream.Close();
        //    }
        //}

        public Stream GetFTPFileStream(string folderPath, string FileName, string Path)
        {
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(spcEnt.FTP.Host + "/" + folderPath + "/" + FileName);
            ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
            ftpRequest.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
            ftpRequest.UseBinary = true;
            ftpRequest.UsePassive = true;
            ftpRequest.KeepAlive = true;
            FileStream fs = new FileStream(Path, FileMode.OpenOrCreate);
            using (FtpWebResponse ftpresponse = (FtpWebResponse)ftpRequest.GetResponse())
            {
                Stream responseStream = ftpresponse.GetResponseStream();
                responseStream.CopyTo(fs);
            }
            fs.Seek(0, SeekOrigin.Begin);
            return fs;
        }

        #region Normal
        private SharePointFile[] GetNormalDocuments(string folderPath, bool includeFromSubfolders = false, string orderby = "", string FileName = "", bool CompareWithExt = true)
        {
            List<SharePointFile> fileList = new List<SharePointFile>();
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);
                if (FileName != string.Empty && CompareWithExt)
                {
                    var serverRelativeUrl = server.ServerRelativeUrlPrefix + folderPath + "/" + FileName;
                    var file = server.web.GetFileByServerRelativeUrl(serverRelativeUrl);
                    server.context.Load(file);
                    server.context.ExecuteQuery();

                    var cc = server.context;
                    cc.Load(file);
                    cc.Load(file.ListItemAllFields);
                    cc.ExecuteQuery();
                    var vals = file.ListItemAllFields;
                    var Id = vals.FieldValues.Any(q => q.Key == "ID") ? vals["ID"].ToString() : vals["Id"].ToString();
                    bool onFTP = false;
                    try
                    {
                        onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                    }
                    catch (KeyNotFoundException)
                    {
                        onFTP = false;
                    }

                    FTPDetailEnt ftpEnt = null;
                    if (onFTP) { ftpEnt = spcEnt.FTP; }
                    string url = onFTP ? spcEnt.FTP.DMS + "/" + folderPath + "/" + file.Name : server.ServerUrl + "/" + file.ServerRelativeUrl;
                    var sph = new SharePointFile(
                        Id,
                        file.Name,
                        url,
                        (vals.FieldValues["Uploader"] == null) ? "" : (string)vals.FieldValues["Uploader"],
                        (vals.FieldValues["Comments"] == null) ? "" : (string)vals.FieldValues["Comments"],
                        onFTP,
                        (vals.FieldValues["File_x0020_Size"] == null) ? "" : (string)vals.FieldValues["File_x0020_Size"]
                        );

                    fileList.Add(sph);
                }
                else
                {
                    var folder = GetFolderFromPath(server, folderPath);
                    if (folder != null)
                    {
                        var cc = server.context;
                        var files = folder.Files;
                        var folders = folder.Folders;
                        cc.Load(files);
                        cc.Load(folders);
                        cc.ExecuteQuery();

                        foreach (var doc in files)
                        {
                            Shp.File file = doc;
                            cc.Load(file);
                            cc.Load(file.ListItemAllFields);
                            cc.ExecuteQuery();
                            var vals = file.ListItemAllFields;
                            var Id = vals.FieldValues.Any(q => q.Key == "ID") ? vals["ID"].ToString() : vals["Id"].ToString();
                            bool onFTP = false;
                            try
                            {
                                onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                            }
                            catch (KeyNotFoundException)
                            {
                                onFTP = false;
                            }

                            FTPDetailEnt ftpEnt = null;
                            if (onFTP) { ftpEnt = spcEnt.FTP; }
                            string url = onFTP ? spcEnt.FTP.DMS + "/" + folderPath + "/" + file.Name : server.ServerUrl + "/" + file.ServerRelativeUrl;
                            var sph = new SharePointFile(
                                Id,
                                file.Name,
                                url,
                                (vals.FieldValues["Uploader"] == null) ? "" : (string)vals.FieldValues["Uploader"],
                                (vals.FieldValues["Comments"] == null) ? "" : (string)vals.FieldValues["Comments"],
                                onFTP,
                                (vals.FieldValues["File_x0020_Size"] == null) ? "" : (string)vals.FieldValues["File_x0020_Size"]
                                );
                            if (FileName != string.Empty)
                            {
                                if (CompareWithExt && FileName == sph.Name) { fileList.Add(sph); break; }
                                else if (!CompareWithExt && FileName == sph.NameWithoutExt) { fileList.Add(sph); break; }
                            }
                            else
                            {
                                fileList.Add(sph);
                            }
                        }
                        if (includeFromSubfolders)
                        {
                            foreach (var doc in folders)
                            {
                                Shp.Folder f = doc;
                                cc.Load(f);
                                cc.ExecuteQuery();
                                cc.Load(f.Files);
                                cc.ExecuteQuery();
                                var folderName = f.Name;
                                foreach (var file in f.Files)
                                {
                                    cc.Load(file);
                                    cc.Load(file.ListItemAllFields);
                                    cc.ExecuteQuery();
                                    var vals = file.ListItemAllFields;
                                    var Id = vals.FieldValues.Any(q => q.Key == "ID") ? vals["ID"].ToString() : vals["Id"].ToString();
                                    bool onFTP = false;
                                    try
                                    {
                                        onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                                    }
                                    catch (KeyNotFoundException)
                                    {
                                        onFTP = false;
                                    }

                                    string SubFolderPath = f.ServerRelativeUrl.Substring(f.ServerRelativeUrl.IndexOf(folderPath)).Replace(folderPath, "");
                                    FTPDetailEnt ftpEnt = null;
                                    if (onFTP) { ftpEnt = spcEnt.FTP; }
                                    string url = onFTP ? spcEnt.FTP.DMS + "/" + folderPath + SubFolderPath + "/" + file.Name : server.ServerUrl + "/" + file.ServerRelativeUrl;
                                    var sph = new SharePointFile(
                                        Id,
                                        file.Name,
                                        url,
                                        (vals.FieldValues["Uploader"] == null) ? "" : (string)vals.FieldValues["Uploader"],
                                        (vals.FieldValues["Comments"] == null) ? "" : (string)vals.FieldValues["Comments"],
                                        onFTP,
                                        (vals.FieldValues["File_x0020_Size"] == null) ? "" : (string)vals.FieldValues["File_x0020_Size"]
                                        );
                                    if (FileName != string.Empty)
                                    {
                                        if (CompareWithExt && FileName == sph.Name) { fileList.Add(sph); break; }
                                        else if (!CompareWithExt && FileName == sph.NameWithoutExt) { fileList.Add(sph); break; }
                                    }
                                    else
                                    {
                                        fileList.Add(sph);
                                    }
                                    //fileList.Add(new SharePointFile(vals["Id"].ToString(), file.Name, server.ServerUrl + "/" + file.ServerRelativeUrl, (vals["Uploader"] == null) ? "" : (string)vals["Uploader"], (vals["Comments"] == null) ? "" : (string)vals["Comments"]));
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(orderby))
                        {
                            fileList = fileList.OrderBy(x => x.Name).ToList(); ;
                        }
                    }
                }

            }
            catch (Exception) { }
            fileList.ForEach(i => { i.FolderPath = folderPath; });
            return fileList.ToArray();
        }
        private Folder GetFolderFromPath(SharePointServer server, string folderPath)
        {
            try
            {
                if (server == null)
                {
                    return null;
                }
                var path = folderPath;
                if (path[0] == '/')
                    path = path.Substring(1);
                if (path[path.Length - 1] == '/')
                    path = path.Substring(0, path.Length - 1);
                Folder folder = server.web.GetFolderByServerRelativeUrl(server.ServerRelativeUrlPrefix + path);
                server.context.Load(folder);
                server.context.ExecuteQuery();
                return folder;
            }
            catch (Exception) { }
            return null;
        }
        private Stream GetNormalFileStream(string folderPath, string fileName)
        {
            var stream = (Stream)new MemoryStream();
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);
                var folder = GetFolderFromPath(server, folderPath);
                server.context.Load(folder);
                server.context.Load(folder.Files);
                server.context.ExecuteQuery();
                foreach (var file in folder.Files)
                {
                    if (file.Name.Trim().Equals(fileName.Trim()))
                    {
                        server.context.Load(file);
                        server.context.Load(file.ListItemAllFields);
                        server.context.Load(file.ListItemAllFields);
                        server.context.ExecuteQuery();
                        var vals = file.ListItemAllFields;

                        string url = server.ServerUrl + "/" + file.ServerRelativeUrl;

                        HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(server.ServerUrl + file.ServerRelativeUrl);
                        fileReq.Credentials = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
                        HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                        if (fileReq.ContentLength > 0)
                            fileResp.ContentLength = fileReq.ContentLength;
                        stream = fileResp.GetResponseStream();

                        break;
                    }
                }
            }
            catch (Exception e) { throw e; }
            return stream;
        }
        private Stream GetNormalResponseStream(string folderPath, string fileName)
        {
            Stream rtnStream = null;
            //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
            //var server = GetServer(ent);
            var folder = GetFolderFromPath(server, folderPath);
            server.context.Load(folder);
            server.context.Load(folder.Files);
            server.context.ExecuteQuery();
            foreach (var file in folder.Files)
            {
                if (file.Name.Trim().Equals(fileName.Trim()))
                {
                    server.context.Load(file);
                    server.context.Load(file.ListItemAllFields);
                    server.context.Load(file.ListItemAllFields);
                    server.context.ExecuteQuery();
                    var vals = file.ListItemAllFields;

                    string url = server.ServerUrl + "/" + file.ServerRelativeUrl;

                    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(server.ServerUrl + file.ServerRelativeUrl);
                    fileReq.Credentials = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
                    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                    if (fileReq.ContentLength > 0)
                        fileResp.ContentLength = fileReq.ContentLength;
                    rtnStream = fileResp.GetResponseStream();

                    break;
                }
            }
            return rtnStream;
        }
        #endregion

        #region KM
        private SharePointFile[] GetKMDocuments(string Path, string orderby = "", string FileName = "", bool CompareWithExt = true)
        {
            string incl = Path;
            //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
            //var server = GetServer(ent);
            SFolderKM.Lists list = new SFolderKM.Lists();
            NetworkCredential nc = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
            list.Credentials = nc;
            list.Url = spcEnt.URL + "/_vti_bin/lists.asmx";

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            System.Xml.XmlNode listQuery = doc.SelectSingleNode("//Query");

            System.Xml.XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");

            System.Xml.XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");

            listQueryOptions.InnerXml = "<Folder>" + spcEnt.LibraryName + "/" + Path + "</Folder><ViewAttributes Scope=\"Recursive\" />";
            System.Xml.XmlNode items = list.GetListItems(spcEnt.LibraryName, string.Empty, listQuery, listViewFields, string.Empty, listQueryOptions, "");

            XmlDocument wdoc = new XmlDocument();
            string tstx = String.Empty;
            wdoc.LoadXml(items.InnerXml);
            int count = 0;
            string[] strID = new string[2];
            XmlNodeList oNodes = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ItemCount"] != null)
                    {
                        count = Int32.Parse(objReader["ItemCount"].ToString());
                    }
                    break;
                }
            }
            int l = 0;
            List<SharePointFile> files = new List<SharePointFile>();
            XmlNodeList oNodes1 = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes1)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ows_FileLeafRef"] != null)
                    {
                        strID = objReader["ows_FileLeafRef"].ToString().Split('#');
                        string uploader = (objReader["ows_Uploader"] == null) ? "" : objReader["ows_Uploader"].ToString();
                        string comments = (objReader["ows_Comments"] == null) ? "" : objReader["ows_Comments"].ToString();
                        bool onFTP = (objReader["ows_onFTP"] == null || (objReader["ows_onFTP"] != null && objReader["ows_onFTP"].ToString() == "0")) ? false : Convert.ToBoolean(objReader["ows_onFTP"].ToString());
                        string url = onFTP ? spcEnt.FTP.Host.Replace("ftp://", (spcEnt.IsSecureConnection ? "https://" : "http://")) + "/" + spcEnt.FTP.DMS + "/" + Path + "/" + strID[1] : objReader["ows_EncodedAbsUrl"];
                        var sph = new SharePointFile(objReader["ows_ID"].ToString(), strID[1], objReader["ows_EncodedAbsUrl"], uploader, comments, onFTP);
                        if (FileName != string.Empty)
                        {
                            if (CompareWithExt && FileName == sph.Name) { files.Add(sph); break; }
                            else if (!CompareWithExt && FileName == sph.NameWithoutExt) { files.Add(sph); break; }
                        }
                        else
                        {
                            files.Add(sph);
                        }
                        l++;
                    }
                }
            }
            if (!string.IsNullOrEmpty(orderby))
            {
                files = files.OrderBy(x => x.Name).ToList(); ;
            }
            files.ForEach(i => { i.FolderPath = Path; });
            return files.ToArray();
        }
        private Stream GetKMFileStream(string Path, string FileName)
        {
            var stream = (Stream)new MemoryStream();
            HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(spcEnt.URL + "/" + spcEnt.LibraryName + "/" + Path + "/" + FileName);
            fileReq.Credentials = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
            HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
            if (fileReq.ContentLength > 0)
                fileResp.ContentLength = fileReq.ContentLength;
            stream = fileResp.GetResponseStream();
            return stream;
        }
        #endregion

        #endregion

        #region Delete
        public void DeleteFile(string Path, string FileName)
        {
            try
            {
                if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                {
                    DeleteNormalFile(Path, FileName);
                }
                else
                {
                    DeleteKMFile(Path, FileName);
                }
            }
            catch (Exception) { }
        }

        #region Normal
        private void DeleteNormalFile(string Path, string FileName)
        {
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);
                var serverRelativeUrl = server.ServerRelativeUrlPrefix + Path + "/" + FileName;
                var file = server.web.GetFileByServerRelativeUrl(serverRelativeUrl);
                server.context.Load(file);
                server.context.ExecuteQuery();

                #region Remove File From FTP If On FTP Upload
                var cc = server.context;
                cc.Load(file);
                cc.Load(file.ListItemAllFields);
                cc.ExecuteQuery();
                var vals = file.ListItemAllFields;
                bool onFTP = false;
                try
                {
                    onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                }
                catch (KeyNotFoundException)
                {
                    onFTP = false;
                }
                if (onFTP)
                {
                    RemoveFileFromFTP(Path, FileName);
                }
                #endregion

                file.DeleteObject();
                server.context.ExecuteQuery();

            }
            catch (Exception) { }
        }

        private void RemoveFileFromFTP(string Path, string FileName)
        {
            try
            {
                //var FTP = (new clsFileUpload(Type)).GetSharePointConfiguration().FTP;

                ///* Create an FTP Request */
                //FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(FTP.Host + "/" + Path + "/" + FileName);
                ////If you need to use network credentials
                //ftpRequest.Credentials = new NetworkCredential(FTP.UserName, FTP.Password);
                ////additionally, if you want to use the current user's network credentials, just use:
                ////System.Net.CredentialCache.DefaultNetworkCredentials

                ///* When in doubt, use these options */
                //ftpRequest.UseBinary = true;
                //ftpRequest.UsePassive = true;
                //ftpRequest.KeepAlive = true;

                ///* Specify the Type of FTP Request */
                //ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                //FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                //Console.WriteLine("Delete status: {0}", response.StatusDescription);
                //response.Close();
                //ftpRequest = null;
                DeleteFTPFile(Path + "/" + FileName);
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        }
        #endregion

        #region KM
        private void DeleteKMFile(string Path, string FileName)
        {
            var files = GetDocuments(Path);
            var fileCount = files.Where(q => q.Name.Equals(FileName)).Count();
            if (fileCount > 0)
            {
                var file = files.Where(q => q.Name.Equals(FileName)).FirstOrDefault();
                DelDoc(file.FileId, System.Web.HttpUtility.UrlDecode(file.URL));
            }
        }
        private XmlNode DelDoc(string DocID, string fileurl)
        {
            //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
            //var server = GetServer(ent);

            SFolderKM.Lists li = new SFolderKM.Lists();

            NetworkCredential nc = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
            li.Credentials = nc;
            li.Url = spcEnt.URL + "/_vti_bin/lists.asmx";

            string xml = "<Method ID='1' Cmd='Delete'><Field Name='ID'>" + DocID + "</Field >";
            xml += "<Field Name='FileRef'>" + fileurl + "</Field>";
            xml += "</Method>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlElement Batch = doc.CreateElement("Batch");
            Batch.SetAttribute("OnError", "Continue");
            Batch.SetAttribute("ListVersion", "1");
            Batch.SetAttribute("ViewName", "");
            Batch.InnerXml = xml;
            XmlNode resultNode = li.UpdateListItems(spcEnt.LibraryName, Batch);

            return resultNode;

        }
        #endregion

        #endregion

        #region Check File Exist
        /// <summary>
        /// Fetches the files for a given path.
        /// </summary>
        /// <param name="Path">Folder Path to retrieve from. Example - LNC/P01352/SR1</param>
        /// <returns>Dictionary object with Key as File name and Value as the URL that can be used to retrieve it</returns>        
        public bool CheckAttachmentUpload(string folderPath, bool includeFromSubfolders = false)
        {
            var existing = GetDocuments(folderPath, includeFromSubfolders);

            if (!existing.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CheckAnyFileUploadInFolder(string folderPath)
        {
            bool IsExist = false;
            if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
            {
                var folder = GetFolderFromPath(server, folderPath);
                if (folder != null)
                {
                    var cc = server.context;
                    var files = folder.Files;
                    var folders = folder.Folders;
                    cc.Load(files);
                    cc.Load(folders);
                    cc.ExecuteQuery();

                    if (files.Count > 0)
                    {
                        IsExist = true;
                    }
                }
            }
            return IsExist;
        }
        #endregion

        #region File Upload
        public bool FileUpload(string UploadFileName, string folderPath, Stream data, string uploader, string comments, bool onFTP)
        {
            bool IsUpload = false;
            try
            {
                if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                {
                    IsUpload = FileUploadNormal(UploadFileName, folderPath, data, uploader, comments, onFTP);
                }
                else
                {
                    var bytes = GetByteFromStream(data);
                    var result = FileUploadKM(UploadFileName, bytes, folderPath, uploader, comments);
                    if (result == 0) { IsUpload = true; }
                }
            }
            catch (Exception ex)
            {
                IsUpload = false;
                throw ex;
            }
            return IsUpload;
        }
        public byte[] GetByteFromStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public void UploadFileToFtp(string folderPath, string fileName, string localFile)
        {
            FtpWebRequest ftpRequest = null;
            Stream ftpStream = null;
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(spcEnt.FTP.Host + "/" + folderPath + "/" + fileName);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);

                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */

                ftpRequest.Method = WebRequestMethods.Ftp.AppendFile;
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpRequest.GetRequestStream();
                /* Open a File Stream to Read the File for Upload */
                FileStream localFileStream = new FileStream(localFile, FileMode.Open);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[localFileStream.Length];
                int bytesSent = localFileStream.Read(byteBuffer, 0, byteBuffer.Length);
                /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
                try
                {
                    ftpStream.Write(byteBuffer, 0, byteBuffer.Length);
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return;

        }
        #region Normal
        private bool FileUploadNormal(string UploadFileName, string folderPath, Stream data, string uploader, string comments, bool onFTP)
        {
            bool IsUpload = false;
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);
                var folder = CreateNormalFolder(server, folderPath);
                var files = folder.Files;
                server.context.Load(files);
                server.context.ExecuteQuery();
                data.Seek(0, SeekOrigin.Begin);
                var file = files.Add(new FileCreationInformation()
                {
                    ContentStream = data,
                    Overwrite = true,
                    Url = UploadFileName
                });

                file.ListItemAllFields.FieldValues.Add("Uploader", uploader);
                file.ListItemAllFields.FieldValues.Add("Comments", comments);
                if (clsImplementationEnum.UploadType.Normal.GetStringValue() == Type)
                {
                    file.ListItemAllFields.FieldValues.Add("onFTP", onFTP);
                }
                server.context.Load(file);
                server.context.ExecuteQuery();
                server.context.Load(file.ListItemAllFields);
                server.context.ExecuteQuery();
                file.ListItemAllFields["Uploader"] = uploader;
                file.ListItemAllFields["Comments"] = comments;
                if (clsImplementationEnum.UploadType.Normal.GetStringValue() == Type)
                {
                    file.ListItemAllFields["onFTP"] = onFTP;
                }

                file.ListItemAllFields.Update();
                server.context.ExecuteQuery();
                IsUpload = true;
            }
            catch (Exception ex)
            {
                IsUpload = false;
                throw ex;
            }
            return IsUpload;
        }
        private Folder CreateNormalFolder(SharePointServer server, string folderPath)
        {
            try
            {
                var path = folderPath;
                if (path[0] == '/')
                    path = path.Substring(1);
                if (path[path.Length - 1] == '/')
                    path = path.Substring(0, path.Length - 1);
                Folder folder = server.web.GetFolderByServerRelativeUrl(server.ServerRelativeUrlPrefix);
                server.context.Load(folder);
                server.context.Load(folder.Folders);
                server.context.ExecuteQuery();
                var folders = path.Split('/');
                for (int i = 0; i < folders.Count(); i++)
                {
                    if (!folder.Folders.Any(q => q.Name.Equals(folders[i])))
                    {
                        var newFolder = folder.Folders.Add(folders[i]);
                        server.context.Load(newFolder);
                        server.context.ExecuteQuery();
                    }
                    folder = folder.Folders.FirstOrDefault(q => q.Name.Equals(folders[i]));
                    server.context.Load(folder);
                    server.context.Load(folder.Folders);
                    server.context.ExecuteQuery();
                }
                return folder;
            }
            catch (Exception) { }
            return null;
        }

        #endregion

        #region KM
        /// <summary>
        /// Upload File to SharePoint.
        /// </summary>
        /// <param name="FileName">File Name with extension</param>
        /// <param name="fileBytes">Byte Array of the file content.</param>
        /// <param name="FolderName">Folder Path to save in. Example - LNC/P01352/SR1</param>
        /// <returns>Result Code</returns>
        private SharePointUploadResult FileUploadKM(string FileName, byte[] fileBytes, string FolderName, string Uploader, string Comments)
        {
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);

                XmlNode resultNode = null;
                if (FileName != "")
                {
                    string FOLDER_EXISTS = "0x8107090d";
                    string SUCCESS = "0x00000000";
                    string fname = FileName;
                    string ext = System.IO.Path.GetExtension(fname);
                    string foldername = FolderName;
                    var dirs = foldername.Split('/');
                    for (int i = 0; i < dirs.Count(); i++)
                    {
                        string[] prev = new string[i];
                        for (int j = 0; j < i; j++)
                            prev[j] = dirs[j];
                        var root = string.Join("/", prev);
                        resultNode = CreateKMFolder(dirs[i], root);
                    }

                    if (resultNode != null)
                    {
                        // success
                        if (resultNode.FirstChild.FirstChild.InnerText == SUCCESS || resultNode.FirstChild.FirstChild.InnerText == FOLDER_EXISTS)
                        {
                            //already existed
                            try
                            {
                                string filename = FileName;
                                byte[] _byteArray = fileBytes;
                                string url = spcEnt.URL + "/" + spcEnt.LibraryName + "/" + foldername + "/";
                                return UploadKMFileToSharePoint(FileName, url + FileName, _byteArray, Uploader, Comments);

                            }
                            catch (Exception) { return SharePointUploadResult.Unknown_Error; }
                        }
                        else { return SharePointUploadResult.Unknown_Error; }
                    }
                    else { return SharePointUploadResult.Folder_Creation_Failed; }
                }
                else { return SharePointUploadResult.Invalid_Parameters; }
            }
            catch (Exception) { return SharePointUploadResult.Invalid_Parameters; }
        }

        private XmlNode CreateKMFolder(string folder, string RootFolder = "")
        {
            //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
            //var server = GetServer(ent);
            XmlNode resultNode = null;
            SFolderKM.Lists li = new SFolderKM.Lists();
            li.Url = spcEnt.URL + "/_vti_bin/lists.asmx";
            NetworkCredential nc = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
            li.Credentials = nc;
            try
            {
                string xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
                xml += "<Field Name='BaseName'>" + folder + "</Field>";
                xml += "</Method>";

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                XmlElement Batch = doc.CreateElement("Batch");
                Batch.SetAttribute("OnError", "Continue");
                Batch.SetAttribute("ListVersion", "1");
                Batch.SetAttribute("ViewName", "");
                var root = spcEnt.URL + "/" + spcEnt.LibraryName + ((string.IsNullOrWhiteSpace(RootFolder)) ? "" : "/" + RootFolder);
                Batch.SetAttribute("RootFolder", root);
                Batch.InnerXml = xml;
                resultNode = li.UpdateListItems(spcEnt.LibraryName, Batch);
            }
            catch (Exception) { }

            return resultNode;

        }
        private SharePointUploadResult UploadKMFileToSharePoint(string UploadedFilePath, string SharePointPath, byte[] data, string uploader, string comments)
        {
            //WebResponse response = null;
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);

                string filename = Path.GetFileNameWithoutExtension(UploadedFilePath);
                string extn = Path.GetExtension(UploadedFilePath);
                SUploadKM.Copy cf = new SUploadKM.Copy();
                NetworkCredential nc = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
                cf.Credentials = nc;
                cf.Url = spcEnt.URL + "/_vti_bin/copy.asmx";

                SUploadKM.FieldInformation information_Uploader = new SUploadKM.FieldInformation
                {
                    DisplayName = "Uploader",
                    InternalName = "Uploader",
                    Type = SUploadKM.FieldType.Text,
                    Value = uploader,
                };
                SUploadKM.FieldInformation information_Comments = new SUploadKM.FieldInformation
                {
                    DisplayName = "Comments",
                    InternalName = "Comments",
                    Type = SUploadKM.FieldType.Text,
                    Value = ((string.IsNullOrWhiteSpace(comments)) ? "" : comments),
                };

                SUploadKM.FieldInformation[] info = { information_Uploader, information_Comments };
                SUploadKM.CopyResult[] res;
                string[] desturls = { SharePointPath };
                filename = filename + extn;
                uint copyres = cf.CopyIntoItems(filename, desturls, info, data, out res);
                if (res[0].ErrorCode == SUploadKM.CopyErrorCode.Success)
                {
                    return SharePointUploadResult.Success;
                }
                else
                {
                    return SharePointUploadResult.Unknown_Error;
                }
            }
            catch (Exception)
            {
                return SharePointUploadResult.Unknown_Error;
            }
        }
        #endregion

        #endregion

        #region Common Function
        private SharePointServer GetServer(SharePointConfigurationEnt ent)
        {
            Uri url = new Uri(ent.URL);
            string left = ent.URL.Replace(url.GetLeftPart(UriPartial.Authority), "");
            string server = url.GetLeftPart(UriPartial.Authority);
            string URL = string.Empty;
            try
            {
                //string URL = ent.URL.Substring(0, ent.URL.LastIndexOf("/"));
                //string Lib = ent.URL.Substring(ent.URL.LastIndexOf("/") + 1);
                if (ent.URL.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Length > 2)
                {
                    URL = ent.URL.Substring(0, ent.URL.LastIndexOf("/"));
                }
                else
                {
                    URL = ent.URL;
                }
                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(ent.UserName, ent.Password, ent.NetworkDomain);
                Web oWebsite = cc.Web;
                List srcList = oWebsite.Lists.GetByTitle(ent.LibraryName);
                cc.Load(srcList);
                cc.ExecuteQuery();

                cc.RequestTimeout = int.MaxValue;

                return new SharePointServer
                {
                    context = cc,
                    web = oWebsite,
                    Library = srcList,
                    ServerRelativeUrlPrefix = left + '/',
                    ServerUrl = server
                };
            }
            catch (Exception ex) { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }
            return null;
        }
        public FTPDetailEnt GetFTPDetails()
        {
            SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
            return ent.FTP;
        }
        public void CreateFTPFolder(string folderPath, string fileName = "")
        {
            string Path = string.Empty;
            foreach (var item in folderPath.Split('/'))
            {
                Path += "/" + item;
                try
                {
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(spcEnt.FTP.Host + Path);
                    request.Method = WebRequestMethods.Ftp.ListDirectory;
                    request.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);

                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    {
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            if (item == fileName)
                            {
                                Path = Path.Substring(1, Path.Length);
                                DeleteFTPFile(Path);
                            }
                        }
                        continue;
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        FtpWebResponse response = (FtpWebResponse)ex.Response;
                        if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                        {
                            WebRequest request = WebRequest.Create(spcEnt.FTP.Host + Path);
                            request.Method = WebRequestMethods.Ftp.MakeDirectory;
                            request.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
                            using (var resp = (FtpWebResponse)request.GetResponse())
                            {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        private void DeleteFTPFile(string FilePath)
        {
            if (CheckFileExistInFTP(FilePath))
            {
                FtpWebRequest requestDelete = (FtpWebRequest)WebRequest.Create(spcEnt.FTP.Host + "/" + FilePath);
                requestDelete.Method = WebRequestMethods.Ftp.DeleteFile;
                requestDelete.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
                using (FtpWebResponse responseDelete = (FtpWebResponse)requestDelete.GetResponse())
                {
                }
            }
        }

        private bool CheckFileExistInFTP(string FilePath)
        {
            bool isExist = false;
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(spcEnt.FTP.Host + "/" + FilePath);
            ftpRequest.UseBinary = true;
            ftpRequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            ftpRequest.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
            try
            {
                using (FtpWebResponse responseDelete = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    isExist = true;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    //Does not exist
                    isExist = false;
                }
            }
            return isExist;
        }

        public string GetFileUploaderName(string Path, string FileName)
        {
            string Uploader = string.Empty;
            try
            {
                if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                {
                    var listDocs = GetNormalDocuments(Path, false, "", FileName, true);
                    if (listDocs != null)
                    {
                        Uploader = listDocs.FirstOrDefault().Uploader;
                    }
                }
                else
                {
                    var listDocs = GetKMDocuments(Path, "", FileName, true);
                    if (listDocs != null)
                    {
                        Uploader = listDocs.FirstOrDefault().Uploader;
                    }
                }
            }
            catch (Exception) { }
            return Uploader;
        }

        private void GetNormalFileUploaderName(string Path, string FileName)
        {
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);
                var serverRelativeUrl = server.ServerRelativeUrlPrefix + Path + "/" + FileName;
                var file = server.web.GetFileByServerRelativeUrl(serverRelativeUrl);
                server.context.Load(file);
                server.context.ExecuteQuery();

                #region Remove File From FTP If On FTP Upload
                var cc = server.context;
                cc.Load(file);
                cc.Load(file.ListItemAllFields);
                cc.ExecuteQuery();
                var vals = file.ListItemAllFields;
                string Uploader = string.Empty;
                try
                {
                    Uploader = (vals.FieldValues["Uploader"] != null && vals.FieldValues["Uploader"].ToString() != string.Empty) ? Convert.ToString(vals.FieldValues["Uploader"]) : string.Empty;
                }
                catch (KeyNotFoundException)
                {
                    Uploader = string.Empty;
                }

                #endregion

                file.DeleteObject();
                server.context.ExecuteQuery();

            }
            catch (Exception) { }
        }
        #endregion

        #region Delete Files Async
        public async Task DeleteFileAsync(string Path, string FileName)
        {
            Task<int> longRunningTask = LongRunningDeleteFileAsync(Path, FileName);
            int result = await longRunningTask;
        }
        public async Task<int> LongRunningDeleteFileAsync(string Path, string FileName)
        {
            string FTPHOSTNAME = spcEnt.FTP.Host;
            string FTPUSERNAME = spcEnt.FTP.UserName;
            string FTPPASSWORD = spcEnt.FTP.Password;
            string SPUSERNAME = spcEnt.UserName;
            string SPPASSWORD = spcEnt.Password;
            string NETWORKDOMAIN = spcEnt.NetworkDomain;
            string SPURL = spcEnt.URL;
            string SPLIBRARYNAME = spcEnt.LibraryName;
            await Task.Run(() => RunDeleteFileAsync(Path, FileName, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD, NETWORKDOMAIN, SPURL, SPLIBRARYNAME, SPUSERNAME, SPPASSWORD));
            return 1;
        }
        public void RunDeleteFileAsync(string Path, string FileName, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD, string NETWORKDOMAIN, string SPURL, string SPLIBRARYNAME, string SPUSERNAME, string SPPASSWORD)
        {
            try
            {
                if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                {
                    DeleteNormalFileAsync(Path, FileName, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD);
                }
                //else
                //{
                //    DeleteKMFile(Path, FileName);
                //}
            }
            catch (Exception) { }
        }
        private void DeleteNormalFileAsync(string Path, string FileName, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD)
        {
            try
            {
                //SharePointConfigurationEnt ent = GetSharePointConfiguration(Type);
                //var server = GetServer(ent);
                var serverRelativeUrl = server.ServerRelativeUrlPrefix + Path + "/" + FileName;
                var file = server.web.GetFileByServerRelativeUrl(serverRelativeUrl);
                server.context.Load(file);
                server.context.ExecuteQuery();

                #region Remove File From FTP If On FTP Upload
                var cc = server.context;
                cc.Load(file);
                cc.Load(file.ListItemAllFields);
                cc.ExecuteQuery();
                var vals = file.ListItemAllFields;
                bool onFTP = false;
                try
                {
                    onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                }
                catch (KeyNotFoundException)
                {
                    onFTP = false;
                }
                if (onFTP)
                {
                    DeleteFTPFileAsync(Path + "/" + FileName, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD);
                }
                #endregion

                file.DeleteObject();
                server.context.ExecuteQuery();

            }
            catch (Exception) { }
        }
        #endregion

        #region Delete Folder Files Async
        public async Task DeleteFolderFilesAsync(string Path)
        {
            Task<int> longRunningTask = LongRunningDeleteFolderFileAsync(Path);
            int result = await longRunningTask;
        }
        public async Task<int> LongRunningDeleteFolderFileAsync(string Path)
        {
            string FTPHOSTNAME = spcEnt.FTP.Host;
            string FTPUSERNAME = spcEnt.FTP.UserName;
            string FTPPASSWORD = spcEnt.FTP.Password;
            string SPUSERNAME = spcEnt.UserName;
            string SPPASSWORD = spcEnt.Password;
            string NETWORKDOMAIN = spcEnt.NetworkDomain;
            string SPURL = spcEnt.URL;
            string SPLIBRARYNAME = spcEnt.LibraryName;
            await Task.Run(() => RunDeleteFolderFileAsync(Path, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD, NETWORKDOMAIN, SPURL, SPLIBRARYNAME, SPUSERNAME, SPPASSWORD));
            return 1;
        }
        public void RunDeleteFolderFileAsync(string Path, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD, string NETWORKDOMAIN, string SPURL, string SPLIBRARYNAME, string SPUSERNAME, string SPPASSWORD)
        {
            try
            {
                var lstFiles = GetDocuments(Path);
                foreach (var item in lstFiles)
                {
                    if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                    {
                        DeleteNormalFileAsync(Path, item.Name, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD);
                    }
                    else
                    {
                        DeleteKMFile(Path, item.Name);
                    }
                }
            }
            catch (Exception) { }
        }
        #endregion

        #region Copy Files Async
        public async Task CopyFolderContentsAsync(string inputFolderPath, string outputFolderPath, string fileName = "")
        {
            Task<int> longRunningTask = LongRunningCopyFolderContentsAsync(inputFolderPath, outputFolderPath, fileName);
            int result = await longRunningTask;
        }
        public async Task<int> LongRunningCopyFolderContentsAsync(string inputFolderPath, string outputFolderPath, string fileName = "")
        {
            string FTPHOSTNAME = spcEnt.FTP.Host;
            string FTPUSERNAME = spcEnt.FTP.UserName;
            string FTPPASSWORD = spcEnt.FTP.Password;
            string SPUSERNAME = spcEnt.UserName;
            string SPPASSWORD = spcEnt.Password;
            string NETWORKDOMAIN = spcEnt.NetworkDomain;
            string SPURL = spcEnt.URL;
            string SPLIBRARYNAME = spcEnt.LibraryName;
            await Task.Run(() => RunAsyncCopyFolderContents(inputFolderPath, outputFolderPath, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD, NETWORKDOMAIN, SPURL, SPLIBRARYNAME, SPUSERNAME, SPPASSWORD, fileName));
            return 1;
        }

        public void RunAsyncCopyFolderContents(string inputFolderPath, string outputFolderPath, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD, string NETWORKDOMAIN, string SPURL, string SPLIBRARYNAME, string SPUSERNAME, string SPPASSWORD, string fileName = "")
        {
            Uri url = new Uri(SPURL);
            string left = SPURL.Replace(url.GetLeftPart(UriPartial.Authority), "");
            string fname = string.Empty;
            try
            {
                string URL = SPURL.Substring(0, SPURL.LastIndexOf("/"));
                string Lib = SPURL.Substring(SPURL.LastIndexOf("/") + 1);

                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(SPUSERNAME, SPPASSWORD, NETWORKDOMAIN);
                Web oWebsite = cc.Web;
                List srcList = oWebsite.Lists.GetByTitle(SPLIBRARYNAME);//change the library name to your library name
                cc.Load(srcList);
                cc.ExecuteQuery();
                var query = new CamlQuery();
                query.FolderServerRelativeUrl = left + "/" + inputFolderPath;//change this to the server relative URL of your source folder
                var files = srcList.GetItems(query);
                cc.Load(files);
                cc.ExecuteQuery();

                string foldername = outputFolderPath;//"@" + lblInsp.Text.Trim();
                                                     //string subFolder = "@" + lblInsp.Text.Trim();
                var dirs = foldername.Split('/');
                if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                {
                    CreateNormalFolder(server, outputFolderPath);
                }

                foreach (var doc in files)
                {
                    Shp.File file = doc.File;
                    cc.Load(file);
                    cc.Load(file.ListItemAllFields);
                    cc.ExecuteQuery();

                    fname = fileName != string.Empty ? fileName : file.Name;
                    if (fname == file.Name)
                    {
                        var vals = file.ListItemAllFields;
                        var Id = vals.FieldValues.Any(q => q.Key == "ID") ? vals["ID"].ToString() : vals["Id"].ToString();
                        bool onFTP = false;
                        try
                        {
                            onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                        }
                        catch (KeyNotFoundException)
                        {
                            onFTP = false;
                        }
                        if (onFTP)
                        {
                            (new clsFileUpload(Type)).CreateFTPFolderAsync(outputFolderPath, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD, file.Name);
                            string sourcePath = inputFolderPath + "/" + file.Name;
                            string destinationPath = outputFolderPath + "/" + file.Name;
                            CopyFTPFileAsync(sourcePath, destinationPath, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD);
                        }

                        String location = left + "/" + outputFolderPath + "/" + file.Name;//change this to the server relative URL of your destination folder
                        file.CopyTo(location, true);
                        cc.ExecuteQuery();

                        if (fileName != string.Empty)
                        {
                            break;
                        }
                    }
                }
            }
            catch
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void CreateFTPFolderAsync(string folderPath, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD, string fileName = "")
        {
            string Path = string.Empty;
            foreach (var item in folderPath.Split('/'))
            {
                Path += "/" + item;
                try
                {
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FTPHOSTNAME + Path);
                    request.Method = WebRequestMethods.Ftp.ListDirectory;
                    request.Credentials = new NetworkCredential(FTPUSERNAME, FTPPASSWORD);

                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    {
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            if (item == fileName)
                            {
                                Path = Path.Substring(1, Path.Length);
                                DeleteFTPFileAsync(Path, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD);
                            }
                        }
                        continue;
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        FtpWebResponse response = (FtpWebResponse)ex.Response;
                        if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                        {
                            WebRequest request = WebRequest.Create(FTPHOSTNAME + Path);
                            request.Method = WebRequestMethods.Ftp.MakeDirectory;
                            request.Credentials = new NetworkCredential(FTPUSERNAME, FTPPASSWORD);
                            using (var resp = (FtpWebResponse)request.GetResponse())
                            {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        private void DeleteFTPFileAsync(string FilePath, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD)
        {
            if (CheckFileExistInFTPAsync(FilePath, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD))
            {
                FtpWebRequest requestDelete = (FtpWebRequest)WebRequest.Create(FTPHOSTNAME + "/" + FilePath);
                requestDelete.Method = WebRequestMethods.Ftp.DeleteFile;
                requestDelete.Credentials = new NetworkCredential(FTPUSERNAME, FTPPASSWORD);
                using (FtpWebResponse responseDelete = (FtpWebResponse)requestDelete.GetResponse())
                {
                }
            }
        }
        private bool CheckFileExistInFTPAsync(string FilePath, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD)
        {
            bool isExist = false;
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(FTPHOSTNAME + "/" + FilePath);
            ftpRequest.UseBinary = true;
            ftpRequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            ftpRequest.Credentials = new NetworkCredential(FTPUSERNAME, FTPPASSWORD);
            try
            {
                using (FtpWebResponse responseDelete = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    isExist = true;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    //Does not exist
                    isExist = false;
                }
            }
            return isExist;
        }
        public bool CopyFTPFileAsync(string SourceFilePath, string DestinationFileName, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FTPHOSTNAME + "/" + SourceFilePath);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(FTPUSERNAME, FTPPASSWORD);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                CopyFileFromSourceToDestinationAsync(spcEnt.FTP.Host + "/" + DestinationFileName, responseStream, FTPHOSTNAME, FTPUSERNAME, FTPPASSWORD);
                responseStream.Close();
                return true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }
        public bool CopyFileFromSourceToDestinationAsync(string DestinationFilePath, Stream SourceFileStream, string FTPHOSTNAME, string FTPUSERNAME, string FTPPASSWORD)
        {
            try
            {
                System.Net.FtpWebRequest clsRequest = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(DestinationFilePath);
                clsRequest.Credentials = new System.Net.NetworkCredential(FTPUSERNAME, FTPPASSWORD);
                clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                System.IO.Stream destStream = clsRequest.GetRequestStream();
                SourceFileStream.CopyTo(destStream);
                destStream.Close();
                destStream.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Copy Files
        public void CopyFolderContents(string inputFolderPath, string outputFolderPath, string fileName = "")
        {
            Uri url = new Uri(spcEnt.URL);
            string left = spcEnt.URL.Replace(url.GetLeftPart(UriPartial.Authority), "");
            string fname = string.Empty;
            try
            {
                string URL = spcEnt.URL.Substring(0, spcEnt.URL.LastIndexOf("/"));
                string Lib = spcEnt.URL.Substring(spcEnt.URL.LastIndexOf("/") + 1);

                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(spcEnt.UserName, spcEnt.Password, spcEnt.NetworkDomain);
                Web oWebsite = cc.Web;
                List srcList = oWebsite.Lists.GetByTitle(spcEnt.LibraryName);//change the library name to your library name
                cc.Load(srcList);
                cc.ExecuteQuery();
                var query = new CamlQuery();
                query.FolderServerRelativeUrl = left + "/" + inputFolderPath;//change this to the server relative URL of your source folder
                var files = srcList.GetItems(query);
                cc.Load(files);
                cc.ExecuteQuery();

                string foldername = outputFolderPath;//"@" + lblInsp.Text.Trim();
                                                     //string subFolder = "@" + lblInsp.Text.Trim();
                var dirs = foldername.Split('/');
                if (Type == clsImplementationEnum.UploadType.Normal.GetStringValue())
                {
                    CreateNormalFolder(server, outputFolderPath);
                }

                foreach (var doc in files)
                {
                    Shp.File file = doc.File;
                    cc.Load(file);
                    cc.Load(file.ListItemAllFields);
                    cc.ExecuteQuery();

                    fname = fileName != string.Empty ? fileName : file.Name;
                    if (fname == file.Name)
                    {
                        var vals = file.ListItemAllFields;
                        var Id = vals.FieldValues.Any(q => q.Key == "ID") ? vals["ID"].ToString() : vals["Id"].ToString();
                        bool onFTP = false;
                        try
                        {
                            onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                        }
                        catch (KeyNotFoundException)
                        {
                            onFTP = false;
                        }
                        if (onFTP)
                        {
                            (new clsFileUpload(Type)).CreateFTPFolder(outputFolderPath, file.Name);
                            string sourcePath = inputFolderPath + "/" + file.Name;
                            string destinationPath = outputFolderPath + "/" + file.Name;
                            CopyFTPFile(sourcePath, destinationPath);
                        }

                        String location = left + "/" + outputFolderPath + "/" + file.Name;//change this to the server relative URL of your destination folder
                        file.CopyTo(location, true);
                        cc.ExecuteQuery();
                        if (fileName != string.Empty)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public bool CopyFTPFile(string SourceFilePath, string DestinationFileName)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(spcEnt.FTP.Host + "/" + SourceFilePath);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                CopyFileFromSourceToDestination(spcEnt.FTP.Host + "/" + DestinationFileName, responseStream);
                responseStream.Close();
                return true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }


        public bool CopyFileFromSourceToDestination(string DestinationFilePath, Stream SourceFileStream)
        {
            try
            {
                System.Net.FtpWebRequest clsRequest = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(DestinationFilePath);
                clsRequest.Credentials = new System.Net.NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
                clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                System.IO.Stream destStream = clsRequest.GetRequestStream();
                SourceFileStream.CopyTo(destStream);
                destStream.Close();
                destStream.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Zip Folder
        public List<ZipFilesEnt> CreateZipFilelist(string folderPath, bool includeFromSubfolders = false)
        {
            List<ZipFilesEnt> lstZipFilesEnt = new List<ZipFilesEnt>();
            List<SharePointFile> Files = new List<SharePointFile>();
            List<SharePointFile> OtherFoldersFiles = new List<SharePointFile>();
            foreach (var f in folderPath.Split(','))
            {
                OtherFoldersFiles = (new clsFileUpload()).GetDocuments(f, false, "").ToList();
                Files.AddRange(OtherFoldersFiles);
            }

            //var Files = GetDocuments(folderPath, includeFromSubfolders);
            foreach (var file in Files)
            {
                string FilePath = file.URL.ToLower().Substring(file.URL.ToLower().IndexOf(file.FolderPath.ToLower()));
                if (!file.onFTP)
                {
                    string FileName = FilePath.ToLower().Replace(file.FolderPath.ToLower() + "/", "");
                    Stream responseStream = GetFileStream(file.FolderPath, file.Name);
                    lstZipFilesEnt.Add(new ZipFilesEnt() { FileName = FileName, Bytes = responseStream });
                }
            }
            foreach (var file in Files)
            {
                if (file.onFTP)
                {
                    string FilePath = file.URL.ToLower().Substring(file.URL.ToLower().IndexOf(file.FolderPath.ToLower()));
                    if (CheckFileExistInFTP(FilePath))
                    {
                        string FileName = FilePath.ToLower().Replace(file.FolderPath.ToLower() + "/", "");
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(spcEnt.FTP.Host + "/" + FilePath);
                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                        request.Credentials = new NetworkCredential(spcEnt.FTP.UserName, spcEnt.FTP.Password);
                        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                        Stream responseStream = response.GetResponseStream();
                        //lstZipFilesEnt.Add(new ZipFilesEnt() { FileName = file.Name, Bytes = responseStream });
                        lstZipFilesEnt.Add(new ZipFilesEnt() { FileName = FileName, Bytes = responseStream });
                    }
                }
            }
            return lstZipFilesEnt;
        }
        #endregion
        public SharePointConfigurationEnt GetSharePointConfiguration(string Type = "")
        {
            bool outIsSecureConnection = false;
            SharePointConfigurationEnt ent = new SharePointConfigurationEnt();
            var UserName = System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID" + Type];
            var Password = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password" + Type];
            var LibraryName = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Library" + Type];
            var URL = System.Configuration.ConfigurationManager.AppSettings["File_Upload_URL" + Type];
            var AllowExtensions = System.Configuration.ConfigurationManager.AppSettings["AllowExtensions"];
            var NetworkDomain = System.Configuration.ConfigurationManager.AppSettings["NetworkDomain"];
            var IEMQSDOC_Path = System.Configuration.ConfigurationManager.AppSettings["IEMQSDOC_Path" + Type];
            if (System.Configuration.ConfigurationManager.AppSettings["IsSecureConnection"] != null && System.Configuration.ConfigurationManager.AppSettings["IsSecureConnection"].ToString() != "")
            {
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsSecureConnection"].ToString(), out outIsSecureConnection);
            }
            ent.IsSecureConnection = outIsSecureConnection;

            var FTPDMSName = System.Configuration.ConfigurationManager.AppSettings["FTPDMSName" + Type];
            var FTPHost = System.Configuration.ConfigurationManager.AppSettings["FTPHost" + Type];
            var FTPUser = System.Configuration.ConfigurationManager.AppSettings["FTPUser" + Type];
            var FTPPass = System.Configuration.ConfigurationManager.AppSettings["FTPPass" + Type];


            if (UserName != null) { ent.UserName = UserName; }
            if (Password != null) { ent.Password = Password; }
            if (LibraryName != null) { ent.LibraryName = LibraryName; }
            if (URL != null) { ent.URL = URL; }
            if (AllowExtensions != null) { ent.AllowExtensions = AllowExtensions; }
            if (NetworkDomain != null) { ent.NetworkDomain = NetworkDomain; }
            if (IEMQSDOC_Path != null) { ent.IEMQSDOC_Path = IEMQSDOC_Path; }

            if (FTPDMSName != null) { ent.FTP.DMS = FTPDMSName; }
            if (FTPHost != null) { ent.FTP.Host = FTPHost; }
            if (FTPUser != null) { ent.FTP.UserName = FTPUser; }
            if (FTPPass != null) { ent.FTP.Password = FTPPass; }

            return ent;
        }

        public class FTPDetailEnt
        {
            public string Host { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string DMS { get; set; }
        }

        private class SharePointServer
        {
            public ClientContext context { get; set; }
            public Web web { get; set; }
            public string ServerUrl { get; set; }
            public string ServerRelativeUrlPrefix { get; set; }
            public List Library { get; set; }
        }

        public class SharePointConfigurationEnt
        {
            public SharePointConfigurationEnt()
            {
                FTP = new FTPDetailEnt();
            }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string LibraryName { get; set; }
            public string URL { get; set; }
            public string AllowExtensions { get; set; }
            public string NetworkDomain { get; set; }
            public string IEMQSDOC_Path { get; set; }
            public FTPDetailEnt FTP { get; set; }
            public bool IsSecureConnection { get; set; }
        }
        public enum SharePointUploadResult
        {
            Success,
            Invalid_File_Extension,
            File_Size_Too_Big,
            Folder_Creation_Failed,
            Invalid_Parameters,
            Unknown_Error
        }

        public class ZipFilesEnt
        {
            public string FileName { get; set; }
            public string FolderName { get; set; }
            public Stream Bytes { get; set; }
        }
    }

    public class ChunkMergeUtility
    {
        public string FileName { get; set; }
        public string TempFolder { get; set; }
        public int MaxFileSizeMB { get; set; }
        public List<String> FileParts { get; set; }
        public ChunkMergeUtility()
        {
            FileParts = new List<string>();
        }

        /// <summary>
        /// original name + ".part_N.X" (N = file part number, X = total files)
        /// Objective = enumerate files in folder, look for all matching parts of split file. If found, merge and return true.
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public bool MergeFile(string FileName, string mergePath, string FolderPath, string Type, string uploader, bool onFTP, string comment = "")
        {
            bool rslt = false;
            try
            {
                // parse out the different tokens from the filename according to the convention
                string partToken = ".part_";
                string baseFileName = FileName.Substring(0, FileName.IndexOf(partToken));
                string trailingTokens = FileName.Substring(FileName.IndexOf(partToken) + partToken.Length);
                int FileIndex = 0;
                int FileCount = 0;
                int.TryParse(trailingTokens.Substring(0, trailingTokens.IndexOf(".")), out FileIndex);
                int.TryParse(trailingTokens.Substring(trailingTokens.IndexOf(".") + 1), out FileCount);
                // get a list of all file parts in the temp folder
                string Searchpattern = Path.GetFileName(baseFileName) + partToken + "*";
                string[] FilesList = Directory.GetFiles(Path.GetDirectoryName(FileName), Searchpattern);
                //  merge .. improvement would be to confirm individual parts are there / correctly in sequence, a security check would also be important
                // only proceed if we have received all the file chunks

                if (FilesList.Count() == FileCount)
                {
                    // use a singleton to stop overlapping processes
                    if (!MergeFileManager.Instance.InUse(baseFileName))
                    {
                        MergeFileManager.Instance.AddFile(baseFileName);
                        var MergeFileName = mergePath + baseFileName.Split('\\').Last();

                        try
                        {
                            if (System.IO.File.Exists(MergeFileName))
                                System.IO.File.Delete(MergeFileName);
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            MergeFileManager.Instance.RemoveFile(baseFileName);
                            throw ex;
                        }

                        try
                        {
                            // add each file located to a list so we can get them into 
                            // the correct order for rebuilding the file
                            List<SortedFile> MergeList = new List<SortedFile>();
                            try
                            {
                                foreach (string File in FilesList)
                                {
                                    SortedFile sFile = new SortedFile();
                                    sFile.FileName = File;
                                    baseFileName = File.Substring(0, File.IndexOf(partToken));
                                    trailingTokens = File.Substring(File.IndexOf(partToken) + partToken.Length);
                                    int.TryParse(trailingTokens.Substring(0, trailingTokens.IndexOf(".")), out FileIndex);
                                    sFile.FileOrder = FileIndex;
                                    MergeList.Add(sFile);
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                throw ex;
                            }


                            // sort by the file-part number to ensure we merge back in the correct order
                            var MergeOrder = MergeList.OrderBy(s => s.FileOrder).ToList();

                            #region Merge chunk file in Stream and save using Sharepoint Library
                            using (Stream stream = new MemoryStream())
                            {
                                try
                                {
                                    if (onFTP)
                                    {
                                        //FtpCreateFolder(FolderPath, baseFileName.Split('\\').Last(), Type);
                                        (new clsFileUpload(Type)).CreateFTPFolder(FolderPath, baseFileName.Split('\\').Last());
                                    }

                                    foreach (var chunk in MergeOrder)
                                    {
                                        if (onFTP)
                                        {
                                            try
                                            {
                                                //UploadFileToFtp(FolderPath, baseFileName.Split('\\').Last(), chunk.FileName, Type);
                                                (new clsFileUpload(Type)).UploadFileToFtp(FolderPath, baseFileName.Split('\\').Last(), chunk.FileName);
                                                byte[] buffer = Encoding.ASCII.GetBytes(baseFileName.Split('\\').Last());
                                                stream.Write(buffer, 0, buffer.Length);
                                            }
                                            catch (Exception ex)
                                            {
                                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                                throw ex;
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                using (FileStream fileChunk = new FileStream(chunk.FileName, FileMode.Open))
                                                {
                                                    fileChunk.CopyTo(stream);
                                                }
                                            }
                                            catch (IOException ex)
                                            {
                                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                                throw ex;
                                            }
                                        }
                                    }
                                    var UploadFileName = baseFileName.Split('\\').Last();
                                    clsManager Manager = new clsManager();
                                    try
                                    {
                                        (new clsFileUpload(Type)).FileUpload(UploadFileName, FolderPath, stream, uploader, comment, onFTP);
                                    }
                                    catch (Exception ex)
                                    {
                                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                        throw ex;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                    throw ex;
                                }
                                finally
                                {

                                    foreach (var chunk in MergeOrder)
                                    {
                                        if (System.IO.File.Exists(chunk.FileName))
                                        {
                                            System.IO.File.Delete(chunk.FileName);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            MergeFileManager.Instance.RemoveFile(baseFileName);
                            throw ex;
                        }
                        finally
                        {
                            MergeFileManager.Instance.RemoveFile(baseFileName);
                        }
                        rslt = true;
                        // unlock the file from singleton                       
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
            return rslt;

        }

        //private void UploadFileToFtp(string folderPath, string fileName, string localFile, string Type = "")
        //{
        //    FtpWebRequest ftpRequest = null;
        //    Stream ftpStream = null;
        //    try
        //    {
        //        var FTP = (new clsFileUpload(Type)).GetSharePointConfiguration().FTP;
        //        /* Create an FTP Request */
        //        ftpRequest = (FtpWebRequest)FtpWebRequest.Create(FTP.Host + "/" + folderPath + "/" + fileName);
        //        /* Log in to the FTP Server with the User Name and Password Provided */
        //        ftpRequest.Credentials = new NetworkCredential(FTP.UserName, FTP.Password);
        //        /* When in doubt, use these options */
        //        ftpRequest.UseBinary = true;
        //        ftpRequest.UsePassive = true;
        //        ftpRequest.KeepAlive = true;
        //        /* Specify the Type of FTP Request */
        //        ftpRequest.Method = WebRequestMethods.Ftp.AppendFile;
        //        /* Establish Return Communication with the FTP Server */
        //        ftpStream = ftpRequest.GetRequestStream();
        //        /* Open a File Stream to Read the File for Upload */
        //        FileStream localFileStream = new FileStream(localFile, FileMode.Open);
        //        /* Buffer for the Downloaded Data */
        //        byte[] byteBuffer = new byte[localFileStream.Length];
        //        int bytesSent = localFileStream.Read(byteBuffer, 0, byteBuffer.Length);
        //        /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
        //        try
        //        {
        //            ftpStream.Write(byteBuffer, 0, byteBuffer.Length);
        //        }
        //        catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        //        /* Resource Cleanup */
        //        localFileStream.Close();
        //        ftpStream.Close();
        //        ftpRequest = null;
        //    }
        //    catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        //    return;
        //}

        //private void FtpCreateFolder(string folderPath, string fileName, string Type = "")
        //{
        //    var ftpEnt = (new clsFileUpload(Type)).GetFTPDetails();
        //    string Path = string.Empty;
        //    foreach (var item in folderPath.Split('/'))
        //    {
        //        Path += "/" + item;
        //        try
        //        {
        //            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpEnt.Host + Path);
        //            request.Method = WebRequestMethods.Ftp.ListDirectory;
        //            request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
        //            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
        //            {
        //                if (item == fileName)
        //                {
        //                    FtpWebRequest requestDelete = (FtpWebRequest)WebRequest.Create(ftpEnt.Host + Path);
        //                    request.Method = WebRequestMethods.Ftp.DeleteFile;
        //                    request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
        //                    using (FtpWebResponse responseDelete = (FtpWebResponse)request.GetResponse())
        //                    {
        //                    }
        //                }
        //                continue;
        //            }
        //        }
        //        catch (WebException ex)
        //        {
        //            if (ex.Response != null)
        //            {
        //                FtpWebResponse response = (FtpWebResponse)ex.Response;
        //                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
        //                {
        //                    WebRequest request = WebRequest.Create(ftpEnt.Host + Path);
        //                    request.Method = WebRequestMethods.Ftp.MakeDirectory;
        //                    request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
        //                    using (var resp = (FtpWebResponse)request.GetResponse())
        //                    {
        //                        continue;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
    }
    public struct SortedFile
    {
        public int FileOrder { get; set; }
        public String FileName { get; set; }
    }
    public class MergeFileManager
    {
        private static MergeFileManager instance;
        private List<string> MergeFileList;

        private MergeFileManager()
        {
            try
            {
                MergeFileList = new List<string>();
            }
            catch { }
        }

        public static MergeFileManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new MergeFileManager();
                return instance;
            }
        }
        public void AddFile(string BaseFileName)
        {
            MergeFileList.Add(BaseFileName);
        }
        public bool InUse(string BaseFileName)
        {
            return MergeFileList.Contains(BaseFileName);
        }
        public bool RemoveFile(string BaseFileName)
        {
            return MergeFileList.Remove(BaseFileName);
        }
    }
}
