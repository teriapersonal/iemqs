//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL414
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string Description { get; set; }
        public string ReqElevationFrom { get; set; }
        public string ActElevationFrom { get; set; }
        public string ReqOrientation { get; set; }
        public string ReqArcLengthOrientation { get; set; }
        public string ActArcLengthOrientation { get; set; }
        public string ReqProjection { get; set; }
        public string ActProjection { get; set; }
        public string ReqTilt { get; set; }
        public string ActTilt { get; set; }
        public string ReqSize { get; set; }
        public string ActSize { get; set; }
        public string OrientationOutofRoundness { get; set; }
        public string ActualAtTopOutofRoundenss { get; set; }
        public string ActualAtBottomOutofRoundenss { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual PRL410 PRL410 { get; set; }
    }
}
