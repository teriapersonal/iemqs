//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL040
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRL040()
        {
            this.PRL041 = new HashSet<PRL041>();
            this.PRL042 = new HashSet<PRL042>();
            this.PRL043 = new HashSet<PRL043>();
            this.PRL044_1 = new HashSet<PRL044_1>();
            this.PRL044_2 = new HashSet<PRL044_2>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public Nullable<decimal> AllowableOffset { get; set; }
        public Nullable<decimal> TotalHeight { get; set; }
        public Nullable<decimal> RequiredOrientation1 { get; set; }
        public Nullable<decimal> RequiredOrientation2 { get; set; }
        public Nullable<decimal> RequiredOrientation3 { get; set; }
        public Nullable<decimal> RequiredOrientation4 { get; set; }
        public Nullable<decimal> RequiredOrientation5 { get; set; }
        public Nullable<decimal> RequiredOrientation6 { get; set; }
        public Nullable<decimal> RequiredOrientation7 { get; set; }
        public Nullable<decimal> RequiredOrientation8 { get; set; }
        public Nullable<decimal> ActualOrientation1 { get; set; }
        public Nullable<decimal> ActualOrientation2 { get; set; }
        public Nullable<decimal> ActualOrientation3 { get; set; }
        public Nullable<decimal> ActualOrientation4 { get; set; }
        public Nullable<decimal> ActualOrientation5 { get; set; }
        public Nullable<decimal> ActualOrientation6 { get; set; }
        public Nullable<decimal> ActualOrientation7 { get; set; }
        public Nullable<decimal> ActualOrientation8 { get; set; }
        public string RequiredChordLength { get; set; }
        public string ActualChordLength { get; set; }
        public string RequiredRadius { get; set; }
        public string ActualRadius { get; set; }
        public string AllowableGap { get; set; }
        public string ActualGap { get; set; }
        public string Remarks { get; set; }
        public Nullable<decimal> OrientationRequiredAtTop1 { get; set; }
        public Nullable<decimal> OrientationRequiredAtTop2 { get; set; }
        public Nullable<decimal> OrientationRequiredAtTop3 { get; set; }
        public Nullable<decimal> OrientationRequiredAtTop4 { get; set; }
        public Nullable<decimal> OrientationActualAtTop1 { get; set; }
        public Nullable<decimal> OrientationActualAtTop2 { get; set; }
        public Nullable<decimal> OrientationActualAtTop3 { get; set; }
        public Nullable<decimal> OrientationActualAtTop4 { get; set; }
        public Nullable<decimal> OrientationRequiredAtBottom1 { get; set; }
        public Nullable<decimal> OrientationRequiredAtBottom2 { get; set; }
        public Nullable<decimal> OrientationRequiredAtBottom3 { get; set; }
        public Nullable<decimal> OrientationRequiredAtBottom4 { get; set; }
        public Nullable<decimal> OrientationActualAtBottom1 { get; set; }
        public Nullable<decimal> OrientationActualAtBottom2 { get; set; }
        public Nullable<decimal> OrientationActualAtBottom3 { get; set; }
        public Nullable<decimal> OrientationActualAtBottom4 { get; set; }
        public string OrientationRemark1 { get; set; }
        public string OrientationRemark2 { get; set; }
        public string OrientationRemark3 { get; set; }
        public string OrientationRemark4 { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark4 { get; set; }
        public string Remark5 { get; set; }
        public Nullable<decimal> CladStripingWidthMax { get; set; }
        public Nullable<decimal> CladStripingWidthMin { get; set; }
        public Nullable<decimal> CladStripingDepthMax { get; set; }
        public Nullable<decimal> CladStripingDepthMin { get; set; }
        public string FNote1 { get; set; }
        public string FNote2 { get; set; }
        public string FNote3 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string QualityProject { get; set; }
        public string EquipmentNo { get; set; }
        public string DrawingNo { get; set; }
        public Nullable<int> DrawingRevisionNo { get; set; }
        public string DCRNo { get; set; }
        public Nullable<System.DateTime> InspectionDate { get; set; }
        public string SeamNo { get; set; }
        public string InspectionAgency { get; set; }
        public Nullable<System.DateTime> OfferDate { get; set; }
        public string CommentIfAny { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public Nullable<int> RequestNoSequence { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public string Remark2Note { get; set; }
        public string CladdedVessel { get; set; }
        public Nullable<decimal> OutBy0 { get; set; }
        public Nullable<decimal> OutBy90 { get; set; }
        public Nullable<decimal> OutBy180 { get; set; }
        public Nullable<decimal> OutBy270 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL041> PRL041 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL042> PRL042 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL043> PRL043 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL044_1> PRL044_1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL044_2> PRL044_2 { get; set; }
    }
}
