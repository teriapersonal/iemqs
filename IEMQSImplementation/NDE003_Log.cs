//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class NDE003_Log
    {
        public int Id { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string CTQNo { get; set; }
        public string Status { get; set; }
        public string ManufacturingCode { get; set; }
        public string ApplicableSpecification { get; set; }
        public string AcceptanceStandard { get; set; }
        public Nullable<bool> ASMECodeStamp { get; set; }
        public string TPIName { get; set; }
        public Nullable<bool> NotificationRequired { get; set; }
        public string ThicknessOfJob { get; set; }
        public string SizeDimension { get; set; }
        public string NDERemarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ReturnRemarks { get; set; }
    }
}
