//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_CSR_GET_HEADERHISTORYDETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Equipment { get; set; }
        public string Customer { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string Status { get; set; }
        public string PO { get; set; }
        public int Id { get; set; }
    }
}
