//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_MCR_INDEX_DATA_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string LnTInspCertNo { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string TPI1 { get; set; }
        public string TPI2 { get; set; }
        public string TPI3 { get; set; }
        public string TPI4 { get; set; }
        public string TPI5 { get; set; }
    }
}
