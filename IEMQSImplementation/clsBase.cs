﻿using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
namespace IEMQSImplementation
{
    public partial class IEMQSEntitiesContext : DbContext
    {
        public IEMQSEntitiesContext(int timeout)
      : base("name=IEMQSEntitiesContext")
        {
            this.Database.CommandTimeout = timeout;
        }
    }
    public class clsBase : Controller
    {
        /// <summary>
        /// Base class created by Dharmesh
        /// 24-06-2017
        /// </summary>

        public readonly IEMQSEntitiesContext db;

        public static string WebsiteURL = System.Configuration.ConfigurationManager.AppSettings["WebsiteURL"];
        public static string LNLinkedServer = System.Configuration.ConfigurationManager.AppSettings["LNLinkedServer"];
        public const string CryptKey = "!3mQS@jUn_2@17";

        #region Base Contsructor
        public clsBase()
        {
            if (db == null)
            {
                db = new IEMQSEntitiesContext(3600);
            }
        }

        #region Action Executing

        //protected override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        if (filterContext.HttpContext.Request.IsAjaxRequest())
        //        {
        //            filterContext.HttpContext.Response.StatusCode = 403;
        //         //   filterContext.HttpContext.Response.Write(SessionTimeout);
        //            filterContext.HttpContext.Response.End();
        //        }
        //    }
        //}

        #endregion

        #endregion

        #region Session Expire Filter

        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
        public class SessionExpireFilterAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                HttpContext ctx = System.Web.HttpContext.Current;
                // ----- Auto Login by Encrypted PSNo ----- //
                if (!String.IsNullOrWhiteSpace(ctx.Request["upsno"]))
                {
                    string LoginParam = "";
                    LoginParam = "upsno=" + ctx.Request["upsno"];
                    LoginParam = "?" + LoginParam + "&redirect=" + ctx.Request.Url.AbsoluteUri.Replace(LoginParam, "");
                    filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index" + LoginParam));
                    return;
                }

                if (ctx.Session[clsImplementationMessage.Session.LoginInfo] == null)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        //filterContext.HttpContext.Response.StatusCode = 504;
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/SessionTimeOut"));
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                    }
                    return;
                }

                base.OnActionExecuting(filterContext);
            }
        }
        #endregion

        #region User Permission

        public class UserPermissionsAttribute : AuthorizeAttribute
        {
            public override void OnAuthorization(AuthorizationContext filterContext)
            {
                HttpContext ctx = System.Web.HttpContext.Current;
                clsManager objManager = new clsManager();
                clsBase objclsBase = new clsBase();

                var currentArea = ctx.Request.RequestContext.RouteData.DataTokens["area"].ToString();
                var currentController = ctx.Request.RequestContext.RouteData.Values["controller"].ToString();
                var currentAction = ctx.Request.RequestContext.RouteData.Values["action"].ToString();
                var user = ctx.User; // get user from DB
                // ----- Auto Login by Encrypted PSNo ----- //
                if (!String.IsNullOrWhiteSpace(ctx.Request["upsno"]))
                {
                    string LoginParam = "";
                    LoginParam = "upsno=" + ctx.Request["upsno"];
                    LoginParam = "?" + LoginParam + "&redirect=" + ctx.Request.Url.AbsoluteUri.Replace(LoginParam, "");
                    filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index" + LoginParam));
                    return;
                }
                if (ctx.Session[clsImplementationMessage.Session.LoginInfo] == null)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        //filterContext.HttpContext.Response.StatusCode = 504;
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/SessionTimeOut"));
                    }
                    else
                    {
                        //filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                        IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
                        string returnUrl = string.Empty;
                        var auto = db1.CONFIG.Where(x => x.Key == "Autologin").FirstOrDefault();

                        if (auto != null)
                        {
                            if (!String.IsNullOrWhiteSpace(auto.Value))
                            {
                                var Msg = "";
                                clsManager Manager = new clsManager();
                                Manager.SetLoginSessionDetails(auto.Value, ref Msg, clsImplementationEnum.UserActionType.LoginAs);
                                if (System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] != null)
                                {
                                    var url = db1.CONFIG.Where(x => x.Key == "AutoURL").FirstOrDefault();
                                    if (url != null)
                                    {
                                        if (!String.IsNullOrWhiteSpace(url.Value))
                                            returnUrl = url.Value;
                                    }
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                        }
                    }
                    return;
                }
                //else if(!objManager.FN_VALIDATE_AREA_ACCESS(objclsBase.objClsLoginInfo.Location, currentArea))
                //{
                //    filterContext.Result = new RedirectResult("~/Authentication/Authenticate/AccessDenied");
                //    return;
                //}
                else if (objManager.GetpageAccess(currentArea, currentController, currentAction, objclsBase.objClsLoginInfo.UserName) == 0)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/Authenticate/AccessDenied");
                    return;
                }
                //else
                //{
                //    string retUrl = filterContext.HttpContext.Request.RawUrl;
                //    filterContext.Result =
                //                     new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary
                //                             {{ "controller", currentController },
                //          { "action", currentAction },
                //          { "returnUrl",    retUrl } });
                //}
                base.OnAuthorization(filterContext);
            }
            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest() && System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] == null)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/Authenticate/SessionTimeOut");
                }
            }
        }
        #endregion

        #region Session Class object

        public clsLoginInfo _objclsLoginInfo;
        public clsLoginInfo objClsLoginInfo
        {
            get
            {
                if (_objclsLoginInfo == null)
                {
                    _objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
                }
                return _objclsLoginInfo;

            }
            set { }
        }

        #endregion

        #region Manager class object

        public clsManager _Manager;
        public clsManager Manager
        {
            get
            {
                if (_Manager == null)
                {
                    _Manager = new clsManager();
                }
                return _Manager;

            }
            set { }
        }

        #endregion

        #region Save changes Method
        public void Save()
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                DiscardChanges();
            }
        }

        #endregion

        #region Discard changes Method
        public void DiscardChanges()
        {
            var context = db;
            var changedEntries = context.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Modified))
            {
                entry.CurrentValues.SetValues(entry.OriginalValues);
                entry.State = EntityState.Unchanged;
            }

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Added))
            {
                entry.State = EntityState.Detached;
            }

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Deleted))
            {
                entry.State = EntityState.Unchanged;
            }

        }

        #endregion

        #region Json Method
        new public JsonResult Json(object data)
        {
            return Json(data, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Json(object data, Encoding contentEncoding)
        {
            return Json(data, "application/json", contentEncoding, JsonRequestBehavior.AllowGet);
        }
        new public JsonResult Json(object data, JsonRequestBehavior behavior)
        {
            return Json(data, "application/json", Encoding.UTF8, behavior);
        }
        new public JsonResult Json(object data, string contentType)
        {
            return Json(data, contentType, Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        new public JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return Json(data, contentType, contentEncoding, JsonRequestBehavior.AllowGet);
        }
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public static string FetchRedirectURL(string UserName)
        {
            string url = "/MyDashboard/Dashboard/Index";
            var db = new IEMQSEntitiesContext(3600);
            clsManager Manager = new clsManager();
            var objATH001 = db.ATH001.Where(i => i.Employee == UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
            var objRole = objATH001.Select(i => i.Role).Distinct().ToList();
            var IsDESLive = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsDESLive.GetStringValue()));

            List<string> rolegrouplist = db.ATH004.Where(c => objRole.Contains(c.Id)).Select(c => c.RoleGroup).Distinct().ToList();
            var RoleGroup = string.Join(",", rolegrouplist);

            if (IsDESLive)
            {
                if (rolegrouplist.Any(x => x.ToString() == "ENGG"))
                {
                    url = "/DES/Dashboard/Index";
                }
                else
                {
                    url = "/MyDashboard/Dashboard/Index";
                }
            }

            return url;
        }
        #endregion

        #region Cookies Filter
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
        public class CookiesFilterAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (System.Web.HttpContext.Current.Request.Cookies["uid"] != null &&
                    System.Web.HttpContext.Current.Request.Cookies["pid"] != null)
                {
                    var db = new IEMQSEntitiesContext(3600);
                    var UserName = "";
                    var Password = "";
                    string Msg = "";
                    clsManager Manager = new clsManager();
                    UserName = clsHelper.DecryptString(System.Web.HttpContext.Current.Request.Cookies["uid"].Value);
                    Password = clsHelper.DecryptString(System.Web.HttpContext.Current.Request.Cookies["pid"].Value);
                    string LastURL = "";
                    if (System.Web.HttpContext.Current.Request.Cookies["lurl"] != null)
                    {
                        LastURL = clsHelper.DecryptString(System.Web.HttpContext.Current.Request.Cookies["lurl"].Value);
                    }
                    if (db.COM003.Any(i => i.t_psno == UserName))
                    {
                        Manager.SetLoginSessionDetails(UserName, ref Msg, clsImplementationEnum.UserActionType.Login);
                        if (Msg.Length == 0)
                        {
                            var AllowRedirectLastURL = clsImplementationEnum.ConfigParameter.AllowRedirectLastURL.GetStringValue();
                            var objAllowRedirectLastURL = db.CONFIG.Where(i => i.Key == AllowRedirectLastURL).FirstOrDefault();
                            if (objAllowRedirectLastURL != null && Convert.ToBoolean(objAllowRedirectLastURL.Value))
                            {
                                if (!string.IsNullOrEmpty(LastURL))
                                {
                                    filterContext.Result = new RedirectResult(String.Concat("~" + LastURL));
                                }
                                else
                                {
                                    string url = FetchRedirectURL(UserName);
                                    filterContext.Result = new RedirectResult(String.Concat("~/" + url));
                                }
                            }
                            else
                            {
                                string url = FetchRedirectURL(UserName);
                                filterContext.Result = new RedirectResult(String.Concat("~/" + url));
                            }
                            return;
                        }
                    }
                }
                base.OnActionExecuting(filterContext);
            }
        
        }

     
        #endregion

        #region Last URL History
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
        public class LastURLHistoryAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuted(ActionExecutedContext filterContext)
            {
                HttpContext ctx = System.Web.HttpContext.Current;
                var LastURL = ctx.Request.Url.PathAndQuery;

                HttpCookie uid = new HttpCookie("lurl", clsHelper.EncryptString(LastURL));
                uid.Expires = DateTime.Now.AddDays(30);
                ctx.Response.Cookies.Add(uid);

                base.OnActionExecuted(filterContext);
            }
        }
        #endregion
    }
}
