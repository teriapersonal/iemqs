//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_DAT_GET_INDEX_DATA_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int FormId { get; set; }
        public string FormName { get; set; }
        public string HTMLContent { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    }
}
