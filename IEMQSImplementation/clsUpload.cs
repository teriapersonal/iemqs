﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SFolder = IEMQSImplementation.SharepointDevList;
using SUpload = IEMQSImplementation.SharepointDevCopy;
using SFolderKM = IEMQSImplementation.SharepointReference1;
using SUploadKM = IEMQSImplementation.SharepointReference3;
using Microsoft.SharePoint.Client;
using Shp = Microsoft.SharePoint.Client;
using Ionic.Zip;
using IEMQSImplementation;
using System.Web;

namespace IEMQSImplementation
{
    public class clsUploadCopy : clsBase
    {

        public static bool GetFilesFromFolder(string folderPath, Stream outputStream, ATH008 objATH008, bool includeFromSubFolders = true)
        {
            try
            {
                var server = GetServer(objATH008);
                var folder = GetFolderFromPath(server, folderPath);
                var cc = server.context;
                var files = folder.Files;
                var folders = folder.Folders;
                cc.Load(files);
                cc.Load(folders);
                cc.ExecuteQuery();

                MemoryStream ms = new MemoryStream();

                ZipFile zf = new ZipFile();
                List<Stream> streams = new List<Stream>();
                foreach (var doc in files)
                {
                    Shp.File file = doc;
                    cc.Load(file);
                    cc.ExecuteQuery();
                    Stream s = getStream(server.ServerUrl, objATH008, file);
                    if (s != null)
                    {
                        zf.AddEntry(file.Name, s);
                        streams.Add(s);
                    }
                }
                if (includeFromSubFolders)
                {
                    foreach (var doc in folders)
                    {
                        Shp.Folder f = doc;
                        cc.Load(f);
                        cc.ExecuteQuery();
                        cc.Load(f.Files);
                        cc.ExecuteQuery();
                        var folderName = f.Name;
                        foreach (var file in f.Files)
                        {
                            cc.Load(file);
                            cc.ExecuteQuery();
                            Stream s = getStream(server.ServerUrl, objATH008, file);
                            if (s != null)
                            {
                                zf.AddEntry(folderName + "\\" + file.Name, s);
                                streams.Add(s);
                            }
                        }
                    }
                }
                zf.Save(outputStream);
                foreach (var stream in streams)
                    stream.Close();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        private static Stream getStream(string Server, ATH008 objATH008, Shp.File file)
        {
            Stream stream = null;
            long bytesToRead = file.Length;
            byte[] buffer = new Byte[bytesToRead];
            try
            {
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(Server + file.ServerRelativeUrl);
                fileReq.Credentials = new NetworkCredential(objATH008.UserName, objATH008.Password, "lthed");
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;
                stream = fileResp.GetResponseStream();
            }
            catch (Exception) { return null; }
            return stream;
        }

        public static Stream GetFileStream(ATH008 objATH008, string folderPath, string fileName)
        {
            var stream = (Stream)new MemoryStream();
            try
            {
                var server = GetServer(objATH008);
                var folder = GetFolderFromPath(server, folderPath);
                server.context.Load(folder);
                server.context.Load(folder.Files);
                server.context.ExecuteQuery();
                foreach (var file in folder.Files)
                {
                    if (file.Name.Trim().Equals(fileName.Trim()))
                    {


                        server.context.Load(file);
                        server.context.Load(file.ListItemAllFields);
                        server.context.Load(file.ListItemAllFields);
                        server.context.ExecuteQuery();
                        var vals = file.ListItemAllFields;


                        string url = server.ServerUrl + "/" + file.ServerRelativeUrl;

                        HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(server.ServerUrl + file.ServerRelativeUrl);
                        fileReq.Credentials = new NetworkCredential(objATH008.UserName, objATH008.Password, "lthed");
                        HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                        if (fileReq.ContentLength > 0)
                            fileResp.ContentLength = fileReq.ContentLength;
                        stream = fileResp.GetResponseStream();

                        break;
                    }
                }
            }
            catch (Exception e) { throw e; }
            return stream;
        }

        private static SharePointServer GetServer(ATH008 objATH008)
        {
            Uri url = new Uri(objATH008.LibraryRootPath);
            string left = objATH008.LibraryRootPath.Replace(url.GetLeftPart(UriPartial.Authority), "");
            string server = url.GetLeftPart(UriPartial.Authority);
            try
            {
                string URL = objATH008.LibraryRootPath.Substring(0, objATH008.LibraryRootPath.LastIndexOf("/"));
                string Lib = objATH008.LibraryRootPath.Substring(objATH008.LibraryRootPath.LastIndexOf("/") + 1);

                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(objATH008.UserName, objATH008.Password, "lthed");
                Web oWebsite = cc.Web;
                List srcList = oWebsite.Lists.GetByTitle(objATH008.LibraryName);
                cc.Load(srcList);
                cc.ExecuteQuery();

                cc.RequestTimeout = int.MaxValue;

                return new SharePointServer
                {
                    context = cc,
                    web = oWebsite,
                    Library = srcList,
                    ServerRelativeUrlPrefix = left + '/',
                    ServerUrl = server
                };
            }
            catch (Exception) { }
            return null;
        }

        private static Folder GetFolderFromPath(SharePointServer server, string folderPath)
        {
            try
            {
                var path = folderPath;
                if (path[0] == '/')
                    path = path.Substring(1);
                if (path[path.Length - 1] == '/')
                    path = path.Substring(0, path.Length - 1);
                Folder folder = server.web.GetFolderByServerRelativeUrl(server.ServerRelativeUrlPrefix + path);
                server.context.Load(folder);
                server.context.ExecuteQuery();
                return folder;
            }
            catch (Exception) { }
            return null;
        }
        public static Shp.File GetFileFromPath(ATH008 objATH008, string filePath)
        {
            try
            {
                var server = GetServer(objATH008);
                var path = filePath;
                if (path[0] == '/')
                    path = path.Substring(1);
                if (path[path.Length - 1] == '/')
                    path = path.Substring(0, path.Length - 1);
                Shp.File folder = server.web.GetFileByServerRelativeUrl(server.ServerRelativeUrlPrefix + path);
                server.context.Load(folder);
                server.context.ExecuteQuery();
                return folder;
            }
            catch (Exception) { }
            return null;
        }

        private static Folder CreateFolder(SharePointServer server, string folderPath)
        {
            try
            {
                var path = folderPath;
                if (path[0] == '/')
                    path = path.Substring(1);
                if (path[path.Length - 1] == '/')
                    path = path.Substring(0, path.Length - 1);
                Folder folder = server.web.GetFolderByServerRelativeUrl(server.ServerRelativeUrlPrefix);
                server.context.Load(folder);
                server.context.Load(folder.Folders);
                server.context.ExecuteQuery();
                var folders = path.Split('/');
                for (int i = 0; i < folders.Count(); i++)
                {
                    if (!folder.Folders.Any(q => q.Name.Equals(folders[i])))
                    {
                        var newFolder = folder.Folders.Add(folders[i]);
                        server.context.Load(newFolder);
                        server.context.ExecuteQuery();
                    }
                    folder = folder.Folders.FirstOrDefault(q => q.Name.Equals(folders[i]));
                    server.context.Load(folder);
                    server.context.Load(folder.Folders);
                    server.context.ExecuteQuery();
                }
                return folder;
            }
            catch (Exception) { }
            return null;
        }

        private static List GetFolderList(SharePointServer server, string folderPath)
        {
            var Folders = folderPath.Split('/');
            var result = server.Library;
            foreach (var folder in Folders)
            {
                server.context.Load(result);
                server.context.ExecuteQuery();
                var count = result.ItemCount;
            }
            var items = result.GetItems(new CamlQuery() { FolderServerRelativeUrl = server.ServerRelativeUrlPrefix + folderPath });
            server.context.Load(items);
            server.context.ExecuteQuery();

            return result;
        }

        public static void CopyFolderContents(string sourceFolderPath, string destinationFolderPath, ATH008 objATH008, bool copySubfolders = false, bool overWriteFiles = true)
        {
            try
            {
                var server = GetServer(objATH008);
                var sourceFolder = GetFolderFromPath(server, sourceFolderPath);
                var destinationFolder = CreateFolder(server, destinationFolderPath);
                server.context.Load(sourceFolder.Files);
                server.context.Load(sourceFolder.Folders);
                server.context.ExecuteQuery();
                foreach (var file in sourceFolder.Files)
                {
                    server.context.Load(file);
                    server.context.ExecuteQuery();
                    file.CopyTo(destinationFolder.ServerRelativeUrl + "/" + file.Name, overWriteFiles);
                    server.context.ExecuteQuery();
                }
                if (copySubfolders)
                {
                    foreach (var folder in sourceFolder.Folders)
                    {
                        server.context.Load(folder);
                        server.context.ExecuteQuery();
                        var destinationSubFolder = CreateFolder(server, destinationFolder.ServerRelativeUrl + "/" + folder.Name);
                        foreach (var file in folder.Files)
                        {
                            server.context.Load(file);
                            server.context.ExecuteQuery();
                            file.CopyTo(destinationSubFolder.ServerRelativeUrl + "/" + file.Name, overWriteFiles);
                            server.context.ExecuteQuery();
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public static void DeleteFile(string Path, string FileName, ATH008 objATH008)
        {
            try
            {
                var server = GetServer(objATH008);
                var serverRelativeUrl = server.ServerRelativeUrlPrefix + Path + "/" + FileName;//Path.Replace((new Uri(Path)).GetLeftPart(UriPartial.Authority), "");
                /*if (serverRelativeUrl[0] == '/')
                    serverRelativeUrl = serverRelativeUrl.Substring(1);*/
                var file = server.web.GetFileByServerRelativeUrl(serverRelativeUrl);
                server.context.Load(file);
                server.context.ExecuteQuery();
                file.DeleteObject();
                server.context.ExecuteQuery();
            }
            catch (Exception) { }
        }

        public static bool Upload(string UploadFileName, string folderPath, ATH008 objATH008, Stream data, string uploader, string comments)
        {
            try
            {
                var server = GetServer(objATH008);
                var folder = CreateFolder(server, folderPath);
                //var folderList = GetFolderList(server, folderPath);
                //server.context.Load(folderList);
                //server.context.ExecuteQuery();
                ////var Id = folder.Properties.FieldValues.Any(q => q.Key == "ID") ? folder.Properties["ID"].ToString() : folder.Properties["Id"].ToString();
                ////var item = server.Library.GetItemById(Id);
                var files = folder.Files;
                server.context.Load(files);
                server.context.ExecuteQuery();
                var file = files.Add(new FileCreationInformation()
                {
                    ContentStream = data,
                    Overwrite = true,
                    Url = UploadFileName
                });
                file.ListItemAllFields.FieldValues.Add("Uploader", uploader);
                file.ListItemAllFields.FieldValues.Add("Comments", comments);
                server.context.Load(file);
                server.context.ExecuteQuery();
                server.context.Load(file.ListItemAllFields);
                server.context.ExecuteQuery();
                file.ListItemAllFields["Uploader"] = uploader;
                file.ListItemAllFields["Comments"] = comments;
                file.ListItemAllFields.Update();
                server.context.ExecuteQuery();
                return true;
            }
            catch (Exception) { }
            return false;
        }
        public static bool FileUpload(string UploadFileName, string folderPath, ATH008 objATH008, Stream data, string uploader, string comments, bool onFTP)
        {
            bool IsUpload = false;
            try
            {
                var server = GetServer(objATH008);
                var folder = CreateFolder(server, folderPath);
                var files = folder.Files;
                server.context.Load(files);
                server.context.ExecuteQuery();
                data.Seek(0, SeekOrigin.Begin);
                var file = files.Add(new FileCreationInformation()
                {
                    ContentStream = data,
                    Overwrite = true,
                    Url = UploadFileName
                });

                file.ListItemAllFields.FieldValues.Add("Uploader", uploader);
                file.ListItemAllFields.FieldValues.Add("Comments", comments);
                file.ListItemAllFields.FieldValues.Add("onFTP", onFTP);
                server.context.Load(file);
                server.context.ExecuteQuery();
                server.context.Load(file.ListItemAllFields);
                server.context.ExecuteQuery();
                file.ListItemAllFields["Uploader"] = uploader;
                file.ListItemAllFields["Comments"] = comments;
                file.ListItemAllFields["onFTP"] = onFTP;

                file.ListItemAllFields.Update();
                server.context.ExecuteQuery();
                IsUpload = true;
            }
            catch (Exception ex)
            {
                IsUpload = false;
                throw ex;
            }
            return IsUpload;
        }
        public static SharePointFile[] GetDocuments(string folderPath, ATH008 objATH008, bool includeFromSubfolders = false)
        {
            List<SharePointFile> fileList = new List<SharePointFile>();
            try
            {
                var server = GetServer(objATH008);
                var folder = GetFolderFromPath(server, folderPath);
                var cc = server.context;
                var files = folder.Files;
                var folders = folder.Folders;
                cc.Load(files);
                cc.Load(folders);
                cc.ExecuteQuery();

                foreach (var doc in files)
                {
                    Shp.File file = doc;
                    cc.Load(file);
                    cc.Load(file.ListItemAllFields);
                    cc.Load(file.ListItemAllFields);
                    cc.ExecuteQuery();
                    var vals = file.ListItemAllFields;
                    var Id = vals.FieldValues.Any(q => q.Key == "ID") ? vals["ID"].ToString() : vals["Id"].ToString();
                    bool onFTP = (vals.FieldValues["onFTP"] != null && vals.FieldValues["onFTP"].ToString() != string.Empty) ? Convert.ToBoolean(vals.FieldValues["onFTP"]) : false;
                    FTPDetailEnt ftpEnt = null;
                    if (onFTP) { ftpEnt = GetFTPDetails(); }
                    string url = onFTP ? ftpEnt.Host.Replace("ftp://", "http://") + "/DMS/" + folderPath + "/" + file.Name : server.ServerUrl + "/" + file.ServerRelativeUrl;
                    fileList.Add(new SharePointFile(
                        Id,
                        file.Name,
                        url,
                        (vals.FieldValues["Uploader"] == null) ? "" : (string)vals.FieldValues["Uploader"],
                        (vals.FieldValues["Comments"] == null) ? "" : (string)vals.FieldValues["Comments"],
                        onFTP
                        )
                    );
                }
                if (includeFromSubfolders)
                {
                    foreach (var doc in folders)
                    {
                        Shp.Folder f = doc;
                        cc.Load(f);
                        cc.ExecuteQuery();
                        cc.Load(f.Files);
                        cc.ExecuteQuery();
                        var folderName = f.Name;
                        foreach (var file in f.Files)
                        {
                            cc.Load(file);
                            cc.Load(file.ListItemAllFields);
                            cc.ExecuteQuery();
                            var vals = file.ListItemAllFields;
                            fileList.Add(new SharePointFile(vals["Id"].ToString(), file.Name, server.ServerUrl + "/" + file.ServerRelativeUrl, (vals["Uploader"] == null) ? "" : (string)vals["Uploader"], (vals["Comments"] == null) ? "" : (string)vals["Comments"]));
                        }
                    }
                }
            }
            catch (Exception) { }
            return fileList.ToArray();
        }


        public static FTPDetailEnt GetFTPDetails()
        {
            FTPDetailEnt ftpEnt = new FTPDetailEnt();
            ftpEnt.Host = System.Configuration.ConfigurationManager.AppSettings["FTPHost"];
            ftpEnt.UserName = System.Configuration.ConfigurationManager.AppSettings["FTPUser"];
            ftpEnt.Password = System.Configuration.ConfigurationManager.AppSettings["FTPPass"];
            return ftpEnt;
        }
        public class FTPDetailEnt
        {
            public string Host { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }

        }

        private class SharePointServer
        {
            public ClientContext context { get; set; }
            public Web web { get; set; }
            public string ServerUrl { get; set; }
            public string ServerRelativeUrlPrefix { get; set; }
            public List Library { get; set; }
        }
    }

    public class AttachmentPartialModel
    {
        public AttachmentPartialModel(string folderPath)
        {
            this.FolderPath = folderPath;
            this.Type = "Normal";
            this.RepeatNumber = null;
            this.ViewOnly = false;
            this.UsingDataTable = false;
            this.SaveCompleteJS = "";
        }
        public string FolderPath { get; private set; }
        public string Type { get; set; }
        public int? RepeatNumber { get; set; }
        public bool ViewOnly { get; set; }
        public bool UsingDataTable { get; set; }
        public string SaveCompleteJS { get; set; }
    }

    /// <summary>
    /// Created by M. Ozair Khatri (90385894)
    /// on 25-07-2017
    /// to upload files on SharePoint Development Server phzpdshpdev
    /// Copied from clsUploadKM class, added Uploader Column
    /// </summary>
    public partial class clsUpload : clsBase
    {
        #region Variables

        public static string[] allowedExtensions = new string[] { ".jpg", ".jpeg", ".pdf", ".png", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".xlsm" };
        public static float MaxFileSizeInMB = (float)1024;
        private static string FileURL = System.Configuration.ConfigurationManager.AppSettings["File_Upload_URL"];
        private static string UserID = System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID"];
        private static string Password = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password"];
        private static string Library = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Library"];

        public enum SharePointUploadResult
        {
            Success,
            Invalid_File_Extension,
            File_Size_Too_Big,
            Folder_Creation_Failed,
            Invalid_Parameters,
            Unknown_Error
        }

        #endregion

        #region publicMethods

        public static bool GetFilesFromFolder(string folderPath, Stream outputStream)
        {
            Uri url = new Uri(FileURL);
            string left = FileURL.Replace(url.GetLeftPart(UriPartial.Authority), "");
            string server = url.GetLeftPart(UriPartial.Authority);
            try
            {
                string URL = FileURL.Substring(0, FileURL.LastIndexOf("/"));
                string Lib = FileURL.Substring(FileURL.LastIndexOf("/") + 1);

                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(UserID, Password, "lthed");
                Web oWebsite = cc.Web;

                List srcList = oWebsite.Lists.GetByTitle(Library);//change the library name to your library name
                cc.Load(srcList);
                cc.ExecuteQuery();
                var query = new CamlQuery();
                query.FolderServerRelativeUrl = left + "/" + folderPath;//change this to the server relative URL of your source folder
                var files = srcList.GetItems(query);
                cc.Load(files);
                cc.ExecuteQuery();

                MemoryStream ms = new MemoryStream();

                ZipFile zf = new ZipFile();
                List<Stream> streams = new List<Stream>();
                foreach (var doc in files)
                {
                    if (doc.FileSystemObjectType == FileSystemObjectType.Folder)
                    {
                        Shp.Folder folder = doc.Folder;
                        cc.Load(folder);
                        cc.ExecuteQuery();
                        cc.Load(folder.Files);
                        cc.ExecuteQuery();
                        var folderName = folder.Name;
                        foreach (var file in folder.Files)
                        {
                            cc.Load(file);
                            cc.ExecuteQuery();
                            Stream s = getStream(server, file);
                            if (s != null)
                            {
                                zf.AddEntry(folderName + "\\" + file.Name, s);
                                streams.Add(s);
                            }
                        }
                    }
                    else
                    {
                        Shp.File file = doc.File;
                        cc.Load(file);
                        cc.ExecuteQuery();
                        Stream s = getStream(server, file);
                        if (s != null)
                        {
                            zf.AddEntry(file.Name, s);
                            streams.Add(s);
                        }
                    }
                }
                zf.Save(outputStream);
                foreach (var stream in streams)
                    stream.Close();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        /// <summary>
        /// Upload File to SharePoint.
        /// </summary>
        /// <param name="FileName">File Name with extension</param>
        /// <param name="fileBytes">Byte Array of the file content.</param>
        /// <param name="FolderName">Folder Path to save in. Example - LNC/P01352/SR1</param>
        /// <returns>Result Code</returns>
        public static SharePointUploadResult Upload(string FileName, byte[] fileBytes, string FolderName, string Uploader, string Comments)
        {
            try
            {
                XmlNode resultNode = null;
                if (FileName != "")
                {
                    string FOLDER_EXISTS = "0x8107090d";
                    string SUCCESS = "0x00000000";
                    string fname = FileName;

                    string ext = System.IO.Path.GetExtension(fname);
                    //if (ext.ToLower().Equals(".jpg") || ext.ToLower().Equals(".png") || ext.ToLower().Equals(".pdf") || ext.ToLower().Equals(".jpeg") || ext.ToLower().Equals(".docx") || ext.ToLower().Equals(".doc"))
                    if (allowedExtensions.Contains(ext.ToLower()) || true)
                    {
                        if (fileBytes.LongLength <= (MaxFileSizeInMB * 1024 * 1024))
                        {
                            //string foldername = userid;
                            string foldername = FolderName;//"@" + lblInsp.Text.Trim();
                                                           //string subFolder = "@" + lblInsp.Text.Trim();
                            var dirs = foldername.Split('/');
                            for (int i = 0; i < dirs.Count(); i++)
                            {
                                string[] prev = new string[i];
                                for (int j = 0; j < i; j++)
                                    prev[j] = dirs[j];
                                var root = string.Join("/", prev);
                                resultNode = createFolder(dirs[i], root);
                            }

                            if (resultNode != null)
                            {
                                // success
                                if (resultNode.FirstChild.FirstChild.InnerText == SUCCESS || resultNode.FirstChild.FirstChild.InnerText == FOLDER_EXISTS)
                                {
                                    //already existed
                                    try
                                    {
                                        string filename = FileName;
                                        //Stream fStream = file;
                                        //if(fStream.)
                                        byte[] _byteArray = fileBytes;//new byte[fStream.Length];
                                                                      //byte[] btarr = System.IO.File.ReadAllBytes(uploadedFilePath+filename); 
                                                                      //FileStream fs = fStream as FileStream;  

                                        /*fStream.Read(_byteArray, 0, (int)fStream.Length);
                                        fStream.Close();*/
                                        string url = FileURL + "/" + foldername + "/";
                                        string[] destPath = { url + FileName };
                                        return UploadFileToSharePoint(FileName, destPath, _byteArray, Uploader, Comments);
                                    }
                                    catch (Exception) { return SharePointUploadResult.Unknown_Error; }
                                }
                                else { return SharePointUploadResult.Unknown_Error; }
                            }
                            else { return SharePointUploadResult.Folder_Creation_Failed; }
                        }
                        else { return SharePointUploadResult.File_Size_Too_Big; }
                    }
                    else { return SharePointUploadResult.Invalid_File_Extension; }
                }
                else { return SharePointUploadResult.Invalid_Parameters; }
            }
            catch (Exception) { return SharePointUploadResult.Invalid_Parameters; }
        }


        public static SharePointUploadResult UploadOnMultipleDestination(string FileName, byte[] fileBytes, string[] FolderNames, string Uploader, string Comments)
        {
            try
            {
                XmlNode resultNode = null;
                if (FileName != "")
                {
                    string FOLDER_EXISTS = "0x8107090d";
                    string SUCCESS = "0x00000000";
                    string fname = FileName;

                    string ext = System.IO.Path.GetExtension(fname);
                    //if (ext.ToLower().Equals(".jpg") || ext.ToLower().Equals(".png") || ext.ToLower().Equals(".pdf") || ext.ToLower().Equals(".jpeg") || ext.ToLower().Equals(".docx") || ext.ToLower().Equals(".doc"))
                    if (allowedExtensions.Contains(ext.ToLower()) || true)
                    {
                        if (fileBytes.LongLength <= (MaxFileSizeInMB * 1024 * 1024))
                        {
                            List<string> strPaths = new List<string>();
                            foreach (var foldername in FolderNames)
                            {
                                var dirs = foldername.Split('/');
                                for (int i = 0; i < dirs.Count(); i++)
                                {
                                    string[] prev = new string[i];
                                    for (int j = 0; j < i; j++)
                                        prev[j] = dirs[j];
                                    var root = string.Join("/", prev);
                                    resultNode = createFolder(dirs[i], root);
                                }
                                if (resultNode != null)
                                {
                                    if (resultNode.FirstChild.FirstChild.InnerText == SUCCESS || resultNode.FirstChild.FirstChild.InnerText == FOLDER_EXISTS)
                                    {
                                        strPaths.Add(FileURL + "/" + foldername + "/" + FileName);
                                    }
                                }
                            }
                            if (strPaths.Count > 0)
                            {
                                return UploadFileToSharePoint(FileName, strPaths.ToArray(), fileBytes, Uploader, Comments);
                            }
                            else { return SharePointUploadResult.Folder_Creation_Failed; }
                        }
                        else { return SharePointUploadResult.File_Size_Too_Big; }
                    }
                    else { return SharePointUploadResult.Invalid_File_Extension; }
                }
                else { return SharePointUploadResult.Invalid_Parameters; }
            }
            catch (Exception) { return SharePointUploadResult.Invalid_Parameters; }
        }

        /// <summary>
        /// Fetches the files for a given path.
        /// </summary>
        /// <param name="Path">Folder Path to retrieve from. Example - LNC/P01352/SR1</param>
        /// <returns>Dictionary object with Key as File name and Value as the URL that can be used to retrieve it</returns>
        public static SharePointFile[] getDocs(string Path)
        {
            string incl = Path;

            SFolder.Lists list = new SFolder.Lists();
            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            list.Credentials = nc;
            list.Url = "http://phzpdshpdev:9087/sites/IEMQS/_vti_bin/lists.asmx";

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            System.Xml.XmlNode listQuery = doc.SelectSingleNode("//Query");

            System.Xml.XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");

            System.Xml.XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");

            var SPurl = Path;


            listQueryOptions.InnerXml = "<Folder>" + Library.Replace("-", "") + "/" + SPurl + "</Folder><ViewAttributes Scope=\"Recursive\" />";
            // Guid g = GetWebID(SPurl);

            System.Xml.XmlNode items = list.GetListItems(Library, string.Empty, listQuery, listViewFields, string.Empty, listQueryOptions, "");

            XmlDocument wdoc = new XmlDocument();
            //DataTable dtbl = new DataTable();

            string tstx = String.Empty;
            wdoc.LoadXml(items.InnerXml);
            int count = 0;
            string[] strID = new string[2];
            XmlNodeList oNodes = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ItemCount"] != null)
                    {
                        count = Int32.Parse(objReader["ItemCount"].ToString());
                    }
                    break;
                }
            }
            int l = 0;
            // strID = new string[count]; strTitle = new string[count]; strType = new string[count];
            List<SharePointFile> files = new List<SharePointFile>();
            XmlNodeList oNodes1 = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes1)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ows_FileLeafRef"] != null)
                    {
                        strID = objReader["ows_FileLeafRef"].ToString().Split('#');
                        string uploader = (objReader["ows_Uploader"] == null) ? "" : objReader["ows_Uploader"].ToString();
                        string comments = (objReader["ows_Comments"] == null) ? "" : objReader["ows_Comments"].ToString();
                        files.Add(new SharePointFile(objReader["ows_ID"].ToString(), strID[1], objReader["ows_EncodedAbsUrl"], uploader, comments));
                        //files.Add(/*"name" + l + "", */strID[1], FileURL + "/" + Path + "/" + strID[1]);
                        /*docs.Add("type" + l + "", objReader["ows_DocIcon"].ToString());
                        docs.Add("date" + l + "", objReader["ows_Modified"].ToString());
                        //strID[i] = objReader["ows_ID"].ToString();*/

                        l++;

                    }
                }
            }
            return files.ToArray();
        }

        /// <summary>
        /// Fetches the files for a given path.
        /// </summary>
        /// <param name="Path">Folder Path to retrieve from. Example - LNC/P01352/SR1</param>
        /// <returns>Dictionary object with Key as File name and Value as the URL that can be used to retrieve it</returns>
        public static SharePointFile[] getDocsCopy(string Path, bool incluseFromSubfolders = false)
        {
            List<SharePointFile> filesList = new List<SharePointFile>();
            Uri url = new Uri(FileURL);
            string left = FileURL.Replace(url.GetLeftPart(UriPartial.Authority), "");
            string server = url.GetLeftPart(UriPartial.Authority);
            try
            {
                string URL = FileURL.Substring(0, FileURL.LastIndexOf("/"));
                string Lib = FileURL.Substring(FileURL.LastIndexOf("/") + 1);

                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(UserID, Password, "lthed");
                Web oWebsite = cc.Web;

                List srcList = oWebsite.Lists.GetByTitle(Library);//change the library name to your library name
                cc.Load(srcList);
                cc.ExecuteQuery();
                var query = new CamlQuery();
                query.FolderServerRelativeUrl = left + "/" + Path;//change this to the server relative URL of your source folder
                var files = srcList.GetItems(query);
                cc.Load(files);
                cc.ExecuteQuery();
                foreach (var doc in files)
                {
                    if (doc.FileSystemObjectType == FileSystemObjectType.Folder)
                    {
                        if (incluseFromSubfolders)
                        {
                            Shp.Folder folder = doc.Folder;
                            cc.Load(folder);
                            cc.ExecuteQuery();
                            cc.Load(folder.Files);
                            cc.ExecuteQuery();
                            var folderName = folder.Name;

                            foreach (var file in folder.Files)
                            {
                                cc.Load(file);
                                cc.Load(file.ListItemAllFields);
                                cc.ExecuteQuery();
                                var valus = file.ListItemAllFields.FieldValues;
                                filesList.Add(new SharePointFile(valus["ID"].ToString(), folderName + " - " + file.Name, server + file.ServerRelativeUrl, (string)valus["Uploader"], (string)valus["Comments"]));
                            }
                        }
                    }
                    else
                    {
                        Shp.File file = doc.File;
                        cc.Load(file);
                        cc.Load(file.ListItemAllFields);
                        cc.ExecuteQuery();
                        var valus = file.ListItemAllFields.FieldValues;
                        filesList.Add(new SharePointFile(valus["ID"].ToString(), file.Name, server + file.ServerRelativeUrl, (string)valus["Uploader"], (string)valus["Comments"]));
                    }
                }

            }
            catch (Exception) { }
            return filesList.ToArray();
        }

        /// <summary>
        /// Deletes a file from Sharepoint Library
        /// </summary>
        /// <param name="Path">Folder Path to delete the file from. Example - LNC/P01352/SR1</param>
        /// <param name="FileName">File name with extension (as returned from 'getDocs' methods)</param>
        public static void DeleteFile(string Path, string FileName)
        {

            //string incl = Path;

            //SFolder.Lists list = new SFolder.Lists();
            //NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            //list.Credentials = nc;
            //list.Url = "http://phzpdshpdev:9087/sites/IEMQS/_vti_bin/lists.asmx";

            //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            //doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            //System.Xml.XmlNode listQuery = doc.SelectSingleNode("//Query");

            //System.Xml.XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");

            //System.Xml.XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");

            //var SPurl = Path;


            //listQueryOptions.InnerXml = "<Folder>" + SPurl + "</Folder><ViewAttributes Scope=\"Recursive\" />";
            //// Guid g = GetWebID(SPurl);

            //System.Xml.XmlNode items = list.GetListItems(Library, string.Empty, listQuery, listViewFields, string.Empty, listQueryOptions, "");

            //XmlDocument wdoc = new XmlDocument();
            ////DataTable dtbl = new DataTable();

            //string tstx = String.Empty;
            //wdoc.LoadXml(items.InnerXml);
            //int count = 0;
            //string[] strID = new string[2];
            //XmlNodeList oNodes = wdoc.ChildNodes;
            //foreach (XmlNode node in oNodes)
            //{
            //    XmlNodeReader objReader = new XmlNodeReader(node);
            //    while (objReader.Read())
            //    {
            //        if (objReader["ItemCount"] != null)
            //        {
            //            count = Int32.Parse(objReader["ItemCount"].ToString());
            //        }
            //        break;
            //    }
            //}
            //int l = 0;
            //// strID = new string[count]; strTitle = new string[count]; strType = new string[count];
            //Dictionary<string, string> files = new Dictionary<string, string>();
            //XmlNodeList oNodes1 = wdoc.ChildNodes;
            //foreach (XmlNode node in oNodes1)
            //{
            //    XmlNodeReader objReader = new XmlNodeReader(node);
            //    while (objReader.Read())
            //    {
            //        if (objReader["ows_FileLeafRef"] != null)
            //        {
            //            strID = objReader["ows_FileLeafRef"].ToString().Split('#');
            //            if (!files.Any(q => q.Key.Equals(strID[1])))
            //                files.Add(/*"name" + l + "", */strID[1], objReader["ows_ID"].ToString());
            //            /*docs.Add("type" + l + "", objReader["ows_DocIcon"].ToString());
            //            docs.Add("date" + l + "", objReader["ows_Modified"].ToString());
            //            //strID[i] = objReader["ows_ID"].ToString();*/

            //            l++;

            //        }
            //    }
            //}
            var files = (new clsFileUpload()).GetDocuments(Path); //getDocs(Path);

            var fileCount = files.Where(q => q.Name.Equals(FileName)).Count();
            if (fileCount > 0)
            {
                var file = files.Where(q => q.Name.Equals(FileName)).FirstOrDefault();
                DelDoc(file.FileId, HttpUtility.UrlDecode(file.URL));
            }
        }

        /// <summary>
        /// Copy contents from <paramref name="inputFolderPath"/> to <paramref name="outputFolderPath"/> with additional column data.
        /// ** Created date is not copied
        /// </summary>
        /// <param name="inputFolderPath">Folder path to copy From</param>
        /// <param name="outputFolderPath">Folder path to copy To</param>
        public static void CopyFolderContents(string inputFolderPath, string outputFolderPath)
        {
            Uri url = new Uri(FileURL);
            string left = FileURL.Replace(url.GetLeftPart(UriPartial.Authority), "");
            try
            {
                string URL = FileURL.Substring(0, FileURL.LastIndexOf("/"));
                string Lib = FileURL.Substring(FileURL.LastIndexOf("/") + 1);

                ClientContext cc = new ClientContext(URL);
                cc.Credentials = new NetworkCredential(UserID, Password, "lthed");
                Web oWebsite = cc.Web;
                List srcList = oWebsite.Lists.GetByTitle(Library);//change the library name to your library name
                cc.Load(srcList);
                cc.ExecuteQuery();
                var query = new CamlQuery();
                query.FolderServerRelativeUrl = left + "/" + inputFolderPath;//change this to the server relative URL of your source folder
                var files = srcList.GetItems(query);
                cc.Load(files);
                cc.ExecuteQuery();

                string foldername = outputFolderPath;//"@" + lblInsp.Text.Trim();
                                                     //string subFolder = "@" + lblInsp.Text.Trim();
                var dirs = foldername.Split('/');
                for (int i = 0; i < dirs.Count(); i++)
                {
                    string[] prev = new string[i];
                    for (int j = 0; j < i; j++)
                        prev[j] = dirs[j];
                    var root = string.Join("/", prev);
                    createFolder(dirs[i], root);
                }
                foreach (var doc in files)
                {
                    Shp.File file = doc.File;
                    cc.Load(file);
                    cc.ExecuteQuery();
                    String location = left + "/" + outputFolderPath + "/" + file.Name;//change this to the server relative URL of your destination folder
                    file.CopyTo(location, true);
                    cc.ExecuteQuery();
                    /*if (cc.HasPendingRequest)
                        cc.ExecuteQuery();
                    FileInformation fileInfo = Shp.File.OpenBinaryDirect(cc, file.ServerRelativeUrl);
                    Shp.File.SaveBinaryDirect(cc, location, fileInfo.Stream, true);*/
                }
            }
            catch (Exception) { }
        }

        #endregion

        #region PrivateMethods

        private static Stream getStream(string URL, Shp.File file)
        {
            Stream stream = null;
            long bytesToRead = file.Length;
            byte[] buffer = new Byte[bytesToRead];
            try
            {
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(URL + file.ServerRelativeUrl);
                fileReq.Credentials = new NetworkCredential(UserID, Password, "lthed");
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;
                stream = fileResp.GetResponseStream();
            }
            catch (Exception) { return null; }
            return stream;
        }

        private static XmlNode createFolder(string folder, string RootFolder = "")
        {
            bool exist = true;
            XmlNode resultNode = null;
            SFolder.Lists li = new SFolder.Lists();
            li.Url = "http://phzpdshpdev:9087/sites/IEMQS/_vti_bin/lists.asmx";
            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            li.Credentials = nc;
            //string id = ID;
            //fileRef value cannot contain any url encoding characters.
            //string xmlCommand = string.Format("<Method ID='1' Cmd='New'><Field Name='ID'>New</Field><Field Name='FSObjType'>1</Field><Field Name='BaseName'>{1}</Field></Method>", rootFolder, newFolderName);
            //XmlDocument xmlDoc = new System.Xml.XmlDocument();

            //XmlNode ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");

            //XmlNode ndViewFields = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");

            //XmlNode ndQueryOptions = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

            ///*This bit is for checking if the folder exists */

            //ndQueryOptions.InnerXml = "<ViewAttributes Scope='RecursiveAll'/>";

            //ndQuery.InnerXml = "<Where><And><Eq><FieldRef Name='FileLeafRef' /><Value Type='Text'>" + folder + "</Value></Eq><Eq><FieldRef Name='FSObjType'/><Value Type='Text'>1</Value></Eq></And></Where>";

            //XmlNode ndListItems = li.GetListItems("ODMDocuments", null, ndQuery, ndViewFields, null, ndQueryOptions, null);

            //foreach (System.Xml.XmlNode node in ndListItems)
            //{
            //    if (node.Name == "rs:data")
            //    {
            //        if (node.Attributes["ItemCount"].Value == "0")
            //        {
            //            exist = false;
            //        }
            //        else
            //        {
            //            exist = true;
            //            break;
            //        }
            //    }
            //}
            if (exist)
            {
                //fileurl = fileurl + "/@" + userid;
                //folder = "@"+lblInsp.Text.Trim();
                try
                {
                    string xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
                    xml += "<Field Name='BaseName'>" + folder + "</Field>";
                    xml += "</Method>";

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    XmlElement Batch = doc.CreateElement("Batch");
                    Batch.SetAttribute("OnError", "Continue");
                    Batch.SetAttribute("ListVersion", "1");
                    Batch.SetAttribute("ViewName", "");
                    var root = FileURL + ((string.IsNullOrWhiteSpace(RootFolder)) ? "" : "/" + RootFolder);
                    Batch.SetAttribute("RootFolder", root);
                    Batch.InnerXml = xml;
                    // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");elBatch.InnerXml = xml;//XmlNode batchNode = doc.SelectSingleNode("//Batch");
                    resultNode = li.UpdateListItems(Library, Batch);

                }
                catch (Exception) { }
            }
            //else 
            //{
            //    string xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
            //    xml += "<Field Name='BaseName'>" + folder + "</Field>";
            //    xml += "</Method>";

            //    XmlDocument doc = new XmlDocument();
            //    doc.LoadXml(xml);
            //    XmlElement Batch = doc.CreateElement("Batch");
            //    Batch.SetAttribute("OnError", "Continue");
            //    Batch.SetAttribute("ListVersion", "1");
            //    Batch.SetAttribute("ViewName", "");
            //    Batch.SetAttribute("RootFolder", fileurl);
            //    Batch.InnerXml = xml;
            //    // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");elBatch.InnerXml = xml;//XmlNode batchNode = doc.SelectSingleNode("//Batch");
            //    resultNode = li.UpdateListItems("ODMDocuments", Batch);


            //    fileurl = fileurl + "/@" + userid;
            //    folder = "@"+lblInsp.Text.Trim();
            //    xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
            //    xml += "<Field Name='BaseName'>" + folder + "</Field>";
            //    xml += "</Method>";

            //    doc = new XmlDocument();
            //    doc.LoadXml(xml);
            //    Batch = doc.CreateElement("Batch");
            //    Batch.SetAttribute("OnError", "Continue");
            //    Batch.SetAttribute("ListVersion", "1");
            //    Batch.SetAttribute("ViewName", "");
            //    Batch.SetAttribute("RootFolder", fileurl);
            //    Batch.InnerXml = xml;
            //    // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");elBatch.InnerXml = xml;//XmlNode batchNode = doc.SelectSingleNode("//Batch");
            //    resultNode = li.UpdateListItems("ODMDocuments", Batch);

            //}

            return resultNode;

        }

        private static SharePointUploadResult UploadFileToSharePoint(string UploadedFilePath, string[] SharePointPath, byte[] data, string uploader, string comments)
        {
            //WebResponse response = null;
            try
            {

                string filename = Path.GetFileNameWithoutExtension(UploadedFilePath);
                string extn = Path.GetExtension(UploadedFilePath);
                SUpload.Copy cf = new SUpload.Copy();
                NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
                cf.Credentials = nc;
                cf.Url = "http://phzpdshpdev:9087/sites/IEMQS/_vti_bin/copy.asmx";
                SUpload.FieldInformation information_Uploader = new SUpload.FieldInformation
                {
                    DisplayName = "Uploader",
                    InternalName = "Uploader",
                    Type = SUpload.FieldType.Text,
                    Value = uploader,
                };
                SUpload.FieldInformation information_Comments = new SUpload.FieldInformation
                {
                    DisplayName = "Comments",
                    InternalName = "Comments",
                    Type = SUpload.FieldType.Text,
                    Value = ((string.IsNullOrWhiteSpace(comments)) ? "" : comments),
                };

                //FileUploadSharepoint.FieldInformation information1 = new FieldInformation
                //{
                //    DisplayName = "Title",
                //    InternalName = "Title",
                //    Type = FileUploadSharepoint.FieldType.Text,
                //    Value = UploadedFilePath,

                //};
                SUpload.FieldInformation[] info = { information_Uploader, information_Comments };
                SUpload.CopyResult[] res;
                string[] desturls = SharePointPath; // { SharePointPath };
                filename = filename + extn;
                uint copyres = cf.CopyIntoItems(filename, desturls, info, data, out res);
                if (res[0].ErrorCode == SUpload.CopyErrorCode.Success)
                {
                    return SharePointUploadResult.Success;
                }
                else //if (res[0].ErrorCode == SUpload.CopyErrorCode.Unknown)
                {
                    return SharePointUploadResult.Unknown_Error;
                }


            }
            catch (Exception)
            {
                return SharePointUploadResult.Unknown_Error;
            }
        }

        private static XmlNode DelDoc(string DocID, string fileurl)
        {
            SFolder.Lists li = new SFolder.Lists();

            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            li.Credentials = nc;
            li.Url = "http://phzpdshpdev:9087/sites/IEMQS/_vti_bin/lists.asmx";

            string xml = "<Method ID='1' Cmd='Delete'><Field Name='ID'>" + DocID + "</Field >";
            xml += "<Field Name='FileRef'>" + fileurl + "</Field>";
            xml += "</Method>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlElement Batch = doc.CreateElement("Batch");
            Batch.SetAttribute("OnError", "Continue");
            Batch.SetAttribute("ListVersion", "1");
            Batch.SetAttribute("ViewName", "");
            Batch.InnerXml = xml;
            // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");
            // elBatch.InnerXml = xml;
            //XmlNode batchNode = doc.SelectSingleNode("//Batch");
            XmlNode resultNode = li.UpdateListItems(Library, Batch);

            return resultNode;

        }

        #endregion
    }

    /// <summary>
    /// Created by M. Ozair Khatri (90385894)
    /// on 25-07-2017
    /// to upload files on SharePoint
    /// 
    /// Last commit on 28-07-2017 (Added DeleteFile method, File size limitation, Cleaned and Organized class)
    /// </summary>
    public partial class clsUploadKM : clsBase
    {
        #region Variables

        public static string[] allowedExtensions = new string[] { ".jpg", ".jpeg", ".pdf", ".png", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".xlsm" };
        public static float MaxFileSizeInMB = (float)5;
        private static string FileURL = "http://vpwtsheion/IEMQS";
        private static string UserID = "spsadmin";
        private static string Password = "SPS@1234";
        private static string Library = "IEMQS";
        public enum SharePointUploadResult
        {
            Success,
            Invalid_File_Extension,
            File_Size_Too_Big,
            Folder_Creation_Failed,
            Invalid_Parameters,
            Unknown_Error
        }

        #endregion

        #region publicMethods

        /// <summary>
        /// Upload File to SharePoint.
        /// </summary>
        /// <param name="FileName">File Name with extension</param>
        /// <param name="fileBytes">Byte Array of the file content.</param>
        /// <param name="FolderName">Folder Path to save in. Example - LNC/P01352/SR1</param>
        /// <returns>Result Code</returns>
        public static SharePointUploadResult Upload(string FileName, byte[] fileBytes, string FolderName, string Uploader, string Comments)
        {
            try
            {
                XmlNode resultNode = null;
                if (FileName != "")
                {
                    string FOLDER_EXISTS = "0x8107090d";
                    string SUCCESS = "0x00000000";
                    string fname = FileName;

                    string ext = System.IO.Path.GetExtension(fname);
                    //if (ext.ToLower().Equals(".jpg") || ext.ToLower().Equals(".png") || ext.ToLower().Equals(".pdf") || ext.ToLower().Equals(".jpeg") || ext.ToLower().Equals(".docx") || ext.ToLower().Equals(".doc"))
                    if (allowedExtensions.Contains(ext.ToLower()) || true)
                    {
                        if (fileBytes.LongLength <= (MaxFileSizeInMB * 1024 * 1024))
                        {
                            //string foldername = userid;
                            string foldername = FolderName;//"@" + lblInsp.Text.Trim();
                                                           //string subFolder = "@" + lblInsp.Text.Trim();
                            var dirs = foldername.Split('/');
                            for (int i = 0; i < dirs.Count(); i++)
                            {
                                string[] prev = new string[i];
                                for (int j = 0; j < i; j++)
                                    prev[j] = dirs[j];
                                var root = string.Join("/", prev);
                                resultNode = createFolder(dirs[i], root);
                            }

                            if (resultNode != null)
                            {
                                // success
                                if (resultNode.FirstChild.FirstChild.InnerText == SUCCESS || resultNode.FirstChild.FirstChild.InnerText == FOLDER_EXISTS)
                                {
                                    //already existed
                                    try
                                    {
                                        string filename = FileName;
                                        //Stream fStream = file;
                                        //if(fStream.)
                                        byte[] _byteArray = fileBytes;//new byte[fStream.Length];
                                                                      //byte[] btarr = System.IO.File.ReadAllBytes(uploadedFilePath+filename); 
                                                                      //FileStream fs = fStream as FileStream;  

                                        /*fStream.Read(_byteArray, 0, (int)fStream.Length);
                                        fStream.Close();*/
                                        string url = FileURL + "/" + foldername + "/";
                                        return UploadFileToSharePoint(FileName, url + FileName, _byteArray, Uploader, Comments);
                                    }
                                    catch (Exception) { return SharePointUploadResult.Unknown_Error; }
                                }
                                else { return SharePointUploadResult.Unknown_Error; }
                            }
                            else { return SharePointUploadResult.Folder_Creation_Failed; }
                        }
                        else { return SharePointUploadResult.File_Size_Too_Big; }
                    }
                    else { return SharePointUploadResult.Invalid_File_Extension; }
                }
                else { return SharePointUploadResult.Invalid_Parameters; }
            }
            catch (Exception) { return SharePointUploadResult.Invalid_Parameters; }
        }

        /// <summary>
        /// Fetches the files for a given path.
        /// </summary>
        /// <param name="Path">Folder Path to retrieve from. Example - LNC/P01352/SR1</param>
        /// <returns>Dictionary object with Key as File name and Value as the URL that can be used to retrieve it</returns>
        public static SharePointFile[] getDocs(string Path)
        {
            string incl = Path;

            SFolderKM.Lists list = new SFolderKM.Lists();
            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            list.Credentials = nc;
            list.Url = "http://vpwtsheion/_vti_bin/lists.asmx";

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            System.Xml.XmlNode listQuery = doc.SelectSingleNode("//Query");

            System.Xml.XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");

            System.Xml.XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");

            var SPurl = Path;


            listQueryOptions.InnerXml = "<Folder>" + Library + "/" + SPurl + "</Folder><ViewAttributes Scope=\"Recursive\" />";
            // Guid g = GetWebID(SPurl);

            System.Xml.XmlNode items = list.GetListItems(Library, string.Empty, listQuery, listViewFields, string.Empty, listQueryOptions, "");

            XmlDocument wdoc = new XmlDocument();
            //DataTable dtbl = new DataTable();

            string tstx = String.Empty;
            wdoc.LoadXml(items.InnerXml);
            int count = 0;
            string[] strID = new string[2];
            XmlNodeList oNodes = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ItemCount"] != null)
                    {
                        count = Int32.Parse(objReader["ItemCount"].ToString());
                    }
                    break;
                }
            }
            int l = 0;
            // strID = new string[count]; strTitle = new string[count]; strType = new string[count];
            List<SharePointFile> files = new List<SharePointFile>();
            XmlNodeList oNodes1 = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes1)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ows_FileLeafRef"] != null)
                    {
                        strID = objReader["ows_FileLeafRef"].ToString().Split('#');
                        string uploader = (objReader["ows_Uploader"] == null) ? "" : objReader["ows_Uploader"].ToString();
                        string comments = (objReader["ows_Comments"] == null) ? "" : objReader["ows_Comments"].ToString();
                        files.Add(new SharePointFile(objReader["ows_ID"].ToString(), strID[1], objReader["ows_EncodedAbsUrl"], uploader, comments));
                        //files.Add(/*"name" + l + "", */strID[1], FileURL + "/" + Path + "/" + strID[1]);
                        /*docs.Add("type" + l + "", objReader["ows_DocIcon"].ToString());
                        docs.Add("date" + l + "", objReader["ows_Modified"].ToString());
                        //strID[i] = objReader["ows_ID"].ToString();*/

                        l++;

                    }
                }
            }
            return files.ToArray();
        }

        /// <summary>
        /// Deletes a file from Sharepoint Library
        /// </summary>
        /// <param name="Path">Folder Path to delete the file from. Example - LNC/P01352/SR1</param>
        /// <param name="FileName">File name with extension (as returned from 'getDocs' methods)</param>
        public static void DeleteFile(string Path, string FileName)
        {

            /*string incl = Path;

            SFolderKM.Lists list = new SFolderKM.Lists();
            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            list.Credentials = nc;
            list.Url = "http://vpwtsheion/_vti_bin/lists.asmx";

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>");

            System.Xml.XmlNode listQuery = doc.SelectSingleNode("//Query");

            System.Xml.XmlNode listViewFields = doc.SelectSingleNode("//ViewFields");

            System.Xml.XmlNode listQueryOptions = doc.SelectSingleNode("//QueryOptions");

            var SPurl = Path;


            listQueryOptions.InnerXml = "<Folder>" + SPurl + "</Folder><ViewAttributes Scope=\"Recursive\" />";
            // Guid g = GetWebID(SPurl);

            System.Xml.XmlNode items = list.GetListItems(Library, string.Empty, listQuery, listViewFields, string.Empty, listQueryOptions, "");

            XmlDocument wdoc = new XmlDocument();
            //DataTable dtbl = new DataTable();

            string tstx = String.Empty;
            wdoc.LoadXml(items.InnerXml);
            int count = 0;
            string[] strID = new string[2];
            XmlNodeList oNodes = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ItemCount"] != null)
                    {
                        count = Int32.Parse(objReader["ItemCount"].ToString());
                    }
                    break;
                }
            }
            int l = 0;
            // strID = new string[count]; strTitle = new string[count]; strType = new string[count];
            Dictionary<string, string> files = new Dictionary<string, string>();
            XmlNodeList oNodes1 = wdoc.ChildNodes;
            foreach (XmlNode node in oNodes1)
            {
                XmlNodeReader objReader = new XmlNodeReader(node);
                while (objReader.Read())
                {
                    if (objReader["ows_FileLeafRef"] != null)
                    {
                        strID = objReader["ows_FileLeafRef"].ToString().Split('#');
                        if (!files.Any(q => q.Key.Equals(strID[1])))
                            files.Add(/*"name" + l + "", strID[1], objReader["ows_ID"].ToString());
                        /*docs.Add("type" + l + "", objReader["ows_DocIcon"].ToString());
                        docs.Add("date" + l + "", objReader["ows_Modified"].ToString());
                        //strID[i] = objReader["ows_ID"].ToString();

                        l++;

                    }
                }
            }*/
            var files = getDocs(Path);
            var fileCount = files.Where(q => q.Name.Equals(FileName)).Count();
            if (fileCount > 0)
            {
                var file = files.Where(q => q.Name.Equals(FileName)).FirstOrDefault();
                DelDoc(file.FileId, HttpUtility.UrlDecode(file.URL));
            }
        }

        #endregion

        #region PrivateMethods

        private static XmlNode createFolder(string folder, string RootFolder = "")
        {
            bool exist = true;
            XmlNode resultNode = null;
            SFolderKM.Lists li = new SFolderKM.Lists();
            li.Url = "http://vpwtsheion/_vti_bin/lists.asmx";
            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            li.Credentials = nc;
            //string id = ID;
            //fileRef value cannot contain any url encoding characters.
            //string xmlCommand = string.Format("<Method ID='1' Cmd='New'><Field Name='ID'>New</Field><Field Name='FSObjType'>1</Field><Field Name='BaseName'>{1}</Field></Method>", rootFolder, newFolderName);
            //XmlDocument xmlDoc = new System.Xml.XmlDocument();

            //XmlNode ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");

            //XmlNode ndViewFields = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");

            //XmlNode ndQueryOptions = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

            ///*This bit is for checking if the folder exists */

            //ndQueryOptions.InnerXml = "<ViewAttributes Scope='RecursiveAll'/>";

            //ndQuery.InnerXml = "<Where><And><Eq><FieldRef Name='FileLeafRef' /><Value Type='Text'>" + folder + "</Value></Eq><Eq><FieldRef Name='FSObjType'/><Value Type='Text'>1</Value></Eq></And></Where>";

            //XmlNode ndListItems = li.GetListItems("ODMDocuments", null, ndQuery, ndViewFields, null, ndQueryOptions, null);

            //foreach (System.Xml.XmlNode node in ndListItems)
            //{
            //    if (node.Name == "rs:data")
            //    {
            //        if (node.Attributes["ItemCount"].Value == "0")
            //        {
            //            exist = false;
            //        }
            //        else
            //        {
            //            exist = true;
            //            break;
            //        }
            //    }
            //}
            if (exist)
            {
                //fileurl = fileurl + "/@" + userid;
                //folder = "@"+lblInsp.Text.Trim();
                try
                {
                    string xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
                    xml += "<Field Name='BaseName'>" + folder + "</Field>";
                    xml += "</Method>";

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    XmlElement Batch = doc.CreateElement("Batch");
                    Batch.SetAttribute("OnError", "Continue");
                    Batch.SetAttribute("ListVersion", "1");
                    Batch.SetAttribute("ViewName", "");
                    var root = FileURL + ((string.IsNullOrWhiteSpace(RootFolder)) ? "" : "/" + RootFolder);
                    Batch.SetAttribute("RootFolder", root);
                    Batch.InnerXml = xml;
                    // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");elBatch.InnerXml = xml;//XmlNode batchNode = doc.SelectSingleNode("//Batch");
                    resultNode = li.UpdateListItems(Library, Batch);

                }
                catch (Exception) { }
            }
            //else 
            //{
            //    string xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
            //    xml += "<Field Name='BaseName'>" + folder + "</Field>";
            //    xml += "</Method>";

            //    XmlDocument doc = new XmlDocument();
            //    doc.LoadXml(xml);
            //    XmlElement Batch = doc.CreateElement("Batch");
            //    Batch.SetAttribute("OnError", "Continue");
            //    Batch.SetAttribute("ListVersion", "1");
            //    Batch.SetAttribute("ViewName", "");
            //    Batch.SetAttribute("RootFolder", fileurl);
            //    Batch.InnerXml = xml;
            //    // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");elBatch.InnerXml = xml;//XmlNode batchNode = doc.SelectSingleNode("//Batch");
            //    resultNode = li.UpdateListItems("ODMDocuments", Batch);


            //    fileurl = fileurl + "/@" + userid;
            //    folder = "@"+lblInsp.Text.Trim();
            //    xml = "<Method ID='1' Cmd='New'><Field Name='ID'>New</Field ><Field Name='FSObjType'>1</Field>";
            //    xml += "<Field Name='BaseName'>" + folder + "</Field>";
            //    xml += "</Method>";

            //    doc = new XmlDocument();
            //    doc.LoadXml(xml);
            //    Batch = doc.CreateElement("Batch");
            //    Batch.SetAttribute("OnError", "Continue");
            //    Batch.SetAttribute("ListVersion", "1");
            //    Batch.SetAttribute("ViewName", "");
            //    Batch.SetAttribute("RootFolder", fileurl);
            //    Batch.InnerXml = xml;
            //    // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");elBatch.InnerXml = xml;//XmlNode batchNode = doc.SelectSingleNode("//Batch");
            //    resultNode = li.UpdateListItems("ODMDocuments", Batch);

            //}

            return resultNode;

        }

        private static SharePointUploadResult UploadFileToSharePoint(string UploadedFilePath, string SharePointPath, byte[] data, string uploader, string comments)
        {
            //WebResponse response = null;
            try
            {

                string filename = Path.GetFileNameWithoutExtension(UploadedFilePath);
                string extn = Path.GetExtension(UploadedFilePath);
                SUploadKM.Copy cf = new SUploadKM.Copy();
                NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
                cf.Credentials = nc;
                cf.Url = "http://vpwtsheion/_vti_bin/copy.asmx";
                SUploadKM.FieldInformation information_Uploader = new SUploadKM.FieldInformation
                {
                    DisplayName = "Uploader",
                    InternalName = "Uploader",
                    Type = SUploadKM.FieldType.Text,
                    Value = uploader,
                };
                SUploadKM.FieldInformation information_Comments = new SUploadKM.FieldInformation
                {
                    DisplayName = "Comments",
                    InternalName = "Comments",
                    Type = SUploadKM.FieldType.Text,
                    Value = ((string.IsNullOrWhiteSpace(comments)) ? "" : comments),
                };

                //FileUploadSharepoint.FieldInformation information1 = new FieldInformation
                //{
                //    DisplayName = "Title",
                //    InternalName = "Title",
                //    Type = FileUploadSharepoint.FieldType.Text,
                //    Value = UploadedFilePath,

                //};
                SUploadKM.FieldInformation[] info = { information_Uploader, information_Comments };
                SUploadKM.CopyResult[] res;
                string[] desturls = { SharePointPath };
                filename = filename + extn;
                uint copyres = cf.CopyIntoItems(filename, desturls, info, data, out res);
                if (res[0].ErrorCode == SUploadKM.CopyErrorCode.Success)
                {
                    return SharePointUploadResult.Success;
                }
                else //if (res[0].ErrorCode == SUploadKM.CopyErrorCode.Unknown)
                {
                    return SharePointUploadResult.Unknown_Error;
                }


            }
            catch (Exception)
            {
                return SharePointUploadResult.Unknown_Error;
            }
        }

        private static XmlNode DelDoc(string DocID, string fileurl)
        {
            SFolderKM.Lists li = new SFolderKM.Lists();

            NetworkCredential nc = new NetworkCredential(UserID, Password, "lthed");
            li.Credentials = nc;
            li.Url = "http://vpwtsheion/_vti_bin/lists.asmx";

            string xml = "<Method ID='1' Cmd='Delete'><Field Name='ID'>" + DocID + "</Field >";
            xml += "<Field Name='FileRef'>" + fileurl + "</Field>";
            xml += "</Method>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlElement Batch = doc.CreateElement("Batch");
            Batch.SetAttribute("OnError", "Continue");
            Batch.SetAttribute("ListVersion", "1");
            Batch.SetAttribute("ViewName", "");
            Batch.InnerXml = xml;
            // System.Xml.XmlElement elBatch = doc.CreateElement("Batch");
            // elBatch.InnerXml = xml;
            //XmlNode batchNode = doc.SelectSingleNode("//Batch");
            XmlNode resultNode = li.UpdateListItems(Library, Batch);

            return resultNode;

        }

        #endregion
    }

    public class SharePointFile
    {
        public SharePointFile(string Id, string FileName, string URL, string Uploader, string Comments, bool onFTP = false, string Size = "0")
        {
            this.FileId = Id;
            this.Name = FileName;
            this.Size = Size;
            this.URL = URL;
            this.Uploader = Uploader;
            this.Comments = Comments;
            this.onFTP = onFTP;
            this.NameWithoutExt = !string.IsNullOrEmpty(FileName) ? GetFileName(FileName) : "";
            this.FolderPath = "";
        }

        public string FileId { get; private set; }
        public string Name { get; private set; }
        public string Size { get; private set; }
        public string URL { get; private set; }
        public string Uploader { get; private set; }
        public string Comments { get; private set; }
        public bool onFTP { get; private set; }
        public string NameWithoutExt { get; private set; }
        public string FolderPath { get; set; }

        public string GetFileName(string FileName)
        {
            var FileExt = FileName.Split('.').Last();
            return FileName.Substring(0, FileName.Length - (FileExt.Length+1));
        }
    }
}
