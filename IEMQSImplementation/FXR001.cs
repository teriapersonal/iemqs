//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class FXR001
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FXR001()
        {
            this.FXR002 = new HashSet<FXR002>();
        }
    
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public string Customer { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> CDD { get; set; }
        public string Product { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string ReturnRemark { get; set; }
        public string ReviseRemark { get; set; }
        public Nullable<decimal> BudgetedMaterial { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FXR002> FXR002 { get; set; }
    }
}
