//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL096
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Description { get; set; }
        public string ElevationReq { get; set; }
        public string ElevationAct { get; set; }
        public string ProjectionReq { get; set; }
        public string ProjectionAct { get; set; }
        public string OrientationReq { get; set; }
        public string OrientationReqArcLen { get; set; }
        public string OrientationAct { get; set; }
        public string OrientationActArcLen { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual PRL095 PRL095 { get; set; }
    }
}
