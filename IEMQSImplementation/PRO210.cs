//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO210
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO210()
        {
            this.PRO211 = new HashSet<PRO211>();
            this.PRO212 = new HashSet<PRO212>();
            this.PRO213 = new HashSet<PRO213>();
            this.PRO214_2 = new HashSet<PRO214_2>();
            this.PRO214 = new HashSet<PRO214>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string ReqOutOfRoundness { get; set; }
        public string OutByRoundness { get; set; }
        public string OutByActAtTop { get; set; }
        public string OutByActAtBottom { get; set; }
        public string ReqIdCF { get; set; }
        public string ReqOdCF { get; set; }
        public string ReqTotalShellHeight { get; set; }
        public string PeakInOutAllowed { get; set; }
        public string IsEvalueTemplate { get; set; }
        public string ReqChrodLength { get; set; }
        public string ActChordLength { get; set; }
        public string ReqRadius { get; set; }
        public string ActRadius { get; set; }
        public string AllowableGap { get; set; }
        public string ActGap { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public string CheckPoint3 { get; set; }
        public string CheckPoint5 { get; set; }
        public Nullable<System.DateTime> CheckPoint5_2 { get; set; }
        public string CheckPoint6 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string ReqCladStripingDetailsWidth { get; set; }
        public string ReqCladStripingDetailsDepth { get; set; }
        public string ReqCladStripingDetailsOffset { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO211> PRO211 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO212> PRO212 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO213> PRO213 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO214_2> PRO214_2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO214> PRO214 { get; set; }
    }
}
