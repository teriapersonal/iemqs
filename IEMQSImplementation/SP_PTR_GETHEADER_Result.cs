//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_PTR_GETHEADER_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Location { get; set; }
        public string BU { get; set; }
        public string PTRNo { get; set; }
        public string CouponCategory { get; set; }
        public string CouponNo { get; set; }
        public string CreatedBy { get; set; }
        public string EditedBy { get; set; }
        public string Status { get; set; }
        public string QualityProject { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string MechTest { get; set; }
        public string ChemicalTest { get; set; }
        public string LABId { get; set; }
        public Nullable<System.DateTime> LabIdCreatedOn { get; set; }
        public string LabIdCreatedBy { get; set; }
        public string PTRInitiator { get; set; }
    }
}
