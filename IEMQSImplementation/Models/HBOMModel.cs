﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.Models
{
    public class HBOMModel
    {
        public string Project { get; set; }
        public string Part { get; set; }
        public string ParentPart { get; set; }
        public int PartLevel { get; set; }
        public string Description { get; set; }
        public string ItemGroup { get; set; }
        public string RevisionNo { get; set; }
        public string FindNo { get; set; }
        public string MaterialSpecification { get; set; }
        public string MaterialDescription { get; set; }
        public string MaterialCode { get; set; }
        public string ProductType { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public int NoOfPieces { get; set; }
        public double Weight { get; set; }
        public Nullable<double> Thickness { get; set; }
        public string UOM { get; set; }
        public double BOMQty { get; set; }
        public string Element { get; set; }
    }
}
