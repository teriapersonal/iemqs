﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation
{
    class CustomClass
    {
    }

    public partial class clsPTR020
    {
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string TestRemarks { get; set; }
        public string LeakPathExamination1 { get; set; }
        public string LeakPathExamination2 { get; set; }
        public string AxialLength1 { get; set; }
        public string AxialLength2 { get; set; }
        public string MaxPenetrationOnTube { get; set; }
        public string PhotoHeading1 { get; set; }
        public string PhotoHeading2 { get; set; }
        public string Result { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public List<PTR020_Log> PTR020_Log {get;set;}
        public List<PTR020_Log2> PTR020_Log2 {get;set;}
        public List<PTR020_Log3> PTR020_Log3 { get; set; }
    }

    public class clsImpactDetailPara
    {
        public int CouponId { get; set; }
        //public int RowNo { get; set; }
        public string CouponTitle { get; set; }
        public List<PTR012_Log> lstPTR {get; set; }
    }
    public class clsImpactDetailParaTR
    {
        public int RowId { get; set; }
        public PTR012_Log PTR { get; set; }
    }
    public class clsMicroSkectchPara
    {
        public string ImgPath {get;set;}
        public int Sr {get;set;}
        public int HeaderId { get; set; }
        public string ImgTitle { get; set; }
        public string formId { get; set; }
    }

    public class clsPTR023
    {
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string Length { get; set; }
        public string ActualInsideDiameter { get; set; }
        public string RatioOfActualIDToSpecifiedOD { get; set; }
        public string MinReqExpansionOfTubeID { get; set; }
        public string TestRemarks { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public List<PTR023_Log> PTR023_Log { get; set; }
    }
    public class clsPTR022
    {
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string SpecifiedWallThickness { get; set; }
        public string SpecifiedOutsideDiameter { get; set; }
        public string ConstantValue { get; set; }
        public string DistanceBet2Flattening { get; set; }
        public string FirstStep { get; set; }
        public string FirstStepAcceptance { get; set; }
        public string SecondStep { get; set; }
        public string SecondStepAcceptance { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Length { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public List<PTR022_Log> PTR022_Log { get; set; }
    }
    public class clsPTR018
    {
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string SampleId { get; set; }
        public string TestRemarks { get; set; }
        public string PhotoHeading { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public List<PTR018_Log> PTR018_Log { get; set; }
    }
    public class clsPTR019
    {
        public int HeaderId { get; set; }
        public string ReportName { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string SampleId { get; set; }
        public string TestRemarks { get; set; }
        public string IsInclusionRating { get; set; }
        public string ThinA { get; set; }
        public string ThinB { get; set; }
        public string ThinC { get; set; }
        public string ThinD { get; set; }
        public string ThickA { get; set; }
        public string ThickB { get; set; }
        public string ThickC { get; set; }
        public string ThickD { get; set; }
        public string PhotoHeading1 { get; set; }
        public string PhotoHeading2 { get; set; }
        public string PhotoHeading3 { get; set; }
        public string PhotoHeading4 { get; set; }
        public string PhotoHeading5 { get; set; }
        public string PhotoHeading6 { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string PhotoHeading7 { get; set; }
        public string PhotoHeading8 { get; set; }
        public string PhotoHeading9 { get; set; }
        public string PhotoHeading10 { get; set; }
        public string PhotoHeading11 { get; set; }
        public string PhotoHeading12 { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public List<PTR019_Log> PTR019_Log { get; set; }
    }
    public class clsPTR016
    {
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string HardnessScale { get; set; }
        public string RequiredValue { get; set; }
        public string Result { get; set; }
        public string SampleId { get; set; }
        public string SketchName1 { get; set; }
        public string SketchName2 { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Notes2 { get; set; }
        public string Notes3 { get; set; }
        public string Notes4 { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public List<PTR016_Log> PTR016_Log { get; set; }
    }
}
