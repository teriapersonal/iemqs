//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO475
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO475()
        {
            this.PRO476 = new HashSet<PRO476>();
            this.PRO477 = new HashSet<PRO477>();
            this.PRO478 = new HashSet<PRO478>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string ReqActualValue1 { get; set; }
        public string ReqActualValue2 { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public string CheckPoint4_1_1 { get; set; }
        public Nullable<System.DateTime> CheckPoint4_1_2 { get; set; }
        public string CheckPoint4_2_1 { get; set; }
        public Nullable<System.DateTime> CheckPoint4_2_2 { get; set; }
        public string CheckPoint4_3_1 { get; set; }
        public Nullable<System.DateTime> CheckPoint4_3_2 { get; set; }
        public string CheckPoint4_4_1 { get; set; }
        public Nullable<System.DateTime> CheckPoint4_4_2 { get; set; }
        public string CheckPoint4_5_1 { get; set; }
        public Nullable<System.DateTime> CheckPoint4_5_2 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO476> PRO476 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO477> PRO477 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO478> PRO478 { get; set; }
    }
}
