//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QCP001_Log
    {
        public int Id { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public int QCPRev { get; set; }
        public string DocumentNo { get; set; }
        public string Contract { get; set; }
        public string Consultant { get; set; }
        public string ASME { get; set; }
        public string Specification { get; set; }
        public string Code { get; set; }
        public string AbbreviationCode { get; set; }
        public string AbbreviationDesc { get; set; }
        public string Status { get; set; }
        public string TP1 { get; set; }
        public string TP2 { get; set; }
        public string TP3 { get; set; }
        public string TP4 { get; set; }
        public string TP5 { get; set; }
        public string TP6 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string Attachment { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
    }
}
