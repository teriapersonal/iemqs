//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_GET_EMPLOYEE_ROLE_DESCRIPTION_LIST_Result
    {
        public string Employee { get; set; }
        public string EmployeeNameRole { get; set; }
    }
}
