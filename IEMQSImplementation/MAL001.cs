//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class MAL001
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MAL001()
        {
            this.MAL002 = new HashSet<MAL002>();
            this.MAL003 = new HashSet<MAL003>();
            this.MAL004_2 = new HashSet<MAL004_2>();
            this.MAL004_3 = new HashSet<MAL004_3>();
            this.MAL004 = new HashSet<MAL004>();
        }
    
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string DocumentNo { get; set; }
        public string ProcedureNo { get; set; }
        public string CoatingContractorName { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string ReportNo { get; set; }
        public string ItemPreparedOrCoated { get; set; }
        public string MaxRelativeHumidity { get; set; }
        public string MinDiffSteelDewPointTemp { get; set; }
        public string MaxDryBulbTemp { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL002> MAL002 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL003> MAL003 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL004_2> MAL004_2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL004_3> MAL004_3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAL004> MAL004 { get; set; }
    }
}
