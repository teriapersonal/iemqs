//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_FKMS_FR_GET_LN_ALLOCATION_DETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string Project { get; set; }
        public Nullable<int> FXRSrNo { get; set; }
        public Nullable<int> RefLineId { get; set; }
        public int RefId { get; set; }
        public string FixtureNo { get; set; }
        public int LineId { get; set; }
        public string FindNo { get; set; }
        public string ItemCode { get; set; }
        public string ItemCategory { get; set; }
        public string DescriptionofItem { get; set; }
        public string ItemType { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> PLTQty { get; set; }
        public Nullable<decimal> NPLTQty { get; set; }
        public string ErrorMsg { get; set; }
        public Nullable<int> PLTId { get; set; }
        public Nullable<int> NPLTId { get; set; }
    }
}
