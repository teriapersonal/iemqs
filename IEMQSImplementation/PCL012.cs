//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCL012
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public bool JPPReq { get; set; }
        public Nullable<System.DateTime> JPPReqDate { get; set; }
        public string JPPDocNo { get; set; }
        public Nullable<System.DateTime> JPPReleasedOn { get; set; }
        public bool BoxPlotReq { get; set; }
        public Nullable<System.DateTime> BoxPlotReqDate { get; set; }
        public string BoxPlotDocNo { get; set; }
        public Nullable<System.DateTime> BoxPlotReleasedOn { get; set; }
        public bool RollingCapaReq { get; set; }
        public Nullable<System.DateTime> RollingCapaReqDate { get; set; }
        public string RollingCapaDocNo { get; set; }
        public Nullable<System.DateTime> RollingCapaReleasedOn { get; set; }
        public bool ImprovBudgetReq { get; set; }
        public Nullable<System.DateTime> ImprovBudgetReqDate { get; set; }
        public string ImprovBudgetDocNo { get; set; }
        public Nullable<System.DateTime> ImprovBudgetReleasedOn { get; set; }
        public bool RCCPBreakUpReq { get; set; }
        public Nullable<System.DateTime> RCCPBreakUpReqDate { get; set; }
        public string RCCPBreakUpDocNo { get; set; }
        public Nullable<System.DateTime> RCCPBreakUpReleasedOn { get; set; }
        public bool SchedConcertoReq { get; set; }
        public Nullable<System.DateTime> SchedConcertoReqDate { get; set; }
        public string SchedConcertoDocNo { get; set; }
        public Nullable<System.DateTime> SchedConcertoReleasedOn { get; set; }
        public bool SchedExlReq { get; set; }
        public Nullable<System.DateTime> SchedExlReqDate { get; set; }
        public string SchedExlDocNo { get; set; }
        public Nullable<System.DateTime> SchedExlReleasedOn { get; set; }
        public bool SubcontPlanReq { get; set; }
        public Nullable<System.DateTime> SubcontPlanReqDate { get; set; }
        public string SubcontPlanDocNo { get; set; }
        public Nullable<System.DateTime> SubcontPlanReleasedOn { get; set; }
        public bool LongSeamReq { get; set; }
        public Nullable<System.DateTime> LongSeamReqDate { get; set; }
        public string LongSeamDocNo { get; set; }
        public Nullable<System.DateTime> LongSeamReleasedOn { get; set; }
        public bool CircSeamReq { get; set; }
        public Nullable<System.DateTime> CircSeamReqDate { get; set; }
        public string CircSeamDocNo { get; set; }
        public Nullable<System.DateTime> CircSeamReleasedOn { get; set; }
        public bool LineFabReq { get; set; }
        public Nullable<System.DateTime> LineFabReqDate { get; set; }
        public string LineFabDocNo { get; set; }
        public Nullable<System.DateTime> LineFabReleasedOn { get; set; }
        public bool NozzleShellReq { get; set; }
        public Nullable<System.DateTime> NozzleShellReqDate { get; set; }
        public string NozzleShellDocNo { get; set; }
        public Nullable<System.DateTime> NozzleShellReleasedOn { get; set; }
        public bool RollCapaReq { get; set; }
        public Nullable<System.DateTime> RollCapaReqDate { get; set; }
        public string RollCapaDocNo { get; set; }
        public Nullable<System.DateTime> RollCapaReleasedOn { get; set; }
        public bool TablesNozReq { get; set; }
        public Nullable<System.DateTime> TablesNozReqDate { get; set; }
        public string TablesNozDocNo { get; set; }
        public Nullable<System.DateTime> TablesNozReleasedOn { get; set; }
        public bool TableIntReq { get; set; }
        public Nullable<System.DateTime> TableIntReqDate { get; set; }
        public string TableIntDocNo { get; set; }
        public Nullable<System.DateTime> TableIntReleasedOn { get; set; }
        public bool TableExtReq { get; set; }
        public Nullable<System.DateTime> TableExtReqDate { get; set; }
        public string TableExtDocNo { get; set; }
        public Nullable<System.DateTime> TableExtReleasedOn { get; set; }
        public bool HTREdgeReq { get; set; }
        public Nullable<System.DateTime> HTREdgeReqDate { get; set; }
        public string HTREdgeDocNo { get; set; }
        public Nullable<System.DateTime> HTREdgeReleasedOn { get; set; }
        public bool HTRRollReq { get; set; }
        public Nullable<System.DateTime> HTRRollReqDate { get; set; }
        public string HTRRollDocNo { get; set; }
        public Nullable<System.DateTime> HTRRollReleasedOn { get; set; }
        public bool HTRReRollReq { get; set; }
        public Nullable<System.DateTime> HTRReRollReqDate { get; set; }
        public string HTRReRollDocNo { get; set; }
        public Nullable<System.DateTime> HTRReRollReleasedOn { get; set; }
        public bool RefLineSketchReq { get; set; }
        public Nullable<System.DateTime> RefLineSketchReqDate { get; set; }
        public string RefLineSketchDocNo { get; set; }
        public Nullable<System.DateTime> RefLineSketchReleasedOn { get; set; }
        public bool HTRSecPWHTReq { get; set; }
        public Nullable<System.DateTime> HTRSecPWHTReqDate { get; set; }
        public string HTRSecPWHTDocNo { get; set; }
        public Nullable<System.DateTime> HTRSecPWHTReleasedOn { get; set; }
        public bool HTRClosingReq { get; set; }
        public Nullable<System.DateTime> HTRClosingReqDate { get; set; }
        public string HTRClosingDocNo { get; set; }
        public Nullable<System.DateTime> HTRClosingReleasedOn { get; set; }
        public bool HTRNozzReq { get; set; }
        public Nullable<System.DateTime> HTRNozzReqDate { get; set; }
        public string HTRNozzDocNo { get; set; }
        public Nullable<System.DateTime> HTRNozzReleasedOn { get; set; }
        public bool HTRPWHTReq { get; set; }
        public Nullable<System.DateTime> HTRPWHTReqDate { get; set; }
        public string HTRPWHTDocNo { get; set; }
        public Nullable<System.DateTime> HTRPWHTReleasedOn { get; set; }
        public bool HTRLSRReq { get; set; }
        public Nullable<System.DateTime> HTRLSRReqDate { get; set; }
        public string HTRLSRDocNo { get; set; }
        public Nullable<System.DateTime> HTRLSRReleasedOn { get; set; }
        public bool HandlingPlanReq { get; set; }
        public Nullable<System.DateTime> HandlingPlanReqDate { get; set; }
        public string HandlingPlanDocNo { get; set; }
        public Nullable<System.DateTime> HandlingPlanReleasedOn { get; set; }
        public bool LocationTankRotatorReq { get; set; }
        public Nullable<System.DateTime> LocationTankRotatorReqDate { get; set; }
        public string LocationTankRotatorDocNo { get; set; }
        public Nullable<System.DateTime> LocationTankRotatorReleasedOn { get; set; }
        public bool HydrotestReq { get; set; }
        public Nullable<System.DateTime> HydrotestReqDate { get; set; }
        public string HydrotestDocNo { get; set; }
        public Nullable<System.DateTime> HydrotestReleasedOn { get; set; }
        public bool IntFabReq { get; set; }
        public Nullable<System.DateTime> IntFabReqDate { get; set; }
        public string IntFabDocNo { get; set; }
        public Nullable<System.DateTime> IntFabReleasedOn { get; set; }
        public bool BaffleTubeReq { get; set; }
        public Nullable<System.DateTime> BaffleTubeReqDate { get; set; }
        public string BaffleTubeDocNo { get; set; }
        public Nullable<System.DateTime> BaffleTubeReleasedOn { get; set; }
        public bool SurfaceTreatRequestReq { get; set; }
        public Nullable<System.DateTime> SurfaceTreatRequestReqDate { get; set; }
        public string SurfaceTreatRequestDocNo { get; set; }
        public Nullable<System.DateTime> SurfaceTreatRequestReleasedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
    
        public virtual PCL001 PCL001 { get; set; }
    }
}
