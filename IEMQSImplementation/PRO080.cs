//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO080
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO080()
        {
            this.PRO081 = new HashSet<PRO081>();
            this.PRO082 = new HashSet<PRO082>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string Elevationtol { get; set; }
        public string Orientationtol { get; set; }
        public string Projectiontol { get; set; }
        public string Holetol { get; set; }
        public string Remark11 { get; set; }
        public string Remark13 { get; set; }
        public string Remark14 { get; set; }
        public string Remark15 { get; set; }
        public string Remark16 { get; set; }
        public string Remark17 { get; set; }
        public string FLATNESSREADINGtol { get; set; }
        public string Remark21 { get; set; }
        public string Remark23 { get; set; }
        public string Remark24 { get; set; }
        public string Remark25 { get; set; }
        public string Remark26 { get; set; }
        public string Remark27 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<System.DateTime> Remark15_1 { get; set; }
        public Nullable<System.DateTime> Remark25_1 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO081> PRO081 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO082> PRO082 { get; set; }
    }
}
