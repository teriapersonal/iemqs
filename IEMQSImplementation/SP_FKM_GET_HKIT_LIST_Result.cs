//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_FKM_GET_HKIT_LIST_Result
    {
        public string ChildPart { get; set; }
        public string CHildDescription { get; set; }
        public string CHildFindNo { get; set; }
        public double ChildBOMQty { get; set; }
        public string ChildProductType { get; set; }
        public string ParentPart { get; set; }
        public string ParentDescription { get; set; }
        public string ParentFindNo { get; set; }
        public Nullable<double> ParentBOMQty { get; set; }
        public string ParentElement { get; set; }
        public string Hierarchy { get; set; }
    }
}
