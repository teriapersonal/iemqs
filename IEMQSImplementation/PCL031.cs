//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCL031
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public Nullable<double> ShellAllwEB1 { get; set; }
        public Nullable<double> ShellAllwEB2 { get; set; }
        public Nullable<double> ShrinkageCirc { get; set; }
        public Nullable<double> ShringageLong { get; set; }
        public Nullable<double> Mcinglong { get; set; }
        public Nullable<double> Mcingcirc { get; set; }
        public Nullable<double> NegAllow { get; set; }
        public Nullable<double> TOvality { get; set; }
        public Nullable<double> TCircum { get; set; }
        public Nullable<double> DEndMcing { get; set; }
        public Nullable<double> DEndSeam { get; set; }
        public Nullable<double> DEndCirc { get; set; }
        public Nullable<double> DEndESSC { get; set; }
        public Nullable<double> SLTolFlat { get; set; }
        public Nullable<double> SLTolEleva { get; set; }
        public Nullable<double> SLTolHole { get; set; }
        public Nullable<double> CTRGapTop { get; set; }
        public Nullable<double> CTRGapInverted { get; set; }
        public Nullable<double> CTRAllSet { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
    
        public virtual PCL001 PCL001 { get; set; }
    }
}
