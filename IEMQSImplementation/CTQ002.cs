//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class CTQ002
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public Nullable<int> LineRev { get; set; }
        public string Status { get; set; }
        public int SrNo { get; set; }
        public string Specification { get; set; }
        public string CSRMOMDoc { get; set; }
        public string ClauseNo { get; set; }
        public string CTQDesc { get; set; }
        public string Department { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string SendToCompiledBy { get; set; }
        public Nullable<System.DateTime> SendToCompiledOn { get; set; }
        public string CompiledBy { get; set; }
        public Nullable<System.DateTime> CompiledOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string ReturnRemark { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public int CTQRev { get; set; }
        public string ActionDesc { get; set; }
        public string ActionBy { get; set; }
        public Nullable<bool> SendToCompiler { get; set; }
    
        public virtual CTQ001 CTQ001 { get; set; }
    }
}
