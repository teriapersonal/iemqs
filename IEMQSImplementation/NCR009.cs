//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class NCR009
    {
        public int Id { get; set; }
        public string NCRNo { get; set; }
        public string ProjectNumber { get; set; }
        public string EquipmentNumber { get; set; }
        public string DeviationFailure { get; set; }
        public string Customer { get; set; }
        public string CustomerLocation { get; set; }
        public string EndCustomer { get; set; }
        public string PMC { get; set; }
        public string Supplier { get; set; }
        public string SupplierLocation { get; set; }
        public string ProductName { get; set; }
        public string MaterialSpec { get; set; }
        public string MaterialGrade { get; set; }
        public string MaterialThickness { get; set; }
        public string ItemName { get; set; }
        public string SubAssembly { get; set; }
        public string NDEMethod { get; set; }
        public string OperatorName { get; set; }
        public string WelderName { get; set; }
        public string WeldingType { get; set; }
        public string WeldingConsumable { get; set; }
        public string WeldingConsumableSupplier { get; set; }
        public string ShopName { get; set; }
        public string YardName { get; set; }
        public string DepartmentName { get; set; }
        public string NatureOfWeldingDefect { get; set; }
        public string NatureOfMaterialDefec { get; set; }
        public string TPIMaterialInspection { get; set; }
        public string TPIProductInspection { get; set; }
        public string PTCFailures { get; set; }
        public string SubContractorName { get; set; }
        public string HeatTreatment { get; set; }
        public string Machining { get; set; }
        public string Forming { get; set; }
        public string Fastener { get; set; }
        public string YearOfDispatch { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    }
}
