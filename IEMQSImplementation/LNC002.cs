//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class LNC002
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string DocumentNo { get; set; }
        public int SrNo { get; set; }
        public string Activity { get; set; }
        public string JobNo { get; set; }
        public string Learning { get; set; }
        public string CorrectiveAction { get; set; }
        public string RootCause { get; set; }
        public string PreventiveAction { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    
        public virtual LNC001 LNC001 { get; set; }
    }
}
