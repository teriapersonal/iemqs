//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO350
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO350()
        {
            this.PRO351 = new HashSet<PRO351>();
            this.PRO352 = new HashSet<PRO352>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string ProcedureNo { get; set; }
        public string TestDescription { get; set; }
        public Nullable<System.DateTime> DateOfTest { get; set; }
        public string TestPosition { get; set; }
        public string TestMedium { get; set; }
        public string ReqChlorideContentOfWater { get; set; }
        public string ActChlorideContentOfWater { get; set; }
        public string ReqTestPressure { get; set; }
        public string ActTestPressure { get; set; }
        public string ReqHoldingTime { get; set; }
        public string ActHoldingTime { get; set; }
        public string ReqTestTemprature { get; set; }
        public string ActTestTemprature { get; set; }
        public string OutesideCircumferenceInMM { get; set; }
        public string Graph { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO351> PRO351 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO352> PRO352 { get; set; }
    }
}
