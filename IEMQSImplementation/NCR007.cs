//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class NCR007
    {
        public int Id { get; set; }
        public int NCRId { get; set; }
        public string StageFrom { get; set; }
        public string StageTo { get; set; }
    
        public virtual NCR001 NCR001 { get; set; }
    }
}
