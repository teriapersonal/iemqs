//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL195
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRL195()
        {
            this.PRL196 = new HashSet<PRL196>();
            this.PRL197 = new HashSet<PRL197>();
            this.PRL198 = new HashSet<PRL198>();
            this.PRL199_2 = new HashSet<PRL199_2>();
            this.PRL199_3 = new HashSet<PRL199_3>();
            this.PRL199_4 = new HashSet<PRL199_4>();
            this.PRL199_5 = new HashSet<PRL199_5>();
            this.PRL199_6 = new HashSet<PRL199_6>();
            this.PRL199 = new HashSet<PRL199>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string QualityProject { get; set; }
        public string EquipmentNo { get; set; }
        public string DrawingNo { get; set; }
        public Nullable<int> DrawingRevisionNo { get; set; }
        public string DCRNo { get; set; }
        public Nullable<System.DateTime> InspectionDate { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public Nullable<int> RequestNoSequence { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public string InspectionAgency { get; set; }
        public Nullable<System.DateTime> OfferDate { get; set; }
        public Nullable<int> ICLRevNo { get; set; }
        public string ReqInsideCircumference { get; set; }
        public string ActInsideCircumference { get; set; }
        public string RemInsideCircumference { get; set; }
        public string ReqTotalHeight { get; set; }
        public string ActTotalHeight { get; set; }
        public string RemTotalHeight { get; set; }
        public string ReqStraightFace { get; set; }
        public string ActStraightFace { get; set; }
        public string RemStraightFace { get; set; }
        public string ReqCrownRadius { get; set; }
        public string ActCrownRadius { get; set; }
        public string RemCrownRadius { get; set; }
        public string ReqKnuckleRadius { get; set; }
        public string ActKnuckleRadius { get; set; }
        public string RemKnuckleRadius { get; set; }
        public string ReqGapAtKnuckleAreaUsingTemplate { get; set; }
        public string ActGapAtKnuckleAreaUsingTemplate { get; set; }
        public string RemGapAtKnuckleAreaUsingTemplate { get; set; }
        public string ActGapAtKnuckleAreaUsingTemplateMax { get; set; }
        public string RemGapAtKnuckleAreaUsingTemplateMax { get; set; }
        public string ReqOverCrowing { get; set; }
        public string ActOverCrowing { get; set; }
        public string RemOverCrowing { get; set; }
        public string ReqUnderCrowing { get; set; }
        public string ActUnderCrowing { get; set; }
        public string RemUnderCrowing { get; set; }
        public string ReqConcentricity { get; set; }
        public string ActConcentricity { get; set; }
        public string RemConcentricity { get; set; }
        public string ReqTypeOfForming { get; set; }
        public string ActTypeOfForming { get; set; }
        public string RemTypeOfForming { get; set; }
        public string Ovality { get; set; }
        public string OutBy { get; set; }
        public string ReqThiknessMin { get; set; }
        public string ReqThiknessNom { get; set; }
        public string EvalueTemplate { get; set; }
        public string ReqChordLength { get; set; }
        public string ActChordLength { get; set; }
        public string ReqRadius { get; set; }
        public string ActRadius { get; set; }
        public string GapAllowable { get; set; }
        public string ActGap { get; set; }
        public string ReqRootGap { get; set; }
        public string ReqRootFace { get; set; }
        public string Pickinout { get; set; }
        public string OffSet { get; set; }
        public string InsideWepAngle { get; set; }
        public string OutsideWepAngle { get; set; }
        public string DetailThiknessReport { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public string CheckPoint3 { get; set; }
        public string CheckPoint4 { get; set; }
        public string CheckPoint5 { get; set; }
        public string CheckPoint6 { get; set; }
        public string CheckPoint6_2 { get; set; }
        public string CheckPoint6_3 { get; set; }
        public string CheckPoint7 { get; set; }
        public string CheckPoint7_2 { get; set; }
        public string CheckPoint7_3 { get; set; }
        public string CheckPoint7_4 { get; set; }
        public string CheckPoint9 { get; set; }
        public Nullable<System.DateTime> CheckPoint9_2 { get; set; }
        public string CheckPoint10 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string ReqCladStripingWidth { get; set; }
        public string ReqCladStripingDepth { get; set; }
        public string ReqCladStripingOffset { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public string IdentificationOfCleats { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReqFilledBy { get; set; }
        public Nullable<System.DateTime> ReqFilledOn { get; set; }
        public string ActFilledBy { get; set; }
        public Nullable<System.DateTime> ActFilledOn { get; set; }
        public string InspectedBy { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL196> PRL196 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL197> PRL197 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL198> PRL198 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL199_2> PRL199_2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL199_3> PRL199_3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL199_4> PRL199_4 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL199_5> PRL199_5 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL199_6> PRL199_6 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL199> PRL199 { get; set; }
    }
}
