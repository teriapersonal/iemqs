//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QCP003
    {
        public int SubLineId { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public int QCPRev { get; set; }
        public Nullable<int> LineSrNo { get; set; }
        public Nullable<int> SubLineSrNo { get; set; }
        public int SubLineRev { get; set; }
        public string Activity { get; set; }
        public string ReferenceDoc { get; set; }
        public string Characteristics { get; set; }
        public string AcceptanceCriteria { get; set; }
        public string VerifyingDoc { get; set; }
        public string HeaderNotes { get; set; }
        public string FooterNotes { get; set; }
        public string RevisionDesc { get; set; }
        public string InitiatorRemark { get; set; }
        public string ReturnRemark { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReturnedBy { get; set; }
        public Nullable<System.DateTime> ReturnedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string TPIIntervention { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string TPIIntervention2 { get; set; }
        public string TPIIntervention3 { get; set; }
        public string TPIIntervention4 { get; set; }
        public string TPIIntervention5 { get; set; }
        public string TPIIntervention6 { get; set; }
    
        public virtual QCP002 QCP002 { get; set; }
        public virtual QCP001 QCP001 { get; set; }
    }
}
