//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class JPP006
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string ProcurementMaterialItem { get; set; }
        public string SizeQuantity { get; set; }
        public Nullable<System.DateTime> RequiredDatetomeetCDD { get; set; }
        public string PONodate { get; set; }
        public string Vendor { get; set; }
        public Nullable<System.DateTime> ExpectedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> SeqNo { get; set; }
    
        public virtual JPP001 JPP001 { get; set; }
    }
}
