﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace IEMQSImplementation
{
    /// <summary>
    /// Enum Class created by Dharmesh 24-06-2017 
    /// </summary>

    #region Implementation Enum
    public static class clsImplementationEnum
    {
        #region ADD
        public enum ADDStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted For Review")]
            SubmittedForReview,
            [StringValue("Submitted For Approval")]
            SubmittedForApproval,
            [StringValue("Returned By Reviewer")]
            ReturnByReviewer,
            [StringValue("Returned By Assignee")]
            ReturnByAssignee,
            [StringValue("Approved")]
            Approved
        }
        public enum ADDTaskStatus
        {

            [StringValue("Submitted For Approval")]
            SubmittedForApproval,
            [StringValue("Returned")]
            Return,
            [StringValue("Approved")]
            Approved
        }

        public enum ADDIndexTitle
        {
            [StringValue("Maintain ADD")]
            maintainADD,
            [StringValue("Review ADD")]
            ReviewADD,
        }
        public enum ADDIndexType
        {
            //index type & index title
            [StringValue("m")]
            maintain,
            [StringValue("r")]
            review,
            //form action
            [StringValue("a")]
            approve,
        }
        #endregion

        #region RCA
        public enum RCAStatus
        {
            [StringValue("Submit To RP")]
            SubmittoRP,
            [StringValue("Submit To Initiator")]
            SubmittoInitiator,
            [StringValue("Completed")]
            Completed,
        }
        #endregion

        #region CFAR
        public enum CFARStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted For Validation")]
            SubmittedForValidate,
            [StringValue("Submitted For Invalidation")]
            SubmittedForInvalidation,
            [StringValue("Submitted For Assign")]
            SubmittedForAssign,
            [StringValue("Submitted For Approval")]
            SubmittedForApproval,
            [StringValue("Submitted For Implementation")]
            SubmittedForImplementation,
            [StringValue("Submitted For Closure")]
            SubmittedForClosure,
            [StringValue("Submitted For Customer Closure")]
            SubmittedForCustomerClosure,
            [StringValue("Submitted For Closure Approval")]
            SubmittedForClosureApproval,
            [StringValue("Closed")]
            Closed,

            [StringValue("Returned By MKT2/Validator")]
            Returned_By_Validator,
            [StringValue("Returned By PMG2/Assigner")]
            Returned_By_Assigner,
            [StringValue("Returned By QC2/Approver")]
            Returned_By_Approver,
            [StringValue("Returned By PMG2/Implementer")]
            Returned_By_Implementer,
            [StringValue("Returned By QC2/Closer")]
            Returned_By_Closer,
            [StringValue("Returned By QI1/Customer")]
            Returned_By_Customer,
            [StringValue("Returned By QA2/Approver")]
            Returned_By_QA_Approver
        }

        public enum CFARMailTemplate
        {
            [StringValue("Creator")]
            Creater,
            [StringValue("Marketing Manager")]
            Marketing_Manager,
            [StringValue("Project Manager")]
            Project_Manager,
            [StringValue("PMG Manager")]
            PMG_Manager,
            [StringValue("QC Manager")]
            QC_Manager,
            [StringValue("QC Head")]
            QC_Head,
            [StringValue("QA Manager")]
            QA_Manager,


            [StringValue("Validate")]
            Validate,
            [StringValue("Assign")]
            Assign,
            [StringValue("Approve")]
            Approve,
            [StringValue("Implement")]
            Implement,
            [StringValue("QC Closer")]
            QC_Closer,
            [StringValue("Closure with Customer")]
            Closure_with_Customer,
            [StringValue("QA Approval")]
            QA_Approval,
            [StringValue("Release")]
            Released,
            [StringValue("Create")]
            Create
        }

        public enum CFARIndexType
        {
            //index type & index title
            [StringValue("maintain")]
            maintain,
            [StringValue("validate")]
            validate,
            [StringValue("assign")]
            assign,
            [StringValue("approve")]
            approve,
            [StringValue("implement")]
            implement,
            [StringValue("closure")]
            closure,
            [StringValue("customerclosure")]
            customerclosure,
            [StringValue("closureapproval")]
            closureapproval,
            [StringValue("invalidate")]
            invalidate,
        }
        public enum CFARActionType
        {
            //index type & index title
            [StringValue("Created")]
            Created,
            [StringValue("Submitted")]
            Submitted,
            [StringValue("Validated")]
            Validated,
            [StringValue("Assigned")]
            Assigned,
            [StringValue("Approved")]
            Approved,
            [StringValue("Implemented")]
            Implemented,
            [StringValue("QC Closed")]
            QC_Closed,
            [StringValue("Customer Closed")]
            Customer_Closed,
            [StringValue("Closed")]
            Closed,
            [StringValue("Invalidated")]
            Invalidated,
            [StringValue("Returned By Validator")]
            Returned_By_Validator,
            [StringValue("Returned By Assigner")]
            Returned_By_Assigner,
            [StringValue("Returned By Approver")]
            Returned_By_Approver,
            [StringValue("Returned By Implementer")]
            Returned_By_Implementer,
            [StringValue("Returned By Closer")]
            Returned_By_Closer,
            [StringValue("Returned By Customer")]
            Returned_By_Customer,
            [StringValue("Returned By QA Approver")]
            Returned_By_QA_Approver
        }
        public enum CFARIndexTitle
        {
            //index type & index title
            [StringValue("Maintain CFAR")]
            maintainCFAR,
            [StringValue("Validate CFAR")]
            ValidateCFAR,
            [StringValue("Invalidate CFAR")]
            InvalidateCFAR,
            [StringValue("Assign CFAR")]
            AssignCFAR,
            [StringValue("Approve CFAR")]
            ApproveCFAR,
            [StringValue("Implement CFAR")]
            ImplementCFAR,
            [StringValue("Closure CFAR")]
            ClosueCFAR,
            [StringValue("Customer Closure CFAR")]
            CustomerClosueCFAR,
            [StringValue("Closure Approval CFAR")]
            ClosueApprovalCFAR,
        }

        public enum MIPIndexTitle
        {
            //index type & index title
            [StringValue("Maintain MIP Master")]
            maintainMIP,
            [StringValue("Approve MIP Master")]
            ApproveMIP
        }

        public enum MIPHeaderIndexTitle
        {
            //index type & index title
            [StringValue("Maintain MIP")]
            maintainMIP,
            [StringValue("Approve MIP")]
            ApproveMIP,
            [StringValue("Generate EMR")]
            GenerateEMR
        }
        public enum MIPIndexType
        {
            //index type & index title
            [StringValue("m")]
            maintain,
            [StringValue("a")]
            approve,
        }

        public enum CFARCategory
        {
            [StringValue("Transportation Damage")]
            Create,
            [StringValue("Surface Treatment")]
            Surface_Treatment,
            [StringValue("Painting")]
            Painting,
            [StringValue("Short Supplies")]
            Short_Supplies,
            [StringValue("Others")]
            Others,
            [StringValue("Leakage")]
            Leakage,
            [StringValue("Inadequate Performance")]
            Inadequate_Performance,
            [StringValue("Documentation")]
            Documentation,
            [StringValue("Dimensions")]
            Dimensions,
            [StringValue("Cracks")]
            Cracks,
            [StringValue("Cleanliness")]
            Cleanliness,
            [StringValue("Assembly")]
            Assembly,
            [StringValue("Aesthetics")]
            Aesthetics,
            [StringValue("Faulty material")]
            Faulty_materialy,
            [StringValue("Design deficiency")]
            Design_deficiency,
            [StringValue("Workmanship")]
            Workmanship,
        }

        #endregion

        #region MCR
        public enum MCRStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Approved")]
            Approved,
        }
        public enum MCRIndexType
        {
            //index type & index title
            [StringValue("maintain")]
            maintain,
            [StringValue("assign")]
            assign,
            [StringValue("approve")]
            approve,
        }
        public enum MCRIndexTitle
        {
            //index type & index title
            [StringValue("Maintain MCR")]
            maintainMCR,
            [StringValue("Validate MCR")]
            ValidateMCR,
            [StringValue("Assign MCR")]
            AssignMCR,
            [StringValue("Approve MCR")]
            ApproveMCR
        }

        public static List<string> getAlignment()

        {
            List<string> items = GetListOfDescription<ProtocolAlignment>();
            return items;
        }
        public enum ProtocolAlignment
        {
            [StringValue("SURFACE AGIGNMENT")]
            SURFACEAGIGNMENT,
            [StringValue("AXIALALGNMENT")]
            AXIALALGNMENT

        }

        public enum MCRPartCategory
        {
            //index type & index title
            [StringValue("PP")]
            PP,
            [StringValue("APP")]
            APP,
            [StringValue("NPP")]
            NPP
        }
        public static List<string> getMCRPartCategories()
        {
            List<string> items = GetListOfDescription<MCRPartCategory>();
            return items;
        }
        #endregion

        #region CSRStatus
        public enum CSRStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for DL Approval")]
            SendForDLApproval,
            [StringValue("Sent for PMG Approval")]
            SendForPMGApproval,
            [StringValue("Sent for Inactive PMG Approval")]
            SendForInactivePMGApproval,
            [StringValue("Approved Inactive by PMG")]
            ApprovedInactiveByPMG,
            [StringValue("Reject Inactive by PMG")]
            RejectInactiveByPMG,
            [StringValue("Approved by PMG")]
            ApprovedByPMG,
            [StringValue("Approved by Design Lead")]
            ApprovedByDL,
            [StringValue("Returned by Design Lead")]
            ReturnedByDL,
            [StringValue("Returned by PMG")]
            ReturnedByPMG,
            [StringValue("Superseded")]
            Superseded
        }
        #endregion

        #region CTQ
        public enum CTQStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Compile")]
            SendForCompile,
            [StringValue("Sent for Approval")]
            SendForApprovel,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }

        #endregion

        #region FDMSReport
        public enum FDMSReport
        {
            [StringValue("Detail")]
            Detail,
            [StringValue("DPP")]
            DPP,
            [StringValue("Summary")]
            Summary,
        }

        #endregion

        #region IMSReport
        public enum IMSReport
        {
            [StringValue("Instrument")]
            Instrument,
            [StringValue("Issued")]
            Issued,
            [StringValue("Available")]
            Available,
        }

        #endregion

        #region LockingCleat

        public enum CommonStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApprovel,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Accepted By Design")]
            AcceptedByDesign,
            [StringValue("Accepted By QC")]
            AcceptedByQC,
            [StringValue("Rejected")]
            Reject,
            [StringValue("Active")]
            Active,
            [StringValue("Inactive")]
            Inactive,
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified
        }
        public enum LockingCleatMaterial
        {
            [StringValue("IS-2062 Gr.B")]
            IS2062GrB,
            [StringValue("SA516 Gr 70")]
            SA516Gr70,
            [StringValue("SA387 Gr 11 Cl2")]
            SA387Gr11Cl2,
            [StringValue("A524 Type D CL4a")]
            A524TypeDCL4a
        }
        public enum TackWeldMaterial
        {
            [StringValue("IS-2062 Gr. B")]
            IS2062GrB,
            [StringValue("SA-516 Gr. 70")]
            SA516Gr70,
            [StringValue("SA-387 Gr.11 Cl. 2")]
            SA387Gr11Cl2,
            [StringValue("SA-542M Type D Cl. 4a")]
            SA542MTypeDCl4a,
            [StringValue("SA-240 Type 347")]
            SA240Type347
        }
        public enum TackWeldYieldStrength
        {
            [StringValue("240")]
            YS240,
            [StringValue("260")]
            YS260,
            [StringValue("310")]
            YS310,
            [StringValue("414")]
            YS414,
            [StringValue("205")]
            YS205
        }


        public enum YieldStrength
        {
            [StringValue("240")]
            YS240,
            [StringValue("262")]
            YS262,
            [StringValue("310")]
            YS310,
            [StringValue("414")]
            YS414
        }


        public enum LockingCleatStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        public enum AutoCJCStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Submitted")]
            Submitted,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Closed")]
            Closed,
            [StringValue("Pending with A/cs")]
            Pending_with_A_cs,
            [StringValue("Pending with Authoriser")]
            Pending_with_Authoriser,
            [StringValue("Pending with Certifier")]
            Pending_with_Certifier,
            [StringValue("Pending with Initiator")]
            Pending_with_Initiator,
            [StringValue("Returned by A/cs")]
            Returned_by_A_cs,
            [StringValue("Returned by Authoriser")]
            Returned_by_Authoriser,
            [StringValue("Returned by Certifier")]
            Returned_by_Certifier,
            [StringValue("Rejected")]
            Rejected,
        }
        public enum AutoCJCProjectActivity
        {
            [StringValue("PA028")]
            Contingency_amount
        }
        public enum AutoCJCSeamType
        {
            [StringValue("LW")]
            LW,
            [StringValue("CW")]
            CW,
            [StringValue("AW")]
            AW,
            [StringValue("PW")]
            PW,
            [StringValue("NP")]
            NP,
            [StringValue("CR")]
            CR,
            [StringValue("NW")]
            NW,
            [StringValue("BU")]
            BU,
            [StringValue("HF")]
            HF,
            [StringValue("OW")]
            OW,
            [StringValue("RA")]
            RA,
            [StringValue("HX")]
            HX,
            [StringValue("PTCLW")]
            PTCLW,
            [StringValue("PTCCW")]
            PTCCW,
            [StringValue("PTCOW")]
            PTCOW,
            [StringValue("PTCBW")]
            PTCBW
        }
        #endregion

        #region
        public enum AutoCJCSeamCategory
        {
            [StringValue("SC001")]
            SC001,
            [StringValue("SC002")]
            SC002,
            [StringValue("SC003")]
            SC003,
            [StringValue("SC004")]
            SC004,
            [StringValue("SC005")]
            SC005,
            [StringValue("SC006")]
            SC006,
            [StringValue("SC007")]
            SC007,
            [StringValue("SC008")]
            SC008,
            [StringValue("SC009")]
            SC009,
            [StringValue("SC010")]
            SC010,
            [StringValue("SC011")]
            SC011,
            [StringValue("SC012")]
            SC012,
            [StringValue("SC013")]
            SC013,
            [StringValue("SC014")]
            SC014,
            [StringValue("SC015")]
            SC015,
            [StringValue("SC016")]
            SC016,
            [StringValue("SC018")]
            SC018,
            [StringValue("SC017")]
            SC017,
            [StringValue("SC029")]
            SC029,
            [StringValue("SC030")]
            SC030,
            [StringValue("SC019")]
            SC019,
            [StringValue("SC020")]
            SC020,
            [StringValue("SC021")]
            SC021,
            [StringValue("SC031")]
            SC031,
            [StringValue("SC022")]
            SC022,
            [StringValue("SC023")]
            SC023,
            [StringValue("SC024")]
            SC024,
            [StringValue("SC025")]
            SC025,
            [StringValue("SC026")]
            SC026,
            [StringValue("SC027")]
            SC027,
            [StringValue("SC028")]
            SC028,
            [StringValue("SC032")]
            SC032,
            [StringValue("SC033")]
            SC033,
            [StringValue("SC034")]
            SC034,
            [StringValue("SC035")]
            SC035,
            [StringValue("SC036")]
            SC036,
            [StringValue("SC037")]
            SC037,
            [StringValue("SC038")]
            SC038,
            [StringValue("SC039")]
            SC039,
            [StringValue("SC040")]
            SC040
        }
        #endregion

        #region Scan UT
        public enum ScanUTR1Changes
        {
            [StringValue("Cellulose + Water")]
            Cellulose_Water,
            [StringValue("Ref. + 06 dB")]
            Ref_06_dB,

        }
        public enum TOFDUTR1Changes
        {
            [StringValue("Cellulose + Water")]
            Cellulose_Water,
            [StringValue("Semi-Automated")]
            Semi_Automated,
            [StringValue("Co-Axial")]
            Co_Axial,
            [StringValue("5")]
            CL5,
            //PAut
            [StringValue("2")]
            CableLength2,
        }
        #endregion

        #region Planning Check List 
        public enum PLCStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApprovel,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }

        public enum WPPStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SendForApprovel,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Released")]
            Released
        }
        public enum WPPIndexType
        {
            //index type & index title
            [StringValue("m")]
            maintain,
            [StringValue("a")]
            approve,
            [StringValue("r")]
            release,
            [StringValue("rf")]
            releaseFKM,
        }
        #endregion
        #region IPI report ddropdown
        public enum UTTypes
        {
            [StringValue("A Scan UT")]
            UT_ASCAN,
            [StringValue("TOFD UT")]
            UT_TOFD,
            [StringValue("PA UT")]
            UT_PAUT,

        }
        public static string convertUTTypes(string ut)
        {
            string result = "";
            switch (ut)
            {
                case "A Scan UT":
                    result = "UT/ASCAN";
                    break;
                case "TOFD UT":
                    result = "UT/TOFD";
                    break;
                case "PA UT":
                    result = "UT/PAUT";
                    break;

            }
            return result;
        }

        #endregion
        #region TestPlan
        public enum TestPlanStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified
        }

        #endregion

        #region ICL Seam
        public enum ICLSeamStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified
        }
        #endregion

        #region ICL PART
        public enum ICLPartStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified
        }

        #endregion

        #region User Role
        public enum UserRoleName
        {
            [StringValue("AUX1")]
            AUX1,
            [StringValue("AUX2")]
            AUX2,
            [StringValue("EST1")]
            EST1,
            [StringValue("EST2")]
            EST2,
            [StringValue("EST3")]
            EST3,
            [StringValue("FKM1")]
            FKM1,
            [StringValue("FKM2")]
            FKM2,
            [StringValue("FKM3")]
            FKM3,
            [StringValue("FMG1")]
            FMG1,
            [StringValue("FMG2")]
            FMG2,
            [StringValue("FMG3")]
            FMG3,
            [StringValue("LOG1")]
            LOG1,
            [StringValue("LOG2")]
            LOG2,
            [StringValue("MATL1")]
            MATL1,
            [StringValue("MATL2")]
            MATL2,
            [StringValue("MCC2")]
            MCC2,
            [StringValue("MCC3")]
            MCC3,
            [StringValue("MET2")]
            MET2,
            [StringValue("MET3")]
            MET3,
            [StringValue("MKT1")]
            MKT1,
            [StringValue("MKT2")]
            MKT2,
            [StringValue("MKT3")]
            MKT3,
            [StringValue("MTN1")]
            MTN1,
            [StringValue("MTN2")]
            MTN2,
            [StringValue("OS1")]
            OS1,
            [StringValue("OS2")]
            OS2,
            [StringValue("PMG1")]
            PMG1,
            [StringValue("PMG2")]
            PMG2,
            [StringValue("PMG3")]
            PMG3,
            [StringValue("PROD3")]
            PROD3,
            [StringValue("PS1")]
            PS1,
            [StringValue("PS2")]
            PS2,
            [StringValue("PUR1")]
            PUR1,
            [StringValue("PUR2")]
            PUR2,
            [StringValue("QA2")]
            QA2,
            [StringValue("QA3")]
            QA3,
            [StringValue("QC2")]
            QC2,
            [StringValue("QC3")]
            QC3,
            [StringValue("QI2")]
            QI2,
            [StringValue("QI3")]
            QI3,
            [StringValue("SCC3")]
            SCC3,
            [StringValue("SDE")]
            SDE,
            [StringValue("SHOP")]
            SHOP,
            [StringValue("STR1")]
            STR1,
            [StringValue("STR2")]
            STR2,
            [StringValue("SUB1")]
            SUB1,
            [StringValue("SUB2")]
            SUB2,
            [StringValue("SUB3")]
            SUB3,
            [StringValue("TOC2")]
            TOC2,
            [StringValue("WE1")]
            WE1,
            [StringValue("WE2")]
            WE2,
            [StringValue("WE3")]
            WE3,
            [StringValue("WQTC PR")]
            WQTCPR,
            [StringValue("WTC3")]
            WTC3,
            [StringValue("FUR1")]
            FUR1,
            [StringValue("CR1")]
            CR1,
            [StringValue("CR2")]
            CR2,
            [StringValue("ENGG1")]
            ENGG1,
            [StringValue("ENGG2")]
            ENGG2,
            [StringValue("ENGG3")]
            ENGG3,
            [StringValue("FS3")]
            FS3,
            [StringValue("ITA1")]
            ITA1,
            [StringValue("ITA2")]
            ITA2,
            [StringValue("ITA3")]
            ITA3,
            [StringValue("MFG1")]
            MFG1,
            [StringValue("MFG2")]
            MFG2,
            [StringValue("MFG3")]
            MFG3,
            [StringValue("MFGK")]
            MFGK,
            [StringValue("NDE1")]
            NDE1,
            [StringValue("NDE2")]
            NDE2,
            [StringValue("NDE3")]
            NDE3,
            [StringValue("PLNG1")]
            PLNG1,
            [StringValue("PLNG2")]
            PLNG2,
            [StringValue("PLNG3")]
            PLNG3,
            [StringValue("PUR3")]
            PUR3,
            [StringValue("QI1")]
            QI1,
            [StringValue("TPI1")]
            TPI1,
            [StringValue("AUX3")]
            AUX3,
            [StringValue("ENGG4")]
            ENGG4,
            [StringValue("IT1")]
            IT1,
            [StringValue("IT2")]
            IT2,
            [StringValue("IT3")]
            IT3,
            [StringValue("LAB1")]
            LAB1,
            [StringValue("LAB2")]
            LAB2,
            [StringValue("LAB3")]
            LAB3,
            [StringValue("LOG3")]
            LOG3,
            [StringValue("MATL3")]
            MATL3,
            [StringValue("MCPLNG1")]
            MCPLNG1,
            [StringValue("MCPLNG2")]
            MCPLNG2,
            [StringValue("MCPLNG3")]
            MCPLNG3,
            [StringValue("MET1")]
            MET1,
            [StringValue("MTN3")]
            MTN3,
            [StringValue("OE1")]
            OE1,
            [StringValue("OE2")]
            OE2,
            [StringValue("OE3")]
            OE3,
            [StringValue("OS3")]
            OS3,
            [StringValue("PPMD1")]
            PPMD1,
            [StringValue("PPMD2")]
            PPMD2,
            [StringValue("PPMD3")]
            PPMD3,
            [StringValue("QA1")]
            QA1,
            [StringValue("STR3")]
            STR3,
            [StringValue("TRG1")]
            TRG1,
            [StringValue("TRG2")]
            TRG2,
            [StringValue("TRG3")]
            TRG3,
            //start fdms roles
            [StringValue("ResponsiblePerson")]
            ResponsiblePerson,
            [StringValue("Originator")]
            Originator,
            [StringValue("MFG")]
            MFG,
            [StringValue("PE1")]
            PE1,
            [StringValue("PE2")]
            PE2,
            [StringValue("PE3")]
            PE3,
            [StringValue("MDM1")]
            MDM1,
            [StringValue("MDM2")]
            MDM2,
            [StringValue("MDM3")]
            MDM3,
            [StringValue("PURK")]
            PURK,
            //end
        }

        public enum UserAccessRole
        {
            [StringValue("Initiator")]
            Initiator,
            [StringValue("Approver")]
            Approver,
            [StringValue("Releaser")]
            Releaser
        }

        public enum UserActionType
        {
            [StringValue("Login")]
            Login,
            [StringValue("Login As Other User")]
            LoginAs,
            [StringValue("Add")]
            Add,
            [StringValue("Update")]
            Update,
            [StringValue("Delete")]
            Delete
        }

        #endregion

        #region Configuration Parameters
        public enum ConfigParameter
        {
            [StringValue("WebsiteURL")]
            WebsiteURL,
            [StringValue("LNLinkedServer")]
            LNLinkedServer,
            [StringValue("LNCompanyId")]
            LNCompanyId,
            [StringValue("EmailProfile")]
            EmailProfile,
            [StringValue("AskWelderRequiredForStageTypeOnRegenerateNDERequest")]
            AskWelderRequiredForStageTypeOnRegenerateNDERequest,
            [StringValue("NotInGroupOffer")]
            NotInGroupOffer,
            [StringValue("AllowSeamCategoryGroupOffer")]
            AllowSeamCategoryGroupOffer,
            [StringValue("SpecialStageCodeForRTO")]
            SpecialStageCodeForRTO,
            [StringValue("Announcement")]
            Announcement,
            [StringValue("WavedUserName")]
            WavedUserName,
            [StringValue("PTMTForRepairSpots")]
            PTMTForRepairSpots,
            [StringValue("PendingCFAREmailTo")]
            PendingCFAREmailTo,
            [StringValue("PendingCFAREmailCC")]
            PendingCFAREmailCC,
            [StringValue("Maintenance")]
            Maintenance,
            [StringValue("NonPressurePartSeam")]
            NonPressurePartSeam,
            [StringValue("ReinforcementNotApplicableFor")]
            ReinforcementNotApplicableFor,
            [StringValue("InsertTPStagesintoTPI")]
            InsertTPStagesintoTPI,
            [StringValue("StageTypeExludedinOfferDropdown")]
            StageTypeExludedinOfferDropdown,
            [StringValue("RoletoEnableEditForPMI")]
            RoletoEnableEditForPMI,
            [StringValue("RCA_Insert_URL")]
            RCA_Insert_URL,
            [StringValue("RCA_ViewStatus_URL")]
            RCA_ViewStatus_URL,
            [StringValue("RCA_URL")]
            RCA_URL,
            [StringValue("SkipPLM")]
            SkipPLM,
            [StringValue("RCAConnectionstring")]
            RCAConnectionstring,
            [StringValue("AllowRedirectLastURL")]
            AllowRedirectLastURL
            , [StringValue("MTRConnectionString")]
            MTRConnectionString
            , [StringValue("MTRLocationQuery")]
            MTRLocationQuery,
            [StringValue("IsRCAApplicable")]
            IsRCAApplicable,
            [StringValue("CallPLMServiceForPartAndBOMCreate")]
            CallPLMServiceForPartAndBOMCreate,
            [StringValue("PowerBIReportURL")]
            PowerBIReportURL,
            [StringValue("LNServiceTimeout")]
            LNServiceTimeout,
            [StringValue("ExternalImagePath")]
            ExternalImagePath,
            [StringValue("IsDESLive")]
            IsDESLive
        }

        #endregion

        #region PTMTCTQStatus
        public enum PTMTCTQStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded

        }
        #endregion

        #region RTCTQStatus
        public enum RTCTQStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        #endregion

        #region RTCTQStatus
        public enum PlanStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        #endregion


        #region Plan List

        public enum PlanList
        {
            [StringValue("Tank Rotator Plan")]
            Tank_Rotator_Plan,
            [StringValue("Section Handling Plan")]
            Section_Handling_Plan,
            [StringValue("Stool Plan")]
            Stool_Plan,
            [StringValue("Positioner Plan")]
            Positioner_Plan,
            [StringValue("Internal Axel Movement Plan")]
            Internal_Axel_Movement_Plan,
            [StringValue("Equipment Handling Plan")]
            Equipment_Handling_Plan,
            [StringValue("Equipment Jacking Plan")]
            Equipment_Jacking_Plan,
            [StringValue("Air Test Plan")]
            Air_Test_Plan,
            [StringValue("Hydro Test Plan")]
            Hydro_Test_Plan,
            [StringValue("Reference Sketch")]
            Reference_Sketch,
            [StringValue("Locking Cleat")]
            Locking_Cleat,
            [StringValue("Initiate Exit Meeting")]
            Initiate_Exit_Meeting,
            [StringValue("Machining Scope of Work")]
            Machining_Scope_of_Work,
            [StringValue("Heat Treatment Charge Sheet")]
            Heat_Treatment_Charge_Sheet,
            [StringValue("Sub Contracting Plan")]
            Sub_Contracting_Plan,
            [StringValue("Technical Procedure")]
            Technical_Process,
            [StringValue("Planning Checklist")]
            Planning_Checklist,
            [StringValue("Fixture")]
            Fixture,
            [StringValue("Improvement Budget Plan")]
            Improvement_Budget_Plan,
            [StringValue("Pre Manufacturing")]
            Pre_Manufacturing,
            [StringValue("Weld KG Plan")]
            Weld_KG_Plan,
            [StringValue("Learning Capture")]
            Learning_Capture,
            [StringValue("Job Planning & Progress Sheet & Line Sketch")]
            JPP,
            [StringValue("Shell C/S WEP Machining / Trimming Plan")]
            Machining_Instructions,//mci
            [StringValue("Operation Cards")]
            Operation_Cards//opc
            //Section Handling Plan
            , [StringValue("RCCP Breakup For Equipment")]
            RCCP_Breakup_For_Equipment
            , [StringValue("Dimension Data Capturing Plan")]
            Dimension_Data_Capturing_Plan
            , [StringValue("Elbow Overlay Plan")]
            Elbow_Overlay_Plan
            , [StringValue("Top & Bottom Spool Trimming Plan")]
            Top_And_Bottom_Spool_Trimming_Plan
        }

        #endregion

        #region Mom

        public enum Mom
        {
            [StringValue("Project Review Meeting")]
            Project_Review_Meeting,

            //Section Handling Plan
        }

        #endregion

        #region DEN
        public enum YesNoIP
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
            [StringValue("IP")]
            IP
        }
        #endregion

        #region RTCTQStatus
        public enum UTCTQStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Sent for Approval")]
            SendForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        #endregion

        #region ARM Status
        public enum ARMStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Approved")]
            APPROVED,
            [StringValue("Returned")]
            RETURNED,
            [StringValue("Returned By Approver")]
            RETURNED_BY_APPROVER,
            [StringValue("Returned By Reviewer")]
            RETURNED_BY_REVIEWER,
            [StringValue("Submitted")]
            SUBMITTED,
            [StringValue("Send for Approver")]
            SEND_FOR_APPROVER,
            [StringValue("Send for Reviewer")]
            SEND_FOR_REVIEWER
        }
        #endregion

        #region ARM Type
        public enum ARMType
        {
            [StringValue("ARM")]
            ARM,
            [StringValue("Procurement Drawing")]
            PROCUREMENT_DRAWING,
            [StringValue("ARM Code")]
            ARM_CODE
        }
        #endregion

        #region Grid Type(Export Excel)
        public enum GridType
        {
            [StringValue("Header")]
            HEADER,
            [StringValue("Lines")]
            LINES,
            [StringValue("Route")]
            ROUTE,
            [StringValue("RouteHistory")]
            ROUTE_HISTORY,
            [StringValue("Sub Lines")]
            SUB_LINES,
            [StringValue("PAM Request Header")]
            PAM_REQ_HEADER,
            [StringValue("HistoryView")]
            HISTORYVIEW,
            [StringValue("HistoryHeader")]
            HISTORYHEADER,
            [StringValue("HistoryLines")]
            HISTORYLINES,
            [StringValue("HistorySub Lines")]
            HISTORY_SUB_LINES,
            [StringValue("MaintainIndex")]
            MAINTAININDEX,
            [StringValue("ApproveIndex")]
            APPROVEINDEX,
            [StringValue("Cylindrical Shell")]
            WI_CYLINDRICALSHEET,
            [StringValue("Cone")]
            WI_CONE,
            [StringValue("Dished End")]
            WI_DISHED_END,
            [StringValue("Nozzles")]
            WI_NOZZLES,
            [StringValue("Tube Sheet")]
            WI_TUBESHEET,
            [StringValue("Gitth Flanges")]
            WI_GIRTH_FLANGES,
            [StringValue("Tubes")]
            WI_TUBES,
            [StringValue("Other Components")]
            WI_OTHER_COMPONENTS,
            [StringValue("SSS Req Header")]
            SSS_REQ_HEADER,
            [StringValue("HTTest")]
            HTTest,
            [StringValue("MechTest")]
            MechTest,
            [StringValue("ChemicalTest")]
            ChemicalTest,
            [StringValue("HeatPlateTest")]
            HeatPlateTest,
            [StringValue("TestCompletion")]
            TestCompletion,
            [StringValue("Re Offered")]
            ReOffered,
            [StringValue("Tack Weld")]
            tackWeld,
            [StringValue("locking cleat")]
            lockingcleat,
            [StringValue("history locking cleat")]
            historylockingcleat,
            [StringValue("history Tack Weld")]
            historytackWeld,
            [StringValue("FDMS verification Lines")]
            FDMSverificationLines,
            [StringValue("FDMS Assignee Lines")]
            FDMSAssigneeLines,
            [StringValue("Profile Projects")]
            ProfileProjects,
            [StringValue("Profile Contracts")]
            ProfileContracts,
            [StringValue("AssemblyLinkage")]
            AssemblyLinkage,
            [StringValue("DashboardHTRDPP")]
            DashboardHTRDPP,
            [StringValue("MajorMaterialConstructionLine")]
            MajorMaterialConstructionLine,
            [StringValue("SpecificCustomerRequirementLine")]
            SpecificCustomerRequirementLine,
            [StringValue("PaymentTermsLines")]
            PaymentTermsLines,
            [StringValue("DocumentsDrawingDescriptionLines")]
            DocumentsDrawingDescriptionLines,
            [StringValue("ProcurementMaterialItemLines")]
            ProcurementMaterialItemLines,
            [StringValue("SubcontractingPlanLines")]
            SubcontractingPlanLines,
            [StringValue("SpecialAssemblyLines")]
            SpecialAssemblyLines,
            [StringValue("DiscussionPointsLines")]
            DiscussionPointsLines,
            [StringValue("ShopInChargeHeader")]
            ShopInChargeHeader,
            [StringValue("WEHeader")]
            WEHeader,
            [StringValue("WQTHeader")]
            WQTHeader,
            [StringValue("WPQCertificate")]
            WPQCertificate,
            [StringValue("WOPQCertificate")]
            WOPQCertificate,
            [StringValue("TTSCertificate")]
            TTSCertificate,
            //for ims grid
            [StringValue("TestArea")]
            TestArea,
            [StringValue("MaintainInstrumentIssue")]
            MaintainInstrumentIssue,
            [StringValue("RecallNotification")]
            RecallNotification,
            [StringValue("ProcessInstrumentIssue")]
            ProcessInstrumentIssue,
            [StringValue("ProcessIssueRequest")]
            ProcessIssueRequest,
            [StringValue("ReturnInstrumentIssue")]
            ReturnInstrumentIssue,
            [StringValue("MaintainReturnInstrumentIssue")]
            MaintainReturnInstrumentIssue,
            [StringValue("MaintainCalibration")]
            MaintainCalibration,
            [StringValue("InstrumentIssueLine")]
            InstrumentIssueLine,
            [StringValue("InstrumentCalibrationLine")]
            InstrumentCalibrationLine,
            [StringValue("IPIDashboardPieChartData")]
            IPIDashboardPieChartData,
            [StringValue("IPIDashboardSeamGridData")]
            IPIDashboardSeamGridData,
            [StringValue("ProfileProcess")]
            ProfileProcess,
            [StringValue("ProcessWiseRoles")]
            ProcessWiseRoles,
            [StringValue("ProcessRoleEmployee")]
            ProcessRoleEmployee,
            [StringValue("EmployeeWiseRoles")]
            EmployeeWiseRoles,
            [StringValue("ParentChildDepartment")]
            ParentChildDepartment,
            [StringValue("UserChildDepartment")]
            UserChildDepartment,
            [StringValue("GetAllRole")]
            GetAllRole,
            [StringValue("WPSKey")]
            WPSKey,
            //ICC
            [StringValue("GetItemUpdateCreated")]
            GetItemUpdateCreated,
            [StringValue("GetItemUpdateResquest")]
            GetItemUpdateResquest,
            [StringValue("GetItemNotCreated")]
            GetItemNotCreated,
            [StringValue("GetItemUpdateApproval")]
            GetItemUpdateApproval,
            [StringValue("GetAllItem")]
            GetAllItem,
            [StringValue("GetItemGroupMaster")]
            GetItemGroupMaster,
            [StringValue("GetCOPData")]
            GetCOPData
            , [StringValue("GeneratePCR")]
            GeneratePCR
            , [StringValue("StockMaster")]
            StockMaster,
            [StringValue("GeneratePCL")]
            GeneratePCL,
            [StringValue("QAPCL")]
            QAPCL
            , [StringValue("LabDPP")]
            LabDPP

        }
        #endregion

        #region QCP
        public enum QCPStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified
        }
        #endregion

        #region NDE Technique
        public enum NDETechniqueStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }

        //Basic Calibration Block
        public enum NDEBasicCalibrationBlock
        {
            [StringValue("IIW V1 Block")]
            IIWV1Block,
            [StringValue("IIW V2 Block")]
            IIWV2Block,
            //[StringValue("(default)IIW V1 Block")]
            //Mandatory
        }

        public enum Platecuttinglocation
        {
            [StringValue("CNC West")]
            CNCWest,
            [StringValue("East")]
            East,
            [StringValue("Waterjet")]
            Waterjet,
            [StringValue("Outside")]
            Outside
        }

        public enum FabricationType
        {
            [StringValue("Assembly")]
            Assembly,
            [StringValue("Component")]
            Component
        }

        public enum DGSDiagram
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No
        }
        public enum pclProductType
        {
            [StringValue("Rolled Forged Check List")]
            RolledForgedCheckList,
            [StringValue("Urea Rx Check List")]
            UreaRxCheckList,
            //[StringValue("Heater Surface Condenser")]
            //HeaterSurfaceCondenser,
            [StringValue("Coke Drum")]
            CokeDrum,
            [StringValue("Column")]
            Column,
            [StringValue("EO Reactor")]
            EOReactor,
            [StringValue("RR Package")]
            RRPackage,
            [StringValue("Heater")]
            Heater,
            [StringValue("Surface Condenser")]
            SurfaceCondenser,
            [StringValue("Heat Exchanger")]
            HeatExchanger,
            [StringValue("Boiler")]
            Boiler
        }

        public enum NDEEncoderType
        {
            [StringValue("Manual")]
            Manual,
            [StringValue("Automatic")]
            Automatic,
            [StringValue("Semi-Automatic")]
            SemiAutomatic
        }
        public enum NDEScannerType
        {
            [StringValue("Manual")]
            Manual,
            [StringValue("Automatic")]
            Automatic,
            [StringValue("Semi-Automatic")]
            SemiAutomatic,

            [StringValue("Non-Automated")]
            NonAutomatedScanner,
            [StringValue("Semi-Automated")]
            SemiAutomatedScanner,
            [StringValue("Automated")]
            AutomatedScanner

        }
        public enum NDEScanType
        {
            [StringValue("OFFSET")]
            OFFSET,
            [StringValue("D-SCAN")]
            DSCAN,
            [StringValue("TR. D-SCAN")]
            TRDSCAN,
            [StringValue("B-SCAN")]
            BSCAN,
            [StringValue("TR. B-SCAN")]
            TRBSCAN,

        }
        #endregion

        #region IMB
        public enum IMBStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        public enum IMBLineStatus
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
            [StringValue("Short Closed")]
            Short_Closed
        }
        public enum TeamFormation
        {
            [StringValue("Unnati")]
            Unnati,
            [StringValue("Navneet")]
            Navneet,
            [StringValue("Task Force")]
            TaskForce,
            [StringValue("NA")]
            Na
        }

        #endregion

        #region PAM
        public enum PAMStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Issued")]
            Issued,
            [StringValue("Completed")]
            Completed
        }

        public enum PAMAcknowledgement
        {
            [StringValue("Acknowledge")]
            Acknowledge,
            [StringValue("Returned")]
            Returned,
            [StringValue("Pending")]
            Pending,
        }

        public enum PMGApproval
        {
            [StringValue("Pending")]
            PENDING,
            [StringValue("Approved")]
            APPROVED,
            [StringValue("Rejected")]
            REJECTED,
        }

        public enum PAMRequestStatus
        {
            [StringValue("Requested")]
            Requested,
            [StringValue("Denied")]
            Denied,
            [StringValue("Approved")]
            Approved
        }

        public enum PDHComponents
        {
            [StringValue("Plates")]
            Plates,
            [StringValue("Forged Shell")]
            ForgedShell,
            [StringValue("Tube Sheet")]
            TubeSheet
        }

        #endregion

        #region HTR
        public enum HTRStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("In Review")]
            InReview,
            [StringValue("With Furnace Supervisor")]
            WithFurnaceSupervisor,
            [StringValue("With Inspector")]
            WithInspector,
            [StringValue("Approved")]
            Release,
            [StringValue("Cancel")]
            Cancel,
            [StringValue("Superseded")]
            Superseded
        }

        public enum Initiator
        {
            [StringValue("Draft")]
            Draft
        }
        public enum JCPTCfurnase
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("In Review")]
            InReview,
        }
        public enum Inspector
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("In Review")]
            InReview,
            [StringValue("With Furnace Supervisor")]
            WithFurnaceSupervisor,
        }

        public enum JC_Release
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("In Review")]
            InReview,
            [StringValue("With Furnace Supervisor")]
            WithFurnaceSupervisor,
            [StringValue("With Inspector")]
            WithInspector,
        }

        public enum HTRDepartment
        {
            [StringValue("METReviewer")]
            METReviewer,
            [StringValue("Furnacesupervisor")]
            Furnacesupervisor,
            [StringValue("Inspector")]
            Inspector,
        }

        public enum PTC_Release
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("In Review")]
            InReview,
            [StringValue("With Inspector")]
            WithInspector,
        }
        public enum VolTempOf
        {
            [StringValue("Water")]
            Water,
            [StringValue("Oil")]
            Oil
        }
        public enum TypeOfHTR
        {
            [StringValue("Job component-Nuclear")]
            JobComponentNuclear,
            [StringValue("Job component(Section PWHT/ISR/Local PWHT/MTC/UT Blocks etc.)")]
            JobComponent,
            [StringValue("PQR/Batch Testing/Mockup")]
            PQR,
            [StringValue("PTC")]
            PTC
        }

        public enum Treatment
        {
            [StringValue("AGEING")]
            AGEING,
            [StringValue("HARDENING")]
            HARDENING,
            [StringValue("HIGH TEMPERATURE TEMPERING")]
            HIGHTEMPERATURETEMPERING,
            [StringValue("HOT EDGE BREAKING")]
            HOTEDGEBREAKING,
            [StringValue("HOT FORMING")]
            HOTFORMING,
            [StringValue("HOT RE-ROLLING")]
            HOTREROLLING,
            [StringValue("HOT ROLLING")]
            HOTROLLING,
            [StringValue("INTERMEDIATE STRESS RELIEVING")]
            INTERMEDIATESTRESSRELIEVING,
            [StringValue("LOCAL AGEING")]
            LOCALAGEING,
            [StringValue("LOCAL STRESS RELIEVING")]
            LOCALSTRESSRELIEVING,
            [StringValue("NORMALISING")]
            NORMALISING,
            [StringValue("NORMALISING WITH ACCELERATED COOLING")]
            NORMALISINGWITHACCELERATEDCOOLING,
            [StringValue("PRECIPITATION HARDENING")]
            PRECIPITATIONHARDENING,
            [StringValue("QUENCHING")]
            QUENCHING,
            [StringValue("SHRINK FITTING")]
            SHRINKFITTING,
            [StringValue("SOLN.ANNEALING")]
            SOLNANNEALING,
            [StringValue("SPECIAL HT")]
            SPECIALHT,
            [StringValue("STRESS RELIEVING")]
            STRESSRELIEVING,
            [StringValue("TEMPERING")]
            TEMPERING,
            [StringValue("WARM EDGE BREAKING")]
            WARMEDGEBREAKING,
            [StringValue("WARM ROLLING/RE_ROLLING")]
            WARMROLLINGREROLLING,
            [StringValue("Intermediate Normalizing")]
            IntermediateNormalizing,
            [StringValue("Intermediate Tempering")]
            IntermediateTempering,
            [StringValue("REFRATORY DRYOUT")]
            REFRATORY_DRYOUT,
            [StringValue("WARM FORMING")]
            WARMFORMING
        }

        public enum Furnace
        {
            [StringValue("LEMF MODULAR 2")]
            LEMFMODULAR2,
            [StringValue("LEMF MODULAR 1")]
            LEMFMODULAR1,
            [StringValue("LEMF LONG")]
            LEMFLONG,
            [StringValue("AUXILERY SERVICE MFS-4")]
            AUXILERYSERVICEMFS4,
            [StringValue("AUXILERY SERVICE MFS-3")]
            AUXILERYSERVICEMFS3,
            [StringValue("AUXILERY SERVICE MFS-2")]
            AUXILERYSERVICEMFS2,
            [StringValue("AUXILERY SERVICE MFS-1")]
            AUXILERYSERVICEMFS1,
            [StringValue("AUXILERY SERVICE HFS 6B")]
            AUXILERYSERVICEHFS6B,
            [StringValue("AUXILERY SERVICE HFS 6A")]
            AUXILERYSERVICEHFS6A,
            [StringValue("AUXILERY SERVICE HFS 5B")]
            AUXILERYSERVICEHFS5B,
            [StringValue("AUXILERY SERVICE HFS 5A")]
            AUXILERYSERVICEHFS5A,
            [StringValue("AUXILERY SERVICE HFS 4A")]
            AUXILERYSERVICEHFS4A,
            [StringValue("AUXILERY SERVICE HFS 4B")]
            AUXILERYSERVICEHFS4B,

            [StringValue("OUTSIDE - KHS")]
            OUTSIDEKHS,
            [StringValue("OUTSIDE")]
            OUTSIDE,
            [StringValue("INDOTHERM")]
            INDOTHERM,
            [StringValue("NES-2")]
            NES2,
            [StringValue("BAGSVIG")]
            BAGSVIG,
            [StringValue("AUXILERY CIRCULAR")]
            AUXILERYCIRCULAR,
            [StringValue("SHOP PFS")]
            SHOPPFS,
            [StringValue("SHOP HFS 1")]
            SHOPHFS1,
            [StringValue("PLATE HEATING FURNACE (PH)")]
            PLATEHEATINGFURNACEPH,
            [StringValue("MAIN FURNACE (MF)")]
            MAINFURNACEMF,
            [StringValue("LEMF TALL")]
            LEMFTALL,
            [StringValue("LEMF PIPE")]
            LEMFPIPE,
            [StringValue("OUTSIDE - HW")]
            OUTSIDEHW,
            [StringValue("OUTSIDE - HTE")]
            OUTSIDEHTE,
            [StringValue("Therelek Coupon Heat Treatment Furnace")]
            TherelekCouponHeatTreatmentFurnace,
            [StringValue("ELECTRICAL HEATING")]
            ELECTRICALHEATING,
            [StringValue("Auxiliary Service HFS 2A")]
            HFS_2A,
            [StringValue("Auxiliary Service HFS 2B")]
            HFS_2B,
            [StringValue("Auxiliary Service HFS 3A")]
            HFS_3A,
            [StringValue("Auxiliary Service HFS 3B")]
            HFS_3B,
            [StringValue("VHEW-EF-1")]
            VHEWEF1,
            [StringValue("Outside furnace–VHEW")]
            OutsidefurnaceVHEW,
            [StringValue("Auxiliary Furnace LEMF-3")]
            AuxiliaryFurnaceLEMF3,
            [StringValue("Auxiliary Furnace LEMF-4")]
            AuxiliaryFurnaceLEMF4,
            [StringValue("Auxiliary Furnace LEMF-5")]
            AuxiliaryFurnaceLEMF5,
            [StringValue("70 MT Bogie Furnace")]
            MT_Bogie_Furnace

        }

        public enum MinLoadingTemp
        {
            [StringValue("300")]
            Min300C,
            [StringValue("400")]
            Max400C,
            //[StringValue("Editable")]
            //Editable,
        }

        public enum MaxLoadingTemp
        {
            [StringValue("300")]
            Min300C,
            [StringValue("400")]
            Max400C,
            //[StringValue("Editable")]
            //Editable,
        }

        public enum MinRateOfHeating
        {
            [StringValue("100")]
            CHR100,
            [StringValue("80")]
            CHR80,
            [StringValue("50")]
            CHR50,
            [StringValue("30")]
            CHR30,
            //[StringValue("Editable")]
            //Editable,
        }


        public enum MaxRateOfHeating
        {
            [StringValue("100")]
            CHR100,
            [StringValue("80")]
            CHR80,
            [StringValue("50")]
            CHR50,
            [StringValue("30")]
            CHR30,
            //[StringValue("Editable")]
            //Editable,
        }

        public enum SoakingTimeTolerance
        {
            [StringValue("(-0) / (+10) minutes")]
            plus10minutes,
            [StringValue("(-0) / (+15) minutes")]
            plus15minutes,
            [StringValue("(-0) / (+5) minutes ")]
            plus5minutes,
            [StringValue("(-0) / (+30) minutes")]
            plus30minutes,
        }
        public enum MinRateOfCooling
        {
            [StringValue("100")]
            CHR100,
            [StringValue("80")]
            CHR80,
            [StringValue("50")]
            CHR50,
            [StringValue("30")]
            CHR30,
            //[StringValue("Editable")]
            //Editable
        }

        public enum MaxRateOfCooling
        {
            [StringValue("100")]
            CHR100,
            [StringValue("80")]
            CHR80,
            [StringValue("50")]
            CHR50,
            [StringValue("30")]
            CHR30,
            //[StringValue("Editable")]
            //Editable
        }
        public enum CoolPart
        {
            [StringValue("Outside Furnace")]
            OutsideFurnace,
            [StringValue("Inside Furnace")]
            InsideFurnace
        }

        public enum CoolBy
        {
            [StringValue("Not Applicable")]
            NotApplicable,
            [StringValue("Water")]
            Water,
            [StringValue("Still Air")]
            StillAir,
            [StringValue("Oil")]
            Oil,
            [StringValue("Air Blast")]
            AirBlast,
        }

        public enum HeatingThermocouplesVariation
        {
            [StringValue("NA")]
            NA,
            [StringValue("30")]
            HTV30,
            [StringValue("50")]
            HTV50,
            [StringValue("80")]
            HTV80,
            //[StringValue("Editable")]
            //Editable
        }

        public enum CoolingThermocouplesVariation
        {
            [StringValue("NA")]
            NA,
            [StringValue("30")]
            HTV30,
            [StringValue("50")]
            HTV50,
            [StringValue("80")]
            HTV80,
            //[StringValue("Editable")]
            //Editable
        }
        public enum StandardProcedure
        {
            [StringValue("HED/MET/012 (Procedure for stress relieving of pressure vessels & heat exchanger for PC1 & PC2)")]
            HEDMET012,
            [StringValue("HED/MET/040 (Procedure For Heat Treatment Of Defense Components/Equipments)")]
            HEDMET040,
            [StringValue("HED/MET/032 (Procedure for Heat Treatment of Pressure Vessels & components of PC3)")]
            HEDMET032,
            [StringValue("HED/MET/031 (Procedure for stress Relieving of Nuclear facility Components)")]
            HEDMET031,
            [StringValue("HED/MET/029 (Procedure for stress relieving of Welds in power & Process piping)")]
            HEDMET029,
            [StringValue("HED/MET/041 (Procedure For Heat Treatment Of Aerospace Equipments/Components)")]
            HEDMET041,
            [StringValue("HED-HZW-MET-P-080")]
            HEDHZWMETP080,
            [StringValue("NQAP0902")]
            NQAP0902,
            [StringValue("Other")]
            Other
        }
        public enum FurnaceBedDistance
        {
            [StringValue("mm")]
            mm,
            [StringValue("cm")]
            cm,
            [StringValue("ft")]
            ft,
            [StringValue("in")]
            Furnecein,
            [StringValue("m")]
            m,
        }
        public enum WPSNo
        {
            [StringValue("999-154-R3(HAZIRA)")]
            WPSNo999154R3HAZIRA,
            [StringValue("994-15-R3(POWAI)")]
            WPSNo99415R3POWAI,
            [StringValue("999-901-R0(OMAN)")]
            WPSNo999901R0OMAN,
            [StringValue("990-03 (N)")]
            WPSNo99003N,
            [StringValue("Other")]
            Other
        }
        public enum MinUnloadingTemp
        {
            [StringValue("300")]
            Min300C,
            [StringValue("400")]
            Max400C,
            //[StringValue("Editable")]
            //Editable,
        }
        public enum MaxUnloadingTemp
        {
            [StringValue("300")]
            Min300C,
            [StringValue("400")]
            Max400C,
            //[StringValue("Editable")]
            //Editable,
        }
        #endregion

        #region Planning Din Status

        public enum PlanningDinStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Released")]
            RELEASED
        }

        #endregion

        #region WMIStatus
        public enum WMIStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Send To Approval")]
            SendToApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned
        }
        #endregion

        #region PQRStatus
        public enum PQRStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Send To Approval")]
            SendToApproval,
            [StringValue("Approved")]
            Approved           
        }
        #endregion

        #region Partlist Inspection Offer Status

        public enum PartlistOfferInspectionStatus
        {
            [StringValue("Ready to Offer")]
            READY_TO_OFFER,
            [StringValue("Under Inspection")]
            UNDER_INSPECTION,
            [StringValue("Cleared")]
            CLEARED,
            [StringValue("Partially Cleared")]
            PARTIALLY_CLEARED,
            [StringValue("Partially Offered")]
            PARTIALLY_OFFERED,
            [StringValue("Partially Rework")]
            PARTIALLY_REWORK,
            [StringValue("Partially Rejected")]
            PARTIALLY_REJECTED,
            [StringValue("Returned")]
            RETURNED,
            [StringValue("Rework")]
            REWORK,
            [StringValue("Rejected")]
            REJECTED

        }

        #endregion

        #region Fixture
        public enum FixtureStatus
        {
            [StringValue("Returned")]
            Returned,
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent For Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Superseded")]
            Superseded
        }

        public enum FXRMaterialType
        {
            [StringValue("Carbon Steel")]
            Carbon_Steel,
            [StringValue("Alloy Steel")]
            Alloy_Steel,
            [StringValue("Mild Steel")]
            Mild_Steel,
            [StringValue("Aluminium")]
            Aluminium,
            [StringValue("Stainless Steel")]
            Stainless_Steel,
            [StringValue("Copper")]
            Copper
        }

        public enum FXRUnit
        {
            [StringValue("m2")]
            m2,
            [StringValue("m")]
            m,
            [StringValue("kg")]
            kg,
            [StringValue("Nos")]
            nos
        }

        public enum FXRUnit2
        {
            [StringValue("m2")]
            m2,
            [StringValue("m")]
            m,
            [StringValue("kg")]
            kg,
            [StringValue("Nos")]
            nos
        }
        public enum FXRCategory
        {
            [StringValue("Plate")]
            Plate,
            [StringValue("Structural")]
            Structural,
            [StringValue("Pipe-Tube")]
            Pipe_Tube,
            [StringValue("Rod")]
            Rod,
            [StringValue("Sq.Bar")]
            Bar,
            [StringValue("Wire-Rod")]
            Wire_Rod,
            [StringValue("Fastner")]
            Fastner,
            [StringValue("Sheet")]
            Sheet
        }

        public enum FXRStructuralType
        {
            [StringValue("Angle")]
            Angle,
            [StringValue("Channel")]
            Channel,
            [StringValue("Beam")]
            Beam,
            [StringValue("Square Hollow Section")]
            Square_Hollow_Section,
            [StringValue("Rectangle Hollow Section")]
            Rectangle_Hollow_Section,
        }

        public enum FXRNDTRequirement
        {
            [StringValue("PT")]
            PT,
            [StringValue("Visual")]
            Visual
        }

        public enum FXRSubContractingType
        {
            [StringValue("Saddle")]
            Saddle,
            [StringValue("Fixture")]
            Fixture
        }

        public enum FXRTypeofSaddle
        {
            [StringValue("PWHT")]
            PWHT,
            [StringValue("Hydro test")]
            HydroTest,
            [StringValue("Transportation")]
            Transportation
        }

        public enum FXRTypeofFixture
        {
            [StringValue("Dummy shell")]
            DummyShell,
            [StringValue("Spider")]
            Spider,
            [StringValue("Ovality ring")]
            OvalityRing,
            [StringValue("LSR Furnace")]
            LSRFurnace,
            [StringValue("Cleats")]
            Cleats,
            [StringValue("Stool")]
            Stool,
            [StringValue("Base frame")]
            BaseFrame,
            [StringValue("Skid")]
            Skid,
            [StringValue("Prop")]
            Prop,
            [StringValue("Other")]
            Other
        }

        public enum FXRTypeofWork
        {
            [StringValue("Fresh Work")]
            FreshWork,
            [StringValue("Additional work")]
            AdditionalWork,
            [StringValue("Modified")]
            Modified
        }
        #endregion

        #region SWP
        public enum DHTISR
        {

            [StringValue("DHT")]
            DHT,
            [StringValue("ISR")]
            ISR,
            [StringValue("DHT/ISR")]
            DHTISR,
            [StringValue("N/A")]
            NA,
        }
        #endregion

        #region NCR
        public enum NCRStatus                                   //Last Action Performed                             In Tray Of
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Create")]                            //Created                                           CreatedBy
            Create,
            //[StringValue("Send for Validation")]                //Sent for Validation                               Q C Engineer
            // [StringValue("Validate")]
            // SendForValidation,
            //[StringValue("Send for CAPA Submission")]                          //Validated                                         Responsible Dept Manager
            [StringValue("Validate")]
            Validate,
            //[StringValue("Send for CAPA Acceptance")]             //Responsible Department Approved                   Q A Engineer
            [StringValue("CAPA by Responsible Department")]
            ResponsibleDepartment,
            //[StringValue("Send for Review Comments")]                        //Q A Approved                                      S Design Engineer
            // [StringValue("Review Comments")]
            [StringValue("QA Approve")]
            QAApprove,
            [StringValue("Review Comments")]                   //Review Commented                                  Q C Manager
            ReviewComment,
            // [StringValue("Approved")]                           //Approved                                          Conditional
            [StringValue("Approve by QC")]
            Approve,
            //[StringValue("External Approved")]                  //External Approved                                 Quality Manager
            [StringValue("External Approver")]
            ExternalApprove,
            [StringValue("Plan Implementation")]                //Plan Implemented                                  Conditional
            PlanImplementation,
            //[StringValue("Implemented")]                        //Implemented                                       Conditional
            [StringValue("Implement")]
            Implement,
            //[StringValue("QC Closed")]                         //Q C Closed                                        Conditional
            [StringValue("QC Closure")]
            QCClosure,
            //[StringValue("External Closed")]                   //External Closed
            [StringValue("External Closure")]
            ExternalClosure,
            // [StringValue("Closed")]                         //Q A Closed
            [StringValue("QA Closure")]
            QAClosure,
            [StringValue("Closed")]                             //Closed                                            None
            Closed,
            [StringValue("Cancelled")]                          //Cancelled                                         None
            Cancelled,
            [StringValue("Marked For Cancel")]                  //Marked for Cancellation                           CreatedBy
            MarkedForCancel,
        }

        public enum NCRStage
        {
            Created,
            SentForValidation,
            Validated,
            ResponsibleDepartmentApproved,

        }

        public enum NCRLocation
        {
            [StringValue("Kanchipuram")]  //KAN                ------------------------------------------
            Kanchipuram,
            [StringValue("Bangalore")]    //BLR
            Bangalore,
            [StringValue("Hazira")]       //HZW
            Hazira,
            [StringValue("Powai")]        //PEW
            Powai,
            [StringValue("Ranoli")]       //RNW
            Ranoli,
            [StringValue("Vizag")]        //VPC
            Vizag,
            [StringValue("Coimbatore")]   //CBE
            Coimbatore,
            [StringValue("Talegaon")]     //TEW
            Talegaon,
            [StringValue("Chennai")]      //CEN
            Chennai,
            [StringValue("Sohar")]        //SOH
            Sohar,
            [StringValue("Customer Site")]
            CustomerSite,
        }

        public enum NCRTypeOfAssembly
        {
            [StringValue("Other")]
            Other,
            [StringValue("Structural")]
            Structural,
            [StringValue("Tubesheet")]
            Tubesheet,
            [StringValue("Regenerator")]
            Regenerator,
            [StringValue("Shell")]
            Shell,
            [StringValue("Assembly")]
            Assembly,
            [StringValue("Shipbuilding")]
            Shipbuilding,
            [StringValue("Site")]
            Site,
            [StringValue("Skirt")]
            Skirt,
            [StringValue("Special")]
            Special,
            [StringValue("Project")]
            Project,
            [StringValue("Steam Generator")]
            SteamGenerator,
            [StringValue("Pipe")]
            Pipe,
            [StringValue("Plate")]
            Plate,
            [StringValue("Reactor")]
            Reactor,
            [StringValue("Column")]
            Column,
            [StringValue("Tower")]
            Tower,
            [StringValue("Bolting")]
            Bolting,
            [StringValue("Channel")]
            Channel,
            [StringValue("Dished End Assembly")]
            DishedEndAssembly,
            [StringValue("Drum")]
            Drum,
            [StringValue("EO Reactor")]
            EOReactor,
            [StringValue("Exchanger")]
            Exchanger,
            [StringValue("Externals")]
            Externals,
            [StringValue("Gasket")]
            Gasket,
            [StringValue("Internals")]
            Internals,
            [StringValue("Nozzle Forging")]
            NozzleForging,
            [StringValue("Nuclear")]
            Nuclear,
            [StringValue("Gasifier")]
            Gasifier,
            [StringValue("Baffles")]
            Baffles,
        }

        public enum NCRRootCauseCategory
        {
            [StringValue("Base Material Defect")]
            BaseMaterialDefect,
            [StringValue("Dimension Control Machining")]
            DimensionControlMachining,
            [StringValue("Dimension Control Fabrication")]
            DimensionControlFabrication,
            [StringValue("Improper Gas Cutting")]
            ImproperGasCutting,
            [StringValue("Improper Heat Treatment")]
            ImproperHeatHandling,
            [StringValue("Material Handling")]
            MaterialHandling,
            [StringValue("WPS/Wels Plan Violation")]
            WPSWelsPlanViolation,
            [StringValue("Temporary Attachment")]
            TemporaryAttachment,
            [StringValue("Vendor Mistake")]
            VendorMistake,
            [StringValue("Human Error")]
            HumanError,
            [StringValue("Calibration Error")]
            CalibrationError,
            [StringValue("Drawing Error")]
            DrawingError,
            [StringValue("System Violation")]
            SystemViolation,
            [StringValue("PMG/Planning Mistake")]
            PmgPlaninning,
            [StringValue("Assembly & Integration (In Process)")]
            AssemblyAndIntegration,
            [StringValue("System Functional/Operational (FAT)")]
            SystemFunctional,
            [StringValue("Process Parameters / Process Sheet (Process Sheet Requirement)")]
            ProcessParameters,
            [StringValue("Traceability (Material & UID)")]
            Traceability,
            [StringValue("Latest Document Non-Availabilty")]
            LatestDocumentNonAvailability,
            [StringValue("Others")]
            Others,
        }

        public enum NCRProblemDueTo
        {
            [StringValue("Materials")]
            Materials,
            [StringValue("Workmanship")]
            Workmanship,
            [StringValue("Welding")]
            Welding,
            [StringValue("Handling")]
            Handling,
            [StringValue("Painting")]
            Painting,
            [StringValue("Design/Drawing")]
            DesignDrawing,
            [StringValue("Execution")]
            Execution,
            [StringValue("Inspection")]
            Talegaon,
            [StringValue("Vendor")]
            Vendor,
            [StringValue("NDT")]
            NDT,
            [StringValue("Other")]
            Other,
        }

        public enum NCRDisposition
        {
            [StringValue("Accept as it is")]
            AcceptAsItIs,
            [StringValue("Accept Partial Quantity")]
            AcceptPartialQuantity,
            [StringValue("Regrade (Accept Conditionally)")]
            Regrade,
            [StringValue("Reject")]
            Reject,
            [StringValue("Repair (Rectify Partially)")]
            Repair,
            [StringValue("Rework (Complete Rectification)")]
            Rework,
        }

        public enum NCRMinorMajor
        {
            [StringValue("Minor")]
            Minor,
            [StringValue("Major")]
            Major,
        }
        #region Access Request
        public enum AccessRequest
        {
            [StringValue("Approved")]
            Approved,
            [StringValue("Rejected")]
            Rejected,
            [StringValue("Requested")]
            Requested
        }

        #endregion

        public enum NCRReportFormat
        {
            [StringValue("Hazira Format")]
            Hazira,
            [StringValue("Powai Format")]
            Powai,
            [StringValue("Ranoli Format")]
            Ranoli,
        }

        //NCR Methods

        public static List<string> GetNCRReportFormat()
        {
            List<string> items = GetListOfDescription<NCRReportFormat>();
            return items;
        }

        public static List<string> GetNCRStatus()
        {
            List<string> items = GetListOfDescription<NCRStatus>();
            return items;
        }
        public static string convertNCRLocation(string loc)
        {
            string result;
            switch (loc)
            {
                case "Kanchipuram":
                    result = "KAN";
                    break;
                case "Bangalore":
                    result = "BLR";
                    break;
                case "Hazira":
                    result = "HZW";
                    break;
                case "Powai":
                    result = "PEW";
                    break;
                case "Ranoli":
                    result = "RNW";
                    break;
                case "Vizag":
                    result = "VPC";
                    break;
                case "Coimbatore":
                    result = "CBE";
                    break;
                case "Talegaon":
                    result = "TEW";
                    break;
                case "Chennai":
                    result = "CEN";
                    break;
                case "Sohar":
                    result = "SOH";
                    break;
                default:
                    result = "Customer Site";
                    break;
            }
            return result;
        }


        public static List<string> getNCRLocation()
        {
            List<string> items = GetListOfDescription<NCRLocation>();
            return items;
        }


        public static List<string> getTypeofAssembly()
        {
            List<string> items = GetListOfDescription<NCRTypeOfAssembly>();
            return items;
        }
        public static List<string> getDisposition()
        {
            List<string> items = GetListOfDescription<NCRDisposition>();
            return items;
        }
        public static List<string> getProblemDueTo()
        {
            List<string> items = GetListOfDescription<NCRProblemDueTo>();
            return items;
        }

        public static List<string> getRootCauseCategory()
        {
            List<string> items = GetListOfDescription<NCRRootCauseCategory>();
            return items;
        }
        public static List<string> getMinMaj()
        {
            List<string> items = GetListOfDescription<NCRMinorMajor>();
            return items;
        }

        #endregion

        #region IPI

        public enum InspectionFor
        {
            [StringValue("Seam")]
            SEAM,
            [StringValue("Part")]
            PART,
            [StringValue("Assembly")]
            ASSEMBLY,
        }

        public enum QualityIDApplicable
        {
            [StringValue("Seam")]
            SEAM,
            [StringValue("Part")]
            PART,
            [StringValue("Assembly")]
            ASSEMBLY,
        }

        public enum ProductType
        {
            [StringValue("FRG")]
            FRG,
        }
        public enum SeamStageType
        {
            [StringValue("Chemical")]
            Chemical,
            [StringValue("PMI")]
            PMI,
            [StringValue("Hardness")]
            Hardness,
            [StringValue("Ferrite")]
            Ferrite
        }

        public enum QualityIdHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }

        public enum QualityIdStageStatus
        {
            [StringValue("Added")]
            ADDED,
            [StringValue("Deleted")]
            DELETED,
            [StringValue("Modified")]
            MODIFIED,
            [StringValue("Approved")]
            APPROVED
        }
        public enum BOMTemplateStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted")]
            Submitted
        }
        public enum SeamListStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        public enum SeamListInspectionStatus
        {
            [StringValue("Ready to Offer")]
            ReadyToOffer,
            [StringValue("Under Inspection")]
            UnderInspection,
            [StringValue("Cleared")]
            Cleared,
            [StringValue("Rework")]
            Rework,
            [StringValue("Returned")]
            Returned,
            [StringValue("Rejected")]
            Rejected,
            [StringValue("Pending for Confirmation")]
            PendingForConfirmation,
            [StringValue("Offered to NDE")]
            OfferedToNDE,
            [StringValue("Rework Not Attached")]
            ReworkNotAttached,
            [StringValue("Rework Attached")]
            ReworkAttached
        }
        public enum SpotGenerationType
        {
            [StringValue("Circular")]
            Circular,
            [StringValue("Straight")]
            Straight
        }
        public enum SpotResults
        {
            [StringValue("Cleared")]
            Cleared,
            [StringValue("Rejected")]
            Rejected,
            [StringValue("Re-Scan")]
            Rescan,
            [StringValue("Re-Take")]
            Retake,
            [StringValue("Reshoot")]
            Reshoot,
            [StringValue("Miscellaneous")]
            Mishelenious,
            [StringValue("Discard")]
            Discard,

        }

        public enum NDESeamRequestStatus
        {
            [StringValue("Attended")]
            Attended,
            [StringValue("Not Attended")]
            NotAttended
        }

        #endregion
        #region LTFPS
        public enum LTFPSHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Returned")]
            Returned,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Released")]
            Released,
            [StringValue("Superseded")]
            Superseded,
        }
        public enum LTFPSLineStatus
        {
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified,
            [StringValue("Approved")]
            Approved,
            [StringValue("Released")]
            Released
        }
        #endregion
        #region LTFPS
        public enum TravelerHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Returned")]
            Returned,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Released")]
            Released,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Submitted to QI")]
            SubmittedToQI,
            [StringValue("Returned by QI")]
            ReturnedbyQI,
            [StringValue("Submitted to WE")]
            SubmittedToWE,
            [StringValue("Returned By WE")]
            ReturnedbyWE,
            [StringValue("Submitted to Planning")]
            SubmittedToPLANNING,
            [StringValue("Returned by Planning")]
            Returnedbyplanning,
            [StringValue("Approved by Planning")]
            ApprovedbyPlanning,
            [StringValue("Ready to Offer")]
            Readytooffer,
            [StringValue("Completed")]
            Completed
        }
        public enum TravelerLineStatus
        {
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified,
            [StringValue("Approved")]
            Approved,
            [StringValue("Released")]
            Released,
            [StringValue("Superseded")]
            Superseded
        }
        public enum TravelerIdentificationDropdown
        {
            [StringValue("Assembly No")]
            Assembly_No,
            [StringValue("Sub Assembly No")]
            Sub_Assembly_No,
            [StringValue("Component No")]
            Component_No,
            [StringValue("Standard Traveler")]
            StandardTraveler
        }
        #endregion

        #region WPP 
        //Type of weld
        public enum Typeofweld
        {
            [StringValue("Manual")]
            Manual,
            [StringValue("Machine")]
            Machine,
            [StringValue("Semi-Automatic")]
            Semi_Automatic,
            [StringValue("Automatic")]
            Automatic,
            [StringValue("Semi-Automatic/Machine")]
            Semi_Automatic_Machine,
            [StringValue("Manual/Semi-Automatic")]
            Manual_Semi_Automatic,
            [StringValue("Machine/Semi- Automatic")]
            Machine_Semi_Automatic,
            [StringValue("Other 1")]
            Other_1,
            [StringValue("Other")]
            Other
        }
        public enum TypeOfTokenProcess
        {
            [StringValue("WPP")]
            WPP,
            [StringValue("WPS")]
            WPS,
        }

        public enum SyncBOMData
        {
            [StringValue("Bill of Material")]
            COM011,
            [StringValue("Item Master")]
            COM012,
        }
        public static string convertTOTable(string data)
        {
            string result = string.Empty;
            switch (data)
            {
                case "Bill of Material":
                    result = "COM011";
                    break;
                case "Item Master":
                    result = "COM012";
                    break;
                case "Item Purchase Data":
                    result = "COM013";
                    break;
            }
            return result;
        }
        //WEPBy
        public enum WEPBy
        {
            [StringValue("Gas Cutting followed by grinding (min. 3 mm)/Grinding/Water Jet/Machining")]
            Gas_Cutting,
            [StringValue("Plasma Cutting followed by grinding (min. 3 mm)/Grinding/Water Jet/Machining")]
            Plasma_Cutting,
            [StringValue("Machining/Grinding")]
            Machining_Grinding,
        }
        public enum Current
        {
            [StringValue("AC")]
            AC,
            [StringValue("DC")]
            DC,
        }

        public enum Polarity
        {
            [StringValue("EP")]
            EP,
            [StringValue("EN")]
            EN,
        }
        #endregion

        #region PTR
        public enum PTRStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted to WE2")]
            SubmitWE2,
            [StringValue("Submitted to QC3")]
            SubmitQC3,
            [StringValue("Submitted to QA3/WTC3")]
            SubmitQA3,
            [StringValue("Returned by WE2")]
            RetWE2,
            [StringValue("Returned by QC3")]
            RetQC3,
            [StringValue("Returned by QA3/WTC3")]
            RetQA3,
            [StringValue("Closed")]
            Closed,
        }
        public enum CouponCat
        {
            [StringValue("Production Test Coupon")]
            ProdTestCpn,
            [StringValue("Procedure Qualification")]
            ProcQualf,
            [StringValue("Performance Qualification ")]
            PerfQualf,
            [StringValue("Material test coupon")]
            MatTestCpn,
            [StringValue("Consumable qualification")]
            ConsQualf,
            [StringValue("Mock-up")]
            MockUp,
            [StringValue("Water Analysis")]
            WaterAnls
        }

        public enum WPSHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }

        public enum WKGHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent For Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        public enum WQRequestStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Pending with Shop In-charge")]
            PendingWithShopInCharge,
            [StringValue("Pending with WE")]
            PendingWithWE,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Superseded")]
            Superseded
        }
        public enum WQLinesStatus
        {
            [StringValue("Accepted")]
            Accepted,
            [StringValue("RejectedByShop")]
            RejectedByShop,
            [StringValue("RejectedByWeld")]
            RejectedByWeld,
        }

        public enum WQCertificateStatus
        {
            [StringValue("NA")]
            NA,
            [StringValue("Pass")]
            Pass,
            [StringValue("Fail")]
            Fail
        }

        #endregion

        #region ITP
        public enum ITPMaterialCategory
        {
            [StringValue("Forging")]
            Forging,
            [StringValue("Plate")]
            Plate,
            [StringValue("Tube")]
            Tube,
            [StringValue("Bellows")]
            Bellows,
            [StringValue("Valves")]
            Valves,
            [StringValue("Gauges & Instruments")]
            Gauges_Instruments,
            [StringValue("Pipes & pipe fittings")]
            Pipes_pipe_fittings,
            [StringValue("Fasteners")]
            Fasteners,
            [StringValue("Gasket")]
            Gasket,
            [StringValue("Structural")]
            Structural,
            [StringValue("Others")]
            Others
        }
        #endregion

        #region Protocol
        // #NewProtocolLinkage
        public enum ProtocolType
        {
            [StringValue("PROTOCOL001")]
            SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM,
            [StringValue("PROTOCOL002")]
            SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM,
            [StringValue("PROTOCOL003")]
            SETUP_INSPECTION_REPORT_FOR_FORMED,
            [StringValue("PROTOCOL004")]
            SETUP_AND_DIMENSION_REPORT_FOR_NOZZLE,
            [StringValue("PROTOCOL005")]
            VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM,
            [StringValue("PROTOCOL006")]
            VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_D_END_CONE,
            [StringValue("PROTOCOL007")]
            WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE,
            [StringValue("PROTOCOL008")]
            NUB_DIMENSION_INSPECTION_REPORT_FOR_SQUARE_TYPE,
            [StringValue("PROTOCOL009")]
            VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM,
            [StringValue("PROTOCOL010")]
            SETUP_AND_DIMENSION_REPORT_FOR_FORMED_ELBOW_LONG_SEAMS,
            [StringValue("PROTOCOL011")]
            WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE1,
            [StringValue("PROTOCOL012")]
            WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE2,
            [StringValue("PROTOCOL013")]
            NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE,
            [StringValue("PROTOCOL014")]
            VISUAL_AND_DIMENSION_BEFORE_OVERLAY_FOR_SQUARE_TYPE_NUB,
            [StringValue("PROTOCOL015")]
            VISUAL_AND_DIMENSION_AFTER_OVERLAY_FOR_TAPER_TYPE_NUB,
            [StringValue("PROTOCOL016")]
            TSR_DIMENSION_INSPECTION_REPORTS,
            [StringValue("PROTOCOL017")]
            WELD_VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_SUPPORT_BRACKET,
            [StringValue("PROTOCOL018")]
            RECORD_FOR_TEMPORARY_ATTACHMENTS_LOCATION,
            [StringValue("PROTOCOL019")]
            VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_TEMPLATE,
            [StringValue("PROTOCOL020")]
            VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY,
            [StringValue("PROTOCOL021")]
            REPORT_OF_HARDNESS_TEST,
            [StringValue("PROTOCOL022")]
            AIR_TEST_OF_REINFORCEMENT_PADS_OF_NOZZLE,
            [StringValue("PROTOCOL023")]
            HELIUM_LEAK_TEST_REPORT,
            [StringValue("PROTOCOL024")]
            HYDROSTATIC_TEST_REPORT,
            [StringValue("PROTOCOL025")]
            PNEUMATIC_TEST_REPORT,
            [StringValue("PROTOCOL026")]
            REPORT_FOR_N2_FILLING_INSPECTION,
            [StringValue("PROTOCOL027")]
            REPORT_FOR_POSITIVE_MATERIAL_IDENTIFICATION,
            [StringValue("PROTOCOL028")]
            REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET,
            [StringValue("PROTOCOL029")]
            REPORT_OF_ARM_REVIEW,
            [StringValue("PROTOCOL030")]
            VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_OVERLAY_ON_SHELL_DEND,
            [StringValue("PROTOCOL031")]
            REPORT_FOR_VISUAL_AND_DIM_OF_TAILING_LUG,
            [StringValue("PROTOCOL032")]
            OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_DEND_LONG_SEAM_CIRC_SEAM,
            [StringValue("PROTOCOL033")]
            OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_NOZZLE_PIPE_ELBOW,
            [StringValue("PROTOCOL034")]
            THICKNESS_REPORT_FOR_EQUIPMENT,
            [StringValue("PROTOCOL035")]
            NOZZLE_CUTOUT_MARKING_ON_SHELL_CONE_DEND,
            [StringValue("PROTOCOL036")]
            SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2,
            [StringValue("PROTOCOL037")]
            SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2,
            [StringValue("PROTOCOL038")]
            SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITHOUT_CROWN_AND_TIER,
            [StringValue("PROTOCOL039")]
            SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITH_CROWN_AND_TIER,
            [StringValue("PROTOCOL040")]
            SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITHOUT_CROWN_AND_TIER,
            [StringValue("PROTOCOL041")]
            SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITH_CROWN_AND_TIER,
            [StringValue("PROTOCOL042")]
            SETUP_DIMENSION_REPORT_FOR_NOZZLE,
            [StringValue("PROTOCOL043")]
            VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2,
            [StringValue("PROTOCOL044")]
            VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2,
            [StringValue("PROTOCOL045")]
            VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_D_END,
            [StringValue("PROTOCOL046")]
            VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_ELLIPSOIDAL_TORISPHERICAL_FORMED_D_END,
            [StringValue("PROTOCOL047")]
            WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE_V2,
            [StringValue("PROTOCOL048")]
            SETUP_AND_DIMENSION_REPORT_FOR_ATTACHMENT,
            [StringValue("PROTOCOL049")]
            VISUAL_AND_DIMENSION_REPORT_FOR_ATTACHMENT,
            [StringValue("PROTOCOL050")]
            VISUAL_AND_DIMENSION_REPORT_BEFORE_WELD_OVERALY_ON_SHELL,
            [StringValue("PROTOCOL051")]
            VISUAL_AND_DIMENSION_REPORT_AFTER_WELD_OVERALY_ON_SHELL,
            [StringValue("PROTOCOL052")]
            SETUP_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE,
            [StringValue("PROTOCOL053")]
            VISUAL_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE,
            [StringValue("PROTOCOL054")]
            REPORT_FOR_SETUP_AND_DIMENSION_INSPECTION_OF_TSR,
            [StringValue("PROTOCOL055")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_TSR,
            [StringValue("PROTOCOL056")]
            REPORT_FOR_VISUAL_AND_DIMENSION_REPORT_AFTER_MACHINING_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE,
            [StringValue("PROTOCOL057")]
            BEFORE_OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM,
            [StringValue("PROTOCOL058")]
            OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM,
            [StringValue("PROTOCOL059")]
            SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING_IN_SEGMENT_STAGE,
            [StringValue("PROTOCOL060")]
            VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING,
            [StringValue("PROTOCOL061")]
            REPORT_FOR_SETUP_AND_DIMENSION_OF_TAILING_LUG,
            [StringValue("PROTOCOL062")]
            REPORT_FOR_VISUAL_AND_DIMENSION_OF_TAILING_LUG,
            [StringValue("PROTOCOL063")]
            SETUP_AND_DIMENSION_REPORT_FOR_MTB_BLOCK_OR_PTC_LONGITUDINAL_SEAM,
            [StringValue("PROTOCOL064")]
            SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE,
            [StringValue("PROTOCOL065")]
            VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE,
            [StringValue("PROTOCOL066")]
            REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET_V2,
            [StringValue("PROTOCOL067")]
            REPORT_FOR_NOZZLE_CUT_OUT_MARKING_ON_SHELL_HEAD,
            [StringValue("PROTOCOL068")]
            AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_COVERING_WELD_SEAMS,
            [StringValue("PROTOCOL069")]
            AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_OF_NOZZLE,
            [StringValue("PROTOCOL070")]
            HELIUM_LEAK_TEST_REPORT_V2,
            [StringValue("PROTOCOL071")]
            HYDROSTATIC_TEST_REPORT_V2,
            [StringValue("PROTOCOL072")]
            PNEUMATIC_TEST_REPORT_V2,
            [StringValue("PROTOCOL073")]
            REPORT_FOR_CUSO4_TEST,
            [StringValue("PROTOCOL074")]
            REPORT_FOR_N2_FILLING_INSPECTION_V2,
            [StringValue("PROTOCOL075")]
            SETUP_AND_DIMENSION_REPORT_FOR_FORMED_CONE,
            [StringValue("PROTOCOL076")]
            VISUAL_AND_DIMENSION_REPORT_FOR_FORMED_CONE,
            [StringValue("PROTOCOL077")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_BEFORE_OVERLAY,
            [StringValue("PROTOCOL078")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_AFTER_OVERLAY,
            [StringValue("PROTOCOL079")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_BEFORE_OVERLAY,
            [StringValue("PROTOCOL080")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_AFTER_OVERLAY,
            [StringValue("PROTOCOL081")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_BEFORE_OVERLAY,
            [StringValue("PROTOCOL082")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_AFTER_OVERLAY,
            [StringValue("PROTOCOL083")]
            REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY,
            [StringValue("PROTOCOL084")]
            REPORT_FOR_VISUAL_AND_DIMENSION_OF_SKIRT_TEMPLATE,
            [StringValue("PROTOCOL085")]
            REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_1_BOTH_SIDE_FLANGES,
            [StringValue("PROTOCOL086")]
            REPORT_FOR_WELD_VISUAL_AND_DIMENSION_FOR_SPOOL_TYPE_2_ONE_SIDE_FLANGE,
            [StringValue("PROTOCOL087")]
            REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_3_BOTH_SIDE_FLANGES,
            [StringValue("PROTOCOL088")]
            AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE,
            [StringValue("PROTOCOL089")]
            AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE,
            [StringValue("PROTOCOL090")]
            INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE,
            [StringValue("PROTOCOL091")]
            INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE,
            [StringValue("PROTOCOL092")]
            REPORT_FOR_WATER_BOX_MACHINING_INSPECTION_R1,
            [StringValue("PROTOCOL093")]
            REPORT_FOR_SCREW_PLUG_ACME_HEADER_MACHINING_INSPECTION,
            [StringValue("PROTOCOL094")]
            FINAL_ASSEMBLY_INSPECTION_OF_ROUGH_LIQUID_DISTRIBUTION_TRAY,
            [StringValue("PROTOCOL095")]
            FINAL_ASSEMBLY_INSPECTION_OF_VAPOUR_LIQUID_DISTRIBUTION_TRAY,
            [StringValue("PROTOCOL096")]
            FINAL_DIMENSION_INSPECTION_OF_LATTICE_BEAMS,
            [StringValue("PROTOCOL098")]
            FINAL_DIMENSION_AND_VISUAL_INSPECTION_REPORT,
            [StringValue("OtherFiles")]
            Other_Files
        }

        public enum ProtocolRoleType
        {
            [StringValue("A")]
            A_Actualvalue_for_PROD,
            [StringValue("R")]
            R_Requiredvalue_for_QC,
        }
        public enum LocationLeftRight
        {
            [StringValue("Left Side")]
            Left,
            [StringValue("Right Side")]
            Right,
        }

        public enum OutsideInside
        {
            [StringValue("Outside")]
            Outside,
            [StringValue("Inside")]
            Inside,
        }
        #endregion

        #region Traveller Offer Status

        public enum TravelerInspectionStatus
        {
            [StringValue("Ready to Offer")]
            ReadyToOffer,
            [StringValue("Under Inspection")]
            UnderInspection,
            [StringValue("Cleared")]
            Cleared,
            [StringValue("Waived")]
            Waved,
            [StringValue("Rework")]
            Rework,
            [StringValue("Returned")]
            Returned,
            [StringValue("Rejected")]
            Rejected,
            [StringValue("Pending for Confirmation")]
            PendingForConfirmation,
            [StringValue("Offered to NDE")]
            OfferedToNDE,
            [StringValue("Rework Not Attached")]
            ReworkNotAttached,
            [StringValue("Rework Attached")]
            ReworkAttached,
            [StringValue("Returned By TPI")]
            Returned_By_TPI,
            [StringValue("Rework Traveler")]
            Rework_Traveler,
            [StringValue("Ncr No")]
            NCR_No,
            [StringValue("NCR Raised")]
            NCR_Raised,
            [StringValue("Ready to Process")]
            Ready_To_Process,
            [StringValue("Not Applicatble")]
            Not_Applicatble,
            [StringValue("Not Acceptable")]
            Not_Acceptable
        }
        public enum ColorFlagForTraveler
        {
            [StringValue("O")]
            Orange,
            [StringValue("Y")]
            Yellow,
            [StringValue("R")]
            Red
        }
        #endregion

        #region  Code Review Type

        public enum CodeReviewType
        {
            [StringValue("Application")]
            Application,
            [StringValue("Database")]
            Database
        }

        public enum CodeReviewApplicationSubType
        {
            [StringValue("Models")]
            Models,
            [StringValue("Views")]
            Views,
            [StringValue("Controllers")]
            Controllers
        }

        public enum CodeReviewDatabaseSubType
        {
            [StringValue("Tables")]
            Tables,
            [StringValue("Stored Procedures")]
            StoredProcedures,
            [StringValue("Functions")]
            Functions,
            [StringValue("Views")]
            Views
        }
        #endregion

        #region FKMS Enums
        public enum FKMSNodeType
        {
            [StringValue("Project")]
            Project,
            [StringValue("LooseItem")]
            LooseItem,
            [StringValue("SpareItem")]
            SpareItem,
            [StringValue("Template")]
            Template,
            [StringValue("Equipment")]
            Equipment,
            [StringValue("Fabrication Section")]
            Section,
            [StringValue("Assembly")]
            Assembly,
            [StringValue("Self")]
            Self,
            [StringValue("Sub Assembly")]
            SubAssembly,
            [StringValue("Internal Sub Assembly")]
            InternalAssembly,
            [StringValue("External Sub Assembly")]
            ExternalAssembly,
            [StringValue("Nozzel Sub Assembly")]
            NozzelAssembly,
            [StringValue("MTC Sub Assembly")]
            MTCSubAssembly,
            [StringValue("PTC Sub Assembly")]
            PTCSubAssembly,
            [StringValue("PWHT-Section")]
            PWHTSection
        }
        public enum NodeTypes
        {
            [StringValue("ASM")]
            ASM,
            [StringValue("TJF")]
            TJF,
            [StringValue("FRG")]
            FRG,
            [StringValue("PLT")]
            PLT,
            [StringValue("CMP")]
            CMP
        }
        public enum ColorFlagForFKMS
        {
            [StringValue("GR")]
            Green,
            [StringValue("YE")]
            Yellow,
            [StringValue("RE")]
            Red,
            [StringValue("VI")]
            Violet,
            [StringValue("GY")]
            Grey,
            [StringValue("OR")]
            Orange,
            [StringValue("DE")]
            Default,
            [StringValue("BL")]
            Blue
        }

        public enum KitStatus
        {
            [StringValue("RELEASE")]
            Release,
            [StringValue("PENDING")]
            Pending,
            [StringValue("REQUEST")]
            Request,
            [StringValue("READY FOR PARENT")]
            ReadyForParent,
            [StringValue("SEND TO FULLKIT AREA")]
            SendToFullkitArea,
            [StringValue("REQUESTED BY PARENT KIT")]
            RequestedByParentKit,
            [StringValue("SEND TO NEXT KIT")]
            SendToNextKit,
            [StringValue("ACCEPT")]
            Accept,
            [StringValue("SEND TO FKM STORE")]
            SendToKFMStore,
            [StringValue("SEND TO KIT")]
            SendToKit,
            [StringValue("MATERIAL AT FKM STORE")]
            MaterialAtFKMStore,
        }

        public enum AllocateNodeTypes
        {
            [StringValue("PLT")]
            PLT,
            [StringValue("NPLT")]
            NPLT,
            [StringValue("PROJ_PLT")]
            PROJ_PLT,
            [StringValue("PROJ_NPLT")]
            PROJ_NPLT,
        }

        public enum FKMSDocumentType
        {
            [StringValue("HTR")]
            HTR,
            [StringValue("DIN")]
            DIN
        }

        public enum FRStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Returned,
            [StringValue("Retract")]
            Retract,
            [StringValue("Revise")]
            Revise
        }

        public enum FRAllocationStatus
        {
            [StringValue("Requested")]
            Requested,
            [StringValue("Allocated")]
            Allocated,
            [StringValue("Completed")]
            Completed
        }

        public enum FRAttachmentType
        {
            [StringValue("Dimension report")]
            DimensionReport,
            [StringValue("PT report")]
            PTReport
        }

        public enum FullkitAreaStatus
        {
            [StringValue("Requested For Kit")]
            RequestedForKit,
            [StringValue("Send to Kit")]
            SendToKit,
            [StringValue("Pending For Confirmation")]
            PendingConfirmation,
            [StringValue("Confirmed")]
            Confirmed,
            [StringValue("Fixture Manufacturer")]
            FixtureManufacturer,
        }

        public enum FixtureOn
        {
            [StringValue("Departments")]
            Departments,
            [StringValue("Projects")]
            Projects
        }

        public enum UOMEnum
        {
            [StringValue("nos")]
            Nos,
            [StringValue("set")]
            Set,
            [StringValue("lot")]
            Lot,
            [StringValue("lit")]
            Lit,
            [StringValue("kg")]
            KG,
            [StringValue("m")]
            M,
            [StringValue("ft3")]
            FT3,
            [StringValue("m2")]
            M2,
            [StringValue("m3")]
            M3,
            [StringValue("mt")]
            MT,
            [StringValue("pkt")]
            PKT,
            [StringValue("rol")]
            ROL
        }

        public static List<string> getFixtureonCategory()
        {
            List<string> items = GetListOfDescription<FixtureOn>();
            return items;
        }


        public static List<string> getNodeTypes()
        {
            List<string> items = GetListOfDescription<NodeTypes>();
            return items;
        }
        #endregion

        #region ButtonAction Enums

        public enum ButtonStatus
        {
            [StringValue("SendForApproval")]
            SendForApproval,
            [StringValue("Retract")]
            Retract,
            [StringValue("Approve")]
            Approve,
            [StringValue("Revoke")]
            Revoke,
            [StringValue("Return")]
            Return,
            [StringValue("Revise")]
            Revise,
            [StringValue("Save")]
            Save,
            [StringValue("Submit")]
            Submit,
            [StringValue("Fetch")]
            Fetch,
            [StringValue("CRUD")]
            CRUD
        }
        #endregion

        #region TCP Enums
        public enum QueryType
        {
            [StringValue("Drawing Interpretation")]
            DrawingInterpretation,
            [StringValue("DCR")]
            DCR,
            [StringValue("Weld Seam")]
            WeldSeam,
            [StringValue("Material Change")]
            MaterialChange,
            [StringValue("Manufacturing convenience")]
            Manufacturingconvenience,
            [StringValue("Material Clearance")]
            MaterialClearance,
        }
        public enum TCPHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted to Addressee")]
            SubmittedToAddresse,
            [StringValue("Submitted to Assignee")]
            SubmittedToAssignee,
            [StringValue("Responded by Assignee")]
            RespondedByAssignee,
            [StringValue("Responded by Addressee")]
            RespondedByAddressee,
            [StringValue("Closed")]
            Closed,
        }
        #endregion

        #region GTM Enums

        public enum ActionType
        {
            [StringValue("Copy")]
            Copy,
            [StringValue("Insert")]
            Insert,
            [StringValue("Update")]
            Update,
            [StringValue("Delete")]
            Delete
        }
        public enum GTMStatus
        {
            [StringValue("Committed")]
            Committed,
        }

        #endregion

        #region Actions
        public enum Actions
        {
            [StringValue("add")]
            add,
            [StringValue("edit")]
            edit,
        }
        #endregion

        #region SeamList Seam Category
        public enum SeamCategory
        {
            [StringValue("LW")]
            LongSeam,
            [StringValue("CW")]
            CircSeam,
            [StringValue("NW")]
            NozzleWeld
        }
        #endregion

        #region Proposal Handover
        public enum ProductionCenter
        {
            [StringValue("Powai-PCC-2")]
            PowaiPCC2,
            [StringValue("Hazira - PCC-1")]
            HaziraPCC1,
            [StringValue("Hazira - PCC-3A Covered")]
            HaziraPCC3ACovered,
            [StringValue("Hazira - PCC-3A Opened")]
            HaziraPCC3AOpened,
            [StringValue("Hazira - PCC-3B")]
            HaziraPCC3B,
            [StringValue("Hazira - PCC4")]
            HaziraPCC4,
            [StringValue("VHEW - Metalic")]
            VHEWMetalic,
        }
        #endregion

        #region
        public enum MonthlyReports
        {
            [StringValue("RT COMP.WELD")]
            RT,
            [StringValue("UT COMP. WELD")]
            UT,
            [StringValue("A-Scan, TOFD & PA UT")]
            AScan,
            [StringValue("OVERLAY RT & UT")]
            OVERLAY
        }

        public enum QuarterlyReports
        {
            [StringValue("RT")]
            RT,
            [StringValue("UT")]
            UT,
            [StringValue("OVERLAY")]
            OVERLAY
        }

        public enum ddlQuarter
        {
            [StringValue("Quarter 1")]
            Q1,
            [StringValue("Quarter 2")]
            Q2,
            [StringValue("Quarter 3")]
            Q3,
            [StringValue("Quarter 4")]
            Q4
        }

        #endregion

        #region IPI Reports
        public enum IPIReports
        {
            [StringValue("1")]
            QID,
            [StringValue("2")]
            TP,
            [StringValue("3")]
            SWP,
            [StringValue("4")]
            GeneralNotes,
            [StringValue("5")]
            SeamWiseWeldersWelding,
            [StringValue("6")]
            SeamICL,
            [StringValue("7")]
            PrintTestPlan,
            [StringValue("8")]
            SeamStatus,
            [StringValue("9")]
            SeamwiseGroundSpotGPAReportForOverlay,
            [StringValue("10")]
            SeamStageStatus,
            [StringValue("11")]
            InspectionEntryAttendance,
            [StringValue("12")]
            ChemicalTest,
            [StringValue("13")]
            PMITest,
            [StringValue("14")]
            HardnessTest,
            [StringValue("15")]
            FerriteTest,
            [StringValue("16")]
            RepairSummary,
            [StringValue("17")]
            AI_horizontal,
            [StringValue("18")]
            ICL_DPP,
            [StringValue("19")]
            InspectionDashboard_BIReport,
            [StringValue("20")]
            Seam_StageClearanceAnalysis_Report,
            [StringValue("21")]
            ICLDashbord_BIReport,
            [StringValue("22")]
            NCRDashbord_BIReport,

            [StringValue("24")]
            InspectionRejectionAnalysis,

            [StringValue("25")]
            ProjectSeamStatus,

            [StringValue("26")]
            WebBasedProtocolStatus
        }

        public enum IPI_NDEReports
        {
            [StringValue("1")]
            Annexure_II,
            [StringValue("2")]
            Seam_Status,
            [StringValue("3")]
            MT_Report,
            [StringValue("4")]
            PT_Report,
            [StringValue("5")]
            NDE_DPP,
            [StringValue("6")]
            Request_Details,
            [StringValue("7")]
            Daily_Attended_Report,
            [StringValue("8")]
            Repair_Data,
            [StringValue("9")]
            Reshoot_Report,
            [StringValue("10")]
            TPIWiseSummary_Report,
            [StringValue("11")]
            Log_For_PT_MT,
            [StringValue("12")]
            Annexure_II_TPI
        }

        #endregion

        #region PDIN DimensionTracker Data
        public enum PDIN_DimensionTrackerData
        {
            [StringValue("1")]
            Shell_Shrinkage_Data,
            [StringValue("2")]
            Head_Shrinkage_Data,
            [StringValue("3")]
            Shell_Ovality_Data,
            [StringValue("4")]
            Shell_Length_Data,
            [StringValue("5")]
            Nozzle_Elevation_Data
        }
        #endregion

        #region MIP
        public enum MIPHeaderStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Returned")]
            Returned,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Released")]
            Released,
            [StringValue("Superseded")]
            Superseded,
        }
        public enum MIPLineStatus
        {
            [StringValue("Added")]
            Added,
            [StringValue("Deleted")]
            Deleted,
            [StringValue("Modified")]
            Modified,
            [StringValue("Approved")]
            Approved,
            [StringValue("Released")]
            Released,
            [StringValue("Superseded")]
            Superseded,
            [StringValue("Ready to Offer")]
            ReadyToOffer,
        }

        public enum MIPInspectionStatus
        {
            [StringValue("Ready to Offer")]
            ReadyToOffer,
            [StringValue("Under Inspection")]
            UnderInspection,
            [StringValue("Cleared")]
            Cleared,
            [StringValue("Rejected")]
            Rejected,
            [StringValue("Returned")]
            Returned,
            [StringValue("NA")]
            Not_Applicatble,
        }
        #endregion

        #region PBP
        public enum PBPIndexTitle
        {
            //index type & index title
            [StringValue("Maintain Product Based Procedure")]
            MaintainPBP,
            [StringValue("Approve Product Based Procedure")]
            ApprovePBP
        }
        public enum PBPIndexType
        {
            //index type & index title
            [StringValue("m")]
            maintain,
            [StringValue("a")]
            approve,
            [StringValue("d")]
            display,
        }
        public enum PBPStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Returned")]
            Returned,
            [StringValue("Sent for Approval")]
            SentForApproval,
            [StringValue("Approved")]
            Approved,
            [StringValue("Cancel")]
            Cancel,
            [StringValue("Released")]
            Released,
            [StringValue("Superseded")]
            Superseded,
        }
        #endregion

        #region DEN
        public enum DENIndexTitle
        {
            //index type & index title
            [StringValue("Maintain D end Forming Load Plan")]
            MaintainFP,
            [StringValue("Maintain D end Forming Load Plan – Required Date")]
            MaintainFPRD,
            [StringValue("Display D end Forming Load Plan")]
            DisplayFP
        }
        public enum DENIndexType
        {
            //index type & index title
            [StringValue("m")]
            maintain,
            [StringValue("d")]
            display,
        }
        #endregion

        #region ESP
        public enum ESPNodeType
        {
            [StringValue("Project")]
            Project,
            [StringValue("LooseItem")]
            LooseItem,
            [StringValue("SpareItem")]
            SpareItem,
            [StringValue("Template")]
            Template,
            [StringValue("Equipment")]
            Equipment,
            [StringValue("Fabrication Section")]
            Section,
            [StringValue("Assembly")]
            Assembly,
            [StringValue("Self")]
            Self,
            [StringValue("Sub Assembly")]
            SubAssembly,
            [StringValue("Internal Sub Assembly")]
            InternalAssembly,
            [StringValue("External Sub Assembly")]
            ExternalAssembly,
            [StringValue("Nozzel Sub Assembly")]
            NozzelAssembly,
            [StringValue("MTC Sub Assembly")]
            MTCSubAssembly,
            [StringValue("PTC Sub Assembly")]
            PTCSubAssembly,
            [StringValue("PWHT-Section")]
            PWHTSection
        }

        public enum ColorFlagForESP
        {
            [StringValue("GR")]
            Green,
            [StringValue("YE")]
            Yellow,
            [StringValue("RE")]
            Red,
            [StringValue("VI")]
            Violet,
            [StringValue("GY")]
            Grey,
            [StringValue("OR")]
            Orange,
            [StringValue("DE")]
            Default,
            [StringValue("BL")]
            Blue
        }

        public enum ESPMaterialDeliveryStatus
        {
            [StringValue("Request not generated")]
            Requestnotgenerated,
            [StringValue("Request Generated")]
            RequestGenerated,
            [StringValue("Request to be regenerated")]
            Requesttoberegenerated,
            [StringValue("Receipt Confirmation Balance")]
            ReceiptConfirmationBalance,
            [StringValue("Material Received")]
            MaterialReceived,
            [StringValue("Delivery Balance")]
            DeliveryBalance,
            [StringValue("Delivered")]
            Delivered
        }

        public enum ESPNodeGenerateFor
        {
            [StringValue("Merge Planner And Designer")]
            MergePlannerAndDesigner,
            [StringValue("Planner")]
            Planner,
            [StringValue("Designer")]
            Designer
        }

        public enum ESPDocumentType
        {
            [StringValue("HTR")]
            HTR,
            [StringValue("DIN")]
            DIN
        }
        #endregion

        public enum BLDNumber
        {
            [StringValue("2")]
            BU,
            [StringValue("1")]
            LOC,
            [StringValue("3")]
            DEP
        }

        public enum LineStatusFDMS
        {
            [StringValue("Edit")]
            Assigned,
            [StringValue("Submitted")]
            Submitted,
            [StringValue("Rejected")]
            Rejected,
            [StringValue("Accepted")]
            Accepted,
        }
        public enum yesno
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No
        }
        public enum yesna
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("NA")]
            No
        }

        public enum degree
        {
            [StringValue("°C")]
            C,
            [StringValue("°F")]
            F,
            [StringValue("°K")]
            K
        }
        public enum AppVersionType
        {
            [StringValue("Features")]
            Features,
            [StringValue("Enhancements")]
            Enhancements,
            [StringValue("Bug Fix")]
            BugFix
        }
        public enum AppVersionStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Released")]
            Released
        }
        public enum YesNoNA
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
            [StringValue("NA")]
            NA
        }

        public enum ProtocolRT
        {
            [StringValue("100%")]
            RT100,
            [StringValue("SPOT")]
            SPOT,
            [StringValue("NIL")]
            NIL,

        }
        public enum ProtocolUT
        {
            [StringValue("100%")]
            UT100,
            [StringValue("SPOT")]
            SPOT,
            [StringValue("NIL")]
            NIL,
        }
        public enum PassFail
        {
            [StringValue("Pass")]
            Pass,
            [StringValue("Fail")]
            Fail
        }

        public enum ddNumbers
        {
            [StringValue("1")]
            one,
            [StringValue("2")]
            two,
            [StringValue("3")]
            three,
            [StringValue("4")]
            four,
            [StringValue("5")]
            five,
            [StringValue("6")]
            six,
            [StringValue("7")]
            seven,
            [StringValue("8")]
            eight,
            [StringValue("9")]
            nine,
            [StringValue("10")]
            ten
        }
        public enum HeatTreatment
        {
            [StringValue("Pre- Heat Treatment")]
            Pre,
            [StringValue("Post – Heat Treatment")]
            Post,
            [StringValue("NA")]
            NA
        }
        public enum gender
        {
            [StringValue("Male")]
            Male,
            [StringValue("Female")]
            Female,
        }

        public enum PTMethods
        {
            [StringValue("visible")]
            VISIBLE,
            [StringValue("fluorescent")]
            FLUORESCENT,
            [StringValue("water washable")]
            WATER_WASHABLE
        }

        public enum MTMethods
        {
            [StringValue("visible")]
            VISIBLE,
            [StringValue("fluorescent")]
            FLUORESCENT
        }

        public enum MTParticleType
        {
            [StringValue("Dry & Wet")]
            DRY
        }

        public enum MTTechniques
        {
            [StringValue("Yoke")]
            YOKE,
            [StringValue("Prode")]
            Prode
        }

        public enum ScanningRequirement
        {
            [StringValue("Automatic")]
            Automatic,
            [StringValue("Semi-Automatic")]
            Semi_Automatic
        }

        public enum productCategory
        {
            [StringValue("EO Reactor")]
            EOReactor,
            [StringValue("Plate Reactor")]
            PlateReactor,
            [StringValue("Forge Reactor")]
            ForgeReactor,
            [StringValue("Ammonia Convertor")]
            AmmoniaConvertor,
            [StringValue("Methanol Convertor")]
            MethanolConvertor,
            [StringValue("Coke Drum")]
            CokeDrum,
            [StringValue("FCC RR Package")]
            FCCRRPackage,
            [StringValue("HP Heat Exchangers")]
            HPHeatExchangers,
            [StringValue("Feed Water Heater")]
            FeedWaterHeater,
            [StringValue("Condenser")]
            Condenser,
            [StringValue("Gasifier")]
            Gasifier,
            [StringValue("Thick LNG Vessels")]
            ThickLNGVessels,
            [StringValue("Columns")]
            Columns,
            [StringValue("Urea Equipment")]
            UreaEquipment,
            [StringValue("Boiler Package")]
            BoilerPackage,
            [StringValue("Ammonia Convertor Basket")]
            AmoniaConvertorBasket,
            [StringValue("Reactor Internals")]
            ReactorInternals,
            [StringValue("FCC Cyclones")]
            FCCCyclones,
            [StringValue("Canisters")]
            Canisters,
            [StringValue("Gasifier PV")]
            GasifierPV,
            [StringValue("Gasifier Internal")]
            GasifierInternal
            //[StringValue("Reformer")]
            //Reformer
        }

        public enum HPHeatExchangers
        {
            [StringValue("Screw Plug Heat Exchanger - HH Type")]
            ScrewPlugHeatExchangerHHType,
            [StringValue("Screw Plug Heat Exchanger - HL Type")]
            ScrewPlugHeatExchangerHLType,
            [StringValue("Modified Screw Plug Heat Exchanger")]
            ModifiedScrewPlugHeatExchanger,
            [StringValue("Helical Baffles Tube Bundle Exchanger")]
            HelicalBafflesTubeBundleExchanger,
            [StringValue("D Type Exchanger")]
            DTypeExchanger,
            [StringValue("KBR 120C")]
            KBR120C,
            [StringValue("KBR 101C")]
            KBR101C,
            [StringValue("KBR 102C")]
            KBR102C,
            [StringValue("Sulphur Condenser")]
            SulphurCondenser,
            [StringValue("Hot Heat Exchanger")]
            HotHeatExchanger,
            [StringValue("Air cooled Exchangers")]
            AircooledExchangers,
            [StringValue("UHX - Exchangers")]
            UHXExchangers,
            [StringValue("TLX - Exchangers")]
            TLXExchangers,
            [StringValue("DTS strips")]
            DTSstrips
        }
        public enum BoilerPackage
        {
            [StringValue("R.G Boiler")]
            RGBoiler,
            [StringValue("C.G Boiler")]
            CGBoiler,
            [StringValue("P.G Boiler")]
            PGBoiler,
            [StringValue("Methanol Plant Boiler")]
            MethanolPlantBoiler,
            [StringValue("SRU Boiler")]
            SRUBoiler,
            [StringValue("Hydrogen Plant Boiler")]
            HydrogenPlantBoiler,
            [StringValue("Steam Drum")]
            SteamDrum,
            [StringValue("Synloop Boiler")]
            SynloopBoiler,
            [StringValue("Steam super Heater")]
            SteamsuperHeater,
            [StringValue("Piping System")]
            PipingSystem,
            [StringValue("Cold Collector Package (Inlet Distributor, Hair pin) / Transfer line")]
            ColdCollectorPackage
        }
        public enum UreaEquipment
        {
            [StringValue("Carbamate Separator")]
            CarbamateSeparator,
            [StringValue("Urea Reactor")]
            UreaReactor,
            [StringValue("Urea Stripper")]
            UreaStripper,
            [StringValue("Carbamate Condenser")]
            CarbamateCondenser
        }
        public enum FeedWaterHeater
        {
            [StringValue("Low Pressure Heater")]
            LowPressureHeater,
            [StringValue("High Pressure Heater")]
            HighPressureHeater,
            [StringValue("Duplex Heater")]
            DuplexHeater,
            [StringValue("Desuperheater")]
            Desuperheater
        }

        public enum productCategoryFXR
        {
            [StringValue("Coke Drum")]
            CokeDrum,

            [StringValue("Columns")]
            Columns,
            [StringValue("EO Reactor")]
            EOReactor,
            [StringValue("Forge Reactor")]
            ForgeReactor,
            [StringValue("FCC RR Package")]
            FCCRRPackage,



            [StringValue("Urea Equipment")]
            UreaEquipment,

        }
        public enum FurnaceCharge
        {
            [StringValue("Ageing")]
            Ageing,
            [StringValue("Hardening")]
            Hardening,
            [StringValue("High Temprature Tempering")]
            HighTempratureTempering,
            [StringValue("Hot Edge Breaking")]
            HotEdgeBreaking,
            [StringValue("Hot Forming")]
            HotForming,
            [StringValue("Hot Re-Rolling")]
            HotReRolling,
            [StringValue("Hot Rolling")]
            HotRolling,
            [StringValue("Immidiate Normalizing")]
            ImmidiateNormalizing,
            [StringValue("Intermediate Stress Relieving")]
            IntermediateStressRelieving,
            [StringValue("Intermediate Tempering")]
            IntermediateTempering,
            [StringValue("Local Ageing")]
            LocalAgeing,
            [StringValue("Local Stress Relieving")]
            LocalStressRelieving,
            [StringValue("Normalising")]
            Normalising,
            [StringValue("Normalising with Accelerated Cooling")]
            NormalisingwithAcceleratedCooling,
            [StringValue("Other")]
            Other,
            [StringValue("Precipitation Hardening")]
            PrecipitationHardening,
            [StringValue("Quenching")]
            Quenching,
            [StringValue("Shrink Fitting")]
            ShrinkFitting,
            [StringValue("soln.Annealing")]
            solnAnnealing,
            [StringValue("Special HT")]
            SpecialHT,
            [StringValue("Stress Relieving")]
            StressRelieving,
            [StringValue("Tempering")]
            Tempering,
            [StringValue("Warm Edge Breaking")]
            WarmEdgeBreaking,
            [StringValue("Warm Rolling / Warm Re- Rolling")]
            WarmRolling_WarmReRolling
        }
        public enum FurnaceTypes
        {
            [StringValue("Auxilery Circular")]
            CIRCULAR,
            [StringValue("Auxilery Service HFS 4A")]
            HFS4A,
            [StringValue("Auxilery Service HFS 4B")]
            HFS4B,
            [StringValue("Auxilery Service HFS 5A")]
            HFS5A,
            [StringValue("Auxilery Service HFS 5B")]
            HFS5B,
            [StringValue("Auxilery Service HFS 6A")]
            HFS6A,
            [StringValue("Auxilery Service HFS 6B")]
            HFS6B,
            [StringValue("Auxilery Service MFS-1")]
            MFS1,
            [StringValue("Auxilery Service MFS-2")]
            MFS2,
            [StringValue("Auxilery Service MFS-3")]
            MFS3,
            [StringValue("Auxilery Service MFS-4")]
            MFS4,
            [StringValue("BAGSVIG")]
            BAGSVIG,
            [StringValue("INDOTHERM")]
            INDOTHERM,
            [StringValue("LEMF LONG")]
            LEMFLONG,
            [StringValue("LEMF MODULAR 1")]
            MODULAR1,
            [StringValue("LEMF PIPE")]
            LEMFPIPE,
            [StringValue("LEMF TALL")]
            LEMFTALL,
            [StringValue("MAIN FURNACE (MF)")]
            MF,
            [StringValue("NES-2")]
            NES2,
            [StringValue("OTHER")]
            OTHER,
            [StringValue("OUTSIDE")]
            OUTSIDE,
            [StringValue("OUTSIDE - HTE")]
            HTE,
            [StringValue("OUTSIDE - HW")]
            HW,
            [StringValue("OUTSIDE - KHS")]
            KHS,
            [StringValue("PLATE HEATING FURNACE (PH)")]
            PH,
            [StringValue("SHOP HFS-1")]
            HFS1,
            [StringValue("SHOP PFS")]
            PFS,
            [StringValue("Therelek Coupen Heat Treatment Furnace")]
            TCHTF,
            [StringValue("LEMF MODULAR 2")]
            LEMFMODULAR2
        }

        public enum MailLocation
        {
            [StringValue("http://viemqs06")]
            localPath,
            [StringValue("http://viemqs06")]
            ServerPath,
        }
        public enum FDMSStatus
        {
            [StringValue("Draft")]
            DRAFT,
            [StringValue("Final Document Requirement")]
            FDR,
            [StringValue("Document Assignment")]
            DocAssign,
            [StringValue("SCN Date Verification")]
            SCNVer,
            [StringValue("Document Submission and Verification")]
            DocVeri,
            [StringValue("Document Submission to PMG")]
            QAAccept,
            [StringValue("Document Acceptance by PMG")]
            PMGAccept,
            [StringValue("Completed")]
            Completed,
            [StringValue("Returned to Document Submission to PMG")]
            RetQA,
            [StringValue("Returned to Document Submission and Verification")]
            RetDocVeri,
            [StringValue("Returned to SCN Date Verification")]
            RetSCN,
            [StringValue("Returned to Document Assignment")]
            RetDocAssign,
            [StringValue("Returned to Final Document Requirement")]
            RetFDR,
            [StringValue("Returned to Draft")]
            RetDraft,
        }

        public enum KOMStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("KOM Sent for Approval")]
            KOMSent,
            [StringValue("KOM Approved")]
            KOMApprove,
            [StringValue("MKOM Draft")]
            MKOMDraft,
            [StringValue("MKOM Sent for Approval")]
            MKOMSent,
            [StringValue("MKOM Approved")]
            MKOMApprove,

            [StringValue("KOM")]
            KOM,
            [StringValue("KOM Sent for Approval to PMG")]
            KOMSentPMG,
            [StringValue("KOM Approved by PMG")]
            KOMApprovePMG,
            [StringValue("KOM Approved by TOC")]
            KOMApproveTOC,
            [StringValue("KOM Completed")]
            KOMCompleted,

            [StringValue("MKOM")]
            MKOM,
            [StringValue("MKOM Sent for Approval to PMG")]
            MKOMSentPMG,
            [StringValue("MKOM Approved by PMG")]
            MKOMApprovePMG,
            [StringValue("MKOM Approved by TOC")]
            MKOMApproveTOC,
            [StringValue("MKOM Completed")]
            MKOMCompleted,
        }
        public enum NumaricValidation
        {
            [StringValue("Equal")]
            Equal,
            [StringValue("Does Not Equal")]
            DoesNotEqual,
            [StringValue("Starts With")]
            StartsWith,
            [StringValue("Greater Then or Equals")]
            GreaterThenorEquals,
            [StringValue("Less Then or Equals")]
            LessThenorEquals,
            [StringValue("Greater Then")]
            GreaterThen,
            [StringValue("Less Then")]
            LessThen,
            [StringValue("Contains")]
            Contains,
            [StringValue("Does Not Contains")]
            DoesNotContains,
            [StringValue("Ends With")]
            EndsWith,
            [StringValue("Does Not Ends With")]
            DoesNotEndsWith,
            [StringValue("Is Empty")]
            IsEmpty,
            [StringValue("Is Not Empty")]
            IsNotEmpty
        }

        public enum IMSStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted")]
            Submitted,
            [StringValue("Issued and Acknowledged")]
            IssuedAndAcknowledged,
            [StringValue("Returned")]
            Returned,
            [StringValue("Received")]
            Received,
            [StringValue("Closed")]
            Closed,
        }
        public enum FunctionCSR
        {
            [StringValue("WE")]
            WE,
            [StringValue("MCC")]
            MCC,
            [StringValue("PCC")]
            PCC,
            [StringValue("NDE")]
            NDE,
            [StringValue("QA/QC")]
            QAQC,
            [StringValue("PMG")]
            PMG,
            [StringValue("DCC")]
            DCC,
        }

        #region ICC ddl

        public enum ICC_Status
        {
            [StringValue("Not-Created")]
            Not_Created,
            [StringValue("Draft")]
            Draft,
        }

        public enum ICC_NatureOfItem
        {
            [StringValue("Job")]
            Job,
            [StringValue("Non-Job")]
            Non_Job,
            [StringValue("MPI (Manual Purchase and Inventorized for Job)")]
            MPI
        }

        public enum ICC_ItemType_Job
        {
            [StringValue("TBD")]
            TBD,
        }
        public enum ICC_ItemType_Non_Job
        {
            [StringValue("Auto Reorder (Inventorized)")]
            AutoReorder,
            [StringValue("Manual Purchase (Non-Inventorized)")]
            ManualPurchase
        }
        public enum ICC_ItemType_MPI
        {
            [StringValue("Welding Consumable")]
            Welding_Consumable,
            [StringValue("Machine Shop")]
            Machine_Shop
        }

        public enum ICC_ItemSubType_AutoReorder
        {
            [StringValue("FOS")]
            FOS,
            [StringValue("MNT")]
            MNT,
            [StringValue("MCS")]
            MCS
        }
        public enum ICC_ItemSubType__ManualPurchase_CART
        {
            //[StringValue("CRT")]
            //CRT,
            [StringValue("FOS")]
            FOS,
            [StringValue("MNT")]
            MNT,
            [StringValue("MCS")]
            MCS
        }

        public enum ICC_ItemSubType__Welding_Consumable
        {
            [StringValue("WEL")]
            WEL,
        }

        public enum ICC_ItemSubType__Machine_Shop
        {
            [StringValue("MSP")]
            MSP,
        }
        #endregion

        public enum WCFormula
        {
            [StringValue("=IF(LOWER(param1) = 'o', param5, IF(LOWER(param1) = 's', ((PI()) * 7.85 / 4000000) * ((param2 + 2 * param4) ^ 2 - (param2 ^ 2)) * param3, IF(LOWER(param1) = 't', ((PI()) * 7.85 / 4000000) * ((param2 ^ 2) - (param2 - 2 * param4) ^ 2) * param3, IF(LOWER(param1) = 'ed', ((PI()) * 7.85 / 4000000) * ((1.25 * param2) ^ 2 * param4), IF(LOWER(param1) = 'ts', ((PI() * 7.85 / 4000000) * (param2 - 2 * param3 + PI() * param3) ^ 2 * param4), IF(LOWER(param1) = 'c', ((PI() * 7.85 / 1000000) * ((param2 + param5) / 2 + 2 * param4) * param4 * param3), IF(param1 = '', '')))))))")]
            Formula1,
            [StringValue("=IF(NOT(param1='')=TRUE,param1*param2,'')")]
            Formula2,
        }

        public enum MALProtocolType
        {
            [StringValue("Heat treatment summary")]
            HeatTreatment,
            //[StringValue("Name Plate")]
            [StringValue("Name Plate Fixing Report")]
            NamePlate,
            [StringValue("Pickling and passivation report")]
            PicklingPassivation,
            [StringValue("Rub-off Report")]
            RubOff,
            [StringValue("Stenciling Report")]
            Stenciling,
            [StringValue("Blasting protocol")]
            Blasting,
            [StringValue("coating protocol")]
            Coating,
            [StringValue("DFT protocol")]
            DFT,
            [StringValue("Final Activities Report")]
            FinalActivities,
            [StringValue("L&T Certificate")]
            LTCertificate,
            [StringValue("HELIUM LEAK TEST REPORT")]
            Heliumleaktest,
            [StringValue("HYDROSTATIC TEST REPORT")]
            HydrostaticTest,
            [StringValue("PNEUMATIC TEST REPORT")]
            PneumaticTest,
            [StringValue("REPORT FOR N2 FILLING INSPECTION")]
            N2FillingInspection
        }

        
        public static List<string> getQueryType()
        {
            List<string> items = GetListOfDescription<QueryType>();
            return items;
        }
        public static List<string> getFunctionCSR()
        {
            List<string> items = GetListOfDescription<FunctionCSR>();
            return items;
        }
        public static List<string> getFurnacetypes()
        {
            List<string> items = GetListOfDescription<FurnaceTypes>();
            return items;
        }
        public static List<string> getFurnaceCharge()
        {
            List<string> items = GetListOfDescription<FurnaceCharge>();
            return items;
        }
        public static List<string> getGender()
        {
            List<string> items = GetListOfDescription<gender>();
            return items;
        }
        public static List<string> getDdlNumber()
        {
            List<string> items = GetListOfDescription<ddNumbers>();
            return items;
        }
        public static List<string> getyesno()
        {
            List<string> items = GetListOfDescription<yesno>();
            return items;
        }

        public static List<string> getOutsideInside()
        {
            List<string> items = GetListOfDescription<OutsideInside>();
            return items;
        }

        public static List<string> getLocationLeftRight()
        {
            List<string> items = GetListOfDescription<LocationLeftRight>();
            return items;
        }
        public static List<string> getAppVersionType()
        {
            List<string> items = GetListOfDescription<AppVersionType>();
            return items;
        }
        public static List<string> getYesNoNA()
        {
            List<string> items = GetListOfDescription<YesNoNA>();
            return items;
        }

        public static List<string> getCFARCategory()
        {
            List<string> items = GetListOfDescription<CFARCategory>();
            return items;
        }
        public static List<string> getTypeofweld()
        {
            List<string> items = GetListOfDescription<Typeofweld>();
            return items;
        }
        public static List<string> getWEPBy()
        {
            List<string> items = GetListOfDescription<WEPBy>();
            return items;
        }
        public static List<string> getCurrent()
        {
            List<string> items = GetListOfDescription<Current>();
            return items;
        }
        public static List<string> getPolarity()
        {
            List<string> items = GetListOfDescription<Polarity>();
            return items;
        }
        public static List<string> getProtocolRT()
        {
            List<string> items = GetListOfDescription<ProtocolRT>();
            return items;
        }
        public static List<string> getProtocolUT()
        {
            List<string> items = GetListOfDescription<ProtocolUT>();
            return items;
        }
        public static List<string> getPassFail()
        {
            List<string> items = GetListOfDescription<PassFail>();
            return items;
        }
        public static List<string> getHeatTreatment()
        {
            List<string> items = GetListOfDescription<HeatTreatment>();
            return items;
        }
        public static List<string> getProductCategory()
        {
            List<string> items = GetListOfDescription<productCategory>();
            return items;
        }
        public static List<string> getEquipmentCategory(string Product)
        {
            List<string> items = new List<string>();
            if (Product == productCategory.HPHeatExchangers.GetStringValue())
                items = GetListOfDescription<HPHeatExchangers>();
            else if (Product == productCategory.BoilerPackage.GetStringValue())
                items = GetListOfDescription<BoilerPackage>();
            else if (Product == productCategory.UreaEquipment.GetStringValue())
                items = GetListOfDescription<UreaEquipment>();
            else if (Product == productCategory.FeedWaterHeater.GetStringValue())
                items = GetListOfDescription<FeedWaterHeater>();
            else if (Product == productCategory.MethanolConvertor.GetStringValue())
                items.Add("Methanol Convertor");
            else if (Product == productCategory.AmmoniaConvertor.GetStringValue())
                items.Add("Ammonia Convertor");
            else if (Product == productCategory.AmoniaConvertorBasket.GetStringValue())
                items.Add("Ammonia converter basket");
            else if (Product == productCategory.PlateReactor.GetStringValue())
                items.Add("Muiltiwall Vessels");
            else if (Product == productCategory.Condenser.GetStringValue())
                items.AddRange(new string[] { "Surface Condensor", "Deaerator" });
            else if (Product == productCategory.Columns.GetStringValue())
                items.Add("Towers / Columns (with tray)");
            return items;
        }
        public static List<string> getProductCategoryFXR()
        {
            List<string> items = GetListOfDescription<productCategoryFXR>();
            return items;
        }

        public static List<string> getIMBTeamFormation()
        {
            List<string> items = GetListOfDescription<TeamFormation>();
            return items;
        }

        public static List<string> PMGApprovalList()
        {
            List<string> items = GetListOfDescription<PMGApproval>();
            return items;
        }

        public static List<string> GetListOfDescription<T>() where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<Enum>().Select(x => x.GetStringValue()).ToList();
        }

        public static List<string> GetPTMethods()
        {
            List<string> items = GetListOfDescription<PTMethods>();
            return items;
        }

        public static List<string> GetScanningRequirement()
        {
            List<string> items = GetListOfDescription<ScanningRequirement>();
            return items;
        }

        public static List<string> GetMTMethods()
        {
            List<string> items = GetListOfDescription<MTMethods>();
            return items;
        }

        public static List<string> GetPDHComponentsMethods()
        {
            List<string> items = GetListOfDescription<PDHComponents>();
            return items;
        }

        public static List<string> GetITPMaterialCategory()
        {
            List<string> items = GetListOfDescription<ITPMaterialCategory>();
            return items;
        }

        public static List<string> GetMTParticleType()
        {
            List<string> items = GetListOfDescription<MTParticleType>();
            return items;
        }

        public static List<string> GetMTTechniques()
        {
            List<string> items = GetListOfDescription<MTTechniques>();
            return items;
        }

        public static List<string> GetPlanList()
        {
            List<string> items = GetListOfDescription<PlanList>();
            return items;
        }
        public static List<string> getTravelerIdentification()
        {
            List<string> items = GetListOfDescription<TravelerIdentificationDropdown>();
            return items;
        }

        #region ICC 
        public static List<string> getICC_NatureOfItem()
        {
            List<string> items = GetListOfDescription<ICC_NatureOfItem>();
            return items;
        }
        public static List<string> getICC_ItemType_Job()
        {
            List<string> items = GetListOfDescription<ICC_ItemType_Job>();
            return items;
        }
        public static List<string> getICC_ItemType_Non_Job()
        {
            List<string> items = GetListOfDescription<ICC_ItemType_Non_Job>();
            return items;
        }

        public static List<string> getICC_ItemType_MPI()
        {
            List<string> items = GetListOfDescription<ICC_ItemType_MPI>();
            return items;
        }

        public static List<string> getICC_ItemSubType_AutoReorder()
        {
            List<string> items = GetListOfDescription<ICC_ItemSubType_AutoReorder>();
            return items;
        }
        public static List<string> getICC_ItemSubType__ManualPurchase_CART()
        {
            List<string> items = GetListOfDescription<ICC_ItemSubType__ManualPurchase_CART>();
            return items;
        }

        public static List<string> getICC_ItemSubType__Welding_Consumable()
        {
            List<string> items = GetListOfDescription<ICC_ItemSubType__Welding_Consumable>();
            return items;
        }

        public static List<string> getICC_ItemSubType__Machine_Shop()
        {
            List<string> items = GetListOfDescription<ICC_ItemSubType__Machine_Shop>();
            return items;
        }
        #endregion

        public static string GetStringValue(this Enum value)
        {
            // Get the type
            Type type = value.GetType();

            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(StringValueAttribute), false) as StringValueAttribute[];

            // Return the first if there was a match.
            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }

        public static T StringValueToEnum<T>(string strValue)
        {
            T returnResult = default(T);
            foreach (var item in Enum.GetValues(typeof(T)))
            {
                if (GetStringValue((Enum)item).Equals(strValue, StringComparison.InvariantCultureIgnoreCase))
                {
                    returnResult = (T)item;
                    break;
                }
            }
            return returnResult;
        }

        public static List<string> getBasicCalibrationBlock()
        {
            List<string> items = GetListOfDescription<NDEBasicCalibrationBlock>();
            return items;
        }
        public static List<string> getSeamSpotResultType()
        {
            List<string> items = GetListOfDescription<SpotResults>();
            return items;
        }
        public static List<string> getNDEScannerType()
        {
            List<string> items = GetListOfDescription<NDEScannerType>();
            return items;
        }
        public static List<string> getNDEScanType()
        {
            List<string> items = GetListOfDescription<NDEScanType>();
            return items;
        }

        public static List<string> getARMType()
        {
            List<string> items = GetListOfDescription<ARMType>();
            return items;
        }

        public static List<string> getMALProtocolType()
        {
            List<string> items = GetListOfDescription<MALProtocolType>();
            return items;
        }

        public static List<string> getNDEEncoderType()
        {
            List<string> items = GetListOfDescription<NDEEncoderType>();
            return items;
        }
        public static List<string> getDGSyesno()
        {
            List<string> items = GetListOfDescription<DGSDiagram>();
            return items;
        }

        public static List<string> getpclProductType()
        {
            List<string> items = GetListOfDescription<pclProductType>();
            return items;
        }

        public static List<string> GetTypeOfHTR()
        {
            List<string> items = GetListOfDescription<TypeOfHTR>();
            return items;
        }
        public static List<string> GetTypeOfTokenProcess()
        {
            List<string> items = GetListOfDescription<TypeOfTokenProcess>();
            return items;
        }
        public static List<string> GetSyncBOMData()
        {
            List<string> items = GetListOfDescription<SyncBOMData>();
            return items;
        }

        public static List<string> GetFurnace()
        {
            List<string> items = GetListOfDescription<Furnace>();
            return items;
        }
        public static List<string> GetMaxLoadingTemp()
        {
            List<string> items = GetListOfDescription<MaxLoadingTemp>();
            return items;
        }
        public static List<string> GetMinLoadingTemp()
        {
            List<string> items = GetListOfDescription<MinLoadingTemp>();
            return items;
        }
        public static List<string> GetFDMSStatus()
        {
            List<string> items = GetListOfDescription<FDMSStatus>();
            return items;
        }
        public static List<string> GetMCRStatus()
        {
            List<string> items = GetListOfDescription<MCRStatus>();
            return items;
        }

        public static List<string> GetUTTypes()
        {
            List<string> items = GetListOfDescription<UTTypes>();
            return items;
        }

        public static List<string> GetMinRateOfHeating()
        {
            List<string> items = GetListOfDescription<MinRateOfHeating>();
            return items;
        }

        public static List<string> GetMaxRateOfHeating()
        {
            List<string> items = GetListOfDescription<MaxRateOfHeating>();
            return items;
        }
        public static List<string> GetSoakingTimeTolerance()
        {
            List<string> items = GetListOfDescription<SoakingTimeTolerance>();
            return items;
        }
        public static List<string> GetMinRateOfCooling()
        {
            List<string> items = GetListOfDescription<MinRateOfCooling>();
            return items;
        }

        public static List<string> GetMaxRateOfCooling()
        {
            List<string> items = GetListOfDescription<MaxRateOfCooling>();
            return items;
        }
        public static List<string> GetCoolPart()
        {
            List<string> items = GetListOfDescription<CoolPart>();
            return items;
        }

        public static List<string> GetCoolBy()
        {
            List<string> items = GetListOfDescription<CoolBy>();
            return items;
        }

        public static List<string> GetCoolingThermocouplesVariation()
        {
            List<string> items = GetListOfDescription<CoolingThermocouplesVariation>();
            return items;
        }
        public static List<string> GetHeatingThermocouplesVariation()
        {
            List<string> items = GetListOfDescription<HeatingThermocouplesVariation>();
            return items;
        }

        public static List<string> GetStandardProcedure()
        {
            List<string> items = GetListOfDescription<StandardProcedure>();
            return items;
        }
        public static List<string> GetFurnaceBedDistance()
        {
            List<string> items = GetListOfDescription<FurnaceBedDistance>();
            return items;
        }
        public static List<string> GetWPSNo()
        {
            List<string> items = GetListOfDescription<WPSNo>();
            return items;
        }
        public static List<string> GetMinUnloadingTemp()
        {
            List<string> items = GetListOfDescription<MinUnloadingTemp>();
            return items;
        }
        public static List<string> GetMaxUnloadingTemp()
        {
            List<string> items = GetListOfDescription<MaxUnloadingTemp>();
            return items;
        }

        public static List<string> GetTreatment()
        {
            List<string> items = GetListOfDescription<Treatment>();
            return items;
        }
        public static List<string> GetVolTempOf()
        {
            List<string> items = GetListOfDescription<VolTempOf>();
            return items;
        }

        public static List<string> GetQualityIdApplicableCategory()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<QualityIDApplicable>();
            return items;
        }


        public static List<string> GetHTRStatus()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<HTRStatus>();
            return items;
        }


        public static List<string> GetInitiator()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<Initiator>();
            return items;
        }
        public static List<string> GetJCPTCfurnase()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<JCPTCfurnase>();
            return items;
        }
        public static List<string> GetInspector()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<Inspector>();
            return items;
        }

        public static List<string> GetProdCenter()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<ProductionCenter>();
            return items;
        }

        public static List<string> GetSeamTypeForCJC()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<AutoCJCSeamType>();
            return items;
        }

        public enum WeldType
        {
            [StringValue("Groove")]
            Groove,
            [StringValue("Fillet")]
            Fillet,
        }
        public enum WKGProcess
        {
            [StringValue("SMAW")]
            SMAW,
            [StringValue("GMAW")]
            GMAW,
            [StringValue("GTAW")]
            GTAW,
            [StringValue("FCAW")]
            FCAW,
            [StringValue("SAW")]
            SAW,
            [StringValue("ESSC")]
            ESSC,
            [StringValue("Hot Wire Tig")]
            HOTWIRETIG,
        }
        public static List<string> GetWKGProcess()
        {
            List<string> items = GetListOfDescription<WKGProcess>();
            return items;
        }

        public static List<string> GetWeldType()
        {
            List<string> items = GetListOfDescription<WeldType>();
            return items;
        }
        public static List<string> GetYieldStrength()
        {
            List<string> items = GetListOfDescription<YieldStrength>();
            return items;
        }

        public static List<string> GetLockingCleatMaterial()
        {
            List<string> items = GetListOfDescription<LockingCleatMaterial>();
            return items;
        }

        public static List<string> GetJC_Release()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<JC_Release>();
            return items;
        }

        public static List<string> GetTackWeldMaterial()
        {
            List<string> items = GetListOfDescription<TackWeldMaterial>();
            return items;
        }

        public static List<string> GetTackWeldYieldStrength()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<TackWeldYieldStrength>();
            return items;
        }
        public static List<string> GetPTC_Release()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<PTC_Release>();
            return items;
        }

        public static List<string> GetFixtureStatus()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FixtureStatus>();
            return items;
        }

        public static List<string> GetMonthlyReports()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<MonthlyReports>();
            return items;
        }
        public static List<string> GetQuarterlyReports()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<QuarterlyReports>();
            return items;
        }

        public static List<string> GetddlQuarter()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<ddlQuarter>();
            return items;
        }
        public static List<string> GetDeptAcknowledgements()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<PAMAcknowledgement>();
            return items;
        }

        public static List<string> GetDHTISR()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<DHTISR>();
            return items;
        }
        #region FXR
        public static List<string> GetFXRMaterialType()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRMaterialType>();
            return items;
        }

        public static List<string> GetFXRUnit()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRUnit>();
            return items;
        }

        public static List<string> GetFXRUnit2()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRUnit2>();
            return items;
        }

        public static List<string> GetFXRStructuralType()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRStructuralType>();
            return items;
        }

        public static List<string> GetFXRCategory()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRCategory>();
            return items;
        }

        public static List<string> GetFXRNDTRequirement()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRNDTRequirement>();
            return items;
        }

        public static List<string> GetFXRSubContractingType()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRSubContractingType>();
            return items;
        }

        public static List<string> GetFXRTypeofSaddle()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRTypeofSaddle>();
            return items;
        }

        public static List<string> GetFXRTypeofFixture()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRTypeofFixture>();
            return items;
        }

        public static List<string> GetFXRTypeofWork()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<FXRTypeofWork>();
            return items;
        }
        #endregion
        public enum ChecklistYesNo
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
            [StringValue("N/A")]
            NA,
        }
        /*public static string MakeDatatableForSearchs(string jsonSearchFilter)
        {

            JavaScriptSerializer serializer1 = new JavaScriptSerializer();
            string strWhereCondition = string.Empty;
            List<SearchFilter> searchflt = serializer1.Deserialize<List<SearchFilter>>(jsonSearchFilter);
            foreach (var item in searchflt)
            {
                if (!string.IsNullOrWhiteSpace(item.Value) && item.Value != "false")
                {
                    if (item.ColumnName.Contains(','))
                    {
                        
                        strWhereCondition += " and ( 1=0 ";
                       
                        foreach (var Cname in item.ColumnName.Split(','))
                        {
                            if (item.DataType == "string")
                            {
                                strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleCondition(item.FilterType, Cname, item.Value,"or");
                            }
                            else if (item.DataType == "date")
                            {

                                strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleConditionForNumeric(item.FilterType, "CONVERT(date, " + Cname + ")", item.Value, "or");
                            }
                            else
                            {
                                strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleConditionForNumeric(item.FilterType, Cname, item.Value, "or");
                            }
                           
                        }
                      
                        strWhereCondition +=  " ) ";
                    }

                    else
                    {

                        if (item.DataType == "string")
                        {
                            strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleCondition(item.FilterType, item.ColumnName, item.Value);
                        }
                        else if (item.DataType == "date")
                        {

                            strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleConditionForNumeric(item.FilterType, "CONVERT(date, " + item.ColumnName + ")", item.Value);
                        }
                        else
                        {
                            strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleConditionForNumeric(item.FilterType, item.ColumnName, item.Value);
                        }
                    }

                }
            }

            return strWhereCondition;

        }
        public static string MakeDatatableSearchWithMultipleCondition(string filterstatus, string column_name, string column_value,string column_type="string", string operation = "and")
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            if(column_type == "date")
                column_name = "CONVERT(date, " + column_name + ")";
            sb.Append(" " + operation + " ( " + column_name + " ");
            if (filterstatus == clsImplementationEnum.NumaricValidation.Equal.GetStringValue())
            {
                sb.Append("= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEqual.GetStringValue())
            {
                sb.Append("!= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.StartsWith.GetStringValue())
            {
                sb.Append("like '" + column_value + "%' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThenorEquals.GetStringValue())
            {
                sb.Append(">= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThenorEquals.GetStringValue())
            {
                sb.Append("<= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThen.GetStringValue())
            {
                sb.Append("> '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThen.GetStringValue())
            {
                sb.Append("< '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.Contains.GetStringValue())
            {
                sb.Append("like '%" + column_value + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotContains.GetStringValue())
            {
                sb.Append("not like '%" + column_value + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.EndsWith.GetStringValue())
            {
                sb.Append("like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEndsWith.GetStringValue())
            {
                sb.Append("not like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsEmpty.GetStringValue())
            {
                sb.Append("is null ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsNotEmpty.GetStringValue())
            {
                sb.Append("is not null ");
            }
            else // default by data type
            {
                if (column_type == "string")
                    sb.Append("like '%" + column_value + "%'");
                else
                    sb.Append("= '" + column_value + "' ");
            }

                sb.Append(") ");
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }
        public static string MakeDatatableSearchWithMultipleConditionForNumeric(string filterstatus, string column_name, string column_value, string operation="and")
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(" "+ operation + " ( " + column_name + " ");
            if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEqual.GetStringValue())
            {
                sb.Append("!= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.StartsWith.GetStringValue())
            {
                sb.Append("like '" + column_value + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThenorEquals.GetStringValue())
            {
                sb.Append(">= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThenorEquals.GetStringValue())
            {
                sb.Append("<= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThen.GetStringValue())
            {
                sb.Append("> '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThen.GetStringValue())
            {
                sb.Append("< '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.Contains.GetStringValue())
            {
                sb.Append("like '%" + column_value + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotContains.GetStringValue())
            {
                sb.Append("not like '%" + column_value + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.EndsWith.GetStringValue())
            {
                sb.Append("like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEndsWith.GetStringValue())
            {
                sb.Append("not like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsEmpty.GetStringValue())
            {
                sb.Append("is null ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsNotEmpty.GetStringValue())
            {
                sb.Append("is not null ");
            }
            else //Default if (filterstatus == clsImplementationEnum.NumaricValidation.Equal.GetStringValue())
            {
                sb.Append("= '" + column_value + "' ");
            }
            sb.Append(") ");
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }
        */
        public static List<string> getChecklistYesNo()
        {
            List<string> items = GetListOfDescription<ChecklistYesNo>();
            return items;
        }
        public static List<string> getIMBLineStatus()
        {
            List<string> items = GetListOfDescription<IMBLineStatus>();
            return items;
        }

        public static List<string> getCodeReviewType()
        {
            List<string> items = GetListOfDescription<CodeReviewType>();
            return items;
        }

        public static List<string> getCodeReviewApplicationSubType()
        {
            List<string> items = GetListOfDescription<CodeReviewApplicationSubType>();
            return items;
        }

        public static List<string> getyesna()
        {
            List<string> items = GetListOfDescription<yesna>();
            return items;
        }

        public static List<string> getDegree()
        {
            List<string> items = GetListOfDescription<degree>();
            return items;
        }
        public static List<string> getCodeReviewDatabaseSubType()
        {
            List<string> items = GetListOfDescription<CodeReviewDatabaseSubType>();
            return items;
        }

        public enum HandoverReportType
        {
            [StringValue("Firm")]
            Firm,
            [StringValue("Prospect")]
            Prospect
        }

        public enum NotificationType
        {
            [StringValue("Announcement")]
            Announcement,
            [StringValue("Action Required")]
            ActionRequired,
            [StringValue("Alert")]
            Alert,
            [StringValue("Information")]
            Information,
            [StringValue("Newly Added")]
            NewlyAdded
        }
        public enum UploadType
        {
            [StringValue("")]
            Normal,
            [StringValue("_KM")]
            KM
        }
        public class SearchFilter
        {
            public string ColumnName { get; set; }
            public string Value { get; set; }
            public string FilterType { get; set; }
            public string DataType { get; set; }
        }
        public enum FKMSNodeGenerateFor
        {
            [StringValue("Merge Planner And Designer")]
            MergePlannerAndDesigner,
            [StringValue("Planner")]
            Planner,
            [StringValue("Designer")]
            Designer
        }

        public enum FKMSMaterialDeliveryStatus
        {
            [StringValue("Request not generated")]
            Requestnotgenerated,
            [StringValue("Request Generated")]
            RequestGenerated,
            [StringValue("Request to be regenerated")]
            Requesttoberegenerated,
            [StringValue("Receipt Confirmation Balance")]
            ReceiptConfirmationBalance,
            [StringValue("Material Received")]
            MaterialReceived,
            [StringValue("Delivery Balance")]
            DeliveryBalance,
            [StringValue("Delivered")]
            Delivered
        }

        public enum ImageType
        {
            [StringValue("UserProfile")]
            UserProfile,
            [StringValue("SharePoint")]
            SharePoint,
        }
        public enum DownloadReportFormat
        {
            [StringValue("Pdf")]
            pdf,
            [StringValue("Excel")]
            excel,
        }

        public enum FRMaterialDeliveryStatus
        {
            [StringValue("Request not generated")]
            RequestNotgenerated,
            [StringValue("Request Generated")]
            RequestGenerated,
            [StringValue("Request to be regenerated")]
            RequestToBeRegenerated,
            [StringValue("Receipt Confirmation Balance")]
            ReceiptConfirmationBalance,
            [StringValue("Material Received")]
            MaterialReceived,
            [StringValue("Delivery Balance")]
            DeliveryBalance,
            [StringValue("Delivered")]
            Delivered,
            [StringValue("Request")]
            Request,
            [StringValue("CO")]
            Completed
        }

        #region ICC
        public enum ICCStatus
        {
            [StringValue("Not-Created")]
            Not_Created,
            [StringValue("Draft")]
            Draft,
            [StringValue("Created")]
            Created,
            [StringValue("Submitted")]
            Submitted,
            [StringValue("Approved")]
            Approved,
            [StringValue("Returned")]
            Return
        }

        public enum ICCindex
        {
            [StringValue("m")]
            mdm,
            [StringValue("b")]
            bu,
        }
        #endregion

        #region COP
        public enum BU
        {
            [StringValue("RPV")]
            RPV,
            [StringValue("HTE")]
            HTE
        }

        public enum Process
        {
            [StringValue("Non ESSC")]
            NonESSC,
            [StringValue("ESSC")]
            ESSC
        }

        public enum MOC
        {
            [StringValue("CS")]
            CS,
            [StringValue("LAS")]
            LAS,
            [StringValue("SS")]
            SS
        }

        public enum WeldProcessForCOP
        {
            [StringValue("FC-FCAW")]
            FCFCAW,
            [StringValue("SL-GTAW")]
            SLGTAW,
            [StringValue("ES-ESSC")]
            ESESSC,
            [StringValue("ELB-FCAW")]
            ELBFCAW,
            [StringValue("PC-GTAW")]
            PCGTAW,
            [StringValue("SM-SMAW")]
            SMSMAW,
            [StringValue("ME-GTAW")]
            MEGTAW,
            [StringValue("MES-ESSC")]
            MESESSC,
            [StringValue("MEG-GTAW")]
            MEGGTAW,
        }

        public enum TypeofComponent
        {
            [StringValue("Tubesheet")]
            Tubesheet,
            [StringValue("Blind Flange")]
            BlindFlange,
            [StringValue("Spool")]
            Spool,
            [StringValue("Pipe")]
            Pipe,
            [StringValue("Nozzle Forging")]
            NozzleForging,
            [StringValue("Channel Shell")]
            ChannelShell,
            [StringValue("Elbow")]
            Elbow,
            [StringValue("Flange")]
            Flange
        }

        public enum COP_CommonStatus
        {
            [StringValue("NA")]
            NA,
            [StringValue("NS")]
            NS,
            [StringValue("IP")]
            IP,
            [StringValue("CO")]
            Co,
            [StringValue("Yes")]
            Yes,
        }

        public enum ComponentAssembly
        {
            [StringValue("Component")]
            Component,
            [StringValue("Assembly")]
            Assembly,
        }

        public enum Dispatch
        {
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
        }

        public enum Overlay
        {
            [StringValue("true")]
            True,
            [StringValue("false")]
            False,
            [StringValue("Yes")]
            Yes,
            [StringValue("No")]
            No,
        }

        public enum COPCategory
        {
            [StringValue("<=4 inch")]
            LessThan4,
            [StringValue(">4 and <=14 inch")]
            GreterThan4LessThan14,
            [StringValue(">=14 inch")]
            GreaterThan14,
            [StringValue("<14 inch")]
            LessThan14
        }

        public enum SRE_CMD
        {
            [StringValue("##AVOID##")]
            Avoid
        }
        #endregion

        #region WQ
        public enum WMLType
        {
            [StringValue("ASME")]
            ASME,
            [StringValue("Nuclear")]
            Nuclear,
            [StringValue("EN")]
            EN,
            [StringValue("IBR")]
            IBR,
            [StringValue("Project Specific")]
            ProjectSpecific,
        }
        public static List<string> getWMLType()
        {
            List<string> items = GetListOfDescription<WMLType>();
            return items;
        }
        #endregion

        #region PCR/PCL
        public enum StockStatus
        {
            [StringValue("Created")]
            Created = 1,
            [StringValue("Allocated")]
            Allocated = 2,
            [StringValue("Transferred")]
            Transferred = 3,
            [StringValue("Consumed")]
            Consumed = 4,
            [StringValue("Deleted")]
            Deleted = 10,
        }
        public enum StockPhysicalStatus
        {
            [StringValue("Virtual")]
            Virtual = 1,
            [StringValue("At Store")]
            AtStore = 2,
            [StringValue("At PPS")]
            AtPPS = 3,
            [StringValue("Consumed")]
            Consumed = 4,
            [StringValue("Retained by PPS")]
            RetainedbyPPS = 5,
        }
        public enum PCRType
        {
            [StringValue("Drawing")]
            Drawing = 1,
            [StringValue("Hard Copy")]
            HardCopy = 3,
            [StringValue("Modified - Circle")]
            ModifiedCircle = 4,
            [StringValue("Modified - Rectangle")]
            ModifiedRectangle = 5,
            [StringValue("Modified - Ring")]
            ModifiedRing = 6,
            [StringValue("Modified - Ring Segment")]
            ModifiedRingSegment = 7,
        }
        public enum PCRLineStatus
        {
            [StringValue("Created")]
            Created = 1,
            [StringValue("Released to Material Planner")]
            ReleasedtoMaterialPlanner = 2,
            [StringValue("Released to Nesting")]
            ReleasedtoNesting = 3,
            [StringValue("PCL Generated")]
            PCLGenerated = 4,
            [StringValue("PCL Released")]
            PCLReleased = 5,
            [StringValue("Deleted")]
            Deleted = 10,
        }
        public enum MovementType
        {
            [StringValue("Profile Cutting")]
            ProfileCutting = 1,
            [StringValue("Area Wise Cutting")]
            AreaWiseCutting = 2,
            [StringValue("Full Plate")]
            FullPlate = 3,
        }

        /* Zapimon Added Starts*/
        public enum ILNAppName
        {
            [StringValue("PCR")]
            PCR = 0
        }
        public enum PCRProcess
        {
            [StringValue("Generate PCR")]
            GeneratePCR = 0,
            [StringValue("Maintain Planner Wise Project List")]
            MaintainPlannerWiseProjectList = 1,
            [StringValue("Maintain PCR")]
            MaintainPCR = 2
        }
        /* Zapimon Added Ends*/

        public enum PCLStatus
        {
            [StringValue("PCL Released")]
            PCLReleased = 1,
            [StringValue("Confirmed by PMG")]
            ConfirmedbyPMG = 2,
            [StringValue("Returned by PMG")]
            ReturnedbyPMG = 3,
            [StringValue("Approved by QA")]
            ApprovedbyQA = 4,
            [StringValue("Rejected by QA")]
            RejectedbyQA = 5,
            [StringValue("Confirmed by SFC")]
            ConfirmedbySFC = 6,
            [StringValue("Plate Issued")]
            PlateIssued = 8,
            [StringValue("Plate Received")]
            PlateReceived = 9,
            [StringValue("Plate/Pipe Marked")]
            PlatePipeMarked = 10,
            [StringValue("Approved by QC")]
            ApprovedbyQC = 11,
            [StringValue("Rejected by QC")]
            RejectedbyQC = 12,
            [StringValue("Plate/Pipe Cut")]
            PlatePipeCut = 13,
            [StringValue("To be Returned")]
            TobeReturned = 14,
            [StringValue("Plate Returned")]
            PlateReturned = 15,
            [StringValue("PCL Cancelled")]
            PCLCancelled = 16,
            [StringValue("PCL Closed")]
            PCLClosed = 17,
            [StringValue("Discrepancy in PCL")]
            DiscrepancyinPCL = 18
        }

        public enum MaterialOwner
        {
            [StringValue("PPS")]
            PPS,
            [StringValue("FKS")]
            FKS,
            [StringValue("FMG")]
            FMG,
            [StringValue("KIT")]
            KIT
        }

        public enum MaterialOwnerNames
        {
            [StringValue("PPS")]
            SHOP,
            [StringValue("Fullkit Store")]
            STORE,
            [StringValue("Fixture Manufacturer")]
            FixtureManufacturer,
            [StringValue("KIT OWNER")]
            KITOWNER
        }

        public enum StageType
        {
            [StringValue("2nd Stage Cleared")]
            SecondStageCleared,
            [StringValue("Send To FKM Store")]
            SendToFKMStore,
            [StringValue("Received To FKM Store")]
            ReceivedToFKMStore,
            [StringValue("Sent To Kit User")]
            SentToKitUser,
            [StringValue("Received By Kit User")]
            ReceivedByKitUser,
            [StringValue("Fixture Completed")]
            FixtureCompleted,
            [StringValue("Request To PPS By Kit User")]
            RequestToPPSByKitUser,
            [StringValue("Request To FMG By Kit User")]
            RequestToFMGByKitUser,
            [StringValue("Request To FKM By Kit User")]
            RequestToFKMByKitUser,
            [StringValue("Request To Child Kit User By Kit User")]
            RequestToChildKitUserByKitUser,
            [StringValue("Kit Ready For Parent")]
            KitReadyForParent
        }

        public enum TransactionType
        {
            [StringValue("Received")]
            Received,
            [StringValue("Issued")]
            Issued,
            [StringValue("Request")]
            Request
        }

        public enum MaterialType
        {
            [StringValue("PLT")]
            PLT,
            [StringValue("NPLT")]
            NPLT,
            [StringValue("FXR")]
            FXR,
            [StringValue("KIT")]
            KIT
        }

        public static List<string> getPCRType()
        {
            List<string> items = GetListOfDescription<PCRType>();
            return items;
        }
        public static List<string> getMovementType()
        {
            List<string> items = GetListOfDescription<MovementType>();
            return items;
        }
        public static List<string> getMaterialOwners()
        {
            List<string> items = GetListOfDescription<MaterialOwner>();
            return items;
        }
        public static List<string> getStageTypes()
        {
            List<string> items = GetListOfDescription<StageType>();
            return items;
        }
        public static List<string> getTransactionTypes()
        {
            List<string> items = GetListOfDescription<TransactionType>();
            return items;
        }

        public static List<string> getMaterialTypes()
        {
            List<string> items = GetListOfDescription<MaterialType>();
            return items;
        }
        #endregion

        #region Component Overlay Plan
        public enum MachineLocation
        {
            [StringValue("VTL-West")]
            VTLWest,
            [StringValue("Titan VTL")]
            TitanVTL,
            [StringValue("K-VTL")]
            KVTL,
            [StringValue("MK-VTL")]
            MKVTL,
            [StringValue("L-50")]
            L50,
            [StringValue("L-45 CNC")]
            L45CNC,
            [StringValue("L-50 Manual")]
            L50Manual,
            [StringValue("Lathe")]
            Lathe,
            [StringValue("Outsourced")]
            Outsourced,
            [StringValue("NOT REQUIRED")]
            NOTREQUIRED,
        }

        #endregion

        #region Balance Activity List
        public enum BALStatus
        {
            [StringValue("Draft")]
            Draft,
            [StringValue("Submitted")]
            Submitted,
        }
        #endregion
        #region NDE Type List
        public enum NDEType
        {
            [StringValue("RT")]
            RT,
            [StringValue("UT")]
            UT,
            [StringValue("PT")]
            PT,
            [StringValue("MT")]
            MT
        }

        public enum NDEExecution
        {
            [StringValue("Single")]
            Single,
            [StringValue("Double")]
            Double,

        }
        #endregion





    }



    #endregion

    #region String Attribute Class
    /// <summary>
    /// This attribute is used to represent a string value
    /// for a value in an enum.
    /// </summary>
    public class StringValueAttribute : Attribute
    {
        #region Properties

        /// <summary>
        /// Holds the stringvalue for a value in an enum.
        /// </summary>
        public string StringValue { get; protected set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor used to init a StringValue Attribute
        /// </summary>
        /// <param name="value"></param>
        public StringValueAttribute(string value)
        {
            this.StringValue = value;
        }

        #endregion
    }

    #endregion

    #region CTSStatus
    public enum CTSStatus
    {
        [StringValue("Draft")]
        DRAFT,
        [StringValue("Sent for Approval")]
        SendForApproval,
        [StringValue("Approved")]
        Approved,
        [StringValue("Returned")]
        Returned,
        [StringValue("Superseded")]
        Superseded
    }
    #endregion

    #region Mobile App Permissions Menu Listing
    public enum MobileAppPermissionsMenuList
    {
        [StringValue("Offer/Re-Offer Inspection - Seam")]
        SEAM_OFFER,
        [StringValue("Offer/Re-Offer Inspection – Part/Assembly")]
        PART_OFFER,
        [StringValue("Test Result For Inspection - Part")]
        ATTEND_PART,
        [StringValue("Test Result For Inspection-Seam")]
        ATTEND_SEAM,
        [StringValue("Token List")]
        TOKEN_GENERATION
    }
    #endregion

    #region String Attribute Class
    /// <summary>
    /// This attribute is used to represent a string value
    /// for a value in an enum.
    /// </summary>


    #endregion
}
