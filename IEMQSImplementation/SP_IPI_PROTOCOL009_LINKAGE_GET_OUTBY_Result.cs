//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_IPI_PROTOCOL009_LINKAGE_GET_OUTBY_Result
    {
        public Nullable<decimal> OutBy0 { get; set; }
        public Nullable<decimal> OutBy90 { get; set; }
        public Nullable<decimal> OutBy180 { get; set; }
        public Nullable<decimal> OutBy270 { get; set; }
    }
}
