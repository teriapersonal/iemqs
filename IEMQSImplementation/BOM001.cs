//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class BOM001
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BOM001()
        {
            this.BOM002 = new HashSet<BOM002>();
        }
    
        public int HeaderId { get; set; }
        public string ContractNo { get; set; }
        public string Customer { get; set; }
        public string Project { get; set; }
        public string Status { get; set; }
        public string TemplateNo { get; set; }
        public int RevNo { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BOM002> BOM002 { get; set; }
    }
}
