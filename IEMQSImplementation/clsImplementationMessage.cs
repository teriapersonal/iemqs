﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation
{
    /// <summary>
    /// Implementation Messages class created by dharmesh 24-06-2017
    /// </summary>

    #region Implementation Message class
    public static class clsImplementationMessage
    {
        public static class Session
        {
            public const string LoginInfo = "LoginInfo";
            public const string ProcessList = "ProcessList";
            public const string GlobalCategoryUsed = "GlobalCategoryUsed";
        }

        public static class operationMessages
        {
            public const string Success = "";
            public const string unSuccess = "";
        }

        public static class AuthenticationMessages
        {
            public const string UsetNotExist = "Username or password are wrong";
            public const string UsetNotAuthorized = "User is not authorized, Pls contact to IT/PMG";
            public const string UsetNotISODesignationMaintained = "ISO Designation is not maintain, Please contact to IT";
        }

        public static class ExceptionMessages
        {
            public const string ExceptionMessage = "Username or password are wrong";
            public const string Message = "Something went Wrong..!";
            public const string ErplnErrorMessage = "Could not get data from ERPLN Database. Please contact administrator.";
        }

        public static class AuthMatrixMessages
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Comulative = "Action Performed Successfully and existing records ignored if any";
        }
        public static class DuplicateMessage
        {
            public const string Message = "Record already Exist";
            public const string EMessage = "Something Went Wrong";
            public const string AMessage = "Successfully sent to Approval";
            public const string SMessage = "you can not send to intiator for approval";
        }

        public static class ADDMessage
        {
            public const string Approved = "Document has been approved successfully";
            public const string Return = "Document has been returned";
            public const string Transfered = "Document has been transfered successfully";
            public const string Reject = "Document has been rejected";
            public const string TaskCompleted = "Task is Already been Completed";
        }
        public static class CTQMessages
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Duplicate = "CTQ already exists";
            public const string Error = "Please try again";
            public const string CompilerAction = "CTQ Lines are sent to compiler successfully";
        }

        public static class CSRMessages
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string SentToApprove = "Record has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Approve = "Record has been Approved successfully";
            public const string Returned = "Record has been Returned successfully";
            public const string Duplicate = "already exists";
            public const string LineRejected = "Details has been rejected successfully";
        }


        public static class CommonMessages
        {
            public const string Save = "Document {0} created successfully!";
            public const string Notavailable = "Details not available.";
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string CopyRecord = "Record copied successfully";
            public const string Duplicate = "Record already exists";
            public const string Error = "Please try again";
            public const string sentforApprove = "Document has been sent to approver successfully";            
            public const string Copy = "Document copied successfully";
            public const string Transfer = "Document transferred successfully";
            public const string HeaderInsert = "Document added successfully";
            public const string LineInsert = "Details added successfully";
            public const string HeaderUpdate = "Document updated successfully";
            public const string LineUpdate = "Details updated successfully";
            public const string Approve = "Record has been approved successfully";
            public const string Return = "Record has been returned successfully";
            public const string Retract = "Record has been retracted successfully";
            public const string copyseam = "{0} seams data copied successfully and system has skipped {1} seams";
            public const string transferredseam = "{0} seams data transferred successfully and system has skipped {1} seams";
            public const string DuplicateQID = "QID already exists";
            public const string DuplicateJointType = "Joint type already exists";
            public const string Revision = "Revision has been generated successfully";
            public const string Issued = "Contract issued successfully";
            public const string SessionExp = "Session Has Expired.Please Login again!!";
            public const string InvalidHeaderId = "Invalid header id";
            public const string DocMessage = "Note : Planning DIN Document is not released, you can not revise the Document.";
            public const string DocDeleteMessage = "Note : Planning DIN Document is released, you cannot delete the Document.";
            public const string DocLinked = "Note : Planning DIN Document is linked with FKMS, you cannot delete the Document.";
            public const string DocR0DeleteMessage = "Note :Document is in R0 Revision, You cannot delete the Document.";
            public const string Active = "Record activated successfully";
            public const string Inactive = "Record De-Activated successfully";
            public const string StatusChanged = "This record status has been changed already, Please refresh the page.";
            //For WMI
            public const string RecordSave = "Record Saved Successfully";
            //for HTR 
            public const string Promote = "{0} successfully submitted";
            public const string Revoke = "Record has been revoke successfully!";
            public const string UnSuccess = "Unsuccessfully Attempt!";
            public const string pdnRevokeMessage = "Document is already released,You can not revoke now!";
            //for ITP PO Lines
            public const string poLineInsert = "PO lines added succesfully";
            public const string poLineUpdate = "PO lines updated succesfully";
            //PTR
            public const string LABID = "Lab Id Generated succesfully";
            //Protocol
            public const string ProtocolSaved = "Protocol saved successfully";
            public const string ProtocolExists = "Protocol Number already exists";
            public const string SpotAdded = "Spot added successfully";
            public const string SpotUpdate = "Spot updated successfully";
            public const string DataAdded = "Data updated successfully";
            public const string DataUpdate = "Data updated successfully";
            //Linkage
            public const string LinkageSaved = "Protocol saved successfully";

            public const string AppVersionReleased = "App version released successfully!";
            public const string AppVersionNoLines = "Please add atleast one details for released!";
            //for CFAR 
            public const string SubmitedDetails = "{0}  {1}";
            public static string ProjectExist = "Project {0} already exists";
            public static string ProjectExistInHBOM = "Project {0} already exists in HBOM";
            public static string ProjectExistInPLM = "Project {0} not exists in PLM";
            //for add
            public const string SubmitDetailsto = "Document : {0} has been {1} successfully";
            public const string alreadySubmitDetails = "Document : {0} is already {1} ";
            public const string doesnotSubmitDetails = "Document : {0} cannot be {1}. Please check status!";
            public static string ProjectNotExistInLN = "{0} data not maintained by marketing in LN.";
            //FR
            public static string ProjectNotExistInFKMS = "Project {0} not exists in FKMS";
            public static string ProjectNotExistInPDN = "Project {0} not exists in Planning Module";
            public const string ProjectMergeDemerge = "Project Merge/Demerge successfully";
            public const string ReleasedDINProjectMerge = "You can not merge/demerge project for Released DIN!";
            public const string SubContarctingFormError = "Please fill subcontracting form for all fixture";

            //WCS            
            public const string CalculationError = "Some error occured in Calculation. Please try again";
            public const string NotInEditStatus = "This data is already submitted, You can not modify now!!";
            public const string AlreadySubmitted = "This data has been already submitted";
            public const string AlreadyAttended = "This data has been already attended";
        }


        public static class ICLPartMessages
        {
            public const string HeaderInsert = "Document added successfully";
            public const string LineInsert = "Details added successfully";
            public const string HeaderUpdate = "Document updated successfully";
            public const string LineUpdate = "Details updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Approve = "Document has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Copy = "Document copied successfully";
        }
        public static class ICLSeamMessages
        {
            public const string HeaderInsert = "Document added successfully";
            public const string LineInsert = "Details added successfully";
            public const string HeaderUpdate = "Document updated successfully";
            public const string LineUpdate = "Details updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Approve = "Document has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string SentForApproval = "{0} Seam ICL is sent for approval successfully";
            public const string Copy = "Document copied successfully";
        }
        public static class TestPlanMessages
        {
            public const string HeaderInsert = "Document added successfully";
            public const string LineInsert = "Details added successfully";
            public const string HeaderUpdate = "Document updated successfully";
            public const string LineUpdate = "Details updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Approve = "Document has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Copy = "Document copied successfully";
            public const string SentForApproval = "{0} Test Plan is sent for approval successfully";
            public const string DuplicateStage = "Stage with same sequence already exists";
        }
        public static class NDEConsumeMessage
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Approve = "Record has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Category = "Please add 'Cellulos + Water' in 'Couplant' category";
        }

        public static class NDETechniquesMessage
        {
            public const string HeaderInsert = "Document added successfully";
            public const string LineInsert = "Details added successfully";
            public const string HeaderUpdate = "Document updated successfully";
            public const string LineUpdate = "Details updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Approve = "Document has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Copy = "Document copied successfully";
        }

        public static class ProbeMessages
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string Error = "Please try again";
        }
        public static class QCPMessages
        {
            public const string Approve = "Approve successfully";
            public const string Return = "Returned successfully";
            public const string ReturnValidation = "Please add Remark(s) for return";
        }

        public static class PTMTCTQMessages
        {
            public const string Insert = "Document added successfully";
            public const string Update = "Document updated successfully";
            public const string Delete = "Document deleted successfully";
            public const string Duplicate = "PT/MT CTQ already exists";
            public const string Error = "Please try again";
        }

        public static class PlanMessages
        {
            public const string Insert = "Document added successfully";
            public const string Update = "Document updated successfully";
            public const string Delete = "Document deleted successfully";
            public const string Duplicate = "Plan already exists";
            public const string Error = "Please try again";
        }

        public static class TokemMessages
        {
            public const string Insert = "Token Generated Successfully";
            public const string Duplicate = "Token Already Generated";
            public const string Error = "Please try again";
            public const string OverLimit = "Token Limit is exceeded";
        }

        //created by nikita for Planning checklist
        public static class PLCMessages
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string SentToApprove = "Record has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Approve = "Record has been Approved successfully";
            public const string Returned = "Record has been Returned successfully";
            public const string Duplicate = "already exists";
        }

        public static class LockingCleatMessages
        {
            public const string Insert = "Record added successfully";
            public const string Update = "Record updated successfully";
            public const string Delete = "Record deleted successfully";
            public const string SentToApprove = "Record has been sent to approver successfully";
            public const string Error = "Please try again";
            public const string Approve = "Record has been Approved successfully";
            public const string Returned = "Record has been Returned successfully";
            public const string Duplicate = "already exists";
            public const string MustApproveLines = "All details of Locking cleat and Tack weld must be approved";
            public const string LineRejected = "Details has been rejected successfully";
        }

        public static class RTCTQMessages
        {
            public const string Insert = "Document added successfully";
            public const string Update = "Document updated successfully";
            public const string Delete = "Document deleted successfully";
            public const string Duplicate = "RT CTQ already exists";
            public const string Error = "Please try again";
        }

        public static class UTCTQMessages
        {
            public const string Insert = "Document added successfully";
            public const string Update = "Document updated successfully";
            public const string Delete = "Document deleted successfully";
            public const string NullRecord = "All (*) Mandatory fields from {0} are required";
            public const string Duplicate = "RT CTQ already exists";
            public const string Error = "Please try again";
        }

        public static class PTTechniqueMeassage
        {
            public const string Save = "Record Saved Successfully";
            public const string SentForApproval = "Successfully Sent For Approval";
            public const string Delete = "Record deleted successfully";
            public const string Approve = "Approved successfully";
            public const string Return = "Returned successfully";
        }
        public static class MailMessage
        {
            public const string Save = "Record Saved Successfully";
            public const string Delete = "Record deleted successfully";
            public const string Update = "Record Update Successfully";
            public const string Error = "Please try after sometime";

        }
        public static class QualityProjectMessage
        {
            public const string Save = "Quality Project {0} created and activated successfully!";
            public const string Delete = "Quality Project Deleted Successfully";
            public const string Update = "Quality Project {0} Updated Successfully";
            public const string Duplicate = "Quality Project {0} Already Exists";
            public const string Error = "Please try after sometime";

        }

        public static class QualityIDMessage
        {
            public const string Save = "Header Saved successfully!";
            public const string Delete = "{0} QID(s) is/are deleted successfully";
            public const string Update = "Header Updated Successfully";
            public const string Duplicate = "Quality Project Already Exists";
            public const string Error = "Please try after sometime";
            public const string AlreadySentForApproval = "QualityId Header Has Already Been Sent For Approval";
            public const string SentForApproval = "{0} QID is sent for approval successfully";
            public const string Approve = "{0} QID Approved successfully";
            public const string Returne = "{0} QID Returned successfully";
        }

        public static class QualityIDLineMessage
        {
            public const string Save = "QualityId Stage Saved successfully!";
            public const string Delete = "QualityId Stage Deleted Successfully";
            public const string Update = "QualityId Stage Updated Successfully";
            public const string Duplicate = "Stage Already Exists";
            public const string Error = "Please try after sometime";

        }
        public static class FDMS
        {
            public const string Create = "FDMS Created Successfully..!";
            public const string Update = "FDMS Updated Successfully..!";

            public const string LUpdate = "Lines Updated  Successfully..!";
            public const string LInsert = "Lines Created Successfully..!";

            public const string sentToFDR = "FDMS Successfully sent to Final Document Requirement State..!";

            public const string FDRPromte = "FDMS Submitted to Document Assignment State..!";
            public const string FDRDemote = "FDMS Returned to Draft..!";

            public const string DocPromte = "FDMS Submitted to SCN Date Verification..!";
            public const string DocDemote = "FDMS Returned to Final Document Requirement State..!";

            public const string SCNPromte = "FDMS Submitted to Document Submission and Verification..!";
            public const string SCNDemote = "FDMS Returned to Document Assignment State..!";

            public const string DocVerificationPromte = "FDMS Submitted to Document Submission to PMG..!";
            public const string DocVerificatioDemote = "FDMS Returned to SCN Date Verification..!";

            public const string QAPromte = "FDMS Submitted to Document Acceptance by PMG..!";
            public const string QADemote = "FDMS Returned to Document Submission and Verification..!";

            public const string PMGPromte = "FDMS is Completed..!";
            public const string PMGDemote = "FDMS Returned to Document Submission to PMG..!";

            public const string Error = "Something went Wrong..!";

            public const string LApprove = "Lines Approved  Successfully..!";
            public const string LReject = "Lines Rejected Successfully..!";
        }

        public static class AWSBrandMaster
        {
            public const string Save = "AWS Brand Saved successfully!";
            public const string RestrictDelete = "{0} is being used in WPS, it cannot be deleted";
            public const string Delete = "{0} is deleted successfully";
            public const string Update = "AWS Brand Updated successfully!";
        }

        public static class FillerMetalMaster
        {
            public const string Save = "Filler Metal Saved successfully!";
            public const string RestrictDelete = "This record is being used in WPS, it cannot be deleted";
            public const string Delete = "The record is deleted successfully";
        }

        public static class WPSKeyMaster
        {
            public const string Save = "WPS Key Saved successfully!";
            public const string RestrictDelete = "Key : {0}, {1} are being used in WPS Index Master, it cannot be deleted";
            public const string RestrictUpdate = "Key : {0}, {1} are being used in WPS Index Master, it cannot be Modified";
            public const string Delete = "Key : {0}, {1} are deleted successfully";
        }

        public static class WPPKeyMaster
        {
            public const string Save = "WPP Key Saved successfully!";
            public const string RestrictDelete = "Key : {0} is being used in WPP Index Master, it cannot be deleted";
            public const string RestrictUpdate = "Key : {0} is being used in WPP Index Master, it cannot be Modified";
            public const string Delete = "Key : {0} is deleted successfully";
        }

        public static class WPSNoIndexMaster
        {
            public const string Save = "WPS No. Saved successfully!";
            public const string RestrictDelete = "WPS : {0} cannot be deleted, it is used to generate WPS";
            public const string RestrictUpdate = "WPS : {0} is being used to generate WPS , it cannot be Modified";
            public const string Delete = "WPS : {0} is deleted successfully";
            public const string WPSKeyNoNotExist = "WPS Key {0},{1} not exist for {2} location.";
            public const string JointTypeNotDefine = "No Joint Type define for WPS Key {0},{1} in {2} location.";
        }

        public static class WPPNoIndexMaster
        {
            public const string Save = "WPP No. Saved successfully!";
            public const string RestrictDelete = "WPP : {0} cannot be deleted, it is used to generate WPP";
            public const string RestrictUpdate = "WPP : {0} is being used to generate WPS , it cannot be Modified";
            public const string Delete = "WPP : {0} is deleted successfully";
        }
        public static class WPSHeader
        {
            public const string Save = "Document Saved successfully!";
            public const string RestrictDelete = "WPS : {0} cannot be deleted as it is already approved";
            public const string Delete = "WPS : {0} Is Deleted Successfully";
            public const string SentForApproval = "WPS : {0} Is Successfully Sent For Approval";
            public const string Approved = "WPS : {0} Is Successfully Approve";
            public const string Returned = "WPS : {0} Is Successfully Returned";
            public const string ApprovedMultiple = "{0} WPS are Successfully Approve";
        }
        public static class WPPHeader
        {
            public const string Save = "Document Saved successfully!";
            public const string RestrictDelete = "WPP : {0} cannot be deleted as it is already approved";
            public const string Delete = "WPP : {0} Is Deleted Successfully";
            public const string SentForApproval = "WPP : {0} Is Successfully Sent For Approval";
            public const string Approved = "WPP : {0} Is Successfully Approve";
            public const string ApprovedMultiple = "{0} WPP are Successfully Approve";
            public const string Released = "{0} is successfully Released ! ";
            public const string ReleasedMultiple = "{0} WPP are successfully Released ! ";

        }
        public static class WPSLine
        {
            public const string Save = "Line Saved successfully!";
            public const string RestrictDelete = "WPS : {0} Cannot be deleted, It is used to generate WPS";
            public const string RestrictUpdate = "WPS : {0} Is Being Used To Generate WPS , It Cannot Be Modified";
            public const string Delete = "Line Deleted Successfully";
            public const string SentForApproval = "WPS : {0} Is Successfully Sent For Approval";
            public const string Approved = "WPS : {0} Is Successfully Approved";
        }

        public static class WKGHeader
        {
            public const string Save = "Document Saved successfully!";
            public const string RestrictDelete = "WPS : {0} cannot be deleted as it is already approved";
            public const string Delete = "Document Is Deleted Successfully";
            public const string SentForApproval = "Document Is Successfully Sent For Approval";
            public const string Approved = "Document Is Successfully Approved";
        }

        public static class KOMMessage
        {
            public const string Update = "Record updated successfully";
            public const string Status = "Status updated successfully";
            public const string Delete = "Record Deleted successfully";
            public const string FLine = "Add Functional Lead For All Lines";
            public const string SentForApprovalValid = "Add Remarks For All Lines";


        }

        public static class FRMessage
        {
            public const string SentForApprove = "Fixture details has been sent to approver successfully";
            public const string AllocateSuccess = "Fixture allocated successfully";
            public const string DeAllocateSuccess = "Fixture de-allocated successfully";
            public const string AutoAllocateRemarks = "Some of quantity(s) are auto allocated from other Kit(s).";
            public const string ReviseSuccess = "Fixture revised successfully";
            public const string ReviseMismatch = "Revise quantity(s) not matching with new allocation";
        }
        

        public const string GeneratError = "Error occurred during a operation. Please contact administrator.";

    }

    #endregion

}
