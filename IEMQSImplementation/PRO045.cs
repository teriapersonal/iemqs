//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO045
    {
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string TypeOfForming { get; set; }
        public string IdentificationMarking { get; set; }
        public string PtcRequired { get; set; }
        public string ItNo { get; set; }
        public string HeatNo { get; set; }
        public string WepUt { get; set; }
        public string WepPt { get; set; }
        public string WepMt { get; set; }
        public string WepCust { get; set; }
        public string SeamNoPunching { get; set; }
        public string NdtRt { get; set; }
        public string NdtTofdUt { get; set; }
        public string NdtUt { get; set; }
        public string WepAngleIs { get; set; }
        public string WepAngleOs { get; set; }
        public string TacksFreeFromCrack { get; set; }
        public Nullable<decimal> RootGapRequired { get; set; }
        public Nullable<decimal> RootGapActMax { get; set; }
        public Nullable<decimal> RootGapActMin { get; set; }
        public Nullable<decimal> RootFaceRequired1 { get; set; }
        public Nullable<decimal> RootFaceActMax1 { get; set; }
        public Nullable<decimal> RootFaceActMin1 { get; set; }
        public Nullable<decimal> RootFaceRequired2 { get; set; }
        public Nullable<decimal> RootFaceActMax2 { get; set; }
        public Nullable<decimal> RootFaceActMin2 { get; set; }
        public Nullable<decimal> ThicknessReqired { get; set; }
        public Nullable<decimal> ThicknessActual1 { get; set; }
        public Nullable<decimal> ThicknessActual2 { get; set; }
        public Nullable<decimal> ThicknessActual3 { get; set; }
        public Nullable<decimal> ThicknessActual4 { get; set; }
        public Nullable<decimal> ThicknessActual5 { get; set; }
        public Nullable<decimal> ThicknessActual6 { get; set; }
        public Nullable<decimal> ThicknessActual7 { get; set; }
        public Nullable<decimal> ThicknessActual8 { get; set; }
        public Nullable<decimal> ThicknessActual9 { get; set; }
        public Nullable<decimal> OffsetTolerance { get; set; }
        public Nullable<decimal> OffsetActual1 { get; set; }
        public Nullable<decimal> OffsetActual2 { get; set; }
        public Nullable<decimal> OffsetActual3 { get; set; }
        public Nullable<decimal> ProfileMeasurement { get; set; }
        public string SeamNo1 { get; set; }
        public string SeamNo2 { get; set; }
        public string FirstEndPeakIn1 { get; set; }
        public string FirstEndPeakIn2 { get; set; }
        public string FirstEndPeakOut1 { get; set; }
        public string FirstEndPeakOut2 { get; set; }
        public string MidPeakIn1 { get; set; }
        public string MidPeakIn2 { get; set; }
        public string MidPeakOut1 { get; set; }
        public string MidPeakOut2 { get; set; }
        public string SecondEndPeakIn1 { get; set; }
        public string SecondEndPeakIn2 { get; set; }
        public string SecondEndPeakOut1 { get; set; }
        public string SecondEndPeakOut2 { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq1 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq2 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq3 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq4 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq5 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq6 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq7 { get; set; }
        public Nullable<decimal> ProfileMeasurementReq8 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance1 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance2 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance3 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance4 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance5 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance6 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance7 { get; set; }
        public Nullable<decimal> ProfileMeasurementTolerance8 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual1 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual2 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual3 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual4 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual5 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual6 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual7 { get; set; }
        public Nullable<decimal> ProfileMeasurementActual8 { get; set; }
        public Nullable<decimal> CircumferenceReq1 { get; set; }
        public Nullable<decimal> CircumferenceReq2 { get; set; }
        public Nullable<decimal> CircumferenceReq3 { get; set; }
        public Nullable<decimal> CircumferenceReq4 { get; set; }
        public Nullable<decimal> CircumferenceReq5 { get; set; }
        public Nullable<decimal> CircumferenceTolerance1 { get; set; }
        public Nullable<decimal> CircumferenceTolerance2 { get; set; }
        public Nullable<decimal> CircumferenceTolerance3 { get; set; }
        public Nullable<decimal> CircumferenceTolerance4 { get; set; }
        public Nullable<decimal> CircumferenceTolerance5 { get; set; }
        public Nullable<decimal> CircumferenceActual1 { get; set; }
        public Nullable<decimal> CircumferenceActual2 { get; set; }
        public Nullable<decimal> CircumferenceActual3 { get; set; }
        public Nullable<decimal> CircumferenceActual4 { get; set; }
        public Nullable<decimal> CircumferenceActual5 { get; set; }
        public Nullable<decimal> OrientationReq1 { get; set; }
        public Nullable<decimal> OrientationReq2 { get; set; }
        public Nullable<decimal> OrientationReq3 { get; set; }
        public Nullable<decimal> OrientationReq4 { get; set; }
        public Nullable<decimal> OrientationAct11 { get; set; }
        public Nullable<decimal> OrientationAct12 { get; set; }
        public Nullable<decimal> OrientationAct13 { get; set; }
        public Nullable<decimal> OrientationAct14 { get; set; }
        public Nullable<decimal> OrientationAct15 { get; set; }
        public Nullable<decimal> OrientationAct21 { get; set; }
        public Nullable<decimal> OrientationAct22 { get; set; }
        public Nullable<decimal> OrientationAct23 { get; set; }
        public Nullable<decimal> OrientationAct24 { get; set; }
        public Nullable<decimal> OrientationAct25 { get; set; }
        public string OrientationRemark1 { get; set; }
        public string OrientationRemark2 { get; set; }
        public string OrientationRemark3 { get; set; }
        public string OrientationRemark4 { get; set; }
        public string OrientationRemark5 { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public string Note4 { get; set; }
        public string Note5 { get; set; }
        public string Note6 { get; set; }
        public string Note8 { get; set; }
        public string Note9 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Note1_1 { get; set; }
    }
}
