//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL042
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Location { get; set; }
        public Nullable<decimal> At0 { get; set; }
        public Nullable<decimal> At90 { get; set; }
        public Nullable<decimal> At180 { get; set; }
        public Nullable<decimal> At270 { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual PRL040 PRL040 { get; set; }
    }
}
