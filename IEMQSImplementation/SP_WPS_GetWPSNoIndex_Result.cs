//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_WPS_GetWPSNoIndex_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public string Location { get; set; }
        public Nullable<int> Key1 { get; set; }
        public Nullable<int> Key2 { get; set; }
        public string QualityProject { get; set; }
        public string WPSNumber { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<bool> InUsed { get; set; }
        public Nullable<int> WPSHeaderId { get; set; }
    }
}
