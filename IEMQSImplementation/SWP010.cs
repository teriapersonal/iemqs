//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class SWP010
    {
        public int HeaderId { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string SeamNo { get; set; }
        public string JointType { get; set; }
        public string MainWPSNumber { get; set; }
        public Nullable<int> MainWPSRevNo { get; set; }
        public string AlternateWPS1 { get; set; }
        public Nullable<int> AlternateWPS1RevNo { get; set; }
        public string AlternateWPS2 { get; set; }
        public Nullable<int> AlternateWPS2RevNo { get; set; }
        public Nullable<int> Preheat { get; set; }
        public string DHTISR { get; set; }
        public string SWPNotes { get; set; }
        public string Status { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string Returnremarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string SeamDescription { get; set; }
        public Nullable<int> SeamRev { get; set; }
        public string ReturnedBy { get; set; }
        public Nullable<System.DateTime> ReturnedOn { get; set; }
    }
}
