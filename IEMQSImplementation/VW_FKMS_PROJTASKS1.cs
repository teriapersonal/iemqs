//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_FKMS_PROJTASKS1
    {
        public Nullable<int> ParentTaskUID { get; set; }
        public double projectid { get; set; }
        public string projectname { get; set; }
        public double taskid { get; set; }
        public string TaskDesc { get; set; }
        public Nullable<double> TASK_STATUS { get; set; }
        public Nullable<System.DateTime> EXPECTED_START_DATE { get; set; }
        public Nullable<System.DateTime> EXPECTED_END_DATE { get; set; }
        public string taskmanager { get; set; }
        public string Location { get; set; }
        public string workCenter { get; set; }
    }
}
