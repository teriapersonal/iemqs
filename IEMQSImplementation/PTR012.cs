//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PTR012
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PTR012()
        {
            this.PTR012_Log = new HashSet<PTR012_Log>();
        }
    
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public string RefStd { get; set; }
        public string SOPNo { get; set; }
        public string RequiredValue { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
        public string Note1 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PTR012_Log> PTR012_Log { get; set; }
    }
}
